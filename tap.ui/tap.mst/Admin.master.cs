﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;
#endregion

namespace tap.ui.mst
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //security check- check this user is admin or not and if admin then he is authorised or not
            int userId = Convert.ToInt32(Session["UserId"]);
       
            dom.gen.Security userSecurity = new dom.gen.Security();

            userSecurity.IsUserAutherised(userId, Response);
        }
    }
}