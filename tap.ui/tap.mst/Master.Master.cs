﻿//---------------------------------------------------------------------------------------------
// File Name 	: Master.Master.cs

// Date Created  : N/A

// Description   : Master page for all pages
//---------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using tap.dom;
using tap.dom.adm;
using tap.dom.hlp;
using tap.dom.usr;
using System.Web.Services;
using tap.dom.gen;
using System.Globalization;
using System.Configuration;

namespace tap.ui.mst
{
    public partial class Master : System.Web.UI.MasterPage
    {

        public string _isSU = ConfigurationManager.AppSettings["IsSU"];

        tap.dom.adm.Company _company = new dom.adm.Company();
        tap.dom.adm.Divisions _division = new dom.adm.Divisions();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.usr.Event _event = new dom.usr.Event();
        tap.dom.adm.SiteSettings _sitesetting = new dom.adm.SiteSettings();

        private int _eventId;

        #region "Security Access for EventId"

        /// <summary>
        /// Get Event Id
        /// </summary>
        public int EventId
        {
            get
            {
                int i = 0;
                string routeEventId = Convert.ToString(Page.RouteData.Values["EventId"]);             
                int.TryParse(_cryptography.Decrypt(routeEventId), out i);
                return i;
            }
        }

        /// <summary>
        /// Get UserId
        /// </summary>
        public int UserId
        {
            get
            {
                return Convert.ToInt32(Session["UserId"]);
            }
        }

        /// <summary>
        /// Get Encrypted EventId
        /// </summary>
        public string EncryptedEventId
        {
            get
            {
                return Convert.ToString(Session["EncryptedEventId"]);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            dom.gen.Security security = new dom.gen.Security();
         
                if (EventId > 0)
                {                  
                    security.CheckEventAccess(EventId, UserId, Response);

                }
           
        }

        #endregion

        #region "Page Load Event"
        /// <summary>
        /// Store the comapnyid and virtual location in hidden fields 
        /// for use in the js files while calling the service methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Page Title           
                if (Page.RouteData.Values["EventId"] != null && !string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()))
                {
                    string EventName = _event.getEventName(Page.RouteData.Values["EventId"].ToString());
                    if(!string.IsNullOrEmpty(EventName))
                        Page.Title = "TapRooT® - " + EventName;
                }

                bool isSUCompany = _company.isSUCompany(Convert.ToInt32(Session["CompanyId"]));
                if (isSUCompany && _isSU != null && _isSU.Equals("1"))
                    hdnSUCompany.Value = "1";

                bool isSUDivision = _company.isSUDivision(Convert.ToInt32(Session["CompanyId"]));
                if (isSUDivision && _isSU != null && _isSU.Equals("1"))
                    hdnSUDivision.Value = "1";

                int divisionId = 0;
                if (Page.RouteData.Values["DivisionID"] != null)
                    int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);

                Session["DivisionId"] = null;
                if (divisionId > 0)
                {
                    hdnDivisionId.Value = divisionId.ToString();
                    Session["DivisionId"] = divisionId.ToString();
                }



                if (Request.QueryString["setup"] == "1")
                {
                    string details = _division.CompanyAdminSetUpDetails();
                    if (details != null && details.Contains(':'))
                    {
                        string[] arrDetails = details.Split(':');
                        if (arrDetails != null)
                        {
                            Session["UserId"] = arrDetails[1];
                            Session["CompanyId"] = arrDetails[0];
                            Session["UserName"] = arrDetails[2];
                            Session["FirstName"] = arrDetails[3];
                        }
                    }

                    Response.Redirect("~/Admin/Configuration/AdminSetting/1");
                }
                if ((Session.Count == 0) || (Convert.ToString(Session["CompanyId"]) == string.Empty))
                {
                    Response.Redirect("~/tap.usr/Login.aspx?message=1");
                }

                //Set the hidden fileds value, those will set as the value of some javascript common global variable
                hdnCompanyId.Value = Convert.ToString(Session["CompanyId"]);
                hdnUserId.Value = Convert.ToString(Session["UserId"]);
                hdnUserName.Value = Convert.ToString(Session["UserName"]);
                hdnFirstName.Value = Convert.ToString(Session["FirstName"]);
                hdnUserDateFormat.Value = Convert.ToString(Session["DateFormat"]);
                //SessionService.CurrentCulture = CultureInfo.CurrentCulture;

                //Display banner image
                string myImgPath = CommonOperation.GetSiteRoot() + "/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(Session["CompanyId"]);
                clientLogo.Src = myImgPath;                
                //divBanner.Attributes["style"] = "background-image:url(" + myImgPath + ");height:105px;";
                
                //CheckPasswordPolicy();
                lblUser.InnerText = " " + Session["FirstName"].ToString().First().ToString().ToUpper() + String.Join("", Session["FirstName"].ToString().Skip(1));

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
        #endregion

        protected void CheckPasswordPolicy()
        {
            Password password = new Password();
            bool isPasswordMatched = password.MatchPasswordPolicy(UserId);
            if (!isPasswordMatched)
                hdnIsPasswordOutOfPolicy.Value = "1";
            else
                hdnIsPasswordOutOfPolicy.Value = "0";

            bool isPasswordExpired = password.PasswordExpired(UserId);
            if (isPasswordExpired)
                hdnIsPasswordExpired.Value = "1";
            else
                hdnIsPasswordExpired.Value = "0";
           
        }

    }


}