﻿//Global Variables
var _currentTabId = ''; ;
var _currentEventId = '';
var _data = '';
var _vEventId = '';
var _treeFieldId = '';

var _sysCheckedNodes = new Array();
var _sysCheckedNames = new Array();

var _systemId = '';
var _systemData = '';
var _treeFieldId = '';
var _listId = '';
//_classificationIdArray = new Array;


_classificationCheckedNodes = new Array();
_classificationCheckedNames = new Array();

var _classificationId = '';
var _classificationFieldId = '';
var _classificationFieldCount = '';
//-----------------
var _fields = new Object();
var _eventId = '';
var _undoStack = new Array;
var _customListLabel = '';
var _subTabName = '';
var _allcustomtreeviewIds = new Array();
//-------------------

$(document).ready(function () {
  
    $('#ancUndo').hide();
    $('#ancUndoAll').hide();
  
    //hide admin menu 
    HideMenus();

    //hide Custom Popups
    CloseCustomFieldPopup();

    
    GetIdFromQueryString();


    $('#btnUsrFieldTreeOk').click(function () {
       
        PopulateSystemlistData();
    });

    $('#btnSelectEventClassificationTest').click(function () {
        CustomListDisplay();
    });

    $('#btnCustomCancel').click(function () {
        CloseCustomFieldPopup();
    });

    $('#btnCustomTreeCancel').click(function () {
        CloseCustomFieldPopup();
    });
 });

//Hide menus
function HideMenus() {
    $('#menu').hide();
    $('#AdminConfigMenu').hide();
}

//Close custom field pop up
function CloseCustomFieldPopup() {

    $('#divCustomSelectOneTreeview').hide();
    $("#divCustomSelectOneTreeview").dialog("destroy");
    $('#divCustomSelectOneTreeview').bind("dialogclose", function (event, ui) {
    });


    $('#divCustomMultipleSelectTreeview').hide();
    $("#divCustomMultipleSelectTreeview").dialog("destroy");
    $('#divCustomMultipleSelectTreeview').bind("dialogclose", function (event, ui) {
    });

}

//Save FieldValue
function SaveFieldValues() {

    var type = '';
    var fieldValue = '';
    var fieldId = '';
    var eventId = '';
    var controlId = '';
    var rbYesValue = '';
    var rbNoValue = '';

    var childControls = $('#ulCustomField').children();

    if (childControls != null && childControls != undefined) {

        $('#ulCustomField').each(function () {

            eventId = _currentEventId;

            $(this).find('input').each(function () {

                type = $(this).attr('type');
                controlId = $(this).attr('id');
                fieldId = controlId.substring(3, controlId.length);

                //For Text Box
                if (type == 'text') {

                    fieldValue = $(this).attr('value');
                    _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                    SaveFieldValue(_fielddetails);

                }

                //For checkbox
                else if (type == 'checkbox') {

                    fieldValue = $(this).attr('checked') ? 1 : 0;
                    _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                    SaveFieldValue(_fielddetails);

                }

                //For Radioo Buttons
                else if (type == 'radio') {

                    if (controlId.substring(0, 3) == 'rbY') {
                        rbYesValue = $(this).attr('checked') ? 1 : 0;
                    }
                    else {
                        _rbnvalue = $(this).attr('checked') ? 1 : 0;
                        
                        if (rbYesValue == 1) {
                            fieldValue = 1;

                            _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                            SaveFieldValue(_fielddetails);

                        }
                        else if (_rbnvalue == 1) {
                            fieldValue = 0;

                            _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                            SaveFieldValue(_fielddetails);
                        }

                        rbYesValue = '';
                        _rbnvalue = '';
                    }
                }
            });
            //TextArea
            $(this).find('textarea').each(function () {

                controlId = $(this).attr('id');
                fieldId = controlId.substring(7, controlId.length);
               
                fieldValue = $(this).val();

                _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                SaveFieldValue(_fielddetails);
            });
            //For dropdownlist
            $(this).find('select').each(function () {
                controlId = $(this).attr('id');
                fieldId = controlId.substring(3, controlId.length);
                fieldValue = $(this).val();

                _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                SaveFieldValue(_fielddetails);

            });

            //For location
            $(this).find('label').each(function () {
                controlId = $(this).attr('id');
                if (controlId.indexOf('Cust') > 0) {

                    fieldId = controlId.substring(7, controlId.length);
                    fieldValue = $('#hdn' + fieldId).val();

                    _fielddetails = new FieldValue(-1, _currentEventId, fieldId, fieldValue);
                    SaveFieldValue(_fielddetails);
                }
            });
        });
    }

}


//Save field value
function SaveFieldValue(fieldDetails) {

    _data = JSON.stringify(fieldDetails);
    var serviceURL = _baseURL + 'Services/FieldValues.svc/SaveFieldValues';
    AjaxPost(serviceURL, _data, eval(SaveFieldValues_Succ), eval(SaveFieldValues_Fail));

}

//On success of SaveField
function SaveFieldValues_Succ(result) {

    if (result.d) {

        var message = 'Field _data saved successfully.';
        var messageType = ' jSuccess ';
        NotificationMessage(message, messageType, true, 1000);

    }

}

//On fail of SaveField Values
function SaveFieldValues_Fail() {
}

//Set field value
function FieldValue(fieldValueId, eventId, fieldId, fieldValue) {

    var fieldDetails = {};
    fieldDetails.fieldValueId = fieldValueId;
    fieldDetails.eventId = eventId;
    fieldDetails.fieldId = fieldId;
    fieldDetails.fieldValue = fieldValue;
    return fieldDetails;
}

//Get id from query string
function GetIdFromQueryString() {
  
    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    _subTabName = GetDetailsSubTabDetails(urlArray[urlArray.length - 3], 'name');
    //_tabId = GetDetailsSubTabDetails(urlArray[urlArray.length - 3], 'id');//we can use in future if we need TabId

    _currentTabId = urlArray[urlArray.length - 2][1];
    _currentEventId = urlArray[urlArray.length - 1];

}


//Get sub tab details
function GetDetailsSubTabDetails(subTabName, val) {

    var subTab = '';
    var arrayName = subTabName.split('-');

    if (val == 'name') {

        if (arrayName.length > 2) {
            subTab = subTabName.substring(0, subTabName.lastIndexOf('-')).replace(/-/g, ' ');
        }
        else {
            subTab = arrayName[0];
        }

    }
    else if (val == "id") {

        subTab = arrayName[arrayName.length - 1];

        if (subTab.indexOf('#') != -1) {
            subTab = subTab.replace('#', "");
        }
    }

    return subTab;
}

//Validate control value
function ValiadteControlValue() {

    var errorMessage = '';
    var controlId = '';
    var fieldId = '';
    var childControls = $('#ulCustomField').children();

    if (childControls != null && childControls != undefined) {
       
        $('#ulCustomField').each(function () {

            $(this).find('span').each(function () {

                controlId = $(this).attr('id');

                if (controlId != undefined && controlId != '') {
                    fieldId = controlId.substring(3, controlId.length);
                   
                    if ($(this).text() == '*') {

                        if ($('#txtarea' + fieldId).val() != '')
                            ShowHideWarningMessage('txtarea' + fieldId, 'Please specify', false);
                        if ($('#txt' + fieldId).val() != '')
                            ShowHideWarningMessage('txt' + fieldId, 'Please specify', false);
                        if ($('#rbY' + fieldId).length > 0 || $('#rbN' + fieldId).length > 0)
                            //  ShowHideWarningMessage('rbN' + fieldId, 'Please specify', false);
                            var radioField = $('#rbN' + fieldId).next();
                            ShowHideWarningMessageForRadio(radioField, 'Please specify', false);

                        if ($('#hdn' + fieldId).val() != '')
                            ShowHideWarningMessage('hdn' + fieldId, 'Please specify', false);
                        if ($('#ddl' + fieldId).val() != '')
                            ShowHideWarningMessage('ddl' + fieldId, 'Please specify', false);

                        if ($('#txtarea' + fieldId).val() == '')//for the text and text area
                        {
                            errorMessage += TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')) + ",\n";
                            ShowHideWarningMessage('txtarea' + fieldId, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                        }
                        else if ($('#txt' + fieldId).val() == '') {//for the text and text area
                            errorMessage += TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')) + ",\n";
                            ShowHideWarningMessage('txt' + fieldId, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                        }

                        else if ($('#rbY' + fieldId).length > 0) {//for the radio button

                            if ($('#rbY' + fieldId).attr('checked') != 'checked' && $('#rbN' + fieldId).attr('checked') != 'checked') {
                                errorMessage += TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')) + ",\n";
                                //ShowHideWarningMessage('rbN' + fieldId, 'Please specify ' + document.getElementById('lbl' + fieldId).textContent.replace('*', '').trim(), true);
                                var radioField = $('#rbN' + fieldId).next();
                                ShowHideWarningMessageForRadio(radioField, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                            }

                        }
                        else if ($('#hdn' + fieldId).val() == '') {//for the location

                            errorMessage += TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')) + ",\n";
                            ShowHideWarningMessage('hdn' + fieldId, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                        }
                        else if ($('#ddl' + fieldId).val() == '0') {
                            errorMessage += TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')) + ",\n";
                            ShowHideWarningMessage('ddl' + fieldId, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                        }
                       
                       
                    }
                   
                    //number validation
                    if ($('#txt' + fieldId)[0] != undefined) {                       
                       
                        var dataTypeValidation = '';

                        if ($('#txt' + fieldId)[0].dataset != undefined)
                        {
                            $('#txt' + fieldId)[0].dataset.entityDatatype;
                        }
                        
                        if (dataTypeValidation == 'Int')
                        {
                            var fieldIdSelector = '#txt' + fieldId;
                            var fieldLabel = $(fieldIdSelector).parent().prev().find(LABEL);
                            var labelText = $(fieldLabel).clone().children().remove().end().text();

                            var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');

                            var a = $('#txt' + fieldId)[0].value;

                            //allow only number(s)                         
                            var filter = /^\d+$/ ;

                            if (filter.test(a) || ($('#txt' + fieldId)[0].value == '' && !isRequiredField)) {
                              
                                ShowHideWarningMessage('txt' + fieldId, 'Please specify a valid number', false);
                                _isNumberValidTrue = true;
                               
                            }
                            else if ($('#txt' + fieldId)[0].value != '' && !filter.test(a)) {
                                ShowHideWarningMessage('txt' + fieldId, 'Please specify a valid number', true);
                                _isNumberValidTrue = false;

                            }else{
                                ShowHideWarningMessage('txt' + fieldId, 'Please specify ' + TrimString(document.getElementById('lbl' + fieldId).textContent.replace('*', '')), true);
                                _isNumberValidTrue = false;
                               
                            }
                        }


                    }

                }
            });
        });
    }
   
    if (errorMessage.length > 0) {        
        
        errorMessage = errorMessage.substring(0, errorMessage.lastIndexOf(','));       
        return errorMessage;
    }
    return true;
}



var vListId = '';

//Method to load treeview
function LoadTreeView(fieldId, listId) {

    _treeFieldId = fieldId;
    vListId = listId;

    var data = '&listId=' + listId;
    var serviceURL = _baseURL + 'Services/ListValues.svc/GetListValueName';

    Ajax(serviceURL, data, eval(GetListNameSucc), eval(GetListNameFail));
}

//Get list name
function GetListNameSucc(result) {

    result = result.d;

    $('#ErrCustomField').html('');
    $('#ErrCustomField').removeClass('WarningMessage');
    $('#ErrCustomField').removeClass('SuccessMessage');

    //detroy the dialog
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCustomSelectOneTreeview").dialog("destroy");
    $('#UsrFieldTreeviewList').jstree('destroy').empty();

    $('#btnCustomTreeCancel').hide();
    $('#btnUsrFieldTreeOk').hide();
    //add the title
    $("#divCustomSelectOneTreeview").attr("title", result);

      var loadTreeviewData = GetLoadTreeViewDetail(vListId, "0");

    _data = JSON.stringify(loadTreeviewData);

    var serviceURL = _baseURL + 'Services/ListValues.svc/BuildJSTreeView';
    AjaxPost(serviceURL, _data, eval(LoadTreeView_Succ), eval(LoadTreeView_Fail));

}

//open select one node from Treeview popup dialog
function OpenSelectOneFromTreeview()
{
    $('#btnCustomTreeCancel').show();
    $('#btnUsrFieldTreeOk').show();
    $("#divCustomSelectOneTreeview").dialog({
        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
}

//On fail of GetListName method
function GetListNameFail() {
}

//Get load tree view details
function GetLoadTreeViewDetail(listId, fullPath) {

    var loadTreeViewDetail = {};
    loadTreeViewDetail.listId = vListId;
    loadTreeViewDetail.fullPath = fullPath;
    return loadTreeViewDetail;
}

//On success of loadTreeViewDetail 
function LoadTreeView_Succ(result) {

    if (result.d != null && result.d != "") {

        var jsonResponse = jQuery.parseJSON(result.d);
        DispSystemTreeNodes(jsonResponse);

        //open select one node from Treeview popup dialog
        OpenSelectOneFromTreeview();

    }
}

//On fail of loadTreeViewDetail 
function LoadTreeView_Fail() {
}

//Display system tree node
function DispSystemTreeNodes(resjson) {

    if (_treeFieldId != '') {

        $('#UsrFieldTreeviewList').jstree({

            "json_data": { "data": resjson },
            rules: { multiple: "ctrl" },
            checkbox: {
                two_state: true
            },
            "themes": {
                "theme": "default",
                "dots": false,
                "icons": true
            },
            plugins: ["themes", "json_data", "dnd", "checkbox", "crrm", "ui", "types"]

        });
    }

    ExpandTreeNode();

    $("#UsrFieldTreeviewList").bind("change_state.jstree", function (e, data) {

        if (data.args[1] == false) {
            var vSelectedNodeId = $(data.rslt).attr('id');
        }

        var vChecked = $("#UsrFieldTreeviewList").jstree("get_checked", null, true);
        $(vChecked).each(function (i, node) {
            var vCheckedId = $(node).attr("id");
            if (vSelectedNodeId != vCheckedId) {
                $(this).removeClass('jstree-checked').addClass('jstree-unchecked');

            }
        });

    });

    setTimeout(function () { CheckCSTParentNode(); }, 0);
}

//For opening of all nodes
function ExpandTreeNode() {

    $('#UsrFieldTreeviewList').bind("loaded.jstree", function (event, data) {

        $('#UsrFieldTreeviewList').jstree("open_all");

        var fieldValue = $('#hdn' + _treeFieldId).val();

        //checked the selcted item
        $('#' + fieldValue).each(function () {
            //$(this).removeClass('jstree-unchecked').addClass('jstree-checked');
            $('#UsrFieldTreeviewList').find('#' + fieldValue).removeClass('jstree-unchecked').addClass('jstree-checked');
        });

    });
}

//Populate system list data
function PopulateSystemlistData() {

    var selectedNodePath = '';
    _sysCheckedNodes = [];   
    _systemId = '';    

    $("#UsrFieldTreeviewList").jstree("get_checked", false, false).each(function (index) {
        if (!$(this).hasClass('not-default')) {
            _sysCheckedNodes.push(this.id);
            _systemId = this.id;

            selectedNodePath = $(this).attr("title");
        }
    });

    var fieldLabel = $('#lblCust' + _treeFieldId).parent().prev().find(LABEL);
    var labelText = $(fieldLabel).clone().children().remove().end().text();
    var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');
    var isDetailsPage = ($('#body_txtEventName').length > 0);

    if (_sysCheckedNodes.length == 0 && isRequiredField && isDetailsPage) {
    //if (_sysCheckedNodes.length == 0) {

        NotificationMessage('Please check item to select.', 'jError', true, 3000);
        return false;
    }   
    else {
        var oldSelectedVal = $('#lblCust' + _treeFieldId).html();

        $('#lblCust' + _treeFieldId).html(selectedNodePath);

        //Add generic save code and track the changes
        if (_systemId.lastIndexOf(',') != -1)
            _systemId = _systemId.substring(0, _systemId.lastIndexOf(','));

        var customLabelId = 'lblCust' + _treeFieldId;

        //for Details tab - if new/create then undo should not show-deepa
        if (_subTabName != _detailsTab || _currentEventId != 'New') {
            GenericSaveFields(_fields, customLabelId, _systemId);
        }

        //For pre checking the check marks in custom list tree.
        $('#hdn' + _treeFieldId).val(_systemId);
        _allcustomtreeviewIds.push(_treeFieldId);

        _fields[customLabelId].previousValue.push($('#' + customLabelId).html());       

        _fields[customLabelId].previousListId.push(_systemId);
        
        var field = {};
        field.fieldName = customLabelId;
        field[customLabelId] = {};
        field[customLabelId] = _fields[customLabelId];

        //for Details tab - if new/create then undo should not show-deepa
        if (_subTabName != _detailsTab || _currentEventId != 'New') {
           
           
            if (oldSelectedVal.replace(/&gt;/g, '>') != selectedNodePath) {

                //Pass the field info into the stack
                _undoStack.push(field);
                $('#ancUndo').show();
            }
            //Check for the modified values to show the UNDOALL link
            if (_undoStack.length > 1)
                $('#ancUndoAll').show();
        }

        CloseCustomFieldPopup();
       
    }
}

//To Get the FormattedFullpath
function GetFormattedSysFullpath(systemId, eventId, systemFieldId) {

    var systemData = GetSystemDetail(systemId, eventId, systemFieldId);
    _data = JSON.stringify(systemData);
    var serviceURL = _baseURL + 'Services/ListValues.svc/SelectLocationPath';
    AjaxPost(serviceURL, _data, eval(GetFormattedSysFullpathSucc), eval(GetFormattedSysFullpathFail));

}

//Get system detail
function GetSystemDetail(systemId, eventId, systemFieldId) {

    var classificationDetail = {};
    classificationDetail.systemListIds = systemId;
    classificationDetail.eventId = eventId;
    classificationDetail.systemFieldId = systemFieldId;

    return classificationDetail;
}

//On success of getting formatted system full path 
function GetFormattedSysFullpathSucc(result) {

    $('#lblCust' + _treeFieldId).html('');
    $('#hdn' + _treeFieldId).val('');

    var path = '';

    if (result.d != null) {

        var jsonResult = jQuery.parseJSON(result.d);
        path = path + jsonResult[0];

        $('#lblCust' + _treeFieldId).html(path);
        $('#hdn' + _treeFieldId).val(_systemData);
        _allcustomtreeviewIds.push(_treeFieldId);
    }

    _systemId = '';
}

//On fail of getting formatted system full path 
function GetFormattedSysFullpathFail() {
    alert('Service failed.')
}

//Text area max length
function TextAreaMaxlength(fieldId, maxLen) {
   
    var controlVal = $('#txtarea' + fieldId).val();
    if (controlVal > maxLen) {
        controlVal = controlVal.substring(0, maxLen);
    }

    $('#txtarea' + fieldId).val(controlVal);
}


/*multi selection tree view*/

//display the multi selection tree view
function OpenMultiSelectionTreeView(fieldId, listId) {

    _listId = listId;
    _treeFieldId = fieldId;

    var data = '&listId=' + listId;
    var serviceURL = _baseURL + 'Services/ListValues.svc/GetListValueName';
    Ajax(serviceURL, data, eval(GetMultiSelectionListNameSucc), eval(GetMultiSelectionListNameFail));

}
//On success of Get list name
function GetMultiSelectionListNameSucc(result) {   

    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCustomMultipleSelectTreeview").dialog("destroy");
    $('#EventClassificationTreeListTest').jstree('destroy').empty();    
    
    //add the title
    $("#divCustomMultipleSelectTreeview").attr("title", result.d);

    var loadTreeviewData = GetClassTreeviewDetail(_listId, "0");
    _data = JSON.stringify(loadTreeviewData);

    var serviceURL = _baseURL + 'Services/ListValues.svc/BuildJSTreeView';
    AjaxPost(serviceURL, _data, eval(LoadClassificationTreeSucc), eval(LoadClassificationTreeFail));

}

//Open Multiple selection tree view popup dialog
function OpenCustomMultipleSelectTreeview()
{
    $("#divCustomMultipleSelectTreeview").dialog({
      
        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
}

//On fail of Get list name
function GetMultiSelectionListNameFail() {
    alert('Service Failed.');
}

//Get class tree view detail
function GetClassTreeviewDetail(listId, fullPath) {

    var treeviewdetail = {};
    treeviewdetail.listId = listId;
    treeviewdetail.fullPath = fullPath;
    return treeviewdetail;
}

//On success of load classification tree
function LoadClassificationTreeSucc(result) {
    var jsonData = result.d;
    var jsonResult = jQuery.parseJSON(jsonData);
    DisplayClassificationTree(jsonResult);

    //Open Multiple selection tree view popup dialog
    OpenCustomMultipleSelectTreeview();
}

//On fail of Load classifictaion
function LoadClassificationTreeFail() {
    alert('Service Failed.');
}

//Display classification tree
function DisplayClassificationTree(resJson) {

    $("#EventClassificationTreeListTest").jstree({
        "json_data": {
            "data": resJson
        },
        checkbox: {
            "real_checkboxes": true,
            "real_checkboxes_names": function (n) {
                return [n[0].id, 1];
            },
            "two_state": true
        },
        "themes": {
            "theme": "default",
            "dots": false,
            "icons": true
        },
        plugins: ["themes", "json_data", "checkbox", "ui"]
    });

    //Expand classification tree node
    ExpandClassificationTreeNode();

    //checkbox select and de select from treeview

    $('#EventClassificationTreeListTest').bind("change_state.jstree", function (e, data) {
        if (data.args[1] == false) {
            var selectedId = $(data.rslt).attr('id');
        }

        //var selectedIdArray = $('#EventClassificationTreeListTest').jstree("get_checked", null, true);

        //$(selectedIdArray).each(function (i, node) {

        //    var treeviewItemId = $(node).attr("id");
        //    if (selectedId != treeviewItemId) {
        //        $(this).removeClass('jstree-checked').addClass('jstree-unchecked').removeClass('not-default');
        //    }
        //});
    });


    setTimeout(function () { CheckCSTParentNode(); }, 0);
}

//For opening of all nodes
function ExpandClassificationTreeNode() {

    $('#EventClassificationTreeListTest').bind("loaded.jstree", function (event, data) {

        $('#EventClassificationTreeListTest').jstree("open_all");
        var fieldValue = $('#hdn' + _treeFieldId).val().split(',');
        //Auto check mark.
        for (i = 0; i <= fieldValue.length; i++) {
            $('#' + fieldValue[i]).each(function () {
                //$(this).removeClass('jstree-unchecked').addClass('jstree-checked');
                $('#EventClassificationTreeListTest').find('#' + fieldValue[i]).removeClass('jstree-unchecked').addClass('jstree-checked');
            });
        }

    });

}



function CheckCSTParentNode() {
    var checkedItems = $('.jstree-checked');
    // Loop through the items and check the parent nodes if any child node (of any level) is checked by user
    $.each(checkedItems, function (index, item) {
        CheckCSTParentByCode(item);
    });

    // Bind a click event for the checkboxes in classification
    $('.jstree').bind('change_state.jstree', function (e, data) {
        // if data.args[1] is true, then node is unchecked. If false, then node is checked
        if (!data.args[1]) {            
            CheckCSTParentByCode(data.args[0]);
        }
        else {
            if (!$(data.args[0]).closest('li').siblings().hasClass('jstree-checked'))
                UnCheckCSTParentByCode(data.args[0]);

            // Uncheck the child items
            UncheckChildItems(data.args[0]);
        }
    });
}


// Find the parent node of object - obj and check the node
function CheckCSTParentByCode(item) {
    if ($(item).length > 0) {
        if ($(item).hasClass('jstree-unchecked'))
            $(item).removeClass('jstree-unchecked');

        if (!$(item).hasClass('jstree-checked')) {
            $(item).addClass('jstree-checked');
            $(item).addClass('not-default');
        }

        CheckParentByCode(item);
    }
}

// Find the parent node of object - obj and uncheck the node
function UnCheckCSTParentByCode(item) {
    if ($(item).length > 0) {        
        if ($(item).hasClass('jstree-checked') && $(item).hasClass('not-default'))
            $(item).removeClass('jstree-checked');

        if (!$(item).hasClass('jstree-unchecked') && $(item).hasClass('not-default')) {
            $(item).addClass('jstree-unchecked');
        }

        $(item).removeClass('not-default');
        UnCheckParentByCode(item);
    }
}

// Find the parent node of object - obj and check the node
function CheckParentByCode(obj) {
    var x = $(obj).closest('ul').closest('li');

    if ($(x).length > 0) {
        if ($(x).hasClass('jstree-unchecked'))
            $(x).removeClass('jstree-unchecked');

        if (!$(x).hasClass('jstree-checked')) {
            $(x).addClass('jstree-checked');
            $(x).addClass('not-default');
        }

        CheckParentByCode(x);
    }
}

// Find the parent node of object - obj and uncheck the node
function UnCheckParentByCode(obj) {
    var x = $(obj).closest('ul').closest('li');

    if ($(x).length > 0) {
        if ($(x).hasClass('jstree-checked') && $(x).hasClass('not-default'))
            $(x).removeClass('jstree-checked');

        if (!$(x).hasClass('jstree-unchecked') && $(x).hasClass('not-default')) {
            $(x).addClass('jstree-unchecked');
        }

        $(x).removeClass('not-default');

        UnCheckParentByCode(x);
    }
}

// Find the child items and uncheck the nodes
function UncheckChildItems(item) {
    if ($(item).length > 0) {
        if (!$(item).hasClass('jstree-unchecked') && $(item).hasClass('not-default')) {
            $(item).addClass('jstree-unchecked');
        }

        $(item).removeClass('not-default');
        
        // Use the Anchor tag that contains the uncheckbox and get the next ul sibling 
        var itemAnchor = $(item).closest('a').siblings('ul');
        // Find the li's and remove the classes [jstree-checked, not-default] and add the class [jstree-unchecked]
        $(itemAnchor).find('li').removeClass('jstree-checked').addClass('jstree-unchecked').removeClass('not-default');
    }
}


function CustomListDisplay() {
   // $('#lblEventClassificationData').html('');-- commented by deepa to test details tab functionality
    var oldSelectedVal = $('#lblCust' + _treeFieldId).html();

    _classificationCheckedNodes = [];
    _classificationCheckedNames = [];

    var selectedNodePath = '';
    _classificationId = '';
   
    $("#EventClassificationTreeListTest").jstree("get_checked", null, true).each(function () {
        
        if (_classificationId != null && _classificationId.length > 0 && _classificationId.indexOf(',') > -1) {
            var splClsIds = _classificationId.split(',');
            for (var i = 0; i < splClsIds.length; i++) {
                if (splClsIds[i] == this.id)
                    return;
            }
        }

        if (!$(this).hasClass('not-default')) {
            _classificationCheckedNodes.push(this.id);
            if ($(this).attr("title") != undefined) {
                _classificationId = _classificationId + this.id + ',';
                // _classificationCheckedNames.push($(this).children('a').text().trim());
                if (selectedNodePath == '') {
                    selectedNodePath = selectedNodePath + $(this).attr("title");
                }
                else {
                    selectedNodePath = selectedNodePath + "<br/>" + $(this).attr("title");
                }
            }
        }
    });

    //if (_classificationCheckedNodes.length == 0) {

    //    NotificationMessage('Please check item to select.', 'jError', true, 3000);
    //    return false;
    //}
    
        var fieldLabel = $('#lblCust' + _treeFieldId).parent().prev().find(LABEL);
        var labelText = $(fieldLabel).clone().children().remove().end().text();
        var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');
        var isDetailsPage = ($('#body_txtEventName').length > 0);

        if (_classificationCheckedNodes.length == 0 && isRequiredField && isDetailsPage) {

            NotificationMessage('Please check item to select.', 'jError', true, 3000);
            return false;
       
        }
        else
        {
        
        $('#lblCust' + _treeFieldId).html(selectedNodePath);
        //Add generic save code and track the changes
        if (_classificationId.lastIndexOf(',') != -1)
        _classificationId = _classificationId.substring(0, _classificationId.lastIndexOf(','));

        var classificationValue = 'lblCust' + _treeFieldId;
       
        //for Details tab - if new/create then undo should not show-deepa
            if (_subTabName != _detailsTab || _currentEventId != 'New') {
                GenericSaveFields(_fields, classificationValue, _classificationId);
            }
            //For pre checking the check marks in custom list tree.
            $('#hdn' + _treeFieldId).val(_classificationId);
            _allcustomtreeviewIds.push(_treeFieldId);

            ShowHideWarningMessage('hdn' + _treeFieldId, 'Please specify', false);

            _fields[classificationValue].previousValue.push($('#' + classificationValue).html());
            _fields[classificationValue].previousListId.push(_classificationId);

            var field = {};
            field.fieldName = classificationValue;
            field[classificationValue] = {};
            field[classificationValue] = _fields[classificationValue];

            //for Details tab - if new/create then undo should not show-deepa
            if (_subTabName != _detailsTab || _currentEventId != 'New') {
               
                //Pass the field info into the stack
                if (oldSelectedVal.replace(/&gt;/g, '>') != selectedNodePath) {

                    _undoStack.push(field);
                    $('#ancUndo').show();
                }

                //Check for the modified values to show the UNDOALL link
                if (_undoStack.length > 1)
                    $('#ancUndoAll').show();

            }
      
        CloseClassificationPopup();
    }

}

// Close classification pop up
function CloseClassificationPopup() {

    $('#divCustomMultipleSelectTreeview').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCustomMultipleSelectTreeview").dialog("destroy");
    $('#divCustomMultipleSelectTreeview').bind("dialogclose", function (event, ui) {

    });
}