﻿

/* Implement Auto save functionality on custom fields */

//Global variables
var _fields = new Object();
var _eventId = '';
var _undoStack = new Array;
var _eventType = '';
var _initialData = '';
var _userId = -1;
var _companyId = -1;

$(document).ready(function () {
   
    PageLoadAction();

    JqueryTabs();// ISSUERELATEDTOIE

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });

    _companyId = GetCompanyId();
    _userId = GetUserId();

    GetTMAccessWithEventId(_eventId, 'divEventDetails');
});



function PageLoadAction() {
  
    $('#ancUndo').hide();
    $('#ancUndoAll').hide();

    var customElements = $('.trackCustomChanges');

    GetParameterFromUrl();
  
    //Got the event id from the Code behind via aspx page.
    _eventId = _ucEventId;
    
    GetInitialValues(customElements);

    //in Details Tab 1st time auto save is executing which is not required- 1st time we are saving with Create button
    if (_eventId != 0) {
        //  Auto save the fields data on edit mode
        var inputControls = ':input';
        OnBlurCheck(inputControls);

        //On selecting the custom fields radio button.
        RadioButtonSelect();
    }
}

//Numeric validation
function NumericValidation(evt, Fldid) {
   
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        //debugger;
        var errorMessage = 'Enter only numbers';
        var errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 20000);
        var trimString = document.getElementById(Fldid).value;
        trimString += String(evt.char);
        trimString = trimString.replace(/[^\d]/g, '');
        document.getElementById(Fldid).value = trimString;
        return false;
    }
    else
        return true;
}

//On selecting the custom fields radio button. 
function RadioButtonSelect() {

    var radioButtonSelectors = ".trackRadioButtonChanges";

    $(radioButtonSelectors).change(function (e) {     
        SaveFields(e);
    });
    
}
var _detailsTab = 'Details';
//to avoid on click of Details tab it should not fire TabColor
function GetParameterFromUrl() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');

    var subTab = GetDetailsSubTabDetails(splitUrl[splitUrl.length - 3], 'name');
  
    //if not Details tab- in Events page already given TabColor so it should avoid in Custom Page
    if (subTab != 'Details')
      TabColor(location, 2);
}

