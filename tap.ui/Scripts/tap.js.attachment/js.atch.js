﻿/* User side Attachments to attach the documents.*/

//Global Variables
var _nodeId = '';
var _selectedNoderelation = '';
var _parentOfSelectedNode = '';
var _eventId = '';
_userAttachCheckedNodes = new Array;
var _selectedNodeName = '';
var _folderId = 0;
var _deleteFolderName = '';
_attachmentTreeId = 'UserAttachmentTreeview';
var _snapCaps = 'SnapCaps';

//Page load methods.
$(document).ready(function () {

    GetQueryStringFromUrl();

    SetPageLoad();

    //Upload button click
    FileUploadDisplay();

    //For expanding the UserAttachment treeview on load
    ExpandAttachTreeLoad();

    //For closing the File upload Pop-in and refreshing the tree
    UserAttachTreeRefresh();

    //For Deleting the documents of UserAttachtree
    UserAttachDelete();

    //for color Change in Subtab
    TabColor(location, 2);

    //Jquery Main Tabs
    JqueryTabs();

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });

}); //End of ready method

function SetPageLoad() {

    //hide the popup page
    $('.backgroundPopup').hide();
    $("#uploadAttachmentModal").hide();

    $('#fileUpload').hide();

    //To hide the old menu for displaying the UC
    HideMenu();

    //Hide the button, Delete File .
    DeleteFileButtonHide();

    //Display UserAttachment treeview
    UserAttachFolderTreeviewLoad();

    GetTMAccessWithEventId(_eventId, 'divAttachmentTab');

    $('#btnUserUpload').hide();
}

function HideMenu() {

    $('#menu').hide();
    $('#AdminConfigMenu').hide();
}

function UploadFile() {
   
    var selectedNodeID = $.trim(_userAttachCheckedNodes[0]);

    //avoid uploading files to SnapCaps folder
    if ($('#' + selectedNodeID).find('a')[0].textContent.trim() != _snapCaps) {
        //logic to get the folder id based on user selection
        //User can select any folder or document to upload a file but need to get only the folder id.
        if (_selectedNoderelation != '' && _selectedNoderelation != 'document') {
            _folderId = (_selectedNoderelation == 'document') ? _parentOfSelectedNode : selectedNodeID;

            //Modify the URL for adding the Folder id.
            UrlOfSelectedFolder();

            //Show the modal pop up to upload a file.
            AttachmentModalShow();
        }
        else {
            //Display the notification       
            var alertMsg = 'Please select the folder you wish to upload this attachment to.';
            //var msgType = 'jNotify';
            var msgType = 'jError';
            NotificationMessage(alertMsg, msgType, true, 3000);
        }
    }
}

//Modify the URL for adding the Folder id.
function UrlOfSelectedFolder() {

    //var url = "/Tools/UploadFile/EventId/Folder-{FolderId}";// $("#uploadAttachment").attr("src");
    var url = $("#uploadAttachment").attr("src");

    if (url.indexOf("EventId") > 0) {
        url = url.substring(0, url.indexOf("EventId"));
    }
    url = url + _eventId;

    if (url.indexOf("/Folder-") > 0) {
        url = url.substring(0, url.indexOf("Folder-"));
    }

    //Add folder id to the URL 
    url = url + "/Folder-" + _folderId;

    //Passed the folder id as querystring to the url.
    $("#uploadAttachment").attr("src", url);

}

//Show the modal pop up to upload a file.
function AttachmentModalShow() {

    $("#uploadAttachmentModal").show();
    $("#uploadAttachmentModal").dialog("destroy");

    $("#uploadAttachmentModal").dialog({
        height: 320,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

}

//Expand the tree view soon after binding.
function ExpandAttachTreeLoad() {
    $('#' + _attachmentTreeId).bind("loaded.jstree", function (event, data) {
        $('#' + _attachmentTreeId).jstree("open_all");
    });
}

//To display the upload modal to upload any documents.
function FileUploadDisplay() {
    $('#btnUserUpload').click(function () {
        UploadFile();
        return false;
    });
}

//Refreshing the treeview soon after closing the Upload modal pop up.
function UserAttachTreeRefresh() {
    $('#btnAttachClose').click(function () {
        _selectedNoderelation = '';
        //Hide the Upload Modal pop up.
        $('#uploadAttachmentModal').hide();
        $("#dialog:ui-dialog").dialog("destroy");
        $("#uploadAttachmentModal").dialog("destroy");
        $('#uploadAttachmentModal').bind("dialogclose", function (event, ui) { });

        //Refresh the tree.
        UserAttachFolderTreeviewLoad();
        return false;
    });
}

//To delete the selected document in the treeview.
function UserAttachDelete() {
    $('#btnUserDelete').click(
    function () {
        if (_selectedNoderelation != "" && _selectedNoderelation == 'document' && _selectedNodeName != '') {
            if (UserAttachConfirmDeleteNodes()) {
                UserAttachDeleteTreeNodes(_userAttachCheckedNodes);
                //_selectedNodeName = '';
            }
        }
        else {
            var alertMsg = 'Please select any document to delete.';
            var msgType = 'jNotify';
            NotificationMessage(alertMsg, msgType, true, 3000);
        }
    });
}

//Initial call to build the Attachment tree view.
function UserAttachFolderTreeviewLoad() {

    var data = '&eventId=' + _eventId + '&companyId=' + _companyId;
    serviceUrl = _baseURL + 'Services/ListValues.svc/BuildAttachmentTree';
    Ajax(serviceUrl, data, eval(TreeLoadSuccess), eval(TreeLoadFail), 'Attachments');
}
function TreeLoadSuccess(result) {
    if (result != null && result != "") {
        var resJson = jQuery.parseJSON(result.d);

        //For displaying the treeview for Attachments in Userside    
        UserDisplayAttachTreeNodes(_attachmentTreeId, resJson);
    }
}
function TreeLoadFail(result) { }

//For displaying the treeview for Attachments in Userside
function UserDisplayAttachTreeNodes(treeViewId, resJson) {

    //Hide the button, Delete File .
    DeleteFileButtonHide();

    $("#" + treeViewId).jstree({
        "json_data": { "data": resJson },
        rules: { multiple: "ctrl" },

        "themes": {
            "dots": false,
            "icons": true
        },

        "types": {
            "type_attr": "extn",
            "types": {
                ".png": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/image.png" }
                },
                ".gif": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/image.png" }
                },
                ".jpg": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/image.png" }
                },
                ".docx": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/word.png" }
                },
                ".doc": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/word.png" }
                },
                ".xlsx": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/excel.png" }
                },
                ".xls": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/excel.png" }
                },
                ".pptx": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/ppt.png" }
                },
                ".ppt": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/ppt.png" }
                },
                ".pdf": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/pdf.png" }
                },
                ".txt": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/txt.png" }
                },
                ".bat": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/bat.png" }
                },
                ".xml": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/xml.png" }
                },
                ".zip": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/zip.png" }
                },
                "other": {
                    "valid_children": ["none"],
                    "icon": { "image": "../../../tap.ui.css/tap.stl.jstree/images/other.png" }
                }
            }
        },
        "ui": {
            //"initially_select": ["1"]
        },
        "crrm": {
            "move": {
                "check_move": function (data) {
                    if (data.o.attr('rel') == 'document') {
                        return true;
                    }
                }
            }
        },
        plugins: ["themes", "json_data", "crrm", "ui", "types", "dnd"]
    });

    //For Expanding all the nodes of the treeview for Attachments in Userside
    $('#' + _attachmentTreeId).bind("loaded.jstree", function (event, data) {
        $('#' + _attachmentTreeId).jstree("open_all");

        //Hide the Root folder.
        $(".jstree-last .jstree-icon").first().hide();
        $("#UserAttachmentTreeview a:contains('Attachments')").css("visibility", "hidden");
        var sub_node = $("#UserAttachmentTreeview a:contains('Attachments')").next('ul').children('li').attr('id');
      
        if (sub_node == null || sub_node == 'undefined') {
           
            $('#btnUserUpload').hide();
         
        }
    });

    $('#' + _attachmentTreeId).bind('open_node.jstree', function (e, data) {

        var parentObj = data.rslt.obj;
        $(data.rslt.obj).find("li").each(function (idx, listItem) {

            var child = $(listItem);
            var docId = $(this).attr('id');

            if ($(child).attr('rel') == 'document') {
               
                var nextIcons = $(child).find('a').find('img');
                nextIcons.remove();

                var imageSrc = _baseURL + 'Images/download1.jpg';
                var delimgSrc = _baseURL + 'Images/Delete.png';
                var parentId = $(child).parent().closest('li').attr('id');

                $(child).find('a').append('<img src="' + imageSrc + '" title="Download" style="margin-left:10px; height:13px;" onclick="DownloadIconClk(' + docId + ')" />');
                $(child).find('a').append('<img src="' + delimgSrc + '" title="Delete" style="margin-left:5px; height:13px;" onclick="DeleteFileIconClk(' + docId + ',' + parentId + ')" />');
            }

            if ($(child).attr('rel') == 'folder') {

                var nextIcons = $(child).find('a').find('img');
                nextIcons.remove();
              
                if ($(child).find('a')[0].textContent.trim() != _snapCaps)//avoid upload the SnapCaps folder
                {
                    var imageSrc = _baseURL + 'Images/upload1.gif';
                    $(child).find('a').append('<img src="' + imageSrc + '" title="Upload" class="FileUpload" style="margin-left:10px; height:13px;" />');
                }
            }
        });
    });

    //For getting both the id and rel attribute of the selected node.
    $('#' + _attachmentTreeId).bind("select_node.jstree",
        function (e, data) {

            DeleteFileButtonHide();

            //To get the id of the selected node
            _nodeId = data.rslt.obj[0].id;
            _folderId = _nodeId;

            //Clear the Array.
            _userAttachCheckedNodes = [];

            _userAttachCheckedNodes.push(_nodeId);
            if (data.rslt.e != undefined) {
                //To get the id of the selected node name 
                _selectedNodeName = data.rslt.e.target.textContent.trim();
            }

            //To get the rel of the selected node
            _selectedNoderelation = $('#' + _attachmentTreeId).jstree('get_selected').attr("rel");

            // To get the Parent node of the selected node
            if (_selectedNoderelation != "root")
                _parentOfSelectedNode = $.trim(data.inst._get_parent(data.rslt.obj).attr("id"));
          
            //Show the Delete File button.
            if (_selectedNoderelation == 'folder')
                UploadFile();
        });

    //For Drag And Drop the attachments
    $('#' + _attachmentTreeId).bind('move_node.jstree', function (event, data) {

        var AttachmentID = data.rslt.o[0].id;
        var ParentNodeID = data.rslt.np[0].id;
        var ReferenceNodeID = data.rslt.r[0].id;

        if (ParentNodeID == _attachmentTreeId)
            ParentNodeID = ReferenceNodeID;

        AttachmentsMove(AttachmentID, ParentNodeID);
    });

    //On double click of any node in tree view 
    //get the respective id, validate and call for the download.
    $('#' + _attachmentTreeId).bind("dblclick.jstree", function (event, data) {

        var doubleClickedNode = $.jstree._focused().get_selected();

        if ($(doubleClickedNode).attr("rel") == "document")
            CallDownloadPage($(doubleClickedNode).attr("id"));
    });
    $('#' + _attachmentTreeId).bind("click.jstree", function (event, data) {
        var doubleClickedNode = $.jstree._focused().get_selected();
        if ($(doubleClickedNode).attr("rel") == "document" && $(event.target).text() != null && $(event.target).text().length>0) {
            OpenFileSelected($(doubleClickedNode).attr("id"));

            UserAttachFolderTreeviewLoad();
            return false;
        }
    });
    
}

//On Single click,  open the file in new window.
function OpenFileSelected(id) {
    var url = _baseURL + 'tap.usr/OpenFile.aspx?id=' + id;
    //var winFea = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=800,height=1000';
    var winFea = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes';
    window.open(url, 'FileOpenWindow', winFea);
}


//Navigating to the DownloadAttachment.aspx for downloading the attachment files.
function CallDownloadPage(id) {
    var url = _baseURL + 'tap.usr/DownloadAttachment.aspx?id=' + id;
    window.location.href = url;
    window.location.replace(url);
}

//Delete icon click function
function DeleteFileIconClk(nodeId, parentId) {

    _selectedNoderelation = 'document';
    _userAttachCheckedNodes = [];
    _selectedNodeName = $('#' + parentId).find('#' + nodeId).find('a').text();
    _userAttachCheckedNodes.push(nodeId);

    if (_selectedNoderelation != "" && _selectedNoderelation == 'document') {

        if (UserAttachConfirmDeleteNodes())
            UserAttachDeleteTreeNodes(_userAttachCheckedNodes);
        else {
            UserAttachFolderTreeviewLoad();
            return false;
        }
    }
    else {

        var alertMsg = 'Please select any document to delete.';
        var msgType = 'jNotify';
        NotificationMessage(alertMsg, msgType, true, 3000);
    }

}
//Download icon click function
function DownloadIconClk(nodeId) {
    if (nodeId > 0)
        CallDownloadPage(nodeId);
}


//To display message if there is no sub folder.
function NoSubFolderExist() {
    var msgtype = 'jNotify';
    var alertMsg = 'Your System Administrator has not set up any attachment folders.';
    NotificationMessage(alertMsg, msgtype, true, 3000);
}

//Confirmation for the deletion of files
function UserAttachConfirmDeleteNodes() {
    _deleteFolderName = _selectedNodeName;
    return confirm('Are you sure want to delete' + _selectedNodeName + '?');
}

//To delete the attached files.
function UserAttachDeleteTreeNodes(arrCheckedNodes) {

    var data = JSON.stringify({ 'UserAttachmentCheckedNodes': _userAttachCheckedNodes });
    serviceUrl = _baseURL + 'Services/ListValues.svc/UserAttachTreeDelete';
    AjaxPost(serviceUrl, data, eval(UsrAtchDeleteTreeNodesSuccess), eval(UsrAtchDeleteTreeNodesFail));
}

function UsrAtchDeleteTreeNodesSuccess(msg) {

    //Display success notification.
    var msgtype = 'jSuccess';
    var alertMsg = _deleteFolderName + ' file deleted successfully';

    NotificationMessage(alertMsg, msgtype, true, 3000);
    _selectedNodeName = '';
    _deleteFolderName = '';

    //Reload the attachments tree
    UserAttachFolderTreeviewLoad();
}

function UsrAtchDeleteTreeNodesFail(msg) {
    var failMsg = 'File deletion have been failed';
    FailureMsg(failMsg);
}

function AttachMoveNodesInfo(AttachmentID, ParentNodeID) {
    this.attachmentNodeId = AttachmentID;
    this.attachmentParentNode = ParentNodeID;
}

//To Update the attachments, Parent Id after drag and drop
function AttachmentsMove(AttachmentID, ParentNodeID) {
    var atchMoveInfo = new AttachMoveNodesInfo(AttachmentID, ParentNodeID);
    var data = JSON.stringify(atchMoveInfo);
    serviceUrl = _baseURL + 'Services/Attachments.svc/AttachmentsMove';
    AjaxPost(serviceUrl, data, eval(AttachmentsMoveSuccess), eval(AttachmentsMoveFail));
}

function AttachmentsMoveSuccess() {

}

function AttachmentsMoveFail() { }

//To get the EventId from the URL
function GetQueryStringFromUrl() {

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _eventId = splitUrl[splitUrl.length - 1];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }
}

//Hide the button, Delete File .
function DeleteFileButtonHide() {
    $('#btnUserDelete').hide();
}

//Show the button, Delete File on selecting a file.
function DeleteFileButtonShow() {
    $('#btnUserDelete').show();
}

