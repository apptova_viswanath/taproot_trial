﻿
//Globa Variables
_imageSize = '';

$(document).ready(function () {
    
    //this code will be executed when a new file is selected
    $('#fileImage').bind('change', function () {

        //converts the file size from bytes to MB
        var fileSize = this.files[0].size;

        //gets the full file name including the extension
        var fileName = this.files[0].name;

        //checks whether the file less than 1 GB
        if (fileSize > 30000000) {
            $('#btnImageAttach').attr("disabled", "disabled");
            _message = 'There is a 30 MB attachment size limit.'
            _messageType = 'jSuccess';
            parent.NotificationMessage(_message, _messageType, true, 1000);

        }
        else {          
            $('#btnImageAttach').removeAttr("disabled");
        }
    });
});
//set Report Id to get in Parent Page
function SetReportId(reportId, elementId) {
    
    if (reportId != "") {
        //once image is uploaded get the Report Id and assign to global variable  
        parent._reportId = reportId;

        var divTag = parent.$('#' + parent._imageId);
        var imageTag = new Image();
        imageTag.id = 'img' + parent._imageId;
        imageTag.setAttribute("runat", "server");
        divTag[0].setAttribute('data-elementid', elementId);

        divTag[0].appendChild(imageTag);

        imageTag.onload = null;
        imageTag.src = _baseURL + 'tap.snap.hand/LoadImage.ashx?elementId=img' + elementId;

    }
    var id = parent.$('#report-image-madal');
    id.bind("dialogclose", function (event, ui) {
    });

    self.close();
    parent.$('#report-image-madal').hide();
    parent.$("#dialog:ui-dialog").dialog("destroy");
    parent.$("#report-image-madal").dialog("destroy");

}



//validation for image types
function ValidateImageTypes()
{
  
    _imageSize = parent.$('#hdnImageAccordianSize').val();

    $('#hdnImageSize').val(_imageSize);

    document.getElementById('lblErrorMessage').innerHTML = "";
    
    var errorMessage = '';
    var errorMessageType = 'jError';

    var imageFilePath = document.getElementById('fileImage').value.split('.');
    imageType = imageFilePath[imageFilePath.length - 1].toLowerCase();

    if (imageType != "jpg" && imageType != "png" && imageType != "gif" && imageType != "bmp" && imageType != "tif") {

        document.getElementById('lblErrorMessage').innerHTML = "Upload .jpg / .gif /.png /.bmp and .tif files.";
        document.getElementById('lblErrorMessage').className='WarningMessage';    
        return false;
    }

    return true;
}