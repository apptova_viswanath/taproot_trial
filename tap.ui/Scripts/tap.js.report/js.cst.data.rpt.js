﻿

//ready function
$(document).ready(function () {

    //page load method
    PageLoadAction();

    //display Report Data
    DisplayReportData();

});


//Global Variables`
var _eventId = '';
var _reportId = '';
var _line = 'Line';
var _image = 'Image';
var _isDelete = '';
var _fileLength = 0;
var _arrFileNames;
var _message = '';
var _messageType = '';
var _reportType = '';
var _lineBreak = 'Line Break';
var _pageBreak = 'Page Break';

//page load method
function PageLoadAction() {

    //get the Query String's from url
    GetQueryStringsFromUrl();

    if (_reportType != '1')
        $('#btnPrintPdf').hide();

}

//get Event ID from url
function GetQueryStringsFromUrl() {
   
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _isDelete = splitUrl[splitUrl.length - 1];
    _reportType = splitUrl[splitUrl.length - 2];
    _reportId = splitUrl[splitUrl.length - 3];
    _eventId = splitUrl[splitUrl.length - 4];
}


//generate Report method
function DisplayReportData() {
   
    var reportData = new ReportDataDetails(_eventId, _reportId);
    var data = JSON.stringify(reportData);
    _serviceUrl = _baseURL + 'Services/CustomReport.svc/GenerateReport';
    AjaxPost(_serviceUrl, data, DisplayReportDataSuccess, DisplayReportDataFail);
}

function ReportDataDetails(eventId, reportId) {
   
    this.eventID = eventId;
    this.reportId = reportId;
}

var _tapRooTIntegrate = "Integrate";
var _tapRooTIntegrateType = "";
var _arrIntegrateTypes = [];
//Generate the Report values
function DisplayReportDataSuccess(result) {
    
    var reportCount = 0;
    var reportData = '';
    _arrFileNames = [];
    var divValues = document.getElementById('generateReport');
 
    var jsonData = jQuery.parseJSON(result.d);

    //if user/system report from Admin side then do not show any values
    if (jsonData[0].isAdminReport) {
        lblReportTemplateMsg.innerHTML = 'You are previewing a report template. No Data is available as no Event is associated with it.';
    }
   
    var divTagTitle = document.createElement('div');
    var divTagUser = document.createElement('div');
    var divTagLineBreak = '<br/><br/>';
    var divTagPageBreak = '-------------------------- page break ------------------------------';
    var lineField = 'border-bottom:1px solid #000;margin:10px;width:100%;';

    if (jsonData.length != 0) {
       
        //Report Logo 
        if (jsonData[0].ReportLogoName != undefined && jsonData[0].ReportLogoName != null) {
            $('#divDataImage').show();
            var imagePath = window.location.protocol + "//" + window.location.host + _baseURL + "temp/" + jsonData[0].ReportLogoName;
            $("#imgDisplayLogo").attr("src", imagePath);
        }

        $('#divCompanyInfo').className = '';

        if ($('#imgDisplayLogo')[0] != undefined) {

            $('#divCompanyInfo').className = 'divCompanyInfo';
            $('#hdnGetLogoUrlPath')[0].value = $('#imgDisplayLogo')[0].src;
        } else {

            $('#divCompanyInfo').className = 'divwithoutHeaderLogo';
        }
       
        //get the values length and apply styles dynamically
        for (var i = 0; i < jsonData.length; i++) {
            
            var divTag = document.createElement('div');
            var imageTag = new Image();

            divTag.className = 'reportValues';

            divTag.style.position = 'absolute';
            divTag.style.top = jsonData[i].TopPosition.toString();
            divTag.style.left = jsonData[i].LeftPosition.toString();

            divTag.setAttribute("style", jsonData[i].ElementStyle.toString());
           
            //give space between column name and value- 1 main div covered by 2 left and right div's
            var divMain = document.createElement('div');
            var divText = document.createElement('div');
            var divValue = document.createElement('div');
          
            elementText = jsonData[i].ElementText;
           
            if (elementText.trim() != "" &&  elementText != _tapRooTIntegrate) {

                var elementValue = jsonData[i].ElementValue.toString();
               
                //for classification getting more than one value-give line break
                if (elementValue.indexOf(',') != '-1') {
                    elementValue = elementValue.replace(/,/g, '');                  
                }
                if (elementValue == 0)
                    elementValue = "";
            }

           
            if (elementText == _tapRooTIntegrate) {
               
                _tapRooTIntegrateType = jsonData[i].IntegrateType;
                _arrIntegrateTypes.push(_tapRooTIntegrateType);

                ((_tapRooTIntegrateType == 'Causal Factor') ? $('#hdnIntegrateTypeCF').val(_tapRooTIntegrateType) : (_tapRooTIntegrateType == 'Root Cause') ? $('#hdnIntegrateTypeRCT').val(_tapRooTIntegrateType) : $('#hdnIntegrateTypeCAP').val(_tapRooTIntegrateType));
               
                $('#hdnIntegrateType').val(_tapRooTIntegrateType);

                divTag.id = "div" + _tapRooTIntegrate;             
               
                divTag.setAttribute('style', 'display:none');
                elementValue = "";
                var createIntegrateDiv = '';

                if (_tapRooTIntegrateType == _cfIntegrateType) {
                    createIntegrateDiv = document.getElementById('tapRooTCFTemplateResults');
                }
                if (_tapRooTIntegrateType == _rctIntegrateType) {
                    createIntegrateDiv = document.getElementById('tapRooTRCTTemplateResults');
                }

                if (_tapRooTIntegrateType == _capIntegrateType) {
                    createIntegrateDiv = document.getElementById('tapRooTCAPTemplateResults');
                }
                $("#div" + _tapRooTIntegrate).hide();

                createIntegrateDiv.setAttribute('style', 'clear:both;');
                var divShowIntegrateCF = document.getElementById('divShowIntegrateCF');
                var divShowIntegrateRCT = document.getElementById('divShowIntegrateRCT');
                var divShowIntegrateCAP = document.getElementById('divShowIntegrateCAP');

                $('#divShowIntegrateCF').hide();
                $('#divShowIntegrateRCT').hide();
                $('#divShowIntegrateCAP').hide();
               
                divValues.appendChild(divShowIntegrateCF);
                divValues.appendChild(divShowIntegrateRCT);
                divValues.appendChild(divShowIntegrateCAP);

                divValues.appendChild(createIntegrateDiv);

            }

            if (elementText == _line) {

                divTag.setAttribute('style', lineField);
                divTag.innerHTML = "";

            }
            else if (elementText == _lineBreak || elementText == _pageBreak)//page break and line break
            {
                divTag.innerHTML = (elementText == _lineBreak ? divTagLineBreak: divTagPageBreak);
            }           
          
            //images code
            if (jsonData[i].ElementText == _image) {

                var elementId = jsonData[i].ElementId;
                elementId = elementId + 1;

                imageTag.id = 'img' + elementId;
                imageTag.setAttribute("runat", "server");

                imageTag.onload = null;

                imageTag.src = window.location.protocol + "//" + window.location.host + _baseURL + "temp/" + jsonData[i].FileName[_fileLength];
                _arrFileNames.push(jsonData[i].FileName[_fileLength]);

                divTag.appendChild(imageTag);
                _fileLength++;
            }
           
            else if (elementText == _causalFactor || elementText == _rootCauses || elementText == _actionPlans )
            {

                var divSingleItemValue = document.createElement('div');
                divTag.innerHTML = '<strong> ' + elementText + ': </strong>' + elementValue;                

            }
        
            else if (elementText.trim() != "" && elementText != _line && elementText != _tapRooTIntegrate && elementText != _lineBreak && elementText != _pageBreak) {

                divText.innerHTML = '<strong>' + elementText + ': </strong>';
                divValue.innerHTML = elementValue;
                
                divText.setAttribute("style", 'width:120px;float:left;min-width:12%;' + jsonData[i].ElementStyle.toString());
                divValue.setAttribute("style", 'min-width:86%;float:right;' + jsonData[i].ElementStyle.toString());
              
                divMain.appendChild(divText);
                divMain.appendChild(divValue);
                divMain.setAttribute("style",  ";clear:both;");
                divValues.appendChild(divMain);          

            }                    
          
                divTag.className = "ListElementText";
                divValues.appendChild(divTag);

                if (elementText != _tapRooTIntegrate && elementText != "" && elementText != _lineBreak) {
                    //display value
                    var divTag = document.createElement('div');

                    divTag.className = "ListElementValue";
                    divTag.setAttribute("style", jsonData[i].ElementStyle.toString());
                    divValues.appendChild(divTag);

                }
        }
    }

    $("#hdnFileNames").val(_arrFileNames);
    
    // ajax call for TapRooT Integrate values
    if (_eventId != '7BI1vMfp0eQ%3d' && _eventId != '0') {
        //for (var j = 0; j < _arrIntegrateTypes.length; j++)
        //{
            //BindTapRooTIntegrateValues(_arrIntegrateTypes[j]);
            GetAllTapRooTIntegrateValues(_arrIntegrateTypes);
        //}
        
    }
    else {

        var divValues = document.getElementById("div" + _tapRooTIntegrate);

        $('#div' + _tapRooTIntegrate).show();
        if (divValues != null) {
            var columnBind = "<div id='rptItems' class='ClearAll'><div >"
                   + "TapRooT® Summary by " + _tapRooTIntegrateType + '<br/>'
                   + '<span class="paddingLeft55"> - ' + 'Causal Factors' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Root Causes' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Action Plans' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Tasks' + '</span>'
            "</div></div>";
            divValues.innerHTML = columnBind;
        }

    }

}

_causalFactor='Causal Factors';
_rootCauses='Root Causes';
_actionPlans='Action Plans';
_tasks='Tasks';

//service fail method
function DisplayReportDataFail() {
    alert('Service failed.');
}

function GetAllTapRooTIntegrateValues(tapRooTIntegrateType) {
    _integrateType = tapRooTIntegrateType;

    var divValues = document.getElementById("div" + _tapRooTIntegrate);
    if (divValues != null) {

        //Causal Factor,Action Plan,Root Cause
        var reportIntegrateData = new ReportDataIntegrateDetails(_eventId, _reportId, tapRooTIntegrateType);

        var data = JSON.stringify(reportIntegrateData);
        _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetAllTapRooTIntegrateValues';
        //used async: false -need to make it Generic method
        AjaxPost(_serviceUrl, data, GetTapRooTIntegrateValuesSuccess, GetTapRooTIntegrateValuesFail, false);

    }
}


var _integrateType = '';
function BindTapRooTIntegrateValues(tapRooTIntegrateType) {  
    _integrateType = tapRooTIntegrateType;

    var divValues = document.getElementById("div" + _tapRooTIntegrate);
    if (divValues != null) {
       
        //Causal Factor,Action Plan,Root Cause
        var reportIntegrateData = new ReportDataIntegrateDetails(_eventId, _reportId, tapRooTIntegrateType);//will remove hard code

        var data = JSON.stringify(reportIntegrateData);
        _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetTapRooTIntegrateValues';

        //used async: false -need to make it Generic method
        AjaxPost(_serviceUrl, data, GetTapRooTIntegrateValuesSuccess, GetTapRooTIntegrateValuesFail, false);

    }
}

function ReportDataIntegrateDetails(eventId, reportId,integrateType)
{
    this.eventID = eventId;
    this.reportId = reportId;
    this.integrateType = integrateType;
}

var _cfIntegrateType = "Causal Factor";
var _rctIntegrateType = "Root Cause";
var _capIntegrateType = "Action Plan";

//display TapRooT Integrate values-still need to work on
function GetTapRooTIntegrateValuesSuccess(result) {
    
    if (result.d.length > 1)
    {
        for (var z = 0; z < result.d.length; z++) {
            var jsonData = jQuery.parseJSON(result.d[z]);
            
            if (jsonData != null) {
                if (_eventId != '0')
                    $('#div' + _tapRooTIntegrate).hide();

                if (jsonData.length > 0)
                    _integrateType = jsonData[0].ResultType;

                // Causal Factor
                if (_integrateType == _cfIntegrateType) {
                    if (jsonData != null && jsonData.length > 0) {
                        var dataCF = jsonData;
                        var actualCF = [];
                        
                        $.each(dataCF, function (index, item) {
                            if($.inArray(item.Name, actualCF) === -1)
                            {
                                actualCF.push(item.Name);
                            }
                        });

                        var actualData = [];
                        tempString = '<table cellspacing="0" style="width:100%; border: 1px solid black;">';
                        for (var i = 0; i < actualCF.length; i++) {
                            tempString = tempString + '<tr><td colspan="3" style="background-color:black; color:white; text-align:center;">'
                                            + '<span><b>Causal Factor : ' + actualCF[i] + '</b></span></td></tr>';

                            tempString = tempString + '<tr><td colspan="3"><div class="divCFSubHeader">'
                                        + '<div class="divCFSubRootCause"><b>Root Cause</b></div>'
                                        + '<div class="divCFSubActionPlanHeader"><span><b>Corrective Action</b></span></div>'
                                        + '<div class="divCFSubTasksHeader"><span><b>Tasks</b></span></div></div></td></tr>';

                            actualData = $.grep(jsonData, function (item, index) {
                                return (item.Name == actualCF[i]);
                            });
                            
                            for (var j = 0; j < actualData.length; j++) {
                                tempString = tempString + '<tr><td style="border: 1px solid black; width:33%;">' + actualData[j].Title + '</td>'
                                            + '<td style="border: 1px solid black; width:33%;">' + actualData[j].Identifier + '</td>'
                                            + '<td style="border: 1px solid black; width:33%;">';

                                for (var k = 0; k < actualData[j].Tasks.length; k++) {
                                    tempString = tempString + actualData[j].Tasks[k].TaskTypeName + ' by ' + actualData[j].Tasks[k].TaskResponsiblePerson + '<br />';
                                }
                                tempString = tempString + '</td></tr>';
                            }
                        }

                        tempString = tempString + '</table>';

                        $(".divRooTCFTemplate").append(tempString).appendTo("#tapRooTCFTemplateResults");
                    }
                    else {
                        $(".divRooTCFTemplate").append("<div style='border:1px solid #000000;height:15px;padding:5px'>No Causal Factor.</div>").appendTo("#tapRooTCFTemplateResults");
                    }
                }

                // Root Cause
                if (_integrateType == _rctIntegrateType) {
                    if (jsonData != null && jsonData.length > 0) {
                        var dataRC = jsonData;
                        var tempValue = 0;
                        var tempString = '';
                        var actualRC = [];
                        var actualDataRC = [];

                        $.each(dataRC, function (index, item) {
                            if ($.inArray(item.Identifier, actualRC) === -1) {
                                actualRC.push(item.Identifier);
                            }
                        });
                        
                        for (var p = 0; p < actualRC.length; p++) {
                            tempString = tempString + '<table cellspacing="0" style="width:100%; border: 1px solid black;"><tr><td colspan="3"><div class="divCFSubHeader"><div class="divCFSubRootCause"><b>Root Cause</b></div>'
                                    + '<div class="divCFSubActionPlanHeader"><span><b>Corrective Action</b></span></div>'
                                    + '<div class="divCFSubTasksHeader"><span><b>Tasks</b></span></div></div></td></tr>';

                            actualDataRC = $.grep(jsonData, function (item, index) {
                                return (item.Identifier == actualRC[p]);
                            });

                            for (var k = 0; k < actualDataRC.length; k++) {
                                if (k == 0)
                                    tempValue = actualDataRC[k].CausalFactorID;

                                if (tempValue != actualDataRC[k].CausalFactorID && tempValue != 0)
                                    tempValue = actualDataRC[k].CausalFactorID;


                                tempString = tempString + '<tr><td style="border: 1px solid black; width:33%;">' + actualDataRC[k].Title + '</td>'
                                                        + '<td style="border: 1px solid black; width:33%;">' + actualDataRC[k].Identifier + '</td>'
                                                        + '<td style="border: 1px solid black; width:33%;">';

                                for (var l = 0; l < actualDataRC[k].Tasks.length; l++) {
                                    tempString = tempString + actualDataRC[k].Tasks[l].TaskTypeName + ' by ' + actualDataRC[k].Tasks[l].TaskResponsiblePerson + '<br />';
                                }

                                tempString = tempString + '</td></tr>';
                            }

                            tempString += '</table>';

                        }

                        
                        $('.divRooTRCTTemplate').append(tempString).appendTo("#tapRooTRCTTemplateResults");
                    }
                    else {
                        $('.divRooTRCTTemplate').append("<div style='border:1px solid #000000;height:15px;padding:5px;'>No Root Causes.</div>").appendTo("#tapRooTRCTTemplateResults");
                    }
                }

                // Action Plans
                if (_integrateType == _capIntegrateType) {
                    if (jsonData != null && jsonData.length > 0) {
                        var cfValue = 0;
                        var tempString = '<table cellspacing="0" style="width:100%; border: 1px solid black;">';
                        var iFlag = false;

                        for (var n = 0; n < jsonData.length; n++) {
                            // If the data is not selected, make the name and title null so that they dont get displayed at the end report
                            if(jsonData[n].IsSelected != true)
                            {
                                jsonData[n].Title = null;
                                jsonData[n].Name = null;
                            }

                            // Code to remove the hyphen at the end of the Identifier
                            jsonData[n].Identifier = jsonData[n].Identifier.trim();
                            
                            if (jsonData[n].Identifier.substring(jsonData[n].Identifier.length - 1, jsonData[n].Identifier.length - 0) == '-') {
                                jsonData[n].Identifier = jsonData[n].Identifier.substring(0, jsonData[n].Identifier.length - 1);
                            }
                        }

                        for (var m = 0; m < jsonData.length; m++) {
                            if (m == 0)
                                cfValue = jsonData[m].CorrectiveActionID;

                            if (cfValue != jsonData[m].CorrectiveActionID && cfValue != 0) {
                                iFlag = false;
                                cfValue = jsonData[m].CorrectiveActionID;
                            }

                            // If Flag is true, just add a record for Root Causes Fixed and Causal Factor
                            // If Flag is false, add header and Tasks and then add Root Causes Fixed and Causal Factor
                            if (iFlag) {
                                if (jsonData[m].IsSelected)
                                    tempString = tempString + '<tr><td style="width:50%; border: 1px solid black;">' + jsonData[m].Title
                                                + '</td><td style="width:50%; border: 1px solid black;"> ' + jsonData[m].Name + '</td></tr>';
                            }
                            else {
                                if (jsonData[m].Tasks.length > 0) {
                                    for (var n = 0; n < jsonData[m].Tasks.length; n++) {
                                        if (n == 0) {
                                            tempString = tempString + '<tr><td colspan="2" style="border: 1px solid black; color:white; background-color:black; text-align:center"><b>'
                                                        + 'Action Plan: ' + jsonData[m].Identifier + '</b></td></tr>';
                                            tempString = tempString + '<tr><td colspan="2" style="border: 1px solid black;"><span><b>Tasks</b></span></td></tr>';
                                            iFlag = true;
                                        }
                                        tempString = tempString + '<tr><td colpans="2">' + jsonData[m].Tasks[n].TaskTypeName + ' by '
                                                    + jsonData[m].Tasks[n].TaskResponsiblePerson + '</td></tr>';
                                    }
                                }
                                else
                                {
                                    tempString = tempString + '<tr><td colspan="2" style="border: 1px solid black; color:white; background-color:black; text-align:center"><b>'
                                                        + 'Action Plan: ' + jsonData[m].Identifier + '</b></td></tr>';
                                    tempString = tempString + '<tr><td colspan="2" style="border: 1px solid black;"><span><b>No Tasks</b></span></td></tr>';
                                }
                                
                                if (jsonData[m].Title == null && jsonData[m].Name == null) {
                                    tempString = tempString + '<tr><td colspan="2" style="width:50%; border: 1px solid black;"><span><b>No Root Causes and Causal Factor</b></span></td></tr>';
                                    iFlag = false;
                                }
                                else
                                    tempString = tempString + '<tr><td style="width:50%; border: 1px solid black;"><span><b>Root Causes Fixed</b></span></td><td '
                                                + 'style="width:50%; border: 1px solid black;"><span><b>Causal Factor</b></span></td></tr>';

                                if (jsonData[m].IsSelected)
                                    tempString = tempString + '<tr><td style="width:50%; border: 1px solid black;">' + jsonData[m].Title
                                                + '</td><td style="width:50%; border: 1px solid black;"> ' + jsonData[m].Name + '</td></tr>';
                            }
                        }

                        $('.divRooTCAPTemplate').append(tempString).appendTo("#tapRooTCAPTemplateResults");
                    }
                    else {
                        //$('#divShowIntegrateCAP').show();
                        $('.divRooTCAPTemplate').append("<div style='border:1px solid #000000;height:15px;padding:5px;'>No Action Plans.</div>").appendTo("#tapRooTCAPTemplateResults");
                    }
                }
            }
        }
    }
    else
    {
        var jsonData = jQuery.parseJSON(result.d);

        if (_eventId != '0')
            $('#div' + _tapRooTIntegrate).hide();

        if (jsonData.length > 0)
            _integrateType = jsonData[0].ResultType;

        if (_integrateType == _cfIntegrateType) {
            if (jsonData != null && jsonData.length > 0) {
                $("#tapRooTCFTemplate").tmpl(jsonData).appendTo("#tapRooTCFTemplateResults");
                $('#hdnCFValues').val("#tapRooTCFTemplateResults");
            }
            else {
                $('#divShowIntegrateCF').show();
                $('#divShowIntegrateCF')[0].innerHTML = "<div style='border:1px solid #000000;height:15px;padding:5px'>No Causal Factor.</div>";
            }
        }

        if (_integrateType == _rctIntegrateType) {
            if (jsonData != null && jsonData.length > 0) {
                $("#tapRooTRCTTemplate").tmpl(jsonData).appendTo("#tapRooTRCTTemplateResults");
            }
            else {
                $('#divShowIntegrateRCT').show();
                $('#divShowIntegrateRCT')[0].innerHTML = "<div style='border:1px solid #000000;height:15px;padding:5px;'>No Root Causes.</div>";
            }
        }
        if (_integrateType == _capIntegrateType) {
            if (jsonData != null && jsonData.length > 0) {
                $("#tapRooTCAPTemplate").tmpl(jsonData).appendTo("#tapRooTCAPTemplateResults");
            }
            else {
                $('#divShowIntegrateCAP').show();
                $('#divShowIntegrateCAP')[0].innerHTML = "<div style='border:1px solid #000000;height:15px;padding:5px;'>No Action Plans.</div>";
            }
        }
    }
    
}

//service fail method.
function GetTapRooTIntegrateValuesFail() {
    alert('Service Failed.');
}

