﻿///*Custom Reports Methods*/

//Global Variables
var _companyId = '';
var _userId = '';
var _virtualDirectory = '';
var _eventId = '';
var _editEventId = '';
var _subTab = '';
var _module = '';
var _message = '';
var _shouldCancel = false;
var _sorting = false;
var _deleteIcon = false;
var _updateHiddenValue = false;
var _spanCount = 0;
_previousTabName = '';
var _arrLabelElements;
var _arrFieldElements;
var _customControlField = 'Fields';
var _customSpacer = 'Line Break';
var _customPageBreak = "Page Break";
var _customSmallImage = 'Image (Small)';
var _customMediumImage = 'Image (Medium)';
var _customLargeImage = 'Image (Large)';
var _line = 'Line';
var _label = 'Label';
var _customCount = 0;
var _customLabel = ' Label:[ - Label ] ';
var _customLine = ' Line:[ - Line ] ';
var _uplaodImage = ' Image:[ - Image ] ';
var _imagePathName = '';
var _serviceUrl = '';
var _selectedLabelId = '';
var _columnName = '';
var _imageId = '';
var _deleteId = '';
var _reportTitleName = 'Click to add text';
var _showCompanyLogo = 'Show Company Logo';
var _showCompanyName = 'Show Company Name';
var _showReportTitle = 'Show Report Title';
var _showCreatedDate = 'Show Date Created';
var _headerLabel = 'Header Elements - Details';
var _customFieldLabel = 'Visual Elements - Fields';
var _snapCapLabel = 'SnapCap - Images';
var _tapRootItem = 'TapRooT® Items';
var _tabNameItem = 'Items1';
var _tabNameSingleItem = 'Items';
var _selectedSortOder = '';
var _reportId = "";
var _imageName = "";
var _elementId = '';
var _isTempReport = false;
var _tabName = "Events";
var _reportType = '';
var _isEdit = false;
var _isTempReportDelete = false;
var _isWarningmessageShow = false;
var _isWindowCloseFire = false;
var _eventReport = 'Event';
var _userReport = 'User';
var _systemReport = 'System';

//ready function
$(document).ready(function () {
    _isWindowCloseFire = false;

    //page load method
    PageLoadAction();

    //save Report
    $('#divSaveIcon').click(function () {
        _isTempReport = false;
        SaveReportTemplate();
        _isWindowCloseFire = false;
    });

    $('#btnReportName').click(function () {

        GetReportName(true);
        return false;
    });


    //preview Report
    $('#divPreviewicon').click(function () {
        _isTempReport = true;
        _isWindowCloseFire = false;
        GetReportName(false);
        return false;

    });

    //back to manage Report Page
    GoBackToMainReportPage();

    //on click of Report Title lable show Text Box
    $('#spnReportTitle').click(function () {

        ShowHideTextBox(true);
    });

    $('#txtRptTitle').blur(function () {

        ShowHideTextBox(false);
    });

    $('#txtRptTitle').keypress(function (ev) {
        if (ev.which === 13) {
            ev.preventDefault();
            ShowHideTextBox(false);
        }
    });

    $('txtRptTitle').bind("enterKey", function (e) {

        e.preventDefault();
        ShowHideTextBox(false);
    });

    //close the dialog box
    $('#btnReportTitleClose').click(function () {

        CloseTemplatePopup();
    });

    //close the dialog box
    $('#btnIntegrateClose').click(function () {

        CloseIntegratePopup();
    });

    //close the dialog box
    $('#btnIntegrateOK').click(function () {

        GetIntegrateOptionDesign();
    });
    $("#divReportIntegrateDialog").hide();

    $("body").keydown(function (e) {
        if (e.which == 116 || e.which == 17) {
            _isPreviewReportDelete = true;           
        }
    });

});
//end ready function

//Page Load Methods
var prevOffset, curOffset;
function PageLoadAction() {
    $('#spnReportTitle').show();
    $('#divCreateEditReportDialog').hide();

    if ($('#spnReportTitle')[0] != undefined && $('#spnReportTitle')[0] != '')
        $('#spnReportTitle')[0].textContent = _reportTitleName;

    var companyId = $('#hdnCompanyId').val();
    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        companyId = divisionId;

    _userId = $('#hdnUserId').val();//GetUserId();

    _virtualDirectory = GetVirtualDirectory();

    //get EventId from URL
    GetIdsFromUrl();

    //load the Database table columns
    LoadDataColumn();

    //report area where  droped columns/elements hover
    ReportAreaDivsHover();

    //edit populate the columns
    if (_reportId != "")
        EditColumns();

}

//get all label names from ReportElement table and display in Report Area
function EditColumns() {
   
    //get all label names depends on ReportID
    var reportColumnData = new ReportColumnDataDetails();
    var data = JSON.stringify(reportColumnData);

    _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetColumnLabelNames';
    AjaxPost(_serviceUrl, data, eval(GetColumnLabelNamesSuccess), eval(GetColumnLabelNamesFail));
}

//pass paramerter 
function ReportColumnDataDetails() {

    var companyId = $('#hdnCompanyId').val();
    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        companyId = divisionId;

    this.userID = _userId;
    this.companyID = companyId;
    this.reportId = _reportId;
}

var _isPreviewReportDelete = false;
var _editReportType = '';
//success method for Get Column Names for edit
function GetColumnLabelNamesSuccess(result) {

    var jsonData = jQuery.parseJSON(result.d);
    if (jsonData.length > 0) {
        var columnBind = '';

        var tabName = _tabName;
        var columnName = '';
        var sortOrder = '';
        var elementType = '';
        var elementId = '';
        var elementStyle = '';
        var reportTitleStyle = '';
        var isCompanyLogoShow = jsonData[0].isCompanyLogoUsed;
        var isCompanyNameShow = jsonData[0].isCompanyNameUsed;
        var isCreatedDateShow = jsonData[0].isCreatedDateUsed;
        var isReportTitleShow = jsonData[0].isReportTitleUsed;


        _reportId = jsonData[0].ReportId;
        _isTempReportDelete = jsonData[0].isTemporaryReport;
      
        _editReportType = jsonData[0].ReportType;

        _eventId = $.cookie("EventId");

        if (jsonData[0].ReportTitle != "")
            $('#spnReportTitle')[0].textContent = jsonData[0].ReportTitle;


        if (jsonData[0].ReportName != "")
            _isPreviewReportDelete = true;

        $('#txtReportName').val(jsonData[0].ReportName);

        if (jsonData[0].Description != '')
            $('#txtReportDescription').val(jsonData[0].Description);

        if (jsonData[0].ReportTitleStyle)
            $('#spnReportTitle')[0].style.cssText = jsonData[0].ReportTitleStyle;


        //header details hide and show
        EditHideShowHeaderDetails(isCompanyLogoShow, isCompanyNameShow, isCreatedDateShow, isReportTitleShow);

        //take all label names and bind it to div's
        for (var i = 0; i < jsonData.length; i++) {

            _customCount++;

            var imageEditTag = new Image();

            if (jsonData[i].ElementText != '')
                columnName = jsonData[i].ElementText;

            if (jsonData[i].ElementType[i] != '')
                elementType = jsonData[i].ElementType[i];

            if (jsonData[i].ElementType[i] != undefined && jsonData[i].ElementType[i] != "")
                tabName = ((elementType == 6) ? _tabNameSingleItem : (elementType == 7) ? _tabNameItem : (elementType == 4) ? _tabName : (elementType == 5) ? 'Custom Data' : (elementType == 2 || elementType == 1 || elementType == 3) ? 'Fields' : string.emty);

            if (jsonData[i].SortOrder != "")
                sortOrder = jsonData[i].SortOrder;

            if (jsonData[i].ElementId != "")
                elementId = jsonData[i].ElementId;


            if (jsonData[i].ElementStyle != "")
                elementStyle = jsonData[i].ElementStyle.trim();

            elementId = elementId + 1;
            _tempDropId = _customCount;
            if (columnName == _line) {

                columnBind += "<div class='ClearAll width98' style='height:26px;' id='rptItems'>"
                                   + "<div class='customLabel' id='" + _customCount + "' style='" + elementStyle + "' data-tabname='" + tabName + "' data-edit='true' data-elementid='" + elementId + "'  data-columnname='" + jsonData[i].ElementText + "' >"

                                   + "  <span id='delete" + sortOrder + "' onClick=\'javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + ")\; return false;'></span>"
                                   + "<hr id='" + _customCount + "' class='customLine'></div></div>";

                _tempDropId = sortOrder;
                _deleteId = sortOrder;

                _deleteIcon = false;//avoid on click of cross icon it was firing this method
            }
            else if (elementType == 1)//label
            {

                columnBind += "<div class='ClearAll ' style='height:26px;' id='rptItems'><input type='text' id='txt" + sortOrder + "' onblur=\'javascript:RemoveTextBox(" + _customCount + "," + sortOrder + ")\; return false;' style='margin-top:10px;display:none;'> </input>"
                             + "<div class='customLabel' id=" + _customCount + " style='" + elementStyle + "' data-tabname='" + tabName + "' data-edit='true' data-elementid='" + elementId + "'  onClick=\'javascript:InsertTextBox(" + _customCount + "," + sortOrder + ")\; return false;' data-columnname='" + jsonData[i].ElementText + "' >"
                                + jsonData[i].ElementText + ':' +
                                "  <span id='delete" + sortOrder + "' onClick=\'javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + ")\; return false;'></span></div><div class='ui-droppable dropBreak' id=space" + sortOrder + "></div></div>";

            }
            else if (elementType == 3)//image
            {
                _deleteIcon = false;
                _imageId = _customCount;


                var elementId = jsonData[i].ElementId;
                elementId = elementId + 1;

                imageEditTag.id = 'img' + elementId;
                imageEditTag.setAttribute("runat", "server");

                imageEditTag.onload = null;
                imageEditTag.src = _baseURL + 'tap.snap.hand/LoadImage.ashx?elementId=' + imageEditTag.id;

                //image functionality
                columnBind += "<div class='ClearAll' id='rptItems' ><div id='div" + _customCount + "' data-tabname='" + tabName + "' data-elementid='" + elementId + "' data-edit='true' data-columnname='" + jsonData[i].ElementText + "'   >"

                         + "  <span id='delete" + sortOrder + "' onClick=\'javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + ")\; return false;'></span>"
                          + "<img id='img" + elementId + "'  src='" + _baseURL + "tap.snap.hand/LoadImage.ashx?elementId=" + imageEditTag.id + "' runat='server'> </div><div class='ui-droppable dropBreak' id='space" + _customCount + "'></div></div>";

            }
                //for TapRooT integration
            else if (elementType == 7) {

                columnBind += "<div id='rptItems' class='ClearAll  showTapRootIntegrate'><div style='" + elementStyle + "' id=" + _customCount + " data-tabname='" + tabName + '1' + "' data-elementid='" + elementId + "' data-edit='true' onClick=\'javascript:FormatText(" + sortOrder + ")\; return false;' data-columnname='" + jsonData[i].ElementText + "' >"
                   + "TapRooT® Summary by " + jsonData[i].IntegrateType + '<br/>'
                   + '<span class="paddingLeft55"> - ' + 'Causal Factors' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Root Causes' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Action Plans' + '</span><br/>'
                   + '<span class="paddingLeft55"> - ' + 'Tasks' + '</span>' +
                   " <span id='delete" + sortOrder + "' onClick=\'javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + ")\; return false;'></span></div><div class='ui-droppable dropBreak' id=space" + _customCount + "></div></div>";
                $('.showTapRootIntegrate').show();

            }
            else if (elementType == 6) {
                //display div - Column Name and Module Name   
                columnBind += "<div id='rptItems' class='ClearAll'><div id=" + _customCount + " style='" + elementStyle + "' data-tabname='" + tabName + "' data-edit='true' data-elementid='" + elementId + "' data-columnname='" + jsonData[i].ElementText + "' onClick=\'javascript:FormatText(" + sortOrder + ");return false;\' >"
                              + '[ TapRooT® : ' + jsonData[i].ElementText +

                               " ]<span id='delete" + sortOrder + "' onClick=\'javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + "); return false;\'></span></div><div class='ui-droppable dropBreak' id=space" + _customCount + " ></div></div>";


            }
            else {

                //display div - Column Name and Module Name   
                columnBind += "<div id='rptItems' class='ClearAll' ><div id='" + _customCount + "' style='" + elementStyle + "' data-tabname='" + tabName + "' data-edit='true' data-elementid='" + elementId + "' data-columnname='" + jsonData[i].ElementText + "' onClick=\'javascript:FormatText(" + sortOrder + ");return false;\' >"
                               + jsonData[i].ElementText +
                               ":[  "
                               + jsonData[i].ElementText +
                               " ]<span id='delete" + sortOrder + "' onClick='javascript:DeleteSelectedColumn(" + sortOrder + "," + _customCount + "); return false;'></span></div><div class='ui-droppable dropBreak' id='space" + _customCount + "'></div></div>";


            }

            ChildDroppable('space' + _customCount);
        }


        //for delete id- added count when next column drag and drops it should start from next count number
        _spanCount = jsonData.length;
        $('#reportArea').html(columnBind);

        $("#reportArea").droppable({ disabled: false });

        //After creating div's in Report Area- rpt items hover method
        ReportItemsDivHover();

        AddEditItemNames(null, null);

        _isWarningmessageShow = true;
        ChildDroppable('space' + _customCount);
    }

}

function EditHideShowHeaderDetails(isCompanyLogoShow, isCompanyNameShow, isCreatedDateShow, isReportTitleShow) {


    //hide all div's
    $('#divCompanyLogo').hide();
    $('#divTitle').hide();
    $('#divCompany').hide();
    $('#divDate').hide();
    $('#chkShowCompanyLogo')[0].checked = false;
    $('#chkShowCompanyName')[0].checked = false;
    $('#chkShowDateCreated')[0].checked = false;
    $('#chkShowReportTitle')[0].checked = false;

    if (isCompanyLogoShow && _isReportLogo) {
        $('#divCompanyLogo').show();
        $('#chkShowCompanyLogo')[0].checked = true;
    }


    if (isCompanyNameShow) {

        $('#divCompany').show();
        $('#chkShowCompanyName')[0].checked = true;

    }

    if (isCreatedDateShow) {
        $('#divDate').show();
        $('#chkShowDateCreated')[0].checked = true;
    }

    if (isReportTitleShow) {
        $('#divTitle').show();
        $('#chkShowReportTitle')[0].checked = true;
    }

    //if header details unchecked then hide the header width
    if (divCompanyLogo.style.display == 'none' && divTitle.style.display == 'none' && divCompany.style.display == 'none' && divDate.style.display == 'none') {

        $('#reportArea').removeClass('margin14');
        $('#reportArea').addClass('margin8');
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');

    }
    else if (divCompanyLogo.style.display == 'none') {

        $('#reportArea').removeClass('margin14');
        $('#reportArea').addClass('margin8');
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');
    }
    else {
        $('#reportArea').removeClass('margin8');
        $('#reportArea').addClass('margin14');
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');

    }
}

//service fail method
function GetColumnLabelNamesFail(msg) {
    alert('Service Fails.');
}


//show textbox
function ShowHideTextBox(isTextBox) {

    if (isTextBox) {

        if ($('#txtRptTitle')[0].value == "" && $('#spnReportTitle')[0].textContent == _reportTitleName) {
            $('#spnReportTitle')[0].textContent = _reportTitleName;
            $('#spnReportTitle')[0].style.cssText = "display:inline";
        }
        else {
            $('#txtRptTitle')[0].value = $('#spnReportTitle')[0].textContent;
            FormatText('spnReportTitle');
        }

        $('#spnReportTitle').hide();
        $('#txtRptTitle').show();
        $('#txtRptTitle').focus();


    }
    else {
        $('#spnReportTitle').show();
        $('#txtRptTitle').hide();
        reportTitleValue = $('#txtRptTitle')[0].value;

        if (reportTitleValue != '' && reportTitleValue !== undefined)
            $('#spnReportTitle')[0].innerHTML = reportTitleValue;
        else {
            $('#spnReportTitle')[0].textContent = _reportTitleName;
            $('#spnReportTitle')[0].style.cssText = "display:inline";
        }

    }

    FormatText('spnReportTitle');
}


//load the table columns
function LoadDataColumn() {
    var columnNamesData = new GetColumnNamesDetails();
    var data = JSON.stringify(columnNamesData);
    var serviceURL = _baseURL + 'Services/CustomReport.svc/GetColumnNames';
    PostAjax(serviceURL, data, eval(GetColumnNamesSuccess), eval(GetColumnNamesFail));
}

function GetColumnNamesDetails() {
    this.companyId = _companyId;
    this.userId = _userId;
    this.eventId = _eventId;
    this.reportType = _reportType;
    this.reportId = (_reportId == '' ? '0' : _reportId);
}

//Column Data parameter
function ColumnDataDetail(tabId, eventId, moduleId, virtualDirectory) {

    this.tabId = tabId;
    this.eventID = eventId;
    this.moduleId = moduleId;
    this.virtualDirectory = virtualDirectory;
}

_isReportLogo = false;
//GetColumnNames success method
function GetColumnNamesSuccess(result) {
    var moduleName = '';
    var headerModule = 'Header Elements';
    var jsonData = jQuery.parseJSON(result.d);
    var divValues = document.getElementById('divAccordian');
    var reportMargin = '5px';

    //Report Logo 
    if (jsonData[0][0].ReportLogoName != undefined && jsonData[0][0].ReportLogoName != null) {

        _isReportLogo = true;
        $('#divCompanyLogo').show();
        var imagePath = window.location.protocol + "//" + window.location.host + _baseURL + "temp/" + jsonData[0][0].ReportLogoName;

        $("#imgCompanyLogo").attr("src", imagePath);
        $('#reportArea').removeClass('margin8');
        $('#reportArea').addClass('margin14');
        $('#divHeader').removeClass('height5');
        $('#divHeader').addClass('height15');

    }
    else {
        $('#divCompanyLogo').hide();
        $('#reportArea').removeClass('margin14');
        $('#reportArea').addClass('margin8');
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');

    }

    for (var i = 0; i < jsonData.length; i++) {

        _customCount = jsonData.length - 2;
        var resultset = jsonData[i][0];
        var anchortag = document.createElement('a');
        var livalues = document.createElement('li');
        var divAccordionTag = document.createElement('div');
        var checkBoxTag = document.createElement('input');

        if (resultset.TabName != undefined && resultset.TabName != "") {

            var tabName = resultset.TabName;

            //create header -Module Name with Tab name
            if (_previousTabName != tabName) {

                var divtag = document.createElement('div');
                var ulheader = document.createElement('ul');
                var headerTag = document.createElement('h2');

                anchortag.href = "#";
                anchortag.className = 'anchorHeader';
                anchortag.id = 'anc' + i.toString();
                anchortag.style.cursor = "move";//for crosshair icon

                //for isUniversal Tab - Module name is coming null.Replace null to  Incident Module 
                moduleName = ((resultset.ModuleName != null) ? resultset.ModuleName : 'Incident');

                if (moduleName == headerModule)
                    anchortag.style.cursor = "pointer";//for crosshair icon
             
                anchortag.innerHTML = (moduleName=='TapRooT®' ?  moduleName +' '+ tabName:  moduleName + ' - ' + tabName);
                headerTag.id = 'hd' + i.toString();
               
                headerTag.setAttribute('data-tabname', tabName);
                //headerTag.setAttribute('data-custom', resultset.CustomTab);
                if (resultset.CustomTab)
                {
                    headerTag.setAttribute('class', 'notranslate');
                    headerTag.setAttribute('style','url("images/ui-bg_highlight-soft_75_c7c7c7_1x100.png") repeat-x scroll 50% 50% #c7c7c7');
                    ulheader.setAttribute('class', 'notranslate');
                }
                   

                headerTag.appendChild(anchortag);
                

                _previousTabName = tabName;
            }

            //create column names
            if (resultset.ColumnName != undefined && resultset.ColumnName != "") {

                livalues.innerHTML = resultset.ColumnName;
                livalues.style.cursor = "move";//for crosshair icon

                if (moduleName == headerModule) {

                    checkBoxTag.type = "checkbox";
                    checkBoxTag.id = "chk" + resultset.ColumnName.replace(/ /g, '');
                    checkBoxTag.checked = true;
                    checkBoxTag.setAttribute('onclick', "javascript:ShowHideHeaderDetails('" + resultset.ColumnName.replace(/ /g, '') + "')");

                    livalues.style.cursor = "pointer";//for crosshair icon
                    livalues.appendChild(checkBoxTag);

                }
            }

            livalues.id = 'li_' + i.toString();
            livalues.style.marginTop = reportMargin;

            
            livalues.setAttribute('data-tabname', tabName);
            livalues.setAttribute('data-staticfield', resultset.isStaticColumn);
            livalues.setAttribute('data-snapimageid', resultset.SnapCapImageId);
            livalues.setAttribute('data-fieldvalue', resultset.ColumnName);
            livalues.setAttribute('style', 'display:block');
            //livalues.setAttribute('data-edit', resultset.CustomTab);
            ulheader.appendChild(livalues);

            divtag.appendChild(ulheader);
        }

        divValues.appendChild(headerTag);
        divValues.appendChild(divtag);

    }

    //accordian tabs and columns
    $("#divAccordian").accordion({
        collapsible: true,
        autoHeight: false
    });

  

    $('#divAccordian > h2 > a').click(function () {
      
        var id = $(this).attr("class");

        var n_id = "#" + id;

        $(n_id).slideUp();

        if ($(this).parent().next().is(':hidden')) {
            $(this).parent().next().slideDown();
        }
        else
        {
            $(this).parent().next().slideUp();           
           
        }          

        return false;
    });

    //toolbox accordian columns
    AccordianColumns();

    //toolbox accordian Tab(s)
    AccordianTabs();

    //drag and drop functionality for Column Names
    DragDropArea();

    //drag and drop functionality for Tab(s) 
    DragDropTabs();

}



//show and hide the header details-on select of check boxes
function ShowHideHeaderDetails(id) {

    var checkBoxId = ($('#chk' + id).attr('checked') == "checked" ? true : false);

    if (id == _showCompanyLogo.replace(/ /g, ''))
        ((checkBoxId == true && _isReportLogo) ? $('#divCompanyLogo').show() : $('#divCompanyLogo').hide());

    if (id == _showCompanyName.replace(/ /g, ''))
        (checkBoxId == true ? $('#divCompany').show() : $('#divCompany').hide());

    if (id == _showReportTitle.replace(/ /g, ''))
        (checkBoxId == true ? $('#divTitle').show() : $('#divTitle').hide());

    if (id == _showCreatedDate.replace(/ /g, ''))
        (checkBoxId == true ? $('#divDate').show() : $('#divDate').hide());

    //if header details unchecked then hide the header width
    if (divCompanyLogo.style.display == 'none' && divTitle.style.display == 'none' && divCompany.style.display == 'none' && divDate.style.display == 'none') {

        $('#reportArea').removeClass('margin14');
        $('#reportArea').addClass('margin8');
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');
    }
    else if (divCompanyLogo.style.display == 'none') {
        $('#reportArea').removeClass('margin14');
        $('#reportArea').addClass('margin8');
        $('#divCompanyLogo').hide();
        $('#divHeader').removeClass('height15');
        $('#divHeader').addClass('height5');
    }
    else {
        $('#reportArea').removeClass('margin8');
        $('#reportArea').addClass('margin14');
        $('#divHeader').removeClass('height5');
        $('#divHeader').addClass('height15');

    }

}

//toolbox accordian columns-on mouse hover highlight yellow color
function AccordianColumns() {

    $("#divAccordian li ").hover(function () {

        var columnName = $(this)[0].textContent;

        if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate)
            $(this).addClass('highlight');

    }, function () {
        $(this).removeClass('highlight');
    });
}

//toolbox accordian Tab(s)-on mouse hover highlight yellow color
function AccordianTabs() {

    $("#divAccordian h2 a").hover(function () {

        if ($(this)[0].textContent != _headerLabel && $(this)[0].textContent != _customFieldLabel)
            $(this).addClass('highlight');

    }, function () {
        $(this).removeClass('highlight');

    });
}

//report area where we can drop columns/elements-on mouse hover highlight yellow color
function ReportAreaDivsHover() {

    $("#reportArea div").hover(function () {

        if ($(this)[0].id != '')
            $(this).addClass('highlight');

    }, function () {

        $(this).removeClass('highlight');
    });

}

//service fail method
function GetColumnNamesFail() {
    alert('Service method failed.');
}

////Drag and drop code for columns
function DragDropArea() {

    $("#divAccordian").accordion(); //hide and show

    //draggable
    $("#divAccordian li").draggable({

        appendTo: "body",
        containment: 'parent', //// sets to can be dragged only within "#reportArea"  element  
        helper: "clone",//get the name of column name while drag starts to ends       
        tolerance: 'fit',
        handle: "h2",
        //connectToSortable: '#reportArea',
        start: function (event, ui) {

            var columnName = $("#" + $(this)[0].id)[0].textContent;

            if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate
                    && columnName != _headerLabel && columnName != _customFieldLabel && columnName != _snapCapLabel) {

                // get the initial X and Y position when dragging starts
                xpos = ui.position.left;
                ypos = ui.position.top;

                $("#reportArea").droppable({ disabled: false });

                // given dashed border 
                $("#" + $(this)[0].id).addClass('drag-highlight');

                //  give red background when it starts drag
                $(ui.helper).addClass("not-droppable");

            }

        },
        // when dragging stops
        stop: function (event, ui) {

            var columnName = $("#" + $(this)[0].id)[0].textContent;

            if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate
                    && columnName != _headerLabel && columnName != _customFieldLabel && columnName != _snapCapLabel) {

                // calculate the dragged distance, with the current X and Y position and   the "xpos" and "ypos"
                var xmove = ui.position.left - xpos;
                var ymove = ui.position.top - ypos;

                // define the moved direction: right, bottom (when positive), left, up  (when negative)
                var xd = xmove >= 0 ? ' To right: ' : ' To left: ';
                var yd = ymove >= 0 ? ' Bottom: ' : ' Up: ';


                //remove dashed border -yellow and gray dashed lines
                $(this).removeClass('drag-highlight');
                _isWindowCloseFire = true;
            }
        }


    });

    //drop functionality
    DropAllFields();

}

var _tempDropId = '';
var _columnType = '';
var dropHelp = true;
//drop functionality for columns,labels,lines..
function DropAllFields() {

    $("#reportArea").droppable({


        accept: ":not(.ui-sortable-helper)",

        drop: function (event, ui) {

            var columnName = ui.draggable[0].textContent;

            if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate
                && columnName != _headerLabel && columnName != _customFieldLabel && columnName != _snapCapLabel) {

                var tabName = ui.draggable.attr('data-tabname');
                var isStaticColumn = ui.draggable.attr('data-staticfield');
                var snapCapImageId = ui.draggable.attr('data-snapimageid');
                var columnValue = ui.draggable.attr('data-fieldvalue');

                if (snapCapImageId == undefined)
                    snapCapImageId = 0;

                if (ui.draggable.attr('id') != undefined) {

                    var divId = ui.draggable.attr('id').substring(3);//remove drag id ('li_'- append for sort order)

                    var checkLi = "li_";
                    var isCheckLiExist = ui.draggable.attr('id').indexOf(checkLi);

                    if (isCheckLiExist != -1) {

                        _spanCount++;

                        //custom control-Label
                        if (tabName == _customControlField) {

                            _customCount++;
                            _columnName = ui.draggable.text();

                            if (columnValue == _customSmallImage || columnValue == _customMediumImage || columnValue == _customLargeImage) {

                                _deleteIcon = false;
                                _imageId = _customCount;

                                $('#hdnImageAccordianSize').val(columnValue);
                                _tempDropId = _customCount;
                                OpenImageDialog();


                                //image functionality
                                $("<div class='ClearAll' id='rptItems' ><div id='" + _customCount + "' data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-edit='false' data-columnname='" + columnValue + "'  >"
                                         + "  <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak' >break</div></div>").appendTo(this);
                                ChildDroppable('space' + _customCount);

                            }
                            else {

                                _deleteId = _spanCount;

                                //line  give 100% width              
                                if (columnValue == _line) {
                                    _tempDropId = _customCount;

                                    $("<div class='ClearAll width98' id='rptItems'>"
                                     + "<div class='customLabel' id=" + _customCount + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "'  data-edit='false'  data-columnname='" + columnValue + "' >"
                                                            +
                                                            "  <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span>"
                                                            + "<hr id=" + _customCount + " class='customLine'></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);

                                }
                                else if (columnValue == _customSpacer || columnValue == _customPageBreak) {
                                    _tempDropId = _customCount;

                                    //display div - Column Name and Module Name   
                                    $("<div class='ClearAll' id='rptItems' ><div id=" + _customCount + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-columnname='" + columnValue + "' data-edit='false'  onClick=\'javascript:FormatText(" + _spanCount + ")\'; return false; >"
                                                + ui.draggable.text() +
                                                ":[ "
                                                + ui.draggable.text() +
                                                " ] <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);//optimise this line 2 places same code is there

                                    _customCount++;
                                }

                                else {

                                    _tempDropId = _spanCount;

                                    $("<div class='ClearAll' id='rptItems'><input type='text' id='txt" + _spanCount + "' onblur=\'javascript:RemoveTextBox(" + _customCount + "," + _spanCount + ")\'; return false; style='display:none;margin-top:10px;'> </input>"
                                 + "<div class='customLabel' id=" + _customCount + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-edit='false'  onClick=\'javascript:InsertTextBox(" + _customCount + "," + _spanCount + ")\'; return false; data-columnname='" + columnValue + "' >"
                                                     +
                                                     "  <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);//optimise this line 2 places same code is there

                                }

                                //insert the textbox when user drag and drop Custom Label
                                InsertTextBox(_customCount, _spanCount);
                            }

                        }
                        else {

                            _tempDropId = divId;
                            //taproot single item
                            if (tabName == _tabNameSingleItem) {
                                //display div - Column Name and Module Name   
                                $("<div class='ClearAll' id='rptItems' ><div id=" + divId + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-columnname='" + columnValue + "' data-edit='false'  onClick=\'javascript:FormatText(" + _spanCount + ")\'; return false; >"
                                    + '[ TapRooT® : ' + ui.draggable.text() +
                                            " ] <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + divId + " class='dropBreak'></div></div>").appendTo(this);//optimise this line 2 places same code is there  
                            }
                            else if (tabName == 'Images') {
                                _customCount++;
                                _tempDropId = _customCount;
                                _deleteIcon = false;
                                _imageId = _customCount;

                                $('#hdnImageAccordianSize').val(_columnName);

                                var imagePath = window.location.protocol + "//" + window.location.host + _baseURL + "temp/" + snapCapImageId + '.jpg';
                                //image functionality
                                $("<div class='ClearAll' id='rptItems' ><div  id='" + _customCount + "'  data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-edit='false' data-columnname='" + columnValue + "'   >"
                                         + "  <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span><img id='img43' runat='server' src=' " + imagePath + "' ></div><div id=space" + divId + " class='dropBreak'></div></div>").appendTo(this);//optimise this line 2 places same code is there


                            }
                            else//normal columns like events , custom fields
                            {
                                _customCount++;
                                _tempDropId = _customCount;
                                //display div - Column Name and Module Name   
                                $("<div class='ClearAll'  id='rptItems'  style='display:block;')><div style='margin:0px;padding:0px;' id=" + _customCount + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-columnname='" + columnValue + "' data-edit='false'  onClick=\'javascript:FormatText(" + _spanCount + ")\'; return false; >"
                                            + ui.draggable.text() +
                                            ":[ "
                                            + ui.draggable.text() +
                                            " ] <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);


                                //child droppable
                                ChildDroppable('space' + _customCount);

                            }

                        }
                        //child droppable
                        ChildDroppable('space' + divId);
                    }
                    else {

                        //get all columns inside the main tab
                        divSiblings = ui.draggable[0].nextElementSibling.childNodes[0].childNodes;

                        //for google translate bug - 
                        if (tabName == _tabNameSingleItem) {

                            OpenIntegrateDialog();

                            divId = divSiblings[0].id.substring(3);
                            var columnValue = divSiblings[0].getAttribute('data-fieldValue');
                            _spanCount++;

                            _tempDropId = _customCount;

                            $("<div id='rptItems' class='ClearAll showTapRootIntegrate hide'><div style='border:0.5px solid #747474;border-style: dashed;margin-left:5px;' id=" + _customCount + " data-tabname='" + tabName + '1' + "'  data-edit='false' onClick=\'javascript:FormatText(" + _spanCount + ")\'; return false; data-columnname='" + columnValue + "' >"
                               + "TapRooT® Summary by " + '<br/>'
                               + divSiblings[0].textContent + '<br/>'
                               + divSiblings[1].textContent + '<br/>'
                               + divSiblings[2].textContent + '<br/>'
                               + divSiblings[3].textContent +
                               " <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);

                            _customCount++;
                        }
                        else if (tabName != 'Details' && tabName != 'Fields' && tabName != 'Images')//this condition is for Google Translate bug when we change language it is allowing to drop Details,Fields columns
                        {

                            //display div - Column Name and Module Name 
                            for (var i = 0; i < divSiblings.length; i++) {

                                _spanCount++;
                                divId = divSiblings[i].id.substring(3);

                                _tempDropId = _customCount;
                                //IE10 issue
                                var isStaticColumn = divSiblings[i].getAttribute('data-staticfield');

                                var columnValue = divSiblings[i].getAttribute('data-fieldValue');


                                $("<div id='rptItems' class='ClearAll' ><div id=" + _customCount + " data-tabname='" + tabName + "' data-snapimageid='" + snapCapImageId + "' data-static='" + isStaticColumn + "' data-edit='false' onClick=\'javascript:FormatText(" + _spanCount + ")\'; return false; data-columnname='" + columnValue + "' >"
                                           + divSiblings[i].textContent +
                                           ":[ "
                                           + divSiblings[i].textContent +
                                           " ]<span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span></div><div id=space" + _customCount + " class='dropBreak'></div></div>").appendTo(this);


                                //child droppable
                                ChildDroppable('space' + _customCount);
                                _customCount++;
                            }
                        }
                    }


                    //drag and drop Custom Label,Line
                    DragDropCustomControls();

                    $("#reportArea").droppable({ disabled: false });

                    //add or edit items in arraylist
                    AddEditItemNames(event, ui);
                }
            }

        },

        over: function (event, ui) {

            DropHover(event, ui);

        },
        out: function (event, ui) {

            DropOut(event, ui);

        }

    })
        .sortable({
            tolerance: "pointer",
            axis:'y',
            sort: function () {
                // gets added unintentionally by droppable interacting with sortable
                // using connectWithSortable fixes this, but doesn't allow you to  customize active/hoverClass options
                $(this).removeClass("ui-state-default");
            },
            update: function (event, ui) {
                AddEditItemNames(event, ui);
            },

            start: function (event, ui) {
                SortStart(event, ui);
            },

            // when dragging stops
            stop: function (event, ui) {
                SortStop(event, ui);
            },

            over: function (event, ui) {
                SortHover(event, ui);
            },

            out: function (event, ui) {
                SortOut(event, ui);
                _isWindowCloseFire = true;
            }
        });

    $('#reportArea>.ClearAll').resizable({ handles: 's' });

}


//sorting start method
function SortStart(event, ui) {
   
    _sorting = true;
    _deleteIcon = true;
    var id = "";
    var textContent = ui.item[0].textContent;

    var getTextBoxId = ui.item[0].children[0].id;

    if (getTextBoxId != "" || $('#' + getTextBoxId).val() == undefined || $('#' + getTextBoxId).val() == "") {

        id = (((textContent == _customLabel) || (textContent == _customLine)) ? ui.item[0].children[1].id : ui.item[0].children[0].id);

        if (getTextBoxId != "" && getTextBoxId.indexOf('txt') > -1)
            deleteId = ui.item[0].children[1].lastElementChild.id;
        else
            deleteId = (((textContent == _customLabel) || (textContent == _customLine)) ? ui.item[0].children[1].lastElementChild.id : textContent.lastIndexOf('.') != '-1' ? ui.item[0].children[1].children[0].id : ui.item[0].children[0].children[0].id);
    }
    else {
        id = ui.item[0].childNodes[2].id;
        deleteId = ui.item[0].childNodes[2].childNodes[1].id;
    }

    //green,yellow and red cross color classes
    $("#" + id).addClass('drag-highlight-green');
    $("#" + deleteId).removeClass('imageDeleteControl');
    $("#" + id).removeClass('highlight');

}

//sorting stop method
function SortStop(event, ui) {

    //commonDropElements(event, ui);
    _deleteIcon = true;
    var id = "";
    var getTextBoxId = ui.item[0].children[0].id;
    var textContent = ui.item[0].textContent;

    if ($('#' + getTextBoxId).val() == undefined || $('#' + getTextBoxId).val() == "") {

        id = (((textContent == _customLabel) || (textContent == _customLine)) ? ui.item[0].children[1].id : textContent.lastIndexOf('.') != '-1' ? ui.item[0].children[1].id : ui.item[0].children[0].id);
    }
    else {
        id = ui.item[0].childNodes[2].id;
    }

    //green,yellow and red  color classes
    $("#" + id).removeClass('drag-highlight-green');
    $("#" + id).removeClass('highlight');
    $("#" + id).removeClass("not-droppable");
    _sorting = false;
    _deleteIcon = true;
    _isWindowCloseFire = true;
}

//sorting on mouse hover method
function SortHover(event, ui) {

    _shouldCancel = true;

    var id = "";
    var getTextBoxId = ui.item[0].children[0].id;
    var textContent = ui.item[0].textContent;

    if ($('#' + getTextBoxId).val() == undefined || $('#' + getTextBoxId).val() == "") {

        id = (((textContent == _customLabel) || (textContent == _customLine)) ? ui.item[0].children[1].id : textContent.lastIndexOf('.') != '-1' ? ui.item[0].children[1].id : ui.item[0].children[0].id);
    }
    else {
        id = ui.item[0].childNodes[2].id;
    }

    //green,yellow and red cross color classes
    $("#" + id).addClass('drag-highlight-green');
    $("#" + id).addClass("not-droppable");
    $("#" + id).removeClass('highlight');

}

//sorting on mouse out method
function SortOut(event, ui) {

    _shouldCancel = false;

    var getTextBoxId = ui.item[0].children[0].id;
    var textContent = ui.item[0].textContent;
    var id = "";

    if ($('#' + getTextBoxId).val() == undefined || $('#' + getTextBoxId).val() == "") {

        id = (((textContent == _customLabel) || (textContent == _customLine)) ? ui.item[0].children[1].id : textContent.lastIndexOf('.') != '-1' ? ui.item[0].children[1].id : ui.item[0].children[0].id);
    }
    else {
        id = ui.item[0].childNodes[2].id;
    }

    $("#" + id).removeClass('drag-highlight-green');

    $("#" + id).addClass("not-droppable");
    $("#" + id).removeClass('highlight');

}


//drop on mouse hover method
function DropHover(event, ui) {

    var columnName = $(ui.helper)[0].textContent;
 
    if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate
        && columnName != _headerLabel && columnName != _customFieldLabel && columnName != _snapCapLabel) {

        _shouldCancel = true;

        //for tab drag
        var helperClass = $(ui.helper)[0].childNodes[1];

        if (helperClass !== undefined && $(ui.helper)[0].childNodes[1].id != 'report-image-madal' && $(ui.helper)[0].childNodes[1].id != 'divCreateEditReportDialog') {
            helperClass.className = '';
            $(ui.helper)[0].childNodes[1].className = 'drag-highlight-green';
        }

        if (helperClass == undefined) {
            //for column drag
            $(ui.helper).removeClass("not-droppable");
            $(ui.helper).addClass("drag-highlight-green");
        }

    }
}

//drop on mouse out method
function DropOut(event, ui) {

    var columnName = $(ui.helper)[0].textContent;

    if (columnName != _showCompanyLogo && columnName != _showCompanyName && columnName != _showReportTitle && columnName != _showCreatedDate
        && columnName != _headerLabel && columnName != _customFieldLabel && columnName != _snapCapLabel) {

        _shouldCancel = false;
        var helperClass = $(ui.helper)[0].childNodes[1];

        //for tab drag
        if (helperClass !== undefined && $(ui.helper)[0].childNodes[1].id != 'report-image-madal' && $(ui.helper)[0].childNodes[1].id != 'divCreateEditReportDialog') {
            $(ui.helper)[0].childNodes[1].className = '';
            $(ui.helper)[0].childNodes[1].className = 'not-droppable';
        }

        if (helperClass == undefined) {
            //for column drag
            $(ui.helper).removeClass("drag-highlight-green");
            $(ui.helper).addClass("not-droppable");

        }
    }
}

// drag and drop columns in between for 1st time
function ChildDroppable(divId) {
   
    $("#" + divId).droppable({        
    
        accept: ":not(.ui-sortable-helper)",

        drop: function (event, ui) {
          
            var element = $("#" + $(this)[0].id);

            if (_columnName == _label)  {
                $($("#" + _customCount)[0].parentElement).insertAfter($($("#" + $(this)[0].id)[0].parentElement));
            }
            else
                $($("#" + _tempDropId)[0].parentElement).insertAfter($($("#" + $(this)[0].id)[0].parentElement));
            
            //update arraylist
            AddEditItemNames(event, ui);

        }

    });

}



//Format selected text
function FormatText(id) {


    if (!_isFormatFalse) {
        _selectedLabelId = "";

        if (id == 'spnReportTitle') {
            $("#" + id).addClass('selectText')
            _selectedLabelId = id;
            var getId = $("#" + id)[0];
        }
        else {

            if (_selectedLabelId != "")
                $($("#delete" + _selectedLabelId)[0].parentNode)[0].className = "ui-droppable ";

            $($("#delete" + id)[0].parentNode).addClass('selectText');
            _selectedLabelId = id;
            var getId = $($("#delete" + id)[0].parentNode)[0];
        }
        if (getId != undefined) {
            var getSelectedStyles = [getId.style.fontWeight, getId.style.fontStyle, getId.style.textDecoration];
            var getUnselectedStyles = ['bold', 'italic', 'underline'];

            if (getSelectedStyles != []) {
                for (var i = 0; i < getSelectedStyles.length; i++) {
                    (getSelectedStyles[i] != "" ? $('#div' + getSelectedStyles[i])[0].style.border = '1px solid #1B268B' : $('#div' + getUnselectedStyles[i])[0].style.border = '');

                }
            }
        }
    }
    _isFormatFalse = false;
}


//insert text box on click of Custom Label
function InsertTextBox(labelId, textboxId) {

    if (!_deleteIcon)
        var customDataName = $('#' + labelId).attr('data-columnname');//get the name of the column(Line and Image)

    if (!_deleteIcon && customDataName != _customSmallImage && customDataName != _customMediumImage && customDataName != _customLargeImage && customDataName != _line && customDataName != undefined)//if Line or Image avoid insert text box.
    {
        $('#' + labelId).hide();
        $('#txt' + textboxId).show();
        $('#txt' + textboxId).focus();
        $('#txt' + textboxId).removeClass('not-droppable');
        $('#txt' + textboxId).addClass('selectText');
        _selectedLabelId = labelId;

        if ($('#' + labelId)[0] != undefined)
            $('#txt' + textboxId).val($('#' + labelId)[0].textContent);

        $('#txt' + textboxId).keypress(function (ev) {

            if (ev.which === 13) {
                ev.preventDefault(labelId, textboxId);
                RemoveTextBox(labelId, textboxId);
            }
        });

        $('#txt' + textboxId).bind("enterKey", function (e) {

            e.preventDefault();
            RemoveTextBox(labelId, textboxId);
        });

    }

    _deleteIcon = false;//avoid on click of cross icon it was firing this method
}

//on blur of textbox show Custom Label
function RemoveTextBox(labelId, textboxId) {

    //get the value from textbox and set to Custom Label
    var getTextBoxValue = $('#txt' + textboxId).val();
    var getLabelName = $('#' + labelId)[0].innerText;

    $('#' + labelId).show();
    $('#txt' + textboxId).hide();

    //if drag and drop textbox and dont have value then delete it.
    if (getTextBoxValue.trim() == "")
        $($('#txt' + textboxId)[0].parentNode).remove();

    if (getTextBoxValue.trim() != "") {

        //adding span coz while updating div value it is not adding span control  (cross icon)
        $('#' + labelId)[0].innerHTML = getTextBoxValue + "<span id='delete" + textboxId + "'  onClick=\'javascript:DeleteSelectedColumn(" + textboxId + "," + _customCount + ")\; return false;' >";
    }

    //after edit the text in text box update arraylist
    AddEditItemNames(null, null);

}


//drag and drop functionality for Tab(s) 
function DragDropTabs() {

    //drag and drop full tab-accordian
    $("#divAccordian h2").draggable({

        appendTo: "body",
        tolerance: 'fit',
        autoHeight: false,

        // containment: 'parent', // sets to can be dragged only within "#reportArea" element
        helper: "clone",//get the name of column name while drag starts to ends


        start: function (event, ui) {

        
                if ($(this)[0].innerText != _headerLabel && $(this)[0].innerText != _customFieldLabel) {
                    $("#reportArea").droppable({ disabled: false });

                    //given dashed border 
                    var anchorTagId = $(this)[0].childNodes[1].id;

                    $("#" + anchorTagId).addClass('drag-highlight');

                    //give red background when it starts drag           
                    $(ui.helper)[0].childNodes[1].className = 'not-droppable';


             
            }
        },

        // when dragging stops
        stop: function (event, ui) {
          

                if ($(this)[0].innerText != _headerLabel && $(this)[0].innerText != _customFieldLabel && $(this)[0].textContent != _snapCapLabel) {

                    //remove dashed border -yellow and gray dashed lines
                    var anchorTagId = $(this)[0].childNodes[1].id;
                    $("#" + anchorTagId).removeClass('drag-highlight');
                    $(ui.helper)[0].childNodes[1].className = '';
                    _isWindowCloseFire = true;
             
            }
        }

    });
}

function DragDropCustomControls() {

    $(".customControl").draggable({

        containment: '#reportArea', // sets to can be dragged only within "#reportArea" element
        //  cursor: 'move',
        tolerance: 'fit',

        // get the initial X and Y position when dragging starts
        start: function (event, ui) {

            xpos = ui.position.left;
            ypos = ui.position.top;

            $("#reportArea").droppable({ disabled: true });
            $("#reportArea").removeClass("ui-droppable-disabled ui-state-disabled");


        },
        // when dragging stops
        stop: function (event, ui) {

            // calculate the dragged distance, with the current X and Y position and the "xpos" and "ypos"
            var xmove = ui.position.left - xpos;
            var ymove = ui.position.top - ypos;

            // define the moved direction: right, bottom (when positive), left, up (when negative)
            var xd = xmove >= 0 ? ' To right: ' : ' To left: ';
            var yd = ymove >= 0 ? ' Bottom: ' : ' Up: ';

            alert('The DIV was moved,\n\n' + xd + xmove + ' pixels \n' + yd + ymove + ' pixels');
        }
    });
}

//start element for drag
function DragStartElements(event, ui) {

    $("#reportArea").droppable({ disabled: false });
    var id = ui.helper[0].children[0].id;

    //given dashed border 
    $("#" + id).addClass('drag-highlight');

    //give red background when it starts drag
    $(ui.helper).addClass("not-droppable");
}

//stop element for drag
function DragStopElements(event, ui) {

    //remove dashed border -yellow and gray dashed lines
    $(this).removeClass('drag-highlight');
}

var _arrEditLabelElements;
var _arrEditFieldElements;
var _arrEventTableColumns = ['EventTime', 'EventDate', 'DateModified', 'DateCreated', 'Name', 'Location', 'Classification'];



//create/update values from items
function AddEditItemNames(event, ui) {

    var countColumnLength = 0;
    _arrLabelElements = [];
    _arrFieldElements = [];
    _arrEditLabelElements = [];
    var elementStyle = '';
    var integrateCount = 0;

    var margin = ';margin:5px 0px 0px 5px;';
    $('#rptItems div').each(function (e) {
        var currentId = 0;
        if ($(this)[0].id.indexOf('space') != 0) {
            currentId = $(this)[0].id;

        }

        countColumnLength++;//for sort order

        // var id = $(this)[0].id;
        var id = currentId;
        var columnName = '';
        var tapRooTElementType = 'Integrate';
        var integrateTypeValue = '';

        if (id != '') {
           
            //format in [Label,columnName,sortOrder,TabName] and [Custom/Data Field,'',sortOrder,TabName] way in 2 arrays            
            var tabName = $('#' + currentId).data("tabname");
          
            var isStaticColumn = $('#' + currentId).data("static");
            var snapCapImageId = $('#' + currentId).data("snapimageid");
       
            if (snapCapImageId == undefined)
                snapCapImageId = 0;

            var elementype = ((tabName == "Images") ? "Image" : (tabName == _tabNameItem) ? tapRooTElementType : (tabName == _tabNameSingleItem) ? 'Stand Alone' : (isStaticColumn || tabName != _tabName) ? 'Custom Field' : 'Data Field');

            if (isStaticColumn && tabName == _tabName)
                tabName = 'Details';
            
            integrateTypeValue = ((elementype == tapRooTElementType) ? arrIntegrateTypes[integrateCount] : "");
            
            if ((integrateTypeValue != undefined && integrateTypeValue != '') ? integrateCount++ : '');
          
            columnName = ((tabName == _tabNameItem) ? tapRooTElementType : $('#' + currentId).data("columnname"))

            var editcolumn = $('#' + currentId).data("edit");
           
            //for label and line
            if (tabName == _customControlField) {

                switch (columnName) {

                    case _label:
                        columnName = $('#' + currentId)[0].textContent;
                        elementype = _label;
                        break;

                    case _line:
                        elementype = _line;
                        break;
                    case _customSpacer:
                        elementype = _line;
                        break;
                    case _customPageBreak:
                        elementype = _line;
                        break;

                }
            }

            if (_columnName == _customSmallImage || _columnName == _customMediumImage || _columnName == _customLargeImage) {

                _selectedSortOder = '';
                _selectedSortOder = countColumnLength;//image
            }
          
            var elementStyle = $('#' + currentId)[0].style.cssText;
            if (elementStyle.indexOf(',') != '-1')
                elementStyle = elementStyle.replace(/,/g, '!');

            //insert
            if (!editcolumn) {
                elementStyle = elementStyle + margin;
                _arrLabelElements.push('Label,' + columnName + ',' + countColumnLength + ',' + tabName + ',' + elementStyle.trim() + ',' + integrateTypeValue + ',' + snapCapImageId);
                _arrFieldElements.push(elementype + ', ,' + countColumnLength + ',' + tabName + ',' + elementStyle + ',' + integrateTypeValue + ',' + snapCapImageId);//sending blank value for Element Text-if we send null then it is storing as string value

            }
            else//edit
            {
                var elementId = $('#' + currentId).attr("data-elementid");
                _arrEditLabelElements.push(_reportId + ', ' + elementId + ', ' + elementStyle.trim() + ', ' + countColumnLength + ', ' + integrateTypeValue + ',' + snapCapImageId);
            }

            //on drag of Image column pass Report Id 
            var reportId = 0;
            if (_reportId == "") {

                _reportId = reportId;
            }

            $('#ifReportImage')[0].src = _baseURL + "tap.usr/UploadReportImage.aspx?SortOrder=" + _selectedSortOder + "&ReportId=" + _reportId;
        }
    });
    
    //After creating div's in Report Area- rpt items hover method
    ReportItemsDivHover();
}


var _isFormatFalse = false;
//delete Dropped column
function DeleteSelectedColumn(deleteId, divId) {

    _deleteIcon = true;
    _isFormatFalse = true;

    var intregrateClass = $($("#delete" + deleteId)[0].parentNode.parentElement)[0].className;
    //if (intregrateClass == 'ClearAll showTapRootIntegrate hide' || intregrateClass == 'ClearAll showTapRootIntegrate')
    //    _isIntegrateDisplay = false;

    $($("#delete" + deleteId)[0].parentNode.parentElement).remove();

    AddEditItemNames(null, null);

    if (_elementId != undefined && _elementId != '') {

        var serviceURL = _baseURL + 'Services/CustomReport.svc/DeleteImageData';
        var imageElementData = new ImageElementDataDetails(_elementId, _reportId);
        var data = JSON.stringify(imageElementData);
        AjaxPost(serviceURL, data, eval(DeleteImageDataSuccess), eval(DeleteImageDataFail));
    }


}

_tabNameItem1 = 'Items11';
//After creating div's in Report Area- rpt items hover method
function ReportItemsDivHover() {
    var currentId = 0;

    $("#rptItems div").hover(function () {
        currentId = 0;

        if ($(this)[0].id.indexOf('space') != 0) {
            currentId = $(this)[0].id;
        }

        $('#' + currentId).addClass('highlight');
        var tabName = $('#' + currentId).attr('data-tabname');
        _elementId = $('#' + currentId).attr('data-elementid');

        //take delete id and apply cross icon-mouse hover
        if (!_sorting && currentId != 0) {

            //ternary condition to get TapRooT Item or normal columns
            var divId = ((tabName == _tabNameItem || tabName == _tabNameItem1) ? $('#' + currentId)[0].children[8].id : $('#' + currentId)[0].children[0].id);

            $("#" + divId).addClass("imageDeleteControl");
        }

    }, function () {
        if (currentId != 0) {
            $('#' + currentId).removeClass('highlight');
            var tabName = $('#' + currentId).attr('data-tabname');

            //take delete id and remove cross icon-on mouse out 
            var divId = ((tabName == _tabNameItem || tabName == _tabNameItem1) ? $('#' + currentId)[0].children[8].id : $('#' + currentId)[0].children[0].id);

            $("#" + divId).removeClass("imageDeleteControl");
        }
    });

}

function ImageElementDataDetails(elementId, reportId) {
    this.elementId = _elementId;
    this.reportId = _reportId;
}

function DeleteImageDataSuccess(result) {


}

function DeleteImageDataFail(msg) {
    alert('Service Failed.');
}

//get Report Title Details
function GetReportName(isReportName) {

    var reportType = '';
    var reportName = $('#txtReportName')[0].value.trim();
    var reportDescription = $('#txtReportDescription').val().trim();

    var reportTitle = $('#spnReportTitle')[0].innerHTML;
    var reportTitleStyle = $('#spnReportTitle')[0].style.cssText;

    if (reportTitle == _reportTitleName)
        reportTitle = '';
  
    if (_isEdit)//if check box selects event to user 
        reportType = _editReportType;
    else
        reportType = ((_reportType == '1') ? _eventReport : (_reportType == '2') ? _userReport : _systemReport);


    var isCompanyLogo = ($('#chkShowCompanyLogo').attr('checked') == "checked" ? true : false);
    var isCompanyName = ($('#chkShowCompanyName').attr('checked') == "checked" ? true : false);
    var isReportTitle = ($('#chkShowReportTitle').attr('checked') == "checked" ? true : false);
    var isDateCreated = ($('#chkShowDateCreated').attr('checked') == "checked" ? true : false);


    var isUserToSystemReport = false; 
 
    if (ReportValidation(reportName, isReportName)) {
        // Reset the values of array label and fields
        CheckReportIssuesForUndefined();
        
        var elementData = new ElementDataDetail(reportName, reportDescription, _arrLabelElements, _arrFieldElements, reportTitle, isCompanyLogo,
                                                isCompanyName, isReportTitle, isDateCreated, isUserToSystemReport, _isTempReport, reportTitleStyle, reportType);


        var data = JSON.stringify(elementData);
        _serviceUrl = _baseURL + 'Services/CustomReport.svc/SaveReportTemplate';

        //used async: false -need to make it Generic method            
        PostAjax(_serviceUrl, data, eval(SaveReportTemplateSuccess), eval(SaveReportTemplateFail));
        
        CloseTemplatePopup();
        _isWindowCloseFire = false;
    }

}

function ReportValidation(reportName, isReportName) {

    var txtReportName = 'txtReportName';
    var errorMessage = '';
    var errorMessageType = 'jError';

    if (reportName == null && reportName.length == 0 || (reportName == "" && _isTempReport == false)) {
        errorMessage = 'Please enter Report Name.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        ShowHideWarningMessage('txtReportName', 'Please specify Report Name', true);
        return false;
    }
    else
        ShowHideWarningMessage('txtReportName', 'Please specify Report Name', false);

    var isReportColumnsExists = $('#reportArea')[0].innerHTML.trim();
   
    if (isReportColumnsExists == "" || isReportColumnsExists == undefined) {
        errorMessage = 'Please drag and drop any columns to create Report.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }

    return true;
}


//Save Report
function SaveReportTemplate() {

    //validate Columns and labels -if true then Open Report Title madal
    if (ValidationReportTemplate(_arrLabelElements, _arrFieldElements)) {
        OpenTemplatePopup();
    }
}

//Report Template parameters
function ElementDataDetail(reportName, reportDescription, arrLabelElements, arrFieldElements, reportTitle, isCompanyLogo,
                            isCompanyName, isReportTitle, isDateCreated, isUserToSystemReport, isTempReport, reportTitleStyle, reportType) {

    var companyId = $('#hdnCompanyId').val();
    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        companyId = divisionId;
    
    if (_reportId == "")
        _reportId = 0;

    if (_arrEditLabelElements == undefined)
        _arrEditLabelElements = ['0', '0', '0'];

    this.reportId = _reportId;
    this.userID = $('#hdnUserId').val();
    this.companyID = companyId;
    this.reportName = reportName;
    this.reportDescription = reportDescription;
    this.arrLabelElements = arrLabelElements;
    this.arrFieldElements = arrFieldElements;
    this.reportTitle = reportTitle;
    this.isCompanyLogo = isCompanyLogo;
    this.isCompanyName = isCompanyName;
    this.isReportTitle = isReportTitle;
    this.isDateCreated = isDateCreated;
    if (_eventId == null || _eventId == '')
    {
        _eventId = $('[id$=hdnEventId]').val();
    }
    
    this.eventID = _eventId;
    this.isUserToSystemReport = isUserToSystemReport;
    this.isTempReport = isTempReport;
    this.arrEditLabelElements = _arrEditLabelElements;
    this.reportTitleStyle = reportTitleStyle;
    this.reportType = reportType;
}

//Report Template Success method
function SaveReportTemplateSuccess(result) {
    if (result.d != '') {

        _reportId = result.d[0];

        if (!_isTempReport) {
            _message = 'Report saved successfully.'
            _messageType = 'jSuccess';
            NotificationMessage(_message, _messageType, true, 1000);
        }
       
        try {
            //refresh parent page to get saved report- //dispaly Report Titles-on click of each link -need to display report values(Generate Report)
            if (!_isTempReport) {
                if (typeof window.opener.ReloadReportListGrid == 'function')
                    window.opener.ReloadReportListGrid();
                if (typeof window.opener.DisplayReportTitle == 'function')
                    window.opener.DisplayReportTitle();
            }
        } catch (e) { }
        
    }

    //preview Report -if isTempReport is false then open generate report page.
    if (_isTempReport) {
        if (_isEdit)
            var url = _baseURL + 'CustomReportsData/Reports/' + _eventId + '/' + _reportId + '/' + _reportType + '/' + false;
        else
            var url = _baseURL + 'CustomReportsData/Reports/' + _eventId + '/' + _reportId + '/' + _reportType + '/' + true;

        var currentLocation = location;
        url = _baseURL + "Report/View/" + _eventId + "/" + _reportId;
        newwindow = window.open(url, "_blank", "directories=no, status=no, resizable=no, width=1200, height=750, top=250, left=120, scrollbars=1, toolbar=no, menubar=no");

        if (window.focus) {
            newwindow.focus();
        }
    }

    var eventType = ((result.d[1] == _eventReport) ? '1' : (result.d[1] == _userReport ? '2' : '3'));


    if (_type != _editType) {
        //once save -change to edit mode
        window.location.href = _baseURL + 'CustomReports/Edit/' + _module + '/' + eventType + '-' + _subTab + '/' + _reportId;

    }
    else {
        EditColumns();

    }


}

//service fail method
function SaveReportTemplateFail(msg) {
    alert('Service method failed.');
}

//validation for Report Template
function ValidationReportTemplate(arrLabelElements, arrFieldElements, reportTitle) {

    var errorMessage = '';
    var errorMessageType = 'jError';

    var isReportColumnsExists = $('#reportArea')[0].innerHTML.trim();
   
    if (isReportColumnsExists == "" || isReportColumnsExists == undefined) {
        errorMessage = 'Please drag and drop any columns to create Report.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }

    if (errorMessage != '') {
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
    return true;

}

//Open Report Title madal
function OpenTemplatePopup() {
  
    //pre populate Report Title in Report Name.
    var reportTitle = $('#spnReportTitle')[0].innerHTML;

    if (reportTitle != _reportTitleName && (!_isEdit))
        $('#txtReportName')[0].value = reportTitle;
  //  if (reportTitle != _reportTitleName)
     

    var systemType = '3';
    var madalHeight = ((_module == 'Admin' && _eventId == 0) ? 250 : 300);
    var madalWidth = ((_module == 'Admin' && _eventId == 0) ? 320 : 350);

    $("#divCreateEditReportDialog").dialog({
        width: madalWidth,
        height: madalHeight,
        modal: true,
        draggable: false,
        resizable: false,

    });

    $('#divCreateEditReportDialog').show();
    $("#dialog:ui-dialog").dialog("destroy");
}

function OpenImageDialog() {

    $('#report-image-madal').show();
    $("#dialog:ui-dialog").dialog("destroy");

    $("#report-image-madal").dialog({

        height: 200,
        modal: true,
        draggable: false,
        resizable: false,

    });

}

//open TapRooT Integration Type option madal popup
function OpenIntegrateDialog() {

    $('#rdCausalFactor')[0].checked = true;
    $('#rdRootCause')[0].checkBoxId = false;
    $('#rdActionPlan')[0].checked = false;


    $('#divReportIntegrateDialog').show();
    $("#dialog:ui-dialog").dialog("destroy");

    $("#divReportIntegrateDialog").dialog({

        height: 270,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 200],
        close: function (event, ui) {
            CloseIntegratePopup();
        }

    });
}

var _integrateType = '';

var arrIntegrateTypes = [];
function GetIntegrateOptionDesign() {
    
    $('.showTapRootIntegrate').show();

    var isCausalFactor = ($('#rdCausalFactor').attr('checked') == "checked" ? true : false);
    var isRootCause = ($('#rdRootCause').attr('checked') == "checked" ? true : false);
    var isActionPlan = ($('#rdActionPlan').attr('checked') == "checked" ? true : false);

    _integrateType = ((isCausalFactor == true) ? 'Causal Factor' : (isRootCause == true) ? 'Root Cause' : 'Action Plan');
    
    arrIntegrateTypes.push(_integrateType);

    //update the integrate option selection value with delete span control
    $('#' + _tempDropId)[0].innerHTML = 'TapRooT® Summary by ' + _integrateType + '<br><span class="paddingLeft55"> - Causal Factors</span><br><span class="paddingLeft55"> - Root Causes</span><br><span class="paddingLeft55"> - Action Plans</span><br><span class="paddingLeft55"> - Tasks</span>'
        + " <span id='delete" + _spanCount + "' onClick=\'javascript:DeleteSelectedColumn(" + _spanCount + "," + _customCount + ")\; return false;'></span>";

    AddEditItemNames(null, null);

    //close the integrate popup madal
    $('#divReportIntegrateDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divReportIntegrateDialog").dialog("destroy");
    $('#divReportIntegrateDialog').bind("dialogclose", function (event, ui) {
    });

}

//on image dialog close - remove the rptitems div- gap is coming
$('#report-image-madal').bind('dialogclose', function (event) {

    $($("#div" + _customCount)[0].parentNode).remove();

});

//close Report Title Popup
function CloseTemplatePopup() {


    $('#divCreateEditReportDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCreateEditReportDialog").dialog("destroy");
    $('#divCreateEditReportDialog').bind("dialogclose", function (event, ui) {
    });

}

function CloseIntegratePopup() {
    //delete already created integrate div
    DeleteSelectedColumn(_spanCount, _customCount);

    $('#divReportIntegrateDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divReportIntegrateDialog").dialog("destroy");
    $('#divReportIntegrateDialog').bind("dialogclose", function (event, ui) {
    });
}

//close the madal popu of Created Report 
function CloseImageDialogBox() {

    $($("#div" + _customCount)[0].parentNode).remove();

    $('#report-image-madal').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#report-image-madal").dialog("destroy");
    $('#report-image-madal').bind("dialogclose", function (event, ui) {
    });
}

function askConfirm() {
    var isPageRedirect = true;
   
    var isReportColumnsExists = $('#reportArea')[0].innerHTML.trim();
    //debugger;
    if (!_isWarningmessageShow && isReportColumnsExists != "" && isReportColumnsExists != undefined) {
       return confirm("Are you sure you want to leave this page? \r\n You will lose the changes you made to this report. \r\n Click OK to leave this page, and click Cancel to stay on this page.");
     
    }

   // return isPageRedirect;
}

//back to main Report Page
function GoBackToMainReportPage() {
  
    window.onbeforeunload = function (e) {

        var e = e || window.event;
        var isReportColumnsExists = $('#reportArea')[0].innerHTML.trim();

        if (isReportColumnsExists == "" || isReportColumnsExists == undefined) {
            _isWindowCloseFire = false;
        }

        //if ($('#txtRptTitle').val().length > 0)
        //    _isWindowCloseFire = true;
        
        if (_isWindowCloseFire) {
            // For IE and Firefox prior to version 4
            if (e) {
                if (_isTempReportDelete && !(_isPreviewReportDelete)) {
                    DeletePreviewReportData();
                }
                if (_isWindowCloseFire) {
                   
                    e.returnValue = 'You will lose the changes you made to this report.';
                }
                e.returnValue = 'You will lose the changes you made to this report.';
            }
            if (_isTempReportDelete && !(_isPreviewReportDelete)) {
                DeletePreviewReportData();
            }
            
            // For Safari
            return 'You will lose the changes you made to this report.';
        }

    };
  
}

function DeletePreviewReportData() {

    var reportData = new ReportDeleteDataDetails();
    var data = JSON.stringify(reportData);

    _serviceUrl = _baseURL + 'Services/CustomReport.svc/DeletePreviewReportData';

    //used async: false -need to make it Generic method  
    PostAjax(_serviceUrl, data, eval(DeletePreviewReportDataSuccess), eval(DeletePreviewReportDataFail));

}



function ReportDeleteDataDetails() {
    var deleteData = {};
    deleteData.eventID = _eventId;
    deleteData.reportId = _reportId;
    return deleteData;
}

//Preview Report Data success function
function DeletePreviewReportDataSuccess(result) {
  
   
}

//service fail
function DeletePreviewReportDataFail() {
    alert('Service Failed.');
}


////before redirect to Main Report Page check any columns drag and dropped-give confirmation box
//function ConfirmToRedirect(arrLabelElements, arrFieldElements) {

  
//}

var _type = '';
var _editType = 'Edit';
//get EventId from URL
function GetIdsFromUrl() {

    var createType = 'Create';
  
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _type = splitUrl[splitUrl.length - 4];
    _subTab = splitUrl[splitUrl.length - 2];

    _reportType = _subTab.substring(0, 1);
    _subTab = _subTab.substring(_subTab.lastIndexOf('-') + 1);
    _module = splitUrl[splitUrl.length - 3];

    //if reportType is event Report ie '1' else for System and User Report it should be display as Template
    document.title = _reportType == '1' ? 'Report Builder' : 'Template Builder';
    
    (_reportType == '1' ? $('#lblReportName')[0].innerHTML = 'Report Name' : $('#lblReportName')[0].innerHTML = 'Template Name');

    if (_type == createType) {
        _eventId = splitUrl[splitUrl.length - 1];

        if (_eventId.lastIndexOf('#') != '-1') {
            _eventId = _eventId.replace('#', '');
        }

        //  for user report event id will be null - once retrive am making null value
        $.cookie("EventId", _eventId, { expires: 30, path: '/' });

        //only if user come from Report Sub
        if (_eventId != 0) {

            $('#btnBackToMainReport').show();
        }
        //if reportType is event Report ie '1' else for System and User Report it should be display as Template
        (_reportType == '1' ? $("#divCreateEditReportDialog").attr("title", "Report") : $("#divCreateEditReportDialog").attr("title", "Template"));

        (_reportType == '1' ? $('#btnReportName').val('Create Report') : $('#btnReportName').val('Create Template'));



    }
    else if (_type == 'Edit') {

        $('#btnReportName').val('Apply Changes');

        //if reportType is event Report ie '1' else for System and User Report it should be display as Template
        (_reportType == '1' ? $("#divCreateEditReportDialog").attr("title", "Edit Report") : $("#divCreateEditReportDialog").attr("title", "Save Template"));

        _reportId = splitUrl[splitUrl.length - 1];
        if (_reportId.lastIndexOf('#') != '-1') {

            _reportId = _reportId.replace('#', '');
        }
        _isEdit = true;
    }
}


function CheckReportIssuesForUndefined()
{
    
    var data1, data2;
    var actualIntegrateType = $('#reportArea > div').not('.dropBreak');
    var j = 0, data, columnName = '';
    var countColumnLength = 0;
    _arrEditLabelElements = [];
    _arrFieldElements = [];
    _arrLabelElements = [];
    var margin = ';margin:5px 0px 0px 5px;';
    
    var tapRooTElementType = 'Integrate';
    
    for (var i = 0; i < actualIntegrateType.length; i++) {
        
        var isStaticColumn = $(actualIntegrateType[i]).find('div:eq(0)').data("static");
        data = $(actualIntegrateType[i]);
        var tabName = $(actualIntegrateType[i]).find('div:eq(0)').data("tabname");
        var style = $(actualIntegrateType[i]).find('div:eq(0)').attr('style');

        var elementype = ((tabName == "Images") ? "Image" : (tabName == _tabNameItem) ? tapRooTElementType : (tabName == _tabNameSingleItem) ? 'Stand Alone' : (isStaticColumn || tabName != _tabName) ? 'Custom Field' : 'Data Field');
        columnName = ((tabName == _tabNameItem) ? tapRooTElementType : $(actualIntegrateType[i]).find('div:eq(0)').data("columnname"))
        if (tabName == 'Items1')
            var integrateType = $(actualIntegrateType[i]).text().split('-')[0].split('by')[1].trim();
        
        var snapCapImageId = $(actualIntegrateType[i]).find('div:eq(0)').data("snapimageid");
        if (snapCapImageId == undefined)
            snapCapImageId = 0;

        //if (isStaticColumn && tabName == _tabName)
        //    tabName = 'Details';

        var elementStyle = $(actualIntegrateType[i]).find('div:eq(0)')[0].style.cssText;
        if (elementStyle.indexOf(',') != '-1')
            elementStyle = elementStyle.replace(/,/g, '!');

        // Declare currentId
        var currentId = 0;
        if ($(actualIntegrateType[i]).find('div:eq(0)')[0].id.indexOf('space') != 0) {
            currentId = $(actualIntegrateType[i]).find('div:eq(0)')[0].id;
        }

        //for label and line
        if (tabName == _customControlField) {

            switch (columnName) {

                case _label:
                    columnName = $('#' + currentId)[0].textContent;
                    elementype = _label;
                    break;

                case _line:
                    elementype = _line;
                    break;
                case _customSpacer:
                    elementype = _line;
                    break;
                case _customPageBreak:
                    elementype = _line;
                    break;

            }
        }

        if (_columnName == _customSmallImage || _columnName == _customMediumImage || _columnName == _customLargeImage) {

            _selectedSortOder = '';
            _selectedSortOder = countColumnLength;//image
        }
        countColumnLength++;
        //alert(tapRooTElementType + ', ,' + countColumnLength + ',' + tabName + ',' + elementStyle + ',' + integrateType + ',' + snapCapImageId);

        var editcolumn = $(actualIntegrateType[i]).find('div:eq(0)').data("edit");
        //insert
        if (!editcolumn) {
            elementStyle = elementStyle + margin;
            _arrLabelElements.push('Label,' + columnName + ',' + countColumnLength + ',' + tabName + ',' + elementStyle.trim() + ',' + integrateType + ',' + snapCapImageId);
            _arrFieldElements.push(elementype + ', ,' + countColumnLength + ',' + tabName + ',' + elementStyle + ',' + integrateType + ',' + snapCapImageId);//sending blank value for Element Text-if we send null then it is storing as string value

        }
        else//edit
        {
            var elementId = $(actualIntegrateType[i]).find('div:eq(0)').attr("data-elementid");
            _arrEditLabelElements.push(_reportId + ', ' + elementId + ', ' + elementStyle.trim() + ', ' + countColumnLength + ', ' + integrateType + ',' + snapCapImageId);
        }
    }
    
}








