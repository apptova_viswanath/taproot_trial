﻿
// Page to create and edit System Notifications messages
// by admin
var _companyId = '';
var _dateFormat = '';
var _userId = '';
var _svcURL = '';
var _data = '';
var dateDisplayFormat = '';


$(document).ready(function () {    

    //page load method
    PageLoadAction();    

    // If "Create new notificaiton' is clicked, set SysId to -1
    // So that the Popup will be opened creation mode
    $('#lnkCreateNewNotification').click(function () {
      
        //Open Home Message Popup madal dialog
        OpenHomeMessagePopup();
        return false;
    });

    $('#btnSaveNofication').click(function () {

        SaveSysNotifications();
    });

    $('#btnCancelNotification').click(function () {

        ClosePopUp();
    });

    $('#btnDeleteNotification').click(function () {

        DeleteSysNotification();
    });

 
});

//page load methods
function PageLoadAction()
{
    //assigning the DatePicker to the DateFields
    var dateSelectors = "#txtFromDate, #txtToDate";

    DataBindToGrid();

    _companyId = GetCompanyId();
    
    $("#txtFromDate").datepicker({ dateFormat: localStorage.dateFormat });
    $("#txtToDate").datepicker({ dateFormat: localStorage.dateFormat });
    $("#ui-datepicker-div").addClass("notranslate");
}

//Open Add Home Message Popup madal dialog
function OpenHomeMessagePopup()
{
    ClearNotificationFields();
    ResetAllWarnings();
    $('#chkActiveSystemNotification').prop('checked', true);
    $('#txtSysID').val(0);
    OpenPopup(0);
}

// Function to open create/edit notification Popup dialog
function OpenPopup(data) {
   
    // Set Popup title based on the parameter
    var pageTitle = '';

    if (data == 0) {

        pageTitle = 'Add Message';//'Create new notification';
        $('#btnDeleteNotification').hide();
        $('#btnSaveNofication')[0].parentElement.className = 'buttonCenter';
        $('#btnDeleteNotification')[0].parentElement.className = 'buttonCenter';
        $('#btnCancelNotification')[0].parentElement.className = 'buttonCenter';
        $('#btnSaveNofication').val('Create');
    }
    else {
      
        pageTitle = 'Edit Message';
        $('#btnDeleteNotification').show();    
        $('#btnSaveNofication').val('Apply Changes');
    }

    //// Compose url to be called based on the SysId
    //// If the SysID is 0, then Popup will be opened in creation mode
    //// If not, then Popup will be opened in edit mode
    JqueryPopUp(pageTitle, 'divHomeMessageDialog');
}

// Function to create grid and bind data
function DataBindToGrid() {

    var $grid = $("#NotificationsGrid"), gridcol;
    jQuery("#NotificationsGrid").jqGrid({

        datatype: function (pdata) { NotificationData(pdata); },
        colNames: ['Title', 'Message', 'From Date', 'To Date', 'SysID'], //define column names
        colModel: [{ name: 'Title', index: 'Title', width: 70, align: 'left', sortable: true, formatter: TextFormatter, resizable: true },
                        { name: 'Message', index: 'Message', width: 80, align: 'left', sortable: true, resizable: true },
                        { name: 'FromDate', index: 'FromDate', width: 40, align: 'center', sortable: true, resizable: true },
                        { name: 'ToDate', index: 'ToDate', width: 40, align: 'center', sortable: true, resizable: true },
                        { name: 'SysID', index: 'SysID', width: 40, hidden: true, align: 'center', sortable: true, resizable: true },
        ],
        toppager: false,
        emptyrecords: 'There are no messages.',
        // Grid total width and height  
        autowidth: true,
        height: '100%',
        caption: 'Home Page Messages',
        rowNum: 50,
        hidegrid: false,
        pager: jQuery('#pageNavigation'),
        viewrecords: true,
        sortname: 'ToDate',
        sortorder: "desc",
        forceFit: true,
      
        loadComplete: function () {

            jqgridCreatePager('pageNavigation', 'NotificationsGrid', 5);
        },
        onSelectRow: function (ids) {

            // Get the SysId and store it in the hidden field
            // So that it can be used by Popup for saving data
            var grid = jQuery('#NotificationsGrid');
            var sel_id = grid.jqGrid('getGridParam', 'selrow');
            var myCellData = grid.jqGrid('getCell', sel_id, 'SysID');            
            $('#txtSysID').val(myCellData);

        },
        ondblClickRow: function (rowId) {

            var rowData = jQuery(this).getRowData(rowId);
            var myCellData = rowData["SysID"];
            $('#txtSysID').val(myCellData);

            $('#btnSaveNofication')[0].parentElement.className = 'buttonEdit';
            $('#btnDeleteNotification')[0].parentElement.className = 'buttonDelete';
            $('#btnCancelNotification')[0].parentElement.className = 'buttonCancel';

            EditNotification(myCellData);
        }
    });

    jQuery("#NotificationsGrid").click(function (e) {
        var el = e.target;
        var row = $(el, this.rows).closest("tr.jqgrow");
        if ($(el).index() == 0) {
            $('#txtSysID').val(row[0].id);

            $('#btnSaveNofication')[0].parentElement.className = 'buttonEdit';
            $('#btnDeleteNotification')[0].parentElement.className = 'buttonDelete';
            $('#btnCancelNotification')[0].parentElement.className = 'buttonCancel';
            EditNotification(row[0].id);
        }
    });

    gridcol = $grid.jqGrid('getGridParam', 'colModel');

   
    $(window).bind('resize', function () {
        $("#NotificationsGrid").setGridWidth($('.NotificationListGrid').width() - 15);
    });

}


function EditNotification(param) {
   
    var pageTitle = 'Edit Message';

    if ($('#txtSysID').val() > 0) {
   
        //clear the current data and load new Data
        ClearNotificationFields();
        ResetAllWarnings();

        $('#chkActiveSystemNotification').prop('checked', true);
  
        GetNotificationByID();
    }
  
}

// Reload Grid
function ReloadGridData() {
    jQuery("#NotificationsGrid").trigger("reloadGrid");
}

function NotificationData(pData) {
    var sysId = 0;
    var NotificaionDetails = new SysNotificationDetails(sysId, pData);
    _data = JSON.stringify(NotificaionDetails);
    serviceUrl = _baseURL + 'Services/SysNotifications.svc/GetSystemNotifications';
    AjaxPost(serviceUrl, _data, eval(GetSystemNotificationsSuccess), eval(GetSystemNotificationsFail));
}

function SysNotificationDetails(sysId, pData) {
    this.page = pData.page;
    this.rows = pData.rows;
    this.sysId = sysId;
    this.sortIndex = pData.sidx;
    this.sortOrder = pData.sord;
    this.companyID = GetCompanyId();
}

function GetSystemNotificationsSuccess(result) {

    var gridId = jQuery("#NotificationsGrid")[0];

    if (result.d != "") {
        gridId.addJSONData(JSON.parse(result.d));
        jqgridCreatePager('pageNavigation', 'NotificationsGrid', 5);
     
    }
    
}

function GetSystemNotificationsFail(result) {
}

function TextFormatter(cellvalue, options, rowObject) {
    var result = "<a href='' onclick='return false'>" + cellvalue + "</a>";
    return result;
}

//Display the jquery pop up
function JqueryPopUp(pageTitle, divID) {

    $('#' + divID).removeClass('seasonContentHide');
    $('#' + divID).addClass('seasonContentShow');
    $("#dialog:ui-dialog").dialog("destroy");

    $('#' + divID).dialog({

        title: pageTitle,
        minheight: 300,
        width: 600,
        modal: true,
        draggable: true,
        resizable: false,
        scrollbar: false,
        position: ['middle', 200],
        open: function (ev, ui) {
            $('#' + divID).css('overflow', 'hidden');
        }
    });

}


function ShowMessageForChild(message, messageType) {
    NotificationMessage(message, messageType, true, 3000);
}

function GetNotificationByID() {
   
    var sysId = $('#txtSysID').val();
    
    var NotificaionDetails = new SysNotificationDetailsByID(sysId);
    _data = JSON.stringify(NotificaionDetails);
    serviceUrl = _baseURL + 'Services/SysNotifications.svc/GetNotificationByID';
    AjaxPost(serviceUrl, _data, eval(GetNotificationByIDSuccess), eval(GetNotificationByIDFail));
}

function SysNotificationDetailsByID(sysId) {
    this.sysId = sysId;
    this.companyId = _companyId;
}

//Convert the json date format to javascript date format 
function ConvertToDate(dateValue) {
    if (dateValue != null) {
        var jsonDate = new Date(parseInt(dateValue.substr(6)));

        return displayDate = $.datepicker.formatDate(localStorage.dateFormat, jsonDate);//change with site date setting 
    }
}


function GetNotificationByIDSuccess(result) {

    if (result.d != '') {

        var jsonData = jQuery.parseJSON(result.d);
        var sysId = jsonData[0].SysID;
        var title = jsonData[0].Title;
        var message = jsonData[0].Message;
        var fromDate = ((jsonData[0].FromDate != null && jsonData[0].FromDate.length > 0) ? (ConvertToDate(jsonData[0].FromDate)) : '');
        var toDate = ((jsonData[0].ToDate != null && jsonData[0].ToDate.length > 0) ? (ConvertToDate(jsonData[0].ToDate)) : ''); 
        var pageTitle = 'Edit Message';
        var active = jsonData[0].Active;

        if (active) {
            $('#chkActiveSystemNotification').prop("checked", true);
        }
        else {
            $('#chkActiveSystemNotification').prop('checked', false);
        }

        $('#txtTitle').val(title);
        $('#txtFromDate').val(fromDate);
        $('#txtToDate').val(toDate);
        $('#txtNotifyMessage').val(message);
        $('#txtSysID').val(sysId);
        
        $('#btnDeleteNotification').show();
        $('#btnSaveNofication').val('Apply Changes');

        var titleId = 'txtTitle';

        ShowHideWarningMessage(titleId, 'Please specify Title Name', false);

        //Display the Home Page edit popup dialog
        JqueryPopUp(pageTitle, 'divHomeMessageDialog');

    }

   
}

function GetNotificationByIDFail(result) {
}

//Method to Set the DateFormat based on Sitesetting
function SiteSettingDateFormat() {

    //get companyId and UserId
    _serviceUrl = _baseURL + 'Services/SiteSettings.svc/GetDateFormat';
    Ajax(_serviceUrl, _data, eval(SiteSettingDateFormatSuccess), eval(SiteSettingDateFormatFail), 'GetDateFormat');
}

//success method for SiteSettingDateFormat
function SiteSettingDateFormatSuccess(result) {
    var dateFormat = '';
    if (result != null) {

        switch (result.d) {

            case 'dd/MM/yyyy':
                dateFormat = 'dd/mm/yy';
                break;

            case 'MM/dd/yyyy':
                dateFormat = 'mm/dd/yy';
                break;

            case 'MMM dd yyyy':
                dateFormat = 'M dd yy';
                break;

            case 'dd-MM-yyyy':
                dateFormat = 'dd-mm-yy';
                break;

            case 'militaryformat':
                dateFormat = 'dd-M-yy';
                break;
            default:
                dateFormat = 'mm/dd/yy';
                break;
        }
        dateDisplayFormat = dateFormat;
    }
    if (dateFormat != '') {
        $("#body_txtFromDate").datepicker({ dateFormat: dateFormat });
        $("#body_txtToDate").datepicker({ dateFormat: dateFormat });
    }
}

//service failed method
function SiteSettingDateFormatFail() {
    alert("Error in loading data.");
}

//Convert the json date format to javascript date format 


function SaveSysNotifications() {

    var sysId = $('#txtSysID').val();
    var title = TrimString($('#txtTitle').val());
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var message = TrimString($('#txtNotifyMessage').val());
    var active = $('#chkActiveSystemNotification').attr('checked') ? 1 : 0;
    
    if (validateDates() && validNotificationDetails(title, fromDate, toDate, message)) {
        var notificationDetails = new notificationDetailsData(title, fromDate, toDate, message, sysId, active);

        _data = JSON.stringify(notificationDetails);
        _serviceUrl = _baseURL + 'Services/SysNotifications.svc/AddSysNotification';
        AjaxPost(_serviceUrl, _data, eval(AddSysNotificationSuccess), eval(AddSysNotificationFailed));
    }
}

function AddSysNotificationSuccess(result) {

    var message = '';
    var title = TrimString($('#txtTitle').val());
    if ($('#txtSysID').val() == 0) {
        message = title + ' message added successfully.';
    }
    else {
        message = title + ' message updated successfully.';
    }
    var messageType = 'jSuccess';
    parent.ReloadGridData();
    ClosePopUp();
    parent.ShowMessageForChild(message, messageType);
}

function AddSysNotificationFailed() {
    alert('Service failed.');
}

function validateDates() {

    var fromDateId = 'txtFromDate';
    var toDateId = 'txtToDate';

    if (Date.parse($('#txtFromDate').val()) > Date.parse($('#txtToDate').val())) {
        ShowHideWarningMessage(fromDateId, 'Please specify Date', true);
        ShowHideWarningMessage(toDateId, 'Please specify Date', true);
        var message = 'TO date cannot be before FROM date';
        var messageType = 'jError';
        NotificationMessage(message, messageType, false, 3000);
        return false;
    }
    else {
        ShowHideWarningMessage(fromDateId, 'Please specify Date', false);
        ShowHideWarningMessage(toDateId, 'Please specify Date', false);
        return true;
    }
}

function validNotificationDetails(title, fromDate, toDate, message) {

    var missingFields = '';

    if (title == '') {
        missingFields = 'Title';
    }

    if (message == '') {
        if (missingFields != '') {
            missingFields = missingFields + ', ' + 'Message';
        }
        else {
            missingFields = 'Message';
        }
    }

    if (missingFields != '') {

        showValidationsError(title, fromDate, toDate, message);

        var message = 'The following field(s) are missing : ' + missingFields;
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
        return false;
    }
    return true;
}

function showValidationsError(title, fromDate, toDate, message) {

    var txtTitle = 'txtTitle';
    var txtFromDate = 'txtFromDate';
    var txtToDate = 'txtToDate';
    var txtMessage = 'txtNotifyMessage';

    if (title == null || title.length == 0) {
        ShowHideWarningMessage(txtTitle, 'Please specify Title Name', true);
    }
    else {
        ShowHideWarningMessage(txtTitle, 'Please specify Title Name', false);
    }

    if (message == null || message.length == 0) {
        ShowHideWarningMessage(txtMessage, 'Please specify Message', true);
    }
    else {
        ShowHideWarningMessage(txtMessage, 'Please specify Message', false);
    }
}

//property of Users class
function notificationDetailsData(title, fromDate, toDate, message, sysId, active) {

    this.title = title;
    this.fromDate = fromDate;
    this.toDate = toDate;
    this.message = message;
    this.sysId = sysId;
    this.active = active;
    this.companyId = _companyId;
}

function ClosePopUp(refresh) {

    parent.$("#divHomeMessageDialog").dialog('close');
}


function OnBlurTitle() {

    var title = $('#txtTitle').val();
    var titleId = 'txtTitle';

    if (title == '') {
        ShowHideWarningMessage(titleId, 'Please specify Title Name', true);
        return 'Title';
    }
    else {
        ShowHideWarningMessage(titleId, 'Please specify Title Name', false);
        return '';
    }
}

function FromDateChanged() {

    var fromDate = $('#txtFromDate').val();
    var fromDateId = 'txtFromDate';

    if (fromDate == '') {
        ShowHideWarningMessage(fromDateId, 'Please specify Date', true);
        return 'From Date';
    }
    else {
        ShowHideWarningMessage(fromDateId, 'Please specify Date', false);
        return '';
    }
}

function ToDateChanged() {

    var toDate = $('#txtToDate').val();
    var toDateId = 'txtToDate';

    if (toDate == '') {
        ShowHideWarningMessage(toDateId, 'Please specify Date', true);
        return 'To Date';
    }
    else {
        ShowHideWarningMessage(toDateId, 'Please specify Date', false);
        return '';
    }
}

function OnBlurMessage() {

    var message = $('#txtNotifyMessage').val();
    var messageId = 'txtNotifyMessage';

    if (message == '') {
        ShowHideWarningMessage(messageId, 'Please specify Message', true);
        return 'Message';
    }
    else {
        ShowHideWarningMessage(messageId, 'Please specify Message', false);
        return '';
    }
}

function DeleteSysNotification() {

    var title = TrimString($('#txtTitle').val());
    var result = confirm("Are you sure you want to delete " + title + " ?");
    if (result != true) {
        return;
    }
    var sysId = $('#txtSysID').val();

    var notificationDetails = new notificationID(sysId);
    _data = JSON.stringify(notificationDetails);
    _serviceUrl = _baseURL + 'Services/SysNotifications.svc/DeleteSysNotification';
    AjaxPost(_serviceUrl, _data, eval(DeleteNotificationSuccess), eval(DeleteSysNotificationFailed));
}

function DeleteNotificationSuccess(result) {

    var message = 'Message deleted successfully';
    var messageType = 'jSuccess';
    ReloadGridData();
    ClosePopUp();
    ShowMessageForChild(message, messageType);
}

function DeleteSysNotificationFailed() {

    alert('Service failed.');
}

function notificationID(sysId) {

    this.sysId = sysId;
    this.companyId = _companyId;
}

function ClearNotificationFields() {

    $('#txtTitle').val('');
    $('#txtFromDate').val('');
    $('#txtToDate').val('');
    $('#txtNotifyMessage').val('');
}

function ResetAllWarnings() {

    var controlId = 'txtTitle';
    ShowHideWarningMessage(controlId, 'Please specify Title Name', false);

    controlId = 'txtFromDate';
    ShowHideWarningMessage(controlId, 'Please specify Date', false);

    controlId = 'txtToDate';
    ShowHideWarningMessage(controlId, 'Please specify Date', false);

    controlId = 'txtNotifyMessage';
    ShowHideWarningMessage(controlId, 'Please specify Message', false);
}