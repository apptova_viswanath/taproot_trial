﻿
// Javascript for manage users page
// Manage users page will get the users details and display in the grid
var _page = 1;
var _searchEventAccessUser = '';

$(document).ready(function () {
    var createUserPageUrl = GetBaseUrl() + "/New";
    $("#lnkCreateNewUser").attr("href", createUserPageUrl);
    
    //// bind Encryption Password function to btnEncryptPassword button
    //$("#btnEncryptPassword").on("click", EncryptPassword);

    $('#txtSearchUser').focus();
    OnPageLoad();
});


function OnPageLoad() {
   
    if (localStorage.IsGlobal == 'true')
        $(".divSearchUser").show();
    else
        $(".divSearchUser").hide();

    DataBindToGrid();

    $('#ancUploadUsers').click(function () {
        openUploadUserPopUp();
        return false;
    });

    $("#uploadUsersModal").hide();

    $('#ancEventAccess').click(function () {
        OpenEventAccessPopup();
        return false;
    });

    $('#btnEventAccessNext').click(function () {
        OnClickNextEventAccess();
        return false;
    });
    $('#btnEventAccessBack').click(function () {
        OnClickBackEventAccess();
        return false;
    });

    $('#btnEventAccessSave').click(function () {
        OnClickSaveEventAccess();
        return false;
    });


    $('#btnEventAccessCancel').click(function () {
        OnClickCloseEventAccess();
    });

    BindEventAccessUsersGrid();

    //team user search option 
    $("#txtSearchUserEventAccess").keyup(function () {
        EventAccessUserSearch();
    });

}

function EventAccessUserSearch() {
    _searchEventAccessUser = $('#txtSearchUserEventAccess').val().trim();
    BindEventAccessUsersGrid();
    ReloadUserEventAcsGridData();
}

function OnClickBackEventAccess() {
    $('#divEventUserAccessGrid').show();
    $('#divEventAccessOptions').hide();
    $('#btnEventAccessCancel').show();
    $('#btnEventAccessBack').hide();
    $('#btnEventAccessNext').show();
    $('#btnEventAccessSave').hide();
}

function OnClickCloseEventAccess() {
    $('#divEventAccessPopup').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divEventAccessPopup").dialog("destroy");
    $('#divEventAccessPopup').bind("dialogclose", function (event, ui) {

    });
}

function OnClickSaveEventAccess() {
    var ids = $('#EventUserAccessGrid').jqGrid('getDataIDs');
    var userIds = '';
    for (var i = 0; i < ids.length ; i++) {
        var isChecked = ($("#jqg_EventUserAccessGrid_"+ ids[i]).prop('checked') )? true : false;
        if (i > 0 && userIds != null && userIds.length>0 && isChecked)
            userIds = userIds +  "," ;
        
        if (isChecked)
            userIds = userIds + ids[i];
    }
    var view = ($('#chkEventAccessView').attr('checked') == 'checked') ? 1 : 0;
    var edit = ($('#chkEventAccessEdit').attr('checked') == 'checked') ? 1 : 0;

    if (userIds != null && userIds.length > 0) {
        if (view == 1 || edit == 1) {
            var userDetails = new EventAccessData(userIds, view, edit);
            _data = JSON.stringify(userDetails);
            _serviceUrl = _baseURL + 'Services/UserDetails.svc/AddEventAccessUsers';
            AjaxPost(_serviceUrl, _data, eval(EventAccessSuccess), eval(EventAccessFails));
        } else {
            var message = "Please select Event Access for Users.";
            messageType = 'jError';
            NotificationMessage(message, messageType, true, 3000);
        }
    }
}

function EventAccessData(userIds, view, edit) {
    this.companyId = GetCompanyId();
    this.userIds = userIds;
    this.isEdit = edit;
    this.isView = view;
}

function EventAccessSuccess(result) {
    if (result != null) {
        var message = result.d;
        messageType = 'jSuccess';
        NotificationMessage(message, messageType, true, 3000);
        OnClickCloseEventAccess();
    }
}

function EventAccessFails() {
    alert('Service failed.');
}

function OnClickNextEventAccess() {
    var ids = $('#EventUserAccessGrid').jqGrid('getDataIDs');
    var userIds = '';
    for (var i = 0; i < ids.length ; i++) {
        var isChecked = ($("#jqg_EventUserAccessGrid_"+ ids[i]).prop('checked') )? true : false;
        if (i > 0 && userIds != null && userIds.length > 0 && isChecked)
            userIds = userIds +  "," ;
        if (isChecked)
            userIds = userIds + ids[i];
    }

    if (userIds != null && userIds.length > 0) {
        $('#divEventUserAccessGrid').hide();
        $('#divEventAccessOptions').show();
        $('#btnEventAccessCancel').hide();
        $('#btnEventAccessBack').show();
        $('#btnEventAccessNext').hide();
        $('#btnEventAccessSave').show();
    }
    else {
        var message = "Please select user to give access.";
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
    }
    
}

//Open Pop up page for Event Access
function OpenEventAccessPopup() {
    $('#txtSearchUserEventAccess').val('');
    _searchEventAccessUser = '';
    
    var pageTitle = 'Event Access';
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divEventAccessPopup").dialog({
        title: pageTitle,
        height: 400,
        width: 700,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
    BindEventAccessUsersGrid();
    ReloadUserEventAcsGridData();
    OnClickBackEventAccess();
}

//Show the modal pop up to upload a file.
function openUploadUserPopUp() {

    $("#uploadUsersModal").dialog("destroy");

    $("#uploadUsersModal").dialog({
        height: 320,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

}

// Reload Grid
function ReloadGridData() {
    jQuery("#UsersGrid").trigger("reloadGrid");
}

function DataBindToGrid() {
    var $grid = $("#UsersGrid"), gridcol;

    jQuery("#UsersGrid").jqGrid({
        datatype: function (pdata) { UsersData(pdata); },
        colNames: ['First Name', 'Last Name', 'Email', 'User Name'], //define column names
        colModel: [{ name: 'FirstName', index: 'FirstName', width: 90, align: 'left', sortable: true, formatter: TextFormatter },
                        { name: 'LastName', index: 'LastName', width: 90, align: 'left', sortable: true },
                        { name: 'Email', index: 'Email', width: 90, align: 'center', sortable: true },
                        { name: 'UserName', index: 'UserName', width: 90, align: 'center', sortable: true },
        ],
        toppager: false,
        emptyrecords: 'There are no Users.',
        // Grid total width and height  
        autowidth: true,
        height: '100%',
        caption: 'Users',
        rowNum: 50,
        hidegrid: false,
        pager: jQuery('#pageNavigation'),
        viewrecords: true,
        sortname: 'FirstName',
        sortorder: "desc",        
        forceFit: true,
        ondblClickRow: function (id) { },

        ondblClickRow: function (rowId) {
            var rowData = jQuery(this).getRowData(rowId);
            var title = rowData["FirstName"];
            var url = $(title).attr('href');
            window.location = url;
        },
        loadComplete: function () {
            
            jqgridCreatePager('pageNavigation', 'UsersGrid', 5);
        }

    });
   
    gridcol = $grid.jqGrid('getGridParam', 'colModel');
    if (gridcol != undefined) {
        $('#gbox_' + $.jgrid.jqID($grid[0].id) +
        ' tr.ui-jqgrid-labels th.ui-th-column').each(function (i) {
            var cmi = gridcol[i], colName = cmi.name;

            if (cmi.sortable !== false) {
                // show the sorting icons
                $(this).find('>div.ui-jqgrid-sortable>span.s-ico').show();
            }
        });
    }

    //$(window).bind('resize', function () {
    //    $("#UsersGrid").setGridWidth($('.bodyArea').width() - 15);
    //}).trigger('resize');
   
}

function TextFormatter(cellvalue, options, rowObject) {
    var opt = options.rowId;
    var Data = opt.split(',');
    var baseUrl = GetBaseUrl();
    var result = "<a href=" + baseUrl + "/" + Data[0] + "/Edit >" + cellvalue + "</a>";
    return result;
}

// Function to find the user id from url
function GetBaseUrl() {
    var url = jQuery(location).attr('href');
    var baseUrl = url;
    return baseUrl;
}


function UsersData(pData) {
    
    var filterString = $('#txtSearchUser').val();
    //used to have a "ignore case" checkbox. deleting since there is no use for it
    //var ignoreCase = ($('#chkIgnoreCase').attr('checked') == 'checked') ? 1 : 0;

    var gridDetails = new GridParams(filterString, pData);
    _data = JSON.stringify(gridDetails);
    serviceUrl = _baseURL + 'Services/ManageUsers.svc/GetUsersDetails';
    AjaxPost(serviceUrl, _data, eval(GetUsersDetailsSuccess), eval(GetUsersDetailsFailed));
}

function GridParams(filterString, pData) {
    
    _page = pData.page;
    this.page = pData.page;
    this.rows = pData.rows;
    this.searchFilter = filterString;
   // this.searchFilter = "richa";
    this.sortIndex = pData.sidx;
    this.sortOrder = pData.sord;
    this.companyId = parseInt($('#hdnDivisionId').val()) > 0 ? parseInt($('#hdnDivisionId').val()) : _companyId;
    this.isDivision = parseInt($('#hdnDivisionId').val()) > 0 ? true : false;
    this.isGlobalAdmin = localStorage.IsGlobal;
    
    
}

function GetUsersDetailsSuccess(result) {
    var gridId = jQuery("#UsersGrid")[0];
    if (result.d != "") {
        gridId.addJSONData(JSON.parse(result.d));
    }
    else {
        var message = 'There are no users available.';
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
    }
    jqgridCreatePager('pageNavigation', 'UsersGrid', 5);
}

//service Fail
function GetUsersDetailsFailed() {
    alert('Service failed.');
}

function ValueChanged() {
    
    var filter = $('#txtSearchUser').val();
    //var ignoreCase = '';
    //ignoreCase = ($('#chkIgnoreCase').attr('checked') == 'checked') ? 1 : 0;
    //UsersData(filter);
    GetFilteredUsersList(filter);
}

// Reload Grid
function ReloadGridData() {
    jQuery("#UsersGrid").trigger("reloadGrid");
}

function GetFilteredUsersList(pdata) {
    var params = new Object();
    params.page = _page;
    params.rows = 50;
    params.searchFilter = pdata;
    params.companyId = _companyId;
    params.isGlobalAdmin = localStorage.IsGlobal;
    data = JSON.stringify(params);

    serviceUrl = _baseURL + 'Services/ManageUsers.svc/GetUsersDetails';
    AjaxPost(serviceUrl, data, eval(GetUsersDetailsSuccess), eval(GetUsersDetailsFailed));
}


//Event Access Users List Grid Binding
function BindEventAccessUsersGrid() {

    jQuery("#EventUserAccessGrid").jqGrid({

        datatype: function (parameterData) {
            GetEventUserLists(parameterData);
        },
        colNames: [ 'First Name', 'Last Name', 'Email', 'User Name', 'View Events', 'Edit Events'], //define column names
        colModel: [ { name: 'FirstName', index: 'FirstName', width: 100, align: 'left', resizable: true, sortable: true },
                        { name: 'LastName', index: 'LastName', width: 100, align: 'left', sortable: true, resizable: true },
                        { name: 'Email', index: 'Email', width: 100, align: 'center', sortable: true, resizable: true },
                        { name: 'UserName', index: 'UserName', width: 100, align: 'center', sortable: true, resizable: true },
                        { name: 'ViewEvents', index: 'ViewEvents', width: 100, align: 'center', formatter: "checkbox", formatoptions: { disabled: true, value: "Yes:No" }, editable: false, edittype: "checkbox", sortable: false, resizable: true },
                        { name: 'EditEvents', index: 'EditEvents', width: 100, align: 'center', formatter: "checkbox", formatoptions: { disabled: true, value: "Yes:No" }, editable: false, edittype: "checkbox", sortable: false, resizable: true }
        ],
        gridview: false,
        rowattr: function (rd) {
        },
        toppager: false,
        emptyrecords: 'There are no Users.',
        // Grid total width and height        
        autowidth: true,
        rowNum: 10,
        hidegrid: false,
        //rowList: [10, 20, 50, 100],
        pginput: "false",
        height: '100%',
        caption: 'Users',
        pager: jQuery('#EventUserAccessGridNavigation'),
        viewrecords: true,
        sortname: 'FirstName',
        sortorder: "asc",
      
        cmTemplate: { title: false },
        multiselect: true,
        loadComplete: function () {
        }
    });

    //for zoom / Ctrl ++ - screen should fit within the page only
    $(window).bind('resize', function () {

        $("#EventUserAccessGrid").setGridWidth($('.EventUserAccessGrid').width());
    });
   
} //grid end

// Event Access Reload Grid
function ReloadUserEventAcsGridData() {
    jQuery("#EventUserAccessGrid").trigger("reloadGrid");
}

//to get Event Access User Lists
function GetEventUserLists(parameterData) {

    var companyId = GetCompanyId();
    var userListData = new Object();
    userListData.page = parameterData.page;
    userListData.rows = parameterData.rows;
    userListData.SearchFilter = _searchEventAccessUser;
    userListData.IgnoreCase = 0;
    userListData.sortIndex = parameterData.sidx;
    userListData.sortOrder = parameterData.sord;
    userListData.companyId = companyId;

    data = JSON.stringify(userListData);
    _serviceUrl = _baseURL + 'Services/ManageUsers.svc/GetEventAccessUsers';
    AjaxPost(_serviceUrl, data, eval(GetEventUserListsSuccess), eval(GetEventUserListsFail));
}

// Event Access User List Success
function GetEventUserListsSuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jError';
    if (result.d != null) {
        var gridData = jQuery("#EventUserAccessGrid")[0];
        gridData.addJSONData(JSON.parse(result.d));
        var resultCount = ParseToJSON(result.d);
        if (resultCount.rows.length == 0) {
            $("#ReloadGrid").show();
            errorMessage = 'There are no users available.';
            errorMessageType = 'jError';
        }
    }
}

//service fail
function GetEventUserListsFail() {
    alert('Service failed.');
}

/*
// Encrypting the passwords
function EncryptPassword()
{
    _serviceUrl = _baseURL + 'Services/Users.svc/EncryptDbPasswords';
    AjaxPost(_serviceUrl, '', eval(EncryptPasswordSuccess), eval(EncryptPasswordFail));
}

// Encrypting the passwords - On Sucess
function EncryptPasswordSuccess(result) {
    if (result.d === 'True') {
        NotificationMessage('Passwords are encrypted.', 'jSuccess', true, 3000);
    }
    else if (result.d === 'done') {
        NotificationMessage('Passwords are already encrypted.', 'jError', true, 3000);
    }
    else {
        NotificationMessage('Service failed.', 'jError', true, 3000);
    }
}

// Encrypting the passwords - On Fail
function EncryptPasswordFail() {
    NotificationMessage('Service failed.', 'jError', true, 3000);
}
*/