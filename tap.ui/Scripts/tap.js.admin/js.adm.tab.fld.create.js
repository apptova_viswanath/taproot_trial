﻿//A JS CLASS USED BY THE ADMIN > CUTOM TAB PAGE > FIELD SECTION > FOR CREATING CONTROLS

//Create dynamic field
function CreateDynamicFieldName(displayName, isRequired) {

    if (isRequired)
        field = "<label class='ContentUserLabel wordWrap'>" + displayName + "<span style='color:red'>*</span>" + "</label>";

    else
        field = "<label class='ContentUserLabel wordWrap'>" + displayName + "</label>";

    return field;

}

//Create reactivate button
function CreateReactivateButton(displayName, fieldId) {

    var editButton = "<input class='btn btn-primary btn-small' type='button' id='btn" + displayName
                    + "' value='Reactivate'" + "onClick=javascript:ReactivateFieldData('" + fieldId + "') />";
    return editButton;
}

//Create edit button
function CreateEditButton(displayName, fieldId, controlType) {
        
    var buttonID = 'btn' + displayName;
    var editButton = '';
  
    if (controlType == 'Paragraph Text') {
        editButton = '<input class="btn btn-primary btn-small"  type="button"  value = "Edit" onClick=\'javascript:OpenAddEditFieldPopup(' + fieldId + ',"' +
                       displayName + '");\'; return false;  id="' + buttonID + '"/> ';

    }
    else {
        editButton = '<input style="margin-top:-2px" class="btn btn-primary btn-small"  type="button"  value = "Edit" onClick=\'javascript:OpenAddEditFieldPopup(' + fieldId + ',"' +
                       displayName + '");\'; return false;  id="' + buttonID + '"/> ';
    }
    
    return editButton;
}

//Create info button
function CreateInfoButton(description) {
    var editButton = '';

    if (description != null) {

        editButton = "<span class='help' style='z-index:1000px;padding-left:10px;vertical-align:middle;padding-top:0px;'><img border='0' src='" + _baseURL + "Images/Info.png' /><span><p>" + description + "</p></span></span>";
    }
    else {
       //editButton = "<span  style='display:none'></span>";
        //editButton = "<span class='help' style='width:16px;z-index:1000px;padding-left:10px;vertical-align:middle;padding-top:0px;'><span><p>" + description + "</p></span></span>";
        editButton = "<div class='help' style='width:16px;z-index:1000px;padding-left:10px;vertical-align:middle;padding-top:0px;'></div>";
    }
  
    return editButton;
}

//Create dynamically the disabled field
function CreateDisableFieldsDynamic(displayName, controlType, systemListName, systemListID, treeViewType) {
    
    var control;
    if (controlType == 'Text Box' || controlType == 'Date Field' || controlType == 'DateTime') {
        control = "<input disabled='true' class='ContentInputTextBox WidthP40'  type='text' id='txt" + displayName + "' />";
    }

    else if (controlType == 'Radio Button') {

        control = "<div class='CustomDiv'><input disabled='true' type='radio' class='MarginLeft10' style='margin-left:20px;' id='rby" +
                    displayName + "'; name='group" + displayName + "'; value='Yes' /><spam class='ContentCustomRadioButton'>Yes</spam>";
        control += "<input disabled='true' type='radio' class='MarginLeft10' style='margin-left:20px;' id='rbn" + 
                    displayName + "'; name='group" + displayName + "'; value='No' /><spam class='ContentCustomRadioButton'>No</spam></div>";

    }

    else if (controlType == 'Paragraph Text') {
        control = "<textarea disabled='true' class='ContentInputTextBox WidthP40 TextAreaHeight' rows='3' cols='25' id='txtArea" + displayName + "' />";
    }

    else if (controlType == 'Select one from list' || controlType == 'Select multiple from list') {

        if (treeViewType == 2)
            control = '<div class="CustomDiv"><img class="PaddingLeft20" disabled=true alt="OpenLink" id="lnk' + displayName
                      + '" src=' + _baseURL + 'Images/OpenLink.png' + ' /></div>';

        else if (treeViewType == 1) {
            control = '<select disabled=true style="width:40.7%" id=ddl' + systemListID + ' class="ContentInputTextBox WidthP40"></select>';
        }

    }

    return control;
}


//Create dynamic field
function CreateFieldsDynamic(name, controlType, systemListName, systemListID, treeViewType) {

    var control;
    var labelName = name;
    var displayName = name.replace(/ /g, '');
   
    if (controlType == 'Text Box' || controlType == 'Date Field' || controlType == 'DateTime') {
        control = "<input class='ContentInputTextBox WidthP40 userPreview'  type='text' id='txt" + displayName + "' />";
    }

    else if (controlType == 'Radio Button') {

        control = "<div class='CustomDiv'><input type='radio' class='MarginLeft10' style='margin-left:20px;' id='rby" +
                  displayName + "'; name='group" + displayName + "'; value='Yes' /><spam class='ContentCustomRadioButton'>Yes</spam>";

        control += "<input type='radio' class='MarginLeft10' style='margin-left:20px;' id='rbn" + displayName + "'; name='group" +
                    displayName + "'; value='No' /><spam class='ContentCustomRadioButton'>No</spam></div>";

    }
    else if(controlType == 'Label')
    {
        control = "<span class='WidthP40 adminContentLabel' id='spn" + displayName + "'>" + labelName + "</span>";
    }

    else if (controlType == 'Paragraph Text') {

        control = "<textarea class='ContentInputTextBox WidthP40 TextAreaHeight userPreview' rows='3' cols='25' id='txtArea" + displayName + "' />";

    }
    else if (controlType == 'Select one from list' || controlType == 'Select multiple from list') {

        if (treeViewType == 2) {

            control = '<div class="CustomDiv"><img style="padding-top:2px;" class="PaddingLeft20 HandCursor" alt="OpenLink" id="lnk' +
                            displayName + '" src=' + _baseURL + 'Images/OpenLink.png' +
                            '  onClick=\'javascript:OpenFieldTreeviewPopup(' + systemListID + ',"' + systemListName + '") \' /></div>';
        }
        else if (treeViewType == 1) {
            control = '<select style="width:40.7%" id=ddl' + displayName.replace(/ /g, '') + ' class="ContentInputTextBox WidthP40"></select>';

        }
    }

    return control;

}

