﻿//A JS CLASS USED BY THE ADMIN > CUTOM TAB PAGE > ADD/EDIT FIELD POP UP > CURD OPERATION

//Global variables
var _fieldIdList = '';
var _fieldIdListTrim = '';
var _customEditFieldId = '';

// On success of field data insert
function FieldDataInsertSuccess(result) {

    if (result.d) {

        if (_editFieldId == '') {

            $('#FieldErrorMsg').html('');
            $('#FieldErrorMsg').html('Inserted Field Data Successfully.');
            $('#FieldErrorMsg').addClass('SuccessMessage');
            ClearData();

        }
        else {

            $('#FieldErrorMsg').html('');
            AddFields(_tabEdit);
            ClearData();

        }
    }
    else {

        $('#FieldErrorMsg').html('Field Name already Exists.');
        $('#FieldErrorMsg').addClass('WarningMessage');

    }

    _editFieldId = '';

}


//On fail of field data insert
function FieldDataInsertFail(result) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}


//Insert new field
function InsertField(fieldTabId) {

    _dataTypeValue = $("#ddDataType option:selected").text();
    _controlType = $("#ddControlType option:selected").text();

    var displayName = $('#txtFldName').val().trim();
    var controlTypeValue = $("#ddControlType option:selected").text();

    if (_dataTypeValue == 'Yes/No' || _dataTypeValue == 'Choose from a list') {
        _maxLength = 0;
    }
    else {
        _maxLength = $('#txtMaxLen').val();
    }

    if ($("#ddDataType option:selected").text() == 'Text')
        _dataTypeValue = 'String';

    else if ($("#ddDataType option:selected").text() == 'Date Time')
        _dataTypeValue = 'DateTime';

    else if ($("#ddDataType option:selected").text() == 'Number')
        _dataTypeValue = 'Int';

    else if ($("#ddDataType option:selected").text() == 'Yes/No')
        _dataTypeValue = 'Boolean';

    else if ($("#ddDataType option:selected").text() == 'Choose from a list')
        _dataTypeValue = 'String';

    var isRequired = $('#chkIsRequired').attr('checked') ? true : false;

    var dataListValue = $('#ddDataList option:selected').val();

    var dataControlTypeValue = $('#ddControlType option:selected').val();

    if (ValidateData(displayName, _dataTypeValue, controlTypeValue, _maxLength, dataListValue)) {

        var fieldData = GetFieldsData(fieldTabId, displayName, _dataTypeValue, controlTypeValue, _maxLength, isRequired, dataListValue);
        var data = JSON.stringify(fieldData);
        _serviceURL = _baseURL + 'Services/Fields.svc/SaveField';
        AjaxPost(_serviceURL, data, eval(FieldDataInsertSuccess), eval(FieldDataInsertFail));

    }
}


//Validate data
function ValidateData(displayName, _dataTypeValue, controlTypeValue, _maxLength, dataListValue) {

    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;
    var dataListControl = $("#ddDataList option:selected").text();

    var txtFieldName = 'txtFldName';
    var txtDataType = 'ddDataType';
    var txtDataList = 'ddDataList';
    var ddControlType = 'ddControlType';

    if (displayName == '' || displayName == null) {
        ShowHideWarningMessage(txtFieldName, 'Please specify Field Name', true);
    }
    else {

        ShowHideWarningMessage(txtFieldName, 'Please specify Field Name', false);
    }
    if (_dataTypeValue == 'Select') {
        ShowHideWarningMessage(txtDataType, 'Please select a Data Type', true);
    }
    else {
        ShowHideWarningMessage(txtDataType, 'Please select a Data Type', false);
    }
    if (_dataTypeValue == 'String' && dataListControl == 'Select' && controlTypeValue != 'Text Box' && controlTypeValue != 'ParagraphText') {
        ShowHideWarningMessage(txtDataList, 'Please select any Data List', true);
    }
    else {
        ShowHideWarningMessage(txtDataList, 'Please select any Data List', false);
    }
    if (_dataTypeValue == 'String' && (controlTypeValue == 'Select one from list' || controlTypeValue == 'Select multiple from list') && dataListValue == 0) {
        ShowHideWarningMessage(txtDataList, 'Please select any Data List', true);
    }
    else {
        ShowHideWarningMessage(txtDataList, 'Please select any Data List', false);
    }

    errorMessage += ((displayName == '' || displayName == null) ? 'Field Name,\n' : '');
    errorMessage += ((_dataTypeValue == 'Select') ? 'Data Type,\n' : '');
    errorMessage += ((_dataTypeValue == 'String' && dataListControl == 'Select' && controlTypeValue != 'Text Box' && controlTypeValue != 'ParagraphText') ? 'Data List,\n' : '');
    errorMessage += ((_dataTypeValue == 'String' && (controlTypeValue == 'Select one from list' || controlTypeValue == 'Select multiple from list') && dataListValue == 0) ? 'Data List\n' : '');

    if (controlTypeValue == '' || controlTypeValue == 'Select') {
        errorMessage += ' Control Type';
        ShowHideWarningMessage(ddControlType, 'Please select any Control Type', true);
    }
    else
        ShowHideWarningMessage(ddControlType, 'Please select any Control Type', false);


    if (errorMessage.length > primaryErrorMessage.length) {

        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    }
    return true;
}


//Used to add a field to the tab
function AddFieldForCurrentTab() {

    $('#customTabButton').removeClass('PaddingRight80');
    $('#customTabButton').addClass('PaddingRight120');

    //Clear the custom fields.
    ClearCustomData();

    //Hide the Delete button
    $('#btnDelete').hide();

    $('#FieldErrorMsg').html('');
    $('#FieldErrorMsg').removeClass('WarningMessage');
    $('#FieldErrorMsg').removeClass('SuccessMessage');
    $("#dialog:ui-dialog").dialog("destroy");

    $("#divAddEditFieldsDialog").dialog({
        width: 400,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

    $('#TabErrorMsg').html('');
    $('#txtFldName').focus();

    LoadDataLists('ddDataList');
}


//Add field
function AddFields(fieldTabId) {

    _data = '&_tabId=' + fieldTabId;

    if (fieldTabId != null && fieldTabId != undefined) {

        _serviceURL = _baseURL + 'Services/Fields.svc/GetFieldListByTabID';
        Ajax(_serviceURL, _data, eval(AddFieldSuccess), eval(AddFieldFail), 'InsertFieldsControls');

    }
}


//On success of add field
function AddFieldSuccess(result) {
}


//On error of the add field, Display error message
function AddFieldFail(result) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}


//Added for Deleting Fields 
function DeleteField() {

    if (Confirmation('Are you sure you want to permanently delete the field ' + '"' + $('#txtFldName').val() + '"')) {

        var serviceURL = _baseURL + 'Services/Fields.svc/DeleteCustomField';
        _data = '&fieldId=' + _editFieldId + '&userId=' + $('#hdnUserId').val();
        Ajax(serviceURL, _data, eval(DeleteFieldSuccess), eval(DeleteFieldFail), 'CheckDeleteCustomField');

    }
}

var _isDeleteField = false;
//On success of delete field
function DeleteFieldSuccess(result) {

    var fieldName = $('#txtFldName').val();

    if (result != null) {

        if (result.d) {

            var _message = fieldName + ' Field Deleted Successfully.';
            var _messageType = 'jSuccess';
            NotificationMessage(_message, _messageType, true, 3000);
            _isDeleteField = true;
            ClosePopupAndRefreshPage();
        }
        else {

            var message = 'There is corresponding _data with (' + fieldName + '),\n  we are unable to delete (' + fieldName +
                       ').\n If you no longer want to use (' + fieldName + '), please un-check the Active check box.';

            $("#divPopupMessage").html(message);

            $("#divPopupMessage").dialog({
                title: "Message from Website",
                height: 200,
                width: 350,
                modal: true,
                draggable: true,
                resizable: false,
                position: ['middle', 200],
                buttons:
                {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                }

            });
        }
    }
}


//On Fail of delete field
function DeleteFieldFail() {

}


// Delete custom field
function DeleteCustomField(fieldTabId) {

    //Confirirmation for deleting the field.
    if (Confirmation('Are you sure you want to delete ' + '"' + $('#txtFldName').val() + '" field?')) {

        _dataTypeValue = $("#ddDataType option:selected").text();

        var displayName = $('#txtFldName').val();
        var controlTypeValue = $("#ddControlType option:selected").text();

        if (_dataTypeValue == 'Yes/No' || _dataTypeValue == 'Choose from a list') {
            _maxLength = 0;
        }
        else {
            _maxLength = $('#txtMaxLen').val();
        }

        if ($("#ddDataType option:selected").text() == 'Text') {
            _dataTypeValue = 'String';
        }
        else if ($("#ddDataType option:selected").text() == 'Date Time') {
            _dataTypeValue = 'DateTime';
        }
        else if ($("#ddDataType option:selected").text() == 'Number') {
            _dataTypeValue = 'Int';
        }
        else if ($("#ddDataType option:selected").text() == 'Yes/No') {
            _dataTypeValue = 'Boolean';
        }
        else if ($("#ddDataType option:selected").text() == 'Choose from a list') {
            _dataTypeValue = 'String';
        }

        var isRequired = $('#chkIsRequired').attr('checked') ? true : false;
        var isActive = false;
        var description = $('#txtDescription').val();

        var dataListValue = $('#ddDataList option:selected').val();

        if (ValidateData(displayName, _dataTypeValue, controlTypeValue, _maxLength, dataListValue)) {

            var fieldData = GetFieldsData(fieldTabId, displayName, _dataTypeValue, controlTypeValue, _maxLength, isRequired, dataListValue, isActive, description);
            var _data = JSON.stringify(fieldData);
            var serviceURL = _baseURL + 'Services/Fields.svc/SaveField';
            AjaxPost(serviceURL, _data, eval(CustomFieldDataDeleteSuccess), eval(CustomFieldDataDeleteFail));

        }
    }
}


// On success of custom field data delete
function CustomFieldDataDeleteSuccess(result) {

    if (result.d) {

        if (_customEditFieldId == '') {

            $('#FieldErrorMsg').html('');
            $('#FieldErrorMsg').html('Field Deleted Successfully.');
            $('#FieldErrorMsg').addClass('SuccessMessage');

            //for activity log
            var logDetails = $('#txtFldName').val() + ' Field Data deleted Successfully.';

            SaveActivityLog(GetCompanyId(), eventId, GetUserId(), categoryId, logDetails);

            ClearCustomData();
        }
    }

    _customEditFieldId = '';
}


// On fail of custom field data deletion
function CustomFieldDataDeleteFail(result) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}


// On fail of custom field data insertion
function CustomFieldDataInsertFail(result) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}


// Clear custom data
function ClearCustomData() {

    _editFieldId = '';
    AddOptionsDataType();

    $('#txtFldName').val('');
    $('#txtDescription').val('');
    $("#ddDataType").val($("#ddDataType option:first").val());
    $("#ddControlType").val($("#ddControlType option:first").val());
    $("#ddDataList").val($("#ddDataList option:first").val());
    $('#chkIsRequired').attr('checked', false);
    $('#txtMaxLen').val('');
    $('#DatalistDisplay').hide();
    $('#MaxLength').hide();

    var txtFieldName = 'txtFldName';
    var txtDataType = 'ddDataType';
    var txtDataList = 'ddDataList';
    var txtControlType = 'ddControlType';

    ShowHideWarningMessage(txtFieldName, 'Please specify Field Name', false);
    ShowHideWarningMessage(txtDataType, 'Please select a Data Type', false);
    ShowHideWarningMessage(txtDataList, 'Please select any Data List', false);
    ShowHideWarningMessage(txtControlType, 'Please select any Control Type', false);
}

// Make visible or invisible in active field region
function DisplayUserPreviewFieldRegion(fieldData) {
  
    if (fieldData != '' || _isDeleteField) {

        $("#FieldInfo").html(fieldData);
        $("#divUserPreviewFields").show();

    }
    else {
        $("#FieldInfo").html(fieldData);
        $("#divUserPreviewFields").hide();
    }
}


// Make visible or invisible in active field region
function DisplayInActiveFieldRegion(deletedFieldData) {

    if (deletedFieldData == '') {
        $("#divInactiveFields").hide();

    }
    else {

        $("#divInactiveFields").show();
        $("#DeletedFieldInfo").html(deletedFieldData);
    }
}

//fill drop down control dynamically for Tree view list
function CustomDropDownTreeListsucc(result) {

    var jsonResult = jQuery.parseJSON(result.d);

    if (jsonResult != null && jsonResult != [] && jsonResult != undefined && jsonResult.length > 0) {

        var controlId = jsonResult[0]["DisplayName"].replace(/ /g, '');
        $("#ddl" + controlId).append($("<option></option>").attr("value", '0').text('<< Select >>'));

        for (var i = 1; i < jsonResult.length; i++) {

            var listValueId = jsonResult[i]["ListValueID"];
            var listValueName = jsonResult[i]["ListValueName"];
            var displayName = jsonResult[i]["DisplayName"];

            $("#ddl" + controlId).append($("<option></option>").attr("value", listValueId).text(listValueName));
        }
    }
}


//On fail of loading custom tree list
function CustomDropDownTreeListfail() {
    alert('Error in Loading Drop down tree view.');
}

//Insert custom field
function InsertCustomField() {

    //validation
    _dataTypeValue = $("#ddDataType option:selected").text();

    var displayname = $('#txtFldName').val();
    var controlTypeValue = $("#ddControlType option:selected").text();

    if (_dataTypeValue == 'Yes/No' || _dataTypeValue == 'Choose from a list') {
        _maxLength = 0;
    }
    else {
        _maxLength = $('#txtMaxLen').val();
    }

    if ($("#ddDataType option:selected").text() == 'Text') {
        _dataTypeValue = 'String';
    }
    if($("#ddDataType option:selected").text() == 'Label')
    {
        _dataTypeValue = 'Label';
    }

    else if ($("#ddDataType option:selected").text() == 'Date Time') {
        _dataTypeValue = 'DateTime';
    }

    else if ($("#ddDataType option:selected").text() == 'Date') {
        _dataTypeValue = 'Date';
    }

    else if ($("#ddDataType option:selected").text() == 'Number') {
        _dataTypeValue = 'Int';
    }

    else if ($("#ddDataType option:selected").text() == 'Yes/No') {
        _dataTypeValue = 'Boolean';
    }

    else if ($("#ddDataType option:selected").text() == 'Choose from a list') {
        _dataTypeValue = 'String';
    }

    var isRequired = $('#chkIsRequired').attr('checked') ? true : false;
    var isActive = $('#chkIsActive').attr('checked') ? true : false;
    var description = $('#txtDescription').val();
    var dataListValue = $('#ddDataList option:selected').val();


    if (ValidateData(displayname, _dataTypeValue, controlTypeValue, _maxLength, dataListValue)) {

        var fieldData = GetFieldsData(_customTabId, displayname, _dataTypeValue, controlTypeValue, _maxLength, isRequired, dataListValue, isActive, description);
        var data = JSON.stringify(fieldData);
        var serviceURL = _baseURL + 'Services/Fields.svc/SaveField';
        AjaxPost(serviceURL, data, eval(CustomFieldDataInsertSuccess), eval(CustomFieldDataInsertFail));

    }
}


// On success of custom field data insert
function CustomFieldDataInsertSuccess(result) {

    if (result.d) {

        var eventId = 0;
        var categoryId = 1;
        var logDetails = '';

        if (_customEditFieldId == '') {

            $('#FieldErrorMsg').html('');
            $('#FieldErrorMsg').html('Inserted Field Data Successfully.');
            $('#FieldErrorMsg').addClass('SuccessMessage');

            //for activity log          
            logDetails = $('#txtFldName').val() + ' Field Data inserted Successfully.';
            SaveActivityLog(GetCompanyId(), eventId, GetUserId(), categoryId, logDetails);

            ClearCustomData();
        }
        else {

            $('#FieldErrorMsg').html('');
            AddCustomFields();

            $('#FieldDialog').hide();
            $('.backgroundMask').hide();

            ClearCustomData();
            $('#backgroundPopup').hide();

            //for activity log       
            logDetails = $('#txtFldName').val() + ' updated Field Data Successfully.';
            SaveActivityLog(GetCompanyId(), eventId, GetUserId(), categoryId, logDetails);
        }
    }
    else {

        $('#FieldErrorMsg').html('Field Name already Exists.');
        $('#FieldErrorMsg').addClass('WarningMessage');
    }

    _customEditFieldId = '';

    //close the popup Automatically
    $('#FieldErrorMsg').html('');

    CloseAddEditFieldPopup();
    AddCustomFields();
    ClearCustomData();

}