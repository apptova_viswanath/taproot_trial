﻿

//Global Variables
var _companyId = -1;
var _searchDivision = '';
var _divisionId = -1;
var _bannerLogo = 1;
var _reportLogo = 2;
var _listId = -1;
var _parentDivisionId = '';
var _gridCaption = 'Manage Divisions';
//Page load
$(document).ready(function () {
    _companyId = GetCompanyId();
    OnLoad();
    
});

function OnLoad() {
    //alert('2');

    ////Password Policy
    //$('#body_Adminbody_txtPassword').keyup(function () {

    //    ValidatePassWordPolicy($(this).val());

    //}).focus(function () {

    //    ValidatePassWordPolicy($(this).val());
    //    $('#pswd_info').show();

    //}).blur(function () {

    //    $('#pswd_info').hide();
    //    DivisionValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val());

    //});


    ////Password Policy
    //$('#body_Adminbody_txtConfirmPassword').keyup(function () {
    //    DivisionValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val());
    //});







    _divisionId = $('#body_Adminbody_hidden_DivisionId').val();
    BindDivisionListGrid();
    $("#txtSearchDivision").val('');
    
    $("#txtSearchDivision").keyup(function () {
        SearchDivision();
    })

    $('#SaveDivisionDetails').click(function () {
        SaveDivisionWithAdmin();
    });

    $('#clearDivisionFields').click(function () {
        ClearDivisionFields();
    });
    if (_divisionId != null && _divisionId > 0)
        $('#saveDivisionDetails').val('Save Changes');

    $('#ancBackDivisionsList').attr('href', _baseURL + 'Admin/Divisions');
    $('#adddivisionimage').attr('src', _baseURL + 'Images/add.png');
    $('#lnkCreateNewDivision').attr('href', _baseURL + 'Admin/Divisions/New');

    
    NextPrevSubSection();


    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0) 
        $('#lnkCreateNewDivision').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/New');
    

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0)// If Internet Explorer, return version number
    {
        $('#divMainDivisionUser').css('padding-left', '260px');
    }

    var $strtDate = $('#txtSubscriptionStart');
    $strtDate.datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:2050',
        maxDate: '+30Y',
        dateFormat: 'd M yy',
        onClose: function (dateText, inst) {
            $(this).datepicker('option', 'dateFormat', 'd M yy');
        }
    });

    var $endDate = $('#txtSubscriptionEnd');
    $endDate.datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1950:2050',
        maxDate: '+30Y',
        dateFormat: 'd M yy',
        onClose: function (dateText, inst) {
            $(this).datepicker('option', 'dateFormat', 'd M yy');
        }
    });

    _isSUCompany = $('#hdnSUCompany').val();
    if (_isSUCompany != null && _isSUCompany == '1') {
        $('#divDivisionSubscriptionStrt').show();
        $('#divDivisionSubscriptionEnd').show();
        //
        ////Replace Divisions with Companies for Single user.
        //$('#ancViewDivision').html('<img id="Img1" class="imgHeader marginTop0Px" src="' + _baseURL + '/Images/Company.png" />Companies');
        
        //Rename add Division with add Company.
        $('#lnkCreateNewDivision').html('<img id="adddivisionimage" class="addImage" alt="Demo" src="' + _baseURL + 'Images/add.png" />Add Company');

        //Display Search Companies instead of Search Divisions.
        $('#txtSearchDivision').attr("placeholder", "Search");

        //Change the Division to Company for Single User.
        //-------------------------------------------------------//        
        $('#liCreateDivsion').html('Create Company');
        $('#newCreation').html('You are about to create a new Company. Please enter some basic company information and create an admin user account for this company. ');
        //------------------------------------------------------//        
    }
}



function ValidatePassWordPolicy(pswd) {


    if (pswd.length < $('#spnLength').html()) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }

    //validate upper case letter
    if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
        $('#upperCase').removeClass('invalid').addClass('valid');
    } else {
        $('#upperCase').removeClass('valid').addClass('invalid');
    }

    //validate lower case letter
    if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
        $('#lowerCase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowerCase').removeClass('valid').addClass('invalid');
    }

    //validate number
    var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
    if (pswd.match(numberPattern)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }


    //validate special character
    var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
    if (pswd.match(specialCharPattern)) {
        $('#specialCharacter').removeClass('invalid').addClass('valid');
    } else {
        $('#specialCharacter').removeClass('valid').addClass('invalid');
    }


    if (pswd.length > $('#spnMaxCharacterCount').html()) {
        $('#maxCharacter').removeClass('valid').addClass('invalid');
    } else {
        $('#maxCharacter').removeClass('invalid').addClass('valid');
    }

}


function NextPrevSubSection() {
        
    $('#divisionNextBtn').click(function () {
        if ($('#divNewDivision').is(':visible')) {
           
            var divisionname = TrimString($('#body_Adminbody_divisionName').val());
            var email = TrimString($('#body_Adminbody_divisionEmail').val());
            var phonenumber = TrimString($('#body_Adminbody_divisionPhoneNumber').val());
            var website = TrimString($('#body_Adminbody_divisionWebsite').val());
            var subscriptionStart = TrimString($('#txtSubscriptionStart').val());
            var subscriptionEnd = TrimString($('#txtSubscriptionEnd').val());

            if (DivisionDetailvalidation(divisionname, email, phonenumber, website, subscriptionStart, subscriptionEnd)) {
                $('#divNewDivision').hide();
                $('#divCreateUsers').show();
                $('#liCreateDivsion').removeClass('progtrckr-todo');
                $('#liCreateDivsion').addClass('progtrckr-done');
                $('#SaveDivisionDetails').show();

                $('#divisionPreviousBtn').show();
               // $('#toContinueText').html('To continue, click Create');
                $('#toContinueText').html('');
                $('#body_Adminbody_txtFirstName').focus();
                $('#divisionNextBtn').hide();
                return false;
            }
        }
    }
    );

    $('#divisionPreviousBtn').click(function () {
        if ($('#divCreateSettings').is(':visible')) {
            $('#divCreateSettings').hide();
            $('#divCreateUsers').show();
            $('#divisionNextBtn').show();
            $('#toContinueText').html('To continue, click Next');
            return false;
        }
        if ($('#divCreateUsers').is(':visible')) {
            $('#divCreateUsers').hide();
            $('#divNewDivision').show();
            $('#divisionPreviousBtn').hide();
            $('#divisionNextBtn').show();
            $('#toContinueText').html('To continue, click Next');
            $('#SaveDivisionDetails').hide();
            return false;
        }
    }
    );


    if ($('#body_Adminbody_divisionRemoveReportLogo').is(':visible')) {
        $('#body_Adminbody_divisionUploadReportLogo').hide();
    } else {
        $('#body_Adminbody_divisionUploadReportLogo').show();
        $('#body_Adminbody_divisionReportLogoChange').hide();
    }

    if ($('#body_Adminbody_divisionRemoveBannerLogo').is(':visible')) {
        $('#body_Adminbody_divisionUploadBannerLogo').hide();
    } else {
        $('#body_Adminbody_divisionUploadBannerLogo').show();
        $('#body_Adminbody_divisionBannerLogoChange').hide();
    }

    if ($('#body_Adminbody_divisionRemoveBannerLogo').is(':visible') && $('#body_Adminbody_divisionRemoveReportLogo').is(':visible')) {
        $('#body_Adminbody_btnUpload').hide();
    }

    $('#body_Adminbody_divisionReportLogoCancel').hide();
    $('#body_Adminbody_divisionBannerLogoCancel').hide();
}
//Division List Grid Binding
function BindDivisionListGrid() {
    //Change the wording for Companies
    _isSUCompany = $('#hdnSUCompany').val();
    if (_isSUCompany != null && _isSUCompany == '1') {
        _gridCaption = 'Manage Companies';
    }

    jQuery("#DivisionListGrid").jqGrid({

        datatype: function (parameterData) {
            DivisionList(parameterData);
        },
        colNames: ['DivisionId','Name', 'Primary User'
        ],
        colModel: [
            { name: 'DivisionId', index: 'DivisionId', width: 10, align: 'left', resizable: false, editable: false, hidden: true },
            { name: 'Name', width: 50, align: 'center', index: 'Name', editable: true, formatter: TextNameFormatter },
            { name: 'primaryUser', index: 'primaryUser', width: 30, align: 'center' }
        ],
        gridview: false,
        rowattr: function (rd) {
        },
        toppager: false,
        emptyrecords: 'There are no Divisions.',
        // Grid total width and height        
        autowidth: true,
        hidegrid: false,
        rowNum: 50,
        forceFit: true,
        //rowList: [10, 20, 50, 100],
        pginput: "false",
        height: '100%',
        caption: _gridCaption,
        pager: jQuery('#DivisionListGridNavigation'),
        viewrecords: true,
        sortname: 'Name',
        sortorder: "asc",
        cmTemplate: { title: false },
    });

    $(window).bind('resize', function () {
        $("#DivisionListGrid").setGridWidth($('.DivisionListGrid').width());
    }).trigger('resize');
}


function TextNameFormatter(cellvalue, options, rowObject) {
    var opt = options.rowId;
    var Data = options.rowId;
    var baseUrl = GetBaseUrl();
    var result = "<a href=" + _baseURL + "Admin/Division/" + Data + "/Profile/Edit >" + cellvalue + "</a>";
    return result;
}

// Function to find the user id from url
function GetBaseUrl() {
    var url = jQuery(location).attr('href');
    var baseUrl = url;
    return baseUrl;
}

//to get Division Lists
function DivisionList(parameterData) {

    var companyId = _companyId;
    var divisionListData = new Object();
    divisionListData.page = parameterData.page;
    divisionListData.rows = parameterData.rows;
    divisionListData.SearchFilter = _searchDivision;
    divisionListData.IgnoreCase = 0;
    divisionListData.sortIndex = parameterData.sidx;
    divisionListData.sortOrder = parameterData.sord;

    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        divisionListData.companyID = divisionId;
    else
        divisionListData.companyID = companyId;

    data = JSON.stringify(divisionListData);
    _serviceUrl = _baseURL + 'Services/Divisions.svc/DivisionDetails';
    AjaxPost(_serviceUrl, data, eval(DivisionListSuccess), eval(DivisionListFail));
}

// Division List Success
function DivisionListSuccess(result) {
    var resultCount = '';
    var errorMessage = 'There are no divisions that match your search.';
    var errorMessageType = 'jError';
    if (result != null && result.d != null) {
        var gridData = jQuery("#DivisionListGrid")[0];
        gridData.addJSONData(JSON.parse(result.d));
        resultCount = ParseToJSON(result.d);
    }
    if (resultCount.rows.length == 0) {
        $("#ReloadGrid").show();
        if (_searchDivision != null && _searchDivision.length > 0)
            NotificationMessage(errorMessage, errorMessageType, true, 20000);
    }
}

//Division List fail
function DivisionListFail() {
    alert('Service failed.');
}


//Reload Grid
function ReloadGridData() {
    jQuery("#DivisionListGrid").trigger("reloadGrid");
}

function SearchDivision() {
    _searchDivision = TrimString($('#txtSearchDivision').val());
    if (_searchDivision == null || _searchDivision.length == 0) {
        BindDivisionListGrid();
        ReloadGridData();
        return;
    }
    BindDivisionListGrid();
    ReloadGridData();
}



//company details validations
function DivisionDetailvalidation(name, email, phone, website, subscriptionStart, subscriptionEnd) {
    var primaryErrorMessage = 'You are missing the following field: ';
    var errorMessage = primaryErrorMessage;
    errorMessage += ((name == '' || name == null || name.length == 0) ? 'Company Name,\n' : '');

    var isValid = true;

    if (name == null || name.length == 0) {
        ShowHideWarningMessage('body_Adminbody_divisionName', 'Please specify Company name', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage('body_Adminbody_divisionName', 'Please specify Company name', false);
    }

    if (phone != null && phone.length > 0) {
        if (!validatePhone('body_Adminbody_divisionPhoneNumber')) {
            ShowHideWarningMessage('body_Adminbody_divisionPhoneNumber', 'Please specify a valid phone number', true);
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_divisionPhoneNumber', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_divisionPhoneNumber', 'Please specify a valid phone number', false);



    if (email != null && email.length > 0) {
        if (validateEmail(email)) {
            ShowHideWarningMessage('body_Adminbody_divisionEmail', 'Please specify a Email', false);
        }
        else {
            ShowHideWarningMessage('body_Adminbody_divisionEmail', 'Please specify a valid Email', true);
            isValid = false;
        }
    } else
        ShowHideWarningMessage('body_Adminbody_divisionEmail', 'Please specify a Email', false);
   

    if (website != null && website.length > 0) {
        if (!WebsiteValidation('body_Adminbody_divisionWebsite')) {
            ShowHideWarningMessage('body_Adminbody_divisionWebsite', 'Please specify a valid website', true);
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_divisionWebsite', 'Please specify a valid website', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_divisionWebsite', 'Please specify a valid website', false);

    _isSUCompany = $('#hdnSUCompany').val();
    if (_isSUCompany == "1") {

        if (subscriptionStart == null || subscriptionStart.length == 0) {
            ShowHideWarningMessage('txtSubscriptionStart', 'Please specify a subscription start date', true);
            isValid = false;
            errorMessage += ((subscriptionStart == '' || subscriptionStart == null || subscriptionStart.length == 0) ? ' Subscription Start Date,\n' : '');
        }
        else
            ShowHideWarningMessage('txtSubscriptionStart', 'Please specify a subscription start date', false);

        if (subscriptionEnd == null || subscriptionEnd.length == 0) {
            ShowHideWarningMessage('txtSubscriptionEnd', 'Please specify a subscription end date', true);
            isValid = false;
            errorMessage += ((subscriptionEnd == '' || subscriptionEnd == null || subscriptionEnd.length == 0) ? ' Subscription End Date' : '');
        }
        else
            ShowHideWarningMessage('txtSubscriptionEnd', 'Please specify a subscription end date', false);
    }

    if (errorMessage.length > primaryErrorMessage.length && errorMessage.substring(errorMessage.length - 1) == ',')
        errorMessage = errorMessage.replace(errorMessage.substring(errorMessage.length - 1), '');

    if (errorMessage.length > primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 2000);
        return false;
    }
    else
    {
        //if (_isSUCompany != null && _isSUCompany == '1') {
        //    errorMessage = SubscriptionValidation('txtSubscriptionStart', 'txtSubscriptionEnd');

        //    if (errorMessage != null && errorMessage.length > 0) {
        //        _message = errorMessage;
        //        _messageType = 'jError';
        //        NotificationMessage(_message, _messageType, true, 2000);
        //        return false;
        //    }
        //}
    }

    if (!isValid)
        return false;

    return true;
}

//clear fields
function ClearDivisionFields() {
    $('#body_Adminbody_divisionName').val('');
    $('#body_Adminbody_divisionPhoneNumber').val('');
    $('#body_Adminbody_divisionWebsite').val('');
    $('#body_Adminbody_divisionAddress').val('');
    $('#body_Adminbody_divisionEmail').val('');
}



//save division with admin
function SaveDivisionWithAdmin() {

    var parentCompanyId = _companyId;
    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        parentCompanyId = divisionId;

    var companyName = TrimString($('#body_Adminbody_divisionName').val());
    var phoneNumber = TrimString($('#body_Adminbody_divisionPhoneNumber').val());
    var website = TrimString($('#body_Adminbody_divisionWebsite').val());
    var address = TrimString($('#body_Adminbody_divisionAddress').val());
    var email = TrimString($('#body_Adminbody_divisionEmail').val());
    var subscriptionStart = TrimString($('#txtSubscriptionStart').val());
    var subscriptionEnd = TrimString($('#txtSubscriptionEnd').val());

    var adsecurity = 0;

    var firstName = TrimString($('#body_Adminbody_txtFirstName').val());
    var lastName = TrimString($('#body_Adminbody_txtLastName').val());
    var userEmail = TrimString($('#body_Adminbody_txtEmail').val());
    var userPhone = TrimString($('#body_Adminbody_txtPhone').val());
    var userName = TrimString($('#body_Adminbody_txtUserName').val());
    var password = TrimString($('#body_Adminbody_txtPassword').val());
    var confirmPassword = TrimString($('#body_Adminbody_txtConfirmPassword').val());
    var userActive = true


    if (DivisionDetailvalidation(companyName, email, phoneNumber, website, subscriptionStart, subscriptionEnd)
            && ValidateDivisionUserFields(userName, firstName, lastName, userEmail, password, confirmPassword, userActive, userPhone)
            && DivisionValidatePasswords(password, confirmPassword)) {

        $('#SaveDivisionDetails').hide();
        $('#divisionPreviousBtn').hide();
        $('#divisionWorkProgressImage').show();

        var divisionDetails = new DivisionSetUpDetailData(parentCompanyId, companyName, phoneNumber, website, address, email, adsecurity, firstName, lastName, userEmail, userPhone, userName, password);
        _data = JSON.stringify(divisionDetails);
        _serviceUrl = _baseURL + 'Services/Divisions.svc/SaveCompanyWithAdmin';
        AjaxPost(_serviceUrl, _data, eval(DivisionSetUpSuccess), eval(DivisionSetUpFail));
    }
}



function DivisionValidatePasswords(Password, cPassword) {
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    if (Password != null && Password.match(_passwordRegularExpression) != null) {
        if (Password != cPassword) {
            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;
        } else {
            ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }
    else {
        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', true);
        return false;
    }

    return true;
}

function DivisionSetUpDetailData(parentCompanyId, companyName, phoneNumber, website, address, email, adsecurity, firstName, lastName, userEmail, userPhone, userName, password) {
    this.parentCompanyId = parentCompanyId;
    this.companyName = companyName;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.companyEmail = email;
    this.website = website;
    this.firstName = firstName;
    this.lastName = lastName;
    this.userEmail = userEmail;
    this.password = password;
    this.userName = userName;
    this.userPhone = userPhone;
    this.subscriptionStart = $('#txtSubscriptionStart').val();
    this.subscriptionEnd = $('#txtSubscriptionEnd').val();
}

//success message saving data
function DivisionSetUpSuccess(result) {
    _divisionId = 0;
    var txtUserName = 'body_Adminbody_txtUserName';
    var txtDivisionName = 'body_Adminbody_divisionName';
    _message = 'Company setup completed successfully.';

    if (result.d != "" && result.d.indexOf(':') < 0) {
        $('#divisionWorkProgressImage').hide();
        $('#SaveDivisionDetails').show();
        $('#divisionPreviousBtn').show();
        
        _messageType = 'jError';
        _message = result.d;

        if (result.d.indexOf("User") > -1) {
            if (result.d == 'User ' + $('#body_Adminbody_txtEmail').val() + ' already exists') {//Checked for email availability.
                _message = $('#body_Adminbody_txtEmail').val() + ' already exists';
                ShowHideWarningMessage('body_Adminbody_txtEmail', 'Please specify User Email', true);
            }
            else {
                ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
            }
        }
        else {
            if (result.d != null && result.d == $('#body_Adminbody_divisionEmail').val() + ' already exists') {//Checked for email availability.
                ShowHideWarningMessage('body_Adminbody_divisionEmail', 'Please specify Company Email', true);
            }
            else
                ShowHideWarningMessage(txtDivisionName, 'Please specify different Division Name', true);
        }

        //if (result.d.indexOf("User") > -1)
        //    ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
        //else
        //    ShowHideWarningMessage(txtDivisionName, 'Please specify different Division Name', true);

        NotificationMessage(_message, _messageType, true, 3000);
    }
    else if (result.d.indexOf(':') > -1) {
        var array = result.d.split(':');
        _divisionId = array[1];
        _messageType = 'jSuccess';
        NotificationMessage(_message, _messageType, false, 2000);
        var divisionId = $('#hdnDivisionId').val();
        if (divisionId > 0)
            window.location = _baseURL + 'Admin/Division/'+divisionId+'/Divisions';
        else
            window.location = _baseURL + 'Admin/Divisions';
    }
}

//fail message
function DivisionSetUpFail() {
    alert('Service failed.')
}

function ValidateDivisionUserFields(username, firstname, lastname, email, password, confirmpassword, active, phonenumber) {

    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;
    var isValid = true;

    errorMessage += ((username == '') ? 'User Name,\n' : '');
    errorMessage += ((firstname == '') ? 'First Name,\n' : '');
    errorMessage += ((lastname == '') ? 'Last Name,\n' : '');
    errorMessage += ((email == '') ? 'Email,\n' : '');
    errorMessage += ((password == '') ? 'Password,\n' : '');
    errorMessage += ((confirmpassword == '') ? 'Confirm Password,\n' : '');

    if (errorMessage[errorMessage.length - 2] == ',') {
        errorMessage = errorMessage.substring(0, errorMessage.length - 2);
        errorMessage = errorMessage + '.';
    }

    var txtUserName = 'body_Adminbody_txtUserName';
    var txtFirstName = 'body_Adminbody_txtFirstName';
    var txtLastName = 'body_Adminbody_txtLastName';
    var txtEmail = 'body_Adminbody_txtEmail';
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    var isValidEmail = true;
    var isPasswordMatch = true;

    if (username == null || username.length == 0) {
        ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
        isValid = false;
    }
    else {
         ShowHideWarningMessage(txtUserName, 'Please specify User Name', false);
    }
    if (firstname == null || firstname.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtFirstName, 'Please specify First Name', true);
    }
    else {
        if (!CharsValidation(firstname)) {
            isValid = false;
            ShowHideWarningMessage(txtFirstName, 'Please enter a valid First Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtFirstName, 'Please specify First Name', false);
        }
    }

    if (lastname == null || lastname.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtLastName, 'Please specify Last Name', true);
    }
    else {
        if (!CharsValidation(lastname)) {
            isValid = false;
            ShowHideWarningMessage(txtLastName, 'Please enter a valid Last Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtLastName, 'Please specify Last Name', false);
        }
    }

    if (email == null || email.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtEmail, 'Please specify a Email', true);
    }
    else {
        if (validateEmail(email)) {
            ShowHideWarningMessage(txtEmail, 'Please specify a Email', false);
        }
        else {
            ShowHideWarningMessage(txtEmail, 'Please specify a valid Email', true);
            isValidEmail = false;
            isValid = false;
        }
    }

    if (phonenumber != null && phonenumber.length > 0) {
        if (!validatePhone('body_Adminbody_txtPhone')) {
            isValid = false;
            ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', true);
        }
        else
            ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', false);

    if (confirmpassword == null || confirmpassword.length == 0) 
        ShowHideWarningMessage(txtConfirmPassword, 'Please specify a Confirm Password', true);
    else
        ShowHideWarningMessage(txtConfirmPassword, 'Please specify a Confirm Password', false);

    if (password == null || password.length == 0) {
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', true);
    }
    else {
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
        if (confirmpassword == null || confirmpassword.length == 0) {
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);
            isPasswordMatch = false;
            isValid = false;
        }
        else {
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }

    if (errorMessage.length != primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 20000);
        return false;
    }

    if (!isValid)
        return false;

    return true;
}

function ValidatePasswords(Password, cPassword) {
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    if (Password != cPassword) {
        _message = 'Password and Confirm password are not matching. Please re-enter';
        _messageType = 'jError';

        ShowHideWarningMessage(txtPassword, 'Please specify a Password', true);
        ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    } else {
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
        ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
    }

    return true;
}