﻿$(document).ready(function () {
 //   BindGrid();
    
    PopulateSiteSetting();

    $("#btnSave").click(function () {
        
        SaveSiteSettings();
    });

    $('#body_Adminbody_cmbDateFormat').change(function () {

        SetFormatExample();
        //changeSubscriptiondates();
    });

    //SetFormatExample();
});

// to change subscription dates on format change
function changeSubscriptiondates() {
   
    var substrtDate = $('#body_Adminbody_txtSubscriptionStart').val();
    var subEndDate = $('#body_Adminbody_txtSubscriptionEnd').val();
    var Dateformat = $('#body_Adminbody_cmbDateFormat option:selected').val();
   
    $('#body_Adminbody_txtSubscriptionStart').val(formattedDate(substrtDate, Dateformat));
    $('#body_Adminbody_txtSubscriptionEnd').val(formattedDate(subEndDate,Dateformat));
    //document.getElementById('lblEndDate').innerHTML = '(' + Dateformat + ')';
    //document.getElementById('lblstartDate').innerHTML = '(' + Dateformat + ')';
    
    
}
function formattedDate(date,format) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    var monthMM = ("0" + month).slice(-2);
    var monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][d.getMonth()];
    day = ("0" + day).slice(-2);
    var shortyear=  ("0" +year).slice(-2);
    var val = format;
    var convertedDate = val == '' ? '' :
        val == 'dd/MM/yy' ? day + '/' + monthMM + '/' + shortyear :
        val == 'MM/dd/yy' ? monthMM + '/' + day + '/' + shortyear :
        val == 'MMM dd yyy' ? monthName+' '+day+' '+year :
        val == 'MMM dd yyyy' ? monthName + ' ' + day + ' ' + year :
        val == 'dd-MM-yy' ? day + '-' + monthMM + '-' + shortyear :
        val == 'dd-M-yy' ? day+'-'+month+'-'+shortyear : ''
       return convertedDate;
}

//To show date format example
function SetFormatExample() {
    
    var val = $('#body_Adminbody_cmbDateFormat option:selected').val();

    var exampleFormat=  FormatDate(new Date(), val);

        
    if (val == '')
        $('#spanExampleDateFormat').hide();
    else
        $('#spanExampleDateFormat').show();

    $('#DateFormatExample').html(exampleFormat);
}

function PopulateSiteSetting()
{

    //debugger;

    var siteSettingData = new Object();
    siteSettingData.companyId = GetCompanyId();
    var data = JSON.stringify(siteSettingData);

    var serviceUrl = _baseURL + 'Services/SiteSettings.svc/GetSiteSettingData';
    AjaxPost(serviceUrl, data, eval(GetSiteSettingDataSuccess), eval(GetSiteSettingDataFail));
}

function GetSiteSettingDataSuccess(result)
{
    
    var resultData = ParseToJSON(result.d);
    if ((resultData[0].TimeFormat) == false)
        $('#body_Adminbody_cmdTimefFormat').val('24');
    else
        $('#body_Adminbody_cmdTimefFormat').val('12');

    $('#body_Adminbody_chkTimeFormat').attr('checked', resultData[0].TimeFormat);
    //$('#body_txtDateFormat').val(resultData[0].DateFormat);
    $('#body_Adminbody_cmbDateFormat').val(resultData[0].DateFormat);
    //document.getElementById('lblEndDate').innerHTML = '(' + resultData[0].DateFormat + ')';
    //document.getElementById('lblstartDate').innerHTML = '(' + resultData[0].DateFormat + ')';
    $('#body_Adminbody_txtTimeOut').val(resultData[0].ApplicationTimeOut);
    $('#body_Adminbody_cmbPasswordPolicy').val(resultData[0].PasswordPolicyType);
    $('#body_Adminbody_cmbPasswordExpiration').val(resultData[0].PasswordValidity);
    SetFormatExample();

  //  $('.mydatepickerclass').datepicker({ altFormat: resultData[0].DateFormat });
    //$.datepicker.setDefaults($.datepicker.regional['en']);
    //$('.mydatepickerclass').datepicker('option', {
    //    dateFormat: 'mm/dd/yyyy'
    //});
}

function GetSiteSettingDataFail()
{
    alert('Service failed.');
}


function SaveSiteSettingDataSuccess() {
    
    NotificationMessage('Settings saved successfully', 'jSuccess', true, 1000);
}

function SaveSiteSettingDataFail()
{
    alert('Service failed.');
}



function SaveSiteSettings() {
   
    var siteSettingData = new Object();
    //siteSettingData.dateFormat = $('#body_txtDateFormat').val();
    siteSettingData.dateFormat = $('#body_Adminbody_cmbDateFormat').val();
    if ($('#body_Adminbody_cmdTimefFormat').val() == '24')
        siteSettingData.isTwelveHourFormat = false;
    else
        siteSettingData.isTwelveHourFormat = true;

    //siteSettingData.isTwelveHourFormat = $("#body_Adminbody_chkTimeFormat").is(':checked');

    siteSettingData.applicationTimeOut = $('#body_Adminbody_txtTimeOut').val();
    siteSettingData.companyId = GetCompanyId();

    var data = JSON.stringify(siteSettingData);
    var serviceUrl = _baseURL + 'Services/SiteSettings.svc/SaveSiteSettingData';
    AjaxPost(serviceUrl, data, eval(SaveSiteSettingDataSuccess), eval(SaveSiteSettingDataFail));

}