﻿

$(document).ready(function () {
    $('#btnUserUploadCancel').click(function () {
        var id = parent.$('#uploadUsersModal');
        id.bind("dialogclose", function (event, ui) {
        });

        self.close();
        parent.$('#uploadUsersModal').hide();
        parent.$("#dialog:ui-dialog").dialog("destroy");
        parent.$("#uploadUsersModal").dialog("destroy");
    });
});


function closeUploadUserPopUp() {

    var id = parent.$('#uploadUsersModal');
    id.bind("dialogclose", function (event, ui) {
    });

    self.close();
    parent.$('#uploadUsersModal').hide();
    parent.$("#dialog:ui-dialog").dialog("destroy");
    parent.$("#uploadUsersModal").dialog("destroy");
}
