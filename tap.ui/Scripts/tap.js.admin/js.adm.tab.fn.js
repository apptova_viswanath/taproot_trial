﻿//A JS CLASS USED BY THE ADMIN > CUTOM TAB PAGE > CURD OPERATION

//******************************
//***DELETE TAB FUNCTIONLITY
//******************************


//Delete tab
function DeleteTab() {    
    if (Confirmation('Are you sure you want to delete this tab and all associated fields?')) {
        var serviceURL = _baseURL + 'Services/Tabs.svc/DeleteTabByID';
        var data = '&tabId=' + _tabId + '&userId=' + $('#hdnUserId').val();
        Ajax(serviceURL, data, eval(DeleteTabSuccess), eval(DeleteTabFail), 'Test');

    }
}


//On success of deleteTab
function DeleteTabSuccess() {


    //Show success message
    NotificationMessage('Tab deleted successfully.', 'jSuccess', true, 1000);

    //Get active module name and related link name
    GetActiveModuleIdAndName();
    var activeModuleLink = GetCurrentlyActiveModuleLink();

    //fire the click event of that currently active link, to refresh the page
    $(activeModuleLink).click();

}


//On fail of deleteTab
function DeleteTabFail() {
    NotificationMessage('Error in deleting the tab.', 'JError', true, 3000);
}


//******************************
//*** SAVE TAB FUNCTIONALITY
//******************************


//Function For inserting TabDetails
function SaveTab() {

    var mainTabName = GetModuleName(2);

    var _data = JSON.stringify(GetTabDetailsFromPage());

    if (ValidateForm()) {

        if (_data != "false") {

            _serviceURL = _baseURL + 'Services/Tabs.svc/SaveTab';
            AjaxPost(_serviceURL, _data, eval(InsertTabSuccess), eval(InsertServiceTabError));

        }
    }
}

function SaveCustomTabsOrder(moduleid, tabsorder) {

    if (moduleid != null && tabsorder != null) {
        var moduleId = moduleid;
        var tabsOrder = tabsorder;

        var tabDetail = {};
        tabDetail.moduleID = moduleid;
        tabDetail.tabsOrder = tabsorder;

        var _data = JSON.stringify(tabDetail);

        _serviceURL = _baseURL + 'Services/Tabs.svc/SaveTabsOrder';

        AjaxPost(_serviceURL, _data, eval(InsertTabsOrderSuccess), eval(InsertServiceTabsOrderError));
    }

}


//Get the form values and pass to the function to build a new tab object
function GetTabDetailsFromPage() {
    var txtTabName = 'txtTabName';
    var validationImage = 'validationTabNameImg';
    var tabId = 0;
    var selectedModuleName = '';

    if (_tabId != null && _tabId != 0 && _tabId != '') {
        tabId = _tabId;
    }

    var tabName = $('#txtTabName').val().trim();

    var isUniversal = false;
    var active = $('#chkActive').attr('checked') ? 1 : 0;

    var restrictedAccess = $('#chkRestrictedAccess').attr('checked') ? 1 : 0;

    var selectedModuleName = GetSelectedModuleName();

    var tabDetail = new Tabdetail(tabId, tabName, isUniversal, active, selectedModuleName, restrictedAccess);

    return tabDetail;
}


//Get selected module name for teh new tab
function GetSelectedModuleName() {

    var selectedModule = '';

    selectedModule = 'Incident:' + $('#chkIncident').attr('checked');
    selectedModule += ',' + 'Investigation:' + $('#chkInvestigation').attr('checked');
    selectedModule += ',' + 'Audit:' + $('#chkAudit').attr('checked');
    selectedModule += ',' + 'Action Plan:' + $('#chkCAP').attr('checked');

    return selectedModule;
}


//Creating the Tabclass object- Set the properties
function Tabdetail(tabId, tabName, isUniversal, active, selectedModuleName, restrictedAccess) {
    this.tabId = tabId;
    this.companyID = GetCompanyId(); //remove hard code
    this.tabName = tabName;
    this.currentModuleName = _activeModuleName;
    this.isUniversal = isUniversal;
    this.isSystem = 0;
    this.sortOrder = 1;
    this.active = active;
    this.selectedModuleName = selectedModuleName;
    this.baseURL = _baseURL;
    this.restrictedAccess = restrictedAccess;
    this.userId = $('#hdnUserId').val();
}

//Validate form
function ValidateForm() {

    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;

    var tabName = $('#txtTabName').val().trim();
    var isModuleAssigned = (($('#chkShowAllTab').attr('checked')) || $('#chkIncident').attr('checked') || ($('#chkInvestigation').attr('checked')) || ($('#chkAudit').attr('checked'))
                        || ($('#chkCAP').attr('checked')));

    errorMessage += ((tabName == '') ? 'Tab Name,\n' : '');
    errorMessage += ((!isModuleAssigned) ? 'Module' : '');


    var txtTabName = 'txtTabName';
    var txtModuleName = 'lblCAP';


    if (tabName == null || tabName.length == 0) {
        ShowHideWarningMessage(txtTabName, 'Please specify Tab Name', true);
    }
    else {

        ShowHideWarningMessage(txtTabName, 'Please specify Tab Name', false);
    }

    if (!isModuleAssigned) {
        ShowHideWarningMessage(txtModuleName, 'Please select atleast one Module', true);
    }
    else {
        ShowHideWarningMessage(txtModuleName, 'Please select atleast one Module', false);
    }


    if (errorMessage.length > primaryErrorMessage.length) {

        //remove last comma if it is there
        errorMessage = errorMessage.replace(/,(\s+)?$/, '');

        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    }
    return true;
}


//On error of service tab
function InsertServiceTabError(msg) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}

//On error of service tabsOrder
function InsertServiceTabsOrderError(msg) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}
//Used to set the css class for the current module related link
function HighLightCurrentTab() {


    //fetch Module name from dropdown (if we create new tab from Incident module and select CAP then highlight will not work)

    GetActiveModuleIdAndName();
    var moduleLink = GetCurrentlyActiveModuleLink();

    $(moduleLink)[0].offsetParent.className = 'ui-state-default ui-corner-top ui-tabs-selected ui-state-active';

    if (_activeModuleId != 1) {

        $('#adminTabs li')[0].className = '';
        $('#adminTabs li')[0].className = 'ui-state-default ui-corner-top';
    }
}

// function called on success of web service method InsertServiceTab
function InsertTabSuccess(result) {

    var mainTab = '';
    var subTab = '';

    if (result != null) {

        result = result.d;
        var errorMessage = result[0] + '.';
        var errorMessageType = (errorMessage.indexOf('exists') > 0) ? 'jError' : 'jSuccess';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);

        mainTab = result[1];
        subTab = result[2];
        _customTabId = result[3];

        if (mainTab != "-1") {

        }

        //for activity log

        if (errorMessageType != 'jError') {


            var logDetails = $('#txtTabName').val() + '  ' + result[0];
            SaveActivityLog(GetCompanyId(), 0, GetUserId(), 1, logDetails);
            var moduleName = (($('#chkIncident').attr('checked') == 'checked') ? 'Incident'
                                                                : ($('#chkInvestigation').attr('checked') == 'checked') ? 'Investigation'
                                                                : ($('#chkAudit').attr('checked') == 'checked') ? 'Audit' : 'Action Plan');


            if ($('#chk' + _activeModuleName).attr('checked') == 'checked') {
                moduleName = _activeModuleName;
            }


            var divisionId = $('#hdnDivisionId').val();
            if (divisionId > 0)
                window.location.href = _baseURL + "Admin/Division/" + divisionId + "/" + moduleName.replace(/ /g, '') + "/" + $('#txtTabName').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-') + "-" + _customTabId;
            else
                window.location.href = _baseURL + "Admin/Configuration/" + moduleName.replace(/ /g, '') + "/" + $('#txtTabName').val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-') + "-" + _customTabId;




        }

    }



}

// function called on success of web service method InsertServiceTab
function InsertTabsOrderSuccess(result) {
    if (result != null) {
        NotificationMessage('Tabs order saved successfully.', 'jSuccess', true, 1000);
    }
}


//******************************
//*** Building tab
//******************************
function GetModuleName(position) {
    var url = window.location.href.split('/');
    var mainTabName = url[url.length - position];
    return mainTabName;
}

//Jquery Tabs
$(function () {
    //on current tab click

    //generate tabs
    $('#adminTabs').tabs();

    var customTab = "Custom-Tabs";

    var url = window.location.href.split('/');
    var moduleNamePosition = (url[url.length - 1] != customTab && url[url.length - 2] != customTab) ? 2 : 1;
    var subModuleNamePosition = 1;

    //highlight current tab    
    var moduleName = GetModuleName(moduleNamePosition);
    var subModuleName = GetModuleName(subModuleNamePosition);


    if (moduleName == customTab && (subModuleName != undefined)) {
        moduleName = subModuleName;
    }

    var moduleId = (moduleName == "Custom-Tabs" ? 1 : moduleName == "Incident" ? 1 : moduleName == "Investigation" ? 2 : moduleName == "Audit" ? 3 : 4);

    if ($('#lnkmain' + moduleId)[0] != undefined) {
        $('#lnkmain' + moduleId)[0].offsetParent.className = 'ui-state-default ui-corner-top ui-tabs-selected ui-state-active';
    }

    //remove default Tab selection color
    if (moduleId != 1) {

        if ($('#adminTabs li')[0] != undefined) {
            $('#adminTabs li')[0].className = '';
            $('#adminTabs li')[0].className = 'ui-state-default ui-corner-top';
        }

    }
});

