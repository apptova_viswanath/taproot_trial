﻿
//General function to create an toolbar icon.
function ToolbarIconCreate(id, imagePath, srcAlternate, title, classname, clickFunctionName) {
    
    var elem = document.createElement("img");
    elem.setAttribute('id', id);
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + imagePath);
    elem.setAttribute("alt", srcAlternate);
    elem.className = classname;
    elem.setAttribute('title', title);

    elem.setAttribute('onclick', clickFunctionName);
    return elem;
}

//Enable the Event and Condition icons.
function EventConditionIconsDisplay(graph) {   
    
    if (graph.getSelectionCount() == 1 && (graph.getSelectionCell().style.match(/rectangle;/) || graph.getSelectionCell().style.match(/rounded=1;?/))) {
        //Enable the icons
        ToolbarIconsByShapeSelectionDisplay('eventIcon',true);
        ToolbarIconsByShapeSelectionDisplay('conditionIcon',true);
    }
}

//Create font color icon in the toolbar
function FontColorInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'fontIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/font.png');
    elem.setAttribute("alt", "Change the Font color");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Change the Font Color');

    elem.setAttribute('onclick', 'FontColorChange(editui)');
    return elem;
}

//Font color icon, click action functionality 
function FontColorChange(ui) {
    ui.menus.pickColor(mxConstants.STYLE_FONTCOLOR, 'forecolor', '000000');
}

//Creation of change to Condition button in toolbar
function ConditionInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'conditionIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/condition.png');
    elem.setAttribute("alt", "Change to Condition");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Change to Condition');

    elem.setAttribute('onclick', 'SelectedShapeChange(editui.editor.graph, "tocurvedrectangle")');
    return elem;
}

//Creation of change to Event button in toolbar
function EventInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'eventIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/event.png');
    elem.setAttribute("alt", "Change to Event");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Change to Event');

    elem.setAttribute('onclick', 'SelectedShapeChange(editui.editor.graph, "torectangle")');
    return elem;

}

//Create an causal factor icon in toolbar
function CausalfactorInToolbarCreate(editui) {
    var elem = document.createElement("img");
    elem.setAttribute('id', 'causalfactorIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/causal-factor.png');
    elem.setAttribute("alt", "CausalFactor");
    elem.className = 'geButton mxDisabled mxIconDisabled';
    //elem.className = 'geButton geDisabled';
    elem.setAttribute('title', 'Causal Factor');

    //Make only one event.
    elem.setAttribute('onclick', 'CausalfactorApply(editui.editor);'); // for FF
    elem.onclick = function () { CausalfactorApply(editui.editor); }; // for IE

    return elem;
}

//Make/Remove causal factor for the shapes with safeguarding .
function CausalfactorApply(editor) {
    if (editor.graph.getSelectionCells().length == 1) {

        var cell = editor.graph.getSelectionCell();

        //Restricting the shapes like Incident, label control, connector and the three safeguards that doesn't require assumption .
        if (cell.style.match(/shape=image;?/) || cell.style.match(/text;?/) || cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/)
                 || cell.style.match(/ellipse;?/) != null) {
            return;
        }

        CellCausalfactorMake();
    }
}

//Enable/disable the Causalfactor icon .
function CausalfactorBySelectionDisplay(CausalfactorId, flag) {

    if (flag) {
        $('#' + CausalfactorId).removeClass('geButton mxDisabled mxIconDisabled');
        $('#' + CausalfactorId).addClass('geButton');
    }
    else {
        $('#' + CausalfactorId).removeClass('geButton');
        $('#' + CausalfactorId).addClass('geButton mxDisabled mxIconDisabled');
    }
}

//Toggle the causalfactor icons based on the current icon.
function CausalfactorsFlag() {
    var flag = $('#causalfactorIcon').attr('src').match(/causal-factor_on/) ? false : true;
    CausalfactorIconsToggle(flag);
}

//Toggle the Causalfactor icons
function CausalfactorIconsToggle(flag) {
    if (flag)
        $('#causalfactorIcon').attr("src", _baseURL + 'Images/safeguards/causal-factor_on.png');
    else
        $('#causalfactorIcon').attr("src", _baseURL + 'Images/safeguards/causal-factor.png');
}

//Create an assumption icon in toolbar
function AssumptionInToolbarCreate(editui) {
    var elem = document.createElement("img");
    elem.setAttribute('id', 'assumptionIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/Assumption.png');
    elem.setAttribute("alt", "Assumption");
    elem.className = 'geButton mxDisabled mxIconDisabled';
    //elem.className = 'geButton geDisabled';
    elem.setAttribute('title', 'Assumption');

    //Make only one event.
    elem.setAttribute('onclick', 'AssumptionApply(editui.editor);'); // for FF
    elem.onclick = function () { AssumptionApply(editui.editor); }; // for IE

    return elem;
}

//Make/Remove assumption for the shapes with safeguarding .
function AssumptionApply(editor) {


    if (editor.graph.getSelectionCells().length == 1) {

        var cell = editor.graph.getSelectionCell();

        //Restricting the shapes like Incident, label control, connector and the three safeguards that doesn't require assumption .
        if (cell.style.match(/shape=image;?/) || cell.style.match(/text;?/) || cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/)
                 || cell.style.match(/ellipse;?/) != null) {
            return;
        }

        CellAssumptionMake();
    }
}

//Enable/disable the Assumption icon .
function AssumptionIconBySelectionDisplay(AssumptionId, flag) {

    if (flag) {
        $('#' + AssumptionId).removeClass('geButton mxDisabled mxIconDisabled');
        $('#' + AssumptionId).addClass('geButton');
    }
    else {
        $('#' + AssumptionId).removeClass('geButton');
        $('#' + AssumptionId).addClass('geButton mxDisabled mxIconDisabled');
    }
}

//Toggle the assumption icons based on the current icon.
function AssumptionsFlag() {
    var flag = $('#assumptionIcon').attr('src').match(/Assumption_on/) ? false : true;
    AssumptionIconsToggle(flag);
}

//Toggle the Assumption icons
function AssumptionIconsToggle(flag) {
    if (flag)
        $('#assumptionIcon').attr("src", _baseURL + 'Images/safeguards/Assumption_on.png');
    else
        $('#assumptionIcon').attr("src", _baseURL + 'Images/safeguards/Assumption.png');
}

//Enable and disable the date icon.
function DateIconBySelectionDisplay(dateIconId, flag) {

    if (flag) {
        $('#' + dateIconId).removeClass('geButton mxDisabled mxIconDisabled');
        $('#' + dateIconId).addClass('geButton');
    }
    else {
        $('#' + dateIconId).removeClass('geButton');
        $('#' + dateIconId).addClass('geButton mxDisabled mxIconDisabled');
    }
    //if (flag) {
    //    $('#' + dateIconId).removeClass('geButton geDisabled');
    //    $('#' + dateIconId).addClass('geButton');
    //}
    //else {
    //    $('#' + dateIconId).removeClass('geButton');
    //    $('#' + dateIconId).addClass('geButton geDisabled');
    //}
}

//Creation of Date-time button in tool bar.
function DatetimeInToolbarCreate(editui) {
    var elem = document.createElement("img");
    elem.setAttribute('id', 'dateIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/date.png');
    elem.setAttribute("alt", "Date and Time");
    elem.className = 'geButton mxDisabled mxIconDisabled';
    //elem.className = 'geButton geDisabled';
    elem.setAttribute('title', 'Select Date');
    //elem.setAttribute("onmouseover", 'DateHoverChange(this)');

    elem.setAttribute('onclick', 'DateTimeApply(editui.editor);'); // for FF
    elem.onclick = function () { DateTimeApply(editui.editor); }; // for IE

    return elem;
}

//
function DateTimeApply(editor) {

    if (editor.graph.getSelectionCells().length == 1) {

        var dateFormat = $('#hdnDateFormat').val();
        if (dateFormat != null && dateFormat.match(/yyyy/)) {
            dateFormat = dateFormat.replace('yyyy', 'yy');
        }

        var dateDialogMarkup = '<div id="dateBox" class="DateTimeDialog" ><p tabindex="1"></p><label class="DialogLabel" >Date : </label><input type="text" id="dateField" readonly="readonly" class="DialogInputTextBox" ><br><br><label class="DialogLabel" >Time : </label>'
            + '<input type="text" id="timeField" class="DialogInputTextBox" onblur="javascript:FormatMilitaryTime(' + '\'' + "timeField" + '\'' + ')" >' + '</div>';
        var dateDiv = $(dateDialogMarkup);
        dateDiv.dialog({
            title: "Set Date and Time ",
            modal: true,
            width: 350,
            open: function (event, ui) {
                event.preventDefault();
                $("#dateField").blur();
            },
            buttons: [
                { text: "Apply", id: "applyChanges", click: function () { DateTimeGet(this, editor); } },
                { text: "Cancel", id: "cancelChanges", click: function () { DateTimeDialogClose(this, 'dateField', 'timeField'); } },
                { text: "Remove Date and Time", id: "removeDateandTime", click: function () { DateTimeRemove(this, editor, 'dateField', 'timeField'); } }
            ],
            close: function (event, ui) {
                DateTimeDialogClose(this, 'dateField', 'timeField');
            }
        });

        // for adding default custom button style by deepthi
        $("#applyChanges, #cancelChanges, #removeDateandTime").removeClass().addClass("btn btn-primary btn-small");
        $("#applyChanges, #cancelChanges, #removeDateandTime").button().hover(function (e) {
            $(this).removeClass("ui-state-hover");
        });
        //no hover effect in IE9 as its not proper
        if (isIE9() != 9) {
            $("#applyChanges, #cancelChanges, #removeDateandTime").hover(function () {
                $(this).css('background-color', '#0044cc');
            });
        }
        //TODO : Apply styles for the buttons.
        //$('.ui-dialog-buttonpane').find('button:contains("Cancel")')[0].className = 'btn btn-primary btn-small ';
        //$('.ui-dialog-buttonpane').find('button:contains("Apply Changes")')[0].className = 'btn btn-primary btn-small';        


        //Set datepicker to the date field.
        $('#dateField').datepicker({ maxDate: '+0d', dateFormat: localStorage.dateFormat });
        //$('#dateField').datepicker({ maxDate: '+0d', dateFormat: sessionStorage.dateFormat });


        //Position the date popup exactly below the Date textbox.
        $('#dateField').focusin(function () {
            $('#dateField').datepicker('widget').css({ "font-size": "12.3px", position: "fixed" });
        });

        $("#ui-datepicker-div").addClass("notranslate");

        //Get Date and Time and display on top of the seleted shape.
        function DateTimeGet(thisDate, editor) {
            //Validation            
            if ($('#timeField').next().hasClass('warningImage')) {
                return;
            }

            //Combine the date and time values to display on the selected shape.
            var dateTimeValue = TrimString($('#dateField').val()) + ' ' + TrimString($('#timeField').val()); //TODO: Trim the values

            //Display the selected date and time on the selected shape.      
            var cell = editor.graph.getSelectionCell();
            if (cell.getChildCount() > 0) {
                var dateTimecellStyle = 'verticalAlign=bottom;editable=0;selectable=0';

                if (cell.getChildCount() == 1) {

                    if (cell.children[0].style == dateTimecellStyle) {//Update the date on the shape
                        cell.children[0].value = dateTimeValue;
                    }
                    else {//Shape consists of Causalfactor
                        //Add new child cell to the selected shape.
                        DateTimeCellAdd(editor, dateTimeValue);
                    }
                }

                if (cell.getChildCount() == 2) {
                    var child = cell.children[0].style == dateTimecellStyle ? 0 : 1;
                    cell.children[child].value = dateTimeValue;//Update the selected date
                }
                editor.graph.refresh();
            }
            else { //Add new child cell to the selected shape.
                DateTimeCellAdd(editor, dateTimeValue);
            }

            //Closes the dialog after setting the date and time to the selected shape.
            DateTimeDialogClose(thisDate, 'dateField', 'timeField');
        }

        //Removes Date and Time from the selected shape .
        function DateTimeRemove(thisData, editor, dateId, timeId) {

            //Clear the field values
            $('#' + dateId).val('');
            $('#' + timeId).val('');

            //Removes the selected cell .
            CausalfactorCellStyleRevert(editor.graph, editor.graph.getSelectionCell().children[0]);

            //Close the dialog
            DateTimeDialogClose(thisData, dateId, timeId);
        }


        //Pre display the date and time of the selected shape in the respective fields.
        if (editor.graph.getSelectionCell().children != null && editor.graph.getSelectionCell().children.length > 0) {

            var value = TrimString(editor.graph.getSelectionCell().children[0].value);

            var shapeDate = value;
            var shapeTime = '';

            if (value.match(/:/)) {
                shapeDate = TrimString(value.substring(0, value.indexOf(":") - 2));
                shapeTime = value.substring(value.indexOf(":") - 2), value.length - 1;
            }

            $('#dateField').val(shapeDate);
            $('#timeField').val(shapeTime);
        }

        //Enable/disable the button, Remove Date and Time 
        TrimString($('#dateField').val()) != '' || TrimString($('#timeField').val()) != '' ? $('#removeDateandTime').button("enable") : $('#removeDateandTime').button("disable");

    }

    return false;
}

//Adds new child cell, Date and Time cell to the selected shape.
function DateTimeCellAdd(editor, dateTimeValue) {
    var dateTimeValueLabel = editor.graph.insertVertex(editor.graph.getSelectionCells()[0], null, dateTimeValue, 0.5, 0, 0, 0,
                    'verticalAlign=bottom;editable=0;selectable=0', true);
}

//Closes the  Date and time dialog
function DateTimeDialogClose(thisDialog, dateId, timeId) {
    $('#' + dateId).remove();
    $('#' + timeId).remove();

    $(thisDialog).dialog("destroy");
}

//Not in use
function DateHoverChange(element) {
    //Get the Date time icon for hover and display.
    //debugger;
}

//Remove the unnecessary icons from the tool bar.
function ToolbarNonUseIconsRemove() {
    $('.geSprite-gradientcolor').parent().remove();//Removes the gradient color icon
    $('.geSprite-shadow').parent().remove();//Removes the shadow icon
    $('.geSprite-image').parent().remove();//Removes the image upload icon
    $('.geSprite-left').parent().remove();//Removes the Align icon
    //Removes the library Font color icon to replace the customized icon
    $('.geSprite-fontcolor').parent().remove();


    //LVC    
    $('.geSprite-strokecolor').parent().prev().prev().prev().remove();//Removes separator    

    //LVC 
    $('.geSprite-orthogonal').parent().remove();//Removes the lines collection
    $('.geSprite-straight').parent().remove();//Removes the lines collection
    $('.geSprite-startclassic').parent().remove();//Removes the starting arrow collection
    $('.geSprite-endclassic').parent().remove();//Removes the ending arrow collection

    //LVC
    //Removes the separator beside the line related icons.
    $('.geSprite-strokecolor').parent().prev().remove();

    //Fix for IPAD toolbar icon touch issues 
    /////////////////////////////////////////////////////////////////////    
    if (mxClient.IS_IE || mxClient.IS_IE11) {
        return false;
    }
    ///////////////////////////////////////////////////////////////////
}

//Rearrange the toolbar icons
function ToolbarIconsRearrange() {

    //Move the save icon to the very first of the tool bar. //strict
    $('#saveIcon').insertBefore('a[title="Undo"]');

    $('#cutIcon').insertBefore('a[title="Delete"]');
    $('#copyIcon').insertBefore('a[title="Delete"]');
    $('#pasteIcon').insertBefore('a[title="Delete"]');
    $('#printIcon').insertAfter('#saveIcon');

    ////Rearrange the toolbar icons
    //$('#settingsIcon').insertBefore('#saveIcon');
    //$('#Separator').insertAfter('#settingsIcon');

    $('a[title="Grid"]').insertAfter('a[title="Zoom out"]');
    $('a[title="Guides"]').insertAfter('a[title="Grid"]');

    //Remove the unnecessary icons from the tool bar.    
    ToolbarNonUseIconsRemove();

    $('a[title="Hyperlink..."]').remove().insertAfter('a[title="Fill color..."]');
    $('a[title="Line color..."]').remove().insertAfter('a[title="Fill color..."]');

    $('#fontIcon').insertAfter('a[title="Underline"]');

    $('#Separator').insertAfter('#CaptureIcon');
}

//Create an object,  edgePoint with essential properties.
function EdgePointCreate(edge) {

    var edgePoint = {};
    edgePoint.ys = edge.source.geometry.y;
    edgePoint.yt = edge.target.geometry.y;
    edgePoint.xs = edge.source.geometry.x;
    edgePoint.xt = edge.target.geometry.x;

    //Check and delete the below 4 properties.
    edgePoint.midDownX = edge.source.geometry.width / 2;
    edgePoint.midDownY = edge.target.geometry.height / 2;
    edgePoint.midUpX = edge.target.geometry.width / 2;
    edgePoint.midUpY = edge.source.geometry.height / 2;
    ///////////////////////////////////////////////////////////   

    edgePoint.souWidth = edge.source.geometry.width;
    edgePoint.souHeight = edge.source.geometry.height;
    edgePoint.tarWidth = edge.target.geometry.width;
    edgePoint.tarHeight = edge.target.geometry.height;

    edgePoint.mpSouW = edge.source.geometry.width / 2;
    edgePoint.mpTarH = edge.target.geometry.height / 2;
    edgePoint.mpTarW = edge.target.geometry.width / 2;
    edgePoint.mpSouH = edge.source.geometry.height / 2;

    return edgePoint;
}

//General function to enable/disable the toolbar icons .
function ToolbarIconsByShapeSelectionDisplay(iconId, flag) {

    if (flag) {
        $('#' + iconId).removeClass('geButton mxDisabled mxIconDisabled');
        $('#' + iconId).addClass('geButton');
    }
    else {
        $('#' + iconId).removeClass('geButton');
        $('#' + iconId).addClass('geButton mxDisabled mxIconDisabled');
    }
}

//Merge the menubar into the toolbar .
function ToolbarMenusMerge(thistoolbarmenu) {
    thistoolbarmenu.toolbarContainer.appendChild(thistoolbarmenu.menubar.container);
}

//Display the event name on the snapchart .
function EventNameDisplay() {

    var title = document.title;

    if(title != null && title != undefined && title.length > 12)
        title = title.substring(12, title.length);

    $('<span/>', {
        'id': 'spanEventName',
        'style': 'font-size:18px;font-weight:bold;',
        'text': title,
    }).appendTo($('.geMenubarContainer'));

    $('.geMenubarContainer').css({ 'text-align': 'center' });    
}

//Change the Preference menu as the Settings icon .
function PreferencesAsSettingsChange() {

    var preferencesMenu = $("a.geItem:contains('Preferences')");    
    //preferencesMenu[0].style.float = 'right';
    preferencesMenu[0].innerHTML = '<img id="settingsIcon" href="javascript:void(0);" src=' + _baseURL + 'Images/settings-black.png alt="Settings of SnapCharT®" class="geButton" title="Settings">';    
    // preferencesMenu[0].innerHTML = $('#settingsIcon')[0].outerHTML;
}

//Enable the cut and copy icons
function CutCopyIconsDisplay(graph){
    if (graph.getSelectionCount() > 0) {        
        ToolbarIconsByShapeSelectionDisplay('cutIcon', true);
        ToolbarIconsByShapeSelectionDisplay('copyIcon', true);
    }
}

//Cut operation
function CutApply(graph) {
    if(graph.getSelectionCount() > 0)
        mxClipboard.cut(graph);
}

//Copy operation
function CopyApply(graph) {
    if (graph.getSelectionCount() > 0)
        mxClipboard.copy(graph);
}

//Paste operation
function PasteApply(graph) {    
        mxClipboard.paste(graph);
}

//Print operation
function PrintApply(ui) {
    ui.showDialog(new PrintDialog(ui).container, 300, 120, true, true);
}


//Container for Seasons in the sidebar .
function NewSeasonContainerInSidebar(editui) {
    
    var newseasonBar = DataContainerMake();
    newseasonBar.setAttribute('id', 'newseasonbarContainer');
    newseasonBar.setAttribute("style", "color:gray; height:80px; font-color:black; font-size:16px;");
    //newseasonBar.setAttribute("style", "color:gray; width:203px; height:80px; font-color:black; font-size:16px;");
    editui.newseasonContainer = newseasonBar;    
    editui.sidebar.addPalette('changeseasons', 'Seasons', true, function (content) { content.appendChild(editui.newseasonContainer); });

    var seasonWidth = screen.width + 'px';    
    //Create divs for the 4 seasons and append to the new season container .
    
    var springMarkup = '<div id=newSpring class="seasonPadding commonSeason" style="width:' + seasonWidth + '"><div id="springCheck" class="greenCheck"></div> Spring (Plan)</div>';
    var summerMarkup = '<div id=newSummer class="seasonPadding commonSeason" style="width:' + seasonWidth + '"><div id="summerCheck" class="greenCheck"></div> Summer (Investigate)</div>';
    var autumnMarkup = '<div id=newAutumn class="seasonPadding commonSeason" style="width:' + seasonWidth + '">Autumn (Analyze)</div>';
    var winterMarkup = '<div id=newWinter class="seasonPadding " style="display:inline-block; width:' + seasonWidth + '">Winter (Report)</div>';

    var seasoncontainerMarkup = springMarkup + summerMarkup + autumnMarkup + winterMarkup;    
    $('#newseasonbarContainer').append(seasoncontainerMarkup); 

    //Background colors based on the seasons
    $('#newSpring').css({ "background-color": "#EFEA72" });
    $('#newSummer').css({ "background-color": "#60E237" });
    $('#newAutumn').css({ "background-color": "#F6B144" });
    $('#newWinter').css({ "background-color": "#EFEFEF" });    

    $('#newSpring').click(function () {

        //Validate the season clicks .
        if (_selectedSeason == SPRING_SEASON || seasonId == SPRING_SEASON)
            return;

        //Removes the green checkmark .
        if ($('#springGreenCheck').length > 0)
            SeasonChangeRevert('newSpring', 'springGreenCheck');

        if ($('#summerGreenCheck').length > 0)
            SeasonChangeRevert('newSummer', 'summerGreenCheck');

        //Disable the causalfactor icon in the Spring and Summer seasons
        CausalfactorBySelectionDisplay('causalfactorIcon', false);

        // #1 Change the background color of the divider, #2 Updates the season id, #3 Saves the graph in db .
        SnapchartSeasonShow(SPRING_SEASON, 'newSpring', false);
    });

    $('#newSummer').click(function () {

        //Validate the season clicks .
        if (_selectedSeason == SUMMER_SEASON || seasonId == SUMMER_SEASON)
            return;

        //Remove/Add the green checkmark .
        if ($('#summerGreenCheck').length > 0) {
            SeasonChangeRevert('newSummer', 'summerGreenCheck');
        }
        else {
            SeasonChangeUpdate('newSpring', 'springCheck', 'springGreenCheck', 'Images/green-check.png');
        }
         
        //Disable the causalfactor icon in the Spring and Summer seasons
        CausalfactorBySelectionDisplay('causalfactorIcon', false);

        // #1 Change the background color of the divider, #2 Updates the season id, #3 Saves the graph in db .
        SnapchartSeasonShow(SUMMER_SEASON, 'newSummer', false);
        
    });

    $('#newAutumn').click(function () {
        
        //Validate the season clicks .
        if (_selectedSeason == AUTUMN_SEASON || seasonId == AUTUMN_SEASON || $('#springGreenCheck').length == 0)
            return;

        //Add the green checkmark .
        SeasonChangeUpdate('newSummer', 'summerCheck', 'summerGreenCheck', 'Images/green-check.png');

        // #1 Change the background color of the divider, #2 Updates the season id, #3 Saves the graph in db .
        SnapchartSeasonShow(AUTUMN_SEASON, 'newAutumn', false);

    });
    
}

// #1 Change the background color of the divider, #2 Updates the season id, #3 Saves the graph in db .
function SnapchartSeasonShow(currentSeasonId, season, initial) {
    
    //Make the font as bold on selection of the season
    FontBoldChange(season);

    //Background color for the vertical divider based on the season selection .
    $('.geHsplit').css('background-color', SeasonColorByIdGet(currentSeasonId));

    //Change the event name color based on the selected season
    EventnameColorBySeasonChange(currentSeasonId);

    if (!initial) {

        //Track the selected season
        _selectedSeason = currentSeasonId;
        seasonId = currentSeasonId;

        // Save the snapchart
        SnapChartXmlSave(mxUtils.getXml(editui.editor.getGraphXml()),  true);
       
    }
}

// Save the snapchart while changing the seasons
function SnapChartXmlSave(xmlgraph, isParentUpdate) {
    
    isBackgroundRefresh = isParentUpdate;

    SaveStatusDisplay('Saving....');

    var snapchartInfo = new snapchartSaveDetails(eventId, xmlgraph);
    var data = JSON.stringify(snapchartInfo);    
    serviceurl = _baseURL + 'Services/SnapCharT.svc/SnapChartXmlSave';
    AjaxPost(serviceurl, data, eval(SnapChartXmlSaveSuccess), eval(SnapChartXmlSaveFail));
}

function snapchartSaveDetails(eventId, xmlgraph) {
    this.eventID = eventId;
    this.seasonId = seasonId;
    this.snapChartData = xmlgraph;
    //this.userId = userId;
}

function SnapChartXmlSaveSuccess(result) {
    
    //Display the save changes in save status bar.
    SaveStatusDisplay('Changes saved.');

    //Make negative the graph modifications.
    if (_editor != null)
        _editor.modified = false;

    //Display the auto save information, 5 seconds after saving the graph.
    setTimeout(function () {
        SaveStatusDisplay(AutoSaveInfoSet(JSON.parse(readCookie('autosaveOption'))));
    }, 5000);

    //Partially refresh the parent page.
    if (isBackgroundRefresh) {
        BackgroundPagePartialRefresh();
    }

}

function SnapChartXmlSaveFail() { }

//Remove the padding class AND append the green check image .
function SeasonChangeUpdate(seasonDivId, imageDivId, imageId, imagePath ) {
    
    $('#' + seasonDivId).removeClass('seasonPadding');
    var greenCheckImage = '<image id="'+ imageId + '" alt="Green Check" src="' + _baseURL + imagePath + ' ">';
    $('#' + imageDivId).append(greenCheckImage);
}

//Remove the green check image and add back the padding class .
function SeasonChangeRevert(seasonDivId, imageId) {
    $('#' + seasonDivId).addClass('seasonPadding');
    $('#' + imageId).remove();
}

//Get the season color based on the season id .
function SeasonColorByIdGet(currentSeasonId) {
   var seasonColour = currentSeasonId == SPRING_SEASON ? '#EFEA72'
                                        : currentSeasonId == SUMMER_SEASON ? '#60E237'
                                        : currentSeasonId == AUTUMNG_SEASON ? '#F6B144' : '#EFEFEF';
   return seasonColour;
}

//Initial display of the completed season with green check mark .
function GreenCheckmarkDisplay(seasonId) {

    //TODO tomorrow
    //Allow user to click any season if user is in Autumn season
    //Discriminate the seasons , from SPRING > SUMMER > AUTUMN
    //Remove the mouse hover selection
    //Totally remove the OLD season bar

    if (seasonId == SPRING_SEASON) {
        SeasonCursorDiscriminate('newSummer', '');
        return;
    }

    if (seasonId == SUMMER_SEASON) {
        SeasonChangeUpdate('newSpring', 'springCheck', 'springGreenCheck', 'Images/green-check.png');
        SeasonCursorDiscriminate('newSpring', 'newAutumn');
        return;
    }

    if (seasonId == AUTUMNG_SEASON) {
        SeasonChangeUpdate('newSpring', 'springCheck', 'springGreenCheck', 'Images/green-check.png');
        SeasonChangeUpdate('newSummer', 'summerCheck', 'summerGreenCheck', 'Images/green-check.png');
        SeasonCursorDiscriminate('newSpring', 'newSummer');
    }    
}

//Shows the hand cursor indicating the clickable season
function SeasonCursorDiscriminate(clickableSeasonId1, clickableSeasonId2) {
    //First remove the cursor pointer from all the seasons.
    HandCursorRemove();
    
    $('#' + clickableSeasonId1).addClass('cursorPointer');

    if(clickableSeasonId2 != '')
        $('#' + clickableSeasonId2).addClass('cursorPointer');
}

//Removes the hand cursor for the seasons
function HandCursorRemove() {
    $('#newSpring').removeClass('cursorPointer');
    $('#newSummer').removeClass('cursorPointer');
    $('#newAutumn').removeClass('cursorPointer');
    $('#newWinter').removeClass('cursorPointer');
}

//Add the background color to the menubar
function MenubarBackgroundcolorChange() {
    $('.geMenubarContainer').css('background-color', '#EFEFEF');
}

//Change the event name color based on the selected season
function EventnameColorBySeasonChange(currentSeasonId) {
    
    $('#spanEventName').css('background-color', SeasonColorByIdGet(currentSeasonId));

}

//Make the font as bold on selection of the season
function FontBoldChange(season) {

    //Clear all the seasons font bold
    SeasonsFontBoldClear();

    $('#' + season).css('font-weight','bold');
}

//Clear all the seasons font bold
function SeasonsFontBoldClear() {

    $('#newSpring').css('font-weight', 'normal');
    $('#newSummer').css('font-weight', 'normal');
    $('#newAutumn').css('font-weight', 'normal');
    $('#newwinter').css('font-weight', 'normal');
}

//Get the season div id
function SeasonDivIdBySeasonId(currentSeasonId) {
    
    return currentSeasonId == 1 ? 'newSpring' :
            currentSeasonId == 2 ? 'newSummer' :
            currentSeasonId == 3 ? 'newAutumn' : 'newWinter';
}

//Increase the font size of the left side bar section titles
function LeftsideBarTitlesSizeIncrease() {
    $('.geTitle').css('font-size','16px');
}

//Container for the Optional techniques in the sidebar
function OptionalTechniquesContainerInSidebar() {
    var OptionalTechniquesBar = DataContainerMake();
    OptionalTechniquesBar.setAttribute('id', 'optionalTechniquesContainer');
    OptionalTechniquesBar.setAttribute("style", "color:gray; height:80px; color:black; font-size:16px;");
    //OptionalTechniquesBar.setAttribute("class", "leftContainer");
    editui.OptionalTechniquesContainer = OptionalTechniquesBar;
    editui.sidebar.addPalette('OptionalTechniques', 'Optional Techniques', true, function (content) { content.appendChild(editui.OptionalTechniquesContainer); });
}

//Container for the Snapchart Versions in the sidebar
function SnapCharTVersionsContainerInSidebar() {
    var SnapchartVersionsBar = DataContainerMake();
    SnapchartVersionsBar.setAttribute('id', 'optionalTechniquesContainer');
    SnapchartVersionsBar.setAttribute("style", "color:gray; height:80px; color:black; font-size:16px;");
    editui.SnapchartVersionsContainer = SnapchartVersionsBar;
    editui.sidebar.addPalette('SnapchartVersions', 'Versions', true, function (content) { content.appendChild(editui.SnapchartVersionsContainer); });
}

//Get the difference value
function DifferenceValue(opr1, opr2, operator) {
    if (operator == 'deduct') {
        return opr1 - opr2;
    }
    return opr1 + opr2;
}
//Get align position.
function AlignPositionGet(edgePoint, quad) {

    var quadrant = quad == 'up' ? 'top' : 'bottom';
    var ptA = quad == 'up' ? edgePoint.ys : edgePoint.yt;
    var ptB = quad == 'up' ? edgePoint.yt : edgePoint.ys;
    var ptC = edgePoint.xs;
    var ptD = edgePoint.xt;

    var alignPosition = (ptA - ptB <= 60) ? quadrant :
        ptC < ptD ? (ptD - ptC <= 60) ? 'left' : '' :
            (ptC - ptD <= 60) ? 'right' : '';

    //Don't allow the connecting line inside of the shape instead align it . 
    if (edgePoint.xt < edgePoint.xs && edgePoint.xt + edgePoint.tarWidth > edgePoint.xs + edgePoint.mpSouW) {

        alignPosition = 'left'; //horizontal
    }

    if (edgePoint.yt < edgePoint.ys && edgePoint.ys + edgePoint.mpSouH < edgePoint.yt + edgePoint.tarHeight) {

        alignPosition = 'top'; //vertical
    }

    return alignPosition;
}

//Calculate align location.
function AlignAttributeGet(edge, target, graph) {

    var cellsGeoInfo = EdgePointCreate(edge);

    //Geometrical x and y co-ordinates for source and target cells.
    var ys = edge.source.geometry.y;
    var yt = edge.target.geometry.y;
    var xs = edge.source.geometry.x;
    var xt = edge.target.geometry.x;

    //Get the quadrant.
    var quad = ys < yt ? 'down' : 'up';

    //Get the align position. // top, bottom, left and right or '' .
    var aliPos = AlignPositionGet(cellsGeoInfo, quad);

    //Select the attribute to calculate the target co-ordinates .
    var attr = aliPos == 'top' || aliPos == 'bottom' ? 'height' : aliPos == 'left' || aliPos == 'right' ? 'width' : '';

    if (attr != '') {

        var Targeo = edge.target.geometry;
        var Sougeo = edge.source.geometry;
        var add = 'append';
        var substract = 'deduct';

        if (edge.geometry.points != null) {
            edge.geometry.points = null;
        }

        //Align the cells based on cell's width.
        if (attr == 'width') {

            Targeo.x = xt = xs;
            var srcX = xs + cellsGeoInfo.mpSouW;
            var trgX = xt + cellsGeoInfo.mpTarW;

            var operator = trgX > srcX ? substract : add;
            var mpDiff = trgX > srcX ? trgX - srcX : srcX - trgX;

            Targeo.x = DifferenceValue(xt, mpDiff, operator);
        }

        //Align the cells based on cell's height. 
        if (attr == 'height') {

            Targeo.y = yt = ys;
            var srcY = ys + cellsGeoInfo.mpSouH;
            var trgY = yt + cellsGeoInfo.mpTarH;

            var operator = trgY > srcY ? substract : add;
            var mpDiff = trgY > srcY ? trgY - srcY : srcY - trgY;

            Targeo.y = DifferenceValue(yt, mpDiff, operator);
        }

        edge.target.setGeometry(Targeo);

        if (target != '')
            target.setGeometry(Targeo);

        graph.refresh();

    }
    else { //If not in the nearest location to align

        //Connect with customized connecting line.
        EdgeGeometryModify(graph, edge, cellsGeoInfo, quad);
    }
}

//Modify the geometry for the connecting line.
function EdgeGeometryModify(graph, edge, edgePoint, quad) {

    var newPtA = quad == 'up' ? edgePoint.xt : edgePoint.xs;
    var newPtB = quad == 'up' ? edgePoint.ys : edgePoint.yt;
    var newMidPtX = quad == 'up' ? edgePoint.midUpX : edgePoint.midDownX;
    var newMidPtY = quad == 'up' ? edgePoint.midUpY : edgePoint.midDownY;

    graph.setCellStyles('edgeStyle', 'orthogonalEdgeStyle');

    var edgeGeometry = graph.model.getGeometry(edge);
    edgeGeometry = edgeGeometry.clone();
    edgeGeometry.points = [new mxPoint(newPtA + newMidPtX, newPtB + newMidPtY)];
    graph.model.setGeometry(edge, edgeGeometry);
}

//Partially refresh the parent page, refresh the TapRooT tab in the parent page.     
function BackgroundPagePartialRefresh() {
    if (window.opener != null && !window.opener.closed && window.opener.GetInvestigationSession != undefined)
        window.opener.GetInvestigationSession();//To update the season and causal factor in the parent page with out page refreshing.
}

//Check for the original value.
function NullCoelscingCheck(original, custom) {

    if (original === null || original === undefined)
        return custom;

    return original;
}

//Modify the super long word .
function LongWordModify(value) {

    value = value.replace(/(^\s*)|(\s*$)/gi, "");
    value = value.replace(/[ ]{2,}/gi, " ");
    value = value.replace(/\n /, "\n");
    var valueArray = value.split(' ');
    var valuePart = "";

    if (valueArray.length > 0) {
        for (var i = 0; i < valueArray.length; i++) {
            var delimitValue = 17;
            if (valueArray[i].length > delimitValue) {
                var count = Math.floor(valueArray[i].length / delimitValue);
                for (var j = 0; j < count; j++) {
                    valuePart += valueArray[i].substring(j * delimitValue, (j * delimitValue) + delimitValue) + " ";
                }

                valuePart += valueArray[i].substring(count * delimitValue, valueArray[i].length) + " ";
                valueArray[i] = valuePart;
            }
        }
    }

    valueArray = valueArray.slice(0, valueArray.length).join(' ');//Removes commas.
    valueArray = valueArray + '';//Converts array back to string.    
    return valueArray;
}

//Commented because of removing Causalfactor checkbox from the menubar .
////Disable the Causal factor option.
//function CausalfactorDisable() {

//    CausalFactor.children.cbCausalFactor.disabled = true;
//    CausalFactor.children.cbCausalFactor.checked = false;
//    CausalFactor.children.labelCausalFactor.style.color = "gray";
//}

//Hide/Show the other default shapes.
function OtherDefaultShapesHideORShow(shape) {

    //Hide the Event shape.
    if (shape == 'rectangle') {
        $('#ancConditionDefaultShape').hide();
        $('#ancIncidentDefaultShape').hide();
        $('#ancTextDefaultShape').hide();
    }

    //Hide the Condition shape.
    if (shape == 'oval') {
        $('#ancEventDefaultShape').hide();
        $('#ancIncidentDefaultShape').hide();
        $('#ancTextDefaultShape').hide();
    }

    //Hide the Incident shape.
    if (shape == 'circle') {
        $('#ancEventDefaultShape').hide();
        $('#ancConditionDefaultShape').hide();
        $('#ancTextDefaultShape').hide();
    }

    //Hide label, text shape.
    if (shape == 'label') {
        $('#ancEventDefaultShape').hide();
        $('#ancConditionDefaultShape').hide();
        $('#ancIncidentDefaultShape').hide();
    }

    //Show all default shapes.
    if (shape == 'showAllOtherShapes') {

        $('#ancEventDefaultShape').show();
        $('#ancConditionDefaultShape').show();
        $('#ancIncidentDefaultShape').show();
        $('#ancTextDefaultShape').show();
    }

}

//Change the title of the causal factor based on the season.
function CausalFactorTitleSet(currentSeasonId) {
    var causalfactorTitle = currentSeasonId == SPRING_SEASON || currentSeasonId == SUMMER_SEASON ? 'Causal Factor selection is only enabled in Autumn' : '';
    $('#labelCausalFactor').attr('title', causalfactorTitle);
}

//Commented because of removing Causalfactor checkbox from the menubar .
////Enable the causal factor
//function CausalFactorEnable() {

//    CausalFactor.children.cbCausalFactor.disabled = false;
//    CausalFactor.children.labelCausalFactor.style.color = "#000000";
//}

//Commented because of removing Assumption checkbox from the menubar .
////Disable the Assumption.
//function AssumptionDisable() {

//    Assumption.children.cbAssumption.disabled = true;
//    Assumption.children.cbAssumption.checked = false;
//    Assumption.children.labelAssumption.style.color = "gray";
//}

//Makes Landscape page view as default.
function LandscapeDefaultMake(editor, pageView) {

    editor.graph.pageFormat = pageView;

    //editor.outline.outline.pageFormat = pageView;
    //editor.updateGraphComponents();
    //editor.graph.view.validateBackground();
    //editor.graph.sizeDidChange();
    //editor.outline.update();
}

//Make an array which contain the connecting line .
function CellsArrayMake(edge) {

    var cells = new Array;
    cells[0] = edge;
    return cells;
}

//NOT_IN_USE .
//Check the causal factors in the spring and summer season and remove it.
function SpringSummerCausalfactorRemove(editui) {
    if (seasonId == 1 || seasonId == 2) {
        var cells = editui.editor.graph.getChildCells();
        if (cells.length > 0) {
            var cellStyle = [];
            for (var index = 0; index < cells.length; index++) {
                //cellStyle.push(editui.editor.graph.getCellStyle(cells[index]));
                if (cells[index].style != null && cells[index].style.match(/image=?/)) {
                    CausalfactorSelectedCellRemove(editui.editor.graph, mxUtils, editui.editor, cells[index].style);
                }
            }
        }
    }
}

//Calculate the value to move the shape.
function AlignmentValueGet(align, target, source) {
    if (align == 'top' || align == 'bottom') {
        value = (target.geometry.height - source.geometry.height) / 2;
    }
    else {
        value = (target.geometry.width - source.geometry.width) / 2;
    }
    return align == 'top' || align == 'left' ? value : -value;
}

//Move the shape by the calculated value.
function AlignmentDifferenceSet(align, target, value) {

    if (align == 'top' || align == 'bottom') {
        target.geometry.y = target.geometry.y - (value);
    }
    else {
        target.geometry.x = target.geometry.x - (value);
    }
    return target.geometry;
}

//function DisplayOrHideComplete(flag) {
//    //if (flag) {
//    //    Complete.children.cbComplete.style.visibility = "visible";
//    //    Complete.children.labelComplete.style.visibility = "visible";
//    //    $(Complete.children.labelComplete).parent('a').css('background', '');

//    //    if (isComplete == 'True') {
//    //        Complete.children.cbComplete.checked = true;
//    //    }
//    //}
//    //else {
//    //    Complete.children.cbComplete.style.visibility = "hidden";
//    //    Complete.children.labelComplete.style.visibility = "hidden";
//    //    $(Complete.children.labelComplete).parent('a').css('background', 'none');
//    //}

//    Complete.children.cbComplete.style.visibility = "hidden";
//    Complete.children.labelComplete.style.visibility = "hidden";
//    $(Complete.children.labelComplete).parent('a').css('background', 'none');

//}

///////////////////////////////////////////////////////////////////////
