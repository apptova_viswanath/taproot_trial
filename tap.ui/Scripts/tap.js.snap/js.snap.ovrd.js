﻿/* ------------------------------- Override the mxgraph library files. -------------------------------------------------*/
//Global variables.
var _ds = '';

//Override the function, styleChange for auto resizing the shape after changing the font style.
function StyleChangeOverride() {

    editui.menus.styleChange = function (menu, label, keys, values, sprite, parent) {
        return menu.addItem(label, null, mxUtils.bind(this, function () {
            var graph = this.editorUi.editor.graph;

            graph.getModel().beginUpdate();
            try {
                for (var i = 0; i < keys.length; i++) {
                    graph.setCellStyles(keys[i], values[i]);
                }
            }
            finally {
                graph.getModel().endUpdate();
                //MOD_OVRD
                //Auto re-size the shape after changing the font size.
                //L_V_C
                //ShapeAutoResize(graph);
            }
        }), parent, sprite);
    };
}

//Override the function, cellsRemoved to restrict the causal factors from deletion.
function CellsRemovedOverride() {
    editui.editor.graph.cellsRemoved = function (cells) {
        //MOD_OVRD
        var graph = editui.editor.graph;
        var tmp = [];
        var cellsSelected = graph.getDeletableCells(graph.getSelectionCells());
        var causalfactorFound = false;
        var message = 'Warning: Shapes that are Causal Factors cannot be deleted. To delete this shape, you must first remove the causal factor.';

        for (var i = 0; i < cellsSelected.length; i++) {

            //if (cellsSelected[i].style != null && cellsSelected[i].style.match(/shape=label;image=?/))
            //    causalfactorFound = true;
            //else
            //    tmp.push(cellsSelected[i]);

            //TODO : optimize it
            if (cellsSelected[i].getChildCount() == 1) {               
                if (cellsSelected[i].children[0].style.match(/shape=image;image=?/) )
                    causalfactorFound = true;
                else
                    tmp.push(cellsSelected[i]);
            }
            else
                tmp.push(cellsSelected[i]);
        }

        if (causalfactorFound)
            graph.validationAlert(message);

        //Cells, which are free from the causal factor .
        cells = graph.getDeletableCells(graph.addAllEdges(tmp));

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Library code ff//
        if (cells != null && cells.length > 0) {
            var scale = this.view.scale;
            var tr = this.view.translate;

            this.model.beginUpdate();
            try {
                // Creates hashtable for faster lookup
                var hash = new Object();

                for (var i = 0; i < cells.length; i++) {
                    var id = mxCellPath.create(cells[i]);
                    hash[id] = cells[i];
                }

                for (var i = 0; i < cells.length; i++) {
                    // Disconnects edges which are not in cells
                    var edges = this.getConnections(cells[i]);

                    for (var j = 0; j < edges.length; j++) {
                        var id = mxCellPath.create(edges[j]);

                        if (hash[id] == null) {
                            var geo = this.model.getGeometry(edges[j]);

                            if (geo != null) {
                                var state = this.view.getState(edges[j]);

                                if (state != null) {
                                    geo = geo.clone();
                                    var source = state.getVisibleTerminal(true) == cells[i];
                                    var pts = state.absolutePoints;
                                    var n = (source) ? 0 : pts.length - 1;

                                    geo.setTerminalPoint(
                                            new mxPoint(pts[n].x / scale - tr.x,
                                                pts[n].y / scale - tr.y), source);
                                    this.model.setTerminal(edges[j], null, source);
                                    this.model.setGeometry(edges[j], geo);
                                }
                            }
                        }
                    }

                    this.model.remove(cells[i]);
                }

                this.fireEvent(new mxEventObject(mxEvent.CELLS_REMOVED,
                        'cells', cells));
            }
            finally {
                this.model.endUpdate();
            }
        }

    }
}

//Override the function, dblClick for rendering the selected shape on double click on the plain graph.
function DblClickOverride() {

    editui.editor.graph.dblClick = function (evt, cell) {
        var mxe = new mxEventObject(mxEvent.DOUBLE_CLICK, 'event', evt, 'cell', cell);
        this.fireEvent(mxe);

        // Handles the event if it has not been consumed
        if (this.isEnabled() && !mxEvent.isConsumed(evt) && !mxe.isConsumed() &&
            cell != null && this.isCellEditable(cell)) {
            this.startEditingAtCell(cell, evt);
        }//MOD_OVRD
        else {// Render the default/sidebar's selected shape on double click on the graph.            
            _ds == '' ? DefaultShapeRender(this, evt) : DoubleClickGraphToShapeRender(this, evt, _ds);
        }
    };
}

//Override the function, mxConnectionHandler.prototype.connect to
/*
1. Auto align the shapes after connecting.
2. Change the connecting lines between particular shapes.
3. Makes every connecting line to layer behind the shapes.
*/
function ConnectOverride() {

    mxConnectionHandler.prototype.connect = function (source, target, evt, dropTarget) {
        if (target != null || this.isCreateTarget() || this.graph.allowDanglingEdges) {
            // Uses the common parent of source and target or
            // the default parent to insert the edge
            var model = this.graph.getModel();
            var terminalInserted = false;
            var edge = null;

            model.beginUpdate();
            try {
                if (source != null && target == null && this.isCreateTarget()) {
                    target = this.createTargetVertex(evt, source);

                    if (target != null) {
                        dropTarget = this.graph.getDropTarget([target], evt, dropTarget);
                        terminalInserted = true;

                        // Disables edges as drop targets if the target cell was created
                        // FIXME: Should not shift if vertex was aligned (same in Java)
                        if (dropTarget == null || !this.graph.getModel().isEdge(dropTarget)) {
                            var pstate = this.graph.getView().getState(dropTarget);

                            if (pstate != null) {
                                var tmp = model.getGeometry(target);
                                tmp.x -= pstate.origin.x;
                                tmp.y -= pstate.origin.y;
                            }
                        }
                        else {
                            dropTarget = this.graph.getDefaultParent();
                        }

                        this.graph.addCell(target, dropTarget);
                    }
                }

                var parent = this.graph.getDefaultParent();

                if (source != null && target != null &&
                    model.getParent(source) == model.getParent(target) &&
                    model.getParent(model.getParent(source)) != model.getRoot()) {
                    parent = model.getParent(source);

                    if ((source.geometry != null && source.geometry.relative) &&
                        (target.geometry != null && target.geometry.relative)) {
                        parent = model.getParent(parent);
                    }
                }

                // Uses the value of the preview edge state for inserting
                // the new edge into the graph
                var value = null;
                var style = null;

                if (this.edgeState != null) {
                    value = this.edgeState.cell.value;
                    style = this.edgeState.cell.style;
                }

                edge = this.insertEdge(parent, null, value, source, target, style);

                if (edge != null) {
                    // Updates the connection constraints
                    this.graph.setConnectionConstraint(edge, source, true, this.sourceConstraint);
                    this.graph.setConnectionConstraint(edge, target, false, this.constraintHandler.currentConstraint);

                    // Uses geometry of the preview edge state
                    if (this.edgeState != null) {
                        model.setGeometry(edge, this.edgeState.cell.geometry);
                    }

                    // Makes sure the edge has a non-null, relative geometry
                    var geo = model.getGeometry(edge);

                    if (geo == null) {
                        geo = new mxGeometry();
                        geo.relative = true;

                        model.setGeometry(edge, geo);
                    }

                    // Uses scaled waypoints in geometry
                    if (this.waypoints != null && this.waypoints.length > 0) {
                        var s = this.graph.view.scale;
                        var tr = this.graph.view.translate;
                        geo.points = [];

                        for (var i = 0; i < this.waypoints.length; i++) {
                            var pt = this.waypoints[i];
                            geo.points.push(new mxPoint(pt.x / s - tr.x, pt.y / s - tr.y));
                        }
                    }

                    if (target == null) {
                        var t = this.graph.view.translate;
                        var s = this.graph.view.scale;
                        var pt = new mxPoint(this.currentPoint.x / s - t.x, this.currentPoint.y / s - t.y);
                        pt.x -= this.graph.panDx / this.graph.view.scale;
                        pt.y -= this.graph.panDy / this.graph.view.scale;
                        geo.setTerminalPoint(pt, false);
                    }
                    
                    //MOD_OVRD
                    //Change the connecting lines between particular shapes.
                    if (edge.target != null)                        
                        SnapchartConnectionChange(edge);

                    //Makes every connecting line to layer behind the shapes .                                        
                    this.graph.orderCells(true, CellsArrayMake(edge));
                    //////////////////////////////////////////////////////////

                    this.fireEvent(new mxEventObject(mxEvent.CONNECT, 'cell', edge, 'terminal', target,
                        'event', evt, 'target', dropTarget, 'terminalInserted', terminalInserted));
                }
            }
            catch (e) {
                mxLog.show();
                mxLog.debug(e.message);
            }
            finally {
                model.endUpdate();
            }

            if (this.select) {
                this.selectCells(edge, (terminalInserted) ? target : null);
            }

            //MOD_OVRD
            if (target != null) {
                AlignAttributeGet(edge, target, this.graph);
                this.selectCells(edge, target);
            }
            //////////////////////////////////////////
            
        }
    };

    //mxConnectionHandler.prototype.connect = function (source, target, evt, dropTarget) {
    //    if (target != null || this.isCreateTarget() || this.graph.allowDanglingEdges) {
    //        // Uses the common parent of source and target or
    //        // the default parent to insert the edge
    //        var model = this.graph.getModel();
    //        var edge = null;

    //        model.beginUpdate();
    //        try {
    //            if (source != null && target == null && this.isCreateTarget()) {
    //                target = this.createTargetVertex(evt, source);

    //                if (target != null) {
    //                    dropTarget = this.graph.getDropTarget([target], evt, dropTarget);

    //                    // Disables edges as drop targets if the target cell was created
    //                    // FIXME: Should not shift if vertex was aligned (same in Java)
    //                    if (dropTarget == null || !this.graph.getModel().isEdge(dropTarget)) {
    //                        var pstate = this.graph.getView().getState(dropTarget);

    //                        if (pstate != null) {
    //                            var tmp = model.getGeometry(target);
    //                            tmp.x -= pstate.origin.x;
    //                            tmp.y -= pstate.origin.y;
    //                        }
    //                    }
    //                    else {
    //                        dropTarget = this.graph.getDefaultParent();
    //                    }

    //                    this.graph.addCell(target, dropTarget);
    //                }
    //            }

    //            var parent = this.graph.getDefaultParent();

    //            if (source != null && target != null &&
    //                model.getParent(source) == model.getParent(target) &&
    //                model.getParent(model.getParent(source)) != model.getRoot()) {
    //                parent = model.getParent(source);

    //                if ((source.geometry != null && source.geometry.relative) &&
    //                    (target.geometry != null && target.geometry.relative)) {
    //                    parent = model.getParent(parent);
    //                }
    //            }

    //            // Uses the value of the preview edge state for inserting
    //            // the new edge into the graph
    //            var value = null;
    //            var style = null;

    //            if (this.edgeState != null) {
    //                value = this.edgeState.cell.value;
    //                style = this.edgeState.cell.style;
    //            }

    //            edge = this.insertEdge(parent, null, value, source, target, style);

    //            if (edge != null) {
    //                // Updates the connection constraints
    //                this.graph.setConnectionConstraint(edge, source, true, this.sourceConstraint);
    //                this.graph.setConnectionConstraint(edge, target, false, this.constraintHandler.currentConstraint);

    //                // Uses geometry of the preview edge state
    //                if (this.edgeState != null) {
    //                    model.setGeometry(edge, this.edgeState.cell.geometry);
    //                }

    //                // Makes sure the edge has a non-null, relative geometry
    //                var geo = model.getGeometry(edge);

    //                if (geo == null) {
    //                    geo = new mxGeometry();
    //                    geo.relative = true;

    //                    model.setGeometry(edge, geo);
    //                }

    //                // Uses scaled waypoints in geometry
    //                if (this.waypoints != null && this.waypoints.length > 0) {
    //                    var s = this.graph.view.scale;
    //                    var tr = this.graph.view.translate;
    //                    geo.points = [];

    //                    for (var i = 0; i < this.waypoints.length; i++) {
    //                        var pt = this.waypoints[i];
    //                        geo.points.push(new mxPoint(pt.x / s - tr.x, pt.y / s - tr.y));
    //                    }
    //                }

    //                if (target == null) {
    //                    var pt = this.graph.getPointForEvent(evt, false);
    //                    pt.x -= this.graph.panDx / this.graph.view.scale;
    //                    pt.y -= this.graph.panDy / this.graph.view.scale;
    //                    geo.setTerminalPoint(pt, false);
    //                }

    //                //MOD_OVRD
    //                //Change the connecting lines between particular shapes.
    //                //SnapchartConnectionChange(edge);
    //                //////////////////////////////////////////////////////////

    //                this.fireEvent(new mxEventObject(mxEvent.CONNECT,
    //                        'cell', edge, 'event', evt, 'target', dropTarget));
    //            }
    //        }
    //        catch (e) {
    //            mxLog.show();
    //            mxLog.debug(e.message);
    //        }
    //        finally {
    //            model.endUpdate();
    //        }

    //        if (this.select) {
    //            this.selectCells(edge, target);

    //            //MOD_OVRD
    //            //AlignAttributeGet(edge, target, this.graph);
    //            //this.selectCells(edge, target);                
    //            //////////////////////////////////////////
    //        }
    //    }
    //};
}

//Override the function, mxGraph.prototype.cellSizeUpdated
/* 1.Auto resize the shape
   2.Update the causal factor data
   3.Modify the super long word
   4.Condition for editing Date and Time label.
*/
function CellSizeUpdatedOverride() {
    mxGraph.prototype.cellSizeUpdated = function (cell, ignoreChildren) {
        if (cell != null) {
            this.model.beginUpdate();
            try {

                //MOD_OVRD
                //Add spaces to the long word in the text. //skip edges , skip the values which are not just a string .
                if (typeof (cell.value) == 'string' && !cell.isEdge())
                    cell.value = LongWordModify(cell.value);
                ////////////////////////////////////////////////////////////

                var size = this.getPreferredSizeForCell(cell);
                var geo = this.model.getGeometry(cell);

                if (size != null && geo != null) {
                    var collapsed = this.isCellCollapsed(cell);
                    geo = geo.clone();

                    if (this.isSwimlane(cell)) {
                        var state = this.view.getState(cell);
                        var style = (state != null) ? state.style : this.getCellStyle(cell);
                        var cellStyle = this.model.getStyle(cell);

                        if (cellStyle == null) {
                            cellStyle = '';
                        }

                        if (mxUtils.getValue(style, mxConstants.STYLE_HORIZONTAL, true)) {
                            cellStyle = mxUtils.setStyle(cellStyle,
                                    mxConstants.STYLE_STARTSIZE, size.height + 8);

                            if (collapsed) {
                                geo.height = size.height + 8;
                            }

                            geo.width = size.width;
                        }
                        else {
                            cellStyle = mxUtils.setStyle(cellStyle,
                                    mxConstants.STYLE_STARTSIZE, size.width + 8);

                            if (collapsed) {
                                geo.width = size.width + 8;
                            }

                            geo.height = size.height;
                        }

                        this.model.setStyle(cell, cellStyle);
                    }
                    else {
                        geo.width = size.width;
                        geo.height = size.height;
                    }

                    if (!ignoreChildren && !collapsed) {
                        var bounds = this.view.getBounds(this.model.getChildren(cell));

                        if (bounds != null) {
                            var tr = this.view.translate;
                            var scale = this.view.scale;

                            var width = (bounds.x + bounds.width) / scale - geo.x - tr.x;
                            var height = (bounds.y + bounds.height) / scale - geo.y - tr.y;

                            geo.width = Math.max(geo.width, width);
                            geo.height = Math.max(geo.height, height);
                        }
                    }

                    //MOD_OVRD
                    //Change the height and width based on the current shape size.
                    ShapeResize(geo, cell);
                    
                    //Update the causal factor data.
                    CausalFactorValueUpdate(cell);

                    //Restrict the label control from being empty.
                    if (cell.value == '' && cell.style == 'text;align=center;whiteSpace=wrap;html=1') {
                        cell.value = 'Text';
                    }

                    //For safeguards do not change the geometry.
                    geo = cell.style.match(/shape=image;?/) ? cell.geometry : geo;
                    //geo = cell.style.match(/shape=image;?/) || cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/) ? cell.geometry : geo;

                    ///////////////////////////////////////////////////////////////

                    ////MOD_OVRD
                    //// if condition for editing Date and time label.
                    //if (cell.parent.geometry == undefined)
                    //    this.cellsResized([cell], [geo]);
                    ///////////////////////////////////////////////////////////////////
                    this.cellsResized([cell], [geo]);
                    
                    //MOD_OVRD   
                    //To align all the connected shapes after changing size of a shape containing multiple connections.
                    if (!cell.isEdge() && cell.edges != null) {
                        
                        var target = '';                        
                        for (var index = 0; index < cell.edges.length ; index++) {
                            var edge = cell.edges[index];
                            AlignAttributeGet(edge, target, this.cellEditor.graph);
                            this.cellEditor.graph.refresh();
                        }
                    }
                    ///////////////////////////////////////////////
                    
                }
            }
            finally {
                this.model.endUpdate();
            }
        }
    };
}

//Override the function, Sidebar.prototype.createVertexTemplateFromCells to implement double click on the shape in the sidebar.
function CreateVertexTemplateFromCellsOverride() {

    Sidebar.prototype.createVertexTemplateFromCells = function (cells, width, height, title, showLabel) {        
        
        var elt = this.createItem(cells, title, showLabel);        
        var ds = this.createDragSource(elt, this.createDropHandler(cells, true), this.createDragPreview(width, height));
        this.addClickHandler(elt, ds, cells);

        //MOD_OVRD
        //Implement with double click
        DoubleClickHandler(this, elt, ds);
        ///////////////////////////////////////////

        // Uses guides for vertices only if enabled in graph
        ds.isGuidesEnabled = mxUtils.bind(this, function () {
            return this.editorUi.editor.graph.graphHandler.guidesEnabled;
        });

        // Shows a tooltip with the rendered cell
        mxEvent.addGestureListeners(elt, null, mxUtils.bind(this, function (evt) {
            if (mxEvent.isMouseEvent(evt)) {
                this.showTooltip(elt, cells, title, showLabel);
            }
        }));

        return elt;
    };
}

//Override the function, Sidebar.prototype.createItem to implement double click on the shape in the sidebar.
function CreateItemOverride() {
    Sidebar.prototype.createItem = function (cells, title, showLabel, showTitle, width, height) {
        var elt = document.createElement('a');
        elt.setAttribute('href', 'javascript:void(0);');
        elt.className = 'geItem';
        elt.style.overflow = 'hidden';
        var border = (mxClient.IS_QUIRKS) ? 8 + 2 * this.thumbPadding : 2 * this.thumbBorder;
        elt.style.width = (this.thumbWidth + border) + 'px';
        elt.style.height = (this.thumbHeight + border) + 'px';
        elt.style.padding = this.thumbPadding + 'px';

        // Blocks default click action
        mxEvent.addListener(elt, 'click', function (evt) {
            mxEvent.consume(evt);
        });
     
        var bounds = this.createThumb(cells, this.thumbWidth, this.thumbHeight, elt, title, showLabel, showTitle);
        var dx = 0;
        var dy = 0;

        if (cells.length > 1 || cells[0].vertex) {
            dx = (bounds.width - width - 1) / 2;
            dy = (bounds.height - height - 1) / 2;

            var ds = this.createDragSource(elt, this.createDropHandler(cells, true, dx, dy),
                    this.createDragPreview(bounds.width - 1, bounds.height - 1), cells);
            this.addClickHandler(elt, ds, cells);

            // Uses guides for vertices only if enabled in graph
            ds.isGuidesEnabled = mxUtils.bind(this, function () {
                return this.editorUi.editor.graph.graphHandler.guidesEnabled;
            });
        }
        else if (cells[0] != null && cells[0].edge) {
            var ds = this.createDragSource(elt, this.createDropHandler(cells, false, dx, dy),
                this.createDragPreview(width, height), cells);
            this.addClickHandler(elt, ds, cells);
        }

        // Shows a tooltip with the rendered cell
        if (!mxClient.IS_IOS) {
            mxEvent.addGestureListeners(elt, null, mxUtils.bind(this, function (evt) {
                if (mxEvent.isMouseEvent(evt)) {
                    this.showTooltip(elt, cells, bounds.width, bounds.height, title, showLabel, dx, dy);
                }
            }));
        }

        //MOD_OVRD
        //Implement with double click        
        DoubleClickHandler(this, elt, ds);
        ///////////////////////////////////////////

        return elt;
    };
}

//Override the function, addClickHandler
/* 1. Restrict rendering of the shape on graph , by clicking on the shape in the sidebar.
   2. Used global variables for using in the double click functionality. */
function AddClickHandlerOverride() {
    Sidebar.prototype.addClickHandler = function (elt, ds, cells) {

        var graph = this.editorUi.editor.graph;
        var oldMouseUp = ds.mouseUp;
        var first = null;

        mxEvent.addGestureListeners(elt, function (evt) {
            first = new mxPoint(mxEvent.getClientX(evt), mxEvent.getClientY(evt));
        });

        ds.mouseUp = mxUtils.bind(this, function (evt) {
            if (!mxEvent.isPopupTrigger(evt) && this.currentGraph == null && first != null) {
                var tol = graph.tolerance;

                if (Math.abs(first.x - mxEvent.getClientX(evt)) <= tol &&
                    Math.abs(first.y - mxEvent.getClientY(evt)) <= tol) {

                    //MOD_OVRD
                    /*Using global variables to implement, rendering the selected shape on double click on the graph. */
                    _ds = ds;

                    //makes the edited data in the shape persistant.
                    graph.stopEditing(false);

                    //restrict rendering the selected shape in the graph.
                    //this.itemClicked(cells, ds, evt);
                }
            }

            oldMouseUp.apply(ds, arguments);
            first = null;

            // Blocks tooltips on this element after single click
            this.currentElt = elt;
        });
    };

}

//Override the function, createDropHandler for text label control functionality.
function CreateDropHandlerOverride() {

    Sidebar.prototype.createDropHandler = function (cells, allowSplit) {
        return function (graph, evt, target, x, y) {
            cells = graph.getImportableCells(cells);

            //MOD_OVRD
            //Check for the label control and dynamically change the size of the cell.
            if (cells[0].style.match(/text;?/))
                LabelControlSizeChange(graph, cells[0]);
            ////////////////////////////////////////////////////////////

            if (cells.length > 0) {
                var validDropTarget = (target != null) ?
                    graph.isValidDropTarget(target, cells, evt) : false;
                var select = null;

                if (target != null && !validDropTarget) {
                    target = null;
                }

                // Splits the target edge or inserts into target group
                if (allowSplit && graph.isSplitEnabled() && graph.isSplitTarget(target, cells, evt)) {
                    graph.splitEdge(target, cells, null, x, y);
                    select = cells;
                }
                else if (cells.length > 0) {
                    select = graph.importCells(cells, x, y, target);
                }

                if (select != null && select.length > 0) {

                    //MOD_OVRD
                    //Get the selected shape type.                    
                    var shape = CurrentShapeGet(select[0].style);
                    //Set the default style to the current shape.
                    if (shape != '')
                        select[0].style = DefaultShapeApply(shape, graph, select[0].style);

                    graph.refresh();
                    ////////////////////////////////////////////////////////////////////////////

                    graph.scrollCellToVisible(select[0]);
                    graph.setSelectionCells(select);
                }
            }
        };
    };
}

//Override the function, mxDragSource.prototype.mouseUp for making the cell editable soon after dragging the shape from sidebar.
function DragSourceMouseUpOverride() {

    mxDragSource.prototype.mouseUp = function (evt) {
        if (this.currentGraph != null) {
            if (this.currentPoint != null && (this.previewElement == null ||
                this.previewElement.style.visibility != 'hidden')) {
                var scale = this.currentGraph.view.scale;
                var tr = this.currentGraph.view.translate;
                var x = this.currentPoint.x / scale - tr.x;
                var y = this.currentPoint.y / scale - tr.y;

                this.drop(this.currentGraph, evt, this.currentDropTarget, x, y);
            }

            this.dragExit(this.currentGraph);
        }

        this.stopDrag(evt);

        //MOD_OVRD                
        //Make cell editable soon after dragging from sidebar.
        if (this.currentGraph != null)
            this.currentGraph.cellEditor.startEditing(this.currentGraph.getSelectionCells()[0]);
        ////////////////////////////////////////////////

        if (this.eventSource != null) {
            mxEvent.removeGestureListeners(this.eventSource, null, this.mouseMoveHandler, this.mouseUpHandler);
            this.eventSource = null;
        }

        mxEvent.removeGestureListeners(document, null, this.mouseMoveHandler, this.mouseUpHandler);
        this.mouseMoveHandler = null;
        this.mouseUpHandler = null;
        this.currentGraph = null;

        mxEvent.consume(evt);
    };
}


//Override the function, createUndoManager for Undo the CF in db and undo the shapes style.
function CreateUndoManagerOverride() {
    
    Editor.prototype.createUndoManager = function () {
        //MOD_OVRD
        //Variable declaration
        //To use in the function, undoHandler
        var editor = this;

        //To use in the function, listener
        var intialChange = 0;
        /////////////////////////
        
        var graph = this.graph;
        var undoMgr = new mxUndoManager(); 
        
        // Installs the command history
        var listener = function (sender, evt) {
                        
            //MOD_OVRD
            //for the first time on displaying the snapchart graph, do not allow to track the change .
            if (intialChange > 0) {                
                undoMgr.undoableEditHappened(evt.getProperty('edit'));
                //Track the causal factor to UNDO
                if(_cfCreated > 0)
                    _cfCreated++;
            }               

            intialChange++;
            /////////////////////////////////////

            //undoMgr.undoableEditHappened(evt.getProperty('edit'));
        };

        graph.getModel().addListener(mxEvent.UNDO, listener);
        graph.getView().addListener(mxEvent.UNDO, listener);

        // Keeps the selection in sync with the history
        var undoHandler = function (sender, evt) {
            var cand = graph.getSelectionCellsForChanges(evt.getProperty('edit').changes);            
            var cells = [];
            
            //MOD_OVRD
            if (_cfCreated == 2) {                
                CausalFactorStyleUndo('', graph, mxUtils.getXml(editor.getGraphXml()));
            }
            
            for (var i = 0; i < cand.length; i++) {
                if (graph.view.getState(cand[i]) != null) {
                //if (cand[i] != null) {
                    //MOD_OVRD
                    //Undo the causal factor.
                    //Revert causal factors from database.
                    //Note:currently commenting the function will fix later.
                    //debugger;
                    //cfUndo = CausalFactorStyleUndo(cand[i], graph, mxUtils.getXml(editor.getGraphXml()));
                    /////////////////////////////////////////////////

                    cells.push(cand[i]);
                }
            }

            //MOD_OVRD
            //Rendering the wrong shape on Undo so customize the shape's undo part.
            //if(cfUndo =='')
               ShapesGeometryUndoReset(cand, graph);
            
            //////////////////////////////////////
            //if (cfUndo == '')
                graph.setSelectionCells(cells);
           
        };

        undoMgr.addListener(mxEvent.UNDO, undoHandler);
        undoMgr.addListener(mxEvent.REDO, undoHandler);

        return undoMgr;
    };
}

//Override the function, EditorUi.prototype.save  for saving the graph in the database.
function SaveOverride() {

    EditorUi.prototype.save = function (name) {

        if (name != null) {

            var xml = mxUtils.getXml(this.editor.getGraphXml());

            try {
                if (useLocalStorage) {
                    if (localStorage.getItem(name) != null &&
                        !mxUtils.confirm(mxResources.get('replace', [name]))) {
                        return;
                    }

                    localStorage.setItem(name, xml);
                    this.editor.setStatus(mxResources.get('saved'));
                }
                else {
                    if (xml.length < MAX_REQUEST_SIZE) {
                        //MOD_OVRD
                        // xml = encodeURIComponent(xml);
                        //name = encodeURIComponent(name);
                        // new mxXmlRequest(SAVE_URL, 'filename=' + name + '&xml=' + xml).simulate(document, "_blank");	

                        var mode = 'Save';
                        var isComplete = false;
                        var isRevision = false;
                        var isParentUpdate = false;

                        ////Clear the causal factor array.
                        //_causalfactorsIdList = [];
                        //_causalfactorsList = [];

                        SnapchartAjaxSave(xml, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate);

                    }
                    else {
                        mxUtils.alert(mxResources.get('drawingTooLarge'));
                        mxUtils.popup(xml);

                        return;
                    }
                }

                this.editor.filename = name;
                this.editor.modified = false;
            }
            catch (e) {
                this.editor.setStatus('Error saving file');
            }
        }
    };

}

//Override the function, Sidebar.prototype.createThumb to remove the titles for some sidebar shapes.
function CreateThumbOverride() {
    
    Sidebar.prototype.createThumb = function (cells, width, height, parent, title, showLabel) {
        this.graph.labelsVisible = (showLabel == null || showLabel);
        this.graph.view.scaleAndTranslate(1, 0, 0);
        this.graph.addCells(cells);
        var bounds = this.graph.getGraphBounds();
        var corr = this.thumbBorder;
        var s = Math.min((width - 2) / (bounds.width - bounds.x + corr),
            (height - 2) / (bounds.height - bounds.y + corr));
        var x0 = -Math.min(bounds.x, 0);
        var y0 = -Math.min(bounds.y, 0);
        this.graph.view.scaleAndTranslate(s, x0, y0);

        bounds = this.graph.getGraphBounds();
        var dx = Math.max(0, Math.floor((width - bounds.width - bounds.x) / 2));
        var dy = Math.max(0, Math.floor((height - bounds.height - bounds.y) / 2));

        var node = null;

        // For supporting HTML labels in IE9 standards mode the container is cloned instead
        if (this.graph.dialect == mxConstants.DIALECT_SVG && !mxClient.NO_FO) {
            node = this.graph.view.getCanvas().ownerSVGElement.cloneNode(true);
        }
            // LATER: Check if deep clone can be used for quirks if container in DOM
        else {
            node = this.graph.container.cloneNode(false);
            node.innerHTML = this.graph.container.innerHTML;
        }

        this.graph.getModel().clear();

        // Catch-all event handling
        if (mxClient.IS_IE6) {
            parent.style.backgroundImage = 'url(' + this.editorUi.editor.transparentImage + ')';
        }

        var dd = 3;
        node.style.position = 'relative';
        node.style.overflow = 'hidden';
        node.style.cursor = 'pointer';
        node.style.left = (dx + dd) + 'px';
        node.style.top = (dy + dd) + 'px';
        node.style.width = width + 'px';
        node.style.height = height + 'px';
        node.style.visibility = '';
        node.style.minWidth = '';
        node.style.minHeight = '';

        parent.appendChild(node);

        // Adds title for sidebar entries
        if (this.sidebarTitles && title != null) {
            var border = (mxClient.IS_QUIRKS) ? 2 * this.thumbPadding + 2 : 0;
            parent.style.height = (this.thumbHeight + border + this.sidebarTitleSize + 8) + 'px';

            var div = document.createElement('div');
            div.style.fontSize = this.sidebarTitleSize + 'px';
            div.style.textAlign = 'center';
            div.style.whiteSpace = 'nowrap';

            if (mxClient.IS_IE) {
                div.style.height = (this.sidebarTitleSize + 12) + 'px';
            }

            div.style.paddingTop = '4px';

            //MOD_OVRD
            if (title == 'Event' || title == 'Condition' || title == 'Incident')
                mxUtils.write(div, title);
            ///////////////////////////////////////////////////////////////////////////

            //mxUtils.write(div, title);
            parent.appendChild(div);
        }
    };
}

//Override the function, ColorDialog.prototype.createApplyFunction to save the default shape selected color.
function CreateApplyFunctionOverride() {

    ColorDialog.prototype.createApplyFunction = function () {
        return mxUtils.bind(this, function (color) {
            //MOD_OVRD    
            var cell = this.editorUi.editor.graph.getSelectionCell();
            if (cell != null && _defaultShapeAction != _defaultShapeActions.None) {
                //Get the selected shape type.
                var shape = CurrentShapeGet(cell.style);
                //Save the shape's color in the database.
                if (shape != '') {
                    DefaultShapeSave(shape, _defaultShapeAction, color);
                    _isFontSize = '';
                    _isFontFamily = '';
                    _isFontStyle = false;
                }

            }
            if (!_colorPickerDefaultShape) {

                /////////////////////////////////////////////////
                this.editorUi.editor.graph.setCellStyles(this.currentColorKey, (color == 'none') ? 'none' : color);
            }
            else {
                if (_fontColorDefaultShape)
                    _DefaultShapeStyle.FontColor = (color == 'none') ? 'none' : color;
                else if (_fontBGColorDefaultShape)
                    _DefaultShapeStyle.BackgroundColor = (color == 'none') ? 'none' : color;

                FillColorInPreviewShape();
            }
            _colorPickerDefaultShape = false;
            _fontColorDefaultShape = false;
            _fontBGColorDefaultShape = false;
        });
    };
}

//Override the function, Menus.prototype.addSubmenu to track default shape's font size and font color.
function AddSubmenuOverride() {

    Menus.prototype.addSubmenu = function (name, menu, parent, isDefault) {
        var enabled = this.get(name).isEnabled();

        //MOD_OVRD
        if (name == 'fontSize' && isDefault) {
            _isFontSize = _defaultShapeActions.FontSize;
        }

        if (name == 'fontFamily' && isDefault) {
            _isFontFamily = _defaultShapeActions.FontFamily;
        }
        ////////////////////////////////////////////////////////////////////////////

        if (menu.showDisabled || enabled) {
            var submenu = menu.addItem(mxResources.get(name), null, null, parent, null, enabled);
            this.addMenu(name, menu, submenu);
        }
    };
}

//Override the function, mxGraph.prototype.setCellStyles to save the default shape's font size and font color.
function SetCellStylesOverride() {

    mxGraph.prototype.setCellStyles = function (key, value, cells) {

        cells = cells || this.getSelectionCells();
        mxUtils.setCellStyles(this.model, cells, key, value);

        //MOD_OVRD                

        //Save the Font size and Font family in the database.
        if (cells.length > 0 && _isFontStyle == true && (key == 'fontSize' || key == 'fontFamily') && (_isFontSize == 'FontSize' || _isFontFamily == 'FontFamily')) {
            key == 'fontSize' ? _defaultShapeAction = _defaultShapeActions.FontSize : _defaultShapeAction = _defaultShapeActions.FontFamily;
            _isFontSize = '';
            _isFontFamily = '';
            _isFontStyle = false;

            var cell = cells[0];

            //Get the selected shape type.
            var shape = CurrentShapeGet(cell.style);
            //Save the shape's color in the database.
            if (shape != '') {
                DefaultShapeSave(shape, _defaultShapeAction, value);
            }
        }

        ///////////////////////////////////////////////////////////////////////////
    };
}

//Override the function, Menus.prototype.addMenuItem to track the font styles.
function AddMenuItemOverride() {

    Menus.prototype.addMenuItem = function (menu, key, parent) {
        var action = this.editorUi.actions.get(key);

        if (action != null && (menu.showDisabled || action.isEnabled()) && action.visible) {

            //MOD_OVRD
            //just flag value.            
            if (key == 'bold' || key == 'italic' || key == 'underline') {
                _isFontStyle = true;
            }
            //////////////////////////////////////////////////////////////////////////////

            var item = menu.addItem(action.label, null, action.funct, parent, null, action.isEnabled());

            // Adds checkmark image
            if (action.toggleAction && action.isSelected()) {
                this.addCheckmark(item);
            }

            this.addShortcut(item, action);

            return item;
        }

        return null;
    };
}

//Override the function, mxGraph.prototype.toggleCellStyleFlags to save default shape's font styles.
function ToggleCellStyleFlagsOverride() {

    mxGraph.prototype.toggleCellStyleFlags = function (key, flag, cells) {

        this.setCellStyleFlags(key, flag, null, cells);
        //MOD_OVRD
        if (_isFontStyle) {
            _isFontStyle = false;
            _defaultShapeAction = _defaultShapeActions.FontStyle;

            //Save the shape's font style.
            cells = this.getSelectionCells();
            var cell = cells[0];
            var style = cell.style;

            //Get the selected shape type.
            var shape = CurrentShapeGet(style);

            //Save the shape's color in the database.
            if (shape != '') {
                var startIndex = style.indexOf(key + '=');
                var value = style.substring(startIndex + key.length + 1);
                //Parse the string to an integer.
                value = ~~value;
                DefaultShapeSave(shape, _defaultShapeAction, value);
            }
        }
        /////////////////////////////////////////////////////////////////////////////
    };
}

//Override the function, mxGraphHandler.prototype.moveCells to change the connecting lines on move of a shape.
function GraphHandlerMoveCellsOverride() {

    mxGraphHandler.prototype.moveCells = function (cells, dx, dy, clone, target, evt, keyCode) {
        if (clone) {
            cells = this.graph.getCloneableCells(cells);
        }

        // Removes cells from parent
        if (target == null && this.isRemoveCellsFromParent() &&
            this.shouldRemoveCellsFromParent(this.graph.getModel().getParent(this.cell), cells, evt)) {
            target = this.graph.getDefaultParent();
        }

        // Passes all selected cells in order to correctly clone or move into
        // the target cell. The method checks for each cell if its movable.
        cells = this.graph.moveCells(cells, dx - this.graph.panDx / this.graph.view.scale,
                dy - this.graph.panDy / this.graph.view.scale, clone, target, evt);

        if (this.isSelectEnabled() && this.scrollOnMove) {
            this.graph.scrollCellToVisible(cells[0]);
        }

        // Selects the new cells if cells have been cloned
        if (clone) {
            this.graph.setSelectionCells(cells);
        }

        //MOD_OVRD
        var quad = '';
        var aliPos = 'alignment';
        var stCells = {};
        var edgePoint = {};
        var edge = {};       
        
        for (var i = 0; i < cells.length; i++) {

            if (cells[i].isEdge() != 1 && cells[i].edges != null && cells[i].edges.length >= 1) {

                for (var index = 0; index < cells[i].edges.length; index++) {

                    edge = cells[i].edges[index];
                    if (edge.source != null && edge.target != null) {
                        
                        //Fix for cursor keys jumping shapes.
                        if ((keyCode == 39 || keyCode == 37) && edge.source.geometry.y == edge.target.geometry.y) { return false; }
                        if ((keyCode == 38 || keyCode == 40) && edge.source.geometry.x == edge.target.geometry.x) { return false; }

                        edgePoint = EdgePointCreate(edge);
                        quad = edgePoint.ys < edgePoint.yt ? 'down' : 'up';
                        aliPos = AlignPositionGet(edgePoint, quad);
                        
                        if (aliPos == '') {
                            EdgeGeometryModify(this.graph, edge, edgePoint, quad);
                        }

                        if (aliPos != '' && aliPos != 'alignment') {

                            if (edge.geometry.points != null) {
                                edge.geometry.points = null;
                            }
                            
                            AlignShapeOnMove(aliPos, edge, edgePoint, quad, this.graph);                            
                            this.graph.setSelectionCell(edge.target);//NOW                            
                            
                            //When connecting rectangle, rectangle should not move.
                            //if (edge.target.style.match(/rectangle;?/)) {
                            //    aliPos = RectangleAlignmentChange(aliPos);
                            //}
                            
                            //Remove CurrentObjectForAlignmentModify(edge)

                            //stCells = CurrentObjectForAlignmentModify(edge);
                            //this.graph.alignCells(aliPos, stCells, null);

                        }
                    }//edge Check

                }//for loop of edges
            }
        }//for loop of cells
        ////////////////////////////////////////////////////

    };
}

//Overrides clipboard from copying and paste the causalfactor.
function CausalfactorCopyRestrict(editui) {
    // Overrides clipboard to update paste action state
    var paste = editui.actions.get('paste');

    var updatePaste = function () {
        paste.setEnabled(!mxClipboard.isEmpty());
    };

    var mxClipboardCopy = mxClipboard.copy;
    mxClipboard.copy = function () {

        //MOD_OVRD                
        cells = editui.editor.graph.getSelectionCells();
        for (var i = 0; i < cells.length; i++) {
            if (cells[i].getChildCount() == 1) {

                var action = editui.actions.get('paste');
                action.visible = false;
                mxClipboard.setCells(null);                
                return false;
            }
        }

        var action = editui.actions.get('paste');
        action.visible = true;
        //////////////////////////////////////////////////////////////////////////////
        mxClipboardCopy.apply(this, arguments);
        updatePaste();
    };
}

//Override Cell selected function
function CellSelectedOverride() {

    editui.editor.graph.getSelectionModel().addListener(mxEvent.CHANGE, function (sender, evt)
    {
        if (sender.cells == null)
            return;

        if (sender.cells.length == 1)
            return CausalfactorCopyRestrict(editui);
        else if (sender.cells.length > 1) {
            //Set mxClipboard with cells
            SetmxClipboardCellsOverride(sender.cells);

            //Enable paste action
            EnablePasteAction();

            //Allow to paste non CF shape(ctrl+a functionality)
            ShapeMenuPasteOverriade();
        }
    });
}

//Restrict to the CF shape for copy & paste (ctrl + A)
function ShapeMenuPasteOverriade() {

    mxClipboard.paste = function (graph) {
        if (!mxClipboard.isEmpty()) {
            var cells = graph.getImportableCells(mxClipboard.getCells());
            var delta = mxClipboard.insertCount * mxClipboard.STEPSIZE;
            var parent = graph.getDefaultParent();

            graph.model.beginUpdate();
            try {
                for (var i = 0; i < cells.length; i++) {
                    //Check if shape don't have CF, then it will be paste
                    if (cells[i].getChildCount() == 0) {
                        var tmp = (mxClipboard.parents != null && graph.model.contains(mxClipboard.parents[i])) ?
                             mxClipboard.parents[i] : parent;
                        cells[i] = graph.importCells([cells[i]], delta, delta, tmp)[0];
                    }
                }
            }
            finally {
                graph.model.endUpdate();
            }

            // Increments the counter and selects the inserted cells
            mxClipboard.insertCount++;
            graph.setSelectionCells(cells);
        }
    };
}

//Set mxClipboard with cells
function SetmxClipboardCellsOverride(cells) {
    cells = cells || editui.editor.graph.getSelectionCells();
    var result = editui.editor.graph.getExportableCells(cells);
    mxClipboard.insertCount = 1;
    mxClipboard.setCells(cells);

    //Override clipboard copy functionality
    mxClipboard.copy = function () {

        //MOD_OVRD                
        cells = editui.editor.graph.getSelectionCells();
        var result = editui.editor.graph.getExportableCells(cells);

        mxClipboard.parents = new Object();

        for (var i = 0; i < result.length; i++) {
            mxClipboard.parents[i] = editui.editor.graph.model.getParent(cells[i]);
        }

        mxClipboard.insertCount = 1;
        mxClipboard.setCells(editui.editor.graph.cloneCells(result));

        return result;
    }
}

//Enable clipboard paste action
function EnablePasteAction() {
    var paste = editui.actions.get('paste');
    paste.setEnabled(!mxClipboard.isEmpty());

    var action = editui.actions.get('paste');
    action.visible = true;
}

//Shape Label change override
function LabelChangeOverride() {

    mxGraph.prototype.labelChanged = function (cell, value, evt) {
        this.model.beginUpdate();
        try {
            var old = cell.value;

            //MOD_OVRD
            //Controls the user from not clearing the title of the causal factor shape .
            if (cell.getChildCount() > 0 && (cell.children[0].style.match(/shape=image;image=/) || (cell.children[1] != undefined && cell.children[1].style.match(/shape=image;image=/)))) {
                //value = value.replace(/\r/g, '');
                if (value == null || value.length == 0) { value = old;}
            }
            ////////////////////////////////////////////////////////////////////////////////

            this.cellLabelChanged(cell, value, this.isAutoSizeCell(cell));
            this.fireEvent(new mxEventObject(mxEvent.LABEL_CHANGED,
                'cell', cell, 'value', value, 'old', old, 'event', evt));            
        }
        finally {
            this.model.endUpdate();
        }

        return cell;
    };
}

//mxCellEditor stop editing override
function StopEditingOverride() {
    mxCellEditor.prototype.stopEditing = function (cancel) {
        cancel = cancel || false;

        if (this.editingCell != null) {
            if (this.textNode != null) {
                this.textNode.style.visibility = 'visible';
                this.textNode = null;
            }

            if (!cancel && this.isModified()) {
                this.graph.labelChanged(this.editingCell, this.getCurrentValue(), this.trigger);
            }

            if (this.textDiv != null) {
                document.body.removeChild(this.textDiv);
                this.textDiv = null;
            }

            this.editingCell = null;
            this.trigger = null;
            this.bounds = null;
            this.textarea.blur();
            if (this.getCurrentValue() != null && this.getCurrentValue().length > 0)
                this.textarea.parentNode.removeChild(this.textarea);
        }
    };
}

//Override the function, mxUndoManager.prototype.undo for updating the parent's page causal factor title on undo .
function UndomxUndoManagerOverride() {

    mxUndoManager.prototype.undo = function () {
        while (this.indexOfNextAdd > 0) {
            var edit = this.history[--this.indexOfNextAdd];
            edit.undo();

            if (edit.isSignificant()) {
                this.fireEvent(new mxEventObject(mxEvent.UNDO, 'edit', edit));
                //MOD_OVRD
                //Update the parent page causal factor title on undo the CF title changes.		    
                if (edit.changes[0].cell != null && edit.changes[0].cell.children != null) {
                    if (edit.changes[0].cell.children[0].style.match(/shape=image;image=?/)) {
                        CausalFactorValueUpdate(edit.changes[0].cell);
                    }
                } //MOD_END
                break;
            }
        }
    };

}

//Override the function, mxGraph.prototype.resizeCells to align all the connected shapes after resize of a shape containing multiple connections.
function MxGraphResizeCellsOverride() {

    mxGraph.prototype.resizeCells = function (cells, bounds, recurse) {
        recurse = (recurse != null) ? recurse : this.isRecursiveResize();

        this.model.beginUpdate();
        try {
            
            this.cellsResized(cells, bounds, recurse);

            //MOD_OVRD   
            //To align all the connected shapes after resize of a shape containing multiple connections.
            var cell = cells[0];
            if (!cell.isEdge() && cell.edges != null) {
                
                var target = '';
                for (var index = 0; index < cell.edges.length ; index++) {
                    var edge = cell.edges[index];
                    AlignAttributeGet(edge, target, this.cellEditor.graph);
                    this.cellEditor.graph.refresh();
                }
            }
            ///////////////////////////////////////////////

            this.fireEvent(new mxEventObject(mxEvent.RESIZE_CELLS,
                    'cells', cells, 'bounds', bounds));
        }
        finally {
            this.model.endUpdate();
        }

        return cells;
    };
}

//Override the function, mxCellEditor.prototype.startEditing to stop showing the scrollbar while typing in the shape.
function MxCellEditorStartEditingOverride() {

    mxCellEditor.prototype.startEditing = function (cell, trigger) {
        // Lazy instantiates textarea to save memory in IE
        if (this.textarea == null) {
            this.init();
        }

        this.stopEditing(true);
        var state = this.graph.getView().getState(cell);

        if (state != null) {
            this.editingCell = cell;
            this.trigger = trigger;
            this.textNode = null;

            if (state.text != null && this.isHideLabel(state)) {
                this.textNode = state.text.node;
                this.textNode.style.visibility = 'hidden';
            }

            // Configures the style of the in-place editor
            var scale = this.graph.getView().scale;
            var size = mxUtils.getValue(state.style, mxConstants.STYLE_FONTSIZE, mxConstants.DEFAULT_FONTSIZE) * scale;
            var family = mxUtils.getValue(state.style, mxConstants.STYLE_FONTFAMILY, mxConstants.DEFAULT_FONTFAMILY);
            var color = mxUtils.getValue(state.style, mxConstants.STYLE_FONTCOLOR, 'black');
            var align = mxUtils.getValue(state.style, mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT);
            var bold = (mxUtils.getValue(state.style, mxConstants.STYLE_FONTSTYLE, 0) &
                    mxConstants.FONT_BOLD) == mxConstants.FONT_BOLD;
            var italic = (mxUtils.getValue(state.style, mxConstants.STYLE_FONTSTYLE, 0) &
                    mxConstants.FONT_ITALIC) == mxConstants.FONT_ITALIC;
            var uline = (mxUtils.getValue(state.style, mxConstants.STYLE_FONTSTYLE, 0) &
                    mxConstants.FONT_UNDERLINE) == mxConstants.FONT_UNDERLINE;

            this.textarea.style.lineHeight = (mxConstants.ABSOLUTE_LINE_HEIGHT) ? Math.round(size * mxConstants.LINE_HEIGHT) + 'px' : mxConstants.LINE_HEIGHT;
            this.textarea.style.textDecoration = (uline) ? 'underline' : '';
            this.textarea.style.fontWeight = (bold) ? 'bold' : 'normal';
            this.textarea.style.fontStyle = (italic) ? 'italic' : '';
            this.textarea.style.fontSize = Math.round(size) + 'px';
            this.textarea.style.fontFamily = family;
            this.textarea.style.textAlign = align;

            //MOD_OVRD
            //stops showing the scrollbar while typing in the shape .
            //this.textarea.style.overflow = 'auto';
            this.textarea.style.overflow = 'hidden';
            ////////////////////////////////////////////
            this.textarea.style.outline = 'none';
            this.textarea.style.color = color;

            // Specifies the bounds of the editor box
            var bounds = this.getEditorBounds(state);
            this.bounds = bounds;

            this.textarea.style.left = bounds.x + 'px';
            this.textarea.style.top = bounds.y + 'px';
            this.textarea.style.width = bounds.width + 'px';
            this.textarea.style.height = bounds.height + 'px';
            this.textarea.style.zIndex = this.zIndex;

            var value = this.getInitialValue(state, trigger);

            // Uses an optional text value for empty labels which is cleared
            // when the first keystroke appears. This makes it easier to see
            // that a label is being edited even if the label is empty.
            if (value == null || value.length == 0) {
                value = this.getEmptyLabelText();
                this.clearOnChange = value.length > 0;
            }
            else {
                this.clearOnChange = false;
            }

            this.setModified(false);
            this.textarea.value = value;
            this.graph.container.appendChild(this.textarea);

            if (this.textarea.style.display != 'none') {
                if (this.autoSize) {
                    this.textDiv = this.createTextDiv();
                    document.body.appendChild(this.textDiv);
                    this.resize();
                }

                this.textarea.focus();

                if (this.isSelectText() && this.textarea.value.length > 0) {
                    if (mxClient.IS_IOS) {
                        document.execCommand('selectAll');
                    }
                    else {
                        this.textarea.select();
                    }
                }
            }
        }
    };
}

//Override the function, mxEdgeHandler.prototype.connect to change the independent connecting line based on the target shape .
function MxEdgeHandlerConnectOverride() {

    mxEdgeHandler.prototype.connect = function (edge, terminal, isSource, isClone, me) {
        var model = this.graph.getModel();
        var parent = model.getParent(edge);

        model.beginUpdate();
        try {
            // Clones and adds the cell
            if (isClone) {
                var clone = this.graph.cloneCells([edge])[0];
                model.add(parent, clone, model.getChildCount(parent));

                var other = model.getTerminal(edge, !isSource);
                this.graph.connectCell(clone, other, !isSource);

                edge = clone;
            }

            var constraint = this.constraintHandler.currentConstraint;

            if (constraint == null) {
                constraint = new mxConnectionConstraint();
            }

            this.graph.connectCell(edge, terminal, isSource, constraint);
        }
        finally {
            model.endUpdate();
        }
        //MOD_OVRD
        //Change the connecting line based on the target shape.	
        SnapchartConnectionChange(edge);
        this.graph.refresh();
        ///////////////////////////////
        return edge;
    };
}

//Override the function, Menus.prototype.createPopupMenu to remove unnecessary context menu options .
function MenusCreatePopupMenuOverride() {

    Menus.prototype.createPopupMenu = function (menu, cell, evt) {

        //MOD_OVRD
        //Removes unnecessary context menu options

        var graph = this.editorUi.editor.graph;
        menu.smartSeparators = true;

        //Display nothing in the context menu for the causal factor shape
        if (cell != null && cell.getChildCount() > 0 && cell.children[0].style.match(/shape=image;image=/)) return;

        if (graph.isSelectionEmpty()) {
            this.addMenuItems(menu, ['undo', 'redo', '-', 'paste'], null, evt);
        }
        else {

            //Display just DELETE option in the context menu of connecting line            
            if (cell.isEdge()) {
                this.addMenuItems(menu, ['delete'], null, evt);
            }
            else
                this.addMenuItems(menu, ['delete', 'cut', 'copy', 'duplicate'], null, evt);

            //this.addMenuItems(menu, ['delete', '-', 'cut', 'copy', '-', 'duplicate'], null, evt);
        }

        if (!graph.getSelectionCount() > 0) {
            this.addMenuItems(menu, ['-', 'selectVertices', 'selectEdges', '-', 'selectAll'], null, evt);
        }
        ///////////////////////////////////////////////

        //if (graph.getSelectionCount() == 1) {
        //    this.addMenuItems(menu, ['setAsDefaultStyle'], null, evt);
        //}

        //menu.addSeparator();

        //if (graph.getSelectionCount() > 0) {
        //    var cell = graph.getSelectionCell();
        //    var state = graph.view.getState(cell);

        //    if (state != null) {
        //        this.addMenuItems(menu, ['toFront', 'toBack', '-'], null, evt);

        //        if (graph.getModel().isEdge(cell) && mxUtils.getValue(state.style, mxConstants.STYLE_EDGE, null) != 'entityRelationEdgeStyle' &&
        //            mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null) != 'arrow' &&
        //            mxUtils.getValue(state.style, mxConstants.STYLE_SHAPE, null) != 'link') {
        //            var handler = graph.selectionCellsHandler.getHandler(cell);
        //            var isWaypoint = false;

        //            if (handler instanceof mxEdgeHandler && handler.bends != null && handler.bends.length > 2) {
        //                var index = handler.getHandleForEvent(graph.updateMouseEvent(new mxMouseEvent(evt)));

        //                // Configures removeWaypoint action before execution
        //                // Using trigger parameter is cleaner but have to find waypoint here anyway.
        //                var rmWaypointAction = this.editorUi.actions.get('removeWaypoint');
        //                rmWaypointAction.handler = handler;
        //                rmWaypointAction.index = index;

        //                isWaypoint = index > 0 && index < handler.bends.length - 1;
        //            }

        //            this.addMenuItems(menu, ['-', (isWaypoint) ? 'removeWaypoint' : 'addWaypoint'], null, evt);

        //            // Adds reset waypoints option if waypoints exist
        //            var geo = graph.getModel().getGeometry(cell);

        //            if (geo != null && geo.points != null && geo.points.length > 0) {
        //                this.addMenuItems(menu, ['resetWaypoints'], null, evt);
        //            }
        //        }

        //        if (graph.getSelectionCount() > 1) {
        //            menu.addSeparator();
        //            this.addMenuItems(menu, ['group'], null, evt);
        //        }
        //        else (graph.getSelectionCount() == 1 && graph.getModel().getChildCount(graph.getSelectionCell()) > 0)
        //        {
        //            menu.addSeparator();
        //            this.addMenuItems(menu, ['ungroup'], null, evt);
        //        }

        //        if (graph.getSelectionCount() == 1) {
        //            menu.addSeparator();
        //            this.addMenuItems(menu, ['editLink'], null, evt);

        //            // Shows edit image action if there is an image in the style
        //            if (graph.getModel().isVertex(cell) && mxUtils.getValue(state.style, mxConstants.STYLE_IMAGE, null) != null) {
        //                menu.addSeparator();
        //                this.addMenuItem(menu, 'image', null, evt).firstChild.nextSibling.innerHTML = mxResources.get('editImage') + '...';
        //            }
        //        }
        //    }
        //}
        //else {
        //    this.addMenuItems(menu, ['-', 'selectVertices', 'selectEdges', '-', 'selectAll'], null, evt);
        //}
    };
}

//Override the function, mxGraph.prototype.canExportCell to restrict the causal factor shape from copying .
function CanExportCellOverrride() {

    mxGraph.prototype.canExportCell = function (cell) {
        //MOD_OVRD
        //Restrict the causal factors from copy .
        if (cell.getChildCount() > 0 && cell.children[0].style.match(/shape=image;image=/)) {
            //alert('Warning: Shapes that are Causal Factors cannot be copied. To copy this shape, you must first remove the causal factor.');
            return false;
        }
        ////////////////////////////////////////
        return this.exportEnabled;
    };

}

//Override the function, Graph.prototype.setLinkForCell to change the shape of the new link shape .
function SetLinkForCellOverride() {

    Graph.prototype.setLinkForCell = function (cell, link) {
        //MOD_OVRD
        //Makes the new hyperlink as label shape .
        var origLinkcellStyle = cell.style;
        var linkcellStyleToChange = 'text;align=center;whiteSpace=wrap;';
        cell.style = origLinkcellStyle.replace(/rounded=1(;)?/, linkcellStyleToChange);
        ///////////////////////////////////////////////////////////////////////////////
        this.setAttributeForCell(cell, 'link', link);
    };

}

// -----------------------------------------   LVC Start ---------------------------------------------------------------------------------

//Override the function, EditorUi.prototype.createKeyHandler to handle the cursor/direction keys and to maintain proper connection . 
function CreateKeyHandlerOverride() {
    EditorUi.prototype.createKeyHandler = function (editor) {
        var graph = this.editor.graph;
        var keyHandler = new mxKeyHandler(graph);

        // Routes command-key to control-key on Mac
        keyHandler.isControlDown = function (evt) {
            return mxEvent.isControlDown(evt) || (mxClient.IS_MAC && evt.metaKey);
        };

        // Helper function to move cells with the cursor keys
        function nudge(keyCode, stepSize) {
            if (!graph.isSelectionEmpty() && graph.isEnabled()) {
                stepSize = (stepSize != null) ? stepSize : 1;

                var dx = 0;
                var dy = 0;

                if (keyCode == 37) {
                    dx = -stepSize;
                }
                else if (keyCode == 38) {
                    dy = -stepSize;
                }
                else if (keyCode == 39) {
                    dx = stepSize;
                }
                else if (keyCode == 40) {
                    dy = stepSize;
                }
                
                //LVC //MOD_OVRD  
                //Selected shape moved using the keyboard direction keys should align/connected properly.                
                graph.graphHandler.moveCells(graph.getSelectionCells(), dx, dy, null, null, null, keyCode);                
                //graph.moveCells(graph.getSelectionCells(), dx, dy); //commented by me
                graph.scrollCellToVisible(graph.getSelectionCell());
            }
        };

        // Binds keystrokes to actions
        keyHandler.bindAction = mxUtils.bind(this, function (code, control, key, shift) {
            var action = this.actions.get(key);

            if (action != null) {
                var f = function () {
                    if (action.isEnabled()) {
                        action.funct();
                    }
                };

                if (control) {
                    if (shift) {
                        keyHandler.bindControlShiftKey(code, f);
                    }
                    else {
                        keyHandler.bindControlKey(code, f);
                    }
                }
                else {
                    if (shift) {
                        keyHandler.bindShiftKey(code, f);
                    }
                    else {
                        keyHandler.bindKey(code, f);
                    }
                }
            }
        });

        var ui = this;
        var keyHandleEscape = keyHandler.escape;
        keyHandler.escape = function (evt) {
            keyHandleEscape.apply(this, arguments);
        };

        // Ignores enter keystroke. Remove this line if you want the
        // enter keystroke to stop editing.
        keyHandler.enter = function () { };
        keyHandler.bindControlKey(13, function () { graph.foldCells(false); }); // Ctrl+Enter
        keyHandler.bindControlKey(8, function () { graph.foldCells(true); }); // Ctrl+Backspace
        keyHandler.bindKey(33, function () { graph.exitGroup(); }); // Page Up
        keyHandler.bindKey(34, function () { graph.enterGroup(); }); // Page Down
        keyHandler.bindKey(36, function () { graph.home(); }); // Home
        keyHandler.bindKey(35, function () { graph.refresh(); }); // End
        keyHandler.bindKey(37, function () { nudge(37); }); // Left arrow
        keyHandler.bindKey(38, function () { nudge(38); }); // Up arrow
        keyHandler.bindKey(39, function () { nudge(39); }); // Right arrow
        keyHandler.bindKey(40, function () { nudge(40); }); // Down arrow
        keyHandler.bindShiftKey(37, function () { nudge(37, graph.gridSize); }); // Shift+Left arrow
        keyHandler.bindShiftKey(38, function () { nudge(38, graph.gridSize); }); // Shift+Up arrow
        keyHandler.bindShiftKey(39, function () { nudge(39, graph.gridSize); }); // Shift+Right arrow
        keyHandler.bindShiftKey(40, function () { nudge(40, graph.gridSize); }); // Shift+Down arrow
        keyHandler.bindAction(8, false, 'delete'); // Backspace
        keyHandler.bindAction(46, false, 'delete'); // Delete
        keyHandler.bindAction(82, true, 'switchDirection'); // Ctrl+R
        keyHandler.bindAction(83, true, 'save'); // Ctrl+S
        keyHandler.bindAction(83, true, 'saveAs', true); // Ctrl+Shift+S
        keyHandler.bindAction(107, false, 'zoomIn'); // Add
        keyHandler.bindAction(109, false, 'zoomOut'); // Subtract
        keyHandler.bindAction(65, true, 'selectAll'); // Ctrl+A
        keyHandler.bindAction(65, true, 'selectVertices', true); // Ctrl+Shift+A
        keyHandler.bindAction(69, true, 'selectEdges', true); // Ctrl+Shift+E
        keyHandler.bindAction(69, true, 'style'); // Ctrl+E
        keyHandler.bindAction(66, true, 'toBack'); // Ctrl+B
        keyHandler.bindAction(70, true, 'toFront', true); // Ctrl+Shift+F
        keyHandler.bindAction(68, true, 'duplicate'); // Ctrl+D
        keyHandler.bindAction(68, true, 'setAsDefaultStyle', true); // Ctrl+Shift+D   
        keyHandler.bindAction(90, true, 'undo'); // Ctrl+Z
        keyHandler.bindAction(89, true, 'redo'); // Ctrl+Y
        keyHandler.bindAction(88, true, 'cut'); // Ctrl+X
        keyHandler.bindAction(67, true, 'copy'); // Ctrl+C
        keyHandler.bindAction(81, true, 'connectionPoints'); // Ctrl+Q
        keyHandler.bindAction(86, true, 'paste'); // Ctrl+V
        keyHandler.bindAction(71, true, 'group'); // Ctrl+G
        keyHandler.bindAction(77, true, 'editData'); // Ctrl+M
        keyHandler.bindAction(71, true, 'grid', true); // Ctrl+Shift+G
        keyHandler.bindAction(76, true, 'lockUnlock'); // Ctrl+L
        keyHandler.bindAction(76, true, 'layers', true); // Ctrl+Shift+L
        keyHandler.bindAction(79, true, 'outline', true); // Ctrl+Shift+O
        keyHandler.bindAction(80, true, 'print'); // Ctrl+P
        keyHandler.bindAction(85, true, 'ungroup'); // Ctrl+U
        keyHandler.bindAction(112, false, 'about'); // F1
        keyHandler.bindKey(113, function () { graph.startEditingAtCell(); }); // F2

        return keyHandler;
    };
}


//Override the function, mxRubberband.prototype.repaint Due to google language translate 
  //the selection starts from bottom of the arrow instead from the tip of the arrow pointer.
function RepaintOverride() {

    mxRubberband.prototype.repaint = function () {
        if (this.div != null) {
            var x = this.currentX - this.graph.panDx;
            var y = this.currentY - this.graph.panDy;

            this.x = Math.min(this.first.x, x);
            this.y = Math.min(this.first.y, y);
            this.width = Math.max(this.first.x, x) - this.x;
            this.height = Math.max(this.first.y, y) - this.y;

            var dx = (mxClient.IS_VML) ? this.graph.panDx : 0;
            var dy = (mxClient.IS_VML) ? this.graph.panDy : 0;

            //MOD
            //Due to google language translate, the selection starts from bottom of the arrow instead from the tip of the arrow pointer.
            var xVar = isGoogleTranslateOn ? 10 : 0;
            var yVar = isGoogleTranslateOn ? 19 : 0;

            this.div.style.left = (this.x - xVar + dx) + 'px';
            this.div.style.top = (this.y - yVar + dy) + 'px';
            //MOD_end

            //this.div.style.left = (this.x + dx) + 'px'; //commented by me
            //this.div.style.top = (this.y + dy) + 'px'; //commented by me
            this.div.style.width = Math.max(1, this.width) + 'px';
            this.div.style.height = Math.max(1, this.height) + 'px';
        }
    };


}

//Override the function, Toolbar.prototype.addEnabledState to make simple the toolbar icon Selection/Hover view .
function AddEnabledStateOverride() {

    Toolbar.prototype.addEnabledState = function (elt) {
        var classname = elt.className;

        elt.setEnabled = function (value) {
            elt.enabled = value;

            if (value) {
                elt.className = classname;
                //elt.style.display = '';
            }
            else {
                //MOD_OVRD
                elt.className = classname + ' mxDisabled' + ' mxIconDisabled';
                //elt.style.display = 'none';
            }
        };

        elt.setEnabled(true);
    };

}

//Override the function, mxGraphHandler.prototype.mouseMove to stop displaying the pixels while moving the shape.
function GraphHandlerMouseMoveOverride() {

    mxGraphHandler.prototype.mouseMove = function (sender, me) {
        var graph = this.graph;

        if (!me.isConsumed() && graph.isMouseDown && this.cell != null &&
            this.first != null && this.bounds != null) {
            // Stops moving if a multi touch event is received
            if (mxEvent.isMultiTouchEvent(me.getEvent())) {
                this.reset();
                return;
            }

            var delta = this.getDelta(me);
            var dx = delta.x;
            var dy = delta.y;
            var tol = graph.tolerance;

            if (this.shape != null || Math.abs(dx) > tol || Math.abs(dy) > tol) {
                // Highlight is used for highlighting drop targets
                if (this.highlight == null) {
                    this.highlight = new mxCellHighlight(this.graph,
                        mxConstants.DROP_TARGET_COLOR, 3);
                }

                if (this.shape == null) {
                    this.shape = this.createPreviewShape(this.bounds);
                }

                var gridEnabled = graph.isGridEnabledEvent(me.getEvent());
                var hideGuide = true;

                if (this.guide != null && this.useGuidesForEvent(me)) {
                    delta = this.guide.move(this.bounds, new mxPoint(dx, dy), gridEnabled);
                    hideGuide = false;
                    dx = delta.x;
                    dy = delta.y;
                }
                else if (gridEnabled) {
                    var trx = graph.getView().translate;
                    var scale = graph.getView().scale;

                    var tx = this.bounds.x - (graph.snap(this.bounds.x / scale - trx.x) + trx.x) * scale;
                    var ty = this.bounds.y - (graph.snap(this.bounds.y / scale - trx.y) + trx.y) * scale;
                    var v = this.snap(new mxPoint(dx, dy));

                    dx = v.x - tx;
                    dy = v.y - ty;
                }

                if (this.guide != null && hideGuide) {
                    this.guide.hide();
                }

                // Constrained movement if shift key is pressed
                if (graph.isConstrainedEvent(me.getEvent())) {
                    if (Math.abs(dx) > Math.abs(dy)) {
                        dy = 0;
                    }
                    else {
                        dx = 0;
                    }
                }

                this.currentDx = dx;
                this.currentDy = dy;
                this.updatePreviewShape();

                var target = null;
                var cell = me.getCell();

                var clone = graph.isCloneEvent(me.getEvent()) && graph.isCellsCloneable() && this.isCloneEnabled();

                if (graph.isDropEnabled() && this.highlightEnabled) {
                    // Contains a call to getCellAt to find the cell under the mouse
                    target = graph.getDropTarget(this.cells, me.getEvent(), cell, clone);
                }

                var state = graph.getView().getState(target);
                var highlight = false;

                if (state != null && (graph.model.getParent(this.cell) != target || clone)) {
                    if (this.target != target) {
                        this.target = target;
                        this.setHighlightColor(mxConstants.DROP_TARGET_COLOR);
                    }

                    highlight = true;
                }
                else {
                    this.target = null;

                    if (this.connectOnDrop && cell != null && this.cells.length == 1 &&
                        graph.getModel().isVertex(cell) && graph.isCellConnectable(cell)) {
                        state = graph.getView().getState(cell);

                        if (state != null) {
                            var error = graph.getEdgeValidationError(null, this.cell, cell);
                            var color = (error == null) ?
                                mxConstants.VALID_COLOR :
                                mxConstants.INVALID_CONNECT_TARGET_COLOR;
                            this.setHighlightColor(color);
                            highlight = true;
                        }
                    }
                }

                if (state != null && highlight) {
                    this.highlight.highlight(state);
                }
                else {
                    this.highlight.hide();
                }
            }

            //LVC  //MOD_OVRD
            //Commented to disable the pixel display while moving the selected shape .            
            //this.updateHint(me);
            me.consume();

            // Cancels the bubbling of events to the container so
            // that the droptarget is not reset due to an mouseMove
            // fired on the container with no associated state.
            mxEvent.consume(me.getEvent());
        }
        else if ((this.isMoveEnabled() || this.isCloneEnabled()) && this.updateCursor &&
            !me.isConsumed() && me.getState() != null && !graph.isMouseDown) {
            var cursor = graph.getCursorForMouseEvent(me);

            if (cursor == null && graph.isEnabled() && graph.isCellMovable(me.getCell())) {
                if (graph.getModel().isEdge(me.getCell())) {
                    cursor = mxConstants.CURSOR_MOVABLE_EDGE;
                }
                else {
                    cursor = mxConstants.CURSOR_MOVABLE_VERTEX;
                }
            }

            me.getState().setCursor(cursor);
        }
    };
}

//Override the function, mxVertexHandler.prototype.mouseMove to stop displaying the pixels while resizing the shape.
function VertexHandlerMouseMoveOverride() {

    mxVertexHandler.prototype.mouseMove = function (sender, me) {
        if (!me.isConsumed() && this.index != null) {
            // Checks tolerance for ignoring single clicks
            this.checkTolerance(me);

            if (!this.inTolerance) {
                var point = new mxPoint(me.getGraphX(), me.getGraphY());
                var gridEnabled = this.graph.isGridEnabledEvent(me.getEvent());
                var scale = this.graph.view.scale;
                var tr = this.graph.view.translate;

                if (this.index <= mxEvent.CUSTOM_HANDLE) {
                    if (this.customHandles != null) {
                        this.customHandles[mxEvent.CUSTOM_HANDLE - this.index].processEvent(me);
                    }
                }
                else if (this.index == mxEvent.LABEL_HANDLE) {
                    if (gridEnabled) {
                        point.x = (this.graph.snap(point.x / scale - tr.x) + tr.x) * scale;
                        point.y = (this.graph.snap(point.y / scale - tr.y) + tr.y) * scale;
                    }

                    this.moveSizerTo(this.sizers[this.sizers.length - 1], point.x, point.y);
                }
                else if (this.index == mxEvent.ROTATION_HANDLE) {
                    var dx = this.state.x + this.state.width / 2 - point.x;
                    var dy = this.state.y + this.state.height / 2 - point.y;

                    this.currentAlpha = (dx != 0) ? Math.atan(dy / dx) * 180 / Math.PI + 90 : ((dy < 0) ? 180 : 0);

                    if (dx > 0) {
                        this.currentAlpha -= 180;
                    }

                    // Rotation raster
                    if (this.rotationRaster && this.graph.isGridEnabledEvent(me.getEvent())) {
                        var dx = point.x - this.state.getCenterX();
                        var dy = point.y - this.state.getCenterY();
                        var dist = Math.abs(Math.sqrt(dx * dx + dy * dy) - this.state.height / 2 - 20);
                        var raster = Math.max(1, 5 * Math.min(3, Math.max(0, Math.round(80 / Math.abs(dist)))));

                        this.currentAlpha = Math.round(this.currentAlpha / raster) * raster;
                    }
                    else {
                        this.currentAlpha = this.roundAngle(this.currentAlpha);
                    }

                    this.selectionBorder.rotation = this.currentAlpha;
                    this.selectionBorder.redraw();

                    if (this.livePreview) {
                        this.redrawHandles();
                    }
                }
                else {
                    var alpha = mxUtils.toRadians(this.state.style[mxConstants.STYLE_ROTATION] || '0');
                    var cos = Math.cos(-alpha);
                    var sin = Math.sin(-alpha);

                    var ct = new mxPoint(this.state.getCenterX(), this.state.getCenterY());

                    var dx = point.x - this.startX;
                    var dy = point.y - this.startY;

                    // Rotates vector for mouse gesture
                    var tx = cos * dx - sin * dy;
                    var ty = sin * dx + cos * dy;

                    dx = tx;
                    dy = ty;

                    var geo = this.graph.getCellGeometry(this.state.cell);
                    this.unscaledBounds = this.union(geo, dx / scale, dy / scale,
                        this.index, gridEnabled, 1, new mxPoint(0, 0), this.isConstrainedEvent(me));
                    this.bounds = new mxRectangle(((this.parentState != null) ? this.parentState.x : tr.x * scale) +
                            (this.unscaledBounds.x) * scale, ((this.parentState != null) ? this.parentState.y : tr.y * scale) +
                            (this.unscaledBounds.y) * scale, this.unscaledBounds.width * scale, this.unscaledBounds.height * scale);

                    if (geo.relative && this.parentState != null) {
                        this.bounds.x += this.state.x - this.parentState.x;
                        this.bounds.y += this.state.y - this.parentState.y;
                    }

                    cos = Math.cos(alpha);
                    sin = Math.sin(alpha);

                    var c2 = new mxPoint(this.bounds.getCenterX(), this.bounds.getCenterY());

                    var dx = c2.x - ct.x;
                    var dy = c2.y - ct.y;

                    var dx2 = cos * dx - sin * dy;
                    var dy2 = sin * dx + cos * dy;

                    var dx3 = dx2 - dx;
                    var dy3 = dy2 - dy;

                    var dx4 = this.bounds.x - this.state.x;
                    var dy4 = this.bounds.y - this.state.y;

                    var dx5 = cos * dx4 - sin * dy4;
                    var dy5 = sin * dx4 + cos * dy4;

                    this.bounds.x += dx3;
                    this.bounds.y += dy3;

                    // Rounds unscaled bounds to int
                    this.unscaledBounds.x = this.roundLength(this.unscaledBounds.x + dx3 / scale);
                    this.unscaledBounds.y = this.roundLength(this.unscaledBounds.y + dy3 / scale);
                    this.unscaledBounds.width = this.roundLength(this.unscaledBounds.width);
                    this.unscaledBounds.height = this.roundLength(this.unscaledBounds.height);

                    // Shifts the children according to parent offset
                    if (!this.graph.isCellCollapsed(this.state.cell) && (dx3 != 0 || dy3 != 0)) {
                        this.childOffsetX = this.state.x - this.bounds.x + dx5;
                        this.childOffsetY = this.state.y - this.bounds.y + dy5;
                    }
                    else {
                        this.childOffsetX = 0;
                        this.childOffsetY = 0;
                    }

                    // TODO: Apply child offset to children in live preview
                    if (this.livePreview) {
                        // Saves current state
                        var tmp = new mxRectangle(this.state.x, this.state.y, this.state.width, this.state.height);
                        var orig = this.state.origin;

                        // Temporarily changes size and origin
                        this.state.x = this.bounds.x;
                        this.state.y = this.bounds.y;
                        this.state.origin = new mxPoint(this.state.x / scale - tr.x, this.state.y / scale - tr.y);
                        this.state.width = this.bounds.width;
                        this.state.height = this.bounds.height;

                        // Redraws cell and handles
                        var off = this.state.absoluteOffset;
                        off = new mxPoint(off.x, off.y);

                        // Required to store and reset absolute offset for updating label position
                        this.state.absoluteOffset.x = 0;
                        this.state.absoluteOffset.y = 0;
                        var geo = this.graph.getCellGeometry(this.state.cell);

                        if (geo != null) {
                            var offset = geo.offset || this.EMPTY_POINT;

                            if (offset != null && !geo.relative) {
                                this.state.absoluteOffset.x = this.state.view.scale * offset.x;
                                this.state.absoluteOffset.y = this.state.view.scale * offset.y;
                            }

                            this.state.view.updateVertexLabelOffset(this.state);
                        }

                        // Draws the live preview
                        this.state.view.graph.cellRenderer.redraw(this.state, true);

                        // Redraws connected edges
                        this.state.view.invalidate(this.state.cell);
                        this.state.invalid = false;
                        this.state.view.validate();
                        this.redrawHandles();

                        // Restores current state
                        this.state.x = tmp.x;
                        this.state.y = tmp.y;
                        this.state.width = tmp.width;
                        this.state.height = tmp.height;
                        this.state.origin = orig;
                        this.state.absoluteOffset = off;
                    }

                    if (this.preview != null) {
                        this.drawPreview();
                    }
                }
                //LVC  //MOD_OVRD
                //Commented to disable the pixel display while resize .
                //this.updateHint(me);
            }

            me.consume();
        }
            // Workaround for disabling the connect highlight when over handle
        else if (!this.graph.isMouseDown && this.getHandleForEvent(me) != null) {
            me.consume(false);
        }
    };
}

//Override the function, mxEdgeHandler.prototype.mouseMove to stop displaying the pixels while changing the selected edge from one target to another target .
function EdgeHandlerMouseMoveOverride() {

    mxEdgeHandler.prototype.mouseMove = function (sender, me) {
        if (this.index != null && this.marker != null) {
            var point = this.getPointForEvent(me);
            this.error = null;

            if (mxEvent.isShiftDown(me.getEvent()) && this.snapPoint != null) {
                if (Math.abs(this.snapPoint.x - point.x) < Math.abs(this.snapPoint.y - point.y)) {
                    point.x = this.snapPoint.x;
                }
                else {
                    point.y = this.snapPoint.y;
                }
            }

            if (this.isLabel) {
                this.label.x = point.x;
                this.label.y = point.y;
            }
            else {
                this.points = this.getPreviewPoints(point);
                var terminalState = (this.isSource || this.isTarget) ? this.getPreviewTerminalState(me) : null;
                var clone = this.clonePreviewState(point, (terminalState != null) ? terminalState.cell : null);
                this.updatePreviewState(clone, point, terminalState, me);

                // Sets the color of the preview to valid or invalid, updates the
                // points of the preview and redraws
                var color = (this.error == null) ? this.marker.validColor : this.marker.invalidColor;
                this.setPreviewColor(color);
                this.abspoints = clone.absolutePoints;
                this.active = true;
            }

            this.drawPreview();
            //LVC  //MOD_OVRD
            //Commented to disable the pixel display while changing the connecting line target (edge target).
            //this.updateHint(me, point);
            mxEvent.consume(me.getEvent());
            me.consume();
        }
            // Workaround for disabling the connect highlight when over handle
        else if (mxClient.IS_IE && this.getHandleForEvent(me) != null) {
            me.consume(false);
        }
    };
}

// -----------------------------------------   LVC End -----------------------------------------------------------------------------------
