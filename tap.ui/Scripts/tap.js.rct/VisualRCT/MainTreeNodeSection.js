﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
//Display the 4 main tree node in oval shape


function GetMainTreeNodes(rcID) {

    $('#DivloadingImage').show();

    var rctData = {};
    rctData.causalFactorID = GetCausalFactorID();
    rctData.userId = $('#hdnUserId').val();
    rctData = JSON.stringify(rctData);

    var serviceURL = _baseURL + 'Services/RootCause.svc/GetTreeNodes';
    AjaxPost(serviceURL, rctData, eval(GetTreeNodesSuccesss), eval(FailedCallback));

}


//After successfully getting  the 4 main tree nodes from the service call, Draw those data in the page as oval shape
function GetTreeNodesSuccesss(data) {

    var data = ParseToJSON(data.d);
    DrawTreeNodes(data);
           
    var hgt = $('#divCausalFactor').height();
    document.getElementById('rctWindow').style["margin-top"] = hgt + "px";
    $('#rctWindow').css("margin-top", hgt + 20 + "px !important");
}


//Draw the circles for the first main 4 tree nodes, if any of those is selected by the user previously
//then populate the related sections
function DrawTreeNodes(jsonData) {

    var container = document.createElement("div");
  //  $(container).css('margin-top', '20px');
    $(container).css('float', 'top');
    container.setAttribute("style", " float:left;overflow: visible;white-space: nowrap;");
    $('#divTreeNode')[0].appendChild(container);
        
    for (var item = 0; item < jsonData.length; item++) {
      
        var container1 = document.createElement("div");
        //$(container1).css('float', 'left');
        $(container1).css('position', 'relative');
        $(container1).css('display', 'inline-block');

        container.appendChild(container1);

        var rcData = jsonData[item];

        _userLanguage = rcData.UserLanguageName;
        var text = rcData.RCTTitle;
        var isSelected = rcData.IsSelected;
        var analysisComments = rcData.AnalysisComments;
        _guidanceCount = rcData.GuidanceCount;

        StoreInHiddenField(container1, "hdnNodeType" + rcData.RCID, rcData.RCType);

        var canvas = document.createElement("canvas");
        canvas.setAttribute('id', 'canvas' + rcData.RCID);
        $(canvas).css('cursor', 'pointer');

        var context = canvas.getContext("2d");
        context.fillStyle = CONTEXT_DEFAULT_FILL_COLOR;

        var radius = 50;
        DrawShadow(context);
        DrawCircle(context, canvas, radius);

        ClearShadow(context);
        //DrawText(context, radius, text, CIRCLE_SHAPE, FONT_12PX, rcData.RCID);

        context.strokeStyle = CONTEXT_DEFAULT_STROKE_COLOR;
        //context.lineWidth = 3;
        context.stroke();

        //Store the RCT id and RCT title for future reference in the same div to be used in onclick event of this canvas
        AppendElementsAndStoreID(container1, canvas, rcData.RCID);

        text = (text == "HUMAN PERFORMANCE DIFFICULTY") ? "HUMAN<br/>PERFORMANCE<br/>DIFFICULTY" : (text == "EQUIPMENT DIFFICULTY") ? "EQUIPMENT<br/>DIFFICULTY" :
            (text == "NATURAL DISASTER / SABOTAGE") ? "NATURAL<br/>DISASTER /<br/>SABOTAGE" :
            (text == "NATURKATASTROPHE/SABOTAGE") ? "NATURKATASTROPHE/<br/>SABOTAGE" : text;

        AddTextAndClickableArea(container1, context, radius, text, CIRCLE_SHAPE, rcData.RCID);

        //Assign RCT id related to each shape in a hidden field next to each shape
        StoreInHiddenField(container1, 'hdn' + rcData.RCID, rcData.EnglishTitle);
        //RegisterClickEventForMainTreeNode(canvas);

        DrawTools(container1, rcData.RCID, TREE_NODE_TYPE, text);

        //$(canvas).live("click mouseover", function (event) {
            
        //    var divToolId = $(this).next().next().next().attr('id');
        //    HideOptionToolBarAndShowCurrent($('#' + divToolId));
        //});

        var divBlock = '#divBlock' + rcData.RCID;
        RegisterClickEventForNearBasicActualRCTNode(divBlock, canvas, rcData.RCID, true);


        ////If user has sleceted the tree node previously then populate the related section to this
        ////if (isSelected){
        ////$(canvas).trigger("click");
        //UpdateStatusAndSectionForMainTreeNode(rcData.RCID, isSelected);
        ////}

        DrawMainRCTSection(rcData.RCID, isSelected);

        SetAnalysisComments(container, rcData.RCID, analysisComments);

    }

}


function CreateLinkLine(rcId, isCreate)
{

    var x = 260;
    var nextSection = '#content1';
    var marginLeft = '150px';
    var yEndPoint = 70;

    if (rcId == RCID_HUMAN_PERFORMANCE_DIFFICULTY) {
        x = 50;
        nextSection = '#content';
        marginLeft = '-520px';
    }


    if (isCreate) {

        var canvasLink = document.createElement("canvas");
        canvasLink.setAttribute('id', 'canvasLink' + rcId);
        $(canvasLink).css('cursor', 'pointer');
        $(canvasLink).css('margin-left', marginLeft);
        $(canvasLink).css('margin-top', '-60px');

        var lineContext = canvasLink.getContext("2d");
        lineContext.beginPath();
        lineContext.moveTo(x, -300);
        lineContext.lineTo(x, yEndPoint);
        lineContext.stroke();
        lineContext.closePath();

        $(canvasLink).insertBefore($(nextSection));
        $(canvasLink).show();

    }
    else {
        var canvasLink = 'canvasLink' + rcId;
        if ($('#' + canvasLink).length != 0) {

            $('#' + canvasLink).hide();
            $('#' + canvasLink).remove();
        }
    }
}



function UpdateStatusAndSectionForMainTreeNode(rcId, status, radioButton)
{

    var effectSet = true;
        
    var rcTitle = $('#canvas' + rcId).next().next().next().val();


    var isHPDNode = (rcId == RCID_HUMAN_PERFORMANCE_DIFFICULTY);
    var isEquipmentDifficultyNode = (rcId == RCID_EQUIPMENT_DIFFICULTY);
    var containerId = (isHPDNode) ? '#questions' : (isEquipmentDifficultyNode) ? '#div' + RCID_EQUIPMENT_DIFFICULTY : '';

    if (rcId == RCID_HUMAN_PERFORMANCE_DIFFICULTY && (!status)) {

        if (($('#divRCTQuestions').find('.greenCheck').length > 0) || ($('#divRCTQuestions').find('.redCross').length > 0)) {
            
            alert(FIFTEEN_QUESTION_MESSAGE);
            PreventDefault(radioButton);
            return false;
        }
    }


    if (rcId == RCID_EQUIPMENT_DIFFICULTY  && (!status)) {
        effectSet = IsAnyChildNodeSelected(rcId, true);       
    }

    if (effectSet) {
                
        var canvasCtx = $('#canvas' + rcId)[0].getContext('2d');
        var strokeStyle = (status) ? GREEN_COLOR : CONTEXT_DEFAULT_STROKE_COLOR;

        ClearCanvasColor(canvasCtx);
        canvasCtx.lineWidth = (strokeStyle == RED_COLOR) ? 1.5 : (strokeStyle == GREEN_COLOR) ? 3 : 1.5;

        RePaintCanvas(canvasCtx, strokeStyle);

        if (status == false) {// if red cross

            CreateRedCrossCanvas(rcId);
        }
        else {// if green check or erase

            EraseRedCrossCanvas(rcId);
            RePaintCanvas(canvasCtx, strokeStyle);
            
            if (rcId == RCID_EQUIPMENT_DIFFICULTY && (status))
            {
                if (!($(containerId).is(':visible'))) {
                    CreateLinkLine(rcId, true);
                }
            }
        }

        if (!status)
        {

            if (rcId == RCID_EQUIPMENT_DIFFICULTY) {// if red cross or erase, with equipment difficulty

                jQuery("[name*=" + RCID_EQUIPMENT_DIFFICULTY + "-]").parent().hide();
                var canvasUnderParentBasicNode = '#div' + RCID_EQUIPMENT_DIFFICULTY + ' > div';
                $(canvasUnderParentBasicNode).remove();

                CreateLinkLine(rcId, false);
            }
            else if (rcId == RCID_HUMAN_PERFORMANCE_DIFFICULTY) {// if red cross or erase, with RCID_HUMAN_PERFORMANCE_DIFFICULTY
              //  var containerId = '#questions';
                $(containerId).hide();

            }
        }

        SaveResponseAndGetQuestions(rcId, rcTitle, status, radioButton);

    }
   
}


function DrawMainRCTSection(rcId, status)
{

    var isHPDNode = (rcId == RCID_HUMAN_PERFORMANCE_DIFFICULTY);
    var isEquipmentDifficultyNode = (rcId == RCID_EQUIPMENT_DIFFICULTY);
    
    var rcTitle = $('#canvas' + rcId).next().next().next().val();
    

    var canvasCtx = $('#canvas' + rcId)[0].getContext('2d');
    var strokeStyle = (status) ? GREEN_COLOR : CONTEXT_DEFAULT_STROKE_COLOR;
    canvasCtx.lineWidth = (strokeStyle == RED_COLOR) ? 1.5 : (strokeStyle == GREEN_COLOR) ? 3 : 1.5;

    RePaintCanvas(canvasCtx, strokeStyle);

   
    if (status) {// if green check or erase

        RePaintCanvas(canvasCtx, strokeStyle);

        if (isHPDNode || isEquipmentDifficultyNode) {            
            SaveResponseAndGetQuestions(rcId, rcTitle, status, null);
        }

    }
    else if (status == false) {// if red cross
        CreateRedCrossCanvas(rcId);
    }

    if (isEquipmentDifficultyNode) {
        var isCreateLink = (status) ? true : false;
        CreateLinkLine(rcId, isCreateLink);
    }

    //if (isHPDNode) {
    //    var isCreateLink = (status) ? true : false;
    //    CreateLinkLine(rcId, isCreateLink);
    //}

}

function SaveResponseAndGetQuestions(rcID, rcTitle, isSelected, senderRadioButton) {

    var rctData = {};

    var languageId = _userId;
    var fetchQuestion = (rcID == RCID_HUMAN_PERFORMANCE_DIFFICULTY) || (rcID == RCID_EQUIPMENT_DIFFICULTY);
    var isHPDNodeClicked = (rcID == RCID_HUMAN_PERFORMANCE_DIFFICULTY);

    rctData.userId = $('#hdnUserId').val();
    rctData.title = rcTitle;
    rctData.checkedValue = isSelected; //(isSelected == null)? false : isSelected;
    rctData.rootCauseID = rcID;
    rctData.causalFactorID = GetCausalFactorID();


    var questionResponse = '';
    if (senderRadioButton != null) {

        var radioButtonGroup = $(senderRadioButton).parent();
        var questionId = $(senderRadioButton).prop('name');

        var response = ($(radioButtonGroup).find('#rbYes').prop('checked')) ? true
                            : ($(radioButtonGroup).find('#rbNo').prop('checked')) ? false : null;
        questionResponse = questionResponse + questionId + ':' + response;

    }
    //Set the parameter values
    rctData.questionResponse = questionResponse;


    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/SaveResponseAndGetQuestions';
    AjaxPost(serviceURL, rctData, eval(SaveResponseAndGetQuestionsSuccess), eval(FailedCallback));

}

function SaveResponseAndGetQuestionsSuccess(data) {

    if (data != null) {

        var parsedata = ParseToJSON(data.d);
        var isHPDNode = parsedata[0];
        var isEquipmentDifficultyNode = parsedata[1];
        var nodeChecked = parsedata[3];
        var detailData = parsedata[2];

        if( isHPDNode || isEquipmentDifficultyNode){
            RenderDetailsSectionsOfMainTreeNodes(isHPDNode, isEquipmentDifficultyNode, nodeChecked, detailData);
        }

    }

}


function RenderDetailsSectionsOfMainTreeNodes(isHPDNode, isEquipmentDifficultyNode, status, detailData)
{
    
    var containerId = (isHPDNode) ? '#questions' : (isEquipmentDifficultyNode) ? '#div' + RCID_EQUIPMENT_DIFFICULTY : '';

    if (status) {// if has data

        if (!($(containerId).is(':visible'))) {

            $(containerId).show();

            if (isHPDNode) {
                eval(PopulateRCTQuestions(detailData));
                return;
            }
            else (isEquipmentDifficultyNode)
            {

                var id = $("[name=" + RCID_EQUIPMENT_DIFFICULTY + "-]").attr('id');
                var sectionHeader = $("[name=" + RCID_EQUIPMENT_DIFFICULTY + "-]").parent();
                var causalFactorID = GetCausalFactorID();

                //RenderRCTSections('rc' + RCID_EQUIPMENT_DIFFICULTY, $(containerId), GetCausalFactorID());

                _rcID = RCID_EQUIPMENT_DIFFICULTY;
                _parent = $(containerId)[0];

                if (_rcID != RCID_EQUIPMENT_DIFFICULTY) {
                    $('#rc' + _rcID).addClass('nrct');
                }

                GetVisualRCTSuccesss(detailData[0]);
           
            }
        }
    }
    else {
       
        $(containerId).hide();
    }

}







