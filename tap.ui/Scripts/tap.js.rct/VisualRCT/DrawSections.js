﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
/*===============================================================================================================================================================================
Draw RCT Section*/


var _canvasWidth = 300;
var _canvasHeight = 150;


//Based on root cause category type, draw canvas shapes and draw texts within the shape
function DrawRCTSections(jsonData) {
  
    var divId = 'div' + _rcID;
    var wrapper = '';
    var container = '';
    
    wrapper = _parent;

    $('#rc' + _rcID).css('z-index', 2);
    $('#rc' + _rcID).css('position', 'relative');
    //Create a div with all tools icon and register their click events and make it invisible
    DrawTools(wrapper, _rcID, BASIC_ROOT_NODE_TYPE, '');


    for (var item = 0; item < jsonData.length; item++) {

        var rcData = jsonData[item];

        //Create canvas with id set from the RCID , to uniquely distinguish canvas during user interaction
        var canvas = document.createElement(CANVAS);
        canvas.setAttribute('id', CANVAS + rcData.RCID);
        
        SetWidthAndHeightOfCanvas(canvas, rcData.RCID);
        _canvasWidth = canvas.width;
        _canvasHeight = canvas.height;

        var context = canvas.getContext("2d");
        context.fillStyle = CONTEXT_DEFAULT_FILL_COLOR;

        var text = rcData.RCTTitle;
        var rcType = rcData.RCType;
        var rcStatus = rcData.RCTValue;
        var analysisComments = rcData.AnalysisComments;
        _guidanceCount = rcData.GuidanceCount;

        container = document.createElement("div");
        container.setAttribute('id', 'divWrapper' + rcData.RCID);
        wrapper.appendChild(container);

        StoreInHiddenField(container, "hdnNodeType" + rcData.RCID, rcType);

        var rcId = rcData.RCID;
        rcType = GetRootCauseTpeOfRCTNode(rcId);

        if (rcType == ACTUAL_ROOT_NODE_TYPE) {

            DrawActualRCTSection(container, canvas, context, jsonData.length, item, rcData, rcData.RCID, rcType, rcStatus, text);

        }
        else if (rcType == NEAR_ROOT_NODE_TYPE || (rcType == BASIC_ROOT_NODE_TYPE)) {

            DrawNearBasicRCTSection(container, canvas, rcType, rcData.RCID, item, jsonData.length, rcData, context, text, rcStatus)
        }


        SetAnalysisComments(container, rcData.RCID, analysisComments);

    }

}


function SetWidthAndHeightOfCanvas(canvas, rcId) {

    //EQ 1 , Canvas width chages depening on the number of child items presnet underneath it
    var width = (rcId == RCID_DESIGN_SPECIFICATIONS) ? '451' : (rcId == RCID_DESIGN) ? '481' : (rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE) ? '211' : '';

    var height = (rcId == RCID_QUALITY_CONTROL) ? 55
                                  : ((rcId == RCID_DESIGN) || (rcId == RCID_DESIGN_SPECIFICATIONS) || (rcId == RCID_DESIGN_REVIEW)
                                    || (rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE) || (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE)
                                    || (rcId == RCID_PM_NEEDS_IMPROVEMENT) || (rcId == RCID_NO_PM_FOR_EQUIPMENT)
                                    || (rcId == RCID_PM_FOR_EQUIPMENT_NI) || (rcId == RCID_REPEAT_FAILURE) || (rcId == RCID_MANAGEMENT_SYSTEM)) ? 100
                                  : canvas.height;

    canvas.setAttribute('width', width);
    canvas.setAttribute('height', height);

}



//Draw rectangle for near rootcause type nodes
function DrawActualRCTSection(container, canvas, context, length, item, rcData, rcId, rcType, rcStatus, text) {

    DisplayImageForActualRootCause(container, rcId, rcStatus);

    AppendElementsAndStoreID(container, canvas, rcId);

    //Draw only the text for the inner most rct node
    DrawActualRCTNode(container, canvas, context, length, item, rcId, rcStatus, text);

    DrawTools(container, rcId, rcType, text);

    var divBlock = '#divBlock' + rcId;
    RegisterClickEventForNearBasicActualRCTNode(divBlock, canvas, rcId, false);

}

function AddTextAndClickableArea(container, context, position, text, shapeType, rcId)
{

    var marginLeft = (shapeType == CIRCLE_SHAPE) ? 70 : (shapeType == PLAIN_TEXT) ? 0 : (shapeType == QUADRATIC_CURVE_SHAPE) ? 13 : (rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI) ? 4 : 10;
    var marginRight = (shapeType == CIRCLE_SHAPE) ? 70 :  (shapeType == PLAIN_TEXT) ? 0 : 10;

    var iDiv = document.createElement('div');
    iDiv.id = 'divBlock' + rcId; 

    var yPosition = GetYPosition(context, position, text, shapeType, FONT_11PX, rcId, marginLeft, marginRight);
    var style = '';


    //to be removed after insertion of portugese language
    // langaugeCode = langaugeCode.substring(langaugeCode.lastIndexOf('/') + 1);

    var langaugeCode = GetLanguageCode();
    //if ($.cookie != undefined && $.cookie('googtrans') != null) {

    //    langaugeCode = $.cookie('googtrans');
    //    langaugeCode = langaugeCode.substring(langaugeCode.lastIndexOf('/') + 1);

    //}

    if (shapeType == PLAIN_TEXT)
    {
        yPosition = yPosition - 12;
        style = "white-space:normal !important;cursor: pointer;position: absolute;top: " + yPosition + "px;z-index:2;line-height:15px;left:35px;display: block;width: 113px;text-align: left;vertical-align: middle !important;font-size: 11px;font-family: arial;";
    }
    else if (shapeType == CIRCLE_SHAPE)
    {
        yPosition = yPosition - 15;
        //style = "white-space:normal !important;height:75px;cursor: pointer;position: absolute;line-height:16px;top: " + yPosition + "px;z-index:2;display: block;width: " + 79 + "px;text-align: center;vertical-align: middle !important;font-size: 12px;font-family: arial;margin-left: " + marginLeft + "px;margin-right: " + marginRight + ";"
        style = "white-space: normal !important;height: 60px;cursor: pointer;position: absolute;line-height: 16px;top: 49px;z-index: 2;display: table;width: 160px;text-align: center;vertical-align: middle !important;font-size: 12px;font-family: arial;margin-left: 30px;";
    

    }
    else {
        
        var canvas = '#canvas' + rcId;

        var isManagementSystemNodes = ($(canvas).parent().parent().attr('id') == 'div' + RCID_HPD_MANAGEMENT_SYSTEM);
        var isCommunicationNodes = ($(canvas).parent().parent().attr('id') == 'div' + RCID_COMMUNICATIONS);
        isManagementSystemNodes = isManagementSystemNodes || isCommunicationNodes;

        var isDesignSpecChildNodes = ($(canvas).parent().parent().attr('id') == 'divWrapper' + RCID_DESIGN_SPECIFICATIONS);
        var isProcedureChildNodes = ($(canvas).parent().parent().attr('id') == 'div' + RCID_PROCEDURES);
        var isIndependentReviewNINode = $(canvas).attr('id') == CANVAS + RCID_INDEPENDENT_REVIEW_NI;



        var height = (isDesignSpecChildNodes || isProcedureChildNodes || isIndependentReviewNINode) ? 69 :
                     (isManagementSystemNodes || isCommunicationNodes) ? 71 : 60;

    
        ////Russian Fixes
        //if (_userLanguage == _russianLanguage) {
        //    yPosition = (isManagementSystemNodes) ? yPosition - 6 : ((rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI)) ? yPosition - 14 : yPosition - 15;

        //}
        ////Portugese Fixes
        //else if ((langaugeCode == _portugueseLanguage) || (_userLanguage == _portugueseLanguage)) {
            
        //    yPosition = (rcId == RCID_QUALITY_CONTROL) ? yPosition - 20 :(rcId == RCID_INDEPENDENT_REVIEW_NI) ? yPosition - 23 : (isManagementSystemNodes) ? yPosition - 12 : ((rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI)) ? yPosition - 19 : yPosition - 15;

        //}
        //else {
        //    yPosition = (isManagementSystemNodes) ? yPosition - 12 : ((rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI)) ? yPosition - 14 : yPosition - 15;

        //}

        var textWidth = ((rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI)) ? 95 : isManagementSystemNodes ? 118 :
            isCommunicationNodes || (rcId == RCID_QUALITY_CONTROL) ? 110 : 100;

  
        if (shapeType == QUADRATIC_CURVE_SHAPE)
        {            
            textWidth = 130;
            yPosition = 10;
            height = 33;
            style =
            "white-space: normal !important;cursor: pointer;position: absolute;top: " + yPosition + "px;z-index: 2;display: table;width: " + textWidth + "px;text-align: center;vertical-align: middle !important;font-size: 11px;font-family: arial;margin-left: " + marginLeft + "px;line-height: 14px;height:" + height + "px;";

        }
        else {

            text = (text == "Aufsicht/Personalbeziehungen") ? "Aufsicht/<br/>Personalbeziehungenn" : text;

            yPosition = 30;
            style =
            "white-space:normal !important;cursor: pointer;position: absolute;top: " + yPosition + "px;z-index:2;display: table;width: " + textWidth + "px;text-align: center;vertical-align: middle !important;font-size: 11px;font-family: arial;margin-left: " + marginLeft + "px;margin-right: 10px;height:" + height + "px;";


            //if ((_userLanguage == _russianLanguage) || (_userLanguage == _portugueseLanguage)) {
            if ((_userLanguage == _russianLanguage) || (_userLanguage == _portugueseLanguage) || (langaugeCode == _portugueseLanguage)
                 || (_userLanguage == _germanLanguage) || (langaugeCode == _germanLanguage)) {
                
                style =   style + "line-height: 14px;";
            }

        }
        
    }

    iDiv.setAttribute('style',style);
    container.appendChild(iDiv);

    //For making all text at the center and vertical align use this code later
    if (shapeType == QUADRATIC_CURVE_SHAPE || shapeType == RECTANGLE_SHAPE || shapeType ==  CIRCLE_SHAPE) {
        $(iDiv).html("<p style='display: table-cell;vertical-align: middle !important;width: 100%;text-align: center !important;'>" + text + "</p>");
    }
    else {
        $(iDiv).html(text);
    }
    
      

    if (_userLanguage != _germanLanguage && _userLanguage != _frenchLanguage && _userLanguage != _spanishLanguage && _userLanguage != _russianLanguage) {

        $(iDiv).removeClass('notranslate');
    }
    else {

        $(iDiv).addClass('notranslate');
    }

    //$(iDiv).html(function () { - IF NEEDED TO MAKE UPPERCASE
    //    return text.toUpperCase();
    //})

}


function DrawActualRCTNode(container, canvas, context, length, item, rcId, rcStatus, text) {       

    var style = (item == 0) ? FIRST_ROOT_CAUSE_STYLE : ACTUAL_ROOT_CAUSE_STYLE;

    var isManagementSystemNodes = ($(canvas).parent().parent().parent().attr('id') == 'div' + RCID_HPD_MANAGEMENT_SYSTEM);
    var isCommunicationNodes = ($(canvas).parent().parent().parent().attr('id') == 'div' + RCID_COMMUNICATIONS);

    var isProblemNotAncipatedNodes = ($(canvas).parent().parent().attr('id') == 'divWrapper' + RCID_PROBLEM_NOT_ANTICIPATED);
    var isProcedureChildNodes = ($(canvas).parent().parent().parent().attr('id') == 'div' + RCID_PROCEDURES);
    var isIndependentReviewNINodes = ($(canvas).parent().parent().attr('id') == 'divWrapper' + RCID_INDEPENDENT_REVIEW_NI);


    if (isManagementSystemNodes || isCommunicationNodes) {
        style = (item == 0) ? MANAGEMENT_SYSTEM_FIRST_ROOT_CAUSE_STYLE : ACTUAL_ROOT_CAUSE_STYLE;
    }
    else if (isProblemNotAncipatedNodes || isProcedureChildNodes || isIndependentReviewNINodes) {
        style = (item == 0) ? PROCEDURE_FIRST_ROOT_CAUSE_STYLE : ACTUAL_ROOT_CAUSE_STYLE;
    }

   // container.setAttribute('style', style);

    style = style + ';position: relative';
    container.setAttribute('style', style);

    //Draw the RCT Title as text
    if (rcId == RCID_EQUIPMENT_ENVIRONMENT_NOT_CONSIDERED) {

        //DrawText(context, 60, text, PLAIN_TEXT, FONT_11PX, rcId);
        DrawConnectorLineBetweenTexts(context);
        AddTextAndClickableArea(container, context, 120, text, PLAIN_TEXT, rcId);

        //var lineContext = canvas.getContext("2d");
        //lineContext.beginPath();
        //lineContext.moveTo(10, 15 + 0);
        //lineContext.lineTo(10, 15 + 40);
        //lineContext.stroke();
        //lineContext.closePath();

    }
    else {
        // DrawText(context, 130, text, PLAIN_TEXT, FONT_11PX, rcId);

        DrawConnectorLineBetweenTexts(context);
        AddTextAndClickableArea(container, context, 130, text, PLAIN_TEXT, rcId);
    }


    //DRAW THE VERTICAL LINES IN BETWEEN THE ACTUAL RCT NODES
    var startPointX = 10
    var startPointY = 15
    var endPointX = 10
    var endPointY = (item != length - 1) ? 15 + 60 : 15 + 41;

 //   if (item != length - 1) {

        var lineContext = canvas.getContext("2d");
        lineContext.beginPath();
        lineContext.moveTo(startPointX, startPointY);
        lineContext.lineTo(endPointX, endPointY);
        lineContext.stroke();
        lineContext.closePath();
  //  }

  
}


function DisplayImageForActualRootCause(container, rcId, rcStatus) {

    //Set Status Image
    var statusImage = document.createElement("img");
    statusImage.setAttribute("id", "imgStatus" + rcId);

    var style = (rcStatus) ? GREEN_STATUS_STYLE : RED_STATUS_STYLE;
    statusImage.setAttribute("style", style);

    var imageUrl = (rcStatus == null) ? '' : (rcStatus) ? GREEN_TICK_IMAGE : DELETE_RED_IMAGE;
    statusImage.setAttribute('src', imageUrl);

    if (imageUrl == '') {
        $(statusImage).hide();
    }
    else {
        $(statusImage).show();
    }

    container.appendChild(statusImage);

}

//===============================================================================================================================================================================


//Draw rectangle for near rootcause type nodes
function DrawNearBasicRCTSection(container, canvas, rcType, rcId, item, length, rcData, context, text, rcStatus) {

    AppendElementsAndStoreID(container, canvas, rcId);    

    //Draw rectangle with text
    DrawNearBasicRCTNode(container, context, canvas, item, rcId, text, length);

    DrawTools(container, rcId, rcType, text);

    var divBlock = '#divBlock' + rcId;
    RegisterClickEventForNearBasicActualRCTNode(divBlock, canvas, rcId, false);
   
    DrawRCTSectionsForChildNodes(rcData, canvas);

    ClearCanvasColor(context);

    strokeStyle = (rcType == BASIC_ROOT_NODE_TYPE) ? CONTEXT_DEFAULT_STROKE_COLOR : (rcStatus == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (rcStatus) ? GREEN_COLOR : RED_COLOR;

    var strokeRectColor = (strokeStyle == RED_COLOR) ? CONTEXT_DEFAULT_STROKE_COLOR : strokeStyle;
    RePaintCanvas(context, strokeRectColor);

    if (strokeStyle == RED_COLOR) {
        CreateRedCrossCanvas(rcId);
    }

}


function DrawNearBasicRCTNode(container, context, canvas, item, rcId, text, length, rcStatus) {    

    //FOR EQUIPMENT SECTIONNODE, STYLES ARE DIFFERENT BASED ON NODE
    SetStyleForNodes(container, rcId);

    var drawCurvedRect = IsCurvedRectangle(rcId);
    var position = (rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI) ? 98 : 110;
    var shapeType = (drawCurvedRect) ? QUADRATIC_CURVE_SHAPE : RECTANGLE_SHAPE;

    DrawRectangle(container, context, canvas, item == length - 1, item == 0, drawCurvedRect);
  
    AddTextAndClickableArea(container, context, position, text, shapeType, rcId);

    //DrawText(context, position, text, shapeType, FONT_11PX, rcId);

}

function IsCurvedRectangle(rcId)
{
    return (rcId == RCID_PROCUREMENT || rcId == RCID_MANUFACTURING || rcId == RCID_HANDLING || rcId == RCID_STORAGE || rcId == RCID_QUALITY_CONTROL);
}



function DrawRCTSectionsForChildNodes(rcData, canvas) {


    if (rcData.Children != undefined) {

        if (rcData.Children.length > 0) {

            _parent = $(canvas).parent()[0];
            _guidanceCount = rcData.GuidanceCount;
            DrawRCTSections(rcData.Children);

        }
    }
}


function DrawStylesForBasicNodes(rcId, nodeSelection)
{  
    
    //var borderStyle = (nodeSelection) ? "3px solid #52C55A" : "1px solid #000000";
    //var currentStyle = $('#rc' + rcId).attr('style');

    ////document.getElementById('rc' + rcId).setAttribute('style', currentStyle + borderStyle);
    ////$('#rc' + rcId).prop('css', currentStyle + borderStyle);
    
    //$('#rc' + rcId).prop('border', borderStyle);
    //document.getElementById('rc' + rcId).style.border = borderStyle;

    //if (nodeSelection == false) {
    //    CreateRedCrossCanvas(rcId);
    //}


    //var borderStyle = (nodeSelection) ? SELECTED_STYLE : UN_SELECTED_STYLE;
    //var currentStyle = $('#rc' + rcId).attr('style');

    //document.getElementById('rc' + rcId).setAttribute('style', borderStyle);
    //$('#rc' + rcId).prop('css', borderStyle);

    // $('#rc' + rcId).css("border-bottom-color", "#52C55A !important");

    
    var removeClass = (nodeSelection) ? 'unselectedBasicNode' : 'selectedBasicNode';
    var addClass = (nodeSelection) ? 'selectedBasicNode' : 'unselectedBasicNode';

    $('#rc' + rcId).removeClass(removeClass);
    $('#rc' + rcId).addClass(addClass);
    

    if (nodeSelection == false) {
        CreateRedCrossCanvas(rcId);
    }

}


function SetStyleForNodes(container, rcId) {

    var style = '';
   
    if (rcId == RCID_DESIGN) {
        style = DESIGN_STYLE;
    }
    else if (rcId == RCID_TOLERABLE_FAILURE) {
        style = TOLERABLE_FAILURE_STYLE;
    }
    else if (rcId == RCID_REPEAT_FAILURE) {
        style = REPEAT_FAILURE_STYLE;
    }
    else if (rcId == RCID_DESIGN_SPECIFICATIONS) {
        style = DESIGN_SPECIFICATIONS_STYLE;
    }
    else if (rcId == RCID_DESIGN_REVIEW) {
        style = DESIGN_REVIEW_STYLE;
    }
    else if (rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE) {
        style = EQUIPMENT_PARTS_DEFECTIVE_STYLE;
    }
    else if (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE) {
        style = PREVENTIVE_PREDICTIVE_MAINTAINANCE_STYLE;
    }
    else if (rcId == RCID_QUALITY_CONTROL) {
        style = QUALITY_CONTROL_STYLE;
    }
    else if (rcId == RCID_CORRECTIVE_ACTION) {

        style = CORRECTIVE_ACTION_NODE_STYLE;
    }
    else if ((rcId == RCID_SPECIFICATIONS_NI) || (rcId == RCID_DESIGN_NOT_TO_SPECIFICATIONS) || (rcId == RCID_PROBLEM_NOT_ANTICIPATED) || (rcId == RCID_INDEPENDENT_REVIEW_NI)) {
        style = DESIGN_SPECIFICATION_SUBNODE_STYLE;
    }
    else if (rcId == RCID_PROCUREMENT) {

        style = PROCUREMENT_ROOT_CAUSE_NODE_STYLE;
    }
    else if (rcId == RCID_MANAGEMENT_SYSTEM) {

        style = MANAGEMENT_ROOT_CAUSE_NODE_STYLE;
    }
    else if (rcId == RCID_PM_NEEDS_IMPROVEMENT) {
        style = PM_NEEDS_IMPROVEMENT_STYLE;

    }
    else if ((rcId == RCID_NO_INSPECTION) || (rcId == RCID_QC_NI)) {
        style = QUALITY_CONTROL_SUBNODE_STYLE;

    }
    else if ((rcId == RCID_NO_PM_FOR_EQUIPMENT) || (rcId == RCID_PM_FOR_EQUIPMENT_NI)) {
        style = PM_NI_SUBNODE_STYLE;

    }
    else if ((rcId == RCID_MANUFACTURING) || (rcId == RCID_HANDLING) || (rcId == RCID_STORAGE) || (rcId == RCID_QUALITY_CONTROL)) {
        style = EQUIPMENT_PARTS_DEFECTIVE_SUBNODE_STYLE;
    }
    else if (rcId == RCID_SUPERVISION_DURING_WORK) {
        style = SUPERVISION_DURING_WORK_STYLE;
    }    
    else {
        //Default Style

        var parentId = $('#canvas' + rcId).parent().parent().attr('id');
        if (parentId != undefined){
            parentId = parentId.substr(3, parentId.length - 1);
        }
       // alert(parentId);

        if ((parentId == RCID_PROCEDURES) || (parentId == RCID_TRAINING) || (parentId == RCID_HPD_QUALITY_CONTROL) || (parentId == RCID_COMMUNICATIONS)
            || (parentId == RCID_HPD_MANAGEMENT_SYSTEM) || (parentId == RCID_HUMAN_ENGINEERING) || (parentId == RCID_WORK_DIRECTION))
        {
            style = NO_WRAP_NEAR_NODE_STYLE;
        }
        else {
            style = DEFAULT_STYLE;
        }
        
    }

    style = style + 'position: relative';
    container.setAttribute('style', style);

}



//====================================

function RegisterClickEventForNearBasicActualRCTNode(divTextBlock, canvas, rcId, isMainTreeNode) {
 
    if(isMainTreeNode)
    {

        $(divTextBlock).live("click mouseover", function (event) {

            if (!($(this).next().next().is(":visible"))) {
                HideOptionToolBarAndShowCurrent($(this).next().next());
            }

        });

    }
    else {

        $(divTextBlock).live("click mouseover", function (event) {

            if (!($(this).next().is(":visible"))) {
                HideOptionToolBarAndShowCurrent($(this).next());
            }

        });

    }

    //$(canvas).mouseleave(function () { // Update later      and also add 'height:20px' in style section of 'AddTextAndClickableArea'
    //    if (($('#divTools' + rcId).is(":visible"))) {
    //        $('#divTools' + rcId).hide();
    //    }
    //});

    //$('#divTools' + rcId).mouseleave(function () { // Update later        
    //    if (($('#divTools' + rcId).is(":visible"))) {
    //        $('#divTools' + rcId).hide();
    //    }
    //});

}

