﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
function ClearShadow(context) {
    context.shadowBlur = 0;
    context.shadowOffsetX = 0;
    context.shadowOffsetY = 0;
}


function DrawShadow(context) {
    context.shadowColor = '#999';
    context.shadowBlur = 10;
    context.shadowOffsetX = 5;
    context.shadowOffsetY = 5;
}
