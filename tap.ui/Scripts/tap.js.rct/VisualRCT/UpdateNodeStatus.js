﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
/*===============================================================================================================================================================================
Update Status Section*/


/*Update status[Green/red/black] of the node clicked based on selection and display the related section if needed to display.
Also it will set the status for the parent and child nodes - based on c3 need []*/
function UpdateNodeStatusAndSection(rcId, status) {
      
    var effectSet = true;
    var rctNodeType = GetRootCauseTpeOfRCTNode(rcId);

    //if (rctNodeType == TREE_NODE_TYPE) {
    //    effectSet = UpdateStatusAndSectionForMainTreeNode(rcId, status);
    //}
    //else
    if (rctNodeType == BASIC_ROOT_NODE_TYPE) {
        effectSet = UpdateStatusForBasicNode(rcId, status)

    }
    else if (rctNodeType == NEAR_ROOT_NODE_TYPE) {
        effectSet = UpdateStatusForNearNode(rcId, status);

    }
    else if (rctNodeType == ACTUAL_ROOT_NODE_TYPE) {
        effectSet = UpdateStatusForRootNode(rcId, status);

    }

    return effectSet;

}

function HigherRectangularEliminated(canvasId)
{

   // $(canvasId).parent().parent().find('a').prev()[0].tagName
    //return ($(canvasId).parent().parent().children("canvas[id$='RedCross']").length == 1);
    return ($(canvasId).parent().parent().children("canvas[id$='RedCross']").is(":visible"));
}


function UpdateStatusForRootNode(rcId, status) {
    
    var effectSet = true;

    var statusImage = "#imgStatus" + rcId;

    var context = $(statusImage).parent().parent().find('canvas')[0].getContext('2d');
    
    if (HigherRectangularEliminated('#canvas' + rcId)) {
        
        if ((status) || (status == null)) {
            alert(HIGHERITEM_ELIMINATED_MESSAGE);
            return false;
        }
    }
   

    var imageUrl = (status == null) ? '' : (status) ? GREEN_TICK_IMAGE : DELETE_RED_IMAGE;
    var style = (status) ? GREEN_STATUS_STYLE : RED_STATUS_STYLE;

    $(statusImage).attr('style', style);
    $(statusImage).attr('src', imageUrl);

    if (imageUrl == '') {
        $(statusImage).hide();
    }
    else {
        $(statusImage).show();
    }
    
    if (status) {
        SelectAllParentNode(statusImage);
    }
    else if (status == false)
    {
        var parentNode = $(statusImage).parent().parent();

        if (CheckIfAllChildUnSelected(parentNode))
        {
            //var nearParentDivs = $(statusImage).parent().parentsUntil('.div-admin-tabs');
            //var basicParentDiv = $(statusImage).closest('.div-admin-tabs');

            //$(nearParentDivs).each(function (index, value) {

                var context = $(parentNode).children('canvas').not('[id$="RedCross"]')[0].getContext('2d');
                RePaintCanvas(context, GREEN_COLOR);
            //});
        }
    }

    return effectSet;
}

function CheckIfAllChildUnSelected(parentNode)
{
    //debugger;
    //var parentId = $($('#canvas' + rcId).parent().parent().find('input[type="hidden"]')[1]).val();

    //var canvasUnderParentBasicNode = '#div' + parentId + ' > div > canvas';

    //var length = $('#canvas' + rcId).parent().find('canvas').count();
    //alert(canvasUnderParentBasicNode);

   

    var totalActualRCTNodesLength = (parentNode).find("canvas").length - 1;
    var totalUnseletedActualRCTNodesLength = parentNode.find("img[id^='imgStatus'][src$='DeleteRed.png']").length;

    var allQuestionUnselected = (totalActualRCTNodesLength == totalUnseletedActualRCTNodesLength);
    _allQuestionUnselected = allQuestionUnselected;

    return allQuestionUnselected;

}



function CheckIfAllNearChildUnSelected(parentNode) {
    //debugger;
    //var parentId = $($('#canvas' + rcId).parent().parent().find('input[type="hidden"]')[1]).val();

    //var canvasUnderParentBasicNode = '#div' + parentId + ' > div > canvas';

    //var length = $('#canvas' + rcId).parent().find('canvas').count();
    //alert(canvasUnderParentBasicNode);

    var wrapperId = parentNode.attr('id');    
    var totalNearRCTNodesLength = $('#' + wrapperId + ' > div > canvas').not('[id$="RedCross"]').length;
    var totalUnseletedNearRCTNodesLength = $('#' + wrapperId + ' > div > canvas').filter('canvas[id$="RedCross"]').length;


    var allQuestionUnselected = (totalNearRCTNodesLength == totalUnseletedNearRCTNodesLength);
    _allQuestionUnselected = allQuestionUnselected;

    return allQuestionUnselected;

}

function IsAnyChildNodeSelected(rcId, isBasicNode)
{
    
    var userResponse = true;
    var messageShown = false;

    if (isBasicNode) {
    
    var canvasUnderParentBasicNode = '#div' + rcId + ' > div > canvas';

    $(canvasUnderParentBasicNode).each(function () {

            if (!messageShown){

                var canvasCtx = $(this)[0].getContext('2d');

                if (canvasCtx.strokeStyle == '#008000') {

                    userResponse = confirm('One or more causes are currently selected at a lower level.Removing this cause will also remove them. Do you want to continue?');
                    messageShown = true;
                    return false;
                }
            }
        });
    }
    else {

        var rootCauseNodes = $($('#canvas' + rcId).parent()[0]).find("img[id^='imgStatus']");

        $(rootCauseNodes).each(function () {

            if (!messageShown) {
                if (($(this).prop('src').indexOf("GreenTick1.png") > 1) && $(this).is(':visible')) {

                    userResponse = confirm('One or more causes are currently selected at a lower level.Removing this cause will also remove them. Do you want to continue?');
                    messageShown = true;
                    return false;

                }
            }
        });


        if (!IsCurvedRectangle(rcId)) {

            // var canvasUnderParentBasicNode = $($('#canvas' + rcId).parent()[0]).find("canvas");
            //var canvasUnderParentBasicNode = '#canvas' + rcId + ' > div > canvas';
            //var canvasUnderParentBasicNode = $('#canvas' + rcId).siblings().children('canvas');
            var canvasUnderParentBasicNode = $('#canvas' + rcId).siblings().children('canvas').not('[id$="RedCross"]');

            $(canvasUnderParentBasicNode).each(function () {

                if (!messageShown) {
                    var canvasCtx = $(this)[0].getContext('2d');

                    if (canvasCtx.strokeStyle == '#008000') {

                        userResponse = confirm('One or more causes are currently selected at a lower level.Removing this cause will also remove them. Do you want to continue?');
                        messageShown = true;
                        return false;
                        //if(confirm('One or more causes are currently selected at a lower level.Removing this cause will also remove them. Do you want to continue?'))
                        //{
                        //    return true;
                        //}
                    }
                }
            });
        }
    }

    return userResponse;

    //var rootCauseNodes = $($(canvasId).parent()[0]).find("img[id^='imgStatus']");
    //$(rootCauseNodes).each(function () {
    //    if ($(this).prop('src').indexOf("GreenTick1.png") > 1) {
    //        userResponse = confirm('One or more causes are currently selected at a lower level.Removing this cause will also remove them. Do you want to continue?');
    //        effectSet = userResponse;
    //    }
    //});

}

function UpdateStatusForBasicNode(rcId, status) {
    
    var effectSet = true;
    var userResponse = true;
 
    if (!status) {

        userResponse = IsAnyChildNodeSelected(rcId, true);
        effectSet = userResponse;
    }

    if (userResponse) {

      var strokeStyle = (status == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (status) ? GREEN_COLOR : RED_COLOR;
      var strokeRectColor = (strokeStyle == RED_COLOR) ? CONTEXT_DEFAULT_STROKE_COLOR : strokeStyle;
      //  ////var borderStyle = (strokeStyle == GREEN_COLOR) ? "3px solid #52C55A !important" : "1px solid #000000 !important";

        
      //  ////var currentStyle = $('#rc' + rcId).attr('style');
      //  ////document.getElementById('rc' + rcId).setAttribute('style', currentStyle + borderStyle);
      //  //// $('#rc' + rcId).prop('css', currentStyle + borderStyle);
       
      //  // $('#rc' + rcId).prop('border',  borderStyle);
      //  // document.getElementById('rc' + rcId).style.border = borderStyle;

      //  var strokeStyle = (status == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (status) ? GREEN_COLOR : RED_COLOR;
      //  var strokeRectColor = (strokeStyle == RED_COLOR) ? CONTEXT_DEFAULT_STROKE_COLOR : strokeStyle;
      //  var borderStyle = (strokeStyle == GREEN_COLOR) ? "border: solid 3px #52C55A !important" : "border:  solid 1px #000000 !important";
        
      //  var currentStyle = $('#rc' + rcId).attr('style');
      //  var style = currentStyle + borderStyle;
      ////  alert(style);
      //  //document.getElementById('rc' + rcId).setAttribute('style', style);
      //  //$('#rc' + rcId).prop('css', currentStyle + borderStyle);
      //  //  $('#rc' + rcId).css("border-color", "#52C55A !important");
      //  $('#rc' + rcId).css('border', '');
      //  //$('#rc' + rcId).css({
      //  //    "border-color": "#C1E0FF  !important",
      //  //    "border-width": "1px  !important",
      //  //    "border-style": "solid !important"
      //  //});

      var removeClass = (strokeStyle == GREEN_COLOR) ? 'unselectedBasicNode' : 'selectedBasicNode';
        var addClass = (strokeStyle == GREEN_COLOR) ? 'selectedBasicNode' : 'unselectedBasicNode';
        
        $('#rc' + rcId).removeClass(removeClass);
        $('#rc' + rcId).addClass(addClass);



        if (strokeStyle == RED_COLOR) {
            CreateRedCrossCanvas(rcId);
        }
        else
        {
            EraseRedCrossCanvas(rcId);
        }


        if ((strokeStyle == RED_COLOR) || (strokeStyle == CONTEXT_DEFAULT_STROKE_COLOR))
        {
            var canvasUnderParentBasicNode = '#div' + rcId + ' > div > canvas';
            var paintColor = (status == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (!status) ? RED_COLOR : GREEN_COLOR;

            $(canvasUnderParentBasicNode).each(function () {

                var canvasId = $(this).attr('id');
                var rcId1 = canvasId.substr(6, canvasId.length - 1);
                var canvasCtx = $(this)[0].getContext('2d');
                var strokeStyle = (status == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (status) ? GREEN_COLOR : RED_COLOR;

                var strokeRectColor = (strokeStyle == RED_COLOR) ? CONTEXT_DEFAULT_STROKE_COLOR : strokeStyle;

                if (strokeStyle == RED_COLOR) {
                    RePaintCanvas(canvasCtx, strokeRectColor);
                    CreateRedCrossCanvas(rcId1);
                }


                if (status == null) {
                    
                    $('#' + canvasId).parent().find("img[id^='imgStatus']").hide();
                    RePaintCanvas(canvasCtx, strokeRectColor);
                    RePaintCanvasChildNodes(canvasId, CONTEXT_DEFAULT_STROKE_COLOR);

                    EraseRedCrossCanvas(rcId1);
                }
                else if (!status) {
                    UnSelectAllRootCauseNode($('#' + canvasId).parent().find("img[id^='imgStatus']"));
                    RePaintCanvas(canvasCtx, strokeRectColor);
                    RePaintCanvasChildNodes(canvasId, RED_COLOR);
                }

            });
        }
    
    }

    return effectSet;
}


function UnSelectAllRootCauseNode(imgStatus) {

    $(imgStatus).prop("style", RED_STATUS_STYLE);
    $(imgStatus).prop('src', DELETE_RED_IMAGE);
    $(imgStatus).show();


}


function UpdateStatusForNearNode(rcId, status) {

    
    var canvasId = '#canvas' + rcId;
    var basicNodeId = $(canvasId).parent().parent().find('a').prop('id');


    var userResponse = true;
    var effectSet = true;
    var canvasId = '#canvas' + rcId;

    var basicNodeStyle = $(canvasId).parent().parent().find('a').attr('style');
    basicNodeStyle = basicNodeStyle == undefined ? '' : basicNodeStyle;
    var isHigherLevelUnSelected = false;
    var canvasCtx = '';

    //if (basicNodeStyle == '') {

    //    var canvasCtx = $(canvasId).parent().parent().find('canvas')[0].getContext('2d');
    //}


    //if (basicNodeStyle == '' || basicNodeStyle == undefined)
    //{
    //    var canvasCtx = $('#canvas' + rcId)[0].getContext('2d');
    //    isHigherLevelUnSelected = (canvasCtx.strokeStyle == "#90ee90");        
    //}
    //else
    //{
    //    isHigherLevelUnSelected =  basicNodeStyle.indexOf("background-color: rgb(227, 36, 65);") > 1
    //}
    //isHigherLevelUnSelected = basicNodeStyle.indexOf("background-color: rgb(227, 36, 65);") > 1 || (canvasCtx.strokeStyle == "#90ee90");
    isHigherLevelUnSelected = HigherRectangularEliminated('#canvas' + rcId);

    //  
    if (isHigherLevelUnSelected) {

        if (status || (status == null)) {
            alert(HIGHERITEM_ELIMINATED_MESSAGE);
            return false;
        }
    }


    if (!status) {
        //debugger;
        userResponse = IsAnyChildNodeSelected(rcId, false);
        effectSet = userResponse;
    }
   
    if (userResponse) {

        var canvasCtx = $(canvasId)[0].getContext('2d');
        var strokeStyle = (status == null) ? CONTEXT_DEFAULT_STROKE_COLOR : (status) ? GREEN_COLOR : RED_COLOR;

        var strokeRectColor = (strokeStyle == RED_COLOR) ? CONTEXT_DEFAULT_STROKE_COLOR : strokeStyle;

        
        //if (strokeRectColor == GREEN_COLOR) {
        //    canvasCtx.lineWidth = 3;
        //}
        //ClearCanvasColor(canvasCtx);
        RePaintCanvas(canvasCtx, strokeRectColor);


        if (strokeStyle == RED_COLOR) {
            CreateRedCrossCanvas(rcId);
        }


        if (status == null) {
            $($(canvasId).parent()[0]).find("img[id^='imgStatus']").hide();
            RePaintCanvasChildNodes(canvasId, CONTEXT_DEFAULT_STROKE_COLOR);

            EraseRedCrossCanvas(rcId);
        }
        else if (!status) {
            UnSelectAllRootCauseNode($($(canvasId).parent()[0]).find("img[id^='imgStatus']"));
            RePaintCanvasChildNodes(canvasId, RED_COLOR);

            var parentNode = $(canvasId).parent().parent();

            //add code
            if (CheckIfAllNearChildUnSelected(parentNode)) {
                //var nearParentDivs = $(statusImage).parent().parentsUntil('.div-admin-tabs');
                //var basicParentDiv = $(statusImage).closest('.div-admin-tabs');

                //$(nearParentDivs).each(function (index, value) {

                var rctNode = $(parentNode).children('canvas').not('[id$="RedCross"]')[0];
                if (rctNode != undefined)
                {
                    var context = $(parentNode).children('canvas').not('[id$="RedCross"]')[0].getContext('2d');
                    RePaintCanvas(context, GREEN_COLOR);

                }
                else {
                    
                    var basicParentDiv = $(parentNode).closest('.div-admin-tabs');
                    $(basicParentDiv).children('a').removeClass('unselectedBasicNode');
                    $(basicParentDiv).children('a').addClass('selectedBasicNode');
                }

                //});
            }
        }
        else if (status) {

            ////var basicParentNode = $(canvasId).parent().parent().find('a');
            ////$(basicParentNode).css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });
            //var removeClass = (status) ? 'unselectedBasicNode' : 'selectedBasicNode';
            //var addClass = (status) ? 'selectedBasicNode' : 'unselectedBasicNode';

            //$('#rc' + rcId).removeClass(removeClass);
            //$('#rc' + rcId).addClass(addClass);

            SelectAllParentNode(canvasId);


            EraseRedCrossCanvas(rcId);
        }
    }

    return effectSet;
}


function RePaintCanvasChildNodes(canvasId, color) {
    var canvasUnderParentNode = $($(canvasId).parent()[0]).find("canvas");
    $(canvasUnderParentNode).each(function () {

        var canvasCtx = $(this)[0].getContext('2d');

        //if (color != RED_COLOR) {
        //    RePaintCanvas(canvasCtx, color);
        //}

       // RePaintCanvas(canvasCtx, color);
        if (color == RED_COLOR) {
            var focusedLinkID = $(this).attr('id');
            var rcId = parseInt(focusedLinkID.substr(6, focusedLinkID.length - 1));
            RePaintCanvas(canvasCtx, CONTEXT_DEFAULT_STROKE_COLOR);
            CreateRedCrossCanvas(rcId);
        }
        else if (color == CONTEXT_DEFAULT_STROKE_COLOR) {
            RePaintCanvas(canvasCtx, color);
            var focusedLinkID = $(this).attr('id');
            var rcId = parseInt(focusedLinkID.substr(6, focusedLinkID.length - 1));
            EraseRedCrossCanvas(rcId);
        }
    });

}


function SelectAllParentNode(statusImage) {

    //var context = $(statusImage).parent().parent().find('canvas')[0].getContext('2d');
//    var context = $(statusImage).parent().parent().children('canvas').not('[id$="RedCross"]')[0].getContext('2d');
    //debugger;


    //var nearParentDivs = $(statusImage).parentsUntil('.div-admin-tabs');
    var nearParentDivs = $(statusImage).parent().parentsUntil('.div-admin-tabs');
    var basicParentDiv = $(statusImage).closest('.div-admin-tabs');

    $(nearParentDivs).each(function (index, value) {

        var context = $(this).children('canvas').not('[id$="RedCross"]')[0].getContext('2d');
        RePaintCanvas(context, GREEN_COLOR);
    });

    $(basicParentDiv).children('a').removeClass('unselectedBasicNode');
    $(basicParentDiv).children('a').addClass('selectedBasicNode');


   
    ////var parentDivs = $(statusImage).parentsUntil('a');

    ////$(parentDivs).find('canvas').each(function () {

    ////});

    //var basicParentNode = $(statusImage).parent().parent().parent().find('a');
    ////$(basicParentNode).css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });

    //$(basicParentNode).removeClass('unselectedBasicNode');
    //$(basicParentNode).addClass('selectedBasicNode');


}

