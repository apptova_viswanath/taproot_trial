﻿// THIS FILE HAS BEEN CREATED TO HOLD ALL THE FUNCTION RELATED TO RCT ANALYSIS COMMENTS SECTION


// Ready function
$(document).ready(function () {

    //Display the rct questions on click of each header link, except generic cause link
    $('#btnSaveAnalysisComments').live("click", function (event) {
        
        if($('.buttonGeneric').is(':visible')){
            ValidateAndSaveAnalysisComments(this);
       }
    });

    // Remove the validation error from text box and Close the dialog div
    $('#btnCancelAnalysisComments').live("click", function (event) {
        CloseDialog(this, 'txtAnalysisComments');
    });

});


// Validate analysis comments and save
function ValidateAndSaveAnalysisComments(sender) {
   // debugger;

    var txtTextEdit = 'txtAnalysisComments';
    var isValid = true;
    var parentDiv = $(sender).parentsUntil('.ui-dialog');

    var desc = parentDiv.find('#hdnAnalysisComments').val();//$(sender).parents().eq(1).find('#hdnAnalysisComments').val();

    // Check validation
    if (desc == '' || desc == undefined || desc == "null" || desc == "") {
        isValid = IsReStateValid(sender, txtTextEdit, 'Please specify analysis comments.', parentDiv);
    }

    // Save if validated and close dialog
    if (isValid) {
     
        SaveAnalysisComments(sender, parentDiv);
        CloseDialog(sender, txtTextEdit);
        
    }

}


// Save analysis comments
function SaveAnalysisComments(sender, parentDiv) {

    var rctData = {};
    //var parentDiv = $(sender).parentsUntil('.ui-dialog');//$(sender).parents(); //$(sender).parents().eq(1);

    //Get causal factor id
    rctData.causalFactorID = GetCausalFactorID();

    //Get rct id
    var rootCauseID = parseInt(parentDiv.find('#hdnRCIDForAnalysisComments').val());
    rctData.rootCauseID = rootCauseID;

    //get the user response
    //var analysisComments = $(sender).parents().eq(1).find('#txtAnalysisComments').val();
    var analysisComments = parentDiv.find('#txtAnalysisComments').val();
    rctData.analysisComments = analysisComments;
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/SaveRCTAnalysis';
    AjaxPost(serviceURL, rctData, eval(SaveRCTAnalysisSuccess), eval(FailedCallback));

    //Set the value in hidden field
    $(parentDiv).find('#hdnAnalysisComments').val(analysisComments);
    

    //Close dialog
    //CloseDialog(sender, 'txtAnalysisComments');        

    if ($('#hdnAnalysisComments' + rootCauseID).length)        
    {
        $('#hdnAnalysisComments' + rootCauseID).val(analysisComments);
    }
    
    //Set the analysis comments image image based on analysis comments value
    var analysisCommentsImage = '#imgAnalysisComments' + rootCauseID;
    var analysisCommentsImageSource = (analysisComments == '') ? 'Images/GreyStatus.png' : 'Images/GreenStatus.png';
    $(analysisCommentsImage).attr('src', _baseURL + analysisCommentsImageSource);
    if (IsVisualRCTPage()) {

        SetCommentsImage(rootCauseID, (analysisComments != ''));
    }

}


// Get analysis comment div
function GetAnalysisCommentDiv(sender) {
    return $(sender).parent().find('#divParent').find('#divRCTQuestions').children().find('.analysisComments').first();
}


// Display analysis pop up
function DisplayAnalysisPopUp(sender) {

    var dialog = GetAnalysisCommentDiv(sender);
    $(dialog).find('#txtAnalysisComments').val($(dialog).find('#hdnAnalysisComments').val());

    $(dialog).find('#btnSaveAnalysisComments').attr('title', '');
    $(dialog).find('#btnCancelAnalysisComments').attr('title', '');

    OpenPopup(dialog, 'Analysis Comments');

}



// On success of SaveRCTAnalysis
function SaveRCTAnalysisSuccess(data) {
}