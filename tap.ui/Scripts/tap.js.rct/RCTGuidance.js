﻿// THIS FILE HAS BEEN CREATED TO HOLD ALL THE FUNCTION RELATED TO RCT GUIDANCE SECTION


//Display guidance pop up, on click of bulb icon from each RCT node
function DisplayGuidancePopUp(rootCuaseId, linkType) {

    //Declare the parameter object
    var rctData = {};
    //debugger;
    rctData.rootCauseID = parseInt(rootCuaseId);
    rctData.linkType = linkType;
    rctData.userId = _userId
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetRCTGuidance';
    AjaxPost(serviceURL, rctData, eval(GetRCTGuidanceSuccess), eval(FailedCallback));

}


//On success of getting RCT guidance for any rct node, display the content in a pop up div
function GetRCTGuidanceSuccess(data) {
    var germanLanguage = 'de';
    var frenchLanguage = 'fr';
    var spanishLanguage = 'es';
    
    
    //parse the json result to object
    data = ParseToJSON(data.d);

    var selectedLanguage = data[0]["RCTLanguageName"];


    var ctrl = document.createElement('div');
    $(ctrl).attr('id', 'divDic');
    $(ctrl).attr('title', data[0].Title);

    // Get Title
    var title = '';
    if (data[0].Title.split(':')[1] != undefined) {
        title = data[0].Title.split(':')[1];
    }

   
   
    if (data[0].LinkType == 'RCTDictionaryLink' || data[0].LinkType == "RCTQuestionLink") {

        //Get the guidance
        data = data[0].Guidance;

        for (var i = 0; i < data.length; i++) {
            $(ctrl).html($(ctrl).html() + '<br>' + data[i]);
         
        }
    }    
    else {
        $(ctrl).html(data[0].Guidance);
    }
   

    var addMessage = '  <div class="warningYellowImage">' +
                       '<i><p class="notranslate" >You are viewing the English version of TapRooT®, translated by Google Translation.  This translation has not been sanctioned or reviewed by TapRooT®.</p></i><br />' +
                   '</div>';

 
    if (selectedLanguage != germanLanguage && selectedLanguage != frenchLanguage && selectedLanguage != spanishLanguage && selectedLanguage != 'ru' && selectedLanguage != 'undefined' && selectedLanguage != 'en') {
        $(ctrl).html(addMessage + $(ctrl).html());

    }
    else {
        $(ctrl).html($(ctrl).html());

    }



    if (selectedLanguage != germanLanguage && selectedLanguage != frenchLanguage && selectedLanguage != spanishLanguage && selectedLanguage != 'ru') {

        $(ctrl).removeClass('notranslate');
    }
    else {

        $(ctrl).addClass('notranslate');
    }

    //Display dialog

    DisplayDialog(ctrl, title);

    // Set click events for each anchor tag present inside the light bulb content
    SetClickEventForAnchorTags(ctrl);

    

     $(ctrl).find('span').each(function () {
        $(this).attr('style', '');
    });

}


// Display the jquery dialog
function DisplayDialog(ctrl, title) {

    $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        draggable: true,
        resizable: true,
        position: ['middle', 100],
        title: title

    });

}


// Set click events for each anchor tag present inside the content
function SetClickEventForAnchorTags(ctrl) {

    //set the title class, as it conflicts with the jquery accordion title class
    var title = $(ctrl).prev();
    $(title).removeClass('ui-widget-header');
    $(title).addClass('popupTitle');

    //$(ctrl).find('a').each(function () { // REMOVED BECAUSE WE DONT NEED ANY CLICK FUNCUTIONALITY FROM THE POP UP CONTENT, FROM DATABASE ALSO REMOVED ANCHOR TAG
    //WILL UPDATE LATER

    //    $(this).live("click", function (event) {

    //        var linkType = $(this).attr('class');
    //        var rctID = $(this).attr('id');
    //        DisplayGuidancePopUp(rctID, linkType);

    //    });
    //});

}

