﻿// THIS FILE HAS BEEN CREATED TO HOLD ALL THE FUNCTION RELATED TO RADIO BUTTONS AND QUESTION EFFECT
var _isVisualRCTPage = false;

//Set node status (green tick or red cross), when any user answer any question node by checking yes or no radio buttons
function SetRCTNodeStatus(radioButton, event) {
    
    var divStatusImage = $(radioButton).parents().eq(1).parent().parent().find('ul').first().parent().prev();
    SetEffects(divStatusImage, true, radioButton, event, false, false, false);

}


//On click of square div or any question radio button, set the response status , save the user response, change the square div image
function SetEffects(divStatusImage, isFromRadioButton, radioButton, radioButtonevent, setAllNo, setAllBlank, selectNode) {
    
    //Get the focused tab text
    var focusedMainTabText = $('.focused').next().next().val();
    var titleHeader = GetTitleHeader(divStatusImage, isFromRadioButton, radioButton);
    var rctNodeID = GetCorrespondingRCTNodeID(titleHeader);
        
    var userResponse = (setAllNo) ? USER_RESPONSE_NO : (setAllBlank) ? USER_RESPONSE_NULL : (selectNode) ? USER_RESPONSE_YES : GetRCTNodeStatus(divStatusImage, isFromRadioButton, radioButton);

    var isNearCauseTab = (focusedMainTabText != GETTING_STARTED_TAB_TEXT && focusedMainTabText != FIFTEEN_QUESTION_TAB_TEXT);
    var isGettingStartedTab = (rctNodeID == GETTING_STARTED_TAB_ID && (focusedMainTabText == GETTING_STARTED_TAB_TEXT));

    //Only applied to the tabs which is not 'Getting Started' and '15 Questions', as those don't have the multi level accordion
    if (isNearCauseTab) {

        //If any of the  higher level node is unselected, alert the user
        var higherItemEliminated = CheckIfHigherItemEliminated(divStatusImage);
        var isNodeUnSelected = $(divStatusImage).hasClass('redCross');

        if (higherItemEliminated) {
                      
            if ((selectNode) || (userResponse == USER_RESPONSE_NULL)) {
                alert(HIGHERITEM_ELIMINATED_MESSAGE);
                return false;
            }

            if (!isFromRadioButton && isNodeUnSelected) {
                return false;
            }
            
            alert(HIGHERITEM_ELIMINATED_MESSAGE);
            PreventDefault(radioButton);
            return false;
            
        }
        else if (userResponse == USER_RESPONSE_NO || userResponse == USER_RESPONSE_NULL) {
            
            var hasEffect = SetEffectsForDeSelect(divStatusImage, isFromRadioButton, radioButton, userResponse);
            if (!hasEffect) {
                return false;
            }

            // Delete all status from the child nodes
            DeleteStatusForAllChildNodes(divStatusImage, titleHeader, userResponse);
          
        }
        else if (userResponse == USER_RESPONSE_YES) {

            //Green check all the parent nodes
            CheckHigherLevelNodes(titleHeader);
            return false;

        }

    }
    else if (isGettingStartedTab) {
        var hasEffect = SetEffectsForBasicTab(rctNodeID, focusedMainTabText, divStatusImage, isFromRadioButton, radioButton);
        if (!hasEffect) {
            return false;
        }
    }
        
    //Set the rct status image and make visible/invisible top links
    SetQuestionImageAndTopink(userResponse, divStatusImage, isFromRadioButton, radioButton, false);

    //Near Root Causes Are Root Causes when Children are Eliminated
    if (userResponse == USER_RESPONSE_NO && IsRootCause(divStatusImage))
    {
        //find if all children of it parent is unchecked
        var parent = $(titleHeader).parents().eq(4);
        var uncheckedChildLength = $(parent).find('.redCross').length;
        var childLength = $(parent).find('#divStatusImage').length;

        //if true save the true value in db for it parent node
        if (uncheckedChildLength == childLength) {

             if ($(parent).prev('#divStatusImage').css != 'greenCheck') {

                 $(parent).prev('#divStatusImage').removeClass().addClass('greenCheck');

                rctNodeID = GetCorrespondingRCTNodeID($(parent).find('#divRCTTitle'));

                //Save status
                SaveRCTQuestionNode(rctNodeID, USER_RESPONSE_YES , true);

            }
        }
        
    }
}


function IsRootCause(divStatusImage)
{
    return true;
}
// Set effect for getting started tab
function SetEffectsForBasicTab(rctNodeID, focusedMainTabText, divStatusImage, isFromRadioButton, radioButton) {
    
    var hasEffect = true;

    if (rctNodeID == GETTING_STARTED_TAB_ID && (focusedMainTabText == GETTING_STARTED_TAB_TEXT)) {

        //If 15 question tab is green marked/ iterated
        var tabStyle = jQuery("[name=" + rctNodeID + "-]").attr('style');

        if ((tabStyle == TAB_ITERATED_STYLE) || (tabStyle == TAB_ITERATED_STYLE_IE)) {

            if (isFromRadioButton) {

                // IF radio button response is NO and all no selected / no yes selected then show the message to user
                if (CheckIfAllNoSelected(radioButton)) {

                    alert(FIFTEEN_QUESTION_MESSAGE);
                    PreventDefault(radioButton);
                    hasEffect = false;
                }

            }
            else {
                if ($(divStatusImage).hasClass(USER_SELECTED_IMAGE_CLASS)) {

                    alert(FIFTEEN_QUESTION_MESSAGE);
                    hasEffect = false;

                }
            }
        }
    }

    return hasEffect;

}


// Set effects for deselecting a rct node
function SetEffectsForDeSelect(divStatusImage, isFromRadioButton, radioButton, userResponse) {

    var markDeselect = true;

    //Get confirm from user to remove the child RCT nodes as well
    if (IsAnyChildSelected(divStatusImage, isFromRadioButton, radioButton, userResponse)) {
        markDeselect = confirm(CHILDITEM_SELECTED_MESSAGE);
    }

    //Delete the current RCT node and all child RCT nodes selected before
    if (!markDeselect) {

        $(radioButton).attr('checked', false);
        var nodeStatus = $(radioButton).parent().find('#hdnQuestionResponse').val();

        if (nodeStatus == 'true') {
            $(radioButton).prev().attr('checked', true);
        }
    }

    return markDeselect;

}


//Check if all no selected / no yes selected
function CheckIfAllNoSelected(radioButton, hasEffect) {

    var isAllNoSelected = false;

    // IF radio button response is NO
    if ($(radioButton).attr('id') == 'rbNo') {

        var questionContent = $(radioButton).parents().eq(1);
        isAllNoSelected = (questionContent.find("input:radio[id^='rbYes']:checked").length == 0);
    }

    return isAllNoSelected;

}


// Prevent default radio button event and reset the radio buttons checked status
function PreventDefault(radioButton) {

    // Get the previous value from the hidden variable
    var questionResponse = $(radioButton).parent().find('#hdnQuestionResponse').val();
    var rbYes = $(radioButton).parent().find('#rbYes');
    var rbNo = $(radioButton).parent().find('#rbNo');

    //Get the status for the radio buttons
    var rbYesCheckedStatus = (questionResponse == RESPONSE_TRUE) ? true : false;
    var rbNoCheckedStatus = (questionResponse == RESPONSE_FALSE) ? true : false;

    // reset radio button status
    $(rbYes).attr(CHECKED_ATTR, rbYesCheckedStatus);
    $(rbNo).attr(CHECKED_ATTR, rbNoCheckedStatus);


    if ($(radioButton).parents().eq(3).prev().hasClass(USER_UNSELECTED_IMAGE_CLASS)) {
        $(rbYes).attr(CHECKED_ATTR, false);
        $(rbNo).attr(CHECKED_ATTR, true);
    }

}


//Delete yes/no status set for all child nodes
function DeleteStatusForAllChildNodes(divStatusImage, titleHeader, userResponse) {

    //Get all the child level divs
    var childDivs = $(divStatusImage).next().children().eq(1).find('.parent');
    var noRadioStatus = (userResponse == USER_RESPONSE_NO) ? true : false;

    $(childDivs).each(function () {

        //remove the selection of Yes and No question response
        $(childDivs).find("input:radio[id^='rbNo']").prop(CHECKED_ATTR, noRadioStatus);
        $(childDivs).find("input:radio[id^='rbYes']").prop(CHECKED_ATTR, false);

        //Delete the status set and save the status
        var divStatusImage = $(this).prev('#divStatusImage');
        //$(divStatusImage).removeClass().addClass(USER_SELECTED_IMAGE_CLASS);
        SetQuestionImageAndTopink(userResponse, divStatusImage, false, null, true);

    });

}


//Check if the rct node is in unselected mode and any of its child level RCT is in selected/unseleceted mode
function IsAnyChildSelected(divStatusImage, isFromRadioButton, radioButton, userResponse) {

    var isAnyChildSelected = false;
    //var isCurrentUnSelected = false;
    var isAllNoSelected = false;

    if (isFromRadioButton) {

        var questionContent = $(radioButton).parents().eq(1);
        isAllNoSelected = (questionContent.find("input:radio[id^='rbNo']:checked").length == questionContent.find("input:radio[id^='rbNo']").length);
    }

    ////If user has clicked the square box and the square box has green check class then its in unselected state, 
    ////or else if user clicked the radio buttons and  all NO radio buttons are checked, then its in unselected state
    ////isCurrentUnSelected = (!isFromRadioButton) ? $(divStatusImage).hasClass(USER_SELECTED_IMAGE_CLASS) : isAllNoSelected;

    ////If current is unselected
    ////if (isCurrentUnSelected) {

    //    //Get the child divs
        var childDivs = $(divStatusImage).next().children().eq(1).find('.parent');

    //    //For each child div, get the square div checked state
    //    $(childDivs).each(function () {

    //        //If the square div status is either selected/ unselected, return type
          
    //        isAnyChildSelected = (userResponse == USER_RESPONSE_NO) 
    //                             ? ($(this).prev('#divStatusImage').hasClass(USER_SELECTED_IMAGE_CLASS))
    //                             :(($(this).prev('#divStatusImage').hasClass(USER_SELECTED_IMAGE_CLASS)) || ($(this).prev('#divStatusImage').hasClass(USER_UNSELECTED_IMAGE_CLASS)))

    //        return isAnyChildSelected;

    //    });
    ////}

        isAnyChildSelected = $(childDivs).prev('#divStatusImage').hasClass(USER_SELECTED_IMAGE_CLASS);

        return isAnyChildSelected;

}


//Check if any of the  higher level node is unselected, alert the user
function CheckIfHigherItemEliminated(divStatusImage) {

    var isEliminated = false;

    if ($('#divStatusImageParent').hasClass(USER_UNSELECTED_IMAGE_CLASS)) {
        isEliminated = true;
        return isEliminated;
    }

    var parentDivs = divStatusImage.parentsUntil('ul#divAccordion.ui-tabs-nav');

    $(parentDivs).each(function () {

        if ($(this).hasClass('parent')) {
            if ($(this).prev('#divStatusImage').hasClass(USER_UNSELECTED_IMAGE_CLASS)) {
                isEliminated = true;
                return isEliminated;
            }
        }

    });

    return isEliminated;
}


//If any node is selected , also selected its all parent RCT nodes and save the status
function CheckHigherLevelNodes(titleHeader) {

    var divStatusImage = $(titleHeader).parent().prev();

    //Get the parent divs
    var parentDivs = $(titleHeader).parentsUntil('ul#divAccordion.ui-tabs-nav');

    $(parentDivs).each(function () {

        if ($(this).hasClass('parent')) {

            //Iterate each status image div and save the status
            $(this).prev('#divStatusImage').each(function () {
                SetQuestionImageAndTopink(USER_RESPONSE_YES, this, false, null, false);
            });
        }
    });

}


//Highlight related top links/tab , if user response is yes, set background as yellow and if user response is false set background as red
function HighLightTopLink(rctNodeID, userResponse) {

    //if user response is yes, get class with green background and yellow font. If user response is false get class with pink background and red font
    var topLinkHeaderClass = (userResponse == USER_RESPONSE_YES) ? USER_SELECTED_LINK_CLASS :
                                (userResponse == USER_RESPONSE_NO) ? USER_UNSELECTED_LINK_CLASS : USER_RESPONSE_NULL;
        
    //Set the class for the top main links
    jQuery("[name*=" + rctNodeID + "-]").removeClass();


    //Remove the class with green background and yellow font, add class so that user can identify the tab is answered by the user
    jQuery('.focused').removeClass(USER_SELECTED_LINK_CLASS); 

}


function TopNodeIsUnSelected() {
    return $('#divAccordion').find('#divStatusImage').hasClass(USER_UNSELECTED_IMAGE_CLASS);
}



// Set image for square placeholder based on user answers to questions and apply the style to related top link
function SetQuestionImageAndTopink(userResponse, divStatusImage, isFromRadioButton, radioButton, deletedAllChildNode) {
    
    var newStatusImageClass = (userResponse == USER_RESPONSE_YES) ? USER_SELECTED_IMAGE_CLASS : (userResponse == USER_RESPONSE_NO) ? USER_UNSELECTED_IMAGE_CLASS : '';
    var titleHeader = GetTitleHeader(divStatusImage, isFromRadioButton, radioButton)//$(divStatusImage).next('div').children('#divRCTTitle');

    resultedArray = GetTabArray(titleHeader, userResponse);

    // Hide or show the tabs based on the rct node selected and store the data   
     removeTab = HideOrDisplayTabs(resultedArray, radioButton);
    

    if (removeTab) {

        if (!isFromRadioButton) {
            SetRadioButtons(titleHeader, newStatusImageClass);
        }

        //Set question image status class and top link class
        SetQuestionImageAndTopinkClass(userResponse, newStatusImageClass, titleHeader, deletedAllChildNode);
    }

    //Add styles to let user that the user has answered the questions related to the current tab
    if (TopNodeIsUnSelected()) {
        jQuery('.focused').css({
            'background-color': '#E32441 !important',
            'color': '#FFBBBB !important'
        });
    }
    else {
        jQuery('.focused').css({
            'background-color': '#52C55A !important',
            'color': '#FFFFCC !important'
        });
    }

}


// Set radio button status
function SetRadioButtons(titleHeader, newStatusImageClass) {

    var questionContent = titleHeader.next().children().eq(0);

    if (newStatusImageClass == USER_RESPONSE_NULL) {

        questionContent.find("input:radio[id^='rbNo']").prop('checked', false);
        questionContent.find("input:radio[id^='rbYes']").prop('checked', false);

    }
    else if (newStatusImageClass == USER_UNSELECTED_IMAGE_CLASS) {

        questionContent.find("input:radio[id^='rbNo']").prop('checked', true);
        questionContent.find("input:radio[id^='rbYes']").prop('checked', false);

    }

}


//Set question image status class and top link class
function SetQuestionImageAndTopinkClass(userResponse, newStatusImageClass, titleHeader, deletedAllChildNode) {
    

    //Get the related RCT id    
    var rctNodeID = GetCorrespondingRCTNodeID(titleHeader);

    if (userResponse == USER_RESPONSE_YES) {
                
        jQuery("[name*=" + rctNodeID + "-]").parent().fadeIn(1000);

        var isVisualRCTPage = IsVisualRCTPage();

        if (isVisualRCTPage) {
          
            var causalFactorID = GetCausalFactorID();

          //  if ((tabStatus[i].RootCauseCategoryID == 4) || (tabStatus[i].Title == 'Equipment Difficulty')) {

                var sections = jQuery("[name*=" + rctNodeID + "-]");

                for (j = 0 ; j <= sections.length - 1; j++) {

                    var id = $(sections[j]).attr('id');
                    var sectionHeader = $(sections[j]).parent();

                    RenderRCTSections(id, sectionHeader, causalFactorID);
                    
                }
            //  }

        }
    }

    //Update the class of square div
    $(titleHeader).parent().prev().removeClass().addClass(newStatusImageClass);

    //High light top link
    if (!isVisualRCTPage) {        
        HighLightTopLink(rctNodeID, userResponse);
    }

    //Save status
    if (deletedAllChildNode == false) {
        SaveRCTQuestionNode(rctNodeID, userResponse, false);
    }

}


//Get associated tab array  for a rct node modified recently
function GetTabArray(titleHeader, userResponse) {

    var myArray = new Array();
    var resultedArray = new Array();

    if (userResponse != USER_RESPONSE_YES) {

        var rctNodeID = GetCorrespondingRCTNodeID(titleHeader);

        //Get all the tabs whose title related to the rctNodeID
        myArray = jQuery("[name*=" + rctNodeID + "-]");
        resultedArray = jQuery("[name*=" + rctNodeID + "-]");

        //Iterate all the tab ids and get the title, parse by ',' and then for each parsed text find out the square box that is green checked and remove that from hiding list
        var i = 0;

        for (i = 0; i <= myArray.length - 1; i++) {

            //Get the title of the tab id
            var title = $(myArray[i]).attr('name');

            //Parse by ','
            var titleArray = title.split(',');

            //iterate each parsed text and find out the square box
            for (j = 0; j <= titleArray.length - 1; j++) {

                var itemValue = titleArray[j].split('-')[0];

                //If the square box is green checked, remove that from the hiding list
                if ($('#divAccordion').find('#hdn' + itemValue).parent().parent().prev().hasClass('greenCheck') &&
                                            $('#divAccordion').find('#hdn' + itemValue).attr('id') != 'hdn' + rctNodeID) {

                    RemoveTabIdFromArray(resultedArray, myArray[i]);

                }
            }

        }
    }

    return resultedArray;

}


//Hide or show the tabs based on the rct node selected and store the data
function HideOrDisplayTabs(resultedArray, radioButton) {
   
    var removeTab = true;
    var message = USER_MESSAGE;
    _isVisualRCTPage = IsVisualRCTPage();

    //Iterate the hiding tab array list and update the message
    for (i = 0; i <= resultedArray.length - 1; i++) {

        //If 15 question tab is green marked/ iterated
        var tabStyle = jQuery(resultedArray[i]).attr('style');

        var isTabChecked = (_isVisualRCTPage) ? $(resultedArray[i]).hasClass('selectedBasicNode')
                                               : ((tabStyle == TAB_ITERATED_STYLE) || (tabStyle == TAB_ITERATED_STYLE_IE));

        if (isTabChecked) {
            message = message + $(resultedArray[i]).html() + ', ';
        }

    }

    //Check if message is updated i.e there is tab to hide
    if (message.length != USER_MESSAGE.length) {

        // Add texts to message
        message = message.substr(0, message.length - 2);
        message = message + '  tab. Are you sure you want continue?';

        //Confirm from user
        removeTab = confirm(message);
        
    }

    if (removeTab) {

        // Hide and delete tab data
        HideOrDeleteTabData(resultedArray);

    }
    else {            
        PreventDefault(radioButton);
    }

    return removeTab;

}


// Hide and delete tab data
function HideOrDeleteTabData(resultedArray) {

    var nodeIdList = '';
    var nodeId = '';


    //Remove tab style and get the rct node id lists
    for (i = 0; i <= resultedArray.length - 1; i++) {

        nodeId = $(resultedArray[i]).attr('id');
        nodeIdList = nodeIdList + ',' + nodeId.substr(2, nodeId.length - 1);

        if(_isVisualRCTPage){
            jQuery(resultedArray[i]).parent().find('canvas').remove();
            jQuery(resultedArray[i]).parent().find('div').remove();
        }

        jQuery(resultedArray[i]).parent().fadeOut(1000);
        if (!_isVisualRCTPage) {
            jQuery(resultedArray[i]).removeAttr('style');
        }
    }
    
    //Delete all node ids
    if (nodeIdList != "") {
        DeleteTabData(nodeIdList);
    }

}



// Delete all node ids
function DeleteTabData(ids) {

    var tabData = {};

    //Get causal factor id
    tabData.tabIds = ids;
    tabData.causalFactorID = GetCausalFactorID();
    tabData = JSON.stringify(tabData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/DeleteTabData';
    AjaxPost(serviceURL, tabData, eval(DeleteTabDataSuccess), eval(FailedCallback));

}


//On success of tab delete
function DeleteTabDataSuccess() {
}


// Set all question response to NO
function SetAllQuestionToNo(sender, event) {

    // Get the status image div
    var divStatusImage = $(sender).next().next();

    //Set all questions answer to No
    SetEffects(divStatusImage, false, null, null, true, false, false);
}

//function SetEffects(divStatusImage, isFromRadioButton, radioButton, radioButtonevent, setAllNo, setAllBlank) {

function SelectNode(sender, event) {

    
    // Get the status image div
    var divStatusImage = $(sender).next();

    //Set all questions answer to No
    SetEffects(divStatusImage, false, null, null, false, false, true);

}


function EraseStatus(sender) {

    // Get the status image div
    var divStatusImage = $(sender).next().next().next();

    //Set all questions answer to No
    SetEffects(divStatusImage, false, null, null, false, true, false);
}


//Remove tab id from tab array
function RemoveTabIdFromArray(array, item) {

    for (var i in array) {

        if (array[i] == item) {

            array.splice(i, 1);
            break;

        }

    }

}


// Get rct node status div 
function GetRCTNodeStatus(divStatusImage, isFromRadioButton, radioButton) {

    var userResponse = '';
    var questionContent = '';

    //Get the parent div
    if (isFromRadioButton) {
        questionContent = $(radioButton).parents().eq(1);
    }
    else {
        questionContent = $(divStatusImage).parent().find('#divParent').find('#divRCTQuestions').find('#divRadioButtons');
    }

    //Get the user response (yes/no)  - if any question is Yes, the node is green check mark, - if no Yes and not all questions have been answered, it is blank (not set)
    //- if no Yes and all questions are No, then set to red X
    var userResponse = (questionContent.find("input:radio[id^='rbYes']:checked").length > 0) ? USER_RESPONSE_YES
                            : (questionContent.find("input:radio[id^='rbNo']:checked").length == questionContent.find("input:radio[id^='rbNo']").length) ? USER_RESPONSE_NO : '';

    return userResponse;

}