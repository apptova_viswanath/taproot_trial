﻿var NEAR_RC_TYPE = '1';




// Ready function
$(document).ready(function () {


    //Save a blank statement for the generic cause and close the dialog
    $('#btnGenericCauseClose').live("click", function (event) {        
        SaveBlankStatementAndClose(this);
    });


    //Close the dialog
    $('#btnEditGenericCauseClose').live("click", function (event) {
       CloseDialog(this, 'txtEditGenericCause');
    });


    //Save the generic cause
    $('#btnSaveGenericCause').live("click", function (event) {  
        UpdateGenericCauseStatament(this, 'txtGenericCause', false);
    });


    //Save the generic cause
    $('#btnEditGenericCauseSave').live("click", function (event) {
        UpdateGenericCauseStatament(this, 'txtEditGenericCause', true);
    });


    //Display generic cause questions, on click of generic cause top link
    $('#lnkGenericCause').live("click", function (event) {
        SetFocusOnCauses();
        DisplayGenericCauseQuestions();
    });


    //Save user response
    $('#divGenericCausesContent').find('input[type=radio]').live("change", function (event) {
         SaveResponse(this);
    });


    //Save user response
    $('#divGenericStatusImage').live("click", function (event) {
        SaveResponse(this);
    });

    
    //// Display edit generic statement dialog
    //$('#btnEditGenericCause').live("click", function (event) {
    //    DisplayEditStatement(this);
    //});


    ////Delete generic cause 
    //$('#btnDeleteGenericCause').live("click", function (event) {
    //    DeleteGenericCauseByID(this, event);
    //});


    //Set question text as per user response
    $('#divGenericCausesContent').find('txtQuestion1Response').live("change", function (event) {
        SetQuestionText(this);
    });
   

    //$('#lnkPerformAnalysis, #lnkEditAnalysis').live("click", function (event) {
    //    //    $('#lnkPerformAnalysis, #lnkEditAnalysis').unbind('click').bind('click', function (e) {

    //    $('#lnkPerformAnalysis, #lnkEditAnalysis').unbind("click");
    //    alert('event firing');
    //    DisplayOrHideContent(this);
    //    return false;
    //});

    
});

function SetFocusOnCauses()
{
    $("#DivloadingImage").show();
    //Display generic cause as the focused tab
    $('.focused').next('.arrow-down').hide();
    $('.focused').removeClass('focused');
    $('#lnkGenericCause').addClass('focused');
    $('.focused').next('.arrow-down').show();
}

// Display or hide the content
function DisplayOrHideContent(sender) {

    var panel = $(sender).parent().next().next().next().next();    
    var isOpen = panel.is(':visible');

    // open or close as necessary, trigger the correct custom event
    panel[isOpen ? 'slideUp' : 'slideDown']()        
        .trigger(isOpen ? 'hide' : 'show');

    // stop the link from causing a page scroll
    return false;

}


//Save the generic cause
function UpdateGenericCauseStatament(sender, txtDescription, isUpdateMode) {
    
    //Check if entered value is valid
    if (IsReStateValid(sender, txtDescription, 'Please specify description.', $(sender).parentsUntil('.ui-dialog'))) {

        if (isUpdateMode) {

            //Update generic cause
            UpdateGenericCause(sender);

        }
        else {

            //Save generic cause along with user response for different questions
            SaveGenericCauseSatement(sender, $('#hdnRCTIDGeneric').val(), true, $('#hdnQuestion2Response').val(), $('#hdnQuestion3Response').val());

        }

        //Close the dialog
        CloseDialog(sender, txtDescription);

    }

}


// Save blank generic cause statement and close teh dialog
function SaveBlankStatementAndClose(sender) {

    var dialog = $(sender).parentsUntil('.ui-dialog');//$(sender).parents().eq(1);
   // if ($(dialog).attr('id') == 'divGenericRestate') {
        SaveGenericCauseBlankStatement(dialog);
   // }

    // Close the dialog and remove validation error from txtGenericCause if any.
    CloseDialog(sender, 'txtGenericCause');

}


// Close the dialog
function CloseDialog(sender, txtDescription) {   
   
    //var dialog = $(sender).parents().eq(1);
    //var descriptionField = $(sender).parents().eq(1).find('#' + txtDescription);
    //ShowHideValidationMessage(descriptionField, '', false);
    //$(dialog).dialog("close");

    
    var descriptionField = $('#' + txtDescription);
    var dialog = $(sender).closest('.ui-dialog-content');
    ShowHideValidationMessage(descriptionField, '', false);
    $(dialog).dialog("close");
}


// Check if the statement is valid
function IsReStateValid(sender, txtDescription, message, parent) {
    
    var descField = parent.find('#' + txtDescription);
    var description = $(descField).val();

    if (description == '' || description == undefined) {

        NotificationMessage(message, 'jError', true, 3000);
        ShowHideValidationMessage(descField, message, true);
        return false;

    }
    //else {

    //    ShowHideValidationMessage(descField, message, false);
    //}

    return true;
}


//Show or hide validation message
function ShowHideValidationMessage(fieldId, imageTitle, isValid) {

    if (isValid) {

        $(fieldId).addClass('bordered');

        var nextImage = $(fieldId).next('img');
        nextImage.remove();

        var imageSrc = _baseURL + 'Images/warning.png';
        $(fieldId).after('<img src="' + imageSrc + '" title="' + imageTitle + '" class="warningImage" />');

        if (fieldId == 'lnkEventLocation' || fieldId == 'lnkEventClassification' || fieldId == 'lblCAP') {
            $(fieldId).removeClass('bordered');
        }

    }
    else {

        var nextImage = $(fieldId).next('img');

        $(fieldId).removeClass('bordered');
        nextImage.remove();

    }
}


// Update generic cause
function UpdateGenericCause(sender) {
    
    var genericData = {};

    var parent = $(sender).parentsUntil('.ui-dialog');
    //Get parameters
    genericData.genericCauseId = parent.find('#hdnEditGenericCauseID').val();;
    genericData.reState = parent.find('#txtEditGenericCause').val();
    genericData = JSON.stringify(genericData);

    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/UpdateGenericCause';
    AjaxPost(serviceURL, genericData, eval(GetGenericCausesStatements), eval(FailedCallback));
       
}


//Save user response for questions at generic cause tab
function SaveResponse(sender) {
     
    var senderId = $(sender).attr('id');
    var divGenericCause = $(sender).closest("div#divGenericCause");
    var divGenericQuestionsContent = $(divGenericCause).find('#divGenericQuestions').find('#divGenericQuestionsContent');
       

    var rctID = $($(divGenericCause).children()[2]).val();//$(divGenericCause).find('#hdnRCTID').val();
    var rctTitle = $(divGenericCause).find('#hdntitle').val(); //to support multi language getting it from hidden field and not from span 

    if (senderId == 'rbQuestion2Yes') {
        $(sender).parent().next().show();
    }
    else if (senderId == 'rbQuestion2No') {

        $(sender).parent().next().hide();
        $(divGenericCause).find('#rbQuestion3Yes').attr('checked', false);
        $(divGenericCause).find('#rbQuestion3No').attr('checked', false);

    }

    var userResponse = (divGenericQuestionsContent.find("input:radio[id*='Yes']:checked").length == 2)
                        ? USER_RESPONSE_YES
                        : (divGenericQuestionsContent.find("input:radio[id*='No']:checked").length == divGenericQuestionsContent.find("input:radio[id*='No']").length)
                        ? USER_RESPONSE_NO : '';
        
    var question2Response = ($(divGenericCause).find('#rbQuestion2Yes').prop('checked')) ? true
                            : ($(divGenericCause).find('#rbQuestion2No').prop('checked')) ? false : null;

    var question3Response = ($(divGenericCause).find('#rbQuestion3Yes').prop('checked')) ? true
                                : ($(divGenericCause).find('#rbQuestion3No').prop('checked')) ? false : null;

    var isGeneric = (userResponse == USER_RESPONSE_YES) ? true : (userResponse == USER_RESPONSE_NO) ? false : null;
             
    var question1Response = $(divGenericCause).find('#txtQuestion1Response').val();

    SaveGenericCause(sender, rctID, isGeneric, question2Response, question3Response, question1Response);

    if (($(sender).val() == 'Yes') && (divGenericQuestionsContent.find("input:radio[id*='Yes']:checked").length == 2)) {
        
        $('#hdnQuestion2Response').val(question2Response);
        $('#hdnQuestion3Response').val(question3Response);
        $('#hdnRCTIDGeneric').val(rctID);        
        $('#spnRCTTitle').html(rctTitle);
        $('#hdnEditGenericRCTTitle').val(rctTitle); //to support multi language
        

        var titleText = divGenericCause.find('#txtQuestion1Response').val(); //divGenericCause.find('.title').html();
       
        OpenRestateDiv(sender, titleText);
        
    }

    var parent = $(sender).parentsUntil('.ui-dialog');
      
    var divQuestion2 = parent.find('#divQuestion2');
    var divQuestion3 = parent.find('#divQuestion3');

    var question2Responded = $(divQuestion2).find("input:radio[id^='rbQuestion2']:checked").length > 0;
    var question3Responded = $(divQuestion3).find("input:radio[id^='rbQuestion3']:checked").length > 0;

    if (question2Responded && question3Responded) {

        $(sender).parents().eq(3).find('#lnkEditAnalysis').show();
        $(sender).parents().eq(3).find('#lnkPerformAnalysis').hide();

    }
    else {

        $(sender).parents().eq(3).find('#lnkEditAnalysis').hide();
        $(sender).parents().eq(3).find('#lnkPerformAnalysis').show();

    }
}


//Save generic cause statement
function SaveGenericCauseSatement(sender, rctID) {

    var genericData = {};

    genericData.causalFactorId = GetCausalFactorID();
    genericData.rootCauseTreeId = rctID;

    if($(sender).attr('id') == 'btnSaveGenericCause')
    {
        genericData.statement = $(sender).parentsUntil('.ui-dialog').find('#txtGenericCause').val();
    }
    else{
    
        var question1Text =  $(sender).find('#spnRestateCause').html();
        var title = $(sender).find('#hdnEditGenericRCTTitle').val();
        genericData.statement = 'Generic Cause for ' + title + ' is not identified.';
    }

    genericData = JSON.stringify(genericData);

    $('#txtGenericCause').val('');

    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/SaveGenericCauseSatement';

    AjaxPost(serviceURL, genericData, eval(OnSuccessSaveGenericCauseSatement), eval(FailedCallback));

}


//On success of SaveGenericCauseSatement
function OnSuccessSaveGenericCauseSatement() {
    GetGenericCausesStatements();
}


//Save the generic cause
function SaveGenericCause(sender, rctID, isGeneric, question2Response, question3Response, question1Response) {

    //Create the parameter object
    var genericData = {};
    var fromCreateStatement = ($(sender).attr('id') == 'btnSaveGenericCause') ? true : false;

    genericData.causalFactorId = GetCausalFactorID();
    genericData.rootCauseTreeId = rctID;   
    genericData.question2Response = fromCreateStatement ? eval($('#hdnQuestion2Response').val()) : question2Response;
    genericData.question3Response = fromCreateStatement ? eval($('#hdnQuestion3Response').val()) : question3Response;
    genericData.question1Response = fromCreateStatement ? eval($('#hdnQuestion3Response').val()) : question1Response;
    genericData.isGeneric = isGeneric;

    genericData.question2Response = ((genericData.question2Response) == undefined) ? null : genericData.question2Response;
    genericData.question3Response = ((genericData.question3Response) == undefined) ? null : genericData.question3Response;
    
    genericData = JSON.stringify(genericData);

    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/SaveGenericCause';
    AjaxPost(serviceURL, genericData, eval(GetGenericCausesStatements), eval(FailedCallback));

}


//Delete generic cause identified, update corresponding columns in RootCuaseTree
function DeleteGenericCauseByID(sender, event) {
    
    var genericCauseID = $(sender).next().find('#hdnEditGenericCauseID').val();
    var userConfirm = confirm('This will remove this generic cause, Do you want to continue?');

    if (userConfirm) {

        var genericData = {};

        genericData.genericCauseID = genericCauseID;
        genericData = JSON.stringify(genericData);

        //Call the service
        var serviceURL = _baseURL + 'Services/GenericCause.svc/DeleteGenericCauseByID';
        AjaxPost(serviceURL, genericData, eval(GetGenericCausesStatements), eval(FailedCallback));

    }
}


//Display generic cause content with root cause identified and questions
function DisplayGenericCauseQuestions() {
  

    //Create service parameter object
    var rctData = {};

    rctData.causalFactorID = GetCausalFactorID();
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/GetGenericCauseQuestions';
    AjaxPost(serviceURL, rctData, eval(GetGenericCauseQuestionsSuccess), eval(FailedCallback));

}


// Set accordion
function SetAccord() {

    $('.ui-accordion-header').removeClass('ui-corner-all').addClass('ui-accordion-header-active ui-state-active ui-corner-top').attr({ 'aria-selected': 'true', 'tabindex': '0' });
    $('.ui-accordion-header .ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
    $('.ui-accordion-content').addClass('ui-accordion-content-active').attr({ 'aria-expanded': 'true', 'aria-hidden': 'false' }).show();

    $(this).hide();
    $('.close').show();

}


// Save procedure entered by user
function SaveProcedure(sender) {
    
    var divGenericCause = $(sender).closest("div#divGenericCause");
    var divGenericQuestionsContent = $(divGenericCause).find('#divGenericQuestions').find('#divGenericQuestionsContent');
    var rctID = $(divGenericCause).find('#hdnRCTID').val();

    var genericData = {};
    genericData.rctID = rctID;
    genericData.question1Response = $(sender).val();
    genericData = JSON.stringify(genericData);
    
    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/SaveProcedure';
    AjaxPost(serviceURL, genericData, eval(SaveProcedureSuccess), eval(FailedCallback));

    if ($(sender).val() == '') {
        $(divGenericCause).children().first().find('#lnkEditAnalysis').hide();
        $(divGenericCause).children().first().find('#lnkPerformAnalysis').show();
    }
   
}


// On success of Save Procedure
function SaveProcedureSuccess()
{  
}


// Set question texts based on user response
function SetQuestionText(sender) {

    var typedText = $(sender).val();
    var divQuestion2 = $(sender).next();
    var divQuestion3 = $(sender).next().next();
    var lblQuestion2 = $(sender).next().find('#lblQuestion2');
    var lblQuestion3 = $(sender).next().next().find('#lblQuestion3');

    if (typedText != '') {

        $(divQuestion2).show();
        $(lblQuestion2).show();
        $(lblQuestion2).html(typedText);

        $(lblQuestion3).show();
        $(lblQuestion3).html(typedText);

    }
    else {

        $(lblQuestion2).html('');
        $(lblQuestion2).hide();

        $(lblQuestion3).html('');
        $(lblQuestion3).hide();
               
        $(divQuestion2).find("input:radio[id^='rbQuestion2']").attr('checked', false);
        $(divQuestion3).find("input:radio[id^='rbQuestion3']").attr('checked', false);

        $(divQuestion2).hide();
        $(divQuestion3).hide();               

    }
    
    SaveProcedure(sender);
}


//On success of GetQuestions
function GetGenericCauseQuestionsSuccess(data) {

    if (!(IsVisualRCTPage())) {
        //Hide the normal root cause question section and display the generic cause content
        $('#questions').hide();
    }

    $('#divGenericCausesContent').show();
    $("#DivloadingImage").hide();

    //make template empty
    $("#divGenericCauseAccordion").empty();

    //parse the json result to object
    data = ParseToJSON(data.d);
    
    //set the data returned to the template
    $("#genericTemplate").tmpl(data).appendTo("#divGenericCauseAccordion");

    //Design the accordion
    $("#divGenericCauseAccordion").accordion();
    $("#divGenericCauseAccordion").accordion('destroy').accordion({

        header: " div > ul", autoHeight: false, collapsible: true,active: false,

        beforeActivate: function (event, ui) {
            SetAsMultipleDivExpandableAccordion(ui);
            return false; // Cancels the default action
        }

    });
   
  
    //Hide the expandable icon
    $('.ui-accordion .ui-accordion-header .ui-icon').hide();

    var header = $("#divGenericCauseAccordion").find('.ui-accordion-header');
    $(header).each(function () {
        $(header).unbind("click");
        $(header).unbind("hover");
    });
    

    // Set question response
    SetQuestionResponse()

    // Set edit link and perform analysis link
    SetEditAndPerformLink();

    // Set status image 
    SetStatusImage();

    //Get all generic cause statements
    GetGenericCausesStatements();

}


// Set question response
function SetQuestionResponse() {

    //Set Yes No radio button response status doen by user
    $('.question2,.question3').each(function () {
        if ($(this).val() == "") {
            $(this).next().attr('checked', false);
            $(this).next().next().attr('checked', false);

            $(this).parent().next().hide();
        }
        else if ($(this).val() == "true") {
            $(this).next().attr('checked', true);
            $(this).next().next().attr('checked', false);

            $(this).parent().next().show();
        }
        else if ($(this).val() == "false") {
            $(this).next().attr('checked', false);
            $(this).next().next().attr('checked', true);

            $(this).parent().next().hide();
        }

    });
}


// Set edit link and perform analysis link
function SetEditAndPerformLink() {
    
    $('.gcLinks').each(function () {

        var rcType = $(this).prev().val();
        if (rcType == NEAR_RC_TYPE)
        {
            $(this).hide();
        }
    });

    $('.textAreaQuestion1Response').each(function () {


        var divQuestion2 = $(this).next();
        var divQuestion3 = $(this).next().next();

        if ($(this).val() != "") {
            $(divQuestion2).show();
        }
        else {
            $(divQuestion2).hide();
        }


        var question2Responded = $(divQuestion2).find("input:radio[id^='rbQuestion2']:checked").length > 0;
        var question3Responded = $(divQuestion3).find("input:radio[id^='rbQuestion3']:checked").length > 0;

        if (question2Responded && question3Responded) {
            $(this).parents().eq(2).find('#lnkEditAnalysis').show();
            $(this).parents().eq(2).find('#lnkPerformAnalysis').hide();
        }
        else {
            $(this).parents().eq(2).find('#lnkEditAnalysis').hide();
            $(this).parents().eq(2).find('#lnkPerformAnalysis').show();
        }
    });

}


//Get all generic cause statements added by user
function GetGenericCausesStatements() {
    
    //Declare the parameter object
    var rctData = {};

    rctData.causalFactorID = GetCausalFactorID();
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/GenericCause.svc/GetGenericCausesStatements';
    AjaxPost(serviceURL, rctData, eval(GetGenericCausesStatementsSuccess), eval(FailedCallback));

}


//On success of get generic cause statements
function GetGenericCausesStatementsSuccess(data) {

    //make template empty
    $("#divGenericCauseStatements").empty();

    //parse the json result to object
    data = ParseToJSON(data.d);

    if (data.length > 0) {

        $("#lblNone").hide();

        //set the data returned to the template
        $("#GenericCauseStatementsTemplate").tmpl(data).appendTo("#divGenericCauseStatements");

    }
    else {
        $("#lblNone").show();
    }

   // Refresh parent
    RefreshParent();

}


// Open restate div pop up
function OpenRestateDiv(sender, headerText) {
    
    $('#spnRestateCause').html(headerText);
    OpenPopup('#divGenericRestate', 'Create a Generic Cause');
  
}


// Open pop up
function OpenPopup(dialogDiv, titleText) {
    
    var divHeight = 310;
    var divWidth = 503;
    //if ($(dialogDiv).attr('id') == 'divEditGenericCause')
    //{        
        divHeight = 320;
        divWidth = 510;
    //}

    $(dialogDiv).dialog({

        height: divHeight,
        width: divWidth,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 100],
        title: titleText

    });


    //set the title class, as it conflicts with the jquery accordion title class
    var title = $(dialogDiv).prev();
    $(title).removeClass('ui-widget-header');
    $(title).addClass('popupTitle');

    // Hide close icon
    if ($(dialogDiv).attr('id') == 'divGenericRestate') {        
        $('.ui-dialog-titlebar-close').hide();
    }


    // Set close event
    $(dialogDiv).bind('dialogclose', function (event) {

        if ($(dialogDiv).attr('id') == 'divanalysisComments') {
            var descriptionField = $(dialogDiv).find('#txtAnalysisComments');
            ShowHideValidationMessage(descriptionField, '', false);
        }


        $(dialogDiv).hide();
        $("#dialog:ui-dialog").dialog("destroy");
        $(dialogDiv).dialog("destroy");

    });

}


// Save generic cause blank statement
function SaveGenericCauseBlankStatement(dialogDiv) {
   
    var rctID = $(dialogDiv).find('#hdnRCTIDGeneric').val();
    SaveGenericCauseSatement(dialogDiv, rctID);

}


// Display all generic cause staements
function DisplayEditStatement(sender) {
    
    $('#spnRestateCause').html('');
    $(sender).next().next().find('#txtEditGenericCause').val($(sender).prev().val());
    var dialog = $(sender).next().next();
    OpenPopup(dialog, 'Edit Generic Cause');

}