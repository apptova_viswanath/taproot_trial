﻿//*Event Page Methods  (usha) *//

//Global Variable

var _isClassificationExist = '';
var _serviceUrl = '';
var _locationData = '';
var _locationCheckId = '';
var _classificationData = '';
var _eventId = -1;
var _data = '';
var _eventType = '';
var _locationId = '';

var _classificationCount = 0;
var _fields = new Object();
var _classificationId = '';

_classificationIdsArray = new Array();
_locationCheckedItemsArray = new Array();
_locationCheckedNamesArray = new Array();

var _classificationFieldId = '';
var _classificationFieldCount = '';


_classificationCheckedItemsArray = new Array();
_classificationCheckedNamesArray = new Array();
var _hdnLocationId = '';
var _hdnClassificationId = '';
var _fieldValue = '';
var _undoStack = new Array;

var _message = '';
var _messageType = '';
var _companyId = '';
var _userId = '';
var _svcURL = '';
var _eventDuplicate = false;

//ready Method


//Need optimization to handel on sublinks settings
function SetLinks(tabLinks, extraWidth, isMainTab, animateClass) {

    var marginLeft = 0;
    var index = 0
    var separatorWidth = 1;
    var width = 0;
    var lineCount = 1;
    var margin = 40;

    //Need optimization
    $(tabLinks).each(function () {

        marginLeft = 0;

        if (isMainTab) {
            $(this).css("margin-top", '6px');
        }

        width = (isMainTab) ? $(this).width() : $(this).width() + 5;

        $(this).css("width", width + "px");
        $(this).addClass(animateClass);

        //if first line, then get all previous, otherwise get the previous upto li with margin-left: - 8px [first element of that line]
        if (lineCount == 1) {
            var prevAll = $(this).prevAll();
            $(prevAll).each(function () {
                marginLeft = marginLeft + $(this).width() + separatorWidth;
            });
        }
        else {
          
            var prevAll = $(this).prevAll();
            $(prevAll).each(function () {

                if ($(this).css("margin-left") == '-8px') {
                    marginLeft = marginLeft + $(this).width() + separatorWidth;
                    return false;
                }
                marginLeft = marginLeft + $(this).width() + separatorWidth;
            });

            //Set the margin top based on current line number
            $(this).css("margin-top", margin * (lineCount - 1) + "px");
        }

        //Get the margin left for the li
        marginLeft = (!isMainTab) ? marginLeft - 8 : (index == 0) ? marginLeft + 9 : marginLeft + 13;

        //If element width exceeding the width of teh parent div, break it to second line
        if ($('#ulSubTabs').width() < marginLeft + width) {

            lineCount = lineCount + 1;
            marginLeft = -8;

            $(this).css("margin-top", margin * (lineCount - 1) + "px");
            $('#ulSubTabs').css("margin-bottom", lineCount * margin + "px");

        }

        //Set the properties
        $(this).css("left", marginLeft + "px");
        $(this).css("margin-left", marginLeft + "px");
        $(this).css("word-break", 'none');

        index = index + 1;

    });

}


function IfDivFilledUp(marginLeft, width)
{

    

}


function AnimateTab(tabLinks, animateTime) {
   
    $(tabLinks).each(function () {
       
        var left = $(this).offset().left;
        animateTime = animateTime + 250;
      
        $(this).css({ left: -1000}).show().animate({
            left: left
        }, {
            duration: animateTime,
            specialEasing: {
                width: "linear",
                height: "easeOutBounce",
                right: "easeOutBack"
            },
            complete: function () {

            }
        });    

    });

}


function DefineAnimation()
{

    var animateTime = 1250;

    //need to set this at cs code while creating the li
    var mainTabLinks = $("#body_ucevent_divMainTabs li").slice(1);
    $(mainTabLinks).each(function () {
        $(this).hide();

    });

    var subTabLinks = $("#ulSubTabs ul li").slice(1);
    $(subTabLinks).each(function () {
        $(this).addClass('animateSubTab');
        $(this).hide();
    });

    $(window).load(function () {

        SetLinks(subTabLinks, 0, false, 'animateSubTab');

        AnimateTab(mainTabLinks, animateTime);
        AnimateTab(subTabLinks, animateTime);

    })

}


$(document).ready(function () {
  
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');

    var actionType = splitUrl[splitUrl.length - 1].split('?');
    
    if (actionType.length = 2 && actionType[0] == 'Edit' && actionType[1] == '1') {
        DefineAnimation();
    }    
  
    _isEventData = true;

    //page load
    SetPageLoad();

    //save Event
    $('#btnEventSave').click(function () {
        
        SaveEvent();
        return false;
    });

    //open Location popup
    $('#lnkEventLocation').click(function () {

        OpenPopup(1);
    });

    //open classification popup
    $('#lnkEventClassification').click(function () {

        OpenPopup(2);
    });

    //populate classification details
    $('#btnSelectEventClassification').click(function () {

        PopulateClassificationData();
        return false;
    });

    //populate location Details
    $('#btnSelectEventLocation').click(function () {

        PopulateLocationData();
        return false;
    });

    //for Clear Fields
    $('#btnCancel').click(function () {
        ClearFields();
        var url = _baseURL + 'Home';
        location.href = url;
        return false;
    });

    //close modal popup
    $('#btnEventClassificationCancel').click(function () {

        ClosePopup(2);
        return false;
    });

    $('#btnCustomCancel').click(function () {
        CloseCustomFieldPopup();
    });


    //close Modal popup
    $('#btnEventLocationCancel').click(function () {

        ClosePopup(1);
        return false;
    });

    //Jquery main Tabs
    JqueryTabs();

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 3);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 3);
    });

});


//Close custom field pop up
function CloseCustomFieldPopup() {

    $('#divEventClassificationDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divEventClassificationDialog").dialog("destroy");
    $('#divEventClassificationDialog').bind("dialogclose", function (event, ui) {
    });

    //$('#dialog-modal-test').hide();
    //$("#dialog-modal-test").dialog("destroy");
    //$('#dialog-modal-test').bind("dialogclose", function (event, ui) {
    //});


}


//Set location , Need to optimize
function SetLocation() {

    _locationCheckId = parseInt($('#hdnLocationId').val());

    var listFullPathDetails = ListFullPathDetail(_hdnLocationId, _eventId, 1);
    var locationValue = 'lblEventLocationData';
    if (_classificationCount == 0) {

        //Adding the initial required properties for Location Auto save.
        _fields[locationValue] = {};

        _fields[locationValue].previousValue = [];
        _fields[locationValue].newValue = [];
        _fields[locationValue].previousListId = [];
        _fields[locationValue].newListId = [];

        _fields[locationValue].originalValue = $('#' + locationValue).html();
        _fields[locationValue].previousValue.push($('#' + locationValue).html());
        _fields[locationValue].newValue.push($('#' + locationValue).html());
        _fields[locationValue].controlType = 'Select multiple from list';

        _fields[locationValue].originalListId = parseInt(_hdnLocationId);
        _fields[locationValue].previousListId.push(parseInt(_hdnLocationId));

        _classificationCount = _classificationCount + 1;
    }
}

//Set classification, Need to optimize
function SetClassification() {

    var listFullPathDetails = ListFullPathDetail(_hdnClassificationId, _eventId, 2);
    _classificationIdsArray = [];
    var classifications = '';
    if (_hdnClassificationId != null && _hdnClassificationId.length > 0)
        classifications = _hdnClassificationId.split(',');

    for (var j = 0; j < classifications.length; j++) {

        _classificationIdsArray.push(classifications[j]);
        _classificationFieldId = _classificationFieldId + classifications[j] + ",";
    }

    //-------------------------------------------------------------
    var classificationValue = 'lblEventClassificationData';
    if (_classificationFieldCount == 0) {

        _fields[classificationValue] = {};


        _fields[classificationValue].previousValue = [];
        _fields[classificationValue].newValue = [];
        _fields[classificationValue].previousListId = [];
        _fields[classificationValue].newListId = [];

        _fields[classificationValue].originalValue = $('#' + classificationValue).html();
        _fields[classificationValue].previousValue.push($('#' + classificationValue).html());
        _fields[classificationValue].newValue.push($('#' + classificationValue).html());

        _fields[classificationValue].controlType = 'Select multiple from list';
        _classificationFieldId = _classificationFieldId.substring(0, _classificationFieldId.lastIndexOf(','));

        _fields[classificationValue].originalListId = _classificationFieldId;
        _fields[classificationValue].previousListId.push(_classificationFieldId);
        _fields[classificationValue].newListId.push(_classificationFieldId);

        _classificationFieldCount = _classificationFieldCount + 1;
    }
}
//Assigning the control with default Values
function SetPageLoad() {

    //this code to reduce Custom fields main div height
    $('.undoDiv').hide();
    $('.ContentHeader').hide();
    $('#divErrorMsg').hide();
    $('#liCustomFields').removeClass('ContentWidth ClearAll');

    //disable future date
    // $("#body_txtDate").datepicker({ maxDate: '+0d' });

    _sessionLocationId = '';
    var elements = $('.trackChanges');
    GetInitialValues(elements);

    $('#ancUndo').hide();
    $('#ancUndoAll').hide();

    $('#btnEventSave').show();
    $('#btnCancel').show();
    
    //assigning the DatePicker to the DateFields
    var dateSelectors = "#body_txtInvDate, #body_txtDate, #body_txtCapDate";
    OnDateChange(dateSelectors);

    $('#eventErrorMsg').html('');

    EventTypeFromUrl();

    if (_eventId != -1) {


        var systemFieldId = '';
        _isClassificationExist = true;

        _hdnLocationId = $('#hdnLocationId').val();
        _hdnClassificationId = $('#hdnClassificationId').val();

        //for color Change in Subtab
        TabColor(location, 3);

        SetLocation();
        SetClassification();

        //Auto save the fields data on edit mode
        var inputControls = ':input';

        $(inputControls).blur(function (e) {

            var ItemId = $(this).attr('id');
            ValidationOnBlur(e, ItemId);
        });

        //Hide buttons,Save $ Cancel on edit mode
        $('#btnEventSave').hide();
        $('#btnCancel').hide();
    }
   
    var auditLabel = $('#body_lblDate')[0].textContent;
  
    var futureDate = '+0d';
    $("#body_txtDate").datepicker({
        changeMonth: true,
        changeYear: true,      
        maxDate: ((auditLabel == 'Audit Date*' || auditLabel == 'Audit Date') ? '' : futureDate),//display future date for AUDIT only.
        dateFormat: localStorage.dateFormat
    });
    $("#body_txtInvDate").datepicker({ dateFormat: localStorage.dateFormat });
    $('#body_txtCapDate').datepicker({ dateFormat: localStorage.dateFormat });
    $("#ui-datepicker-div").addClass("notranslate");

    $('#EventLocationDialog').hide();

    $('#btnSelectEventClassification').hide();
    $('#btnSelectEventLocation').hide();
    $('#btnEventClassificationCancel').hide();
    $('#btnEventLocationCancel').hide();

    GetTeamMemberAccess('divEventDetails');

}

// to validate fields at edit time
function ValidationOnBlur(e, itemId) {

    var txtEventName = 'body_txtEventName';
    var txtEventDate = 'body_txtDate';
    var txtLocation = 'lnkEventLocation';
    var txtClassification = 'lnkEventClassification';
    var txtEventTime = 'body_txtTime';

    if (itemId != null && itemId.length > 0) {

        if (itemId == txtEventName) {
            var value = TrimString($('#' + txtEventName).val());
            if (value == null || value.length == 0) {
                ShowHideWarningMessage(txtEventName, 'Please specify Event Name', true);
                return;
            }
            else {

                ShowHideWarningMessage(txtEventName, 'Please specify Event Name', false);
            }
            _companyId = GetCompanyId();
            CheckEventName(_companyId, value, _eventId);
            if (!_eventDuplicate) {
                SaveFields(e);

                var eventName = '';
                if (_eventType == 1)
                    eventName = 'Incident';
                else if (_eventType == 2)
                    eventName = 'Investigation';
                else if (_eventType == 3)
                    eventName = 'Audit';
                else if (_eventType == 4)
                    eventName = 'ActionPlan';

                $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').text(eventName + ' > ' + value + ' > Details');
                $(document).attr('title', 'TapRooT® - ' + value);
                return;
            }
            return;
        }
        else if (itemId == txtEventTime) {

            if ($('#' + txtEventTime).next().hasClass('warningImage')) {
                return;
            }
        }

        SaveFields(e);
    }
}

//Close the modal pop up
function ClosePopup(listType) {

    //location-1,classification-2
    var popupId = '';
    popupId = ((listType == 1) ? '#EventLocation' : '#divEventClassificationDialog');
    $(popupId).hide();

    //modal popup code
    $("#dialog:ui-dialog").dialog("destroy");
    $(popupId).dialog("destroy");
    $(popupId).bind("dialogclose", function (event, ui) {
    });
}

//take dynamic values in arraylist
var _arrDynamicFieldIds;
var _arrDynamicFieldValues;
var _arrControlsNames;
//Save the Events
function SaveEvent() {
        
    //get companyId and UserId
    _companyId = GetCompanyId();
    _userId = GetUserId();
    _svcURL = GetServicePath();

    var location = ((_locationData == '') ? _sessionLocationId : _locationData);

    var classification = _classificationData;

    var eventName = TrimString($('#body_txtEventName').val());
    var eventDate = $('#body_txtDate').val();//fromDatefrmatToDate(TrimString($('#body_txtDate').val()));
    
    var eventTime = TrimString($('#body_txtTime').val());
    var eventDescription = TrimString($('#body_txtEventDescription').val());
    var eventFileNumber = TrimString($('#body_txtEventFileNumber').val());

    var status = $('#body_ddlStatus').val();

    var eventType = EventTypeFromUrl();

    var investigationDate = (($('#body_txtInvDate').is(":visible")) ? $('#body_txtInvDate').val() : '');//fromDatefrmatToDate($('#body_txtInvDate').val()) : '');
    var investigationTime = (($('#body_txtInvTime').is(":visible")) ? TrimString($('#body_txtInvTime').val()) : '');

    var capDate = (($('#body_txtCapDate').is(":visible")) ? $('#body_txtCapDate').val() : '');//fromDatefrmatToDate($('#body_txtCapDate').val()) : '');
    var capTime = (($('#body_txtCapTime').is(":visible")) ? TrimString($('#body_txtCapTime').val()) : '');

    var dynamicElements = $('.trackDynamicFields');

    GetDynamicFieldElements(dynamicElements);
    //to validate location
    if (ValidateEventFields(eventName, eventDate, location, classification, _isValidateDynamicField)) {

        CheckEventName(_companyId, eventName, '0');

        if (!_eventDuplicate) {
            var eventDetails = new EventDetailData(_eventId, eventName, eventDate, eventTime, location, classification, status,
                                eventType, investigationDate, investigationTime, eventDescription, eventFileNumber, capDate, capTime, _arrDynamicFieldIds, _arrDynamicFieldValues, _arrControlsNames); //change session user id name

            _data = JSON.stringify(eventDetails);
            _serviceUrl = _baseURL + 'Services/Events.svc/SaveEvent';
            AjaxPost(_serviceUrl, _data, eval(SaveEventSuccess), eval(SaveEventFail));
        }

    }
}

//validate the custom fields.
function ValidateCustomFields(isValidateDynamicField) {

    return ValiadteControlValue();
}

_isValidateDynamicField = false;
// get dynamic field values in arraylist to save in FieldValues table
function GetDynamicFieldElements(dynamicElements) {

    _arrDynamicFieldIds = [];
    _arrDynamicFieldValues = [];
    _arrControlsNames = [];
    var previousRadioButtonId = '';
    _isValidateDynamicField = false;
    for (var i = 0; i < dynamicElements.length; i++) {

        //for tree view need to take label id (systemListId)
        if (dynamicElements[i].tagName.toUpperCase() == 'LABEL' && dynamicElements[i].innerHTML != '') {
            var value = dynamicElements[i].dataset.entityQueryfieldValue;
            if (dynamicElements[i].dataset.entityControltype == 'Select one from list' || dynamicElements[i].dataset.entityControltype == 'Select multiple from list') {
                var id = $(dynamicElements[i]).next("[id=hdn" + value + "]").val();
                _arrDynamicFieldIds.push(value);
                _arrDynamicFieldValues.push(id);

                _arrControlsNames.push(dynamicElements[i].tagName.toUpperCase());
            }

        }
        else {
            if (dynamicElements[i].dataset != undefined) {
            if (dynamicElements[i].dataset.entityControltype == 'Radio Button') {

                var radioButtonId = dynamicElements[i].dataset.entityQueryfieldValue;

                if (radioButtonId != '' && previousRadioButtonId != radioButtonId) {

                    var id = dynamicElements[i].dataset.entityQueryfieldValue;
                    isRadioButtonChecked = ($('#rbY' + id).is(':checked') ? 1 : $('#rbN' + id).is(':checked') ? "0" : "");

                    if (isRadioButtonChecked != "") {
                        _arrDynamicFieldIds.push(dynamicElements[i].dataset.entityQueryfieldValue);
                        _arrDynamicFieldValues.push(isRadioButtonChecked);
                        _arrControlsNames.push(dynamicElements[i].dataset.entityControltype);
                    }
                    previousRadioButtonId = radioButtonId;
                }
            }
            else if (dynamicElements[i].dataset.entityControltype == 'Text Box' || TrimString(dynamicElements[i].dataset.entityControltype) == 'Paragraph Text' || dynamicElements[i].dataset.entityControltype == 'Date Field') {

                if (dynamicElements[i].value != "" && dynamicElements[i].value != undefined) {

                    _arrDynamicFieldIds.push(dynamicElements[i].dataset.entityQueryfieldValue);
                    _arrDynamicFieldValues.push(TrimString(dynamicElements[i].value));
                    _arrControlsNames.push(dynamicElements[i].dataset.entityControltype);
                }
            }
            else if (dynamicElements[i].dataset.entityControltype == 'DropdownList') {
                if (dynamicElements[i].value != "" && dynamicElements[i].value != undefined) {
                    _arrDynamicFieldIds.push(dynamicElements[i].dataset.entityQueryfieldValue);
                    _arrDynamicFieldValues.push(dynamicElements[i].value);
                    _arrControlsNames.push(dynamicElements[i].tagName.toUpperCase());
                }
            }
            }
        }
    }

}

//success method for Save Event
function SaveEventSuccess(result) {

    $('#btnEventSave').hide();
    $('#btnCancel').hide();

    var jsonData = result.d;
    var eventId = jsonData[0];
    var mainTab = jsonData[1];
    var subTab = jsonData[2];
    var decryptEventId = jsonData[4];
    var isSuOrEntity = jsonData[5];
    var moduleId = EventTypeFromUrl();
    var tabId = 1;
    if (jsonData[3] != null)
        tabId = jsonData[3];

    if (mainTab != "-1") {
        $('#body_ucevent_divMainTabs').html('');
        $('#body_ucevent_divSubTabs').html('');

        $('#body_ucevent_divMainTabs').html(mainTab);
        $('#body_ucevent_divSubTabs').html(subTab);


        if (eventId > 0 || (eventId != null && eventId != '0')) {

            if (moduleId == 1)
                _message = "Incident Created.";
            else if (moduleId == 2)
                _message = "Investigation Created.";
            else if (moduleId == 3)
                _message = "Audit Created.";
            else if (moduleId == 4)
                _message = "Action Plan Created.";
            else
                _message = 'Event Created.'

            //_message = 'Data Saved Successfully.'
            _messageType = 'jSuccess';
            NotificationMessage(_message, _messageType, true, 1000);
            SlideTab(moduleId, eventId, tabId);
            //$("html, body").animate({ scrollTop: 0 }, "slow");
            //NavigatePage(moduleId, eventId, tabId);
        }

        $('#body_ucevent_divMainTabs li').addClass('ui-state-default ui-corner-top');

        $('#tabs').tabs();
        $('#ancmain' + moduleId)[0].offsetParent.className = 'ui-state-default ui-corner-top ui-tabs-selected ui-state-active';

        //remove default Tab selection color
        if (moduleId != '1' && moduleId != 3 && _newEventId != 0) {
            $('#tabs li')[0].className = '';
            $('#tabs li')[0].className = 'ui-state-default ui-corner-top';
        }

        var classificationValue = 'lblCust' + _treeFieldId;
        //select multiple from list save -for dynamic controls
        if (classificationValue != '' && _classificationId != '') {
            _eventId = eventId;
            GenericSaveFields(_fields, classificationValue, _classificationId);

        }
    }
}

function SlideTab(moduleId, eventId, tabId) {

    //$("#divTab").hide("slide", { direction: "right" }, 2000, setTimeout(function () {

    //    NavigatePage(moduleId, eventId, tabId);
    //}, 2000));

    $("#ancmain2,#ancmain4").hide(setTimeout(function () {

        NavigatePage(moduleId, eventId, tabId);
    }, 2000));
}


//Error method for save Event
function SaveEventFail() {

    alert('Service failed.');
}

function CheckEventName(companyId, eventName, eventId) {

    if (companyId != null && eventName != null) {

        var data = '&eventId=' + eventId + '&companyID=' + companyId + '&eventName=' + eventName;
        var serviceUrl = _baseURL + 'Services/Events.svc/CheckEventName';
        GetAjax(serviceUrl, data, eval(CheckUniqueEventSuccess), eval(isCheckUniqueEventFail), 'isEventUnique');
    }
}

//success method for check event name
function CheckUniqueEventSuccess(result) {

    if (result.d != 'null') {
        _eventDuplicate = true;
        ShowHideWarningMessage('body_txtEventName', 'An event with this name already exists. Please enter a different name for this event', true);
        NotificationMessage('An event with this name already exists. Please enter a different name for this event.', 'jError', true, 3000);
    }
    else {
        _eventDuplicate = false;
    }
}

//Error method for check  Event Name
function isCheckUniqueEventFail() {
    alert('Service failed.');
}

//navigate to the corresponding Event page
function NavigatePage(moduleId, eventId, tabId) {

    var moduleName = ((moduleId == 1) ? 'Incident' : (moduleId == 2) ? 'Investigation' : (moduleId == 3) ? 'Audit' : 'ActionPlan');
    window.location.href = _baseURL + "Event/" + moduleName.replace(/ /g, '') + "/Details-" + tabId + "/" + eventId + "/Edit?1";
}

//property of Event class
function EventDetailData(eventId, eventName, eventDate, eventTime, location, classification, status, eventType, investigationDate,
                investigationTime, eventDescription, eventFilenumber, capDate, capTime, arrDynamicFieldIds, arrDynamicFieldValues, arrControlsNames) {
    
    this.eventId = eventId;
    this.eventName = eventName;
    this.eventDate = eventDate;
    this.eventTime = eventTime;
    this.location = location;
    this.classification = classification;
    this.status = status;
    this.userID = GetUserId();
    this.eventType = eventType;
    this.investiagtionDate = investigationDate;
    this.investiagtionTime = investigationTime;
    this.eventDescription = eventDescription;
    this.eventFileNumber = eventFilenumber;
    this.capDate = capDate;
    this.capTime = capTime;
    this.companyID = _companyId;
    this.baseURL = _baseURL;
    // to save dynamic field values in FieldValues table
    this.arrDynamicFieldIds = arrDynamicFieldIds;
    this.arrDynamicFieldValues = arrDynamicFieldValues;
    this.arrControlsNames = arrControlsNames;
}



function Decrypt(val) {
    var actual = val;
    var key = 100; //Any integer value
    var result = "";
    for (i = 0; i < actual.length; i++) {
        result += String.fromCharCode(key ^ actual.charCodeAt(i));
    }
    return result;
}

//to get the eventid and moduleid from url-//move to js.com file
function EventTypeFromUrl() {

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var evType = splitUrl[splitUrl.length - 4];

    _eventId = splitUrl[splitUrl.length - 2];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }
    _eventId = ((_eventId == 0) ? -1 : _eventId);
    _eventType = ((evType == 'Incident' || evType == 'New-Incident') ? 1 : (evType == 'Investigation' || evType == 'New-Investigation') ? 2 : (evType == 'Audit' || evType == 'New-Audit') ? 3 : (evType == 'CAP' || evType == 'CorrectiveActionPlan' || evType == 'ActionPlan' || evType == 'Action-Plan') ? 4 : '');

    return _eventType;
}

var LocClsPopUpId = '';
function GetListName(listId) {
    var data = '&listId=' + listId;
    var serviceURL = _baseURL + 'Services/ListValues.svc/GetListValueName';
    Ajax(serviceURL, data, eval(GetListNameSuccess), eval(GetListNameFails));
}

function GetListNameSuccess(result) {
    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");
    $("#" + LocClsPopUpId).dialog({
        //height: 400,
        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        title: result.d,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
}

function GetListNameFails() {
}

//open the modal popup for Location and classification
function OpenPopup(listType) {
    //location-1,classification-2
    var errorLabelId = '';
    var popupId = '';
    var fullPath = '';

    errorLabelId = ((listType == 1) ? '#ErrLocation' : '#ErrClassification');
    popupId = ((listType == 1) ? "#EventLocation" : "#divEventClassificationDialog");
    fullPath = ((listType == 1) ? '\\' + '\\Location' : '\\' + '\\Classification');

    $(errorLabelId).html('');
    $(errorLabelId).removeClass('WarningMessage');
    $(errorLabelId).removeClass('SuccessMessage');

    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");

    $(popupId).dialog({
        //height: 400,
        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });


    //Ajax call
    if (listType == 1) {
        var locationListId = $('#body_hdnLocationListId').val();
        LocClsPopUpId = 'EventLocation';
        GetListName(locationListId);
        $('#btnSelectEventLocation').show();
        $('#btnEventLocationCancel').show();
        var locationDetails = ListDetail(locationListId, '0');
        _data = JSON.stringify(locationDetails);
        _serviceUrl = _baseURL + 'Services/ListValues.svc/BuildJSTreeView';
        AjaxPost(_serviceUrl, _data, eval(LocationTreeviewSuccess), eval(LocationTreeviewFail));
    }
    else {
        var classListId = $('#body_hdnClassListId').val();
        LocClsPopUpId = 'divEventClassificationDialog';
        GetListName(classListId);

        $('#btnSelectEventClassification').show();
        $('#btnEventClassificationCancel').show();
        var classificationDetails = ListDetail(classListId, '0');
        _data = JSON.stringify(classificationDetails);
        _serviceUrl = _baseURL + 'Services/ListValues.svc/BuildJSTreeView';
        AjaxPost(_serviceUrl, _data, eval(ClassificationTreeviewSuccess), eval(ClassificationTreeviewFail));
    }
}

//create object of Location and classification list
function ListDetail(listId, fullPath) {

    var listData = {};
    listData.listId = listId;
    listData.fullPath = fullPath;

    return listData;
}

//Success  method for Loading Classification list
function ClassificationTreeviewSuccess(result) {

    var jsonData = jQuery.parseJSON(result.d);
    DisplayTreeView(2, jsonData);
}

//Error method for Loading Classification list
function ClassificationTreeviewFail() {

    alert('Service Failed.');
}


//Sucees method for loading classification tree list
function LocationTreeviewSuccess(result) {
    var jsonData = jQuery.parseJSON(result.d);
    DisplayTreeView(1, jsonData);
}

//Error method for loading location treeview
function LocationTreeviewFail() {

    alert('Error in service.');
}


//Display Treeview For Location and  classification
function DisplayTreeView(listType, jsonData) {

    //Location-1,classification-2

    var listId = ((listType == 1) ? '#EventLocationTreeList' : '#EventClassificationTreeList');

    $(listId).jstree({
        "json_data": { "data": jsonData },
        rules: { multiple: "ctrl" },
        checkbox: {
            two_state: true
        },
        "themes": {
            "theme": "default",
            "dots": false,
            "icons": true
        },
        plugins: ["themes", "json_data", "dnd", "checkbox", "crrm", "ui", "types"]
    });

    //To display the tree view in expanded form
    ExpandTreeView(listType);

    //checkbox select and de select from treeview
    if (listType == 1) {

        $(listId).bind("change_state.jstree", function (e, data) {
            if (data.args[1] == false) {
                var selectedId = $(data.rslt).attr('id');
            }

            var selectedIdArray = $(listId).jstree("get_checked", null, true);

            $(selectedIdArray).each(function (i, node) {

                var treeviewItemId = $(node).attr("id");
                if (selectedId != treeviewItemId) {
                    $(this).removeClass('jstree-checked').addClass('jstree-unchecked').removeClass('not-default');
                }
            });
        });
    }

    //if (listType == 2)
        setTimeout(function () { CheckParentNode(); }, 0);
}


function CheckParentNode() {
    var checkedItems = $('.jstree-checked');
    // Loop through the items and check the paretn nodes if any child node (of any level) is checked by user
    $.each(checkedItems, function (index, item) {
        CheckParentByCode(item);
    });

    // Bind a click event for the checkboxes in classification
    $('.jstree').bind('change_state.jstree', function (e, data) {
        // if data.args[1] is true, then node is unchecked. If false, then node is checked
        if (!data.args[1]) {
            CheckParentByCode(data.args[0]);
        }
        else {
            if (!$(data.args[0]).closest('li').siblings().hasClass('jstree-checked'))
                UnCheckParentByCode(data.args[0]);

            // Uncheck the child items
            UncheckChildItems(data.args[0]);
        }
    });
}


// Find the parent node of object - obj and check the node
function CheckParentByCode(obj) {
    var x = $(obj).closest('ul').closest('li');

    if ($(x).length > 0) {
        if ($(x).hasClass('jstree-unchecked'))
            $(x).removeClass('jstree-unchecked');

        if (!$(x).hasClass('jstree-checked')) {
            $(x).addClass('jstree-checked');
            $(x).addClass('not-default');
        }

        CheckParentByCode(x);
    }
}

// Find the parent node of object - obj and uncheck the node
function UnCheckParentByCode(obj) {
    var x = $(obj).closest('ul').closest('li');

    if ($(x).length > 0) {
        if ($(x).hasClass('jstree-checked') && $(x).hasClass('not-default'))
            $(x).removeClass('jstree-checked');

        if (!$(x).hasClass('jstree-unchecked') && $(x).hasClass('not-default')) {
            $(x).addClass('jstree-unchecked');
        }

        $(x).removeClass('not-default');

        UnCheckParentByCode(x);
    }
}

//To display the treeView in expanded form
function ExpandTreeView(listType) {

    //location-1,classification-2
    var listId = ((listType == 1) ? '#EventLocationTreeList' : '#EventClassificationTreeList');

    $(listId).bind("loaded.jstree", function (event, data) {
        $(listId).jstree("open_all");

        if (listType == 1) {

            $('#' + _locationCheckId).each(function () {

                $('#EventLocationTreeList').find("#" + _locationCheckId).removeClass('jstree-unchecked').addClass('jstree-checked');
            });

        }
        else {

            $(listId).jstree("refresh");
            for (i = 0; i < _classificationIdsArray.length; i++) {

                if (_classificationIdsArray[i].split(',').length > 1) {

                    var ids = _classificationIdsArray[i].split(',')

                    for (j = 0; j < ids.length; j++) {
                        $('#' + ids[j]).each(function () {
                            $(this).removeClass('jstree-unchecked').addClass('jstree-checked');
                        });
                    }
                }
                else {

                    $('#' + _classificationIdsArray[i]).each(function () {

                        $('#EventClassificationTreeList').find('#' + _classificationIdsArray[i]).removeClass('jstree-unchecked').addClass('jstree-checked');
                    });
                }

            }
        }
    });
}

//populate LocationTreeList  Data
function PopulateLocationData() {

    //replace notification
    $('#ErrLocation').html('');
    $('#ErrLocation').removeClass('WarningMessage');
    $('#ErrLocation').removeClass('SuccessMessage');
    $('#eventErrorMsg').removeClass('WarningMessage');
    $('#eventErrorMsg').removeClass('SuccessMessage');
    $('#eventErrorMsg').html('');

    _locationCheckedItemsArray = [];
    _locationCheckedNamesArray = [];

    //select tree view selected items
    //$("#EventLocationTreeList").jstree("get_checked", false, false).each(function (obj) {
    $('#ErrLocation').html('');
    $('#ErrLocation').removeClass('WarningMessage');

    var totalChecked = 0;
    var totalCheckedItems = $("#EventLocationTreeList").jstree("get_checked", false, false);

    totalCheckedItems = $.grep(totalCheckedItems, function (item, index) {
        return (!$(item).hasClass('not-default'));
    });

    totalChecked = $(totalCheckedItems).length;

    if (totalChecked == 1) {
        ShowHideWarningMessage('lnkEventLocation', 'Please select location', false);

        _locationCheckedItemsArray.push(totalCheckedItems[0].id);
        _locationId = _locationId + totalCheckedItems[0].id + ',';
        _locationCheckedNamesArray.push(TrimString($(totalCheckedItems).children('a').text()));
    }
    else if(totalChecked > 1) {
        NotificationMessage('Please select only one Location.', 'jError', true, 3000);
        return false;
    }
    //});

    if (_locationCheckedItemsArray.length == 0) {
        NotificationMessage('Please check item to select.', 'jError', true, 3000);
        return false;
    }
    else {
        $('#lblEventLocationData').html('');
        if (_locationId != '') {
            if (_locationId.lastIndexOf(',') != -1)
                _locationId = _locationId.substring(0, _locationId.lastIndexOf(','));
            GetFormattedFullPath(_locationId, "0", "0", 1);
            _locationData = _locationId;

            //Auto save the Location.
            if (_eventId != -1)
                SaveLocation(_locationId);

            if (_classificationCount > 0 && _eventId != -1 && _eventId != '0') {

                //Track the Location changes.
                var locationValue = 'lblEventLocationData';

                var field = {};
                field.fieldName = 'lblEventLocationData';
                field[locationValue] = {};
                field[locationValue] = _fields[locationValue];

                //Pass the field info into the stack
                _undoStack.push(field);
                $('#ancUndo').show();

                //Check for the modified values to show the UNDOALL link
                if (_undoStack.length > 1)
                    $('#ancUndoAll').show();
            }
            ClosePopup(1);
        }
    }
}

//save the selected location
function SaveLocation(locationId) {

    //Ajax call to save the location
    var selectLocationDetails = selectLocationDetailData(_eventId, locationId);
    _data = JSON.stringify(selectLocationDetails);
    _serviceUrl = _baseURL + 'Services/Events.svc/LocationSave';
    AjaxPost(_serviceUrl, _data, eval(SaveLocationSuccess), eval(SaveLocationFail));

}

//create Location class
function selectLocationDetailData(eventId, locationId) {

    var locationData = {};
    locationData.eventId = eventId;
    locationData.locationId = locationId;
    return locationData;
}

//success method for SaveSelectedLocation
function SaveLocationSuccess() {

}

//Service fail method
function SaveLocationFail() {

}

//To Get the FormattedFullpath For Location and Classification 

function GetFormattedFullPath(listId, eventId, systemFieldId, listType) {

    //Location-1,classification-2

    var listFullPathDetails = ListFullPathDetail(listId, eventId, systemFieldId);

    _data = JSON.stringify(listFullPathDetails);
    _serviceUrl = _baseURL + 'Services/ListValues.svc/SelectLocation';

    if (listType == 1)
        AjaxPost(_serviceUrl, _data, eval(GetFormattedLocationFullpathSuccess), eval(GetFormattedLocationFullpathFail));

    else
        AjaxPost(_serviceUrl, _data, eval(GetFormattedClassificationFullpathSuccess), eval(GetFormattedClassificationFullpathFail));
}

//Full path Details for Location and  classification

function ListFullPathDetail(listId, eventId, systemFieldId) {

    var listFullPathDetail = {};

    listFullPathDetail.systemListIds = listId;
    listFullPathDetail.eventId = eventId;
    listFullPathDetail.systemFieldId = systemFieldId;

    return listFullPathDetail;
}


//Success  method  for GetFormattedFullpath
function GetFormattedLocationFullpathSuccess(result) {

    var fullPath = '';
    if (result.d != null && result.d != '[]') {
        var jsonData = jQuery.parseJSON(result.d);

        if (fullPath != undefined) {
            fullPath = fullPath + jsonData[0];

            $('#lblEventLocationData').html(fullPath);
            _locationCheckId = jsonData[1];

            var locationValue = 'lblEventLocationData';
            if (_classificationCount == 0) {

                //Adding the initial required properties for Location Auto save.
                _fields[locationValue] = {};
                _fields[locationValue].previousValue = [];
                _fields[locationValue].newValue = [];
                _fields[locationValue].previousListId = [];

                _fields[locationValue].originalValue = $('#' + locationValue).html();
                _fields[locationValue].previousValue.push($('#' + locationValue).html());
                _fields[locationValue].newValue.push($('#' + locationValue).html());

                _fields[locationValue].controlType = 'Select multiple from list';

                _fields[locationValue].originalListId = jsonData[1];
                _fields[locationValue].previousListId.push(parseInt(jsonData[1]));

                _classificationCount = _classificationCount + 1;
            }
            else {
                _fields[locationValue].previousValue.push(fullPath);
                _fields[locationValue].previousListId.push(parseInt(jsonData[1]));
            }
        }
    }
    _locationId = '';
}

//service fail method
function GetFormattedLocationFullpathFail() {

    alert('Service failed.')
}

function PopulateClassificationData() {

    //replace with notification
    $('#ErrClassification').html('');
    $('#ErrClassification').removeClass('WarningMessage');
    $('#ErrClassification').removeClass('SuccessMessage');
    $('#eventErrorMsg').removeClass('WarningMessage');
    $('#eventErrorMsg').removeClass('SuccessMessage');
    $('#eventErrorMsg').html('');

    _classificationCheckedNamesArray = [];
    _classificationCheckedItemsArray = [];

    $("#EventClassificationTreeList").jstree("get_checked", null, true).each(function () {
        ShowHideWarningMessage('lnkEventClassification', 'Please select classification', false);
        
        if (!$(this).hasClass('not-default')) {
            _classificationCheckedItemsArray.push(this.id);
            _classificationId = _classificationId + this.id + ',';
            _classificationCheckedNamesArray.push(TrimString($(this).children('a').text()));
        }
    });

    if (_classificationCheckedItemsArray.length == 0) {
        NotificationMessage('Please check item(s) to select.', 'jError', true, 3000);
        return false;
    }
    else {

        $('#lblEventClassificationData').html('');
        if (_classificationId != '') {
            if (_classificationId.lastIndexOf(',') != -1)
                _classificationId = _classificationId.substring(0, _classificationId.lastIndexOf(','));

            GetFormattedFullPath(_classificationId, "0", "0", 2);
            _classificationData = _classificationId;

            //Save Classification in DB
            if (_eventId != -1)
                SaveClassification(_classificationData);

            //-------------------------------------------------------------------
            if (_classificationFieldCount > 0 && _eventId != -1 && _eventId != '0') {

                var classificationValue = 'lblEventClassificationData';

                var field = {};
                field.fieldName = 'lblEventClassificationData';
                field[classificationValue] = {};
                field[classificationValue] = _fields[classificationValue];

                //Pass the field info into the stack
                _undoStack.push(field);
                $('#ancUndo').show();

                //Check for the modified values to show the UNDOALL link
                if (_undoStack.length > 1)
                    $('#ancUndoAll').show();
            }

            ClosePopup(2);
        }
    }
}

//save classification
function SaveClassification(classificationData) {

    var classificationDataDetail = ClassificationDetail(_eventId, classificationData);
    _data = JSON.stringify(classificationDataDetail);
    _serviceUrl = _baseURL + 'Services/Events.svc/ClassificationSave';
    AjaxPost(_serviceUrl, _data, eval(SaveClassificationSuccess), eval(SaveClassificationFail));

}

//classification class
function ClassificationDetail(eventId, classificationData) {
    var classificationDetail = {};
    classificationDetail.eventId = eventId;
    classificationDetail.classificationId = classificationData;
    return classificationDetail;
}

//Success method for SaveSelectedClassification
function SaveClassificationSuccess() {

}

//service failed method
function SaveClassificationFail() {

}

//Success method for Classification Fullpath

function GetFormattedClassificationFullpathSuccess(result) {

    //listType
    var formattedPath = '';
    if (result.d != null && result.d != '[]') {
        var jsonData = jQuery.parseJSON(result.d);

        _classificationIdsArray = [];

        for (var j = 0; j < jsonData.length; j++) {
            _classificationFieldId = _classificationFieldId + jsonData[j] + ",";
        }

        var data = '';//Store the selected ids.
        for (var i = 0; i < jsonData.length; i++) {

            data += jsonData[i + 1] + ",";
            _classificationIdsArray.push(jsonData[i + 1]);
            formattedPath = formattedPath + jsonData[i] + '<br/>';

            i++;
        }
        $('#lblEventClassificationData').html(formattedPath);

        //-------------------------------------------------------------
        var classificationValue = 'lblEventClassificationData';
        if (_classificationFieldCount == 0) {

            _fields[classificationValue] = {};
            _fields[classificationValue].previousValue = [];
            _fields[classificationValue].newValue = [];
            _fields[classificationValue].previousListId = [];

            _fields[classificationValue].originalValue = $('#' + classificationValue).html();
            _fields[classificationValue].previousValue.push($('#' + classificationValue).html());
            _fields[classificationValue].newValue.push($('#' + classificationValue).html());

            _fields[classificationValue].controlType = 'Select multiple from list';
            _classificationFieldId = _classificationFieldId.substring(0, _classificationFieldId.lastIndexOf(','));

            _fields[classificationValue].originalListId = _classificationFieldId;
            _fields[classificationValue].previousListId.push(_classificationFieldId);

            _classificationFieldCount = _classificationFieldCount + 1;
        }
        else {

            _fields[classificationValue].previousValue.push($('#' + classificationValue).html());
            _fields[classificationValue].previousListId.push(data.substring(0, data.lastIndexOf(',')));
        }
    }
    _classificationId = '';
}

//service failed method
function GetFormattedClassificationFullpathFail() {

    alert('Service failed.')
}

var _isNumberValidTrue = true;

//Method to validate Event
function ValidateEventFields(eventname, eventDate, location, classification, isValidateDynamicField) {

    var error = false;
    //
    var primaryErrorMessage = 'You are missing the following fields: ';
    var errorMessage = primaryErrorMessage;
    errorMessage += ((eventname == '') ? 'Event Name,\n' : '');
    //errorMessage += ((eventDate == '') ? TrimString($('.EventTypeDate').text()).slice(0, -1) + ',\n' : '');
    var ErStr = TrimString($('.EventTypeDate').text());
    errorMessage += ((eventDate == '') ? ErStr.substring(0, ErStr.indexOf('*')) + ',\n' : '');

    if ($('#body_txtTime').next().hasClass('warningImage')) {
        errorMessage += 'Invalid Time,\n';
    }
    if ($('#body_txtInvTime').next().hasClass('warningImage')) {
        errorMessage += 'Invalid Investigation Time,\n';
    }
    if ($('#body_txtCapTime').next().hasClass('warningImage')) {
        errorMessage += 'Invalid Cap Time,\n';
    }

    errorMessage += ((location == '') ? 'Location,\n' : '');

    if (_eventId != '-1')
        _isClassificationExist = true;


    if (classification == '' && _isClassificationExist != true) {
        errorMessage = errorMessage + 'Classification,\n';
    }


    if (ValidateCustomFields(isValidateDynamicField) != true || !_isNumberValidTrue) {

        var numberValidationMessage = "Please enter Number";
        if (errorMessage.length > primaryErrorMessage.length && errorMessage.substr(errorMessage.trim().length - 1, 2) != ",\n")
            errorMessage += ",\n";

        error = ValidateCustomFields(isValidateDynamicField);
        errorMessage += error;

    }

    var txtEventName = 'body_txtEventName';
    var txtEventDate = 'body_txtDate';
    var txtLocation = 'lnkEventLocation';
    var txtClassification = 'lnkEventClassification';

    if (eventname == null || eventname.length == 0) {
        ShowHideWarningMessage(txtEventName, 'Please specify Event Name', true);
    }
    else {

        ShowHideWarningMessage(txtEventName, 'Please specify Event Name', false);
    }
    if (eventDate == null || eventDate.length == 0) {
        ShowHideWarningMessage(txtEventDate, 'Please select event date', true);
        //$('#body_lblEventTime').css('padding-left', '18px');
        if ($.browser.msie && $.browser.version > 8) {
          // $('#body_lblEventTime').css('padding-left', '27px');
        }
    }
    else {
        ShowHideWarningMessage(txtEventDate, 'Please select event date', false);
        //$('#body_lblEventTime').css('padding-left', '40px');
        if ($.browser.msie && $.browser.version > 8) {
            //$('#body_lblEventTime').css('padding-left', '50px');
        }
    }

    if (location == null || location.length == 0) {

        ShowHideWarningMessage(txtLocation, 'Please select location', true);
    }
    else {
        ShowHideWarningMessage(txtLocation, 'Please select location', false);
    }

    if (classification == null || classification.length == 0) {

        ShowHideWarningMessage(txtClassification, 'Please select classification', true);
    }
    else {
        ShowHideWarningMessage(txtClassification, 'Please select classification', false);
    }

    if (errorMessage.length > primaryErrorMessage.length || error) {

        if (error == true)
            return false;

        errorMessage = errorMessage.replace(/,(\s+)?$/, '');
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    }
    return true;
}


//cancel function
function ClearFields() {
    _isDirty = false;
    $('#body_txtEventName').val('');
    $('#body_txtDate').val('');
    $('#body_txtTime').val('');
    $('#body_txtEventDescription').val('');
    $('#body_txtEventFileNumber').val('');

    $('#body_ddlStatus').val(1);
    $('#lblEventLocationData').html('');
    $('#lblEventClassificationData').html('');

    if ($('#body_txtInvDate').is(":visible"))
        $('#body_txtInvDate').val('');
    if ($('#body_txtInvTime').is(":visible"))
        $('#body_txtInvTime').val('');

    //textbox
    $('#ulCustomfield input[type="text"]').val('');

    //check alternative solution -avoid for loop
    //radio
    if ($('#ulCustomfield').find('input[type=radio]:checked').length > 0) {
        for (var i = 0; i < $('#ulCustomfield').find('input[type=radio]:checked').length; i++) {
            $('#ulCustomfield').find('input[type=radio]:checked')[i].checked = "";
        }
    }

    //text area
    if ($('#ulCustomfield textarea').length > 0) {
        for (var i = 0; i < $('#ulCustomfield textarea').length; i++) {
            $('#ulCustomfield textarea')[i].value = "";
        }
    }

    //drop down
    if ($('#ulCustomfield select').length > 0) {

        for (var i = 0; i < $('#ulCustomfield select').length; i++) {
            $('#ulCustomfield select')[i].value = '0';
        }
    }

    //treeview
    if ($('#ulCustomfield .EventImageLabel').length > 0) {
        for (var i = 0; i < $('#ulCustomfield .EventImageLabel').length; i++) {

            $('#ulCustomfield .EventImageLabel')[i].textContent = "";
        }
    }
    _locationCheckId = '';
    _classificationIdsArray = '';
    $('#hdn' + _treeFieldId).val('');

    if (_allcustomtreeviewIds != null && _allcustomtreeviewIds.length > 0) {
        for (var i = 0; i < _allcustomtreeviewIds.length; i++) {
            $('#hdn' + _allcustomtreeviewIds[i]).val('');
        }
        _allcustomtreeviewIds = [];
    }

    $("#ulCustomfield input,input[type='text'],input[type='radio'],select,textarea").each(function () {
        ShowHideWarningMessage($(this).attr('id'), '', false);
    });

    $("#ulCustomfield input[type='radio']").each(function () {
        ShowHideWarningMessageForRadio($(this).next(), '', false);
    });

    ShowHideWarningMessage('body_txtEventName', 'Please specify Event Name', false);
    ShowHideWarningMessage('body_txtDate', 'Please select event date', false);
    ShowHideWarningMessage('lnkEventLocation', 'Please select location', false);
    ShowHideWarningMessage('lnkEventClassification', 'Please select classification', false);
}




