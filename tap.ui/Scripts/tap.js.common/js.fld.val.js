﻿
//Validation of Field
function ValidateField(field, fieldId) {

    var dataType = field[fieldId].dataType;
    var value = $('#' + fieldId).val();
    var isValid = (dataType == "Int" ? ValidateNumber(value) : true);

    return isValid;
}
//checking the input value is number
function ValidateNumber(value) {
    var val = TrimString(value);
    var isNumber = true;
    if (val.length == 0)
        isNumber = false;
    else {
        for (var i = 0; i < val.length; i++) {
            var x = parseInt(val.charAt(i))
            if (isNaN(x)) {
                isNumber = false;
                break;
            }
        }
    }
    return isNumber;
}

//check the input value is Datetime 
function ValidateDate(value) {
    var datePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{2,4})$/;
    //var datePat = /^([Jj][Aa][Nn]|[Ff][Ee][Bb]|[Mm][Aa][Rr]|[Aa][Pp][Rr]|[Mm][Aa][Yy]|[Jj][Uu][Nn]|[Jj][Uu][Lj]|[Aa][Uu][Gg]|[Ss][Ee][Pp]|[Oo][Cc][Tt]|[Nn][Oo][Vv]|[Dd][Ee][Cc])[ ](0[1-9]|[1-9]|[12][0-9]|3[01])[ ](19|20)\d\d$/;
    var dateArray = value.match(datePattern); // is the format ok?
    var isValidDate = true;

    if (dateArray == null)
        isValidDate = false;
    else {
        month = dateArray[1]; // p@rse date into variables 
        day = dateArray[3];
        year = dateArray[5];
        if (month < 1 || month > 12)
            isValidDate = false;
        if (day < 1 || day > 31)
            isValidDate = false;
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31)
            isValidDate = false;
        if (month == 2) {
            // check for february 29th 
            var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
            if (day > 29 || (day == 29 && !isleap))
                isValidDate = false;
        }
    }
    return isValidDate;
}

//Display the Military time
function FormatMilitaryTime(fieldId) {

    var eventTime = TrimString($('#' + fieldId).val());
    var warningImage = 'validationImg';

    //ShowHideWarningMessage(fieldId, 'Invalid Time', true);

    if (eventTime != '' && eventTime != null && eventTime.length > 0)
    {
        var timePattern = /^(?:(?:0?[0-9]|1[0-2]):?[0-5][0-9]\s?(?:(?:[Aa]|[Pp])[Mm])?|(?:1[3-9]|2[0-3]):?[0-5][0-9])$/;
        var matchTime = eventTime.match(timePattern);
        //if (matchTime == null || matchTime == false) {
        if (matchTime)
        {
            ShowHideWarningMessage(fieldId, 'Invalid Time', false);
            if (eventTime.indexOf(':') == -1) {

                var fragmentTime = (eventTime.length == 3) ? 1 : 2;

                hr = eventTime.substring(0, fragmentTime);
                min = eventTime.substring(fragmentTime, eventTime.length);

                formattedDate = hr + ':' + min;

                $('#' + fieldId).val(formattedDate);
            }
        }
        else {
            //if (eventTime.indexOf(':') == -1) {

            //    var fragmentTime = (eventTime.length == 3) ? 1 : 2;

            //    hr = eventTime.substring(0, fragmentTime);
            //    min = eventTime.substring(fragmentTime, eventTime.length);

            //    formattedDate = hr + ':' + min;

            //    $('#' + fieldId).val(formattedDate);

            //    ShowHideWarningMessage(fieldId, 'Invalid Time', true);
            //}
            ShowHideWarningMessage(fieldId, 'Invalid Time', true);
            return false;
        }

    } else {
        ShowHideWarningMessage(fieldId, 'Invalid Time', false);
        return false;
    }
}