﻿//* To save Log Details (User Transactions) (Deepa)*//

//Global Variables
var _serviceUrl;

//Activity Logging Save Method
function SaveActivityLog(companyId, eventId, userId, categoryId, logDetails) {

    //insert the record in Transaction table for Activity Log
    var data = JSON.stringify(LogDetails(companyId, eventId, userId, categoryId, logDetails));
    _serviceUrl = _baseURL + 'Services/Common.svc/SaveActivityLog';
    AjaxPost(_serviceUrl, data, eval(ActivityLogDataSuccess), eval(ActivityLogDataFail));
}

//Activity Logging Parameters
function LogDetails(companyId, eventId, userId, categoryId, logDetails) {

    var logDataDetails = {};
    logDataDetails.companyId = companyId;
    logDataDetails.eventId = eventId;
    logDataDetails.userID = userId;
    logDataDetails.categoryId = categoryId;
    logDataDetails.logDetails = logDetails;
    return logDataDetails;
}

//ActivityLogData Success method
function ActivityLogDataSuccess(result) {

}

//Service fail method
function ActivityLogDataFail() {
    alert('Service fails')
}
//Activity Logging code ends