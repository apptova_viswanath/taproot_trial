﻿//Global Variables
var _textColorImage = 'textColor';
var _bgColor = 'bgColor';
var _bold = 'bold';
var _italic = 'italic';
var _underline = 'underline';
var _shouldShowColor = false;
var _styleProperty = '';


//load image buttons-bold,italic,underline,forecolor and bg color
function ShowButtons() {

    var arrElements = [_bold, _italic, _underline, _textColorImage, _bgColor];

    var divToolBoxValues = document.getElementById('divToolBox');

    for (var i = 0; i < arrElements.length; i++) {

        var divTag = document.createElement('div');
        divTag.className = 'formatBox';
        divTag.id = 'div' + arrElements[i];
        divTag.setAttribute('style', ' background-image:url("' + _baseURL + 'Images/' + arrElements[i] + '.gif") !important;');
        divTag.setAttribute('onclick', "javascript:OpenColorCops('" + arrElements[i] + "')");

        divToolBoxValues.appendChild(divTag);

    }
}

//load color cop codes
function ShowColorCops() {

    var divMainTag = document.getElementById('colorCop');
    var arrcolorCodes = ['#FFFFFF', '#FFCCCC', '#FFCC99', '#FFFF99', '#FFFFCC', '#99FF99', '#99FFFF', '#CCFFFF', '#CCCCFF', '#FFCCFF', '#C0C0C0', '#FF0000'
                        , '#FF9900', '#FFCC66', '#FFFF00', '#33FF33', '#66CCCC', '#33CCFF', '#6666CC', '#CC66CC', '#999999', '#CC0000', '#FF6600', '#FFCC33'
                        , '#FFCC00', '#33CC00', '#00CCCC', '#3366FF', '#6633FF', '#CC33CC', '#666666', '#990000', '#CC6600', '#CC9933', '#999900', '#009900'
                        , '#339999', '#3333FF', '#6600CC', '#993399', '#333333', '#660000', '#993300', '#996633', '#666600', '#006600', '#336666', '#000099'];

    for (var i = 0; i < arrcolorCodes.length; i++) {

        divMainTag.className = 'setColorBox';
        var divColorTags = document.createElement('div');
        divColorTags.className = 'setColorCopBox';

        divColorTags.setAttribute('style', 'background-color:' + arrcolorCodes[i]);
        divColorTags.setAttribute('onclick', "javascript:SetSelectedColor('" + arrcolorCodes[i] + "')");
        divMainTag.appendChild(divColorTags);

    }
}

//open color cop box
function OpenColorCops(option) {

    _styleProperty = option;

    if (_selectedLabelId !== "") {
        if (option == _textColorImage || option == _bgColor) {

            if (!_shouldShowColor) {

                $('#colorCop').show();
                $('#div' + option)[0].style.border = "1px solid #1B268B";
            }
            else {
                $('#div' + option)[0].style.border = "";
                $('#colorCop').hide();

            }
            _shouldShowColor = !_shouldShowColor;

        }
        else {
            
            $('#colorCop').hide();
           
            if (_selectedLabelId == 'spnReportTitle')
                id = $("#" + _selectedLabelId)[0];
            else if ($("#" + _selectedLabelId)[0] != undefined) {
                if ($("#" + _selectedLabelId)[0].className == 'customLabel')
                    id = id = $("#" + _selectedLabelId)[0];
            }
            else
                id = $($("#delete" + _selectedLabelId)[0].parentNode)[0];

            if (id != '' && id != undefined) {

                if ($('#div' + option)[0].style.border == '') {
                    if (option == _bold)
                        id.style.fontWeight = option;
                    if (option == _italic)
                        id.style.fontStyle = option;
                    if (option == _underline)
                        id.style.textDecoration = option;

                    $('#div' + option)[0].style.border = "1px solid #1B268B";
                }
                else {
                    if (option == _bold)
                        id.style.fontWeight = "";
                    if (option == _italic)
                        id.style.fontStyle = "";
                    if (option == _underline)
                        id.style.textDecoration = "";

                    $('#div' + option)[0].style.border = "";
                }
            }

            //after formatting text update arraylist
            AddEditItemNames(null, null);

        }
    }
}

//on click of color -set the color to particular text
function SetSelectedColor(getColorCode) {

    if (_selectedLabelId == 'spnReportTitle')
        id = $("#" + _selectedLabelId)[0];
    else if ($("#" + _selectedLabelId)[0] != undefined) {
        if ($("#" + _selectedLabelId)[0].className == 'customLabel')
            id = id = $("#" + _selectedLabelId)[0];
    }
    else
        id = $($("#delete" + _selectedLabelId)[0].parentNode)[0];

    if (id != '' && id != undefined) {
        if (_styleProperty == _textColorImage)
            id.style.color = getColorCode;
        if (_styleProperty == _bgColor)
            id.style.backgroundColor = getColorCode;

    }

    $('#colorCop').hide();
    $('#div' + _styleProperty)[0].style.border = "";
    _shouldShowColor = false;

    //after formatting text update arraylist
    AddEditItemNames(null, null);
}

