﻿/* Override the native javascript functions if required.  */

var between = function (opA, opB) {
    if (opA > opB) return this >= opB && this <= opA
    else return this >= opA && this <= opB
};