﻿var _isSUCompany ='';

//$(document).ready(function () {
//    PageLoad();    
//});
window.onload = function () {
    PageLoad();
};

function PageLoad() {
    //alert(1);

    SetImageSrc();
    
    _isSUCompany = $('#hdnSUCompany').val();
    var isSUDivision = $('#hdnSUDivision').val();
    if (_isSUCompany != null && _isSUCompany == '1') {
        $('#ancCreateUser').hide();
        $('#divlblMyEvents').hide();
        $('#divChkMyEvents').hide();
        $('#divlblViewAllEvents').hide();
        $('#divChkViewAllEvents').hide();
        $('#divlblEditAllEvents').hide();
        $('#divChkEditAllEvents').hide();
        $('#divUserAdminSection').hide();
        $('.divtxtSearchUser').hide();
        $('#divDivisionSubscriptionStrt').show();
        $('#divDivisionSubscriptionEnd').show();

        //Replace Divisions with Companies for Single user.
        $('#ancViewDivision').html('<img id="Img1" class="imgHeader marginTop0Px" src="' + _baseURL + 'Images/Company.png" />Companies');
        
    }
    else if (isSUDivision != '1' || isSUDivision == '' || isSUDivision == null) {
        $('#divManagerUserSection').show();
        $('#divUserDetailsEventAccess').show();
       
        //if (IE(10)) {
        //    document.getElementById('#divManagerUserSection').style.display = 'block';
        //    document.getElementById('#divUserDetailsEventAccess').style.display = 'block';
        //}
      
        //Change Company Profile to Division Profile along with the respective images.
        $('#ancOrganizationMySetting').html('<img id="diviProfile" class="imgHeader marginTop0Px" src="' + _baseURL + 'Images/divisioninside.png" /> Division Profile');
    }

    if ($('#body_Adminbody_hdnSetUp').val() > 0)
        $('#divCongratSoftware').show();
        
    
    if (isSUDivision != null && isSUDivision == '1') {
        $('#ancCreateUser').hide();
        $('#h2Divisions').hide();
        $('#divlblMyEvents').hide();
        $('#divChkMyEvents').hide();
        $('#divlblViewAllEvents').hide();
        $('#divChkViewAllEvents').hide();
        $('#divlblEditAllEvents').hide();
        $('#divChkEditAllEvents').hide();
        $('#divUserAdminSection').hide();
        $('.divtxtSearchUser').hide();
        $('#ancCreateUserMySetting').hide();
        $('#MySettingh2DivisionHeader').hide();
        $('#MySettingDivisionDetails').hide();
        $('#divUserActive').hide();
    }
    else if (_isSUCompany != '1' || _isSUCompany == '' || _isSUCompany ==null) {
        $('#divManagerUserSection').show();
        $('#divUserDetailsEventAccess').show();
    }

    UserInformationGet();

    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0) {
        $('#AdminGlobalSetting').show();
        $('#AdminMySetting').hide();
        $('#ancSettingName').html('Division Settings');
        isParentCompany(divisionId);
        SetUrlForDivisionSetting();

        if (_isSUCompany != null && _isSUCompany == '1') {
            $('#ancCreateUser').hide();
            $('#h2Divisions').hide();
            $('#divAddNewUser').hide();
            $('#divDivisionDetails').hide();
            $('#ancCreateUserMySetting').hide();
            $('#divSubscriptionStartDate').show();
            $('#divSubscriptionStartEnd').show();
        }
    }

    var $strtDate = $('#body_Adminbody_txtSubscriptionStart');
    $strtDate.datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: localStorage.dateFormat
        //onClose: function (dateText, inst) {
        //    $(body_Adminbody_txtSubscriptionStart).datepicker('option', 'dateFormat', 'd M yy');
        //}
    });

    var $endDate = $('#body_Adminbody_txtSubscriptionEnd');
    $endDate.datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: localStorage.dateFormat
        //onClose: function (dateText, inst) {
        //    $(body_Adminbody_txtSubscriptionEnd).datepicker('option', 'dateFormat', 'd M yy');
        //}
    });


    
}
function IE(v) {
    return RegExp('msie' + (!isNaN(v) ? ('\\s' + v) : ''), 'i').test(navigator.userAgent);
}
function SetImageSrc() {

    //Dvision Setting image header 
    $('#imgDSSetting').attr('src', _baseURL + 'images/setting_3.png');

    $('.addImageUser').attr('src', _baseURL + 'images/add.png');
    $('.addImage').attr('src', _baseURL + 'Images/add.png');
}

function changeAdminSettingMenuBck() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    if (splitUrl[splitUrl.length - 1].indexOf('Profile') > -1 || splitUrl[splitUrl.length - 2].indexOf('Profile') > -1) {
        $('#adminOrgli').css('background', '#94ADC3');
        $('#adminOrgli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Divisions') > -1) {
        $('#adminDivisionli').css('background', '#94ADC3');
        $('#adminDivisionli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Users') > -1 || splitUrl[splitUrl.length - 3].indexOf('Users') > -1 || splitUrl[splitUrl.length - 2].indexOf('Users') > -1) {
        $('#adminUsersli').css('background', '#94ADC3');
        $('#adminUsersli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Lists') > -1) {
        $('#adminListsli').css('background', '#94ADC3');
        $('#adminListsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Attachment') > -1) {
        $('#adminAttachmentsli').css('background', '#94ADC3');
        $('#adminAttachmentsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Custom') > -1 || splitUrl[splitUrl.length - 1].indexOf('NewTab') > -1
       || splitUrl[splitUrl.length - 2].indexOf('Incident') > -1 || splitUrl[splitUrl.length - 2].indexOf('Investigation') > -1
       || splitUrl[splitUrl.length - 2].indexOf('Audit') > -1 || splitUrl[splitUrl.length - 2].indexOf('ActionPlan') > -1) {

        $('#adminCustomTabsli').css('background', '#94ADC3');
        $('#adminCustomTabsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Reports') > -1) {
        $('#adminReportsli').css('background', '#94ADC3');
        $('#adminReportsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Messages') > -1) {
        $('#adminHomePageMsgli').css('background', '#94ADC3');
        $('#adminHomePageMsgli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Messages') > -1) {
        $('#adminNotificationsli').css('background', '#94ADC3');
        $('#adminNotificationsli >div>a').css('color', 'white');
    }
}


function changeMySettingMenuBck() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    if (splitUrl[splitUrl.length - 1].indexOf('Profile') > -1 || splitUrl[splitUrl.length - 2].indexOf('Profile') > -1) {
        $('#mySettingOrgli').css('background', '#94ADC3');
        $('#mySettingOrgli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Users') > -1 || splitUrl[splitUrl.length - 3].indexOf('Users') > -1
        || splitUrl[splitUrl.length - 2].indexOf('Users') > -1) {
        $('#mySettingUsersli').css('background', '#94ADC3');
        $('#mySettingUsersli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Custom') > -1 || splitUrl[splitUrl.length - 1].indexOf('NewTab') > -1
       || splitUrl[splitUrl.length - 2].indexOf('Incident') > -1 || splitUrl[splitUrl.length - 2].indexOf('Investigation') > -1
       || splitUrl[splitUrl.length - 2].indexOf('Audit') > -1 || splitUrl[splitUrl.length - 2].indexOf('ActionPlan') > -1) {

        $('#mySettingCustomli').css('background', '#94ADC3');
        $('#mySettingCustomli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Lists') > -1) {
        $('#mySettingListsli').css('background', '#94ADC3');
        $('#mySettingListsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Attachment') > -1) {
        $('#mySettingAttachmentsli').css('background', '#94ADC3');
        $('#mySettingAttachmentsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Reports') > -1) {
        $('#mySettingReportsli').css('background', '#94ADC3');
        $('#mySettingReportsli >div>a').css('color', 'white');
    }
    if (splitUrl[splitUrl.length - 1].indexOf('Messages') > -1) {
        $('#MySettingliHomePageMsg').css('background', '#94ADC3');
        $('#MySettingliHomePageMsg >div>a').css('color', 'white');
    }
}

//To check that division is parent or not
function isParentCompany(divisionId) {
    var companyId = divisionId;
    var divisionListData = new Object();
    divisionListData.companyId = divisionId;
    data = JSON.stringify(divisionListData);
    _serviceUrl = _baseURL + 'Services/CompanyDetail.svc/ParentCompany';
    AjaxPost(_serviceUrl, data, eval(ParentCompanySuccess), eval(ParentCompanyFails));
}

// ParentCompany success
function ParentCompanySuccess(result) {
    if (result == null || result.d == false) {
        var divisionId = $('#hdnDivisionId').val();
        if (divisionId > 0)
            $('#ancDSDivisions').attr('href', _baseURL + 'Admin/DivisionProfile/' + divisionId + '/Edit');

        return false;
    }
}

function ParentCompanyFails() {
    alert('Service failed.');
}

function SetUrlForDivisionSetting() {

    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0) {
        $('#adminDivisionli').hide();

        $('#ancOrganization').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Profile/Edit');
        $('#ancViewDivision').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Divisions');
        $('#ancCreateDivision').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/New');
        $('#ancViewUser').attr('href', _baseURL + 'Admin/' + divisionId + '/Users'); 
        $('#ancCreateUser').attr('href', _baseURL + 'Admin/' + divisionId + '/Users/New');
        $('#ancSystemLists').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Lists'); 
        $('#ancNotifications').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Messages');
        $('#ancHomePageMsg').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Messages');
        $('#ancAttachmentFolders').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Attachment-Folders-Setup');
        $('#ancReportTemplates').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Report-Templates');
        $('#ancCustomTabs').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Custom-Tabs');

        $('.addImage').attr('src', _baseURL + 'images/add.png');


        //For My Setting section 
        $('#ancOrganizationMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Profile/Edit');
        $('#ancViewDivisionMySett').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Divisions');
        $('#ancNewDivisionMySett').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/New');
        $('#ancViewUserMySetting').attr('href', _baseURL + 'Admin/' + divisionId + '/Users');
        $('#ancCreateUserMySetting').attr('href', _baseURL + 'Admin/' + divisionId + '/Users/New');
        $('#ancSystemListsMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Lists');
        $('#ancHomePageMsgMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Messages');
        $('#ancAttachmentFoldersMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Attachment-Folders-Setup');
        $('#ancReportTemplatesMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Report-Templates');
        $('#ancCustomTabsMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Custom-Tabs');
        $('#ancHomePageMsgMySetting').attr('href', _baseURL + 'Admin/Division/' + divisionId + '/Messages');
    }
}

//User Information Access
function UserInformationGet() {
   // debugger;

    var userListData = new Object();
    userListData.userId = $('#hdnUserId').val();
    userListData.companyId = $('#hdnCompanyId').val();
    userListData.selectedDivisionId = $('#hdnDivisionId').val() == "" ? $('#hdnCompanyId').val() : $('#hdnDivisionId').val();

    data = JSON.stringify(userListData);
    _serviceUrl = _baseURL + 'Services/Users.svc/CheckIfGlobalAdminProfile';
    AjaxPost(_serviceUrl, data, eval(UserInformationGetSuccess), eval(UserInformationGetFail));

}

//org admin login  > profile : //isGlobalAdminPage : true , //$('#hdnDivisionId').val() > 0  : false
//division admin login   > profile : //isGlobalAdminPage : false, //$('#hdnDivisionId').val() > 0 : false
//org admin login  > division profile : //isGlobalAdminPage : true , //$('#hdnDivisionId').val() > 0 : true

// User Information Get Success
function UserInformationGetSuccess(result) {
    
    $('#DivTimeout').hide();

    var result = ParseToJSON(result.d);
    var hideSubscriptionPanel = result.hideSubscriptionPanel;
    var isGlobalAdminPage = result.isGlobalAdminPage;

    var showAppTimeOut = (isGlobalAdminPage) && !($('#hdnDivisionId').val() > 0);

    if (showAppTimeOut)
    {
        $('#DivTimeout').show();

    }

    //kvs
    var isEnt = false;
    //Hide the subscription part in Ent division .
    if (_isSUCompany == null || _isSUCompany == 0) {
        hideSubscriptionPanel = true;
        isEnt = true;
        //isGlobalAdminPage = true;
    }
    //kve

    if (hideSubscriptionPanel)
    {
        $('#divSubscription').hide();
        $('#divSubscriptionStartDate').hide();
        $('#divSubscriptionStartEnd').hide();
        if (isEnt || isGlobalAdminPage) { //kv
            $('#divSubscriptionNotify').hide();
        }
        else {
            $('#divSubscriptionNotify').show();
        }       

    }
    else {
        $('#divSubscription').show();
        $('#divSubscriptionStartDate').show();
        $('#divSubscriptionStartEnd').show();
        $('#divSubscriptionNotify').hide();

    }

    if (isGlobalAdminPage) {

        changeAdminSettingMenuBck();
        $('#AdminGlobalSetting').show();
        $('#AdminMySetting').hide();

        var divisionId = $('#hdnDivisionId').val();

        if (divisionId > 0) {

            $('#divPolicySettings').hide();

            $('#ancDSUsers').attr('href', _baseURL + 'Admin/' + divisionId + '/Users');
            $('#ancDSDivisions').attr('href', _baseURL + 'Admin/' + divisionId + '/Divisions');
            if (_isSUCompany) {
                $('#ancDSDivisions').attr('href', _baseURL + 'Admin/DivisionProfile/' + divisionId + '/Edit');
            }


            $('#ancSettingName').html('Division Settings');
            $('#AdminGlobalSetting').show();
            $('#goBackToGlobalSettings').show();

        }
        else {
            $('#goBackToGlobalSettings').hide();
            //$('#DivTimeout').hide();
            
            //$('#divSubscription').hide();
            //$('#divSubscriptionStartDate').hide();
            //$('#divSubscriptionStartEnd').hide();
            //$('#divSubscriptionNotify').hide();
        }

    }
    else {
        
        changeMySettingMenuBck();

        $('#ancSettingName').html('My Settings');

        //Display only Name instead of Company Name.
        $('#lblcompanyName').html('Name<span id="spnCompanyName" style="color: Red">*</span>');
     
        $('#AdminGlobalSetting').hide();
        $('#AdminMySetting').show();
        $('#goBackToGlobalSettings').hide();

        //$('#divSubscription').hide();
        //$('#divSubscriptionStartDate').hide();
        //$('#divSubscriptionStartEnd').hide();
        $('#divPolicySettings').show();

        var isSUDivision = $('#hdnSUDivision').val();
     

        if (isSUDivision != null && isSUDivision == '1') {
            $('#divSubscriptionNotify').show();
            $('#MySettingliHomePageMsg').hide();
            $('#ancViewUserMySetting').html('<img id="Img3" class="imgHeader marginTop0Px" src="' + _baseURL + 'images/viewuser.png" /> User Profile');
            _userId = $('#hdnUserId').val();
            $('#ancViewUserMySetting').attr('href', _baseURL + 'Admin/Users/' + _userId + '/Edit');

            $('#divUndoUserDetails').hide();
            //$('#DivTimeout').hide();
        }


        var divisionId = $('#hdnDivisionId').val();
        if (divisionId > 0) {
            $('#divPolicySettings').hide();
            var isSUDivision = $('#hdnSUDivision').val();
            if (isSUDivision == null || isSUDivision == '0' || isSUDivision.length==0) {
                $('#ancBackDivisionsList').text('Go back to My Settings');
                $('#ancSettingName').html('Division Settings');
                $('#goBackToGlobalSettings').show();
            }
        }

        
        _isSUCompany = $('#hdnSUCompany').val();
        if (_isSUCompany != null && _isSUCompany == '1' && isSUDivision != null && isSUDivision != '1') {

            //$('#divSubscription').show();
            //$('#divSubscriptionStartDate').show();
            //$('#divSubscriptionStartEnd').show();
            //$('#divSubscriptionNotify').hide();

            var $strtDate = $('#body_Adminbody_txtSubscriptionStart');
            $strtDate.datepicker({              
            });

            var $endDate = $('#body_Adminbody_txtSubscriptionEnd');
            $endDate.datepicker({
            });
        }


    }
}

//service fail
function UserInformationGetFail() {
    alert('Service failed.');
}