﻿function convertAdminDateFormat(Original) {
    var dateFormat = '';
    if (Original != null) {
        switch (Original) {

            case 'dd/MM/yyyy':
                return dateFormat = 'dd/mm/yy';

            case 'MM/dd/yyyy':
                return dateFormat = 'mm/dd/yy';


            case 'MMM dd yyyy':
                return dateFormat = 'M dd yy';

            //case 'dd-MM-yy':
            //    return dateFormat = 'dd-mm-y';


            //case 'dd-M-yy':
            //    return dateFormat = 'dd-m-y';

            //default:
            //    return dateFormat = 'mm/dd/yyyy';

        }
    }

}

function fromDatefrmatToDate(dateValue)
{

    var dateDisplayFormat = localStorage.dateFormat;
  

    if (dateValue != null) {
        switch (dateDisplayFormat) {

            case 'dd/mm/yy':
                {
                    var dateSplit = dateValue.toString().split('/');
                    return new Date(dateSplit[2], dateSplit[1]-1, dateSplit[0]);
                }
            

            case 'mm/dd/yy':
                {
                    var dateSplit = dateValue.toString().split('/');
                    return new Date(dateSplit[2], dateSplit[0] - 1, dateSplit[1]);
                }



            case 'M dd yy':
                {
                    var dateSplit = dateValue.toString().split(' ');
                    return new Date(dateValue);
                }
       

        }
    }
}
function FormatDate(dateValue,format) {
    
   // var today = new Date(dateValue);
    var dd = dateValue.getDate();
    var mm = dateValue.getMonth() + 1; //January is 0!
    var yy = dateValue.getFullYear().toString().substr(2, 2);
    var month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var MMM = month_names_short[dateValue.getMonth()];
    var yyyy = dateValue.getFullYear();


    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
   // var dateValue = mm + '/' + dd + '/' + yy;

    var exampleFormat = format == '' ? '' :
        format == 'dd/MM/yyyy' ? dd + '/' + mm + '/' + yyyy :
        format == 'MM/dd/yyyy' ? mm + '/' + dd + '/' + yyyy :
        format == 'MMM dd yyyy' ? MMM+' '+dd+' '+yyyy : '';
    return exampleFormat;
}