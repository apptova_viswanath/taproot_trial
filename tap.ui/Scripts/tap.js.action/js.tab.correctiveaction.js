﻿/*Corrective Actions Screen Methods (usha)*/

//ready function
$(document).ready(function () {  
   
    //open popup onclick 
    $('#lnkCreateCap').click(function () {
        
        _isFromHomepage = false;
        OpenCorcetiveAction(0, false);
        return false;

    });   
   
    //Load data to page
    LoadPage();

    GetTMAccessWithEventId(_eventId, 'divTabCorrectiveAction');

});

var _taskId = '';
var _isFromHomepage = false;




//open Smart Corrective Action popup
function OpenCorcetiveAction(correctiveActionID) {

    localStorage.url = window.location.href.toString();
    var pageTitle = ((correctiveActionID == 0) ? 'Create Corrective Action' : 'Edit Corrective Action');
    var commandURL = ((correctiveActionID == 0) ? 'Create' : 'Edit');
    var url = _baseURL + 'CorrectiveActionPlan/' + commandURL + '/' + _eventId + '/' + correctiveActionID;
   
    window.location = url;

}


//Page load
function LoadPage() {

     //get query strings from url
    GetQueryStringFromUrl();

    //Jquery main Tabs
    JqueryTabs();

    // highlighting selected Tab
    TabColor(location, 2);

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });

    $('#spnIdentifier').html('');

    //Populate cap related data
    PopulateCorrectiveActions();
}

function GetQueryStringFromUrl()
{
    var splittedURL = window.location.href.split('/');
    var event = splittedURL[splittedURL.length - 1]
    var _subtitle = splittedURL[splittedURL.length - 2]
    //check task id is there or not
    if (event.lastIndexOf('?') != '-1') {
       
        //from home page task link
        _taskId = event.substring(event.lastIndexOf('?') + 1);
        _eventId = event.split('?')[0];

        if (_eventId.lastIndexOf('#') != '-1') {
            _eventId = _eventId.replace('#', '');
        }
        _eventId = ((_eventId == 0) ? -1 : _eventId);

        var evType = splittedURL[splittedURL.length - 3];

        _eventType = ((evType == 'Incident' || evType == 'New-Incident') ? 1
                     : (evType == 'Investigation' || evType == 'New-Investigation') ? 2
                     : (evType == 'Audit' || evType == 'New-Audit') ? 3
                     : (evType == 'CAP' || evType == 'CorrectiveActionPlan' || evType == 'Corrective-Action-Plan') ? 4 : '');

        //get correctiveActionId and isSmarter to open popup page.
        GetCorrectiveActionDetails(_taskId);

    
    }
    else {

        //from home page grid/tabs
        EventTypeFromUrlSelect(3, 1);

    }
}

function GetCorrectiveActionDetails(taskId)
{
    var correctiveActionData = correctiveActionDetails(taskId);
    var data = JSON.stringify(correctiveActionData);
   
    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetCorrectiveActionInfo';
    PostAjax(serviceURL, data, eval(GetCorrectiveActionInfoSuccess), eval(GetCorrectiveActionInfoFail));
}

function correctiveActionDetails(taskId)
{
    var capData = {};
    capData.taskId = taskId;
    return capData;
}

//on success- pass CAID and isSmarter to open popup page 
function GetCorrectiveActionInfoSuccess(result)
{
  
    var jsonData = jQuery.parseJSON(result.d);

    if (jsonData.length > 0) {
      
        OpenEditCorrectiveActionPopup(jsonData[0].CorrectiveActionId, jsonData[0].isSmarter,true);
    }
   
}

function GetCorrectiveActionInfoFail()
{
    alert('Service Fail.');
}


//Populate cap related data
function PopulateCorrectiveActions() {
    
    var capData = {};
    capData.eventID = _eventId;
    capData = JSON.stringify(capData);
    var serviceURL = _baseURL +  'Services/CorrectiveAction.svc/GetCorectiveActionByEventId';
    AjaxPost(serviceURL, capData, eval(LoadDataSuccess), eval(LoadDataFail));
    
}


//On success of Load Data
function LoadDataSuccess(data) {
      
    //Clear the template
    $("#results").empty();

    //Get the result data
    data = ParseToJSON(data.d);

    //Append to the template
    $("#capTemplate").tmpl(data).appendTo("#results");

}


//On fail of Load Data
function LoadDataFail(result) {
}


//Open edit corrcteive action
function OpenEditCorrectiveActionPopup(correctiveActionID, isSmarter, isFromHomepage) {
    
    // to check from Home Page task links or on click of edit CAP button
    _isFromHomepage = isFromHomepage;
    _currentCAPID = correctiveActionID;

    OpenPopup(correctiveActionID, isSmarter);

}


//Open corrcetive action pop up
function OpenPopup(correctiveActionID, isSmarter) {
    var url = '';
    var pageTitle = ((correctiveActionID == 0) ? 'Create Corrective Action' : 'Edit Corrective Action');
    var commandURL = ((correctiveActionID == 0) ? 'Create' : 'Edit');

    if (isSmarter == 'true') {

        url = jQuery(location).attr('href');
        var splitUrl = url.split('/');
        url = _baseURL + splitUrl[splitUrl.length - 4] + '/' + splitUrl[splitUrl.length - 3] + '/' + splitUrl[splitUrl.length - 2] + '/SMARTER/' + _eventId + '/' + correctiveActionID;
        window.location = url;

    }
    else {
       
        
        url = (_isFromHomepage) ?  _baseURL + 'CorrectiveActionPlan/' + commandURL + '/' + _eventId + '/' + correctiveActionID + '?' + _taskId
                                : _baseURL + 'CorrectiveActionPlan/' + commandURL + '/' + _eventId + '/' + correctiveActionID;
        window.location = url;
    }

       
}

