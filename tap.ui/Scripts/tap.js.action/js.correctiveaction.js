﻿/*Edi Correctiv Actions Screen Methods (usha)*/


//ready function
$(document).ready(function () {

    SetSubTabID();


    //var formtitle = $('form').attr('action');
    var urlSplit = window.location.href.split('?');

    if (urlSplit.length > 1) {
        var splittedURL = urlSplit[urlSplit.length - 2].split('/');
        var AddTaskFromHome = urlSplit[urlSplit.length - 1].split('/');
        var taskId = AddTaskFromHome[AddTaskFromHome.length - 1];
    }
    else {
        var splittedURL = urlSplit[urlSplit.length - 1].split('/');
    }
    
    var formTitle = splittedURL[splittedURL.length - 3];
    var isCreatePage = (formTitle == 'Create');

   // var isFromHomePage = AddTaskFromHome[AddTaskFromHome.length - 1];
    BindCommonFieldsAndEvents(isCreatePage, false);

    //Populate causal factor
    //parameter getting passed to recognize if the causal factor  is populated for the tab page or for create/edit cap
    PopulateCausalFactors(true, true, false);


    //If cap page
    LoadCAPDetails();  
   
   
   // var isFromHomePage = window.parent._isFromHomepage;// to check from Home Page task links or on click of edit CAP button
    if (taskId != undefined && taskId != "") { 
        OpenTaskPopup(taskId);
    }
});


//Display dictionary pop up
function DisplayPopUp(ctrl, rctTitle) {
    
    $(ctrl).find('#header').hide();
    $(ctrl).find('.topicheading').hide();

    $('#EfNiImg1').attr('src', _baseURL + 'Images/CAHelp/image001.gif');
    $('#EfNiImg2').attr('src', _baseURL + 'Images/CAHelp/image002.gif');
    $('#EfNiImg3').attr('src', _baseURL + 'Images/CAHelp/image003.gif');
    $('#EfNiImg4').attr('src', _baseURL + 'Images/CAHelp/image004.gif');
    //spanish
    $('#EsEfNiImg1').attr('src', _baseURL + 'Images/CAHelp/Spanish/image001.gif');
    $('#EsEfNiImg2').attr('src', _baseURL + 'Images/CAHelp/Spanish/image002.gif');
    $('#EsEfNiImg3').attr('src', _baseURL + 'Images/CAHelp/Spanish/image003.gif');
    $('#EsEfNiImg4').attr('src', _baseURL + 'Images/CAHelp/Spanish/image004.gif');
    //Russina
    $('#RuEfNiImg1').attr('src', _baseURL + 'Images/CAHelp/Russian/image001.gif');
    $('#RuEfNiImg2').attr('src', _baseURL + 'Images/CAHelp/Russian/image002.gif');
    $('#RuEfNiImg3').attr('src', _baseURL + 'Images/CAHelp/Russian/image003.gif');
    $('#RuEfNiImg4').attr('src', _baseURL + 'Images/CAHelp/Russian/image004.gif');

    $(ctrl).find('a').each(function () {

        var anchorText = $(this).html().replace(/(\r\n|\n|\r)/gm, "").replace(/ +/g, "");
       
        if (anchorText == 'RootCauseTree®Dictionary') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16,id.length);
                DisplayPopUp('#divRCTDictionary' + id, rctTitle);
                ClosePoUp('#divCAPDictionary' + id);
            });
        }

        if (anchorText == 'CorrectiveActionHelper®LiabilityDisclaimer') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                return false;
            });
        }

        if (anchorText == 'CorrectiveActionHelper®'){
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16, id.length);
                DisplayPopUp('#divCAPDictionary' + id, rctTitle);
                ClosePoUp('#divRCTDictionary' + id);
            });
        }
      

    });


      $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        draggable:true,
        position: ['middle', 100],
        title: rctTitle

    });

      $("ctrl").siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');

      return false;
}


function ClosePoUp(dialog)
{
    $(dialog).hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $(dialog).dialog("destroy");
    $(dialog).bind("dialogclose", function (event, ui) {
    });
}

//////////////////////////////CAP HELPER CONTENT//////////////////////

//////////////////////////

//Display guidance pop up, on click of bulb icon from each RCT node
function DisplayCAPGuidancePopUp(rootCuaseId, linkType, rootCause) {
    
    //Declare the parameter object
    var rctData = {};
    
    rctData.rootCauseID = parseInt(rootCuaseId);
    rctData.linkType = linkType;
    rctData.userId = _userId
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetRCTGuidance';
    AjaxPost(serviceURL, rctData, eval(GetRCTGuidanceSuccess), eval(FailedCallback));

}


// This is the callback function invoked if the Web service  failed. It accepts the error object as a parameter.
function FailedCallback(error) {

    // Display the error
    $('#UserMessage').innerHTML = "Service Error: " + error.get_message();
    $('#UserMessage').show();

}

//On success of getting RCT guidance for any rct node, display the content in a pop up div
function GetRCTGuidanceSuccess(data) {
    var germanLanguage = 'de';
    var frenchLanguage = 'fr';
    var spanishLanguage = 'es';


    //parse the json result to object
    data = ParseToJSON(data.d);

    var selectedLanguage = data[0]["RCTLanguageName"];


    var ctrl = document.createElement('div');
    $(ctrl).attr('id', 'divDic');
    $(ctrl).attr('title', data[0].Title);

    // Get Title
    var title = '';
    if (data[0].Title.split(':')[1] != undefined) {
        title = data[0].Title.split(':')[1];
    }

    $(ctrl).html(data[0].Guidance);
  

    if (selectedLanguage != germanLanguage && selectedLanguage != frenchLanguage && selectedLanguage != spanishLanguage) {
        $(ctrl).removeClass('notranslate');
    }
    else {

        $(ctrl).addClass('notranslate');
    }

    //Display dialog
    //DisplayDialog(ctrl, title);

    $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        position: ['middle', 100],
        title: title

    });


    // Set click events for each anchor tag present inside the light bulb content
    SetClickEventForAnchorTags(ctrl);



    $(ctrl).find('span').each(function () {
        $(this).attr('style', '');
    });

}