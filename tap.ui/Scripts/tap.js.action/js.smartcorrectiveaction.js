﻿
/*Smart Corrective Action  Screen Methods (usha)*/

$(document).ready(function ()
{
    _isShareTab = false;
    SetSubTabID();
    
    //_dirtyMsgText = "This SMARTER Corrective Action has not been saved. Do you want to save it now?";
    _dirtyMsgText = "Are you sure you want to leave this page and lose your unsaved SMARTER Corrective Action changes?";
  
    //var formTitle = window.location.href.split('/')[length - 3];
    //var isCreatePage = (formTitle == 'Create');
    var length = window.location.href.split('/').length;
    var capID = window.location.href.split('/')[length - 1];
    var isCreatePage = (capID > 0) ? false : true;

    if (isCreatePage) {

        //$("#accordionCss").attr("disabled", "disabled");

        $("#sec_timely").prependTo("#sec_measurable");
        $("#sec_accountable").prependTo("#sec_measurable");
        $("#sec_reasonable").prependTo("#sec_measurable");

    }
    else {
       

        //To display the detail on header click
        $('#sec_specfic,#sec_measurable,#sec_accountable,#sec_reasonable,#sec_timely,#sec_effective,#sec_reviewed').click(function () {
            ShowHideContent(this.id);
        });

        $("#sec_specfic").appendTo("#divSpecific");
        $('#specificHeader,#measurableHeader,#accountableHeader,#reasonableHeader,#timelyHeader,#effectiveHeader,#reviewedHeader').show();

    }

    BindCommonFieldsAndEvents(isCreatePage, true);
    
    //Populate causal factor
    //parameter getting passed to recognize if the causal factor  is populated for the tab page or for create/edit cap
    PopulateCausalFactors(true, false, true);

    LoadPageForSmartCAP();

    LoadCAPDetails();
  
});

//Load page for smart corrcetive action
function LoadPageForSmartCAP() {

    $("#txtEffectiveDueDate").datepicker({ dateFormat: localStorage.dateFormat });
    $("#txtTimelyDueDate").datepicker({ dateFormat: localStorage.dateFormat });
    $("#txtMInitialDueDate").datepicker({ dateFormat: localStorage.dateFormat });

    $("#ui-datepicker-div").addClass("notranslate");

    //Populate responsible person data
    PopulateResponsiblePerson(_baseURL);

}


//Populate responsible person
function PopulateResponsiblePerson(_baseURL) {
    if (parseInt($('#hdnUserId').val()) > 0) {
        //Declare the parameter object
        var userData = {};
        userData.userId = _userId
        userData = JSON.stringify(userData);

        //Call the service
        var serviceURL = _baseURL + 'Services/Users.svc/GetUserFullName';
        AjaxPost(serviceURL, userData, eval(setPopulateResponsiblePerson), eval(setPopulateResponsiblePerson));
    }
    else
    {
        setPopulateResponsiblePerson(false);
    }
}


function setPopulateResponsiblePerson(data) {
    var userName = $('#hdnUserName').val();

    if (data.d != null && data.d != "" && data.d != undefined && data.d != '')
    {
        userName = data.d; //+ ' (' + userName + ')';
    }

    //add default user name for SU
    if ($('#spnMPersonResponsible')[0].innerHTML == "" || $('#spnMPersonResponsible')[0].innerHTML == "" || $('#spnMPersonResponsible')[0].innerHTML == undefined)
        $('#spnMPersonResponsible')[0].innerHTML = userName;
    if ($('#spnEPersonResponsible')[0].innerHTML == "" || $('#spnEPersonResponsible')[0].innerHTML == "" || $('#spnEPersonResponsible')[0].innerHTML == undefined)
        $('#spnEPersonResponsible')[0].innerHTML = userName;
    if ($('#spnAPersonResponsible')[0].innerHTML == "" || $('#spnAPersonResponsible')[0].innerHTML == "" || $('#spnAPersonResponsible')[0].innerHTML == undefined)
        $('#spnAPersonResponsible')[0].innerHTML = userName;
}

//Close smart corrcteive action pop up
function CloseSmartCAPPopUp() {
   
    parent.$("#dialog-smartcap").dialog('close');
   
    if (parent.$("#hdnStatusValue").val() == 'true') {

        parent.NotificationMessage('Data saved successfully.', 'jSuccess', true, 1000);
        parent.$("#hdnStatusValue").val('false');

    }
}


//To display the detail on header click using Toggle Event
function ShowHideContent(controlId)
{
    $('#' + controlId + 'Content').slideToggle(500);
}



//Display dictionary pop up
function DisplayPopUp(ctrl, rctTitle) {
    
    $(ctrl).find('#header').hide();
    $(ctrl).find('.topicheading').hide();

    $('#EfNiImg1').attr('src', _baseURL + 'Images/CAHelp/image001.gif');
    $('#EfNiImg2').attr('src', _baseURL + 'Images/CAHelp/image002.gif');
    $('#EfNiImg3').attr('src', _baseURL + 'Images/CAHelp/image003.gif');
    $('#EfNiImg4').attr('src', _baseURL + 'Images/CAHelp/image004.gif');
    //spanish
    $('#EsEfNiImg1').attr('src', _baseURL + 'Images/CAHelp/Spanish/image001.gif');
    $('#EsEfNiImg2').attr('src', _baseURL + 'Images/CAHelp/Spanish/image002.gif');
    $('#EsEfNiImg3').attr('src', _baseURL + 'Images/CAHelp/Spanish/image003.gif');
    $('#EsEfNiImg4').attr('src', _baseURL + 'Images/CAHelp/Spanish/image004.gif');
    //Russina
    $('#RuEfNiImg1').attr('src', _baseURL + 'Images/CAHelp/Russian/image001.gif');
    $('#RuEfNiImg2').attr('src', _baseURL + 'Images/CAHelp/Russian/image002.gif');
    $('#RuEfNiImg3').attr('src', _baseURL + 'Images/CAHelp/Russian/image003.gif');
    $('#RuEfNiImg4').attr('src', _baseURL + 'Images/CAHelp/Russian/image004.gif');
    $(ctrl).find('a').each(function () {

        var anchorText = $(this).html().replace(/(\r\n|\n|\r)/gm, "").replace(/ +/g, "");

        if (anchorText == 'RootCauseTree®Dictionary') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16, id.length);
                DisplayPopUp('#divRCTDictionary' + id, rctTitle);
                ClosePoUp('#divCAPDictionary' + id);
            });
        }

        if (anchorText == 'CorrectiveActionHelper®LiabilityDisclaimer') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                return false;
            });
        }

        if (anchorText == 'CorrectiveActionHelper®') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16, id.length);
                DisplayPopUp('#divCAPDictionary' + id, rctTitle);
                ClosePoUp('#divRCTDictionary' + id);
            });
        }


    });


    $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        position: ['middle', 100],
        title: rctTitle

    });

    $("ctrl").siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');

    return false;
}


function ClosePoUp(dialog) {
    $(dialog).hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $(dialog).dialog("destroy");
    $(dialog).bind("dialogclose", function (event, ui) {
    });
}

//////////////////////////////CAP HELPER CONTENT//////////////////////

//////////////////////////

//////////////////////////

//Display guidance pop up, on click of bulb icon from each RCT node
var rootCause = '';
function DisplayCAPGuidancePopUp(rootCuaseId, linkType, rootCauseDesc) {
    rootCause = rootCauseDesc;
    //Declare the parameter object
    var rctData = {};
    
    rctData.rootCauseID = parseInt(rootCuaseId);
    rctData.linkType = linkType;
    rctData.userId = _userId
    rctData = JSON.stringify(rctData);
    
    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetRCTGuidance';
    AjaxPost(serviceURL, rctData, eval(GetRCTGuidanceSuccess), eval(FailedCallback));
}


// This is the callback function invoked if the Web service  failed. It accepts the error object as a parameter.
function FailedCallback(error) {
    // Display the error
    $('#UserMessage').innerHTML = "Service Error: " + error.get_message();
    $('#UserMessage').show();
}


//On success of getting RCT guidance for any rct node, display the content in a pop up div+
function GetRCTGuidanceSuccess(data) {
    
    var germanLanguage = 'de';
    var frenchLanguage = 'fr';
    var spanishLanguage = 'es';
    
    //parse the json result to object
    data = ParseToJSON(data.d);
    var selectedLanguage = data[0]["RCTLanguageName"];

    var ctrl = document.createElement('div');
    $(ctrl).attr('id', 'divDic');
    //$(ctrl).attr('title', data[0].Title);
    $(ctrl).attr('title', rootCause);
    // clear the variable value
    rootCause = '';
    
    //// Get Title
    //var title = '';
    //if (data[0].Title.split(':')[1] != undefined) {
    //    title = data[0].Title.split(':')[1];
    //}
    $(ctrl).html(data[0].Guidance);
    
    var addMessage = '  <div class="warningYellowImage">' +
                      '<i><p class="notranslate" >You are viewing the English version of TapRooT®, translated by Google Translation.  This translation has not been sanctioned or reviewed by TapRooT®.</p></i><br />' +
                  '</div>';


    if ( selectedLanguage != spanishLanguage && selectedLanguage != 'ru' && selectedLanguage != 'undefined' && selectedLanguage != 'en') {
        $(ctrl).html(addMessage + $(ctrl).html());

    }
    else {
        $(ctrl).html($(ctrl).html());

    }



    if ( selectedLanguage != spanishLanguage && selectedLanguage != 'ru') {

        $(ctrl).removeClass('notranslate');
    }
    else {

        $(ctrl).addClass('notranslate');
    }
  
    // Remove the anchor tags from the content description
    if ($(ctrl).find('a').length > 0)
        $(ctrl).find('a').replaceWith(function () { return this.innerHTML; });
        
  

    $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        position: ['middle', 100],
        //title: title,
        closeOnEscape: false,
        open: function () {
            if ($('#divDic').scrollTop() != 0)
                $('#divDic').scrollTop(0);
        },
        close: function () {
            $('#divDic').remove();
        }
    });


    // Set click events for each anchor tag present inside the light bulb content
    if (typeof SetClickEventForAnchorTags == 'function')
        SetClickEventForAnchorTags(ctrl);


    $(ctrl).find('span').each(function () {
        $(this).attr('style', '');
    });

}