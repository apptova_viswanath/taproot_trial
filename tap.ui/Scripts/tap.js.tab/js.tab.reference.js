﻿/*Reference Screen Methods (usha)*/

//ready function
$(document).ready(function () {

    //page load method
    PageLoadAction();


});
//end ready function


//Page load
function PageLoadAction() {

    //Jquery main Tabs
    JqueryTabs();

    // highlighting selected Tab
    TabColor(location, 2);


    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _eventId = splitUrl[splitUrl.length - 1];
    _module = splitUrl[splitUrl.length - 3];

    //GetTeamMemberAccess('divTabReference');
    GetTMAccessWithEventId(_eventId, 'divTabReference');

    UpdateStepText();

    $('a.snp').attr('href', 'javascript:void(0)');
    $('.wntSnp').attr('href', 'javascript:void(0)');

    $('a.snp').click(function (evt) {
        return OpenSnapChartPage(SnapchartUrl(false));
    });
   
    var fixURL = $('#ulSubTabs').find("a:contains('Fix')").attr('href');
    $('.capHelper').attr('href', fixURL);
    

    EnableOrDisableWinterLink();
}

//get Investigation season
function EnableOrDisableWinterLink() {

    var rctData = {};

    //Pass the parameters
    rctData.eventId = _eventId;
    rctData = JSON.stringify(rctData);
    _serviceUrl = _baseURL + 'Services/RootCause.svc/AnyRCTComplete';
    AjaxPost(_serviceUrl, rctData, eval(AnyRCTCompleteSuccess), eval(AnyRCTCompleteFail));
}


function AnyRCTCompleteSuccess(result) {
   
    if (result != null && result.d != null) {

        if (result.d == false) {                       
            $('.wntSnp').addClass('disable');
        }
        else {
            $('.wntSnp').removeClass('disable');
            $('a.wntSnp').click(function (evt) {
                return OpenSnapChartPage(SnapchartUrl(true));
            });
        }
    }
}


function AnyRCTCompleteFail() {
 
}

//function GetPlanStepText(module) {

//   return (module == 'Investigation') ? 'Plan your investigation using Spring SnapCharT®.' : 'Plan your audit using Spring SnapCharT®.';
//}

//function GetPlanOptionalText(module) {

//    return (module == 'Investigation') ?
//    'Reference the Root Cause Tree® to guide data collection and determine what questions to ask. <br>Use Equifactor® to develop your equipment troubleshooting plan'
//    :'Reference the Root Cause Tree® or Equifactor(r) to guide data collection and determine what questions to ask.';

//}

//function GetInvestigateStepText(module) {
//    return (module == 'Investigation') ? 'Determine the sequence of events using Summer SnapCharT®.' : 'Determine the process flow using Summer SnapCharT®.';
//}

//function GetInvestigateOptionalText(module) {
//    return 'Use Equifactor®, CHAP®, or Change Analysis® to add events and conditions to the SnapCharT®';
//}


//function AnalyzeStep3Text(module) {
//    return (module == 'Investigation') ? 'Define Causal Factors using  Autumn SnapCharT®' : 'Define Significant Issues using  Autumn SnapCharT®';
//}

//function AnalyzeStep4Text(module) {
//    return (module == 'Investigation') ? 'Analyze each Causal Factor for root causes using the Root Cause Tree®.' : 'Analyze each Significant Issue for root causes using the Root Cause Tree®';
//}

//function AnalyzeStep5Text(module) {
//    return (module == 'Investigation') ? 'Define Causal Factors using  Autumn SnapCharT®' : 'Define Significant Issues using  Autumn SnapCharT®';
//}