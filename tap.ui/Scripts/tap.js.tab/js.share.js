﻿


//Global Variables

var _serviceUrl;
var _searchTeamUser = '';
var _teamUserId = '';
var _eventId = -1;
var _companyId = -1;
var _userId = -1;
var isDisplayOnReportForTU = false;
var _isSUCompany = '';
var _isSUDivisionId = '';
var _isShareTab = true;
//Page load Events
$(document).ready(function () {
   
    $("#imgAddTeamMember").attr("src", _baseURL + 'Images/add.png');
    $("#imgAddTeamMemberManually").attr("src", _baseURL + 'Images/add.png');

    _isSUCompany = $('#hdnSUCompany').val();
    _isSUDivisionId = $('#hdnSUDivision').val();
    _isShareTab = true;

    //on load code
    SetPageLoad();
    //for color Change in Subtab
    TabColor(location, 3);

    //Jquery main Tabs
    JqueryTabs();
    _companyId = GetCompanyId();

    //if SU company then change title and placeholder.  
    if (_isSUCompany == "1" || _isSUDivisionId=='1')
    {
        $('#txtSearchTeamUser')[0].title = 'Type Name';
        $('#txtSearchTeamUser')[0].placeholder = 'Type Name';
    }
       

});

//on page load Code
function SetPageLoad() {

    SetEventId();

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 3);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 3);
    });

    $('#btnAddTeamMember').click(function () {
        AddTeamMember();
    });
    $('#btnCancelTeamMember').click(function () {
        CloseAddTeamMemberPopup();
    });

    $('#addTeamMemberManually').click(function () {
        AddTeamMemberManually();

    });


    $('#ancAddTeamMember').click(function () {
        OpenAddTeamUserPopup('share');
        return false;
    });

    $('#ui-icon-closethick').click(function () {
        CloseAddTeamUserPopup();
    });
  

    //hide the grid error link
    $("#ReloadGrid").hide();
  
    //team members grid
    BindShareListGrid();
   

    $("input:checkbox").click(function () {
        if ($(this).is(":checked")) {
            var name = $(this).attr('name');
            if (name != null && name.length > 0) {
                $('input[name=chkTM]').prop('checked', false);
                $(this).attr("checked", "checked");
            }
        } else {
            $(this).prop('checked', false);
        }
    });

    _userId = GetUserId();
    GetTeamMemberAccess('divShareTab');
}

function SetEventId() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var evType = splitUrl[splitUrl.length - 4];

    _eventId = splitUrl[splitUrl.length - 2];
}



// Team Member Reload Grid
function ReloadGridData() {
    jQuery("#ShareListGrid").trigger("reloadGrid");
}


//to get Share Lists
function GetTeamMemberLists(parameterData) {
    
    var shareListData = new Object();
    shareListData.page = parameterData.page;
    shareListData.rows = parameterData.rows;
    shareListData.sortIndex = parameterData.sidx;
    shareListData.sortOrder = parameterData.sord;

    shareListData.companyId = _companyId;
    shareListData.eventId = _eventId;

    data = JSON.stringify(shareListData);
    _serviceUrl = _baseURL + 'Services/TeamMembers.svc/GetTeamMemberList';
    AjaxPost(_serviceUrl, data, eval(GetShareListsSuccess), eval(GetShareListsFail));
}

// System List Success
function GetShareListsSuccess(result) {
    
    var errorMessage = '';
    var errorMessageType = 'jError';
    var gridData = jQuery("#ShareListGrid")[0];
    gridData.addJSONData(JSON.parse(result.d));
    var resultCount = ParseToJSON(result.d);

    if (resultCount.rows.length == 0) {

        $("#ReloadGrid").show();
        errorMessage = 'There are no team members available.';
        errorMessageType = 'jError';
    }

}

//service fail
function GetShareListsFail() {
    alert('Service failed.');
}

//Team Member List Grid Binding
function BindShareListGrid() {

    jQuery("#ShareListGrid").jqGrid({

        datatype: function (parameterData) {
            GetTeamMemberLists(parameterData);
        },
        colNames: ['<img src="' + _baseURL + 'Scripts/jquery/images/user-icon.png">', 'Name', 'View', 'Edit', 'Display on Report', 'Remove', 'isManualAdd', 'isOwner'
        ],
        colModel: [
            { name: 'Icon', index: 'Icon', width: 50, align: 'center', editable: false,formatter: usericonFmatter, sortable: false, resizable: true },
            { name: 'Name', index: 'FullName', width: 150, align: 'left', editable: false, resizable: true },
            { name: 'View', width: 50, align: 'center', index: 'hasView', editable: true, formatter: userChkFormatter, resizable: true },
            { name: 'Edit', index: 'hasEdit', width: 50, align: 'center',  formatter: userChkFormatter, resizable: true },
            { name: 'Display on Report', index: 'isDisplayOnReport', width: 100, align: 'center', resizable: false, formatter: userChkFormatter, resizable: true },
            { name: 'options', index: 'options', width: 50, align: 'center', editable: false, classes: "HandCursor", sortable: false, resizable: true },
            { name: 'isManualAdd', index: 'isManualAdd', width: 40, align: 'center', editable: false, hidden: true, resizable: true },
            { name: 'isOwner', index: 'isOwner', width: 40, align: 'center', editable: false, hidden: true, resizable: true }
        ],
        gridview: false,
        rowattr: function (rd) {
            if (rd.isOwner == "True") {
                //return { "class": "ui-state-disabled" };
            }
        },
        toppager: false,
        emptyrecords: 'There are no Team Members.',
        // Grid total width and height        
        autowidth: true,
        rowNum: 10,
        afterInsertRow: function (rowid, rowdata, rowelem) {
         //show delete icon and if login user then hide delete icon
            if (rowdata.isOwner=='False') {
              
                DeleteButton = " <img  id='ShareDelete" + rowid + "' style='margin-top:2px; height:10px;margin-left: -5px;' src='" + _baseURL + "images/CheckRed.gif' title='Remove Team Member' onClick='Delete(" + rowid + ", event);'/>";
                jQuery("#ShareListGrid").setRowData(rowid, { options: DeleteButton });

            }         
           
            _isSUCompany = $('#hdnSUCompany').val();
            var isSUDivision = $('#hdnSUDivision').val();
            //hide the Edit,View and Display on Reports Columns in Grid for SU company
            if ((_isSUCompany != null && _isSUCompany == "1") || (_isSUDivisionId != null && _isSUDivisionId == "1")) {
                $('#ShareListGrid').jqGrid('hideCol', "Edit");
                $('#ShareListGrid').jqGrid('hideCol', "View");
                $('#ShareListGrid').jqGrid('hideCol', "Display on Report");
                $(".ShareListGrid").css('width', '50%');

            }
        },
        hidegrid: false,
        pginput: "false",
        height: '100%',
        caption: '<span class="caption">People</span>',
        pager: jQuery('#SharepageNavigation'),
        viewrecords: true,
        sortname: 'FullName',
        sortorder: "asc",
        cmTemplate: { title: false },
      
    });

    //for zoom / Ctrl ++ - screen should fit within the page only
    $(window).bind('resize', function () {

        $("#ShareListGrid").setGridWidth($('.ShareListGrid').width());
    });

    
} //grid end





function userChkFormatter(cellValue, options, rowObject) {
    
    var isManual = rowObject[6];
    if (isManual == 'True' && (options.pos == 2 || options.pos == 3)) {
            return "<input type='checkbox' disabled='disabled' />";
    }

    if (options.pos == 3) {

        if (cellValue == 'False')
            return "<input type='checkbox' class='tmChkEditCls' onclick=\"TMClickOnChk('" + options.rowId + "' , " + options.pos + ", this, event);\"/>";

        if (cellValue == 'True')
            return "<input type='checkbox' class='tmChkEditCls' onclick=\"TMClickOnChk('" + options.rowId + "' , " + options.pos + ", this, event);\" checked/>";
    }

    if (cellValue == 'False')
        return "<input type='checkbox' onclick=\"TMClickOnChk('" + options.rowId + "' , " + options.pos + ", this, event);\"/>";

    if (cellValue == 'True')
        return "<input type='checkbox' onclick=\"TMClickOnChk('" + options.rowId + "' , " + options.pos + ", this, event);\" checked/>";
}

function TMClickOnChk(rowId, ind, sender, event) {

    if ($(sender).parent().parent().hasClass("ui-state-disabled")) {
        event.preventDefault();
        return false;
    }

    UpdateTMRealTime(rowId, ind, sender, event);
    return;
}

function UpdateTMRealTime(rowId, ind, sender, event) {
    
    var val = $(sender).attr('checked');
    var record = jQuery("#ShareListGrid").jqGrid('getGridParam', 'records');
    var edit = $("tr.jqgrow > td > input.tmChkEditCls", jQuery("#ShareListGrid")[rowId]);

    if (ind == 2 && val != null && val != 'undefined') {
        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(2) > input").attr("checked", "checked");
    }
    if (ind == 3 && val != null && val != 'undefined') {
        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(2) > input").removeAttr("checked");
        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").attr("checked", "checked");
    }
   
    if (!edit.is(':checked')) {
        var errorMessage = 'There must be at least one user with Edit access.';
        var errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);

        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(2) > input").removeAttr("checked");
        $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").attr("checked", "checked");

        event.preventDefault();
        return false;
    }
    if (record == 1 && ind == 2) {
        event.preventDefault();
        return false;
    }

    

    var isEdit = false;
    var isView = false;
    var isDisplayOnReport = false;

    if (ind == 4) {
        if (val != null && val != 'undefined') {
            isDisplayOnReport = true;
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").attr("checked", "checked");
        }
        else {
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").removeAttr("checked");
        }
    }
    else {
        var displayOnReport = $('#ShareListGrid').getCell(rowId, 4);
        isDisplayOnReport = $(displayOnReport).is(':checked');
    }

    if (ind == 3) {
        if (val != null && val != 'undefined') {
            isEdit = true;
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").attr("checked", "checked");
        }
        else {
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
        }
    }
    else {
        var edit = $('#ShareListGrid').getCell(rowId, 3);
        isEdit = $(edit).is(':checked');
    }
    if (ind == 2) {
        if (val != null && val != 'undefined') {
            isView = true;
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(2) > input").attr("checked", "checked");
        }
        else {
            $("#ShareListGrid  tr[id='" + rowId + "'] > td:eq(2) > input").removeAttr("checked");
        }
    }
    else {
        var view = $('#ShareListGrid').getCell(rowId, 2);
        isView = $(view).is(':checked');
    }

    
    if (isView == true || isEdit == true || isDisplayOnReport == true) {
        UpdateTM(rowId, isEdit, isView, isDisplayOnReport);

    } else {
        event.preventDefault();
        var errorMessage = 'Can not remove this setting. Team memeber should have atleast one privillege.';
        var errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
}

function UpdateTM(rowId, isEdit, isView, isDisplayOnReport) {
    var gridDetails = new SetDetailsUpdateTM(rowId, isEdit, isView, isDisplayOnReport);
    var _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/UpdateMember';
    AjaxPost(serviceURL, _data, eval(updateTmSuccess), eval(updateTMFails));
}

function updateTmSuccess(result) {
    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    if (result != null&& result.d != null && result.d.length>0) {
       
        errorMessage = result.d;
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        if (errorMessageType == 'jError')
            BindShareListGrid(); ReloadGridData();

    }
    
}

function updateTMFails() {
    alert('Service Failed.');
}

function SetDetailsUpdateTM(tmId, isEdit, isView, isDiplayOnReport) {
    this.tmID = tmId;
    this.isEdit = isEdit;
    this.isView = isView;
    this.isDisplayOnReport = isDiplayOnReport;
    this.eventID = _eventId;
}

function Delete(id, event) {
    
    //Validation before deletion of team member .
    /*First, temporarily remove the edit access of the selected team member and check , if any other team member has edit access
      if no other team members have edit access then do not delete the selected team member and
      reset the edit access to the selected team member.
    */
    var rowEdit = $('#ShareListGrid').getCell(id, 3);
    var rowHasEdit = $(rowEdit).is(':checked');
    if (rowHasEdit)
        $("#ShareListGrid  tr[id='" + id + "'] > td:eq(3) > input").removeAttr("checked");

    var edit = $("tr.jqgrow > td > input.tmChkEditCls", jQuery("#ShareListGrid")[id]);
    
    //--------------------------------------------------------------------------------------

    var name = jQuery('#ShareListGrid').jqGrid('getCell', id, 1);
    //var isOwner = jQuery('#ShareListGrid').jqGrid('getCell', id, 7); //kv

    var errorMessage = '';
    var errorMessageType = 'jError';

    //if (isOwner == 'True') { //kv
    if (!edit.is(':checked')) {
        errorMessage = 'There must be at least one user with Edit access.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        if (rowHasEdit)
            $("#ShareListGrid  tr[id='" + id + "'] > td:eq(3) > input").attr("checked", "checked");//reset the edit access to the selected team member
        return false;
    }

    if (rowHasEdit)
        $("#ShareListGrid  tr[id='" + id + "'] > td:eq(3) > input").attr("checked", "checked");//reset the edit access to the selected team member

    if (confirm("You are about to remove sharing from " + name + ". Are you sure you want to continue?")) {
        DeleteTM(id);
    }
}

function DeleteTM(tmId) {
    var gridDetails = new SetDetailsDelete(tmId);
    var _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/DeleteTeamMember';
    AjaxPost(serviceURL, _data, eval(deleteTmSuccess), eval(DeleteTMFails));
}

function deleteTmSuccess(result) {
    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    
    if (result != null) {
        errorMessage = result.d;
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        BindShareListGrid();
        ReloadGridData();

    }
}

function DeleteTMFails() {
    alert('Service Failed.');
}

function SetDetailsDelete(id) {
    this.tmID = id;
}

function makeTeamChkFormatter(cellValue, options, rowObject) {
    return "<a title='Share Team Member' style='color:blue; text-decoration:underline;' onclick=\"MakeTeamMemberOnChk('" + options.rowId + "' , " + options.pos + ", this);\"><img src='" + _baseURL + "Images/add.png'  alt='Share Team Member' style='height:10px;'/></a>";
}


function MakeTeamMemberOnChk(rowId, ind, sender) {
    _teamUserId = rowId;

    var val = $(sender).attr('checked');
    var col = '';
    if (ind == 3)
        col = 'View';
    else if (ind == 4)
        col = 'Edit';
    else if (ind == 5)
        col = 'DisplayOnReport';
    if (val != null && val != 'undefined')
        val = true;
    else
        val = false;

    SaveTeamMember(val, col);
    return false;
}

//Open Pop up page for Add team Member
function OpenAddTeamMemberPopup() {

    var pageTitle = '';
    $('input[name=chkTM]').prop('checked', false);

    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divMakeTeamMemberPopUp").dialog({
        title: pageTitle,
        height: 250,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
}



//close Add team Member Popup page.
function CloseAddTeamMemberPopup() {

    $('#divMakeTeamMemberPopUp').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divMakeTeamMemberPopUp").dialog("destroy");
    $('#divMakeTeamMemberPopUp').bind("dialogclose", function (event, ui) {

    });
}


function AddTeamMember(isEdit, isView, isDisplayOnReport) {
    
    var gridDetails = new SetDetailsAddTM(_teamUserId, isEdit, isView, isDisplayOnReport);
    var _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/AddTeamMember';
    AjaxPost(serviceURL, _data, eval(addTmSuccess), eval(addTMFails));

}

function UpdateTeamMember(isEdit, isView, isDisplayOnReport) {

    var gridDetails = new SetDetailsAddTM(_teamUserId, isEdit, isView, isDisplayOnReport);
    var _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/AddTeamMember';
    AjaxPost(serviceURL, _data, eval(addTmSuccess), eval(addTMFails));

}

function SaveTeamMember(val, col) {

    var gridDetails = new SetDetailsSaveTM(_teamUserId, val, col);
    var _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/SaveTeamMember';
    AjaxPost(serviceURL, _data, eval(saveTmSuccess), eval(saveTMFails));

}


function addTmSuccess(result) {
    
    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    if (result != null) {
        _teamUserId = '';
        errorMessage = result.d;
        NotificationMessage(errorMessage, errorMessageType, true, 3000);

        BindShareListGrid();
        ReloadGridData();

    }
}

function saveTmSuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    if (result != null) {
        _teamUserId = '';
        errorMessage = result.d;
        NotificationMessage(errorMessage, errorMessageType, true, 3000);

        BindShareListGrid();
        ReloadGridData();

    }
}

function addTMFails() {
    alert('Service Failed.');
}

function saveTMFails() {
    alert('Service Failed.');
}

function SetDetailsSaveTM(userId, val, col) {

    this.userID = userId;
    this.value = val;
    this.col = col;
    this.eventID = _eventId;
}

function SetDetailsAddTM(userId, isEdit, isView, isDiplayOnReport) {

    this.userID = userId;
    this.isEdit = isEdit;
    this.isView = isView;
    this.isDisplayOnReport = isDiplayOnReport;
    this.eventID = _eventId;
}

function AddTeamMemberManually() {

    var fullName = $('#txtSearchTeamUser').val().trim();
    if (fullName == null || fullName.length == 0)
        return;

    var details = new SetDetailsAddTMManually(fullName);
    var _data = JSON.stringify(details);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/AddTeamMemberManually';
    AjaxPost(serviceURL, _data, eval(addTMManuallySuccess), eval(addTMManuallyFails));
}

function addTMManuallySuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    if (result != null) {
        _teamUserId = '';
        errorMessage = result.d;
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        BindShareListGrid();
        ReloadGridData();
        CloseAddTeamUserPopup();
    }
}

function addTMManuallyFails() {
    alert('Service Failed.');
}

function SetDetailsAddTMManually(fullName) {

    this.fullName = fullName;
    this.eventId = _eventId;
}

















