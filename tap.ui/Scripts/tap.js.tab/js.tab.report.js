﻿/*Report Screen Methods (usha)*/

//ready function
$(document).ready(function () {

    //page load method
    PageLoadAction();

    //onclick of Custom Report 
    $('#lnkCustomReport').click(function () {
        
        RedirectCustomReport('Event');
    });

    $('#lnkReportUserTemplate').click(function () {

        RedirectCustomReport('User');
    });

    //on click of add Report - get all selected Report id's and create event Report
    $('#btnUserTemplateSelect').click(function () {
       
        GetReportIdsAndSaveEventReport();  
        
    });

    $("#lnkReportTemplate").click(function () {

        BindUserReportTemplates();

        OpenUserReportTemplate();
    })

    $('#btnUserTemplateCancel').click(function () {

        CloseUseATemplateDialog();
    });


});
//end ready function

//Global Variables
var _companyId = '';
var _svcURL = '';
var _virtualDirectoryPath = '';
var _eventId = '';
var _subTab = '';
var _module = '';
var _userId = '';
var _isWinter = false;
var _deleteReportType = '';


//Page load
function PageLoadAction() {
  
    //Jquery main Tabs
    JqueryTabs();

    // highlighting selected Tab
    TabColor(location, 2);

    GetSubTabId();

    //get company Id and service pathfrom js.com file
    _companyId = GetCompanyId();

    //get user Id and service pathfrom js.com file
    _userId = GetUserId();

    _svcURL = GetServicePath();

    _virtualDirectoryPath = GetVirtualDirectory();

    GetParameterFromUrl();//changed on 30th April

    try {
        //dispaly Report Titles-on click of each link -need to display report values(Generate Report)
        DisplayReportTitle();
    } catch (e) {  }
    


    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });   

    GetTMAccessWithEventId(_eventId, 'divTabReport');
    $('.anchorheader').removeAttr('disabled');
    
     //Show the Winter snapchart link.G
    //if (seasonId == "3" && isComplete == 'True') {   
    //    $('#imgAutumn').removeClass('seasonContentHide');
    //}
    if (isAllRCTCompleted == "True")
    {
        $('#imgAutumn').removeClass('seasonContentHide');
    }
    else {

        $('#imgAutumn').addClass('seasonContentHide');
    }
    if (isAnyRCTCompleted == "True") {
       
        $('#lnkWinter').removeClass('snapChartHide');
        _isWinter = true;
        if (seasonId == WINTER_SEASON) {
            $('#lnkWinter').html('Open Winter SnapCharT®');
        }      

    }
    else {
        $('#lnkWinter').addClass('snapChartHide');
        _isWinter = false;
    }
    
    SnapChartCompletedSuccess(seasonId);

    //Align the Analyze keyword properly.
    AnalyzeKeywordAlign();

    WinterSnapChartStatus();
}

//Align the Analyze keyword properly.
function AnalyzeKeywordAlign() {

    if (isAllRCTCompleted == "True") {
        $('#divAutumn').removeClass('snapChartDivPadding');
        $('#divAutumn').addClass('snapChartDivPaddingCompleted');
    }    
}

function GetReportIdsAndSaveEventReport()
{
    var arrTemplateReportIds = [];
    $('#divUserReportTemplate input:checked').each(function () {

        arrTemplateReportIds.push($(this).attr('id'));

    });

    $('#divSystemReportTemplate input:checked').each(function () {

        arrTemplateReportIds.push($(this).attr('id'));

    });

    //pass Report id's  array and create event Report
    CreateEventReport(arrTemplateReportIds);

}

//create event Report
function CreateEventReport(arrTemplateReportIds)
{
    
    var eventReportData = new GetEventReportDetails(arrTemplateReportIds);
    var data = JSON.stringify(eventReportData);
    var serviceURL = _baseURL + 'Services/CustomReport.svc/CreateEventReport';
    PostAjax(serviceURL, data, eval(CreateEventReportSuccess), eval(CreateEventReportFail));
}

//pass report id array list and company id and user id
function GetEventReportDetails(arrTemplateReportIds)
{
   
    this.arrTemplateReportIds = arrTemplateReportIds;
    this.companyId = _companyId;
    this.userId = _userId;
    this.eventID = _eventId;
}

//create Event Report success method
function CreateEventReportSuccess(result)
{
    window.location.reload();
}

//service failed
function CreateEventReportFail()
{
    alert('Service Failed.');
}


//close the dialog
function CloseUseATemplateDialog() {

    $('#divReportUserTemplateDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divReportUserTemplateDialog").dialog("destroy");
    $('#divReportUserTemplateDialog').bind("dialogclose", function (event, ui) {
    });
}

//get user report templates based on User and company
function BindUserReportTemplates()
{ 
    var userReportData = new GetUserReportDetails();
    var data = JSON.stringify(userReportData);
    var serviceURL = _baseURL + 'Services/CustomReport.svc/GetUserReportTemplates';
    PostAjax(serviceURL, data, eval(GetUserReportTemplatesSuccess), eval(GetUserReportTemplatesFail));
}

function GetUserReportDetails()
{
    this.companyId = _companyId;
    this.userId = _userId;
}

//User Report Template data success method.
function GetUserReportTemplatesSuccess(result)
{
       
    $("#userReportTemplateResults").empty();
    $("#systemReportTemplateResults").empty();
    var jsonData = jQuery.parseJSON(result.d);
      
    if (jsonData.length > 0) {        

        $("#userReportTemplate").tmpl(jsonData).appendTo("#userReportTemplateResults");
        $("#systemReportTemplate").tmpl(jsonData).appendTo("#systemReportTemplateResults");
    }
    else
    {
        $('#showNoSystemTemplate').show();
        $('#showNoUserTemplate').show();
        $('#systemReportTemplateResults').show();
        $('#userReportTemplateResults').show();
    }    
}

//User Report Template data fail method.
function GetUserReportTemplatesFail()
{
    alert('Service Failed.');
}

function OpenUserReportTemplate()
{  

    $("#divReportUserTemplateDialog").dialog({

        modal: true,
        draggable: false,
        resizable: false,

    });

    $('#divReportUserTemplateDialog').show();
    $("#dialog:ui-dialog").dialog("destroy");
}

function WinterSnapChartStatus() {

    var reportData = {};
    reportData.eventId = _eventId;
    reportData = JSON.stringify(reportData);
    _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetWinterSnapChartUpdate';
    AjaxPost(_serviceUrl, reportData, eval(WinterSnapChartStatusSuccess), eval(WinterSnapChartStatusFail));
}

function WinterSnapChartStatusSuccess(result) {

    if (result != null && result.d != null) {
        if (result.d == true)
            $('#lnkWinter').html('Open Winter SnapCharT®');
    }
}

function WinterSnapChartStatusFail() {
}

//get sub tab id
function GetSubTabId()
{
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _subTab = splitUrl[splitUrl.length - 2];
    _subTab = _subTab.substring(_subTab.lastIndexOf('-') + 1);
}

//get Event ID from url
function GetParameterFromUrl()
{
  
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');   
    _module=splitUrl[splitUrl.length - 3];
    _eventId = splitUrl[splitUrl.length - 1];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }
   
}

//onclick of Custom Report link
function RedirectCustomReport(reportType) {
  
    var reportTypeValue = (reportType == 'Event' ? '1' : '2');
    var url = _baseURL + 'CustomReports/Create/' + _module + '/'+ reportTypeValue +'-' + _subTab + '/' + _eventId ;
    newwindow = window.open(url, 'Report', 'directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes,height=750,width=1200,top=250,left=120');
    if (window.focus) { newwindow.focus() }
}

////get all Report Names depends on Company Id and display in link
//function DisplayReportTitle()
//{
  
//    var data = {};
//    data.companyID = GetCompanyId();
//    data.userID = _userId;
//    data.eventId =_eventId;
//    data = JSON.stringify(data);

//    _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetReportTitleNames';
//    AjaxPost(_serviceUrl, data, eval(GetReportTitleNamesSuccess), eval(GetReportTitleNamesFail));
//}

var _reportName = '';

//for edit Report Name and columns- for User and Event based
function OpenReportTemplatePage(reportId, eventType) {
  
    var url = _baseURL + 'CustomReports/Edit/' + _module + '/' + eventType + '-' + _subTab + '/' + reportId;

    newwindow = window.open(url, 'Report', 'directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes,height=750,width=1200,top=250,left=120');
    if (window.focus) { newwindow.focus() }
}

////service fail method -
//function GetReportTitleNamesFail()
//{
//    alert('Service method failed.');
//}


function DeleteEventReport(reportId,deleteReportType,objDelete)
{
    //var deleteReportName = (deleteReportType == 'user' ? reportName : _reportName);//double quote is not working
    var deleteReportName = $(objDelete).attr('reportName');
    var ConfirmDelete = confirm('Are you sure want to delete ' + deleteReportName + '?');
    if (ConfirmDelete) {
        _deleteReportType = deleteReportType;
        var reportData = new ReportDeleteDataDetails(reportId);
        var data = JSON.stringify(reportData);

        _serviceUrl = _baseURL + 'Services/CustomReport.svc/DeletePreviewReportData';

        //used async: false -need to make it Generic method  
        PostAjax(_serviceUrl, data, eval(DeletePreviewReportDataSuccess), eval(DeletePreviewReportDataFail));
    }
}

function ReportDeleteDataDetails(reportId) {

    var deleteData = {};
    deleteData.eventID = _eventId;
    deleteData.reportId = reportId;
    return deleteData;
}

//Preview Report Data success function
function DeletePreviewReportDataSuccess(result) {
    try {
        if (_deleteReportType == '1')//event report- rebind the reports
        {
            if (window.opener == undefined)
                DisplayReportTitle();
            else if (typeof window.opener.DisplayReportTitle == 'function')
                window.opener.DisplayReportTitle();
        }
        else
            BindUserReportTemplates();//rebind the user and system reports
    } catch (e) { }
        
    
    
}

//service fail
function DeletePreviewReportDataFail() {

    alert('Service Failed.');
}

//open popup page for Report Values as new window
function OpenReportBrowsers(url)
{
    // Code for getting report id and event id if app is hosted in local or test server
    if (window.location.href.indexOf("localhost") == -1) {
        var reportid = url.split('/')[5];
        var eventid = url.split('/')[4];
        url = _baseURL + "Report/View/" + eventid + "/" + reportid;
    }
    else {
        var reportid = url.split('/')[4];
        var eventid = url.split('/')[3];
        url = _baseURL + "Report/View/" + eventid + "/" + reportid;
    }

    newwindow = window.open(url, "_blank", "directories=no, status=no, resizable=no, width=1200, height=750, top=250, left=120, scrollbars=1, toolbar=no, menubar=no");

    if (window.focus) {
        newwindow.focus();
    }
}