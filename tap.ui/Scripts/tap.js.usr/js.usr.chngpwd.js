﻿// On Load functions
//rwrwrwerwe
var usrname, oldpwd, newpwd, confpwd, changePswdType;
$(document).ready(function () {
    
    var CompanyId = document.getElementById('txtCompanyIdCnange');
    if (sessionStorage.Change == 'false')
        window.location.href = _baseURL + 'Home';
    else
        if (sessionStorage.Change == undefined || sessionStorage.Change == 'undefined' || sessionStorage.Change == 'true' || sessionStorage.Change == 'truewithmessage' ) {

            if (sessionStorage.Change != 'truewithmessage')
            sessionStorage.Change = 'true';


            var spilttedURL = window.location.href.split('/');

            if (spilttedURL[spilttedURL.length - 1] == 'message') {
                sessionStorage.Change = 'truewithmessage';
            }


            if (sessionStorage.Change == 'true'){
                $('#ErrorMeeage').show();
                $('#lblErrorMeeageForPolicy').hide();}
            else if (sessionStorage.Change == 'truewithmessage') {
                $('#lblErrorMeeageForPolicy').show();
                $('#ErrorMeeage').hide();
            }
            else {
                $('#ErrorMeeage').hide();
                $('#lblErrorMeeageForPolicy').hide();
            }

            if (spilttedURL[spilttedURL.length - 1].split('?').length > 1)
            {
                $('#ErrorMeeage').hide();
                $('#lblErrorMeeageForPolicy').hide();
            }

            //$('#txtChnPwdUserName').focus();
            var queryStringArgs = {};
            var queryString = location.search.substring(1);
            var pairs = queryString.split("&");

            for (var index = 0; index < pairs.length; index++) {
                var pos = pairs[index].indexOf('=');
                if (pos == -1) continue;
                var argName = pairs[index].substring(0, pos);
                var value = pairs[index].substring(pos + 1);
                queryStringArgs[argName] = unescape(value);
            }

            if (sessionStorage.uName == null && $('#txtChnPwdUserName').val() == null)
                window.location.href = _baseURL + "Signin";


            if (sessionStorage.uName != null)
                $('#txtChnPwdUserName').val(sessionStorage.uName);

            if (sessionStorage.oldPswd == null)
                changePswdType = true;




            $('#btnChnPwdSave').click(function () {


                $('#ChnPwdErrorMsg').html('');
                usrname = $('#txtChnPwdUserName').val();
                oldpwd = sessionStorage.oldPswd;
                newpwd = $('#txtChnPwdNewPwd').val();
                confpwd = $('#txtChnPwdConPwd').val();
                if (Validation(newpwd, oldpwd, changePswdType)) {
                    if (ValidateConfirmPassword(newpwd, confpwd)) {
                        //
                        PerformAction(usrname, oldpwd, newpwd);
                        return false;

                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            });


            $("input").bind("keydown", function (event) {
                var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
                if (keycode == 13) {
                    document.getElementById('btnChnPwdSave').click();
                    return false;
                }
                else
                    return true;
            }); // end of keydown event
        }
        else
            return false;
});      //end of document ready

function PerformAction(usrname, oldpwd, newpwd) {   
    var data = JSON.stringify(UserPasswordDetailsGet(usrname, oldpwd, newpwd));
    serviceurl = _baseURL + 'Services/Users.svc/UserPasswordChange';
    AjaxPost(serviceurl, data, eval(ChngPasswordSuccess), eval(ChngPasswordFail));
}
function ChngPasswordSuccess() {
    
    var errorMessage = 'Your password is successfully changed. Redirecting to Login Page';
    var errorMessageType = 'jSuccess';
    sessionStorage.Change = 'false';
   
    //document.getElementById("divResetPassword").disabled = true;
    //document.getElementById('btnclearSessions').click();
    //NotificationMessage(errorMessage, errorMessageType, true, 5000);
   // window.location.href = _baseURL + "Signin";
    window.location.href = _baseURL + 'Home';

}

function ChngPasswordFail(msg) {
    $('#ChnPwdErrorMsg').removeClass('SuccessMessage');
    $('#ChnPwdErrorMsg').html('Changed password fail.');
    $('#ChnPwdErrorMsg').addClass('WarningMessage');
    ClearPasswordFields();
}
function Validation(newpwd,  oldpswd, changePswdType) {


    var errorMessage = '';
    var errorMessageType = 'jError';
    

        if (newpwd == '') {
            errorMessage = 'Please enter Password';
        }
        else if(confpwd == '')
        {
            errorMessage = 'Please enter Confirm Password';
        }
        else if (newpwd != confpwd) {
            errorMessage = 'Password and Confirm Passowrd should be same';
        }
        if (changePswdType != true) {
            if (oldpswd == newpwd)
                errorMessage = 'Your new password cannot be the same as your previous password.';
        }
    if (errorMessage != '') {
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
        else
        return true;
  
    
    
    

     //return true;
}

function UserPasswordDetailsGet(usrname, oldpwd, newpwd) {
    var userPwdData = {};
    userPwdData.username = usrname;
    userPwdData.oldPassword = oldpwd;
    userPwdData.newPassword = newpwd;
    return userPwdData;
}
function ClearPasswordFields() {
  //  $('#ChnPwdErrorMsg').html('');
   // $('#txtChnPwdOldPwd').val('');
    $('#txtChnPwdNewPwd').val('');
    $('#txtChnPwdConPwd').val('');
}