﻿
/* --------------------------- Helps the Dashboard operations ------------------------------  */

//Global Variables
_companyId = -1;
//Default year to date
_Selection = 'yearToDate';

//Page load
$(document).ready(function () {
    
    _companyId = GetCompanyId();

    LinkSelectionChange();
    DashboardDistribution(CurrentYearSelection(), new Date());
    DateSelection();
   
});

function DashboardDistribution(filterFromDate, filterToDate) {    
    BasicCuaseCategoryDistribution(_companyId, filterFromDate, filterToDate);
    EventBreakdown(_companyId, filterFromDate, filterToDate);
    RootCausesMostActiveGet(_companyId, filterFromDate, filterToDate);
}

function DateSelection() {    

    //var dateFormat = $('#hdnUserDateFormat').val();
    //if (dateFormat != null && dateFormat.match(/yyyy/)) {
    //    dateFormat = dateFormat.replace('yyyy', 'yy');
    //}

    $('#BCCDFromDate').datepicker({
        maxDate: '+0d',
        dateFormat: localStorage.dateFormat,
        onSelect: function (dateText, inst) {
            
            //Filters Dashboard with custom date range
            if (ValidateSelectionDate('BCCDToDate')) {

                _Selection = 'CustomDateRange';
                DashboardByPeriod('CustomDateRange', $(this).val(), $('#BCCDToDate').val());

                //Clear the Date textboxes
                //CustomDatesClear();
            }

        }
    });

    $('#BCCDToDate').datepicker({
        maxDate: '+0d',
        dateFormat: localStorage.dateFormat,
        onSelect: function (dateText, inst) {
            
            //Filters Dashboard with custom date range
            if (ValidateSelectionDate('BCCDFromDate')) {
                
               _Selection = 'CustomDateRange';
               DashboardByPeriod('CustomDateRange', $('#BCCDFromDate').val(), $(this).val());

                //Clear the Date textboxes
               //CustomDatesClear();
            }
            
        }
    });
    
    //Position the date popup exactly below the Date textbox.
    $('#BCCDFromDate').focusin(function () {
        $('#BCCDFromDate').datepicker('widget').css({ "font-size": "12.3px", position: "fixed" });
    });
    $('#BCCDToDate').focusin(function () {
        $('#BCCDToDate').datepicker('widget').css({ "font-size": "12.3px", position: "fixed" });
    });

    $("#ui-datepicker-div").addClass("notranslate");
}

function ValidateSelectionDate(id) {
    
    if ($('#'+ id).val() != '') {
        //Notify here to select FROM date.        
        return true;
    }
        
    return false;
}

function EventBreakdown(companyId, fDate, tDate, isFilter) {
    
    var EventBreakdownInfo = new CategoryDetails(companyId, fDate, tDate, isFilter);
    var data = JSON.stringify(EventBreakdownInfo);
    serviceUrl = _baseURL + 'Services/Dashboard.svc/EventBreakdown';
    AjaxPost(serviceUrl, data, eval(EventBreakdownSuccess), eval(EventBreakdownFail));
}

function EventBreakdownSuccess(result) {    
    if (result != null && result.d != null) {
        var EventBreakdownValues = result.d;            
        //split the values string using delimiter, #
        var EventBreakdownValuesSplit = EventBreakdownValues.split("#");
        var TotalEvents = EventBreakdownValuesSplit[0].substring(EventBreakdownValuesSplit[0].indexOf('=') + 1, EventBreakdownValuesSplit[0].length);
        var TotalIncidents = EventBreakdownValuesSplit[1].substring(EventBreakdownValuesSplit[1].indexOf('=') + 1, EventBreakdownValuesSplit[1].length);
        var TotalInvestigations = EventBreakdownValuesSplit[2].substring(EventBreakdownValuesSplit[2].indexOf('=') + 1, EventBreakdownValuesSplit[2].length);
        var TotalAudits = EventBreakdownValuesSplit[3].substring(EventBreakdownValuesSplit[3].indexOf('=') + 1, EventBreakdownValuesSplit[3].length);
       
        //Calculate the ratio of Investigations and Incidents
        var incinvRatio = TotalInvestigations == 0 && TotalIncidents == 0 ? 0
            : Math.round((parseInt(TotalInvestigations) / (parseInt(TotalIncidents) + parseInt(TotalInvestigations))) * 100);

        $('#lblTotalevents').html(TotalEvents);
        $('#lblIncidents').html(TotalIncidents);
        $('#lblInvestigations').html(TotalInvestigations);
        $('#lblAudits').html(TotalAudits);
        $('#lblIncInvRatio').html(incinvRatio + '%');

    }
}

function EventBreakdownFail() { }

function RootCausesMostActiveGet(companyId, fDate, tDate, isFilter) {
    
    var RootCausesMostActiveGetInfo = new CategoryDetails(companyId, fDate, tDate, isFilter);
    var data = JSON.stringify(RootCausesMostActiveGetInfo);
    serviceUrl = _baseURL + 'Services/Dashboard.svc/MostCommonRootCauses';
    AjaxPost(serviceUrl, data, eval(MostCommonRootCausesSuccess), eval(MostCommonRootCausesFail));
}

function MostCommonRootCausesSuccess(result) {
    
    var jsonResponse = ParseToJSON(result.d);
    if (jsonResponse != null) {

        var commonRCThtml = '';
        
        if (jsonResponse.length == 0) {
            
            commonRCThtml = '<span style="color:#4779ae; font-size:16px;">';
            commonRCThtml += NODataMessage('MostcmnRCT');
            commonRCThtml += '</span>';
        }

        for (var i = 0; i < jsonResponse.length; i++) {

            commonRCThtml += '<li style="list-style:decimal;">' + FullpathModify(jsonResponse[i].rootcause.FullPath, jsonResponse[i].num) + '</li>';            
        }
        
        $('#ListTop10RCT').html(commonRCThtml);
    }
}

function MostCommonRootCausesFail() { }

//Deletes the front and trail backward slashes and replace the backward slash with > 
function FullpathModify(rctPath, oftenUsed) {
    //Remove the leading RootCauseTree also
    rctPath = rctPath.substr(0, rctPath.length - 1).substr(16, rctPath.length).replace(/\\/g, ' > ') + ' (' + oftenUsed + ')';
    return rctPath;
}

function CategoryDetails(companyId, fDate, tDate, isFilter) {
    this.companyId = companyId;
    this.fDate = fDate;
    this.tDate = tDate;
    this.isFilter = isFilter;
 }

function BasicCuaseCategoryDistribution(companyId, fDate, tDate, isFilter) {
     
    var BasicCuaseCategoryDistributionInfo = new CategoryDetails(companyId, fDate, tDate, isFilter);
    var data = JSON.stringify(BasicCuaseCategoryDistributionInfo);
    serviceUrl = _baseURL + 'Services/Dashboard.svc/BasicCauseCategoryDistribution';
    AjaxPost(serviceUrl, data, eval(BasicCauseCategoryDistributionSuccess), eval(BasicCauseCategoryDistributionFail));

}

function BasicCauseCategoryDistributionSuccess(result) {
    
    var jsonResponse = ParseToJSON(result.d);
    if (jsonResponse != null) {
        
        var commonBasicCausehtml = '';
        if (jsonResponse.length == 0) {
            commonBasicCausehtml = '<span style="color:#4779ae;font-size:16px;">';
            commonBasicCausehtml += NODataMessage('BasicCauses');
            commonBasicCausehtml += '</span>';
        }
            

        for (var i = 0; i < jsonResponse.length; i++) {
            commonBasicCausehtml += '<li style="list-style:decimal;">' + jsonResponse[i].basicCause.Title + ' (' + jsonResponse[i].num + ')' + '</li>';
        }

        $('#listTop6BasicCausedistribution').html(commonBasicCausehtml);
    }
}

function BasicCauseCategoryDistributionFail() { }

function ShowPopup(filter) {
    
    $('#filterModal').modal({
        keyboard: true,        
        show: true
    })
}

//Closes the Filter Modal  (Bootstrap modal)
function FilterModalHide() { $('#filterModal').modal('hide'); }

function DashboardByPeriod(durationSelected, selectedFromDate, selectedToDate) {
    if (durationSelected != 'CustomDateRange')
        CustomDatesClear();

    FilterModalHide();
    
    var startingDate = durationSelected == 'thisMonth' ? CurrentMonthSelection()
                : durationSelected == 'thisQuarter' ? CurrentQuarterSelection()
                : durationSelected == 'yearToDate' ? CurrentYearSelection() : selectedFromDate;
    var endingDate = durationSelected == 'CustomDateRange' ? selectedToDate : new Date();
    LinkSelectionChange();

    DashboardDistribution(startingDate, endingDate);   
      
}

//Gets the starting date of the month;
function CurrentMonthSelection() {
    _Selection = 'thisMonth';    
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    return new Date(y, m, 1);
}

//Gets the starting date of current quarter
function CurrentQuarterSelection() {
    _Selection = 'thisQuarter';    
    var d = new Date();
    var quarter = Math.floor((d.getMonth() / 3));
    return new Date(d.getFullYear(), quarter * 3, 1);
}

//Gets the starting date of the current year
function CurrentYearSelection() {
   _Selection = 'yearToDate';
    //LinkSelectionChange();
    return new Date(new Date().getFullYear(), 0, 1);    
}

//Return the message to display for BCCD
function NODataMessage(option) {
    
    var message = '';
    var category = '';
    var duration = _Selection == 'thisMonth' ? 'month'
                     : _Selection == 'thisQuarter' ? 'quarter'
                     : _Selection == 'yearToDate' ? 'year' : 'selected period'

    category = option == 'BasicCauses' ? 'Basic Causes'
            : option == 'MostcmnRCT' ? 'Root Causes' : 'Events';                
   
    return category + ' are not available in this ' + duration;
}

//Makes the links persist.
function LinkSelectionChange() {
    $("#yearToDate").css("text-decoration", "underline");
    $("#thisMonth").css("text-decoration", "underline");
    $("#thisQuarter").css("text-decoration", "underline");
    
    $("#" + _Selection).css("text-decoration", "none");
}

//clear the custom range date values
function CustomDatesClear() {
    $('#BCCDFromDate').val('');
    $('#BCCDToDate').val('');
}