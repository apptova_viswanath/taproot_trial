﻿//** Login Page Methods**//

//global variables
var _serviceUrl;

//Page load Events
$(document).ready(function () {
   // OnLoad();
    //SetCommonPath();

    //Commnon method on load event
   

    FocusUserName();

    $('#btnLogin').click(function () {
        LoginUser();
        return false;
    });

    //Clear the controls
    $('#btnCancel').click(function () {
        ClearFields();
        return false;
    });

    //Redirect to Change Password Page
    $('#btnChangePwd').click(function () {
        RedirectToChangePasswordForm();
    });

    //For making Login button as default
    LoginDefault();

    $('#forgotPswBtn').click(function () {
      
        var email = document.getElementById('txtFgtUserName');
        if (email.value != '') {
            if (ValidateEmail(email.value)) {
                ForgotPassword();

            }
            else {
                _message = 'Please Enter Valid Email';
                _messageType = 'jError';
                NotificationMessage(_message, _messageType, true, 3000);

            }
        }
        else {
            _message = 'Please Enter Email';
            _messageType = 'jError';
            NotificationMessage(_message, _messageType, true, 3000);

        }

        return false;
    });

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0)// If Internet Explorer, return version number
    {
        $('#ErrorMessage').css('margin-left', '270px');
    }

    $('#btnChangePassword').click(function () {
        var password = TrimString($('#txtResetPassword').val());
        var cPassword = TrimString($('#txtConfirmPassword').val());
        if (ValidateConfirmPassword(password, cPassword))
        ResetPassword();
        return false;
    });
    if (sessionStorage.failed == '1') {
        var errorMessage = "Oops! Looks like you have tried to log in 3 times with the wrong password. Enter your email and we can send you a link to reset it";
        var errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        sessionStorage.failed = '0'
    }
    else
        return false;
});
function OnLoad() {
 //    

    //Password Policy
    $('#txtResetPassword').keyup(function () {

        ValidatePassWordPolicy($(this).val());

    }).focus(function () {

        ValidatePassWordPolicy($(this).val());
        $('#pswd_info').show();

    }).blur(function () {

        $('#pswd_info').hide();
        ValidateResetPasswords($('#txtResetPassword').val(), $('#txtConfirmPassword').val());

    });
}


function ValidatePassWordPolicy(pswd) {
    // 

    if (pswd.length < $('#spnLength').html()) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }

    //validate upper case letter
    if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
        $('#upperCase').removeClass('invalid').addClass('valid');
    } else {
        $('#upperCase').removeClass('valid').addClass('invalid');
    }

    //validate lower case letter
    if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
        $('#lowerCase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowerCase').removeClass('valid').addClass('invalid');
    }

    //validate number
    var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
    if (pswd.match(numberPattern)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }


    //validate special character
    var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
    if (pswd.match(specialCharPattern)) {
        $('#specialCharacter').removeClass('invalid').addClass('valid');
    } else {
        $('#specialCharacter').removeClass('valid').addClass('invalid');
    }


    if (pswd.length > $('#spnMaxCharacterCount').html()) {
        $('#maxCharacter').removeClass('valid').addClass('invalid');
    } else {
        $('#maxCharacter').removeClass('invalid').addClass('valid');
    }

}

function ValidateResetPasswords(Password, cPassword) {
    // 
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    if (Password != null && Password.match(_passwordRegularExpression) != null) {
        if (Password != cPassword) {
            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;
        } else {
            ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }
    else {
        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', true);
        return false;
    }

    return true;
}
//Common Methods on load event
function FocusUserName() {

    $('#txtUserName').focus();
}

//Redirect to Change Password Page
function RedirectToChangePasswordForm() {

    window.location.href = "ChangePassword.aspx?username=" + $("#txtUserName").val();
}

//Login Event
function LoginUser() {

    var userName = $('#txtUserName').val();
    var password = $('#txtPassword').val();

    //to save the Username in cookie
    $.cookie("userName", userName, { path: '/' });

    if (ValidateLoginDetails(userName, password)) {

        isAuthenticateUser();
        return;

        var data = JSON.stringify(GetUserDetails());

        //_serviceUrl = _svcURL + 'tap.srv.usr/Users.svc/' + 'UsersMostActiveSelect';
        
        _serviceUrl = _baseURL + 'Services/Users.svc/UsersMostActiveSelect';

        AjaxPost(_serviceUrl, data, eval(LoginEventSuccess), eval(LoginEventFail));
    }
}

function isAuthenticateUser() {
    
    var data = JSON.stringify(GetUserDetails());
    _serviceUrl = _baseURL + 'Services/Users.svc/GetUsersAuthentication';

    AjaxPost(_serviceUrl, data, eval(AuthenticateLoginSuccess), eval(AuthenticateLoginFail));
}


function AuthenticateLoginSuccess(result) {
    //alert(result.d);
    if (result != null && result.d != null) {
        if (result.d == 'Enabled' || result.d == 'true') {

            var data = JSON.stringify(GetUserDetails());

            //_serviceUrl = _svcURL + 'tap.srv.usr/Users.svc/' + 'UsersMostActiveSelect';

            _serviceUrl = _baseURL + 'Services/Users.svc/UsersMostActiveSelect';

            AjaxPost(_serviceUrl, data, eval(LoginEventSuccess), eval(LoginEventFail));
        }
        else if (result.d == 'AttemptsFail')
        {          
           window.location.href = _baseURL + 'tap.usr/ForgotPassword.aspx';
            //'~/tap.usr/ForgotPassword.aspx';
        }
        else if (result.d == "Disabled") {
            var errorMessage = "This account has been disabled.!";
            var errorMessageType = 'jError';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
        else if ((result.d == 'DisabledWrongPassword') || (result.d == 'EnabledWrongPassword'))
        {
            var errorMessage = "The user name or password you entered is incorrect.";
            var errorMessageType = 'jError';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
        else 
        {
            //var errorMessage = "You don't have access to TapRooT@!";
            var errorMessage = "You do not have access to TapRooT&reg;";
            var errorMessageType = 'jError';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
    }
}

//Service failed
function AuthenticateLoginFail(msg) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}

function checkValues()
{
    if (event) {
        if (event.keyCode == 13) {
            LoginUser();
        }
        else
            return;
    }
} 

//validation
function ValidateLoginDetails(userName, password) {

    var errorMessage = '';
    var errorMessageType = 'jError';
    if (userName == '' && password == '') {
        errorMessage = 'Enter User Name and Password.';
    }
    else if (userName == '') {
        errorMessage = 'Please enter User Name.';
    }
    else if (password == '') {
        errorMessage = 'Please enter Password.';
        $('#txtPassword').focus();
    }

    if (errorMessage != '') {
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
    return true;
}

function GetUserDetails() {
    var userData = {};
    userData.userName = $('#txtUserName').val();
    userData.password = $('#txtPassword').val();
    return userData;
}

//Clearing the control values
function ClearFields() {

    $('#txtUserName').val('');
    $('#txtPassword').val('');
    // $('#Msg').html('');
    $('#txtUserName').focus();
    $('#ErrorMessage').hide();
}

//Login Success method
function LoginEventSuccess(result) {
    var result = result.d;
    var errorMessage = '';
    var errorMessageType = 'jError';

    if (result != undefined) {

        errorMessage = (result == 'InActive') ? 'The User Name you have provided is inactive.' : 'The user name or password you entered is incorrect.';
        var resultObject = (result == 'InActive') ? false : result;

        if (resultObject) {

            //Using Hidden field to store username
            $('#hfUserName').val($('#txtUserName').val());
            
            //Taken dummy button which is not visible in Login.aspx 
            //and implemented navigation to respective pages in code behind.
            CheckSubscriptionOnLogIn($('#txtUserName').val());
            //UserLoginDetails();

        }
        else {
            //  
            //removing redirect to forgot password after 3 login attempts
            //var FailureCount = getCookie('FailureCount');
            //if (FailureCount == "")
            //    document.cookie = "FailureCount=1";
            //else {
                
            //    var totalFailureCount = Number(FailureCount + 1)
            //    if (totalFailureCount < 3)
            //        document.cookie = "FailureCount=" + totalFailureCount;
            //    else
            //    {
            //        errorMessage = "Oops! Looks like you have tried to log in 3 times with the wrong password. Enter your email and we can send you a link to reset it";
            //        errorMessageType = 'jError';
            //        if (totalFailureCount == 3)
            //          sessionStorage.failed = 1;

            //        window.location.href = _baseURL + 'tap.usr/ForgotPassword.aspx';
                   
            //    }

            //}
               
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
            $('#txtPassword').val('');
            $('#txtPassword').focus();
            return false;
        }
    }
}

//get cookie 
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return Number( c.substring(name.length, c.length));
    }
    return 0;
}
//Service failed
function LoginEventFail(msg) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}

//get the User Details
function UserLoginDetails() {
    //
    var data = JSON.stringify(GetUserDetails());
    _serviceUrl = _baseURL + 'Services/Users.svc/GetUserandAdminDetails';
    AjaxPost(_serviceUrl, data, eval(LoginDetailsSuccess), eval(LoginDetailsFailed));
}
//function convertAdminDateFormat(Original) {
    
//    var dateFormat = '';
//    if (Original != null) {

//        switch (Original) {

//            case 'dd/MM/yy':
//              return  dateFormat = 'dd/mm/y';
             
//            case 'MM/dd/yy':
//                return dateFormat = 'mm/dd/y';
                

//            case 'MMM dd yyyy':
//                return dateFormat = 'M dd yy';

//            case 'dd-MM-yy':
//                return dateFormat = 'dd-mm-y';
                
               
//            case 'dd-M-yy':
//                return dateFormat = 'dd-m-y';
                
//            default:
//                return dateFormat = 'mm/dd/yy';
                
//        }
       
//    }
    
//}
//Login Success method
function LoginDetailsSuccess(result) {
    if (result != null) {
        if(result.d != 'null')
        {
            
            var jsonResult = ParseToJSON(result.d)[0];
            var companyId = jsonResult.companyId;
            var companyName = jsonResult.companyName;
            var eventId = 0;
            var userId = jsonResult.userId;
            var categoryId = 1;
            var logDetails = jsonResult.userName + ' logged in';
            var firstName = jsonResult.firstName;
            var dateFormat = jsonResult.dateFormat;
            var languageId = jsonResult.languageId;
            var languageCode = jsonResult.languageName;
            var languageDescription = jsonResult.languageDescription;
            
            var languageDetails = [];
            languageDetails[0] = languageId;
            languageDetails[1] = languageCode;
            languageDetails[2] = languageDescription;
           
            localStorage["languageDetails"] = JSON.stringify(languageDetails);



            $.cookie("FirstName", firstName, { path: '/' });
           
            $("#hdnUserID").val(userId);
            $("#hdnUserName").val(jsonResult.userName);
            $('#hdnFirstName').val(firstName);
            $('#hdnCompanyID').val(companyId);
            $('#hdnCompanyName').val(companyName);
            $('#hdnDateFormat').val(dateFormat);         
            //Save Activity Log
            //SaveActivityLog(_companyId, eventId, _userId, categoryId, logDetails);

            //authenticate Button
            //if (jsonResult.IsGlobal == null)
            //    sessionStorage.IsGlobal = true;
            //else
            //    sessionStorage.IsGlobal = false;

            if (jsonResult.IsGlobal == null)
                localStorage.IsGlobal = true;
            else
                localStorage.IsGlobal = false;
 
           sessionStorage.userId = userId;
           sessionStorage.uName = jsonResult.userName;
            sessionStorage.oldPswd = $('#txtPassword').val();
            sessionStorage.companyId = companyId;
            //session storage need to be replace with local storage for using in dateformat in brwser tabs
            //sessionStorage.dateFormat = convertAdminDateFormat(dateFormat);
            localStorage.dateFormat = dateFormat == "" ? convertAdminDateFormat('MMM dd yyyy') : convertAdminDateFormat(dateFormat);

            $('#btnAuthentication').click();
        }
        else
        {
            $('#ErrorMessage').show();
        }


    }
}

//Service failed
function LoginDetailsFailed() {
    alert('Service Failed.');
}

//default click
function LoginDefault() {
    $("input").bind("keydown", function (event) {
        var enterKeyCode = 13;
        var keyCode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keyCode == enterKeyCode) {
            //document.getElementById('btnLogin').click();
            return false;
        }
        else
            return true;
    });  // end of keydown event
}

function ForgotPassword() {
   //  
    var emailId = $('#txtFgtUserName').val();
    if (ForgotPswValidation(emailId)) {
        $('#forgotPswBtn').hide();
        $('#forgotPswSendingImage').show();
        $('#divMessage').hide();
        var data = JSON.stringify(ForgotPswDetails());
        _serviceUrl = _baseURL + 'Services/Users.svc/forgotPassword';
        AjaxPost(_serviceUrl, data, eval(ForgotPswSuccess), eval(ForgotPswFails));
    }
}

function ForgotPswDetails() {
    var userData = {};
    userData.emailId = $('#txtFgtUserName').val();
    userData.baseUrl = window.location.protocol + "//" + window.location.host + _baseURL;
    return userData;
}

function ResetPassword() {
    //  
   // var rstPw = $('#hdnUserName').val(); //$('#hiddenUserName').val;

        var data = JSON.stringify(ResetPswDetails());
        _serviceUrl = _baseURL + 'Services/Users.svc/ResetPassword';
        AjaxPost(_serviceUrl, data, eval(ResetPswSuccess), eval(ResetPswFails));
    }

function ResetPswDetails() {
    var userData = {};
    userData.userName = $('#hdnUserName').val();// document.getElementById('hiddenUserName').value;
    userData.resetPassword = $('#txtResetPassword').val();
    return userData;
}
function ForgotPswFails() {
    
}
function ValidateEmail(Email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(Email);

}
function ForgotPswSuccess(result) {
    var errorMessageType = 'jSuccess';
    var errorMessage = '';
    if (result != null && result.d != null) {

        if (result.d.indexOf(':') > -1) {
            errorMessageType = 'jError';
            var arrayMsg = result.d.split(':');
            errorMessage = arrayMsg[1];
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
            $('#forgotPswSendingImage').hide();
            $('#forgotPswBtn').show();
        }
        else if (result.d.indexOf('Cannot') > -1) {
            $('#forgotPswSendingImage').hide();
            //$('#divNotFoundMessage').html(result.d).show();
            $('#divForgotPsw').show();
            $('#forgotPswBtn').show();
            $('#divFgtSubmitBtn').show();
            $('#divSuccessMessage').hide();

            errorMessage = 'This <i>'+ $('#txtFgtUserName').val() + '</i> email address does not have an account.';
            errorMessageType = 'jError';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);

            $('#txtFgtUserName').val('');
        }
        else {
            $('#divForgotPsw').hide();
            $('#divFgtSubmitBtn').hide();
            $('#forgotPswSendingImage').hide();
            $('#divSuccessMessage').show();
           // errorMessage = result.d;
            //NotificationMessage(errorMessage, errorMessageType, true, 3000);

            //window.location.href = _baseURL + "tap.usr/login.aspx";
        }
    }
}

function ResetPswSuccess(result) {
    // 
    if(result.d)
    {
        window.location.href = _baseURL + 'tap.usr/login.aspx';
    }
}
function ResetPswFails() {
    
}

function ForgotPswValidation(userName) {
    if (userName == null || userName.length == 0) {
        var errorMessage = "Please Enter User Name.";
        var errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        ShowHideWarningMessage('txtFgtUserName', 'Please specify User Name', true);
        return false;
    }
    else
        ShowHideWarningMessage('txtFgtUserName', 'Please specify User Name', false);
    return true;
}

//Check Company Subscription Time
function CheckSubscriptionOnLogIn(username) {
    var userDetails = new SubscriptionData(username);
    _data = JSON.stringify(userDetails);

    var serviceUrl = _baseURL + 'Services/CompanyDetail.svc/CheckSubscriptionTime';
    PostAjax(serviceUrl, _data, eval(SubscriptionMsgSuccess), eval(SubscriptionMsgFail));
}
function SubscriptionData(username) {
    this.userName = username;
}

function SubscriptionMsgSuccess(result) {
    if (result != null && result.d != null) {
        $('#ErrorMessage').html(result.d);
        $('#ErrorMessage').show();
        errorMessage = result.d;
        errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        ClearFields();
    }
    else
        UserLoginDetails();
}
function SubscriptionMsgFail(result) {
}

function ResetPasword(expired) {

    if (expired == 'true') {
        
        $('#divMessage').show();
        $('#divForgotPsw').show();
        $('#divForgotPassword').show();
        $('#forgotPswSendingImage').hide();
        $('#divResetPassword').hide();
    }
    else if (expired == 'false') {
        $('#divMessage').hide();
        $('#divForgotPsw').hide();
        $('#divFgtSubmitBtn').hide();
        $('#forgotPswSendingImage').hide();
        $('#divResetPassword').show();
    }
    else {
        $('#divMessage').hide();
        $('#divForgotPsw').show();
        $('#divFgtSubmitBtn').show();
        $('#forgotPswSendingImage').hide();
        $('#divResetPassword').hide();
    }
    //alert(url);
   // var splitUrl = url.split('/');
   // if (url.
   
    //$('#divSuccessMessage').show();
    //errorMessage = result.d;
    //NotificationMessage(errorMessage, errorMessageType, true, 3000);
}




