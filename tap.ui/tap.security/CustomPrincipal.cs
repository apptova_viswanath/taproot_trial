﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Security.Principal;
public class CustomPrincipal : IPrincipal
{

    private IIdentity _identity;

    private string[] _roles;
    public CustomPrincipal(IIdentity identity, string[] roles)
    {
        _identity = identity;
        _roles = roles;
    }

    public System.Security.Principal.IIdentity Identity
    {
        get { return _identity; }
    }

    public bool IsInRole(string role)
    {
        return true;
    }

}