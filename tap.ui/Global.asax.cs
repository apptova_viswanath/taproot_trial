﻿using System;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using tap.dom.hlp;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Web.Configuration;

namespace tap.ui
{
    public class Global : System.Web.HttpApplication
    {
        
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        Enumeration.Types nullvalue = Enumeration.Types.NullValue;
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.hlp.EFSerializer _efSerializer = new tap.dom.hlp.EFSerializer();
        JavaScriptSerializer js = new JavaScriptSerializer();

        void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = Context.Request.Cookies[cookieName];

            if ((authCookie == null))
            {
                return;
            }

            FormsAuthenticationTicket authTicket = null;

            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);

            }
            catch (Exception ex)
            {
                return;
            }

            if ((authTicket == null))
            {
                return;
            }

            string[] roles = authTicket.UserData.Split('|');

            FormsIdentity id = new FormsIdentity(authTicket);

            CustomPrincipal principal = new CustomPrincipal(id, roles);

            Context.User = principal;
        }
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RewriteUrlUser();
        }
        public void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {           
        }
        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
            //Session.Clear();
            //Session.Abandon();
            //FormsAuthentication.SignOut();
            Exception ex = new Exception("Application_End fired.");
            ErrorLog.LogException(ex);
        }

        void Application_Error(object sender, EventArgs e)
        {
           
            // Code that runs when an unhandled error occurs
            Exception CurrentException = Server.GetLastError();
            //var TransactionTypeDetails = _db.SnapCaps.Where(a => a.EventID == eventID).Select(u => u.SnapCapName).ToArray();
            //return js.Serialize(TransactionTypeDetails);

            string userId = string.Empty;
            string companyID = string.Empty;

            if (HttpContext.Current != null) { 
                  userId = Convert.ToString(HttpContext.Current.Application["userId"]);
                  companyID = Convert.ToString(HttpContext.Current.Application["companyID"]);
            }

            string ErrorDetails = CurrentException.Message.ToString();
            SaveExceptionLog(ErrorDetails, companyID, userId, 4);
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }
        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started            
        }

        //void Session_End(object sender, EventArgs e)
        //{
        //    //Exception ex = new Exception("Session_End fired.");
        //    //ErrorLog.LogException(ex);
        //  //  if (Response != null) { 
        //        Response.Redirect("~/tap.usr/Login.aspx?message=1", true);
        //   // }
        //}


        #region Added For RewriteURl For user module For Reference

        /// <summary>
        /// 
        /// </summary>
        public void RewriteUrlUser()
        {
            RouteTable.Routes.MapPageRoute("UserSignIn", "Signin", "~/tap.usr/login.aspx");
            RouteTable.Routes.MapPageRoute("UserSignOut", "Signout", "~/tap.usr/Logout.aspx");
            RouteTable.Routes.MapPageRoute("homepage", "Home/{*ModuleId}", "~/tap.usr/HomePage.aspx");
            RouteTable.Routes.MapPageRoute("homepage2", "", "~/tap.usr/HomePage.aspx" );
            RouteTable.Routes.MapPageRoute("changepassword", "ChangePassword", "~/tap.usr/ChangePassword.aspx");
            RouteTable.Routes.MapPageRoute("ChangePasswordMessage", "ChangePassword/message", "~/tap.usr/ChangePassword.aspx");
            RouteTable.Routes.MapPageRoute("Tasks", "Tasks", "~/tap.usr/Tasks.aspx");
            RouteTable.Routes.MapPageRoute("Dashboard", "Dashboard", "~/tap.usr/Dashboard.aspx");
                       
            //Admin Pages
            RouteTable.Routes.MapPageRoute("SiteSettings", "Admin/Customization/Attachment-Folders-Setup", "~/tap.adm/AdminAttach.aspx");
            RouteTable.Routes.MapPageRoute("Systemlists", "Admin/Customization/Lists", "~/tap.adm/AdminSystemList.aspx");
            RouteTable.Routes.MapPageRoute("EmailSettings", "Admin/Customization/EmailSettings", "~/tap.adm/EmailSettings.aspx");           
            RouteTable.Routes.MapPageRoute("AdminCustomTabs", "Admin/Configuration/{Module}/{AdmCustomSubTab}-{AdmCustomSubTabId}", "~/tap.adm/Custom-Tabs-Edit.aspx");//12-10-2012
            RouteTable.Routes.MapPageRoute("Users", "Admin/Users", "~/tap.adm/ManageUsers.aspx");
            RouteTable.Routes.MapPageRoute("UserDetails", "Admin/Users/New", "~/tap.adm/UserDetails.aspx");
            RouteTable.Routes.MapPageRoute("User Details", "Admin/Users/{UserID}/{Action}", "~/tap.adm/UserDetails.aspx");
            RouteTable.Routes.MapPageRoute("SystemNotifications", "Admin/Messages", "~/tap.adm/SystemNotifications.aspx");
            RouteTable.Routes.MapPageRoute("EditSystemNotification", "Admin/Notifications/{Action}/{SysID}", "~/tap.adm/EditSystemNotifications.aspx");

            RouteTable.Routes.MapPageRoute("ForgotPassword", "ForgotPassword", "~/tap.usr/ForgotPassword.aspx");

            RouteTable.Routes.MapPageRoute("FileUploadUsers", "Tools/UploadUserFile", "~/tap.usr/UploadUsers.aspx");
            
            RouteTable.Routes.MapPageRoute("Event_InvestigationProcess", "Event/{Module}/7-Steps-{CustomSubTabId}/{EventId}", "~/tap.usr/InvestigationProcess.aspx");

            //Investigation audit system Tabs
            RouteTable.Routes.MapPageRoute("TapRootTab", "Event/{Module}/TapRooT-{CustomSubTabId}/{EventId}", "~/tap.usr/TapRoot-Tab.aspx");
            RouteTable.Routes.MapPageRoute("FixTab", "Event/{Module}/Fix-{CustomSubTabId}/{EventId}", "~/tap.usr/FixTab.aspx");
            RouteTable.Routes.MapPageRoute("FixSMARTERTab", "Event/{Module}/Fix-{CustomSubTabId}/SMARTER/{EventId}/{capID}", "~/tap.usr/SmartCorrectiveAction.aspx");
            RouteTable.Routes.MapPageRoute("CASMARTERTab", "Event/{Module}/CorrectiveActions-{CustomSubTabId}/SMARTER/{EventId}/{capID}", "~/tap.usr/SmartCorrectiveAction.aspx");

            RouteTable.Routes.MapPageRoute("DemoSMARTERTab", "Event/{Module}/Fix-{CustomSubTabId}/DemoSMARTER/{EventId}/{capID}", "~/tap.usr/DemoSmartCorrectiveAction.aspx");

            RouteTable.Routes.MapPageRoute("ReportTab", "Event/{Module}/Report-{CustomSubTabId}/{EventId}", "~/tap.usr/Report-Tab.aspx");
            RouteTable.Routes.MapPageRoute("ReferenceTab", "Event/{Module}/Reference-{CustomSubTabId}/{EventId}", "~/tap.usr/Reference-Tab.aspx");

            //Corrective Action plan Edit Tab
            RouteTable.Routes.MapPageRoute("CorrectiveActionsTab", "Event/{Module}/CorrectiveActions-{CustomSubTabId}/{EventId}", "~/tap.usr/CorrectiveActionTab.aspx");
            
            //SnapChart
            RouteTable.Routes.MapPageRoute("SnapCharT", "Event/{module}/SnapCharT/{EventId}", "~/tap.usr/tap.snap.ui/SnapChart.aspx");
            
            //For Event  Page
            RouteTable.Routes.MapPageRoute("EventPage", "Event/{Module}/Details-{CustomSubTabId}/{EventId}/{Action}", "~/tap.usr/Event.aspx");
            RouteTable.Routes.MapPageRoute("share", "Event/{Module}/Sharing-{CustomSubTabId}/{EventId}/{Action}", "~/tap.usr/Share.aspx");
            RouteTable.Routes.MapPageRoute("attachments", "Event/{Module}/Attachments-{CustomSubTabId}/{EventId}", "~/tap.usr/Attachment.aspx");            
            RouteTable.Routes.MapPageRoute("userCustomTab", "Event/{Module}/{CustomSubTab}-{CustomSubTabId}/{EventId}", "~/tap.usr/CustomTabField.aspx");

            //TOOLS
            RouteTable.Routes.MapPageRoute("FileUpload", "Tools/UploadFile/{EventId}/Folder-{FolderId}", "~/tap.usr/UploadFile.aspx");

            //Added for custom Tab Landing Page
            RouteTable.Routes.MapPageRoute("CustomTabLanding", "Admin/Customization/Custom-Tabs", "~/tap.adm/Custom-Tabs.aspx");
            RouteTable.Routes.MapPageRoute("ModuleSpecificCustomTabs", "Admin/Configuration/Custom-Tabs/{Module}", "~/tap.adm/Custom-Tabs.aspx");

            //Corrective Action plan system Tabs
                      
            RouteTable.Routes.MapPageRoute("CAPCreate", "CorrectiveActionPlan/Create/{EventId}/{CorrectiveActionId}", "~/tap.usr/CorrectiveAction.aspx");
            RouteTable.Routes.MapPageRoute("CAPEdit", "CorrectiveActionPlan/Edit/{EventId}/{CorrectiveActionId}", "~/tap.usr/CorrectiveAction.aspx");

            //Smart Corrective Action plan
            RouteTable.Routes.MapPageRoute("CAPSmartCreate", "SmartCorrectiveActionPlan/Create/{EventId}/{CorrectiveActionId}", "~/tap.usr/SmartCorrectiveAction.aspx");
            RouteTable.Routes.MapPageRoute("CAPSmartEdit", "SmartCorrectiveActionPlan/Edit/{EventId}/{CorrectiveActionId}", "~/tap.usr/SmartCorrectiveAction.aspx");

            //Custom Report Create
            RouteTable.Routes.MapPageRoute("CustomReport", "CustomReports/Create/{Module}/{ReportType}-{CustomSubTabId}/{EventId}", "~/tap.usr/Custom-Report-Template.aspx");

            //Custom Report Edit           
            RouteTable.Routes.MapPageRoute("CustomReportEdit", "CustomReports/Edit/{Module}/{ReportType}-{CustomSubTabId}/{ReportId}", "~/tap.usr/Custom-Report-Template.aspx");

            //Custom Report Data Popup-//changed on 30th April
            RouteTable.Routes.MapPageRoute("CustomReportData", "CustomReportsData/Reports/{EventId}/{ReportId}/{reportType}/{isDelete}", "~/tap.usr/Custom-Report-Data.aspx");

            RouteTable.Routes.MapPageRoute("PrintCustomReportData", "CustomReportsData/Reports/{EventId}/{ReportId}/{isDelete}-Print", "~/tap.usr/Custom-Report-Data.aspx");

            //Report Image Upload
            RouteTable.Routes.MapPageRoute("ReportImageUpload", "Tools/UploadImageFile/{SortOrder}/{ReportId}", "~/tap.usr/UploadReportImage.aspx");

            //Manage Report Page
            RouteTable.Routes.MapPageRoute("ManageCustomReport", "Admin/Reports/Report-Templates", "~/tap.usr/Custom-Manage-Report.aspx");
            
            //Root cause tree
            RouteTable.Routes.MapPageRoute("RootCauseTree", "{Module}/RCT-{CausalFactorID}/{EventId}", "~/tap.usr/RootCauseTree.aspx");


            //Division
            RouteTable.Routes.MapPageRoute("ManageDivision", "Admin/Divisions", "~/tap.adm/ManageDivision.aspx");
            RouteTable.Routes.MapPageRoute("Division", "Admin/Divisions/New", "~/tap.adm/Division.aspx");
            RouteTable.Routes.MapPageRoute("SubDivision", "Admin/Division/{DivisionID}/New", "~/tap.adm/Division.aspx");
            RouteTable.Routes.MapPageRoute("Division Details", "Admin/Divisions/{DivisionID}/{Action}", "~/tap.adm/Division.aspx");
            RouteTable.Routes.MapPageRoute("DivisionProfile", "Admin/Division/{DivisionID}/Profile/{Action}", "~/tap.adm/CompanyProfile.aspx");

            RouteTable.Routes.MapPageRoute("DivisionCustomTabs", "Admin/Division/{DivisionId}/{Module}/{AdmCustomSubTab}-{AdmCustomSubTabId}", "~/tap.adm/Custom-Tabs-Edit.aspx");

            RouteTable.Routes.MapPageRoute("DivisionModuleSpecificCustomTabs", "Admin/Division/{DivisionId}/Custom-Tabs/{Module}", "~/tap.adm/Custom-Tabs.aspx");

            //Manage Report Page
            RouteTable.Routes.MapPageRoute("DivisionManageCustomReport", "Admin/Division/{DivisionId}/Report-Templates", "~/tap.usr/Custom-Manage-Report.aspx");

            RouteTable.Routes.MapPageRoute("SystemDivisionlists", "Admin/Division/{DivisionID}/Lists", "~/tap.adm/AdminSystemList.aspx");
            RouteTable.Routes.MapPageRoute("AdminDivisionCustomTabs", "Admin/Division/{DivisionID}/Custom-Tabs", "~/tap.adm/Custom-Tabs.aspx");
            RouteTable.Routes.MapPageRoute("DivisionSystemNotifications", "Admin/Division/{DivisionID}/Messages", "~/tap.adm/SystemNotifications.aspx");
            RouteTable.Routes.MapPageRoute("DivisionAdminAttach", "Admin/Division/{DivisionID}/Attachment-Folders-Setup", "~/tap.adm/AdminAttach.aspx");
            RouteTable.Routes.MapPageRoute("DivisionUsers", "Admin/{DivisionID}/Users", "~/tap.adm/ManageUsers.aspx");
            RouteTable.Routes.MapPageRoute("DivisionUserDetails", "Admin/{DivisionID}/Users/New", "~/tap.adm/UserDetails.aspx");
            RouteTable.Routes.MapPageRoute("Division User Details", "Admin/{DivisionID}/Users/{UserID}/{Action}", "~/tap.adm/UserDetails.aspx");
            RouteTable.Routes.MapPageRoute("ManageDivisions", "Admin/{DivisionID}/Divisions", "~/tap.adm/ManageDivision.aspx");

            RouteTable.Routes.MapPageRoute("ManageSubDivisions", "Admin/Division/{DivisionID}/Divisions", "~/tap.adm/ManageDivision.aspx");

            RouteTable.Routes.MapPageRoute("AdminSetting", "Admin/Configuration/AdminSetting", "~/tap.adm/AdminSetting.aspx");

            RouteTable.Routes.MapPageRoute("AdminSettingSetUp", "Admin/Configuration/AdminSetting/{SetUp}", "~/tap.adm/AdminSetting.aspx");

            RouteTable.Routes.MapPageRoute("SiteSettingsFormat", "Admin/Configuration/SiteSettings", "~/tap.adm/SiteSettings.aspx");

            RouteTable.Routes.MapPageRoute("CompanyProfile", "Admin/Organization-Profile", "~/tap.adm/CompanyProfile.aspx");

            RouteTable.Routes.MapPageRoute("LoadTesting", "LoadTesting", "~/tap.usr/LoadTesting.aspx");
            RouteTable.Routes.MapPageRoute("AccessDenied", "Security/Access-Denied/{EventId}", "~/tap.usr/Access-Denied.aspx");

            RouteTable.Routes.MapPageRoute("VisualRCT", "{Module}/VisualRCT-{CausalFactorID}/{EventId}", "~/tap.usr/VisualRCT.aspx");

            RouteTable.Routes.MapPageRoute("MasterRCT", "{Module}/RootCauseTree-{CausalFactorID}/{EventId}", "~/tap.usr/MasterRCT.aspx");

            // Display PDF in webpage
            RouteTable.Routes.MapPageRoute("PDFView", "Report/View/{EventId}/{ReportId}", "~/tap.usr/ReportInPDF.aspx");
        }

        #endregion

        public bool SaveExceptionLog(string ErrorDetails, string companyID, string userID, int TypeId)
        {
            tap.dat.UserTransactionsDaily transaction = null;

            //Decrypt the value and pass
            int companyId = (companyID == string.Empty) ? 0 : Convert.ToInt32(companyID);
            int userId = (userID == string.Empty) ? 0 : Convert.ToInt32(userID);

            try
            {
                transaction = new tap.dat.UserTransactionsDaily();

                transaction.CompanyID = companyId;
                transaction.UserID = userId;
                transaction.TransactionTypeID = TypeId;
                transaction.Details = ErrorDetails;
                transaction.TransactionDate = DateTime.UtcNow; //GMT; 
                transaction.IpAddress = tap.dom.transaction.Transaction.GetIPAddress();
                transaction.MACAddress = tap.dom.transaction.Transaction.GetMacAddress();

                _db.AddToUserTransactionsDaily(transaction);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }

        public object TransactionTypeDetails { get; set; }
    }
}
