﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCustomFields.ascx.cs"
    Inherits="tap.usr.ctrl.ucCustomFields" %>

<script type="text/javascript">

    var _ucEventId = '<%=_eventId %>';   
      
</script>

<script src="<%=ResolveUrl("~/Scripts/tap.js.customfield/js.cst.js") %>" type="text/javascript"></script>
<script src="<%=ResolveUrl("~/Scripts/tap.js.customfield/js.cust.fld.js") %>" type="text/javascript"> </script>
<script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
<script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>
<style>    #ui-datepicker-div {
            font-size: 12px;
        }
 </style>
<link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
    type="text/css" />

<div id="liCustomFields" class="ContentWidth ClearAll">
    <div class="ContentHeader">
        <label id="lblHeader" runat="server" visible="false">
        </label>
    </div>
    <div>
        <div class="undoDiv">
            <span onclick="javascript:EventUndo();" class="anchorStyle"  style="display:none" id="ancUndo">Undo</span>&nbsp; <span class="anchorStyle" style="display:none" onclick="javascript:EventUndoAll();"
                id="ancUndoAll">Undo All</span>
        </div>
        <div id="divErrorMsg">
        </div>
    </div>
    <div id="ulCustomfield" class="ulCustomfield-margin notranslate">
        <div id="divCustomfield" runat="server">
        </div>
    </div>
</div>

<div id="divCustomSelectOneTreeview" class="popup" title="Custom Treeview">
    <div>
        <div>
            <label id="ErrCustomField">
            </label>
        </div>
        <div>
            <div class="CenterTree notranslate">
                <div id="UsrFieldTreeviewList" class="treeViewSection">
                </div>
            </div>
        </div>
        <div style="width: 100%;">
            <div class="buttonCenter">
                <input type="button" class="btn btn-primary btn-small btn-float" style="text-align: left; float: left" id="btnCustomTreeCancel" value="Cancel" />
            </div>
            <div class="buttonCenter">
                <input type="button" id="btnUsrFieldTreeOk" style="float: right; text-align: right" value="Select" class="btn btn-primary btn-small btn-float" />
            </div>
        </div>
    </div>
</div>
<div id="divCustomMultipleSelectTreeview" class="popup" title="Classification1">
    <div>
        <div>
            <label id="Label1">
            </label>
        </div>
        <div>
            <div class="CenterTree notranslate">
                <div id="EventClassificationTreeListTest" class="treeViewSection">
                </div>
            </div>
        </div>
        <div class="MarginTop10 PaddingRight100">
            <input type="button" class="btn btn-primary btn-small btn-float" id="btnCustomCancel" value="Cancel" />
            <input type="button" class="btn btn-primary btn-small btn-float" id="btnSelectEventClassificationTest"
                value="Select" />

        </div>
    </div>

</div>

