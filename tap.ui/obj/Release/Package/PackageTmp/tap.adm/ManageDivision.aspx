﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.master" AutoEventWireup="true" CodeBehind="ManageDivision.aspx.cs" Inherits="tap.adm.ManageDivision" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />

    <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />
    <%-- Customized theme ends  --%>

    <%--<link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>" rel="stylesheet" type="text/css" />--%>
    <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />--%>

    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.division.js") %>" type="text/javascript"></script>

    <style type="text/css">
        #txtSearchDivision {
            width: 50%;
        }
    </style>
    <div class="siteContent adminSiteContent">
        <div class=" ClearAll">
            <div style="margin-bottom:10px;">

                <div class="divAddNewUser">
                    <div id="addIcon" class="addDiv">
                        <a id="lnkCreateNewDivision" href="/Admin/Divisions/New">
                            <img id="adddivisionimage" class="addImage" src="../../Images/add.png" alt="Demo">Add Division</a>
                    </div>
                </div>
                <div class="divtxtSearcUser">
                    <input onkeypress="return event.keyCode != 13" id="txtSearchDivision" placeholder="Search Divisions" type="text" class="span2 bootstrp1" autofocus="" />
                </div>
            </div>
            <div class="DivisionListGrid width80" style="width: 95%;">
                <table id="DivisionListGrid" class="scroll notranslate">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="DivisionListGridNavigation" class="scroll" style="text-align: center;">
                </div>

            </div>
          
        </div>
    </div>
</asp:Content>
