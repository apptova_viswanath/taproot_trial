﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.master" AutoEventWireup="true" CodeBehind="AdminSetting.aspx.cs" Inherits="tap.adm.AdminSetting" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <style type="text/css">

                #LoginHeader {
             border: 0 solid #FF0000;
            clear: both;
            color: #265E82;
            font-family: Tahoma,sans-serif;
            font-size: 16px;
            font-weight: bold;
            margin: 5px 0 0 3%;
            
            text-decoration: none;
         }
         #LoginControl{
             border: 0 solid #FF0000;
             margin: 20px 0 0 30%;
             color: #265E82;
             font-family: Tahoma,sans-serif;
            font-size: 14px;
            font-weight: bold;
         }
    </style>

     <input type="hidden" id="hdnSetUp" value="0" runat="server"/>

 <div class="siteContent">
            <div class=" ClearAll" id="divCongratSoftware" style="display:none;">
               <div id="LoginHeader">
                 TapRooT® SOFTWARE
                </div>
            
                <div id="LoginControl">
                    Congratulations, TapRooT® SOFTWARE setup completed.
                </div>
            </div>
        </div>
    </asp:Content>
