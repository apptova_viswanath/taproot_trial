﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Tabs-Edit.aspx.cs"
    Inherits="tap.ui.adm.Custom_Tabs_Edit" MasterPageFile="~/tap.mst/Master.Master" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Tabs-Edit.aspx.cs"
    Inherits="tap.ui.adm.Custom_Tabs_Edit" MasterPageFile="~/tap.mst/Admin.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucAdminTabs.ascx" TagName="Tab" TagPrefix="ucTab" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CustomTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        /*remove folder icon*/
        .jstree-default a ins.jstree-icon {
            display: none;
        }

        .ui-widget-content {
            background: url("images/ui-bg_flat_75_ffffff_40x100.png") repeat-x scroll 50% 50% #FFFFFF;
            border: 0px solid #AAAAAA;
            color: #222222;
        }

        .hiddenDiv {
            display: none;
        }

        .div-sortable-order {
            padding: 0px 2px 0px 2px;
            margin: 0px 0px 0px 0px;
            min-height: 23.1px;
            border: none;
            position: relative;
            float: left;
        }

        #lblIsRequired, #lblActiveField {
            margin-top: -6px;
        }

        /*#FieldInfo li {
            cursor: pointer;
        }*/
         .buttonEdit {
            width: 32%;
            float: right;
          border:0px solid red;
        }

        .buttonDelete {
            width: 20%;
            float: right;
             border:0px solid red;
        }

        .buttonCancel {
            width: 33%;
            float: right;
             border:0px solid red;
        }
    </style>
    <script type="text/javascript" lang="javascript">

        //$(document).ready(function () {
        //    // debugger;
        //    $('#menu').hide();
        //});

    </script>
    <!-- For Sorting and Dragging plugin files -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.create.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.fn.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fn.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/usr.ctrl.syslist.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.restrict.js") %>" type="text/javascript"></script>
       <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />

    <asp:HiddenField ID="hdCurrentModule" ClientIDMode="Static" runat="server" />
    
    <div class="siteContent adminSiteContent">
        <div id="adminTabs">
                        <div >                                                    
                            <div id="addIcon" class="addDiv">
                                <a id="lnkCreateNewTab" href="/Admin/Configuration/Incident/NewTab-0" style="color: rgb(90, 140, 174);"><img id="addtabimage" class="addImage" src="../../Images/add.png" alt="Demo">Add New Tab</a>
                            </div>
                        </div>

            <ucTab:Tab ID="adminTab" runat="server" />
            <div class="MarginTop40">
                <br />
                &nbsp;
            </div>
            <div class="ClearAll ContentBorder">
                <div class="blueHeader">
                    Tab Details
                </div>
                <div class="ContentWidth MarginRight20">
                    <div>
                        <label id="TabErrorMsg">
                        </label>
                    </div>
                    <div class="MarginTop10">
                        <label id="lblTabName" class="ContentCustomLabel" >
                            Tab Name</label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtTabName" class="ContentInputTextBox WidthP40" name="name" placeholder="File or Title"
                            autofocus="" type="text">
                    </div>
                    <div class="MarginTop10">
                        <label id="lblShowModule" class="ContentCustomLabel" style="white-space:nowrap;">
                            This Tab shows up on all modules</label>
                    </div>
                    <div class="MarginTop10">
                        <input type="checkbox" class="MarginLeft20" id="chkShowAllTab" />
                    </div>

                    <div class="MarginTop10">
                        <label id="lblShowTabModule" class="ContentCustomLabel" style="white-space:nowrap;">
                            This Tab shows up on these modules</label>
                    </div>
                    <div class="MarginTop10">

                        <input id="chkIncident" class="MarginLeft20" type="checkbox" />
                        <label id="lblIncident" class="CheckBoxLabel">
                            Incident</label>
                        <input id="chkInvestigation" class="MarginLeft20" type="checkbox" />
                        <label id="lblInvestigation" class="CheckBoxLabel">
                            Investigation</label>
                        <input id="chkAudit" class="MarginLeft20" type="checkbox" />
                        <label id="lblAudit" class="CheckBoxLabel">
                            Audit</label>
                        <input id="chkCAP" class="MarginLeft20" type="checkbox" />
                        <label id="lblCAP" class="CheckBoxLabel">
                            Action Plan</label>

                    </div>

                    <div class="MarginTop10">
                        <label id="lblActive" class="ContentCustomLabel">
                            This Tab is active</label>
                    </div>
                    <div class="MarginTop10">
                        <input type="checkbox" class="MarginLeft20" id="chkActive" checked="checked" />
                    </div>
                    <div id="divRestrictAccess">
                         <div class="MarginTop10">
                            <label id="Label1" class="ContentCustomLabel">
                                Restricted Access
                            </label>
                        </div>
                        <div class="MarginTop10">
                            <input type="checkbox" class="MarginLeft20" id="chkRestrictedAccess" />
                            <a href="javascript:void(0)" id="ancRestrictUser" style=" margin-left:10px; display:none;">Permission</a>
                        </div>
                        
                    </div>

                    <div class="MarginTop10">
                        <label id="xxx" class="ContentCustomLabel">&nbsp;
                            </label>
                    </div>
                    <div class="MarginTop10">
                       <div style="float:left;margin-left:15px;">
                        
                        <input type="button" class="btn btn-primary btn-small btn-float" id="btnSaveTab" value="Create" />
                        <input type="button" class="btn btn-primary btn-small btn-float" name="btnAddField" id="btnAddField"
                            value="Add New Field" />
                        <input type="button" class="btn btn-primary btn-small btn-float" name="btnDeleteTab" id="btnDeleteTab"
                            value="Delete Tab" />
                    </div>
                    </div>
                   
                </div>
            </div>
            <div id="TabField" class="ClearAll Overflow">
                <div id="divUserPreviewFields" class="ContentBorder MarginTop20">
                    <div class="blueHeader">
                        User Preview
                    </div>
                    <div class="MarginTop30 hidden" style="margin-left: 10px;" id="detailsTabBox">
                        <div>
                            <label id="lblName" class="ContentUserLabel">
                                Event Name<span id="spnName" style="color: Red">*</span></label>
                        </div>
                        <div>
                            <input type="text" disabled="disabled" id="txtEventName" class="ContentInputTextBox WidthP40 trackChanges" />
                        </div>
                        <div class="MarginTop10">
                            <label id="lblDate" class="ContentUserLabel EventTypeDate" runat="server">
                                Incident Date<span id="spnIncDate" style="color: Red">*</span>
                            </label>
                            <input type="text" disabled="disabled" style="width: 14.8%;" class="DateInputTextBox trackChanges " />
                            <label id="lblEventTime" class="ContentDateTimeLabel lblEventTime" runat="server">
                                Time
                            </label>
                            <input type="text" disabled="disabled" style="width: 14.8%;" class="DateInputTextBox trackChanges" />
                        </div>
                        <div class="MarginTop10">
                            <label id="lblLocation" class="ContentUserLabel">
                                Location<span id="Span2" style="color: Red">*</span></label>
                        </div>
                        <div>
                            <label id="lblEventLocationData" runat="server" clientidmode="Static" class="EventImageLabel">
                            </label>
                        </div>
                        <div class="FloatLeft PaddingLeft20 Width120">
                            <img alt="OpenLink" id="lnkEventLocation" aria-readonly="true" src="<%=ResolveUrl("../Images/OpenLink.png") %>" tabindex="4" />
                        </div>
                        <div class="MarginTop10 ClearAll" style="width: 150px;">
                            <label id="lblClassification" class="ContentUserLabel">
                                Classification<span id="Span1" style="color: Red">*</span></label>
                        </div>
                        <div>
                            <label id="lblEventClassificationData" runat="server" clientidmode="Static" class="EventImageLabel">
                            </label>
                        </div>
                        <div class="FloatLeft PaddingLeft20">
                            <img alt="OpenLink" aria-readonly="true" id="lnkEventClassification" src="<%=ResolveUrl("../Images/OpenLink.png") %>" tabindex="5" />
                        </div>
                        <div class="MarginTop20 ClearAll">
                            <label id="lblStatus" class="ContentUserLabel">
                                Status</label>
                        </div>
                        <div class="MarginTop10">
                            <select id="ddlStatus" disabled="disabled" style="width: 40.5%" class="ContentInputTextBox WidthP40 trackChanges"
                                runat="server">
                                <option value="1">Active</option>
                                <option value="2">Archived</option>
                                <option value="3">Completed</option>
                            </select>
                        </div>
                    </div>
                    <div id="FieldInfo" class="ContentWidth">
                    </div>

                </div>
                <div id="divInactiveFields" class="ContentBorder MarginTop30">
                    <div id="divInactiveFieldsHeader" class="blueHeader">
                        Inactive Fields
                    </div>
                    <div id="DeletedFieldInfo" class="ContentWidth">
                    </div>
                </div>
            </div>
            <div id="div1" class="MarginTop30">
            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>
        <!--for field tree view popin -->
        <div class="demo">
            <div id="divCustomTreeviewDialog" class="hiddenDiv" title="">
                <div id="ListTreeViewId" class="ContentHeader">
                </div>
                <div class="CenterTree notranslate">
                    <div id="FieldTreeviewList" class="treeViewSection">
                    </div>
                </div>
                <div class="MarginTop10 PaddingRight120">

                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnListTreeCancel" value="Cancel" />

                </div>
            </div>
        </div>
        <!-- for field popin -->
        <div class="demo">

            <div id="divAddEditFieldsDialog" class="hiddenDiv" title="Add Fields">

                <div class="ErrorLabel PaddingLeft50">
                    <label id="FieldErrorMsg">
                    </label>
                </div>
                <div class="MarginTop10 PaddingLeft50">
                    <label id="lblFieldName" class="ContentLabel">
                        Field Name</label>
                </div>
                <div class="MarginTop10 ClearAll PaddingLeft50">
                    <input type="text" class="ContentInputTextBox WidthP40" style="width: 60%;" id="txtFldName" />
                </div>
                <div class="MarginTop10 PaddingLeft50">
                    <label id="lblDescription" class="ContentLabel">
                        Description</label>
                </div>
                <div class="MarginTop10 ClearAll PaddingLeft50">
                    <textarea rows="2" cols="20" class="ContentInputTextBox WidthP40" style="width: 60%;" name="txtDescription"
                        id="txtDescription"></textarea>
                </div>
                <div class="MarginTop10 ClearAll PaddingLeft50">
                    <label id="lblDataType" class="ContentLabel">
                        Data Type</label>
                </div>
                <div class="MarginTop10 ClearAll PaddingLeft50">
                    <select id="ddDataType" class="ContentInputTextBox WidthP40" style="width: 60%;">
                        <option value="Select">Select</option>
                        <option value="ChooseFromAList">Choose from a list</option>
                        <option value="Label">Label</option>
                        <option value="DateTime">Date</option>
                        <option value="Number">Number</option>
                        <option value="Text">Text</option>
                        <option value="Yes/No">Yes/No</option>
                    </select>
                </div>
                <div id="ControlType">
                    <div class="MarginTop10 ClearAll PaddingLeft50">
                        <label id="lblControlType" class="ContentLabel">
                            Control Type</label>
                    </div>
                    <div class="MarginTop10 ClearAll PaddingLeft50">
                        <select id="ddControlType" class="ContentInputTextBox WidthP40" style="width: 60%;">
                            <option value="Select">Select</option>
                            <option value="TextBox">Text Box</option>
                            <option value="Label">Label</option>
                            <option value="ParagraphText">Paragraph Text</option>
                            <option value="DateField">Date Field</option>
                            <option value="RadioButton">Radio Button</option>
                            <option value="SelectOneFromList">Select one from list</option>
                            <option value="SelectMultipleFromList">Select multiple from list</option>
                        </select>
                    </div>
                </div>
                <div id="DatalistDisplay">
                    <div class="MarginTop10 ClearAll PaddingLeft50">
                        <label id="lblDataList" class="ContentLabel">
                            Data List</label>
                    </div>
                    <div class="MarginTop10 ClearAll PaddingLeft50">
                        <select id="ddDataList" class="ContentInputTextBox WidthP40" style="width: 60%;">
                        </select>
                    </div>
                </div>
                <div id="MaxLength">
                    <div class="MarginTop10 ClearAll PaddingLeft50">
                        <label id="lblMaxLength" class="ContentLabel">
                            Max Length</label>
                    </div>
                    <div class="MarginTop10 ClearAll PaddingLeft50">

                        <input type="text" class="ContentInputTextBox WidthP40" style="width: 60%;" id="txtMaxLen" />
                    </div>
                </div>

                <div class="MarginTop10 PaddingLeft50">
                    <input type="checkbox" id="chkIsRequired" class="MarginLeft20 ContentCheckBox" />
                    <label id="lblIsRequired" class="ContentLabel">
                        Recommended</label>
                </div>
                <br />

                <div class="MarginTop10 PaddingLeft50">
                    <div></div>
                    <input type="checkbox" id="chkIsActive" class="MarginLeft20 ContentCheckBox" />
                    <label id="lblActiveField" class="ContentLabel">
                        This Field is Active</label>
                </div>
                <br />
                    <div style="width: 100%; margin-top: 10px;">
                <div class="buttonCenter">
                    <input type="button" class="btn btn-primary btn-small btn-float" style="text-align: left; float: left" id="btnCancel" value="Cancel" />
                </div>
                <div class="buttonCenter">
                    <input type="button" id="btnDelete" style="float: left; text-align: right" value="Delete" class="btn btn-primary btn-small btn-float" />
                </div>
                <div class="buttonCenter">
                    <input type="button" id="btnSave" style="float: right; text-align: right" value="Create" class="btn btn-primary btn-small btn-float" />
                </div>

            </div>
           
            </div>
        </div>
        <div class="demo">
            <div id="divPopupMessage">
            </div>
        </div>
        <div id="divRestrictUserListPopUp" class="hidden" title="Access Users">
            <div class="RestrictUserListGrid divRestrictUserGridPopUp">
                <input type="text" class="span4 bootstrp1" id="txtSearchRestrictUser" placeholder="Search Users" title="Search" autofocus />
                <div id="divRestrictUserListGrid">
                    <table id="RestrictUserListGrid" class="scroll notranslate">
                        <tr>
                            <td />
                        </tr>
                    </table>
                    <div id="RestrictUserListGridNavigation" class="scroll" style="text-align: center;">
                    </div>
                </div>
                <div class="btnCloseRestrictPopUp">
                    <input type="button" class="btn btn-primary btn-small" id="closeRestrictUserPopUp" value="Cancel" style="float: right;" />
                </div>
            </div>
         </div>

    </div>
    

</asp:Content>
