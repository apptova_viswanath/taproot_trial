﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="UserDetails.aspx.cs" Inherits="tap.users.UserDetails" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true"
    CodeBehind="UserDetails.aspx.cs" Inherits="tap.users.UserDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.validate/PasswordPolicy.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';
        var _sessionLocationId = '';
        _sessionLocationId = '<%= Session["LocationId"] %>';
        function setSessionStorage() {

            var _baseURL = '<%= ResolveUrl("~/") %>';
            sessionStorage.Change = 'true';
            window.location = _baseURL + 'ChangePassword';
        }
    </script>

    <style type="text/css">
        label {
            width: 100%;
        }


        .ContentInputTextBox {
            width: 300px;
        }

        .divLabel {
            float: left;
        }

        .ContentWidth {
            margin: 10px 0 10% 5%;
        }

        #body_chkActive {
            margin-left: 22px;
        }

        .divButton {
            float: left;
            margin-left: 240px;
            text-align: center;
            width: 40%;
        }

        .ContentInputTextBox {
            width: 290px !important;
        }

        .divLabel label {
            width: 200px !important;
        }

        /*#pswd_info {
	position:absolute;
    margin-left:30px;
	width:250px;
	padding:15px;
	background:#fefefe;
	font-size:.875em;
	border-radius:5px;
	box-shadow:0 1px 3px #ccc;
	border:1px solid #ddd;
}

#pswd_info::before {
	content: "\25B2";
	position:absolute;
	top:-12px;
	left:45%;
	font-size:14px;
	line-height:14px;
	color:#ddd;
	text-shadow:none;
	display:block;
}
.invalid {
	background:url('../../../Images/Delete.png') no-repeat 0 50%;
	padding-left:22px;
	line-height:24px;
	color:#ec3f41;
}
.valid {
	background:url('../../../Images/green-check.png') no-repeat 0 50%;
	padding-left:22px;
	line-height:24px;
	color:#3a7d34;
}
#pswd_info {
	display:none;
}*/
    </style>

    <input type="hidden" id="hiddenUserId" name="hiddenUserId" value="" runat="server" />
    <input type="hidden" id="hiddenIsUserHasEvent" name="hiddenIsUserHasEvent" value="" runat="server" />
    <input type="hidden" id="hiddenIsUserActive" name="hiddenIsUserActive" value="" runat="server" />
    <input type="hidden" id="hiddenPassword" name="hiddenPassword" value="" runat="server" />
    <div class="siteContent adminSiteContent">
        <div id="divErrorMessage" runat="server">
            <label id="lblErrorMessage" class="ContentUserLabel"><span id="spnName" style="color: Red">Sorry! The user account you are looking for does not exist.</span></label>
        </div>
        <div id="tabs" runat="server">
            <br />
            <div class="UserLabel ClearAll" id="divUserLabel">
                <div id="divUndoUserDetails" style="text-align: left; display: none;">
                    <a id="ancUsersList">Back to Users List</a>&nbsp;
                    <%--                    <a onclick="javascript:EventUndo();" href="#" id="ancUndo">Undo</a>&nbsp; <a href="javascript:EventUndoAll();"
                        id="ancUndoAll">Undo All</a>--%>
                </div>
                <div class="ContentWidth ClearAll">
                    <br />
                    <div class="divLabel ClearAll">
                        <label id="lblName" class="ContentUserLabel">First Name<span id="spnFirstName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtFirstName" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="FirstName" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label1" class="ContentUserLabel">Last Name<span id="spnLastName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtLastName" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="LastName" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>

                    <div class="divLabel ClearAll">
                        <label id="Label3" class="ContentUserLabel">Email<span id="spnEmail" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtEmail" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="Email" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label4" class="ContentUserLabel">Phone</label>
                    </div>
                    <div class="divLabel">
                        <input id="txtPhone" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="PhoneNo" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label5" class="ContentUserLabel">User Name<span id="spnUserName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtUserName" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="UserName" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>
                    <div id="divPassword" runat="server">
                    <div class="divLabel ClearAll">
                        <label id="Label6" class="ContentUserLabel">Password<span id="spanPassword" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtPassword" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="password" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="Password" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                        <%-- <a href="" id="userPasswordPolicy" style="font-size:12px; display:none;">Password Policy</a>--%>
                        <div id="pswd_info" style="margin-left: 12px">
                            <h4>Password must meet the following requirements:</h4>
                            <br />
                            <ul>

                                <li id="lowerCase" class="invalid">At least <strong><span id="spnCountLowerCase">1</span> lowercase letter<span id="lowerS">s</span></strong></li>
                                <li id="upperCase" class="invalid">At least <strong><span id="spnCountUpperrCase">1</span> uppercase letter<span id="upperS">s</span></strong></li>
                                <li id="number" class="invalid">At least <strong><span id="spnCountNumer">1</span> number<span id="numberS">s</span></strong></li>
                                <li id="length" class="invalid">Be at least <strong><span id="spnLength">6</span> character<span id="minS">s</span></strong></li>
                                <li id="maxCharacter" class="invalid">No longer than <strong><span id="spnMaxCharacterCount">6</span> character<span id="maxS">s</span></strong></li>
                                <li id="specialCharacter" class="invalid">At least <strong><span id="spnSpecialCharacterCount">6</span> special character<span id="specialS">s</span></strong></li>
                            </ul>
                        </div>
                    </div>

                    <div class="divLabel ClearAll">
                        <label id="Label2" class="ContentUserLabel">Confirm Password<span id="span7" style="color: Red">*</span></label></div>
                    <div class="divLabel">
                        <input id="txtConfirmPassword" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="password" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                            data-entity-property="Password" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                            data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="UserID" />
                    </div>
                    </div>
                    <div class="divLabel ClearAll" style="display: none;">
                        <label id="lblCompanyID" class="ContentUserLabel">CompanyID</label>

                    </div>
                    <div class="divLabel" style="display: none;">
                        <input id="txtCompanyID" type="text" runat="server" />
                    </div>



                    <%--subscription details for Gobal admin--%>
                    <%-- kvs --%>
                   <%-- <div id="divSubscriptiondetails" runat="server">
                        <div class="divLabel ClearAll">
                            <label id="SubStart" class="ContentUserLabel">Subscription Start<span id="spnSubStart" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtSubscriptionUserStart" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                                data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                                data-entity-property="FirstName" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                                data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                                data-entity-sec-queryfield="UserID" />
                        </div>

                        <div class="divLabel ClearAll">
                            <label id="SubEnd" class="ContentUserLabel">Subscription End<span id="spnSubEnd" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtSubscriptionUserEnd" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="text" runat="server"
                                data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                                data-entity-property="FirstName" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                                data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                                data-entity-sec-queryfield="UserID" />
                        </div>

                    </div>

                    <br />--%>
                    <%-- kve --%>

                    <br />
                    <div id="divUserActive">
                        <div class="divLabel ClearAll">
                            <label id="lblStatus" class="ContentUserLabel">Active<span id="Span2" style="color: Red"></span></label>

                        </div>
                        <div class="divLabel">
                            <input type="checkbox" runat="server" checked="checked" id="chkActive" />

                        </div>
                    </div>
                    <div id="divUserAdminSection">
                        <div class="divLabel ClearAll" id="divlblAdmin">
                            <label id="Label7" class="ContentUserLabel">Admin<span id="Span1" style=""></span></label>

                        </div>
                        <div class="divLabel" id="divChkAdmin">
                            <input type="checkbox" runat="server" id="chkAdmin" />

                        </div>
                    </div>
                    <div id="divUserDetailsEventAccess" style="display: none;">
                        <div class="divLabel ClearAll" id="divlblMyEvents">
                            <label id="Label8" class="ContentUserLabel">My Events Only<span id="Span3" style=""></span></label>

                        </div>
                        <div class="divLabel" id="divChkMyEvents">
                            <input type="checkbox" runat="server" id="chkMyEventOnly" checked="true" onclick="return false;" />

                        </div>
                        <div class="divLabel ClearAll" id="divlblViewAllEvents">
                            <label id="Label9" class="ContentUserLabel">View All Event for My Division<span id="Span4" style=""></span></label>

                        </div>
                        <div class="divLabel" id="divChkViewAllEvents">
                            <input type="checkbox" runat="server" id="chkViewAllEvents" />

                        </div>
                        <div class="divLabel ClearAll" id="divlblEditAllEvents">
                            <label id="Label10" class="ContentUserLabel">Edit All Event for My Division<span id="Span5" style=""></span></label>

                        </div>
                        <div class="divLabel" id="divChkEditAllEvents">
                            <input type="checkbox" runat="server" id="chkEditAllEvents" />

                        </div>
                    </div>
                    <div class="divLabel ClearAll" id="divSubscription" runat="server">
                        <label id="lblSubscription" class="ContentUserLabel">Subscription <span id="Span6" style=""></span></label>

                    </div>
                    <div class="divLabel" id="divSubscriptionDate" runat="server" style="margin-left: 20px;">
                        Subscription is active until 
                        <label id="lblSubscriptionDate" runat="server" style="color: blue;"></label>
                    </div>
                    <div id="passwordExpiryDetails" runat="server">
                        <div class="divLabel ClearAll" id="div1" runat="server">
                            <label id="lblExpiry" class="ContentUserLabel">Password Expiration <span id="Span7" style=""></span></label>

                        </div>
                        <div class="divLabel" id="PasswordNotExpired" runat="server" style="margin-left: 20px;" clientidmode="Static">
                            Password expires in  
                            <label clientidmode="Static" id="lblPasswordexpiresIn" runat="server" style="color: blue;"></label>
                        </div>
                        <div class="divLabel" id="PasswordExpired" runat="server" style="margin-left: 20px;" clientidmode="Static">
                            Password  expired
                            <label clientidmode="Static" id="lblPasswordexpiredIn" runat="server" style="color: blue;"></label>
                            days ago
                        </div>
                    </div>

                    <div style="clear: both">
                    </div>
                    <br />


                    <div class="divButton">
                        <%-- <a class="btn btn-primary btn-small" href="#" id="btnSaveUserDetails">Create</a>
                    <a class="btn btn-primary btn-small" href="#" id="btnDeleteUserDetails">Delete</a>
                    <a class="btn btn-primary btn-small" href="#" id="btnSaveUserDetailsCancel" style="display:none;">Clear</a>--%>
                        <input type="button" class="btn btn-primary btn-small" value="Apply Changes" id="btnSaveUserDetails" />
                        <input type="button" class="btn btn-primary btn-small" value="Delete" id="btnDeleteUserDetails" style="display: none;" />
                        <%--<asp:Button  ClientIDMode="Static" runat="server" Text="" style="display:none" Height="0px" Width="0px"  id="calculateexirydays" OnClick="calculateexirydays_Click"  /> --%>
                    </div>

                    <script id="ForgotPasswordJS" type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.policy/ValidPasswordPolicy.js") %>">
        {
            "txtPassword":"body_Adminbody_txtPassword", 
            "txtConfirmPassword":"body_Adminbody_txtConfirmPassword"                
        }
                    </script>
                    <!-- Page Scripts -->
                    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.usr.edt.js") %>" type="text/javascript"> </script>

                </div>

                <div class="demo hidden">
                    <div id="divUserPasswordPolicy" title="Password Policy">

                        <div class="CenterTree">
                            <%--<h4>Password Policy</h4><br />--%>
                            <div id="divUserPasswordPolicyContent">
                            </div>
                        </div>
                        <div class="MarginTop30" style="text-align: center;">
                            <input type="button" class="btn btn-primary btn-small" value="Cancel" id="btnUserPswPolicyCancel" />

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>




</asp:Content>
