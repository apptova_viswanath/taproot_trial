﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true"
    CodeBehind="AdminSystemList.aspx.cs" Inherits="tap.ui.adm.CustSystemList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />

    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.sys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <style>
        /*remove folder icon*/
        .jstree-default a ins.jstree-icon {
            display: none;
        }

        .jstree-rename-input {
            left: 17px !important;
        }

        .ui-widget {
            /*font-size: 11px;*/
        }

        #divSettings div p {
            margin-left: 7px;
        }

        #imgSetting {
            float: right;
        }

        .rowColor {
            color: silver;
        }

        #divSystemListTreeviewDialog {
            font-size: 12px;
            overflow: hidden;
        }
    </style>
    <input id="txtSysListID" type="hidden" value="" />
    <div class="TapRooTContent">
        <div id="SystemGrid">
            <div class="SystemListGrid">
                <div class="PaddingTop5">

                    <div class="addDiv" id="addIcon">

                        <a id="spnAddSystemList" href="">
                            <img class="addImage" src="../../Images/add.png" alt="Demo">
                            Add List</a>

                    </div>
                </div>

                <table id="SystemListGrid" class="scroll ClearAll notranslate">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="SystemListNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>
        </div>
    </div>
    <div class="backgroundPopup">
    </div>
    <div id="divSystemListTreeviewDialog" class="hidden" title="Lists">
        <div>
            <label id="SystemErrorMsg">
            </label>
        </div>

        <img id="imgSetting" style="cursor: pointer" src="<%=ResolveUrl("../Images/settings-black.png") %>" />

        <div id="systemItems">

            <div class="MarginTop10">
                <div id="TreeviewSystemListBox">

                    <div class="CenterAttachmentTree">
                        <div id="systemTreeviewList" class="sysTreeviewList">
                        </div>
                    </div>

                </div>
            </div>

            <div class="MarginTop10 ClearAll MarginRight20">
                <input type="button" class="btn btn-primary btn-small btn-float" id="btnDeleteSystemTreeItem" value="Delete" />
                <input type="button" class="btn btn-primary btn-small btn-float" id="btnInActivateListItem" value="Inactivate" />
                <input type="button" class="btn btn-primary btn-small btn-float" id="btnNewSystemTreeItem" value="Add item" />
            </div>
        </div>
        <div id="divSettings" class="MarginTop30 hidden">
            <div class="MarginLeft10">
                <div class="MarginTop10">
                    <label id="lblAddListName1" class="ContentUserLabel">
                        List Name
                    <span id="spnListName1" style="color: Red">*</span>
                    </label>
                </div>
                <div>
                    <input type="text" class="ContentInputTextBox WidthP40" style="width: 80%;" id="txtEditSystemListName" />
                   
                </div>
                <div class="MarginTop10">
                    <label id="lblDescription1" class="ContentUserLabel">
                        Description</label>
                </div>
                <div>
                    <textarea class="ContentInputTextBox WidthP40" style="width: 80%; height: 90px;" id="txtEditSystemListDescription"></textarea>
                </div>
                <div style="float: left; margin:25px 0px 0px 40px;">
               
                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnDeleteList" value="Delete" />
                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnInActivateList" value="Inactivate" />
                    <input type="button" id="btnEditSystemList" value="Apply Changes" class="btn btn-primary btn-small btn-float" />
                </div>
            </div>

        </div>

    </div>
      <div id="divAddEditSystemListDialog" class="hidden" title="Add System List">
        <div>
            <label id="addSystemErrorMsg">
            </label>
        </div>
        <div class="MarginLeft10">
            <div class="MarginTop10">
                <label id="lblAddListName" class="ContentUserLabel">
                    List Name
                    <span id="spnListName" style="color: Red">*</span>
                </label>
            </div>
            <div>
                <input type="text" class="ContentInputTextBox WidthP40" style="width: 80%;" id="txtNewSystemListName" />
                <img id="validationListNameImg" class="validationImg" title="Please specify System List Name" src="" />
            </div>
            <div class="MarginTop10">
                <label id="lblDescription" class="ContentUserLabel">
                    Description</label>
            </div>
            <div>
                <textarea class="ContentInputTextBox WidthP40" style="width: 80%; height: 90px;" id="txtNewSystemListDescription"></textarea>
            </div>
        </div>
         <div style="width: 100%;">
            <div id="" class="buttonCenter">
                <input type="button" class="btn btn-primary btn-small btn-float" style="text-align: left; float: left" id="btnCancelSystemList" value="Cancel" />
            </div>
            <div class="buttonCenter">
                <input type="button" id="btnAddSystemList" style="float: right; text-align: right" value="Select" class="btn btn-primary btn-small btn-float" />
            </div>
        </div>

    </div>
</asp:Content>
