﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/tap.mst/Master.Master" CodeBehind="SiteSettings.aspx.cs" Inherits="tap.adm.SiteSettings" %>


<asp:Content ContentPlaceHolderID="body" runat="server">

       <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
    <!--JQGrid Script -->
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <!--Notification -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.site.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
    <style>
        /*remove folder icon*/
        .jstree-default a ins.jstree-icon {
            display: none;
        }

        .jstree-rename-input {
            left:17px !important;
        }

        .ui-widget {
            font-size: 11px;
        }
        .ContentInputTextBox
        {
            padding: 5px 0 5px 5px !important;
            margin-left:30px !important;
        }
        .ContentMargin
        {
            float: left  !important;
            margin-left: 120px  !important;
            min-width: 160px  !important;
        }
        #body_chkTimeFormat
        {
              margin-left: 9px  !important;
        }
    </style>
   

    <div class="siteContent">
    
           <div class="ContentWidth ClearAll">

        <div class="MarginTop10 ClearAll">

            <asp:HiddenField runat="server" ID="hdnSettingId" />
          
        </div>
        <div class="MarginTop10">
            <label id="Label1" class="ContentUserLabel">
                Date Format :
            </label>
            <div class="MarginTop10">
                 <%--<input id="txtDateFormat" class="ContentInputTextBox"  type="text" runat="server" autofocus=""/>--%> 
                 <select id="cmbDateFormat"  class="ContentInputTextBox WidthP15" runat="server" autofocus="">
                        <option value="">-- Select --</option>
                        <option value="dd/MM/yy">dd/MM/yy</option>
                        <option value="MM/dd/yy">MM/dd/yy</option>
                        <option value="MMM dd yyy">MMM dd yyy</option>
                        <option value="dd-MM-yy">dd-MM-yy</option>
                        <option value="dd-M-yy">dd-M-yy</option>
                    </select>
                <span id="spanExampleDateFormat"> Example: <label id="DateFormatExample"></label></span>            
            </div>
        </div>


        <div class="MarginTop10">
            <label id="Label2" class="ContentUserLabel">
                Twelve Hour Time Format :
            </label>
              <div class="MarginTop10">
           <%-- <input id="chkTimeFormat" type="checkbox" runat="server" />--%>
                  <select id="cmdTimefFormat" class="WidthP15 fieldFontFamily notranslate" runat="server" autofocus="">
                        <option value="0">-- Select --</option>
                        <option value="12">12 hour</option>
                        <option value="24">24 hour</option>
                    </select>
            </div>
          
        </div>
        <div class="MarginTop10">
            <label id="Label3" class="ContentUserLabel">
                Application Time Out :
            </label>
              <div class="MarginTop10">
             <input id="txtTimeOut" class="ContentInputTextBox"  type="text" runat="server" />    
            </div>
          
        </div>

        <%-- <br />--%>
         <div class="ContentMargin">
            <input type="button" class="btn btn-primary btn-small btn-float" id="btnSave" value="Save" />
        </div>
        </div>
      </div>
      
  


</asp:Content>