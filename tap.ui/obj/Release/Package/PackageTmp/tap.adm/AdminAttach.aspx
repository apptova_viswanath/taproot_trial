﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="AdminAttach.aspx.cs" Inherits="tap.attachment.AdminAttach" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true"
    CodeBehind="AdminAttach.aspx.cs" Inherits="tap.attachment.AdminAttach" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
<%--    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />--%>

    <!--Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.attachment/js.srt.atch.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
    <style>
        .jstree-default.jstree-focused
        {     
            font-family: Verdana,Arial,sans-serif;
            font-size: 1.1em;          
        }
      
        .jstree-default > ul {
            max-width: 340px !important;
            overflow-y: visible !important;
        }
    </style>
    <div class="siteContent NoOverflow adminSiteContent">
        <div class=" NoOverflow" style="min-height: 600px;">
            <div>
                <label id="AttachFolderErrorMsg">
                </label>
                <asp:Label ID="UploadErrorMsg" runat="server"></asp:Label></div>
            <div>
                <div class="MarginTop10">
                    <div id="TreeviewFolderBox">
                        <div class="CenterAttachmentTree notranslate">
                            <div id="TreeFolderList">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="FolderButtonItems" class="ClearAll FloatLeft PaddingLeft50">
                <input type="button" class="btn btn-primary btn-small btn-float" id="btnDeleteTreeFolder" value="Delete Folder" />
                <input type="button" class="btn btn-primary btn-small btn-float" id="btnNewTreeFolder" value="Add Folder" />
            </div>
        </div>
    </div>
</asp:Content>
