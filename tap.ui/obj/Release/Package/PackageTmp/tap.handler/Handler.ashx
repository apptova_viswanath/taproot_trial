﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.Script;
using System.Web.Security;
using tap.dat;
using Microsoft.Win32;
public class Handler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";//"application/json";
        var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
        JavaScriptSerializer js = new JavaScriptSerializer();  
        var AttachIdcookie = (HttpCookie)HttpContext.Current.Request.Cookies["attachId"];
       // var AttachEventId = (HttpCookie)HttpContext.Current.Request.Cookies["incEvtId"];
        var AttachEventId = (HttpCookie)HttpContext.Current.Request.Cookies["AttachEventId"];
        
        
        foreach (string file in context.Request.Files)
        {
            HttpPostedFile hpf = context.Request.Files[file] as HttpPostedFile;
            BinaryReader b = new BinaryReader(hpf.InputStream);
            string FileName = string.Empty;
            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
            {
                string[] files = hpf.FileName.Split(new char[] { '\\' });
                FileName = files[files.Length - 1];
            }
            else
            {
                FileName = hpf.FileName;
            }
           
         
         //Code for Saving the file in DB
            EFEntity db = new EFEntity();
            Attachments attachments = new Attachments();
           // attachments.EventID = Convert.ToInt32(AttachEventId.Value);
            
            //TODO: GET FROM URL!!! NO MORE COOKIES!
            attachments.EventID = 797;
            //THIS IS HARDCODED FOR DEV
            
           //Get the file extension and get the content type based on the extension
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            RegistryKey key = Registry.ClassesRoot.OpenSubKey(ext);
            string contentType = key.GetValue("Content Type").ToString();            
            attachments.AttachmentFolderID = Convert.ToInt32(AttachIdcookie.Value);
            attachments.DocumentName = FileName;
            attachments.ContentType = contentType;
            attachments.Extension = ext;
            attachments.AttachmentData = b.ReadBytes(hpf.ContentLength);
            db.AddToAttachments(attachments);
            db.SaveChanges();        
           
            r.Add(new ViewDataUploadFilesResult()
            {
                Name = FileName,
                Length = hpf.ContentLength,
                Type = hpf.ContentType
            });
            var uploadedFiles = new
            {
                files = r.ToArray()
            };
            var jsonObj = js.Serialize(uploadedFiles);
        }       
        context.Response.Redirect("~/tap.handler/index.html?action=close");
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}

public class ViewDataUploadFilesResult
{
    public string Thumbnail_url { get; set; }
    public string Name { get; set; }
    public int Length { get; set; }
    public string Type { get; set; }
}