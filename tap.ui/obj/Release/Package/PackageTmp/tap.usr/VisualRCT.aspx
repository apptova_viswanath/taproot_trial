﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisualRCT.aspx.cs" Inherits="tap.ui.usr.VisualRCT" %>
<%@ Register Src="~/tap.usr.ctrl/ucRCTNode.ascx" TagName="ucRCTNode" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucGenericCause.ascx" TagName="ucGenericCause" TagPrefix="uc" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Visual Root Cause Tree®</title>    
    
       <style type="text/css">

        html{
            background:none !important;
        }
        </style>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/TapRootTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet" type="text/css" />   
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/root.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />


    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />    
    <script src="<%=ResolveUrl("~/Scripts/jCanvas/jCanvas.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTAnalysisComments.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/GenericCause.js") %>" type="text/javascript"></script>

     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/DrawSections.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/DrawShapes.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/MainTreeNodeSection.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/OptionToolBar.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/Shadow.js") %>" type="text/javascript"></script>     
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/UpdateNodeStatus.js") %>" type="text/javascript"></script>

      <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';
       
        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

    </script>
   
    <style type="text/css">

        html{
            background:none !important;
        }
        #tabs
        {
            /*margin-top: 5px !important;*/
        }
        .siteContent
        {
            min-height: 0px !important;
        }
        .greenCheck {
            background-image: url('../Images/tick.png');
        }
    </style>


        <style type="text/css">
        .ui-widget-content a {
            /*color: #EDEDED;*/
        }

        .ui-widget-header {
            background: none !important;
            border: none !important;
            color: none !important;
            font-weight: normal !important;
        }

        .popupTitle {
            background: url('../../Scripts/jquery/images/ui-bg_highlight-soft_75_74a6e2_1x100.png') repeat-x scroll 50% 50% #74A6E2;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }

        .ui-tabs .ui-tabs-nav li {
            white-space: normal !important;
        }

        #content {
            overflow: hidden !important;
        }
       

        .siteContent {
            height: 100% !important;
            /*background: #f9f9f9 !important;*/
        }

        .ui-accordion .ui-accordion-header .ui-icon {
            left: 2em !important;
        }

        .selectPath {
            visibility: visible !important;
            display: block !important;
        }

        .unSelectPath {
            visibility: visible !important;
            display: block !important;
        }

        .USER_RESPONSE_NULL {
            visibility: visible !important;
            display: block !important;
        }

        #divLinkContent div {
            display: none;
        }
          #divLinkContent1 div {
            display: none;
        }
#divLinkContent a
{
     /* display: block;
    background-color: #000000;
    font-size: 12px;
    line-height: 30px;
    height: 30px;
    color: #ededed;
    text-decoration: none;
    padding: 0 10px;
    border:1px solid #272727;*/
    color: black !important;
    background-color: rgba(255, 255, 255, 0.2)  !important;

}

        #spnRestate {
            vertical-align: top;
            margin-left: 2px;
        }

        .restateImage {
            cursor: pointer;
            margin-left: 5px;
            margin-top: 5px;
        }

        #lblNone {
            display: none;
            margin-left: 2px;
        }

        .genericQuestion {
            margin-left: 100px;
        }

        .textArea {
            width: 455px;
            height: 170px;
            margin-left:0px !important;
        }

        .buttonGeneric {
            text-align: center;
        }

        #divGettingStartedHeader {
            float: left;
            clear: both;
        }

        #divLinkContent {
            clear: both;
        }
         #divLinkContent1 {
            clear: both;
            margin-left: 25px;
            margin-top:10px;
        }
         
#divLinkContent1 a
{
    display: block;
    background-color: #272727;
    /*font-size: 12px;*/
    line-height: 30px;
    height: 30px;
    color: #ededed;
    text-decoration: none;
    padding: 0 10px;
    border:1px solid #272727;
    cursor:pointer;
    font-weight:bold;
    
}
        #divAccordion {
            float: left;
        }

        #tabs {
            /*margin-top:40px;*/
            
            /*border: 1px solid #b1b1b1;*/
        }

        #divCausalFactor {
            /*font-family: Verdana;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-top: 5px;
            color: #0080C0;*/
             background-color: #8ab4e6;
            font-family: Verdana;
            margin-bottom: 10px;            
            /*margin-top: 5px;*/
            padding-bottom: 5px;
            padding-left: 5px;
            padding-top: 5px;
            /*position: fixed !important;*/
            top: 0;
            width: 97%;
            z-index: 999;
        }

        #spnHeader {
            margin-left: 5px;
            font-weight: bold;
        }

        .analysisComments {
            display: none;
        }

        #lblQuestion2 {
            word-wrap: break-word;
        }

        .side {
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .top {
            border-top: 30px solid #000000;
            padding-top: 3px;
        }

        .bottomSection {
            border-bottom: 3px solid black !important;
        }

        .bottom {
            border-bottom: 0 solid #000000;
            padding-bottom: 2px;
        }


        .accordionHeader {
            margin-top: -1px !important;
        }

        .accordioncontent {
            margin-bottom: 1px !important;
        }

        .lblTextHeader {
            color: #FFFFFF;
            float: left;
            font-weight: bold;
            margin-left: 6px;
            margin-top: -25px;
            position: relative;
            display: none;
        }

        .container {
            border-bottom: 3px solid #000000;
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .basicCauseHeader {
            background-color: #000000;
            color: #FFFFFF;
            height: 28px;
            margin-left: -5px;
            margin-right: -5px;
        }

        #divGenericRestate {
            display: none;
            word-wrap: break-word !important;
            overflow: auto !important;
            height: auto !important;
        }


        .MsoNormal a {
            color: black !important;
        }

        .MsoNormal {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            font-size: 13px !important;
            line-height: 18px !important;
            margin-top: 10px !important;
        }

            .MsoNormal span p {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

            .MsoNormal span {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

        #divDic {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
        }

            #divDic a {
                color: black !important;
              text-decoration: underline;
              cursor:pointer !important;
            }
        
         #divDic p
        {
            margin-bottom: 10px !important;
        }
        /*Testing COMMIT 1*/

        /*body{ overflow-x:hidden;overflow-y:hidden; }*/

        .btn-small {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    padding-bottom: 2px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 2px;
}
.btn-primary {
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-color: #006dcc;
    background-image: linear-gradient(to bottom, #0088cc, #0044cc);
    background-repeat: repeat-x;
    border-bottom-color: rgba(0, 0, 0, 0.25);
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: rgba(0, 0, 0, 0.1);
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: rgba(0, 0, 0, 0.1);
    border-top-color: rgba(0, 0, 0, 0.1);
    color: #ffffff;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}
.btn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: #ccc;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: solid;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: 1px;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: #ccc;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: solid;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: 1px;
    border-top-style: solid;
    border-top-width: 1px;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
    display: inline-block;
    line-height: 18px;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
}
 input[type=text], select 
 {
     background: #ffffff;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
    font: italic 13px Georgia, "Times New Roman", Times, serif;
    outline: none;
    padding:5px 0px 5px 5px;
    margin-left:20px;
    color:#355779;
    box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
    border:1px solid #717171;
 }
    form textarea   
    {
         background: #ffffff;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        -khtml-border-radius: 3px;
        border-radius: 3px;
        font: italic 13px Georgia, "Times New Roman", Times, serif;
        outline: none;
        padding:5px 0px 5px 5px;
        margin-left:20px;
        color:#355779;
        box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
        border:1px solid #717171;
     }
    .unselectedBasicNode {
        color:black !important;cursor:default !important;border:  solid 1px #000000 !important;background-color:white !important;
    }
    .selectedBasicNode {
        color: black !important;cursor: default !important;border: solid 3px #008000 !important;background-color:white !important;
    }
  .nrct{

  }
        #questions {
            /*border-color:#EADDDD !important; *//*rgb(218, 232, 190);*/
            /* border-style:solid;
            border-width:10px;
            min-height:600px !important;*/

            background-color:lightgrey !important;
            float:left;
            margin-left:10px;
        }

        #divGettingStartedHeader
        {
            margin-top:10px !important;
            margin-left:2px !important;
        }
        #divRCTQuestions
        {
            margin-top: -70px !important;
        }
               /*.visualRCTContainer - FUTURE USE IN DESIGN RELATED TASKS
            {
                background-color: #EADDDD !important;
            }
                 #visualRCTWrapper
            {
                  background: #FFFFFF;
                padding: 10px;
                background-color: #EADDDD !important;
                overflow: auto;
                height: 100% !important;
                min-height: 0px !important;
            }*/

             .mainTreeNode{
                    z-index: 2;
                    top: 70px;
                    position: absolute !important;
                    word-break:break-all;
                    width: 100px;
                    text-align: center;
                    vertical-align: middle;
                    left: 65px;
               }
             .divRect{
                   position: absolute;
                  margin-left: 30px;
                  margin-top: -125px;
                  display: block;
                  z-index: 2;
                  word-break: normal;
                  width: 60px;
                  height: 60px;
                  text-align: center;
                  vertical-align: middle;
                  font-size:9px;
             }
             .div-admin-tabs a{
                 -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
box-shadow: 6px 5px 5px 0px rgba(0,0,0,0.75);
color:black !important;
             }

             .question{
                    margin-left:0px  !important;
                    display:table !important;
             }
             #divRadioButtons
             {
                 margin-left:-20px  !important;
             }
             .userSelect
             {
                 display:table !important;
                 width:auto !important;
                 margin-right: 15px !important;
             }
             .ui-accordion .ui-accordion-icons {
   padding-left: 0em !important; 
}
    </style>
</head>

<body> 


    <form id="form1" runat="server">

    </form>
 
    

    <input type="hidden" runat="server" id="hdnUserId" ClientIDMode="static"  />          
                 
     <uc:ucRCTNode ID="ucRCTNodes" runat="server" />

     <div id="content" style="margin-left:25px;">

                <div id="divLinkContent"  style="width:100% !important; ">                   

                    <div class="div-admin-tabs headerTab" id="div21"  style="float:left !important; overflow: visible;white-space: nowrap;width:500px !important; margin-bottom:60px!important; margin-right:40px!important;  ">
                        
                        <a name="rc5-,rc6-,rc8-,rc18-" id="rc21" class="nrct" style="color:black !important;cursor:default !important;width:180px !important">PROCEDURES</a>
                        <input type="hidden" id="hdnProcedures" />
                        <input type="hidden" id="hdnNodeType21" value="2"/>
                    </div>
                      <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;width:300px !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div48">
                      
                            <a name="rc8-,rc9-,rc11-,rc14-" id="rc48" class="nrct" style="color:black !important;cursor:default !important;width:150px !important">TRAINING</a>
                        <input type="hidden" id="hdnTraining" />
                          <input type="hidden" id="hdnNodeType48" value="2"/>
                    </div>
                    <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;width:300px !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div61">
                        
                        <a name="rc19-" id="rc61" class="nrct" style="color:black !important;width:160px!important; cursor:default !important;width:200px !important" >QUALITY CONTROL</a>
                        <input type="hidden" id="hdnQuality Control" />
                        <input type="hidden" id="hdnNodeType61" value="2"/>
                    </div>
                    
                     <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;clear:both;width:450px !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div70">
                       
                          <a name="rc13-,rc14-,rc15-" id="rc70"  class="nrct" style="color:black !important;width:160px  !important; cursor:default !important;">COMMUNICATIONS</a>
                        <input type="hidden" id="hdnCommunication" />
                         <input type="hidden" id="hdnNodeType70" value="2"/>
                    </div>
                     <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;width:600px !important; margin-bottom:60px!important; margin-right:40px!important;  "  id="div84">
                       
                          <a name="rc16-,rc17-,rc18-" id="rc84" class="nrct" style="color:black !important;width:190px  !important; cursor:default !important;">MANAGEMENT SYSTEM</a>
                        <input type="hidden" id="hdnManagement System" />
                         <input type="hidden" id="hdnNodeType84" value="2"/>
                    </div>
                   
                   
                    <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;clear:both;width:600px !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div107">
                       
                         <a name="rc4-,rc5-,rc7-,rc8-,rc10-,rc11-" id="rc107" class="nrct" style="color:black !important;width:290px!important; cursor:default !important;">HUMAN ENGINEERING</a>
                        <input type="hidden" id="hdnHuman Engineering" />
                        <input type="hidden" id="hdnNodeType107" value="2"/>
                    </div>

                    <div class="div-admin-tabs headerTab"  style="float:left !important; overflow: visible;white-space: nowrap;width:400px !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div133">
                         <a name="rc4-,rc5-,rc9-,rc11-,rc18-,rc16-,rc14-" id="rc133" class="nrct" style="color:black !important; width:190px  !important; cursor:default !important;">WORK DIRECTION</a>
                        <input type="hidden" id="hdnWork Direction" />
                        <input type="hidden" id="hdnNodeType133" value="2"/>
                    </div>

                     
                   
      
</div>
         </div>


    <div id="divGenericCauseLink" style="margin-left:25px !important;width:99%;float: left;display:none;border-bottom-style:dotted !important;
    height: 30px;
    list-style-image: none;
    list-style-position: outside;
    list-style-type: none;
    margin-bottom: 10px;
    margin-left: 0;
    margin-right: 1px;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;">

      
                        <a id="lnkGenericCause" class="focused" style="font-weight:bold !important;font-size:15px !important;">Causes</a>

                        <%--<div class='arrow-down'></div>--%>
                        <input type="hidden" id="Hidden1" value="Causes" />
                   
        
        <uc:ucGenericCause ID="ucGenericCauses" runat="server" />
    </div>


    <div id="divanalysisComments" class="analysisComments translate">


        <input type="hidden" id="hdnRCIDForAnalysisComments" value="" />
        <input type="hidden" id="hdnAnalysisComments" value="" />

        <span id="Span1">Enter Analysis Comments:</span>
        <br>
        <textarea rows="2" cols="100" class="textArea" id="txtAnalysisComments"></textarea>

        <div class="buttonGeneric">
            <br />

            <input type="button" class="btn btn-primary btn-small" id="btnSaveAnalysisComments" value="Apply Changes" />
            <input type="button" class="btn btn-primary btn-small" id="btnCancelAnalysisComments" value="Cancel" />
        </div>

    </div>

    
    <div id="divRootCauseQuestions" style="clear: both;">
        
        <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
        <script id="rctQuestionTemplate" type="text/x-jquery-tmpl">
            
            {{if Questions.length > 0}}
            <div style="height:10px;">&nbsp;</div>
                     <div id="divRadioButtons">
                         {{each Questions}}   
                                              
                               <div class="userSelect">
                                    <input type="radio" onchange="UpdateStatusAndSetEffects(this,event,${RCID});" id="rbYes" name="${QuestionID}" value="Yes" />Yes
                                    <input type="radio" onchange="UpdateStatusAndSetEffects(this, event,${RCID});" id="rbNo" name="${QuestionID}" value="No" />No
                                    <input type="hidden" id="hdnQuestionResponse" value="${QuestionResponse}" />
                                   
                               </div>

                         <div class="question">{{html  Question}}</div>
                         <%--<div style="height:26px;">&nbsp;</div>--%>

                         {{/each}}

                     </div>
            {{/if}}
            <input type="hidden" id="rcType" />
        </script>

        <ul>
            <li id="rctQuestionTemplateResults"></li>
        </ul>

    </div>

</body>

</html>
