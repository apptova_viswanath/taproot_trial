﻿<%@ Page Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="Share.aspx.cs" Inherits="tap.ui.usr.Share" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucPerson.ascx" TagPrefix="uc" TagName="ucPerson" %>


<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" lang="javascript">
        var _sessionLocationId = '';
        _sessionLocationId = '<%= Session["LocationId"] %>';    

    </script>

    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.share.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.person.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/ShareTab.css") %>" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        .ui-widget {
            font-size: 12px;
        }
        .caption{
            float:left;
            width:100%;
        }
    </style>
    <!--Main Tabs and Sub Tabs -->
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucevent" runat="server" />


            <div id="divShareTab" style="" class="ContentWidth ClearAll">
                <div class="PaddingTop5 width80">
                    <div class="addDiv FloatLeft">
                        <a id="ancAddTeamMember" href="">
                            <img id="imgAddTeamMember" class="addImage" src="<%=ResolveUrl("~/images/add.png")%>" alt="Add Team Member">
                            Add a Person

                        </a>
                    </div>
                </div>
                <div class="ShareListGrid width80" style="width: 70%;">
                    <table id="ShareListGrid" class="scroll notranslate">
                        <tr>
                            <td />
                        </tr>
                    </table>
                    <div id="SharepageNavigation" class="scroll" style="text-align: center;">
                    </div>
                </div>
  
                <div id="divMakeTeamMemberPopUp" class="hidden" title="Add Team Member">
                    <div>
                        <label id="addTeamMemberErrorMsg">
                        </label>
                    </div>
                    <div class="MarginLeft10">
                        <div style="width: 90%;">
                            <label id="lblAddListName" class="ContentUserLabel" style="width: 90%; display: none;">
                                Team Member options:</label>

                        </div>

                        <div style="width: 90%;" class="MarginTop30">
                            <input id="chkTMView" class="" type="checkbox" name="chkTM" />
                            <label id="lblADSecurity" class="CheckBoxLabel">
                                View</label>
                            <input id="chkTMEdit" class="MarginLeft20" type="checkbox" name="chkTM" />
                            <label id="lblCustomSecurity" class="CheckBoxLabel">
                                Edit</label>
                            <input id="chkTMDisplayOnReport" class="MarginLeft20" type="checkbox" name="" checked />
                            <label id="Label1" class="CheckBoxLabel">
                                Display on Report</label>
                        </div>
                    </div>
                    <div class="MarginTop30 ClearAll">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: right; padding-right: 5px;">
                                    <input type="button" class="btn btn-primary btn-small" id="btnAddTeamMember" value="Add" /></td>
                                <td style="width: 50%; padding-left: 5px;">
                                    <input type="button" class="btn btn-primary btn-small" id="btnCancelTeamMember" value="Cancel" /></td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
        <uc:ucPerson runat="server" ID="ucPerson" />
        <div class="tab-next-prev">
            <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
        </div>
    </div>
</asp:Content>

