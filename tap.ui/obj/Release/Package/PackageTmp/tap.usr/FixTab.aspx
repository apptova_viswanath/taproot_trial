﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixTab.aspx.cs" Inherits="tap.ui.usr.FixTab"
    MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucRootCauseTree.ascx" TagName="ucRCT" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucActionPlan.ascx" TagPrefix="uc" TagName="ucActionPlan" %>


<asp:Content ID="taprootTabContent" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Style sheets -->
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet"
        type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.tab.fix.js") %>" type="text/javascript"> </script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />

    <!--Action Plan -->
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.tab.correctiveaction.js") %>" type="text/javascript"> </script>
    <style>
   .ui-accordion .ui-accordion-header,#ui-accordion-divFixActionPlanAccordion-header-1,#ui-accordion-divFixActionPlanAccordion-header-0{padding:0px;}

     
    </style>

  
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucEventTabs" runat="server" />
            <asp:HiddenField ID="hdnStatusValue" Value="false" runat="server" ClientIDMode="Static" />

            <div id="divTabFix" class="ContentWidth marginTop50">

                <div class="PageTitle" id="fixHeader">
                    Create SMARTER Corrective Actions
                </div>

                <div style="width: 100%; margin-right: 230px; clear: both">

                    <div style="float: left; " class="pageUnderlineTitle">
                        <a id="lnkSmartCap" class="anchorStyle">Add Corrective Action to Action Plan</a>
                    </div>
                    <div style="float: right; margin-right: 267px; width: auto;">
                        <div class="greenSelectImage MarginRight20">
                            Addressed by Corrective Action
                        </div>
                        <div class="warningYellowImage">
                            Not Addressed by Corrective Action
                        </div>
                    </div>
                </div>
                <div id="divCausalFactorContent" style="margin-top: 20px;clear:both;">

                    <div id="divFixActionPlanAccordion">
                        <div class="paddingLeft20">
                            <h4><a style="margin-left:15px;">Causal Factors and Root Causes Identified</a></h4>
                        </div>
                        <div class="fixAccordion">
                            <uc:ucRCT ID="ucRCT" runat="server" />

                        </div>
                        <div class="paddingLeft20">
                            <h4>
                                <a style="margin-left:15px;">Corrective Actions</a></h4>
                        </div>
                        <div class="fixAccordion">

                            <uc:ucActionPlan runat="server" ID="ucActionPlan" />

                        </div>

                    </div>
                </div>
            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>

    </div>

</asp:Content>
