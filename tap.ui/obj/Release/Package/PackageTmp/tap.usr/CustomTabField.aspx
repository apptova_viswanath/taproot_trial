﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomTabField.aspx.cs"
    Inherits="tap.ui.usr.CustomTabField" MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucCustomFields.ascx" TagName="custField" TagPrefix="UcCustField" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="customfieldcontent" ContentPlaceHolderID="body" runat="server">

    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />


    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>"
        rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <!-- jQuery UI Datepicker 1.8.16 --->
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.gen.save.js") %>" type="text/javascript"></script>
    <style>
        #btnSelectEventClassification, #btnCustomCancel, #btnCustomTreeCancel, #btnUsrFieldTreeOk {
            color: #ffffff;
        }
    </style>
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucevent" runat="server" />

            <div>
                <UcCustField:custField ID="customField" runat="server" />

            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>


    </div>
</asp:Content>
