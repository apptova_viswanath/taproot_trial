﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report-Tab.aspx.cs" Inherits="tap.ui.usr.Report_Tab"
    MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="taprootTabContent" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />


    <!-- Page Scripts -->
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.tab.report.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.com.rpt.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        var seasonId = '<% = _seasonId %>';
        var isComplete = '<% = _isComplete %>';
        var isAnyRCTCompleted = '<% = _isAnyRCTCompleted %>';
        var isAllRCTCompleted = '<% = _isAllRCTCompleted %>';

    </script>

    <style>
        .seasonBox {
            height: 42px !important;
        }

        .snapChartBox {
            height: 171px !important;
        }

        .snapChartDivPaddingCompleted {
            padding: 10px 0 0 20px !important;
        }

        .snapChartDivPadding {
            padding: 10px 0 0 35px !important;
        }

        .snapchartAnchorTag {
            padding: 10px 0 0 5px !important;
        }

        .snapChartCompleImage {
            margin-top: 15px !important;
        }

        .snapChartHide {
            display: none;
        }
        .centerButton {
            text-align: center;
            float: left;
            padding-left: 50px;
        }
    </style>
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucEventTabs" runat="server" />
            <div id="divTabReport" class="ContentWidth marginTop50">
                <div class="PageTitle" id="snapchartHeader">
                    Report Your Findings
                </div>
                <div class=" snapChartBox winter" id="snapchartContent">
                    <div class="FloatLeft seasonBoxWidth320">
                        <div class="seasonBox spring">
                            <div class="snapChartImageBox">
                                <img id="imgSpring" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("../Images/green-check.png") %>"
                                    alt="Spring Completed" />
                            </div>
                            <div id="divSpring" class="snapchartDiv snapChartDivPaddingCompleted">
                                Plan
                            </div>
                            <div>
                                <a id="lnkSpring" class="snapchartAnchorTag seasonContentHide" href="#Spring">Open Spring
                                    <span class="notranslate">SnapCharT®</span></a>
                            </div>
                        </div>
                        <div class="seasonBox summer">
                            <div class="snapChartImageBox">
                                <img id="imgSummer" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("../Images/green-check.png") %>"
                                    alt="Summer Completed" />
                            </div>
                            <div id="divSummer" class="snapchartDiv snapChartDivPaddingCompleted">
                                Investigate
                            </div>
                            <div>
                                <a id="lnkSummer" class="snapchartAnchorTag seasonContentHide" href="#Summer">Open Summer
                                     <span class="notranslate">SnapCharT®</span></a>
                            </div>
                        </div>
                        <div class="seasonBox autumn">
                            <div class="snapChartImageBox">
                                <img id="imgAutumn" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("../Images/green-check.png") %>"
                                    alt="Autumn Completed" />
                            </div>
                            <div id="divAutumn" class="snapchartDiv snapChartDivPaddingCompleted">
                                Analyze
                            </div>
                            <div>
                                <a id="lnkAutumn" class="snapchartAnchorTag seasonContentHide" href="#Autumn">Open Autumn
                                    <span class="notranslate">SnapCharT®</span></a>
                            </div>
                        </div>
                        <div class="seasonBox winter">
                            <div class="snapChartImageBox">
                                <img id="imgWinter" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("../Images/green-check.png") %>"
                                    alt="Winter Completed" />
                            </div>
                            <div id="divWinter" class="snapchartDiv snapChartDivPadding">
                                Report
                            </div>
                            <div>
                                <a id="lnkWinter" class="snapchartAnchorTag snapChartHide" href="javascript:void(0)" onclick="return OpenSnapChartPage( SnapchartUrl(true) );">Create Winter <span class="notranslate">SnapCharT®</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="seasonParagraph">
                        <p id="contentSpring" class="seasonContentHide">
                            Plan your investigation and begin gathering information. From your <span class="notranslate">SnapCharT®</span>, you
                            may consider referencing the <span class="notranslate">Root Cause Tree®</span> Dictionary or <span class="notranslate">Equifactor®</span> for a list
                            of questions to ask.
                        </p>
                        <p id="contentSummer" class="seasonContentHide">
                            Investigate and fully determine your sequence of events. From your <span class="notranslate">SnapCharT®</span>, you should consider using CHAP, Change
                            Analysis and <span class="notranslate">Equifactor®</span>.
                        </p>
                        <p id="contentAutumn" class="seasonContentHide">
                            Define your causal factors and analyze each one using the <span class="notranslate">Root Cause Tree®</span> and <span class="notranslate">Root Cause Tree®</span> Dictionary.From your SnapCharT®, you should consider using Safeguards Analysis
                            and <span class="notranslate">Equifactor®</span>.
                        </p>
                        <p id="contentWinter">


                            <b>Step(s)</b><br>

                            <label id="lblAnalyzeStep5">7 - Present / Report your results using Winter <span class="notranslate">SnapCharT®</span> and Report Builder.</label>

                        </p>
                    </div>
                </div>
                <br />
                <br />
                <div style="width: 100%;">
                    <div class="pageTemplateIcon" style="clear: none;">
                        <div id="lnkCustomReport" class="anchorStyle" style="padding-left: 5px;">Report Builder</div>

                    </div>
                      <div class="pageTemplateIcon">
                        <div id="lnkReportUserTemplate" class="anchorStyle" style="padding-left: 5px;">Template Builder</div>

                    </div>
                    <div class="pageUnderlineTitle" style="margin-left: 10px;">
                        <div id="lnkReportTemplate" class="anchorStyle">Use a template</div>

                    </div>
                </div>

                <div id="divCausalFactorContent" class="causalContentBorder">
                    <div>
                        <div class="blueHeader ">
                            Reports
                        </div>
                        <div class="ContentWidth">
                            <div class="Underline PaddingLeft10 notranslate" style="clear: both;" id="myReportContent">
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div id="divReportUserTemplateDialog" class="dialogDiv hidden" title="Use a template">

                <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>

                <script id="userReportTemplate" type="text/x-jquery-tmpl">
                    {{if ReportType=='2'}}
                    <div id="divUserReportTemplate">
                        <input type="checkbox" id="${ReportId}" />&nbsp;<span>${ReportName}   {{if isAdmin==true}}<span><img id='${ReportId}' class='editImage1' onclick='OpenReportTemplatePage(${ReportId},${ReportType}); return false;' src='../../../Images/edit.gif' title='Edit Report' /><img id='${ReportId}' class='editImage1' onclick='DeleteEventReport(${ReportId},"user","${ReportName}"); return false;' src='../../../Images/delete.png' title='Delete Report' /></span>{{/if}}</span>

                    </div>
                
                    {{/if}}                   
                  
                </script>


                <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>


                <script id="systemReportTemplate" type="text/x-jquery-tmpl">
                    {{if ReportType=='3'}}
                    <div id="divSystemReportTemplate">
                        <input type="checkbox" id="${ReportId}" />&nbsp;<span>${ReportName}</span>

                    </div>

                
                    {{/if}}
                </script>

                <ul style="margin-left: 20px;">
                    <strong>My Templates </strong>
                    <li id="showNoUserTemplate" class="hidden">No User Templates</li>
                    <li id="userReportTemplateResults" class="notranslate"></li>
                </ul>
                <ul style="margin: 10px 0 0 20px;">
                    <strong>System Templates </strong>
                    <li id="showNoSystemTemplate" class="hidden">No System Templates</li>
                    <li id="systemReportTemplateResults" class="notranslate"></li>
                </ul>
                <div class="centerButton">
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Cancel" id="btnUserTemplateCancel" />
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Add Reports" id="btnUserTemplateSelect" />
                </div>
            </div>

            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
