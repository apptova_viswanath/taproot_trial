﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="tap.usr.ForgotPassword" %>

<!DOCTYPE html >
<html>
<head>
     <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>TapRooT Forgot Password</title>

    <script type="text/javascript">
        //debugger;
        var _baseURL = '<%= ResolveUrl("~/") %>';

       
    </script>
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <!--Page Scripts -->

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.js") %>" type="text/javascript"></script>
    <%--<script src="../Scripts/tap.js.usr/js.usr.chngpwd.js" type="text/javascript"></script>--%>
<%--   <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.js") %>" type="text/javascript"></script>--%>

   <!--Passwod policy passing parameters to class -->
   
<%--    <script id="ForgotPasswordJS" src="<%=ResolveUrl("~/Scripts/tap.js.policy/ValidPasswordPolicy.js") %>">
        {
            "txtPassword":"txtResetPassword", 
            "txtConfirmPassword":"txtConfirmPassword"

        }
</script>--%>     

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/master.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Login.css") %>" rel="stylesheet" type="text/css" />

        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.validate/PasswordPolicy.css") %>" rel="stylesheet" type="text/css" />
     <style type="text/css">
         .auto-style3 {
             width: 195px;
         }

     </style>
</head>
<%--<body onload="ResetPasword()">--%>
  <body>
    <form id="form1" runat="server">
    <div class="wrapper">

        <div id="TapRooTHeader" runat="server">
            <div>
                <!-- top menu -->
                <div class="TapRooTHeader">
                  
                        <a href="<%=ResolveUrl("~/home") %>" class="TapRooTLogo" title="Home" id="TapRootLogoLink"></a>
                   
                </div>
            </div>
            <div class="Blue-Bar">
            </div>

        </div>
        <!-- site body-->
        <div class="BodyContent heightAutoImp">
           
            
            <div id="forgotControl">

                <div id="divHeader">
                    <label id="fgtPasswordRecovery">
                        Password Recovery Process
                    </label>
                </div>

                <div id="divMessage" class="rstMsg" runat="server">
                    It looks like you clicked on an invalid password reset link. Please try again.
                </div>

                <div id="divForgotPsw" runat="server">
                    <div class="leftDiv"></div>
                    <div>
                        <label id="lblFgtUserName">
                            Email
                        </label>
                        <input type="text" name="txtFgtUserName" style="width: 265px!important;" id="txtFgtUserName" autofocus="" placeholder="Email" onkeydown="if (event.keyCode == 13) document.getElementById('forgotPswBtn').click()" />
                    </div>
                    <div class="rightDiv"></div>
                </div>

                <div id="divFgtSubmitBtn" runat="server">
                    <div class="leftDiv"></div>
                    <div><a href="#" class="btn btn-primary btn-small" id="forgotPswBtn" >Submit</a></div>
                    <div class="rightDiv"></div>
                </div>
         
                <div id="divForgotPasswordImage" runat="server"><img src="<%=ResolveUrl("~/images/progress2.gif")%>" id="forgotPswSendingImage"/ ></div>

                 <div id="divSuccessMessage" class="fgtPswSuccess" runat ="server">
                    We’ve sent you an email containing a link that will allow you to reset your password.<br/><br/>
                      <a href="<%=ResolveUrl("~/tap.usr/login.aspx")%>" class="" style="" id="A1"><< Login Page</a>
                </div>

                <div id="divResetPassword" runat="server" class="rstPsw">
                              <div class="divLabel ClearAll">
                        <label id="Label11" class="ContentUserLabel">Password<span id="spanPassword" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtResetPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" runat="server" />
                        <%--<a href="" id="userPasswordPolicy" style="display:none;">Password Policy</a>--%>
                          <div id="pswd_info" style="margin-left:2px">
        <h4>Password must meet the following requirements:</h4><br />
        <ul>
            
            <li id="lowerCase" class="invalid">At least <strong> <span id="spnCountLowerCase">1</span> lowercase letter<span id="lowerS" >s</span></strong></li>
            <li id="upperCase" class="invalid">At least <strong><span id="spnCountUpperrCase">1</span> uppercase letter<span id="upperS" >s</span></strong></li>
            <li id="number" class="invalid">At least <strong><span id="spnCountNumer">1</span> number<span id="numberS" >s</span></strong></li>
            <li id="length" class="invalid">Be at least <strong><span id="spnLength">6</span> character<span id="minS" >s</span></strong></li>
            <li id="maxCharacter" class="invalid">No longer than <strong><span id="spnMaxCharacterCount">6</span> character<span id="maxS" >s</span></strong></li>
            <li id="specialCharacter" class="invalid">At least <strong><span id="spnSpecialCharacterCount">6</span> special character<span id="specialS" >s</span></strong></li>
        </ul>
    </div>
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label12" class="ContentUserLabel">Confirm Password<span id="span6" style="color: Red">*</span></label></div>
                    <div class="divLabel">
                        <input id="txtConfirmPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" runat="server" />
                    </div>
                   <div></div>
                                    <div class="divButton">
                                       <a class="btn btn-primary btn-small" id="btnChangePassword" style="margin-top: 10px;" href="">Change Password</a>

            </div>
                    <br />
                </div>


            </div>

            <div id="TapRooTFooter" runat="server">
                <div class="TapRooTFooter">
                    <div class="copyright">
                        <p>
                            © Copyright 2015  System Improvements Inc. All Rights Reserved.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
         <input type="hidden" id="hdnUserName" value="" runat="server"/>
    </form>
</body>
</html>
