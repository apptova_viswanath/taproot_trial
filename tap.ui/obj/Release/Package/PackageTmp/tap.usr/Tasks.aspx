﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/tap.mst/Master.master" CodeBehind="Tasks.aspx.cs" Inherits="tap.ui.usr.Tasks" %>
<%@ Register Src="~/tap.usr.ctrl/ucTask.ascx" TagName="ucTask" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucPerson.ascx" TagPrefix="uc" TagName="ucPerson" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <!---Generalized Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--JQGrid Script -->
    <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Date Picker Script for Advanced Search -->
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>


    <!--Page Script -->
    <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.hme.js") %>" type="text/javascript"></script>--%>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.tasks.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.tasks.popup.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>

        <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>--%>
    <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.js") %>" type="text/javascript"> </script>--%>
           <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.person.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #pageNavigation_center {
            display: none;
        }

        .ui-dialog-title {
            font: 10pt "Lucida Sans Unicode", "Trebuchet Ms",Helvetica,Arial;
            margin: 0px;
        }

        td.myPager a {
            text-decoration: underline !important;
        }

        gbox_TasksGrid .ui-widget {
            font-size: 1.1em;
        }

        #ui-datepicker-div {
            font-size: 12px;
        }
    </style>


    <div class="tasksContent" id="TasksDiv">
        <!--Search links -->
        <div>
            <div  id="homeErrror">
                <label id="EventErrorMsg">
                </label>
            </div>
            <div class="NavigationBreadCrumb">
            </div>
<a id="IdShowAllTasks" href="#" style="margin-left: 10px;">Show All Tasks</a>
            <a id="IdShowReadyOnly" href="#" hidden style="margin-left: 10px;">Show only Ready tasks</a>

        </div>
        <%--<input type="button" id="testID" />--%>
        <!--Grid -->
        <div id="Grid" style="margin: 20px 5px 0px 5px;">
            <div class="EventGrid" style="margin: 0px;">
                <table id="TasksGrid" class="scroll">
                     <tr>
                        <td />
                    </tr>
                </table>
                <div id="pageNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>
        </div>

        <div class="ui-jqgrid-view EventGrid hidden" id="divNoRowMessage">
            <div class="ui-jqgrid-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
                <span class="ui-jqgrid-title">Tasks</span>
            </div>
            <div id="divMessage" class="ui-widget-content jqgrow ui-row-ltr">
                No Pending Tasks.
            </div>

        </div>

        <div class="ClearAll MarginTop30">
            <label id="ErrShowGrid">
            </label>
        </div>
        <div class="ClearAll MarginTop30 PaddingLeft50">
        </div>
    </div>
    <div class="backgroundPopup">
        <uc:ucTask ID="UcTask" runat="server"></uc:ucTask>
        <uc:ucPerson runat="server" ID="ucPerson" />
    </div>

   


</asp:Content>