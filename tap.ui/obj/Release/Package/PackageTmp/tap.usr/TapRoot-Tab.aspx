﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TapRoot-Tab.aspx.cs" Inherits="tap.ui.usr.TapRoot_Tab"
    MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucRootCauseTree.ascx" TagName="ucRCT" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="taprootTabContent" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />


    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet"
        type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.tab.taproot.js") %>" type="text/javascript"> </script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript">
        GetIdsFromUrl();
        _virtualdirectorypath = GetVirtualDirectory();

        //var newWindow = '';
        //function popup(url) {
        //    newWindow = window.open(url, 'SnapCharT', 'height=660,width=1300,resizable=yes');
        //    if (window.focus) { newWindow.focus() }
        //    return false;
        //}

        //function SnapchartUrl() {

        //    GetInvestigationSession();
        //    var url = _baseURL + 'Event/' + _module + '/SnapCharT/' + _eventId;
        //    return url;
        //}

    </script>

    <style>
        .ui-widget {
            font-size: 12px;
        }

        #divAnalyzeStep label {
            margin-bottom: 10px;
        }

        .refTaskText {
            clear: both;
        }

        .snapChartBox label {
            /*margin-top:5px;*/
        }

        .snapChartBox ul {
            list-style: square !important;
            margin-left: 13px;
        }

        .snapChartBox li {
            list-style: square !important;
            /*margin-bottom: 5px;
    margin-top: 5px;*/
        }

        .optional {
            clear: both;
            margin-top: 20px;
        }
    </style>
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucEventTabs" runat="server" />
            <div id="divTabTapRooT" class="ContentWidth marginTop50">
                <div class="PageTitle" id="snapchartHeader">
                    Plan Your Investigation
                </div>
               
                    <div class="snapChartBox spring" id="snapchartContent">
                        <div class="FloatLeft seasonBoxWidth320">
                            <div class="seasonBox spring">
                                <div class="snapChartImageBox">
                                    <img id="imgSpring" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>"
                                        alt="Spring Completed" />
                                </div>
                                <div id="divSpring" class="snapchartDiv snapChartDivPadding">
                                    Plan
                                </div>
                                <div>
                                    <a id="lnkSpring" class="snapchartAnchorTag seasonContentHide" href="javascript:void(0)"
                                        onclick="return OpenSnapChartPage( SnapchartUrl(false) );">Open Spring SnapCharT®</a>

                                </div>
                            </div>
                            <div class="seasonBox summer">
                                <div class="snapChartImageBox">
                                    <img id="imgSummer" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>"
                                        alt="Summer Completed" />
                                </div>
                                <div id="divSummer" class="snapchartDiv snapChartDivPadding">
                                    Investigate
                                </div>
                                <div>
                                    <a id="lnkSummer" class="snapchartAnchorTag seasonContentHide" href="javascript:void(0)"
                                        onclick="return OpenSnapChartPage( SnapchartUrl(false) );">Open Summer SnapCharT®</a>

                                </div>
                            </div>
                            <div class="seasonBox autumn">
                                <div class="snapChartImageBox">
                                    <img id="imgAutumn" class="snapChartCompleImage seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>"
                                        alt="Autumn Completed" />
                                </div>
                                <div id="divAutumn" class="snapchartDiv snapChartDivPadding">
                                    Analyze
                                </div>
                                <div>
                                    <a id="lnkAutumn" class="snapchartAnchorTag seasonContentHide" href="javascript:void(0)"
                                        onclick="return OpenSnapChartPage( SnapchartUrl(false) );">Open Autumn SnapCharT®</a>

                                </div>
                            </div>
                        </div>
                        <div class="seasonParagraph">

                            <div id="contentSpring" class="">

                                <b>Step(s)</b><br />
                                <label id="lblPlanStep" class="refTaskText"></label>
                                <div class="optional">
                                    <b>Optional</b>
                                    <ul id="lblPlanOptional" class="refOptionalText">
                                    </ul>
                                </div>

                            </div>

                            <div id="contentSummer" class="">

                                <b>Step(s)</b><br>
                                <label id="lblInvestigateStep" class="refTaskText">
                                </label>
                                <div class="optional">
                                    <b>Optional:</b>
                                    <ul id="lblInvestigateOptional">
                                        <li>Use <span class="notranslate">Equifactor®</span>, <span class="notranslate">CHAP®</span>, or Change <span class="notranslate">Analysis®</span> to add events and conditions to the <span class="notranslate">SnapCharT®</span>.</li>
                                    </ul>
                                </div>
                            </div>
                            <div id="contentAutumn">

                                <div id="divAnalyzeStep" class="refTaskText">
                                    <b>Step(s)</b><br>
                                    <label id="lblAnalyzeStep3"></label>
                                    <br />
                                    <label id="lblAnalyzeStep4"></label>
                                    <br />
                                    <label id="lblAnalyzeStep5">5 - Analyze each root cause for generic causes.</label>

                                    <div class="optional">
                                        <b>Optional</b>
                                        <ul>
                                            <li id="lblAnalyzeOptional3"></li>
                                            <%-- <li  id="lblAnalyzeOptional4"></li>--%>
                                            <li id="lblAnalyzeOptional5">Reference Corrective Action <span class="notranslate">Helper®</span> to guide generic cause determination.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

           
                <div id="divRootCauseResult" class="rootCauseTreeResultBox seasonContentHide">
                    <div class="greenSelectImage PaddingRight10">
                       <span class="notranslate">Root Cause Tree®</span> Complete
                    </div>
                    <div class="warningYellowImage marginLeft50">
                        <span class="notranslate">Root Cause Tree®</span> Incomplete
                    </div>
                </div>
                <div id="divCausalFactorContent" class="causalContentBorder  seasonContentHide">
                    <div>
                        <div class="blueHeader notranslate">
                            Causal Factors Identified
                        </div>

                        <uc:ucRCT ID="ucRCT" runat="server" />
                    </div>
                </div>
            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>

    </div>
</asp:Content>
