﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/tap.mst/Master.master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="tap.ui.usr.Dashboard" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.bootstrap/bootstrap.js") %>" type="text/javascript"></script>

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 

     <!-- jQuery UI Datepicker 1.8.16 --->
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>
    
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.adm.dashboard.js") %>" type="text/javascript"></script>

    <style type="text/css">
        .divBasicCause {
            width: 260px;
            min-width: 120px;
        }

        .divEventBreakdown {
            width: 230px;
            min-width: 100px;
        }

        .divCommonRCT {
            width: 680px;
            min-width: 460px;
        }        
        
        .customRangeLabel{width:80px;text-align:left;}
        
    </style>

    <%-- ------------------- Body markup here -------------------------- --%>

    <div class="siteContent">
        <label id="lblDashboard" class="PageLabel ">Dashboard</label>
        <image id="BCCDFilter" style="margin:8px 0 0 2px;" src="<%=ResolveUrl("~/Images/Filter.png") %>" onclick="ShowPopup(this.id)"></image>

        <div id="upperpannel" class="ClearAll">
            <div id="BasicCauseCDBox" class="FloatLeft  MarginLeft10" style="border: 0px solid green;">
                <label id="lblBCCD" class="ClearAll boldText">Basic Cause Category Distribution</label> 

                <!-- Modal HTML -->
                <div id="filterModal" class="modal fade" style="width:400px;">
                    <div class="modal-dialog" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Filter</h4>
                            </div>
                            <div class="modal-body " style="height:130px; ">
                                <div class="MarginLeft5">
                                    <div class="FloatLeft marginBottom10 ">
                                    <label class="boldText">Date Range</label>
                                    <div class=" MarginTop10">
                                        <a id="thisMonth" class="anchorStyle  ContentAnchorTag" style="padding: 0 0 0 0;" onclick="DashboardByPeriod(this.id)">This Month</a>
                                        <br />
                                        <a id="thisQuarter" class="anchorStyle ContentAnchorTag" style="padding: 0 0 0 0;" onclick="DashboardByPeriod(this.id)">This Quarter</a>
                                        <br />
                                        <a id="yearToDate" class="anchorStyle ContentAnchorTag" style="padding: 0 0 0 0;" onclick="DashboardByPeriod(this.id)">Year To Date</a>
                                    </div>
                                    </div>
                                    <div class=" FloatRight marginBottom10 ">
                                    <label class=" boldText">Custom Range</label>                                         
                                    <div class=" MarginTop10">
                                        <div class="customRangeLabel">
                                          <label>From</label>
                                        </div>
                                          <input id="BCCDFromDate" readonly="readonly" type="text"  />                                            
                                        <br />                                        
                                         <div class="customRangeLabel">
                                          <label>To</label>
                                        </div>
                                           <input id="BCCDToDate" readonly="readonly" type="text" />                                            
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <%-- ############################################################################################################ --%>

                <div id="Top6BasicCausedistribution" class="divBasicCause MarginTop10">
                    <ol id="listTop6BasicCausedistribution" class="PaddingLeft20"></ol>
                </div>
            </div>
            <div id="EventBreakdownBox" class="FloatLeft divEventBreakdown " style="border: 0px solid green;">
                <label id="lblEB" class="ClearAll boldText">Event Breakdown</label>
                <p class="MarginTop10">Total Events (<label id="lblTotalevents"></label>)</p>
                <p>Incidents (<label id="lblIncidents"></label>)</p>
                <p>Investigations (<label id="lblInvestigations"></label>)</p>
                <p>Audits (<label id="lblAudits"></label>)</p>
                <p>Incident Investigation Ratio (<label id="lblIncInvRatio"></label>)</p>
            </div>
            <div id="MostCommonRCbox" class="FloatLeft " style="border: 0px solid green;">
                <label id="lblMCRC" class="ClearAll boldText">Most Common Root Causes</label>

                <div id="Top10RCT" class="divCommonRCT MarginTop10">
                    <ol id="ListTop10RCT" class="PaddingLeft25"></ol>
                </div>
            </div>
        </div>

    </div>
</asp:Content>





