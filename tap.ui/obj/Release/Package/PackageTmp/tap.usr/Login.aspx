﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="tap.ui.usr.Login" %>

<!DOCTYPE html >
<html>
<head>
       <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
     <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>TapRooT® Login</title>

    <script type="text/javascript">

        //debugger;
        var _baseURL = '<%= ResolveUrl("~/") %>';

        function DisplayMessage() {
            //alert('Your Session has timed out. Please log in to continue.');
            alert('Your session has ended due to inactivity. Please log in to continue.');
        }
        function LinkExpired()
        {
            alert('Forgot Password Link expired please try again');
        }

        
        
        //function ForwardToLogin()
        //{
        //   // alert(window.parent.name);
        //    var frame = window.parent.name;
            

        //   // alert(frame);
        //    if (frame == 'RootCauseTree')
        //    {
                
        //        window.parent.close();
        //    }
        //}
      
    </script>
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/json2.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <!--Page Scripts -->

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/master.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Login.css") %>" rel="stylesheet" type="text/css" />
</head>
<%--<body onload="javascript:ForwardToLogin();">--%>
   <body>
    <div class="wrapper">

        <div id="TapRooTHeader" runat="server">
            <div>
                <!-- top menu -->
                <div class="TapRooTHeader">
                    <div>
                        <a href="<%=ResolveUrl("~/home")%>" class="TapRooTLogo" title="Home" id="TapRootLogoLink"></a>

                        <!-- navigation tabs starts -->
                    </div>
                </div>


            </div>
            <div class="Blue-Bar">
            </div>

        </div>
        <!-- site body-->
        <div class="BodyContent">
            <div id="LoginHeader" style="display:none">
                TapRooT® SOFTWARE
                
               
                    <label id="ErrorMessage" class="userInactiveLoginErrorMsg">
                        This account has been disabled. If you believe this is an error, please contact your system administrator.
                    </label>
             
            </div>
                        
            <br />
            <br />
            <br />
            <div id="LoginControl">
                
                <div>
                    <label id="Msg">
                    </label>
                </div>
                <div>
                    <label id="lblUserName">
                        User Name</label>
                </div>
                <div>
                    <input type="text" name="txtUserName" id="txtUserName" autofocus="true" placeholder="enter username"
                        runat="server" onkeyup="checkValues()" onkeydown="if (event.keyCode == 13) document.getElementById('btnLogin').click()"/>
                </div>
                <div>
                    <label id="lblPassword">
                        Password</label>
                </div>
                <div>
                    <input type="password" placeholder="enter password" id="txtPassword"
                        runat="server" onkeyup="checkValues()" onkeydown="if (event.keyCode == 13) document.getElementById('btnLogin').click()"/>
                </div>
                <div id="divButton">

                    <%--  <button id="btnLogin">
                                Sign In</button>--%>
                    <a href="" class="btn btn-primary btn-small" style="float: left;" id="btnLogin">Sign In</a>
                    <form id="frmLogin" runat="server">
                        <asp:HiddenField ClientIDMode="Static" ID="hdnVirtualDirectoryPath" runat="server" />
                        <asp:HiddenField ClientIDMode="Static" ID="hdnServicePath" runat="server" />
                        <asp:Button ID="btnAuthentication" runat="server" Text="Login" Style="visibility: hidden; display: none;"
                            OnClick="btnAuthentication_Click" />
                        <asp:HiddenField ID="hdnUserName" runat="server" />
                        <asp:HiddenField ID="hdnUserID" runat="server" />
                        <asp:HiddenField ID="hdnCompanyID" runat="server" />
                        <asp:HiddenField ID="hdnCompanyName" runat="server" />
                        <asp:HiddenField ID="hdnFirstName" runat="server" />
                        <asp:HiddenField ID="hdnDateFormat" runat="server" />
                       
                    </form>
                    <%-- <button class="rightButton" id="btnCancel">
                         Clear</button>--%>
                   <%-- <a href="#" class="btn btn-primary rightButton btn-small" id="btnCancel">Clear</a>--%>
                     <div class="forgotpwd"><a href="<%=ResolveUrl("~/tap.usr/ForgotPassword.aspx")%>">Forgot User Name or Password?</a>    </div>
                </div>
                <br />
                <br />
               
                <br />
            </div>

            <div id="TapRooTFooter" runat="server">
                <div class="TapRooTFooter">
                    <div class="copyright">
                        <p>
                            © Copyright 2015  System Improvements Inc. All Rights Reserved.
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
</html>
