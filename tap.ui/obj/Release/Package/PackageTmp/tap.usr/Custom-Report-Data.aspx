﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Report-Data.aspx.cs" EnableEventValidation="false" ValidateRequest="false" Inherits="tap.ui.usr.Custom_Report_Data" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>" />
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Report Data</title>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <!-- Notification plugin (modified plugin files) ------------->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/js.noty/jquery.notify.js") %>" type="text/javascript"></script>
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.data.rpt.js") %>" type="text/javascript"> </script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CustomReport.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

        function ShowProcess() {

            $('#lblProcessing').show();
        }
    </script>
    <style>
        /*.divCAP, .divCFSubTasksValues>div {
            border:1px solid black;            
        }*/
    </style>
</head>
<body>

    <form id="form1" runat="server" style="border: 1px solid transparent;">

        <div style="overflow-y: auto; overflow-x: hidden;">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <div class="divProcess">
                <label id="lblProcessing" class="hide" runat="server">Processing...</label>
            </div>
            <div style="float: right; margin: 10px;">
                <asp:Button runat="server" ID="btnPrintPdf" class="btn btn-primary btn-small btn-float" Text="Generate PDF"
                    OnClick="btnPrintPdf_Click" />

            </div>

            <div class="divGenerateReport">
                <div class="warningMessage">
                    <label id="lblReportTemplateMsg"></label>
                </div>
                <div id="divCompanyInfo" runat="server">
                    <div id="divDataImage" class="hide">
                        <asp:Image ID="imgDisplayLogo" runat="server" src="" />
                    </div>
                    <div id="divReportTitle" class="notranslate" runat="server">
                        <span
                            runat="server" id="spnReportTitle"></span>
                    </div>
                    <div id="divDataPageTitle">
                        <div id="divCompanyName" runat="server">
                            <span runat="server" id="spnCompanyName"></span>
                        </div>
                        <div id="divCreatedDate" runat="server" style="clear: both;">
                            <label>Date : </label>
                            <span runat="server" id="spnCreatedDate"></span>
                        </div>
                    </div>

                </div>
                <br />
                <div id="generateReport" class="displayReport notranslate" runat="server">
                </div>
                <input id="hdnCFValues" type="hidden" name="hdnCFValues" value="" runat="server" />
                <input id="hdnIntegrateType" type="hidden" name="hdnIntegrateType" value="" runat="server" />

                <input id="hdnIntegrateTypeCF" type="hidden" name="hdnIntegrateTypeCF" value="" runat="server" />
                <input id="hdnIntegrateTypeRCT" type="hidden" name="hdnIntegrateTypeRCT" value="" runat="server" />
                <input id="hdnIntegrateTypeCAP" type="hidden" name="hdnIntegrateTypeCAP" value="" runat="server" />
                <input id="hdnGetLogoUrlPath" type="hidden" name="hdnCompanyName" value="" runat="server" />
                <input id="hdnGetHtmlData" type="hidden" name="hdnGetHtmlData" value="" runat="server" />
                <input id="hdnFileNames" type="hidden" name="hdnFileNames" value="" runat="server" />
            </div>



            <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>

            <%--<script id="tapRooTCFTemplate" type="text/x-jquery-tmpl">--%>
                <div class="divRooTCFTemplate">
                    <%--<div class="divHeaderValue" id="divCFHeader">
                        <span><b>Causal Factor : ${CausalFactorName}</b></span>
                    </div>--%>
                    <%--{{if jsonData.length > 0}}
                        <table style="width:100%; border: 1px solid black;" >
                            <tr>
                                <td colspan="3">
                                    <div class="divCFSubHeader">
                                        <div class="divCFSubRootCause"><b>Root Cause</b></div>
                                        <div class="divCFSubActionPlanHeader"><span><b>Corrective Action</b></span></div>
                                        <div class="divCFSubTasksHeader"><span><b>Tasks</b></span></div>
                                    </div>
                                </td>
                            </tr>                            
                            {{each jsonData}}
                            <tr style="width:100%; border: 1px solid black;">
                                <td style="border: 1px solid black; width:33%;">${Title}</td>
                                <td style="border: 1px solid black; width:33%;">${Identifier}</td>
                                <td style="border: 1px solid black; width:33%;">${Tasks}</td>
                                <td colspan="2" style="width:100%;">
                                    {{each ActionPlans}}
                                        <table style="width:100%;" ><tr>
                                            <td style="width:50%; border: 1px solid black;">${ActionPlans}</td>
                                            <td style="width:50%; border: 1px solid black;">
                                            {{each Tasks}}
                                                    <div style="border: 1px thin black;">${Tasks}</div>
                                            {{/each}}
                                            </td>
                                        </tr></table>    
                                    {{/each}}
                                </td>
                            </tr>
                            {{/each}}
                        </table>
                    {{else}}
                    <div style="clear: both; overflow: hidden; padding: 2px;">No Root Causes.</div>
                    {{/if}}--%>
                </div>
           <%-- </script>--%>
            <ul id="tapRooTCFTemplateResults" class="merginLeft5 notranslate"></ul>
            <label id="lblCFNone" style="display: none; padding-left: 5px;">No Causal Factor.</label>

            <%--<script id="tapRooTRCTTemplate" type="text/x-jquery-tmpl">--%>
                <div class="divRooTRCTTemplate">
                    <%--<div class="divHeaderValue" style="display: none" id="divCFHeader">
                        <span><b>${CausalFactorName}</b></span>
                    </div>
                    <%--{{if RootCauses.length > 0}}
                    <table style="width:100%; border: 1px solid black;" >
                        <tr><td colspan="3">
                                <div class="divCFSubHeader">
                                    <div class="divCFSubRootCause"><b>Root Cause</b></div>
                                    <div class="divCFSubActionPlanHeader"><span><b>Corrective Action</b></span></div>
                                    <div class="divCFSubTasksHeader"><span><b>Tasks</b></span></div>
                                </div>
                            </td>                    
                        {{each RootCauses}} 
                        <tr style="width:100%; border: 1px solid black;">
                            <td style="border: 1px solid black; width:33%;">${RootCauses}</td>
                            <td colspan="2" style="width:100%;">
                                {{each ActionPlans}}
                                    <table style="width:100%;" ><tr>
                                        <td style="width:50%; border: 1px solid black;">${ActionPlans}</td>
                                        <td style="width:50%; border: 1px solid black;">
                                        {{each Tasks}}
                                                <div style="border: 1px thin black;">${Tasks}</div>
                                        {{/each}}
                                        </td>
                                    </tr></table>    
                                {{/each}}
                            </td>
                        </tr>
                        {{/each}}
                    </table>
                    {{else}}
                    <div style="clear: both; overflow: hidden; padding: 2px;">No Root Causes.</div>
                    {{/if}}--%>
                </div>
            <%--</script>--%>
            <ul id="tapRooTRCTTemplateResults" class="merginLeft5 notranslate"></ul>
            <label id="lblRCTNone" style="display: none; padding-left: 5px;">No Root Cause.</label>


            <%--<script id="tapRooTCAPTemplate" type="text/x-jquery-tmpl">
                <div class="divMainTamplate" style="margin-top:0px;">                  
                    <div class="divHeaderValue">
                        <span><b>Action Plan: ${ActionPlans}</b></span>
                    </div>

                    {{if Tasks.length > 0}}
                    <div class="divCAPTaskSubHeader"><b>Tasks</b></div>
                    <div class="divCAPSubTaskValues">
                        {{each Tasks}}                       
                                <div>${Tasks}</div>

                        {{/each}} 
                    </div>
                    {{else}}
                    <div class="divCAPSubTaskValues" style="padding: 2px;">No Tasks</div>
                    {{/if}}
                    {{if RootCauses.length > 0}}
                    <div class="divCAPSubHeader" >
                        <table border="1" style="width:100%">
                            <tr>
                                <td style="width:50%"><span><b>Root Causes Fixed</b></span></td>
                                <td style="width:50%"><span><b>Causal Factor</b></span></td>
                            </tr>
                            {{each RootCauses}}
                            <tr>
                                <td>${RootCauses}</td>
                                <td>
                                    {{each CausalFactorName}} 
                                    <div>&nbsp;&nbsp;${CausalFactorName}</div>
                                    {{/each}}
                                </td>
                            </tr>
                            {{/each}}
                        </table>
                    </div>
                    {{else}}
                    <div style="clear: both; overflow: hidden; padding: 2px;">No Root Causes and Causal Factor</div>
                    {{/if}}
                   
                </div>
            </script>--%>
            <div class="divRooTCAPTemplate"></div>
            <ul id="tapRooTCAPTemplateResults" class="merginLeft5 notranslate"></ul>

            <label id="lblCAPNone" style="display: none; padding-left: 5px;">No Action Plans.</label>

            <div id="divShowIntegrateCF" style="clear: both; padding: 5px;"></div>
            <div id="divShowIntegrateRCT" style="clear: both; padding: 5px;"></div>
            <div id="divShowIntegrateCAP" style="clear: both; padding: 5px;"></div>
        </div>

    </form>
</body>
</html>
