﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" 
    Inherits="tap.ui.usr.ChangePassword" %>

<!DOCTYPE>
<html lang="en-us">
<head runat="server">
     <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Change Password</title>
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />

    <%--<link href="../Styles/tap.stl.cus/Login.css" rel="stylesheet" type="text/css" />--%>
    
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/master.css") %>" rel="stylesheet"
        type="text/css" />
        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Login.css") %>" rel="stylesheet" type="text/css" />
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.validate/PasswordPolicy.css") %>" rel="stylesheet" type="text/css" />
     <script src=<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %> type="text/javascript"></script>   
     <script src=<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.chngpwd.js") %> type="text/javascript"></script>
        <script id="ForgotPasswordJS" type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.policy/ValidPasswordPolicy.js") %>">
        {
            "txtPassword":"txtChnPwdNewPwd", 
            "txtConfirmPassword":"txtChnPwdConPwd"

        }
</script> 
    <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';
       
    </script>
</head>
<body>
  <form id="form1" runat="server">
      <div class="wrapper">
       <div id="TapRooTHeader" runat="server">
            <div>
                <!-- top menu -->
                <div class="TapRooTHeader">
                  
                        <a <%--href="<%=ResolveUrl("~/home") %>"--%> class="TapRooTLogo" title="Home" id="TapRootLogoLink"></a>
                   
                </div>
            </div>
            <div class="Blue-Bar">
            </div>

        </div>
    <div class="BodyContent heightAutoImp">
          

        <div id="forgotControl">
          
                    <div id="divHeader">

                    <label id="fgtPasswordRecovery">
                        Change Password
                    </label>
                </div>
            <br />
                     <div  id="ShowMessage" style="text-align:center !important">
                <label id="ErrorMeeage" >
                        Oops! Your password is expired. Please change your password.
                    </label>
                         <label id="lblErrorMeeageForPolicy"  >
                        Your password does not meet your company's password policy.<br /> Please change your password.
                    </label>
            </div>
            <div id="divResetPassword" runat="server" class="rstPsw">
                                             
            <div class="divLabel ClearAll">
                <label id="Label6" class="ContentUserLabel">Password<span id="spanPassword" style="color: Red">*</span></label>
            </div>
            <div class="divLabel">
                <input id="txtChnPwdNewPwd" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="password" runat="server"
                data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                data-entity-property="Password" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                data-entity-sec-queryfield="UserID" />
               <%-- <a href="" id="userPasswordPolicy" style="font-size:12px; display:none;">Password Policy</a>--%>
                     <div id="pswd_info" style="margin-left:2px; z-index: 1000; display: none">
        <h4>Password must meet the following requirements:</h4><br />
        <ul>
            
            <li id="lowerCase" class="invalid">At least <strong> <span id="spnCountLowerCase">1</span> lowercase letter<span id="lowerS" >s</span></strong></li>
            <li id="upperCase" class="invalid">At least <strong><span id="spnCountUpperrCase">1</span> uppercase letter<span id="upperS" >s</span></strong></li>
            <li id="number" class="invalid">At least <strong><span id="spnCountNumer">1</span> number<span id="numberS" >s</span></strong></li>
            <li id="length" class="invalid">Be at least <strong><span id="spnLength">6</span> character<span id="minS" >s</span></strong></li>
            <li id="maxCharacter" class="invalid">No longer than <strong><span id="spnMaxCharacterCount">6</span> character<span id="maxS" >s</span></strong></li>
            <li id="specialCharacter" class="invalid">At least <strong><span id="spnSpecialCharacterCount">6</span> special character<span id="specialS" >s</span></strong></li>
        </ul>
                         </div>
            </div>
            <div class="divLabel ClearAll">
                <label id="Label2" class="ContentUserLabel">Confirm Password<span id="span7" style="color: Red">*</span></label>
            </div>
            <div class="divLabel">
                 <input id="txtChnPwdConPwd" class="ContentInputTextBox Width200 trackChanges marginBottom10" type="password" runat="server"
                    data-mode="Update" data-type="Static" data-entity-type="tap.dat.Users,tap.dat" data-entity-set="Users"
                    data-entity-property="Password" data-entity-property-pk="UserID" data-entity-queryfield="UserID"
                    data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                    data-entity-sec-queryfield="UserID" />
            </div>
            <div></div>
              <br/>
                
                
      <input type="hidden" id="txtChnPwdUserName" runat="server" />
<input type="hidden" id="txtCompanyIdCnange" runat="server" />
            
             <div class="divButton">
<%--                 <asp:Button ID="btnclearSessions"   runat="server" Text="Login" Style="visibility: hidden; display: none;"
                           OnClick="btnclearSessions_Click" />--%>

                   <%-- <a class="btn btn-primary btn-small" href="#" id="btnSaveUserDetails">Create</a>
                    <a class="btn btn-primary btn-small" href="#" id="btnDeleteUserDetails">Delete</a>
                    <a class="btn btn-primary btn-small" href="#" id="btnSaveUserDetailsCancel" style="display:none;">Clear</a>--%>
                    <a  class="btn btn-primary btn-small"  id="btnChnPwdSave" style="margin-top: 10px;" href="" >Change Password</a>
                   <%-- <a  class="btn btn-primary btn-small"  id="btnChnPwdClear" style="margin-left:50px;" >Clear</a>--%>
            </div>
      <br />
            </div>
        </div>


                    <div id="TapRooTFooter" runat="server">
                <div class="TapRooTFooter">
                    <div class="copyright">
                        <p>
                            © Copyright 2015  System Improvements Inc. All Rights Reserved.
                        </p>
                    </div>
                </div>
            </div>
    </div>
          </div>
      </form>
</body>
</html>
