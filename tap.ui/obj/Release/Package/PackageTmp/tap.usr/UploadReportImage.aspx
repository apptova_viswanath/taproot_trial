﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadReportImage.aspx.cs" Inherits="tap.ui.usr.UploadReportImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Upload Image</title>
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet"
        type="text/css" />
    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.rpt.image.js") %>" type="text/javascript"></script>

    <base target="_self" />

    <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';
    </script>

    <style>
        .smallImageSize {
            height: 58px;
            width: 60px;
            border: 1px solid #4779ae;
            float: left;
            text-align: center;
            padding-top: 20px;
            cursor: pointer;
            color: #355779;
            font: italic 13px Georgia, "Times New Roman", Times, serif;
        }

        .mediumImageSize {
            height: 70px;
            width: 80px;
            border: 1px solid #4779ae;
            float: left;
            margin-left: 7px;
            text-align: center;
            padding-top: 20px;
            cursor: pointer;
            color: #355779;
            font: italic 13px Georgia, "Times New Roman", Times, serif;
        }

        .largeImageSize {
            height: 90px;
            width: 100px;
            border: 1px solid #4779ae;
            float: left;
            margin-left: 7px;
            text-align: center;
            padding-top: 20px;
            cursor: pointer;
            color: #355779;
            font: italic 13px Georgia, "Times New Roman", Times, serif;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnImageSize" runat="server" />
        <div style="width: 270px;">

            <div>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="ContentLabel"></asp:Label>
            </div>

            <div style="clear: both; padding-top: 10px;">
                <input id="fileImage" class="ContentInputTextBox WidthP40" style="width: 80%" type="file" name="files[]"
                    runat="server" />
            </div>

            <div class="MarginTop10 PaddingRight70 ClearAll" style="height: 30px;">
                <asp:Button runat="server" ID="btnImageClosePopup" class="btn btn-primary btn-small btn-float"
                    Text="Cancel" OnClick="btnImageClosePopup_Click" />
                <asp:Button runat="server"  ID="btnImageAttach" class="btn btn-primary btn-small btn-float"
                    Text="Save Image" OnClick="btnImageAttach_Click" />
          
            </div>
         
        </div>
        <asp:HiddenField ClientIDMode="Static" ID="hdnReportId" runat="server" />
    </form>


</body>
</html>
