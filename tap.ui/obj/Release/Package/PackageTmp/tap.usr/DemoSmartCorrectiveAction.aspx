﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DemoSmartCorrectiveAction.aspx.cs" MasterPageFile="~/tap.mst/Master.Master" 
    Inherits="tap.ui.usr.DemoSmartCorrectiveAction" %>

<%@ Register Src="~/tap.usr.ctrl/ucRootCauseTree.ascx" TagName="ucRCT" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucTask.ascx" TagName="ucTask" TagPrefix="uc" %>
    
<asp:Content ID="content1" runat="server" ContentPlaceHolderID="TapRooTHeader">
    
   <%-- <link id="accordionCss" href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/FixTab.css") %>" rel="stylesheet" />--%>
     <script>

         var _baseURL = '<%= ResolveUrl("~/") %>';
         $(document).ready(function () {

             //$('body').css('display', 'none');
             $('body').fadeIn(3000);
         });

         var length = window.location.href.split('/').length;
         var capID = window.location.href.split('/')[length - 1];
         var isCreatePage = (capID > 0) ? false : true;
         if (isCreatePage) {
             // $("#accordionCss").attr("disabled", "disabled");


 
              //// Removing file is also easy
              //$('#dummy-file').remove();
         }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
   

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Master.css") %>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/Demo/js.democorrectiveaction.common.js") %>" type="text/javascript"> </script>  
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/Demo/js.demosmartcorrectiveaction.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
 
    
       <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';

        $(document).ready(function () {
            $('#divAccordion > section > h2').click(function () {
                if ($(this).parent('section').height() == 250) {
                    $(this).parent('section').css('height', '32');
                    $(this).parent('section').removeClass('targetSection-a');
                    $(this).parent('section').addClass('no-target-section');
                    $(this).find('a').addClass('no-target-section-h2-a');
                    return false;
                }
                else {
                    $(this).parent('section').removeClass('no-target-section');
                    $(this).find('a').removeClass('no-target-section-h2-a');
                    $(this).parent('section').css('background', '#FFF');
                    $(this).parent('section').css('height', '250');
                    $(this).parent('section').find('a').addClass('targetSection-a');
                }

                $('#divAccordion > section').each(function () {
                    if ($(this).height() > 32) {
                        $(this).css('background', '#FFF');
                        $(this).css('height', '250');
                        $(this).find('a').addClass('targetSection-a');
                    }
                });
            });
        });
    </script>
    
    <!-- Black border displaying down in smartCorrectiveAction  page -->
    <style type="text/css">
        html
        {
            background: none;
        }
        .ContentWidth 
        {
            margin: 40px 0px 3%;
            margin-top:0px;
        }
        #divTitle 
        {
            color:#333;
        }
        #divSpecific
        {
            background-color:white;
            padding: 10px;
        }        
        #divSpecific h2 a {
            color: #333333;
            display: block;
            font-size: 14px;
            font-weight: bold;
            margin-top: 10px;
            padding: 0;
            text-decoration: none;
        }

        .siteContent
        {
            padding-left:5% !important; 
        }
    </style>


     <div class="siteContent" style="padding-left:5%;">  

         <div class="PageTitle" id="divTitle">
            Create SMARTER Corrective Action
        </div>
        
        <div id="divRCT" class="ClearAll causalFactorContentBorder">
        
            <div class="blueHeader" style=" background-color:#272727;color:#eee;">
                Root or Generic Causes to Address with this Corrective Action
            </div>  

           <uc:ucRCT ID="ucRCT" runat="server" />

           <br />

        </div>

         <div id="divSection" style="overflow:hidden;"> 

        <div id="divSpecific">


        </div>
        
        <div id="divAccordion" class="accordion vertical">

            <section id="sec_specfic">
                <h2 id="specificHeader" class="seasonContentHide"><a href="javascript:void(0)">SPECIFIC</a></h2>
                <div class="ContentWidth seasonContentHide" id="specificContent" style="display: block;">
                    In detail, describe the corrective action needed to fix the root cause.Any policies/procedures/training/tools/PPE/etc. or special conditions needed 
        to implement this corrective action ? 
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Identifier</strong><span id="spnIdentifierName" style="color: Red">*</span></span>
                        <input type="text" id="txtIdentifier" autofocus="" />
                        <span id="lblIdentifier">Enter a corrective action name or number to identify this corrective action</span>
                    </div>
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Description</strong></span>
                        <textarea id="txtEventDescription" class="contentArea textArea"></textarea>
                    </div>
                </div>
            </section>
            <section id="sec_reasonable">
                <h2 id="reasonableHeader" class="seasonContentHide"><a href="javascript:void(0)">REASONABLE</a></h2>
                <div class="ContentWidth seasonContentHide" id="reasonableContent" style="display: block;">
                    <div>
                        What is the business case for this improvement? What is Return On Investment (ROI) or cost Benefit Ratio? 
            <br />
                        What are the likely consequences if this fix is not implemented? 
                    </div>
                    <div class="marginTop10">
                        <strong><span class="specificLabel">&nbsp;</span></strong>
                        <textarea id="txtBusinessCase" class="contentArea textArea"></textarea>
                    </div>
                </div>
            </section>
            <section id="sec_accountable">
                <h2 id="accountableHeader" class="seasonContentHide"><a href="javascript:void(0)">ACCOUNTABLE</a></h2>
                <div class="ContentWidth seasonContentHide" id="accountableContent" style="display: block;">
                    <div>
                        Who will be responsible to implement the fix and what authority or resources do they need? 

                    </div>
                    <div class="marginTop10">
                        <strong><span class="specificLabel">Person Responsible</span></strong>
                        <select id="ddlAccountablePerson" class="contentArea dropDownArea">
                            <option value="0"><< Select >></option>
                        </select>
                    </div>
                    <div class="marginTop10">
                        <strong><span class="specificLabel">Resources Needed</span></strong>
                        <textarea id="txtResource" class="contentArea textArea"></textarea>
                    </div>
                </div>

            </section>
            <section id="sec_timely">
                <h2 id="timelyHeader" class="seasonContentHide"><a href="javascript:void(0)">TIMELY</a></h2>
                <div class="ContentWidth seasonContentHide" id="Div1" style="display: block;">
                    What is reasonable time to fix?
        Are temporary actions needed before the fix is completed to ensure safety, quality, production or environmental responsibility? 
        (Yes/No) if so, describe these temporary actions:
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Due Date</strong></span>
                        <input type="text" id="txtTimelyDueDate" readonly="readonly" />
                    </div>
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Temporary action required (if any)</strong></span>
                        <textarea id="txtTempAction" class="contentArea textArea"></textarea>
                    </div>
                </div>

            </section>
            <section id="sec_measurable">
                <h2 id="measurableHeader" class="seasonContentHide"><a href="javascript:void(0)">MEASURABLE</a></h2>
                <div class="ContentWidth seasonContentHide" id="measurableContent" style="display: block;">
                    <div>
                        How can the corrective actions be  verified that it was completed as intended?(verification) : Who will verify and by what date?

                    </div>
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Person Responsible</strong></span>
                        <select id="ddlResponsiblePerson" class="contentArea dropDownArea">
                            <option value="0"><< Select >></option>
                        </select>
                    </div>
                    <div class="marginTop10">
                        <span class="specificLabel"><strong>Due Date</strong></span>
                        <input type="text" id="txtMInitialDueDate" readonly="readonly" />
                    </div>
                    <div class="marginTop10">
                        <span class="specificLabel">
                            <strong>Verification Plan</strong>
                        </span>
                        <textarea id="txtVerificationPlan" class="contentArea textArea"></textarea>
                    </div>
                </div>
            </section>
            <section id="sec_effective">

                <h2 id="effectiveHeader" class="seasonContentHide"><a href="javascript:void(0)">EFFECTIVE</a></h2>

                <div class="ContentWidth seasonContentHide" id="" style="display: block; margin-top: 20px;">
                    How will the  fix eliminate the root cause and prevent recurrance of the causal factor?
    How will you measure the effectiveness after implementation (validation)? Who will measure the effectiveness and by what date? 

                    <div class="marginTop10">

                        <strong><span class="specificLabel">Person Responsible</span></strong>
                        <select id="ddlEffectiveResponsilePerson" class="contentArea dropDownArea">
                            <option value="0"><< Select >></option>
                        </select>

                    </div>
                    <div class="marginTop10">

                        <span class="specificLabel"><strong>Due Date</strong></span>
                        <input type="text" id="txtEffectiveDueDate" readonly="readonly" />

                    </div>
                    <div class="marginTop10">

                        <span class="specificLabel"><strong>Validation Plan</strong></span>
                        <textarea id="txtValidationPlan" class="contentArea textArea"></textarea>

                    </div>

                </div>

            </section>
            <section id="sec_reviewed">

                <h2 id="reviewedHeader" class="seasonContentHide"><a href="javascript:void(0)">REVIEWED</a></h2>

                <div class="ContentWidth seasonContentHide" id="" style="display: block;">
                    Has the fix been independently reviewed for unintended  consequences by those that will be impacted and by maintainance, 
        engineering, safety/ quality/ environmental oversight, or others ? (Yes/No) Document the review: 

                    <div class="marginTop10">
                        <strong><span class="specificLabel">&nbsp;</span></strong>
                        <textarea id="txtReviewNotes" class="contentArea textArea"></textarea>
                    </div>

                </div>

            </section>

		</div>

             </div>
        <uc:ucTask runat="server"></uc:ucTask>
          
         <div class="divButtons">
            <%-- <a class="btn btn-primary btn-small" href="#" id="btnSaveCAP">Create</a>
             <a class="btn btn-primary btn-small" href="#" id="btnCancel">Cancel</a>--%>
            
            <input type="button" class="btn btn-primary" id="btnSaveCAP" value="Create" />
            <input type="button" class="btn btn-primary" id="btnCancel" value="Cancel" />
             <input type="button" class="btn btn-primary" id="btnReset" value="Reset" />
        </div>
      
    </div>

 </asp:Content>