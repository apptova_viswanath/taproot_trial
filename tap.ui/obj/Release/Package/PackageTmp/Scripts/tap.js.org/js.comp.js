﻿var _bannerLogo = 1;
var _companyLogo = 2;
var _reportLogo = 3;
var _companyId = '';
var isSUDivision = $('#hdnSUDivision').val();

$(document).ready(function () {

    //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    //Sys.WebForms.PageRequestManager.getInstance().beginAsyncPostBack();
    //function EndRequestHandler(sender, args) {
    //    $('.mydatepickerclass').datepicker({ dateFormat: sessionStorage.dateFormat });
    //}

    $('#body_Adminbody_bannerLogoPreview').click(function () {
        LogoPreview('1');
    });
    $('#body_Adminbody_companyLogoPreview').click(function () {
        LogoPreview('2');
    });
    $('#body_Adminbody_reportLogoPreview').click(function () {
        LogoPreview('3');
    });
    $('#saveCompanyDetails').click(function () {
        SaveCompanyDetails();
        return false;
    });
    $('#clearCompDetailFields').click(function () {
        ClearCompanyFields();
    });


    onPageLoad();
    $("#ui-datepicker-div").addClass("notranslate");
   
});


function onPageLoad() {

    //alert('3');

    //Change the Company Details to Division Details along with respective image.
    if (isSUDivision != '1' || isSUDivision == '' || isSUDivision == null) {
        
        if(!$('#hdnSUCompany').val() == 1)
            $('#lblHeadCompDetails').html('Division Details');
    }

    $('#body_Adminbody_BannerLogoChange').click(function () {
        $('#body_Adminbody_BannerLogoCancel').show();
        ShowFileUploadControl('body_Adminbody_BannerLogoChange', 'body_Adminbody_FileUploadBannerLogo');
        $('#body_Adminbody_FileUploadBannerLogo').val('');
    });
    $('#body_Adminbody_ReportLogoChange').click(function () {
        $('#body_Adminbody_ReportLogoCancel').show();
        ShowFileUploadControl('body_Adminbody_ReportLogoChange', 'body_Adminbody_FileUploadReportLogo');
        $('#body_Adminbody_FileUploadReportLogo').val('');
    });
    $('#body_Adminbody_BannerLogoCancel').click(function () {
        clickCancelLink('body_Adminbody_BannerLogoChange', 'body_Adminbody_FileUploadBannerLogo');
        $('#body_Adminbody_BannerLogoCancel').hide();
        $('#body_Adminbody_UploadBannerLogoValidator').hide();
    });
    $('#body_Adminbody_ReportLogoCancel').click(function () {
        clickCancelLink('body_Adminbody_ReportLogoChange', 'body_Adminbody_FileUploadReportLogo');
        $('#body_Adminbody_ReportLogoCancel').hide();
        $('#body_Adminbody_UploadReportLogoValidator').hide();
    });

    if ($('#body_Adminbody_removeReportLogo').is(':visible')) {
        $('#body_Adminbody_FileUploadReportLogo').hide();
    } else {
        $('#body_Adminbody_FileUploadReportLogo').show();
        $('#body_Adminbody_ReportLogoChange').hide();
    }

    if ($('#body_Adminbody_removeBannerLogo').is(':visible')) {
        $('#body_Adminbody_FileUploadBannerLogo').hide();
    } else {
        $('#body_Adminbody_FileUploadBannerLogo').show();
        $('#body_Adminbody_BannerLogoChange').hide();
    }

    if ($('#body_Adminbody_removeBannerLogo').is(':visible') && $('#body_Adminbody_removeReportLogo').is(':visible')) {
        $('#body_Adminbody_btnUpload').hide();
    }

    $('#body_Adminbody_ReportLogoCancel').hide();
    $('#body_Adminbody_BannerLogoCancel').hide();

    $("input:checkbox").click(function () {
        if ($(this).is(":checked")) {
            var group = "input:checkbox[name='" + $(this).attr("name") + "']";
            $(group).attr("checked", false);
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
        }
    });

    var adSecurity_val = $('#body_Adminbody_hidden_CheckADSecurity').val();
    if (adSecurity_val != null && adSecurity_val.length > 0 && adSecurity_val == 1) {
        $('#chkADSecurity').prop("checked", true);
        $('#chkCustomSecurity').attr("checked", false);
    }
    else {
        $('#chkCustomSecurity').prop("checked", true);
        $('#chkADSecurity').attr("checked", false);
    }

    var divisionId = $('#body_Adminbody_hidden_DivisionId').val();
    if (divisionId > 0)
        $('#lblcompanyName').html('Company Name<span id="spnCompanyName" style="color: Red">*</span>');

    AllowOnlyNumer('body_Adminbody_txtTimeOut');

    //kvs
    //ISSUE: First time selection page goes to the top and adds # to the url, and  Second time datepicker is not displaying
    //datepicker is calling multiple times, is recursing so commented .
    var $strtDate = $('#body_Adminbody_txtSubscriptionStart');
    $strtDate.datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: localStorage.dateFormat

    });

    var $endDate = $('#body_Adminbody_txtSubscriptionEnd');
    $endDate.datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: localStorage.dateFormat
        
    });

    $("#PswPolicyHelpIcon").click(function () {
        OpenPswPolicyPopUp();
        return false;
    });

    $("#btnPswPolicyPopUpCancel").click(function () {
        ClosePswPolicyPopUp();
        return false;
    });

    var dateFormat = localStorage.dateFormat;
    $("#body_Adminbody_txtSubscriptionStart").datepicker({ dateFormat: dateFormat });
    $("#body_Adminbody_txtSubscriptionEnd").datepicker({ dateFormat: dateFormat });
    //$('.mydatepickerclass').datepicker({ altFormat: sessionStorage.dateFormat });
    //$('.mydatepickerclass').datepicker({ dateFormat: sessionStorage.dateFormat });

}

function OpenPswPolicyPopUp() {
    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");

    $('#divPswPolicyDialog').dialog({
        //height: 400,
        minHeight: 200,
        maxheight: 400,
        width: 450,
        modal: true,
        draggable: true,
        resizable: false
    });
}

function ClosePswPolicyPopUp() {
    $("#dialog:ui-dialog").dialog("destroy");
    $('#divPswPolicyDialog').dialog("destroy");
    $('#divPswPolicyDialog').bind("dialogclose", function (event, ui) {
    });
}
// show logo preview popup
function LogoPreview(logoId) {
  
    if (logoId == _bannerLogo) {
        $('#body_Adminbody_imgBannerLogoPreview').show(); $('#body_Adminbody_imgCompanyLogoPreview').hide(); $('#body_Adminbody_imgReportLogoPreview').hide();
    }
    if (logoId == _companyLogo) {
        $('#body_Adminbody_imgCompanyLogoPreview').show(); $('#body_Adminbody_imgBannerLogoPreview').hide(); $('#body_Adminbody_imgReportLogoPreview').hide();
    }
    if (logoId == _reportLogo) {
        $('#body_Adminbody_imgReportLogoPreview').show(); $('#body_Adminbody_imgBannerLogoPreview').hide(); $('#body_Adminbody_imgCompanyLogoPreview').hide();
    }

    $("#divLogoPreviewDialog").dialog({
        width: 400,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });
    
}

function DisplayNotification(messageType) {
    var message = messageType == 'Upload' ? 'Logo uploaded.' : 'Logo removed.'
    NotificationMessage(message, 'jSuccess', true, 1000);

    if (messageType != 'Upload') {
        if (messageType.indexOf('-') > -1) {
            var split = messageType.split('-');
            if (split !=null && split[1] != null && split[1].length > 0) {
                _companyId = GetCompanyId();

                if (_companyId == split[1])
                    $('#TapRooTHeader_divBanner').css('background-image', 'none');
            }
        }
    }
}

function ErrorDisplayNotification() {
    NotificationMessage('Please select at least one logo.', 'jError', true, 3000);
}

function ExtErrorDisplayNotification() {
    NotificationMessage('Please upload only .jpg, .bmp, .gif, .png type file.', 'jError', true, 3000);
}

var _isSubscriptionDivVisible = false;


function getjquerydate() {
}
var companyIdAdmin;
//save company details
function SaveCompanyDetails() {

    _isSubscriptionDivVisible = ($('#divSubscription').is(":visible"));
    _isDirty = false;
    if (localStorage.IsGlobal == 'true')

    var divisionId = $('#body_Adminbody_hidden_DivisionId').val();
    companyIdAdmin = $('#hdnCompanyId').val();

    if (divisionId > 0)
        _companyId = divisionId;
   

    var compnayname = TrimString($('#body_Adminbody_companyName').val());
    var phonenumber = TrimString($('#body_Adminbody_companyPhoneNumber').val());
    var website = TrimString($('#body_Adminbody_companyWebsite').val());
    var address = TrimString($('#body_Adminbody_companyAddress').val());
    var email = TrimString($('#body_Adminbody_companyEmail').val());
    var adsecurity = $('#chkADSecurity').attr('checked') ? 1 : 0;
    var customsecurity = $('#chkCustomSecurity').attr('checked') ? 1 : 0;
    var subscriptionStart = $('#divSubscription').is(":visible") ? $('#body_Adminbody_txtSubscriptionStart').val() : "";
    var subscriptionEnd = $('#divSubscription').is(":visible") ?  $('#body_Adminbody_txtSubscriptionEnd').val(): "";

    if (CompanyDetailvalidation(compnayname, email, phonenumber, website, subscriptionStart, subscriptionEnd)) {
        var companyDetails = new CompanyDetailData(_companyId, compnayname, phonenumber, website, address, email, adsecurity, subscriptionStart, subscriptionEnd);

        _data = JSON.stringify(companyDetails);
        _serviceUrl = _baseURL + 'Services/CompanyDetail.svc/SaveCompanyDetail';
        AjaxPost(_serviceUrl, _data, eval(SaveCompanyDetailsSuccess), eval(SaveCompanyDetailsFail));
    }
}


function CompanyDetailData(companyId, companyName, phoneNumber, website, address, email, adsecurity, subscriptionStart, subscriptionEnd) {
   
    this.companyId = companyId;
    this.companyName = companyName;
    this.phoneNumber = phoneNumber;
    this.website = website;
    this.address = address;
    this.email = email;
    this.adsecurity = adsecurity;
    this.dateFormat = $('#body_Adminbody_cmbDateFormat').val();
    if ($('#body_Adminbody_cmdTimefFormat').val() == '24')
        this.isTwelveHourFormat = false;
    else
        this.isTwelveHourFormat = true;

   // this.isTwelveHourFormat = $("#body_Adminbody_chkTimeFormat").is(':checked');
    this.applicationTimeOut = $('#body_Adminbody_txtTimeOut').val();
    this.subscriptionStart = subscriptionStart;
    this.subscriptionEnd = subscriptionEnd;
    this.passwordPolicy = $('#body_Adminbody_cmbPasswordPolicy').val();
    this.passwordExpiration = $('#body_Adminbody_cmbPasswordExpiration').val();
}


//success message saving data
function SaveCompanyDetailsSuccess(result) {
    
    //
    
    if (result != '' && result.d != null && result.d == 'Saved Successfully') {
        //session storage need to be replace with local storage for using in dateformat in brwser tabs
        //sessionStorage.dateFormat = convertAdminDateFormat($('#body_Adminbody_cmbDateFormat').val());
        if (_companyId == companyIdAdmin) {
            localStorage.dateFormat = $('#body_Adminbody_cmbDateFormat').val() == "" ?
                                      convertAdminDateFormat('MMM dd yyyy') : convertAdminDateFormat($('#body_Adminbody_cmbDateFormat').val());
        }

        ShowHideWarningMessage('body_Adminbody_companyEmail', 'Please specify Email', false);

        var companyName = TrimString($('#body_Adminbody_companyName').val());
        _message = companyName + ' Organization saved successfully.';
        _messageType = 'jSuccess';
        NotificationMessage(_message, _messageType, false, 2000);

       

        $('#btnSetdateformat').click();
        //
        //if (_isSubscriptionDivVisible) {
        //    $('#divSubscription').show();
        //}
        //else {
        //    $('#divSubscription').hide();
        //}
        PageLoad();
        onPageLoad();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
     //   Sys.WebForms.PageRequestManager.getInstance().beginAsyncPostBack();
     
    }//checked for email availability
    else {
        //if (result.d == $('#body_Adminbody_companyEmail').val() + ' already exists') {

        //    ShowHideWarningMessage('body_Adminbody_companyEmail', 'Please specify Email', true);
        //    NotificationMessage(result.d, 'jError', false, 2000);
        //}
        if (result == '') {
            NotificationMessage('Data updation failed.', 'jError', false, 2000);

        }
    }
}

function EndRequestHandler(sender, args) {
    var dateFormat = localStorage.dateFormat;
    $("#body_Adminbody_txtSubscriptionStart").datepicker({ dateFormat: dateFormat });
    $("#body_Adminbody_txtSubscriptionEnd").datepicker({ dateFormat: dateFormat });
    // $('.mydatepickerclass').datepicker({ dateFormat: sessionStorage.dateFormat });
    
    if (!_isSubscriptionDivVisible) {

        $('#divSubscription').hide();
        $('#divSubscriptionStartDate').hide();
        $('#divSubscriptionEndDate').hide();
    }
}

    //fail message
    function SaveCompanyDetailsFail() {
        alert('Service failed.')
}

    //company details validations
    function CompanyDetailvalidation(name, email, phone, website, subscriptionStart, subscriptionEnd) {
        var primaryErrorMessage = 'You are missing the following field: ';
        var errorMessage = primaryErrorMessage;
        var isValid = true;

        errorMessage += ((name == '' || name == null || name.length ==0) ? 'Company Name,\n': '');

        if (name == null || name.length == 0) {
        ShowHideWarningMessage('body_Adminbody_companyName', 'Please specify company name', true);
        }
        else {
        ShowHideWarningMessage('body_Adminbody_companyName', 'Please specify Company name', false);
    }

    if (phone != null && phone.length > 0) {
        if (!validatePhone('body_Adminbody_companyPhoneNumber')) {
            ShowHideWarningMessage('body_Adminbody_companyPhoneNumber', 'Please specify a valid phone number', true);
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_companyPhoneNumber', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_companyPhoneNumber', 'Please specify a valid phone number', false);


    if (email != null && email.length > 0) {
        if (validateEmail(email)) {
            ShowHideWarningMessage('body_Adminbody_companyEmail', 'Please specify a Email', false);
        }
        else {
            ShowHideWarningMessage('body_Adminbody_companyEmail', 'Please specify a valid Email', true);
            isValid = false;
}
} else {
        ShowHideWarningMessage('body_Adminbody_companyEmail', 'Please specify a Email', false);
    }

    if (website != null && website.length > 0) {
        if (!WebsiteValidation('body_Adminbody_companyWebsite')) {
            ShowHideWarningMessage('body_Adminbody_companyWebsite', 'Please specify a valid website', true);
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_companyWebsite', 'Please specify a valid website', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_companyWebsite', 'Please specify a valid website', false);

    _isSUCompany = $('#hdnSUCompany').val();
    var DivisionId = $('#body_Adminbody_hidden_DivisionId').val();

    if (_isSUCompany == "1" && DivisionId > 0) {
        if (subscriptionStart == null || subscriptionStart.length == 0) {
            ShowHideWarningMessage('body_Adminbody_txtSubscriptionStart', 'Please specify a subscription start date', true);
            errorMessage += 'Subscription Start Date,\n';
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_txtSubscriptionStart', 'Please specify a subscription start date', false);

        if (subscriptionEnd == null || subscriptionEnd.length == 0) {
            ShowHideWarningMessage('body_Adminbody_txtSubscriptionEnd', 'Please specify a subscription end date', true);
            errorMessage += 'Subscription End Date';
            isValid = false;
        }
        else
            ShowHideWarningMessage('body_Adminbody_txtSubscriptionEnd', 'Please specify a subscription end date', false);
    }

    if (errorMessage.length > primaryErrorMessage.length && errorMessage.substring(errorMessage.length - 1) == ',')
        errorMessage = errorMessage.replace(errorMessage.substring(errorMessage.length -1), '');


    if (errorMessage.length > primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 2000);
        return false;
}
else {
        if (_isSUCompany == '1' && DivisionId > 0) {

       
            if ($('#divSubscription').is(":visible")) {
                errorMessage = SubscriptionValidation('body_Adminbody_txtSubscriptionStart', 'body_Adminbody_txtSubscriptionEnd');
            }

            if (errorMessage.length > primaryErrorMessage.length) {
                _message = errorMessage;
                _messageType = 'jError';
                NotificationMessage(_message, _messageType, true, 2000);
                return false;
        }
}
    }
    if (!isValid)
            return false;

        return true;
}

    //clear fields
    function ClearCompanyFields() {
        $('#body_Adminbody_companyPhoneNumber').val('');
        $('#body_Adminbody_companyWebsite').val('');
        $('#body_Adminbody_companyAddress').val('');
        $('#body_Adminbody_companyName').val('');
        $('#body_Adminbody_companyEmail').val('');

    $('#body_Adminbody_cmbDateFormat').val('0');
    $('#body_Adminbody_cmdTimefFormat').val('0');
        // $("#body_Adminbody_chkTimeFormat").attr('checked', false);
         $('#body_Adminbody_txtTimeOut').val('');
         $('#spanExampleDateFormat').hide();
         }

         function ShowFileUploadControl(hideId, showId) {
             $('#body_Adminbody_btnUpload').show();
             $('#' +hideId).hide();
             $('#' +showId).show();
    //$('#' + showId).css('visibility', 'visible');
    }

    function clickCancelLink(showId, hideId) {
        $('#' +hideId).hide();
        $('#' +showId).show();
        if ($('#body_Adminbody_BannerLogoChange').is(':visible') && $('#body_Adminbody_ReportLogoChange').is(':visible')) {
        $('#body_Adminbody_btnUpload').hide();
        }
        else {
        $('#body_Adminbody_btnUpload').show();
    }


}