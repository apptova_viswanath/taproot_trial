﻿//** Main Navigation Menus (Deepa)**//

//page load events
$(document).ready(function () {

    //On mouse hover show Sub Menus
    DisplaySubMenuOnMouseHover();
});

//On mouse hover show Sub Menus
function DisplaySubMenuOnMouseHover() {

    jQuery("#TapRooTHeader_Menu_divTopNavigationBar ul ul").hover(
        function () {
            jQuery(this).parent().addClass("selectTabHeader");
        },
    function () {
        jQuery(this).parent().removeClass("selectTabHeader");
    });

    jQuery("#TapRooTHeader_Menu_divTopNavigationBar li#IncidentsTabs").hover(function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#IncidentsTabs ul').fadeIn(100);
    }, function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#IncidentsTabs ul').fadeOut(100);
    });

    jQuery("#TapRooTHeader_Menu_divTopNavigationBar li#InvestigationsTabs").hover(function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#InvestigationsTabs ul').fadeIn(100);
    }, function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#InvestigationsTabs ul').fadeOut(100);
    });

    jQuery("#TapRooTHeader_Menu_divTopNavigationBar li#AuditsTabs").hover(function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#AuditsTabs ul').fadeIn(100);
    }, function () {
        jQuery('#TapRooTHeader_Menu_divTopNavigationBar li#AuditsTabs ul').fadeOut(100);
    });
}