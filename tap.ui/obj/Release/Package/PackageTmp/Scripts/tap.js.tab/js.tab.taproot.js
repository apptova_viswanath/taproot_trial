﻿/*TapRooT Screen Methods (usha)*/

//Global Variables
var _data = '';
var _serviceUrl = '';
var _eventId = '';
var _svcURL = '';
var SPRING_SEASON = "1";
var SUMMER_SEASON = "2";
var AUTUMN_SEASON = "3";
var WINTER_SEASON = "4";
var LNK_SUMMER = 'lnkSummer';
var LNK_AUTUMN = 'lnkAutumn';
var _season = '';
var TAPROOT = 'TapRooT';
var _virtualdirectorypath = '';


//ready function
$(document).ready(function () {
   
    $('#contentSpring').hide();
    $('#contentSummer').hide();
    $('#contentAutumn').hide();

    //page load method
    PageLoadAction();

    //link button click functioanlity to get the season
    $('#lnkSpring,#lnkSummer,#lnkAutumn').click(function () {   
    });
  
    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });

    //GetTeamMemberAccess('divTabTapRooT');
    var tmEventId = _eventId;
    if (_eventId.indexOf('#') > -1)
        tmEventId = _eventId.replace('#', '');
   
    AllRCTComplete();

    GetTMAccessWithEventId(tmEventId, 'divTabTapRooT');

});
//end ready function

function AllRCTComplete() {
    var rctData = {};
    //Pass the parameters
    rctData.eventId = _eventId;
    rctData = JSON.stringify(rctData);
    _serviceUrl = _baseURL + 'Services/RootCause.svc/AllRCTComplete';
    AjaxPost(_serviceUrl, rctData, eval(AllRCTCompleteSuccess), eval(AllRCTCompleteFail));
}

function AllRCTCompleteData() {
    this.eventId = _eventId;
}
function AllRCTCompleteSuccess(result) {
    
    if (result != null && result.d != null) {
        if (result.d == true && $('#rctTemplateResults').html().length > 0) {
            $('#imgAutumn').removeClass('seasonContentHide');
            $('#divAutumn').addClass('snapChartDivPaddingCompleted'); //Align the Analyze word properly.
        }
        else {
            $('#imgAutumn').addClass('seasonContentHide');
            $('#divAutumn').removeClass('snapChartDivPaddingCompleted'); //Align the Analyze word properly.
            $('#divAutumn').addClass('snapChartDivPadding'); //Align the Analyze word properly.
        }
    }
}
function AllRCTCompleteFail() {
    
}

function RedirectToVisualRCT(causalFactorID)
{
    var url = _baseURL + _module + '/VisualRCT-' + causalFactorID + '/' + _eventId;
    newwindow = window.open(url, 'Visual RCT', 'height=1000,width=1300,resizable=yes,scrollbars=yes,scrollbars=1');

    if (window.focus) { newwindow.focus() }
    return false;
}

//onclick of RCT link
function RedirectToRCT(causalFactorID) {
    
    var url = _baseURL + _module + '/RootCauseTree-' + causalFactorID + '/' + _eventId;
    
    //var ua = window.navigator.userAgent;
    //var msie = ua.indexOf("MSIE ");

    //if (msie > 0)
    //{
    //    $(newwindow).css("overflow", "scroll");
    //    newwindow = window.open(url, 'RCT', 'height=1000,width=1300,resizable=yes,scrollbars=yes,scrollbars=1');
    //}
    //else
    //{  
    //    newwindow = window.open(url, 'RCT', 'height=1000,width=1300,resizable=yes,scrollbars=no,scrollbars=0');
    //    $(newwindow).css("overflow", "hidden !important");
    //}
    ////newwindow = window.open(url, 'RCT', 'height=1000,width=1300,resizable=yes,scrollbars=no,scrollbars=0');

    newwindow = window.open(url, 'RootCauseTree', 'height=1000,width=1300,resizable=yes,scrollbars=yes,scrollbars=1');

    if (window.focus) { newwindow.focus() }
    return false;

}


//Page load
function PageLoadAction() {

    //Jquery main Tabs
    JqueryTabs();

    // highlighting selected Tab
    TabColor(location, 2);

    _virtualdirectorypath = GetVirtualDirectory();
    _svcURL=GetServicePath()//get service path

    GetIdsFromUrl();
    GetInvestigationSession();

}


//create object of snapchart class
function SnapChartData(snapChartId, seasonId, isComplete, snapChartData, isReverse) {

    this.snapChartId = snapChartId;
    this.eventId = _eventId;
    this.seasonId = seasonId;
    this.isComplete = isComplete;
    this.snapChartData = snapChartData;
    this.isReverse = isReverse;
}

//get Investigation season
function GetInvestigationSession() {
    
    _data = '&eventId=' + _eventId;
    _serviceUrl = _baseURL + 'Services/TapRootTab.svc/GetSnapChartDetails';
    Ajax(_serviceUrl, _data, eval(GetInvestigationSessionSuccess), eval(GetInvestigationSessionFail), 'GetSnapChartDetails');
}

//Success method for GetInvestigationSession
function GetInvestigationSessionSuccess(result) {
    

    if (result.d != null && result.d != undefined && result.d != "null") {       
        
        //var data = ParseToJSON(result.d);
        //_season = data[0].seasonId;

        var data = result.d.split(",");
        if (data.length > 0)
            _season = data[0];        

        PopulateSnapChartData(_season);

        if (_season == AUTUMN_SEASON) {

            var isRCTpending = true;

            if (data.length > 1)
                 isRCTpending = data[1] > 0 ? true : false;

            if (!isRCTpending) {
                $('#imgAutumn').removeClass('seasonContentHide');
                //Align the Analyze word properly.
                $('#divAutumn').addClass('snapChartDivPaddingCompleted');
            }
            
            //var allRCTCompleted = data[0].allRCTCompleted > 0 ? false : true;

            //if (allRCTCompleted) {
            //    $('#imgAutumn').removeClass('seasonContentHide');
            //    //Align the Analyze word properly.
            //    $('#divAutumn').addClass('snapChartDivPaddingCompleted');
            //}
        }
    }
    else {
        PopulateSnapChartData(SPRING_SEASON);
    }

    PopulateCausalFactors(false, false, false);

}


//service failed method
function GetInvestigationSessionFail() {

}


function PopulateSnapChartData(result) {
    
    HideControls();

    var season = GetSeasonName(result);
    var cssSeason = season.toLowerCase();
    var header = ((result == SUMMER_SEASON) ? 'Investigate' : (result == AUTUMN_SEASON || result == WINTER_SEASON) ? 'Analyze' : ' Plan Your Investigation');
    var navBreadCrumbSeason = '';

    $('#snapchartHeader')[0].innerHTML = header;
    $('#snapchartContent').addClass(cssSeason);

    $('#content' + season).removeClass();
    $('#content' + season).addClass('seasonContentShow');

    $('#lnk' + season).removeClass('seasonContentHide');
    $('#lnk' + season).addClass('seasonContentShow');
    $('#lnk' + season + 'Complete').removeClass('seasonContentHide').addClass('seasonContentShow');

    if (result == AUTUMN_SEASON || result == WINTER_SEASON) {

        $('#divRootCauseResult').removeClass('seasonContentHide');
        $('#divRootCauseResult').addClass('seasonContentShow');

        $('#divCausalFactorContent').removeClass('seasonContentHide');
        $('#divCausalFactorContent').addClass('seasonContentShow');
    }
    
    //display navigation breadcrumb based on season
    header = header == ' Plan Your Investigation' ? 'Plan' : header;
    navBreadCrumbSeason = $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').html();
    navBreadCrumbSeason = navBreadCrumbSeason.substring(0, navBreadCrumbSeason.lastIndexOf('&gt')) + '  >  ' + header;
    //navBreadCrumbSeason = navBreadCrumbSeason.substring(0, navBreadCrumbSeason.lastIndexOf('&gt')) + '  >  ' + TAPROOT + '  -  ' + season;    
    $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').html(navBreadCrumbSeason);
    

    $('#contentSpring').hide();
    $('#contentSummer').hide();
    $('#contentAutumn').hide();

    UpdateStepText();

    $('#content' + season).show();

    SnapChartCompletedSuccess(result);
}

//service failed method
function SnapChartCompletedFail() {

}


//show Hide functionality based on season selected

function HideControls() {
   
    $('#snapchartContent').removeClass('spring');
    $('#snapchartContent').removeClass('summer');
    $('#snapchartContent').removeClass('autumn');

    $('#contentSpring').addClass('seasonContentHide');
    $('#lnkSpring').addClass('seasonContentHide');

    $('#contentSummer').addClass('seasonContentHide');
    $('#lnkSummer').addClass('seasonContentHide');

    $('#contentAutumn').addClass('seasonContentHide');
    $('#lnkAutumn').addClass('seasonContentHide');


    $('#imgSpring').addClass('seasonContentHide');
    $('#imgSummer').addClass('seasonContentHide');
    $('#imgAutumn').addClass('seasonContentHide');

    $('#divRootCauseResult').addClass('seasonContentHide');
    $('#divCausalFactorContent').addClass('seasonContentHide');

    $('#lnkSpringComplete').addClass('seasonContentHide');
    $('#lnkSummerComplete').addClass('seasonContentHide');
    $('#lnkAutumnComplete').addClass('seasonContentHide');
}


//get the seasonname from season id
function GetSeasonName(seasonId) {

    var seasonname = ((seasonId == SUMMER_SEASON) ? 'Summer' : (seasonId == AUTUMN_SEASON || seasonId == WINTER_SEASON) ? 'Autumn' : 'Spring');
    return seasonname;
}

//get eventId  from Url
function GetIdsFromUrl() {

    var spilttedURL = window.location.href.split('/');
    _eventId = spilttedURL[spilttedURL.length - 1];
    _module = spilttedURL[spilttedURL.length - 3];

}



