﻿// Admin's User Edit Page Methods //

//Global Variable

var _serviceUrl = '';
var _data = '';

var _eventId = -1;
var _eventType = '';

var _fields = new Object();
var _fieldValue = '';
var _undoStack = new Array;

var _message = '';
var _messageType = '';
var _companyId = '';
var _userId = '';
var _svcURL = '';

var _isEventData = '';
var manageUserPageUrl = '';
var _initialUserName = '';
var _onBlurParam = '';
var _userSaveButton = 'btnSaveUserDetails';

//ready Method
$(document).ready(function () {
    
    ////Password Policy
    //$('#body_Adminbody_txtPassword').keyup(function () {

    //    ValidatePassWordPolicy($(this).val());      

    //}).focus(function () {

    //    ValidatePassWordPolicy($(this).val());
    //    $('#pswd_info').show();

    //}).blur(function () {
        
    //    $('#pswd_info').hide();
    //    ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val());

    //});


    ////Password Policy
    //$('#body_Adminbody_txtConfirmPassword').keyup(function () {
    //    ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val());
    //});


    //kvs
    //var dateFormat = localStorage.dateFormat;
    //$("#body_Adminbody_txtSubscriptionUserStart").datepicker({ dateFormat: dateFormat });
    //$("#body_Adminbody_txtSubscriptionUserEnd").datepicker({ dateFormat: dateFormat });
    //if (sessionStorage.IsGlobal == 'true')
    //{
    //    $('#divSubscriptiondetails').show();
    //}
    //else
    //{
    //    $('#divSubscriptiondetails').hide();
    //}
    //kve

    $('#btnDeleteUserDetails').hide();

    // Click handler for Save button
    $('#btnSaveUserDetails').click(function () {
      
        SaveUserDetails();
        return false;
    });
    
    $('#btnSaveUserDetailsCancel').click(function () {
        OnSaveUserDetailsCancel();
        return false;
    });

    SetPageLoad();
   
    _isEventData = false;
    
    manageUserPageUrl = GetManageUserUrl();
    $("#ancUsersList").attr("href", manageUserPageUrl);

    $('#body_Adminbody_txtFirstName').focus();

    $('#btnDeleteUserDetails').click(function () {
        OnClickEnableDisable();
    });
});


function ValidatePassWordPolicy(pswd) {

    if (pswd.length < $('#spnLength').html()) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }

    //validate upper case letter
    if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
        $('#upperCase').removeClass('invalid').addClass('valid');
    } else {
        $('#upperCase').removeClass('valid').addClass('invalid');
    }

    //validate lower case letter
    if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
        $('#lowerCase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowerCase').removeClass('valid').addClass('invalid');
    }
    
    //validate number
    var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
    if (pswd.match(numberPattern)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }


    //validate special character
    var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
    if (pswd.match(specialCharPattern)) {
        $('#specialCharacter').removeClass('invalid').addClass('valid');
    } else {
        $('#specialCharacter').removeClass('valid').addClass('invalid');
    }


    if (pswd.length > $('#spnMaxCharacterCount').html()) {
        $('#maxCharacter').removeClass('valid').addClass('invalid');
    } else {
        $('#maxCharacter').removeClass('invalid').addClass('valid');
    }

}


function SetPageLoad() {
    var elements = $('.trackChanges');
    GetInitialValues(elements);
    GetUserId();

    // Hide Undo and Undo all while loading the page
    $('#ancUndo').hide();
    $('#ancUndoAll').hide();
    $('#lblCompanyID').hide();

    $('#body_Adminbody_txtCompanyID').hide();

    //Auto save the fields data on edit mode
    //GetUserId();
    _isSUCompany = $('#hdnSUCompany').val();

    // If the mode is edit mode, then hide save and cancel buttons
    if (_eventId > 0) {
        $('#btnSaveUserDetails').val('Apply Changes');
        _initialUserName = $('#body_Adminbody_txtUserName').val();
    }    
    $('#body_Adminbody_txtPassword').val($('#body_Adminbody_hiddenPassword').val());
    $('#body_Adminbody_txtConfirmPassword').val($('#body_Adminbody_hiddenPassword').val());

    var isSUDivision = $('#hdnSUDivision').val();

    if ($('#body_Adminbody_hiddenUserId').val() > 0) {
        $('#btnSaveUserDetails').val('Apply Changes');
    }
    else {
        $('#btnSaveUserDetails').val('Create');
    }

    if ((_isSUCompany != null && _isSUCompany == '1') || (isSUDivision != null && isSUDivision == "1")) {
        $('#divUndoUserDetails').hide();
    }
    else {
        $('#divUndoUserDetails').show();
    }
    
    if(isSUDivision != null && isSUDivision =="1")
        $('#divUserActive').hide();

    setEnableDisableOnLoad();

}

function setEnableDisableOnLoad() {
    var useractive = $('#body_Adminbody_hiddenIsUserActive').val();
    var userEvents = $('#body_Adminbody_hiddenIsUserHasEvent').val();
    var isSUDivision = $('#hdnSUDivision').val();
    if (userEvents == 'True') {
        $('#btnDeleteUserDetails').hide();
    }
    else if (_isSUCompany != "1" && isSUDivision != "1") {
        $('#btnDeleteUserDetails').show();
    }    

}

// Function to find the user id from url
function GetUserId() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _eventId = splitUrl[splitUrl.length - 2];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }
    _eventType = -1; // Just sending dummy value for event type
    return _eventType;
}

function GetManageUserUrl() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    url = '';
    var i = 0;
    while (splitUrl[i].toLowerCase() != 'users') {
        url = url + splitUrl[i] + '/';
        i++;
    }
    url = url + splitUrl[i];
    return url;
}

function clickOnChkDivision(id) {
}


//Save the User details
function SaveUserDetails() {
    
    _isDirty = false;
    var txtUserName = 'body_Adminbody_txtUserName';
    var validationImage = 'validationImg';
    
    var userName = TrimString($('#body_Adminbody_txtUserName').val());
    var password = TrimString($('#body_Adminbody_txtPassword').val());    
    var cPassword = TrimString($('#body_Adminbody_txtConfirmPassword').val());    
    var companyId = $('#body_Adminbody_txtCompanyID').val();
    var firstName = TrimString($('#body_Adminbody_txtFirstName').val());
    var lastName = TrimString($('#body_Adminbody_txtLastName').val());
    var phoneNumber = TrimString($('#body_Adminbody_txtPhone').val());
    var emailId = TrimString($('#body_Adminbody_txtEmail').val());
    
    var active = ($('#body_Adminbody_chkActive').attr('checked') == 'checked') ? 1 : 0;
    var admin = ($('#body_Adminbody_chkAdmin').attr('checked') == 'checked') ? 1 : 0;
    var userID = TrimString($('#body_Adminbody_hiddenUserId').val());
    var subscriptionStart = TrimString($('#body_Adminbody_txtSubscriptionUserStart').val());
    var subscriptionEnd = TrimString($('#body_Adminbody_txtSubscriptionUserEnd').val());
    var LanguageId = 0;
    
    if ($('#body_Adminbody_txtPassword').prop('disabled')) { password = false; }
    if ($('#body_Adminbody_txtConfirmPassword').prop('disabled')) { cPassword = false; }

    if (!password && !cPassword)
    {
        NotificationMessage('Active Directory user information cannot be updated from TapRooT&#174; software. ', 'jError', true, 20000);
        return false;
    }

    if ($('#btnSaveUserDetails').val() == 'Create')
    {
        LanguageId = 16;
    }

    var viewAllEvents = ($('#body_Adminbody_chkViewAllEvents').attr('checked') == 'checked') ? 1 : 0;
    var editAllEvents = ($('#body_Adminbody_chkEditAllEvents').attr('checked') == 'checked') ? 1 : 0;
    if (ValidateUserDetailFields(userName, firstName, lastName, emailId, password, cPassword,  active, phoneNumber) && ValidatePasswords(password, cPassword)) {
        var userDetails = new UserDetailsData(userID, userName, password, companyId, firstName, lastName, phoneNumber, emailId,  active, admin, viewAllEvents, editAllEvents,subscriptionStart,subscriptionEnd, LanguageId);

        _data = JSON.stringify(userDetails);
        _serviceUrl = _baseURL + 'Services/UserDetails.svc/AddUser';
        AjaxPost(_serviceUrl, _data, eval(SaveUserDetailsSuccess), eval(SaveUserDetailsFailed));
    }
}


function ValidateUserDetailFields(username, firstname, lastname, email, password, confirmpassword, active, phonenumber) {

    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;
    var isValid = true;

    //errorMessage += ((username == '') ? 'User Name,\n' : (username != null && !UserNameCharsValidation(username)) ? 'Invalid User Name,\n' : '');
    errorMessage += ((username == '') ? 'User Name,\n' : '');
    errorMessage += ((firstname == '') ? 'First Name,\n' : '');
    errorMessage += ((lastname == '') ? 'Last Name,\n' : '');
    errorMessage += ((email == '') ? 'Email,\n' : '');

    if (password != false)
        errorMessage += ((password == '') ? 'Password,\n' : '');
    if (confirmpassword != false)
    errorMessage += ((confirmpassword == '') ? 'Confirm Password,\n' : '');

    //errorMessage += ((phonenumber != null && phonenumber.length > 0 && !validatePhone('body_Adminbody_txtPhone')) ? 'Invalid Phone Number,\n' : '');

    if (errorMessage[errorMessage.length - 2] == ',') {
        errorMessage = errorMessage.substring(0, errorMessage.length - 2);
        errorMessage = errorMessage + '.';
    }

    var txtUserGroup = 'body_Adminbody_ddlUserGroup';
    var txtUserStatus = 'body_Adminbody_ddlUserStatus';
    var txtUserName = 'body_Adminbody_txtUserName';
    var txtFirstName = 'body_Adminbody_txtFirstName';
    var txtLastName = 'body_Adminbody_txtLastName';
    var txtEmail = 'body_Adminbody_txtEmail';
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    var isValidEmail = true;
    var isPasswordMatch = true;

    if (username == null || username.length == 0) {
        ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
    }
    else {
        ShowHideWarningMessage(txtUserName, 'Please specify User Name', false);
    }
    if (firstname == null || firstname.length == 0) {

        ShowHideWarningMessage(txtFirstName, 'Please specify First Name', true);
    }
    else {
        if (!CharsValidation(firstname)) {
            isValid = false;
            ShowHideWarningMessage(txtFirstName, 'Please enter a valid First Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtFirstName, 'Please specify First Name', false);
        }
    }

    if (lastname == null || lastname.length == 0) {
        ShowHideWarningMessage(txtLastName, 'Please specify Last Name', true);
    }
    else {
        if (!CharsValidation(lastname)) {
            isValid = false;
            ShowHideWarningMessage(txtLastName, 'Please enter a valid Last Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtLastName, 'Please specify Last Name', false);
        }
    }
    
    if (email == null || email.length == 0) {
        ShowHideWarningMessage(txtEmail, 'Please specify a Email', true);
    }
    else {
        
        if (validateEmail(email)) {
            ShowHideWarningMessage(txtEmail, 'Please specify a Email', false);            
        }
        else {
            ShowHideWarningMessage(txtEmail, 'Please specify a valid Email', true);
            isValidEmail = false;
            isValid = false;
        }
    }
    
    if (password != false) {
        if (password == null || password.length == 0) {
            ShowHideWarningMessage(txtPassword, 'Please specify a Password', true);
        }
        else {
            ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
            if (confirmpassword == null || confirmpassword.length == 0) {

                ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);
                isPasswordMatch = false;
                isValid = false;
            }
            else {
                ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
            }
        }
    }
    else {
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
    }

    if (confirmpassword != false) {
        if (confirmpassword == null || confirmpassword.length == 0) {
            ShowHideWarningMessage(txtConfirmPassword, 'Please specify Confirm Password', true);
        }
        else {
            ShowHideWarningMessage(txtConfirmPassword, 'Please specify Confirm Password', false);
        }
    }
    else
    {
        ShowHideWarningMessage(txtConfirmPassword, 'Please specify Confirm Password', false);
    }

    if (phonenumber != null && phonenumber.length > 0) {
        if (!validatePhone('body_Adminbody_txtPhone')) {
            isValid = false;
            ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', true);
        }
        else
            ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('body_Adminbody_txtPhone', 'Please specify a valid phone number', false);


    if (errorMessage.length != primaryErrorMessage.length) {

        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 20000);
        return false;
    }
    if (!isValid)
        return false;

    return true;
}
function CalulatePasswordexpiry(userId)
{
    
    var UserIDData = {};
    UserIDData.userId = userId;
  
    _data = JSON.stringify(UserIDData);

    _serviceUrl = _baseURL + 'Services/Password.svc/PasswordExpiresInDays';
    AjaxPost(_serviceUrl, _data, eval(PasswordExpiresInDaysSuccess), eval(PasswordExpiresInDaysFailed));
}

function PasswordExpiresInDaysSuccess(result)
{
    
    if (result.d != "") {
        var PasswordexpiresIn = result.d;
        if (Number(PasswordexpiresIn) > 0) {
            document.getElementById('PasswordNotExpired').style.display = 'inline';
            document.getElementById('PasswordExpired').style.display = 'none';
            document.getElementById('lblPasswordexpiresIn').innerHTML = PasswordexpiresIn + ' Days';
        }
        else {
            document.getElementById('PasswordExpired').style.display = 'inline';
            document.getElementById('PasswordNotExpired').style.display = 'none';
            var expiredDaysago = Math.abs(PasswordexpiresIn).toString();
            document.getElementById('lblPasswordexpiredIn').innerHTML = expiredDaysago;
            
        }
    }
}
function PasswordExpiresInDaysFailed()
{

}
function SaveUserDetailsSuccess(result) {
    
    var textId = 'body_Adminbody_txtUserName';
    var warnMsg = 'Please specify User Name';
    //var txtUserName = 'body_Adminbody_txtUserName';
    var message = 'User account created successfully for ' + $('#body_Adminbody_txtFirstName').val();

    var userID = TrimString($('#body_Adminbody_hiddenUserId').val());
    if (userID != null && userID.length > 0) {
        message = 'User details saved successfully for ' + $('#body_Adminbody_txtFirstName').val();
        
        CalulatePasswordexpiry(userID);
      //  $('#calculateexirydays').click();
    }
    var messageType = 'jSuccess';
    if (result.d != "") {
        
        messageType = 'jError';
        message = 'User name ' + $('#body_Adminbody_txtUserName').val() + ' already exists';
        
        if (result.d == $('#body_Adminbody_txtEmail').val() + ' already exists') {
            message = result.d;
            textId = 'body_Adminbody_txtEmail';
            warnMsg = 'Please specify Email';
        }

        ShowHideWarningMessage(textId, warnMsg, true);
        //ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
        NotificationMessage(message, messageType, true, 3000);
    } else {
        ShowHideWarningMessage(textId, warnMsg, false);
        //ShowHideWarningMessage(txtUserName,'Please specify User Name', false);
        NotificationMessage(message, messageType, true, 3000);

        var isSUDivision = $('#hdnSUDivision').val();
        if (isSUDivision != null && isSUDivision == '1')
            return;       
        //else {
            //$('#btnSaveUserDetails').hide();
            //RedirectToUserList();
           // $('#calculateexirydays').click();

       // }
    }
}

function SaveUserDetailsFailed() {
    alert('Service failed.');
}

//property of Users class
function UserDetailsData(userID, userName, password, companyId, firstName, lastName, phoneNumber, emailId, active, admin, viewEvents, editEvents,subscriptionStart, subscriptionEnd,LanguageID) {
    this.userID = userID;
    this.userName = userName;
    this.password = password;
    this.companyId = companyId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.emailId = emailId;
    this.active = active;
    this.admin = admin;
    this.viewEvents = viewEvents;
    this.editEvents = editEvents;
    this.subscriptionStart = subscriptionStart
    this.subscriptionEnd = subscriptionEnd
    this.LanguageID = LanguageID
}

function validateUserDetailsWarningMsg() {

    var txtUserGroup = 'body_Adminbody_ddlUserGroup';
    var txtUserStatus = 'body_Adminbody_ddlUserStatus';
    var txtUserName = 'body_Adminbody_txtUserName';
    var txtFirstName = 'body_Adminbody_txtFirstName';
    var txtLastName = 'body_Adminbody_txtLastName';
    var txtEmail = 'body_Adminbody_txtEmail';
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

}

function ValidatePasswords(Password, cPassword) {

    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    if (Password != null && Password.match(_passwordRegularExpression) != null) {

        if (Password != cPassword) {

            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;

        } else {

            ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }
    else {

        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', true);
        //$('#pswd_info').show();
        return false;
    }
       
    return true;
}

function OnSaveUserDetailsCancel() {
    $('#body_Adminbody_txtUserName').val('');
    $('#body_Adminbody_txtPassword').val('');
    $('#body_Adminbody_txtCompanyID').val('');
    $('#body_Adminbody_txtFirstName').val('');
    $('#body_Adminbody_txtLastName').val('');
    $('#body_Adminbody_txtPhone').val('');
    $('#body_Adminbody_txtEmail').val('');
    $('#body_Adminbody_ddlUserGroup').val(0);
    $('#body_Adminbody_ddlUserStatus').val(1);
    $('#body_Adminbody_txtConfirmPassword').val('');
    $('#body_Adminbody_chkActive').attr('checked', false);
    $('#body_Adminbody_chkViewAllEvents').attr('checked', false);
    $('#body_Adminbody_chkEditAllEvents').attr('checked', false); 
    $('#body_Adminbody_chkAdmin').attr('checked', false);
    //RedirectToUserList();
}

function RedirectToUserList() {   
    if (manageUserPageUrl != '') {
        window.location.href = manageUserPageUrl;
    }
}

//create object of Location and classification list
function ListDetail(userName) {
    var userData = {};
    userData.userName = userName;
    return userData;
}

function OnBlurWithValidation(e) {
    if (TrimString($('#body_Adminbody_txtUserName').val()) == _initialUserName)
        return;
    _onBlurParam = e;
    var userName = TrimString($('#body_Adminbody_txtUserName').val());
    var locationDetails = ListDetail(userName);
    var _data = JSON.stringify(locationDetails);

    _serviceUrl = _baseURL + 'Services/UserDetails.svc/IsUserExists';
    AjaxPost(_serviceUrl, _data, eval(IsUserExistsSuccess), eval(IsUserExistsFailed));
}

function OnBlurWithoutValidation(e, itemId) {

    var txtUserGroup = 'body_Adminbody_ddlUserGroup';
    var txtUserStatus = 'body_Adminbody_ddlUserStatus';
    var txtUserName = 'body_Adminbody_txtUserName';
    var txtFirstName = 'body_Adminbody_txtFirstName';
    var txtLastName = 'body_Adminbody_txtLastName';
    var txtEmail = 'body_Adminbody_txtEmail';
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

    if (itemId != null && itemId.length > 0) {

        if (itemId == txtFirstName) {

            var value = TrimString($('#' + txtFirstName).val());
            if (value == null || value.length == 0) {
                ShowHideWarningMessage(txtFirstName,'Please specify First Name', true);
                return;
            }
            else {
                if (!CharsValidation(value)) {
                    ShowHideWarningMessage(txtFirstName, 'Please enter a valid First Name, specify only letters', true);
                    return;
                } else {
                    ShowHideWarningMessage(txtFirstName, 'Please specify First Name', false);
                }
            }
            
        }
        if (itemId == txtUserStatus) {
            var value = TrimString($('#' + txtUserStatus).val());
            if (!value || value < 0) {
                ShowHideWarningMessage(txtUserStatus,'Please select a Status', true);
            return;
        }
        else {
            ShowHideWarningMessage(txtUserStatus, 'Please select a Status', false);
        }
    }
   
    if (itemId == txtLastName) {
        var value = TrimString($('#' + txtLastName).val());
        if (value == null || value.length == 0) {
            ShowHideWarningMessage(txtLastName,'Please specify Last Name', true);
            return;
        }
        else {
            if (!CharsValidation(value)) {
                ShowHideWarningMessage(txtLastName, 'Please enter a valid Last Name, specify only letters', true);
                return;
            } else {
                ShowHideWarningMessage(txtLastName, 'Please specify Last Name', false);
            }
        }
           
    }
    if (itemId == txtEmail) {
        var value = TrimString($('#' + txtEmail).val());
        if (value == null || value.length == 0) {
            ShowHideWarningMessage(txtEmail, 'Please specify a EmailId', true);
            return;
        }
        else {

            if (validateEmail(value)) {
                ShowHideWarningMessage(txtEmail,'Please specify a EmailId', false);
            }
            else {
                ShowHideWarningMessage(txtEmail, 'Please specify a valid EmailId',true);
                return;
            }
        }
            
    }

}

SaveFields(e);
}

function OnBlurPasswordValidation(e) {
    var passWord = TrimString($('#body_Adminbody_txtPassword').val());
    var confirmPassWord = TrimString($('#body_Adminbody_txtConfirmPassword').val());

    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';
    
    if (passWord == confirmPassWord) {
        ShowHideWarningMessage(txtPassword,'Please specify a Password', false);
        ShowHideWarningMessage(txtConfirmPassword,'Password and Confirm Password must be match', false);
        if (!visibleSaveButton) {
          
            SaveFields(e);
        }
    } else {
        ShowHideWarningMessage(txtPassword,'Password and Confirm Password must be match', true);
        ShowHideWarningMessage(txtConfirmPassword,'Password and Confirm Password must be match', true);
    }

}

function PasswordRegularExpValidation(val) {
    
    var upperCase= new RegExp('[A-Z]');
    var lowerCase= new RegExp('[a-z]');
    var numbers = new RegExp('[0-9]');
    var specials = new RegExp('[`@#!%$&^*()]');
    var  regx = /^[a-zA-Z0-9\s\[\]\.\-#']*$/;
    var pattern = /^[a-zA-Z_0-9@\!#\$\^%&*()+=\-[]\\\';,\.\/\{\}\|\":<>\? ]+$/;
   
    if (val.match(upperCase) && val.match(lowerCase) && val.match(numbers) && val.match(specials)) {
        return true;
    }
    else {
        return false;
    }
}

function StrictPasswordValidation(val) {

    var upperCase = new RegExp('[A-Z]');
    var lowerCase = new RegExp('[a-z]');
    var numbers = new RegExp('[0-9]');
    var specials = new RegExp('[=@#!%$+^*!_]');
    var regx = /^[a-zA-Z0-9\s\[\]\.\-#']*$/;
    var pattern = /^[a-zA-Z_0-9@\!#\$\^%&*()+=\-[]\\\';,\.\/\{\}\|\":<>\? ]+$/;

    val = (val != null) ? TrimString(val) : val;

    if (val != null && val.length < 15)
        return "Must be 15 characters in length";

    if (val.match(upperCase).length >1 && val.match(lowerCase).length > 1 && val.match(numbers).length > 1 && val.match(specials).length > 1) {
        //return true;
        alert('match');
    }
    else
        alert('not match');;
    
}

function StrongPasswordValidation(val) {

    var upperCase = new RegExp('[A-Z]');
    var lowerCase = new RegExp('[a-z]');
    var numbers = new RegExp('[0-9]');
    var regularExp = '^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,}$';
    val = (val != null )? TrimString(val): val;

    if (val != null && val.length < 6)
        return "Must be 6 characters in length";
    

    if (val.match(upperCase) && val.match(lowerCase) && val.match(numbers)) {
        return null;
    }
    else {
        return "Not Match";
    }
}

function IsUserExistsSuccess(result) {
    var txtUserName = 'body_Adminbody_txtUserName';
    var validationImage = 'validationImage';
    if (result.d == true) {
        var message = 'Users name already exists';
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
        $('#body_Adminbody_txtUserName').focus();
        $('#body_Adminbody_txtUserName').val(_initialUserName);
        ShowHideWarningMessage(txtUserName, validationImage,'', true);

    } else {
        ShowHideWarningMessage(txtUserName, validationImage,'', false);
        if (!$('#' + _userSaveButton).is(":visible")) {
           
            SaveFields(_onBlurParam);
        }
        _initialUserName = $('#body_Adminbody_txtUserName').val();
    }
}

//service Fail
function IsUserExistsFailed() {
    alert('Service failed.');
}


function OnStatusChange() {
    //var ddlUserStatus = 'ddlUserStatus';

    //if ($('#ddlUserStatus').val() == 0) {
    //    ShowHideWarningMessage(ddlUserStatus, 'Please select a Status', true);
    //} else {
    //    ShowHideWarningMessage(ddlUserStatus, 'Please select a Status', false);
    //}
}


function OnClickEnableDisable() {

    var userEvents = $('#body_Adminbody_hiddenIsUserHasEvent').val();

    if (userEvents == 'True') {
        alert('User can not be deleted. It has events associated with this.')
        return false;
    }
    else {
        
        var userResponse = confirm('Are you sure you want to delete this user?');

        if (userResponse) {

            var useractive = $('#body_Adminbody_hiddenIsUserActive').val();
            var userEvents = $('#body_Adminbody_hiddenIsUserHasEvent').val();

            if (useractive == 'True') {
                if (userEvents == 'True') {
                    EnableOrDisableUser(false);
                }
                else {
                    DeleteUser();
                }
            }
            else {
                EnableOrDisableUser(true);
            }
        }  
    }
}


function EnableOrDisableUser(isActivate) {

    var activate = false;
    if (isActivate)
        activate = true;

    var userID = TrimString($('#body_Adminbody_hiddenUserId').val());
    if (userID != null) {

        var userDetails = new EnableOrDisableUserData(userID, activate);

        _data = JSON.stringify(userDetails);
        _serviceUrl = _baseURL + 'Services/UserDetails.svc/EnableOrDisableUser';
        AjaxPost(_serviceUrl, _data, eval(EnableOrDisableUserSuccess), eval(EnableOrDisableUserFailed));
    }
}

function EnableOrDisableUserSuccess(result) {
    var message = '';
    if (result != null && result.d.length > 0)
    {
        message = result.d;
        var messageType = 'jSuccess';
        NotificationMessage(message, messageType, true, 3000);
        RedirectToUserList();
    }
}

function EnableOrDisableUserFailed() {
    alert('Service failed.');
}

//property of Users class
function EnableOrDisableUserData(userID, isActivate) {
    this.userID = userID;
    this.isActivate = isActivate;
}

function DeleteUser() {

    var userID = TrimString($('#body_Adminbody_hiddenUserId').val());
    if (userID != null) {

        var userDetails = new DeleteUserData(userID);

        _data = JSON.stringify(userDetails);
        _serviceUrl = _baseURL + 'Services/UserDetails.svc/DeleteUser';
        AjaxPost(_serviceUrl, _data, eval(DeleteUserSuccess), eval(DeleteUserFailed));
    }
}

function DeleteUserSuccess(result) {
    var message = '';
    if (result != null && result.d.length > 0) {
        message = result.d;
        var messageType = 'jSuccess';
        NotificationMessage(message, messageType, true, 3000);
        RedirectToUserList();
    }
}

function DeleteUserFailed() {
    alert('Service failed.');
}

//property of Users class
function DeleteUserData(userID) {
    this.userID = userID;
}