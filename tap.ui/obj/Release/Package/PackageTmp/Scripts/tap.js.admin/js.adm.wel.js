﻿$(document).ready(function () {
    //Password Policy
    //$('#txtPassword').keyup(function () {

    //    ValidatePassWordPolicy($(this).val());

    //}).focus(function () {

    //    ValidatePassWordPolicy($(this).val());
    //    $('#pswd_info').show();

    //}).blur(function () {

    //    $('#pswd_info').hide();
    //    ValidatePasswords($('#txtPassword').val(), $('#txtConfirmPassword').val());

    //});


    ////Password Policy
    //$('#txtConfirmPassword').keyup(function () {
    //    ValidatePasswords($('#txtPassword').val(), $('#txtConfirmPassword').val());
    //});


    $('#SaveCompanyWithAdmin').attr("disabled", "disabled");

    NextPrevSubSection();

    $('#SaveCompanyWithAdmin').click(function () {
        SaveCompanyWithAdmin();
    });

    // Add Test Active Directory Settings event to button
    $('#btnTestSettings').on('click', 
        function () {
            return OpenTestSettingsPopup();
        }
    );
    $("#btnpCancel").on('click', CloseTestSettingsPopup);
    $("#btnpTest").on('click', TestSettingsPopup);
});



function ValidatePassWordPolicy(pswd) {


    if (pswd.length < $('#spnLength').html()) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }

    //validate upper case letter
    if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
        $('#upperCase').removeClass('invalid').addClass('valid');
    } else {
        $('#upperCase').removeClass('valid').addClass('invalid');
    }

    //validate lower case letter
    if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
        $('#lowerCase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowerCase').removeClass('valid').addClass('invalid');
    }

    //validate number
    var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
    if (pswd.match(numberPattern)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }


    ////validate special character
    //var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
    //if (pswd.match(specialCharPattern)) {
    //    $('#specialCharacter').removeClass('invalid').addClass('valid');
    //} else {
    //    $('#specialCharacter').removeClass('valid').addClass('invalid');
    //}


    //if (pswd.length > $('#spnMaxCharacterCount').html()) {
    //    $('#maxCharacter').removeClass('valid').addClass('invalid');
    //} else {
    //    $('#maxCharacter').removeClass('invalid').addClass('valid');
    //}

}



function NextPrevSubSection() {
    
    $('#divisionNextBtn').click(function () {
        if ($('#divNewDivision').is(':visible')) {
            var divisionname = TrimString($('#companyName').val());
            var email = TrimString($('#companyEmail').val());
            var phone = TrimString($('#companyPhoneNumber').val());
            var website = TrimString($('#companyWebsite').val());
            if (CompanyDetailvalidation(divisionname, email, phone, website)) {
                $('#divNewDivision').hide();
                $('#divCreateUsers').show();
                $('#liCreateDivsion').removeClass('progtrckr-todo');
                $('#liCreateDivsion').addClass('progtrckr-done');
                $('#divisionPreviousBtn').show();
                //$('#toContinueText').html('To continue, click Create');
                $('#toContinueText').html('');
                $('#txtFirstName').focus();
                $('#divisionNextBtn').hide();
                $('#SaveCompanyWithAdmin').show();

                if ($('#chbxActiveDirectory').is(':checked')) {
                    // Hide Taproot s/w div and show AD settings DIV
                    $('#divActiveDirectorySetting').show();
                    $('#divCreateUsers').hide();
                    $('.btnADTestBtn').show();
                    $('#SaveCompanyWithAdmin').attr("disabled", "disabled");
                }
                else {
                    // Show Taproot s/w div and hide AD settings DIV
                    $('#divCreateUsers').show();
                    $('#divActiveDirectorySetting').hide();
                    $('.btnADTestBtn').hide();
                    $('#SaveCompanyWithAdmin').removeAttr("disabled", "disabled");
                }
            }
        }

        return false;
    });

    $('#divisionPreviousBtn').click(function () {
        $('.btnADTestBtn').hide();

        if ($('#divCreateSettings').is(':visible')) {
            $('#divCreateSettings').hide();
            $('#divCreateUsers').show();
            $('#divisionNextBtn').show();
            $('#toContinueText').html('To continue, click Next');
            $('#SaveCompanyWithAdmin').hide();
            return false;
        }
        if ($('#divCreateUsers').is(':visible') || $('#divActiveDirectorySetting').is(':visible')) {
            $('#divCreateUsers').hide();
            $('#divActiveDirectorySetting').hide();
            $('#divNewDivision').show();
            $('#divisionPreviousBtn').hide();
            $('#divisionNextBtn').show();
            $('#toContinueText').html('To continue, click Next');
            $('#SaveCompanyWithAdmin').hide();
            return false;
        }
    }
    );
}

//company details validations
function CompanyDetailvalidation(name, email, phone, website) {
    var primaryErrorMessage = 'You are missing the following field: ';
    var errorMessage = primaryErrorMessage;
    var valid = true;
    errorMessage += ((name == '' || name == null) ? 'Company Name' : '');

    if (name == null || name.length == 0) {
        ShowHideWarningMessage('companyName', 'Please specify company name', true);
        valid = false;
    }
    else {
          ShowHideWarningMessage('companyName', 'Please specify Company name', false);
    }

    if (phone != null && phone.length > 0) {
        if (!validatePhone('companyPhoneNumber')) {
            ShowHideWarningMessage('companyPhoneNumber', 'Please specify a valid phone number', true);
            valid = false;
        }
        else
            ShowHideWarningMessage('companyPhoneNumber', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('companyPhoneNumber', 'Please specify a valid phone number', false);


    if (email != null && email.length > 0) {
        if (validateEmail(email)) {
            ShowHideWarningMessage('companyEmail', 'Please specify a Email', false);
        }
        else {
            ShowHideWarningMessage('companyEmail', 'Please specify a valid Email', true);
            valid = false;
        }
    } else {
        ShowHideWarningMessage('companyEmail', 'Please specify a Email', false);
    }

    if (website != null && website.length > 0) {
        if (!WebsiteValidation('companyWebsite')) {
            ShowHideWarningMessage('companyWebsite', 'Please specify a valid website', true);
            valid = false;
        }
        else
            ShowHideWarningMessage('companyWebsite', 'Please specify a valid website', false);

    }
    else
        ShowHideWarningMessage('companyWebsite', 'Please specify a valid website', false);


    if (errorMessage.length > primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 2000);
        return false;
    }
    if (!valid)
        return false;
    
    return true;
}

//save company details
function SaveCompanyWithAdmin() {
    var parentCompanyId = 0;
    var companyName = TrimString($('#companyName').val());
    var phoneNumber = TrimString($('#companyPhoneNumber').val());
    var website = TrimString($('#companyWebsite').val());
    var address = TrimString($('#companyAddress').val());
    var email = TrimString($('#companyEmail').val());
    var adsecurity = 0;
    //// Get User Details
    var firstName = TrimString($('#txtFirstName').val());
    var lastName = TrimString($('#txtLastName').val());
    var userEmail = TrimString($('#txtEmail').val());
    var userPhone = TrimString($('#txtPhone').val());
    var userName = TrimString($('#txtUserName').val());
    var password = TrimString($('#txtPassword').val());
    var confirmPassword = TrimString($('#txtConfirmPassword').val());
    var userActive = true;
    // Get AD details
    var txtServer = TrimString($('#txtServer').val());
    var txtDomain = TrimString($('#txtDomain').val());
    var txtGroupName = TrimString($('#txtGroupName').val());
    var txtADUserName = TrimString($('#txtpUsername').val());
    var txtADPassword = TrimString($('#txtpPassword').val());
    
    if (!($('#chbxActiveDirectory').is(':checked'))) {
        
        if (CompanyDetailvalidation(companyName, email, phoneNumber, website) && ValidateCompanyUserFields(userName, firstName, lastName, userEmail, password, confirmPassword, userActive, userPhone)
        && ValidatePasswords(password, confirmPassword)) {
            $('#SaveCompanyWithAdmin').hide();
            $('#divisionPreviousBtn').hide()
            $('#orgWorkProgressImage').show();
            var divisionDetails;

            divisionDetails = new CompanySetUpDetailData(parentCompanyId, companyName, phoneNumber, website,
                                address, email, adsecurity, firstName, lastName, userEmail, userPhone, userName, password);
            _data = JSON.stringify(divisionDetails);
            _serviceUrl = _baseURL + 'Services/Divisions.svc/SaveCompanyWithAdmin';
            
            AjaxPost(_serviceUrl, _data, eval(CompanySetUpSuccess), eval(CompanySetUpFail));
        }
    }
    else {
        userName = TrimString($('#txtTaprootAdmin').val());
        if (CompanyDetailvalidation(companyName, email, phoneNumber, website) && userName != null && userName != '') {
            
            $('#SaveCompanyWithAdmin').hide();
            $('#divisionPreviousBtn').hide()
            $('#orgWorkProgressImage').show();
            var divisionDetails;


            divisionDetails = new CompanySetUpDetailData_ActiveDirectory(
                            parentCompanyId, companyName, phoneNumber, website, address, email, adsecurity,
                            userName, //firstName, lastName, userEmail, userPhone, userName, password,
                            txtServer, txtDomain, txtGroupName, txtADUserName, txtADPassword);
            _data = JSON.stringify(divisionDetails);
            _serviceUrl = _baseURL + 'Services/Divisions.svc/SaveCompanyWithActiveDirectoryAdmin';

            AjaxPost(_serviceUrl, _data, eval(CompanySetUpSuccess), eval(CompanySetUpFail), false);
        }
    }

    return false;
}


function ValidatePasswords(Password, cPassword) {
    if ($('#chbxActiveDirectory').is(':checked')) return true;

    _passwordRegularExpression = '^(?=(.*[0-9]){1,})(?=(.*[A-Z]){1,})(?=(.*[a-z]){1,}).{6,}$';
    
    var txtPassword = 'txtPassword';
    var txtConfirmPassword = 'txtConfirmPassword';

    if (Password != null && Password.match(_passwordRegularExpression) != null) {
        if (Password != cPassword) {
            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;
        } else {
            ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }
    else {
        //ShowHideWarningMessage(txtPassword, _passwordPolicyDetails, true);
        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', true);
        return false;
    }

    return true;
}

function CompanySetUpDetailData(parentCompanyId, companyName, phoneNumber, website, address, email, adsecurity,
    firstName, lastName, userEmail, userPhone, userName, password) {
    // Company Profile details
    this.parentCompanyId = parentCompanyId;
    this.companyName = companyName;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.companyEmail = email;
    this.website = website;
    // User Details
    this.firstName = firstName;
    this.lastName = lastName;
    this.userEmail = userEmail;
    this.password = password;
    this.userName = userName;
    this.userPhone = userPhone;
}

function CompanySetUpDetailData_ActiveDirectory(parentCompanyId, companyName, phoneNumber, website, address, email, adsecurity,
    userName, //firstName, lastName, userEmail, userPhone, userName, password,
    txtServer, txtDomain, txtGroupName, txtADUserName, txtADPassword) {
    // Company Profile details
    this.parentCompanyId = parentCompanyId;
    this.companyName = companyName;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.companyEmail = email;
    this.website = website;
    // User Details
    //this.firstName = firstName;
    //this.lastName = lastName;
    //this.userEmail = userEmail;
    //this.password = password;
    this.userName = userName;
    //this.userPhone = userPhone;
    // AD settings
    this.serverName = txtServer;
    this.domainName = txtDomain;
    this.groupName = txtGroupName;
    this.adUserName = txtADUserName;
    this.adPassword = txtADPassword;
}

//success message saving data
function CompanySetUpSuccess(result) {
    
    if (result != null && result.d != null) {
        _message = "Congratulations! Your TapRooT (R) installation is complete. Please log in to begin using the software.";
        _messageType = 'jSuccess';
        NotificationMessage(_message, _messageType, false, 2000);
        if (result.d.indexOf(':') > -1) {

            var array = result.d.split(':');
            _companyId = array[0];
            _userId = array[1];

            //to save the Username in cookie
            $.cookie("userName", TrimString($('#txtUserName').val()), { path: '/' });
            $('#hfUserName').val($('#txtUserName').val());
            $.cookie("FirstName", TrimString($('#txtFirstName').val()), { path: '/' });

            $("#hdnUserId").val(_userId);
            $("#hdnUserName").val(TrimString($('#txtUserName').val()));
            $('#hdnFirstName').val(TrimString($('#txtFirstName').val()));
            $('#hdnCompanyId').val(_companyId);
        }
        
        setTimeout(function () {

            window.location.href = window.location.href.toLowerCase().replace("setup.aspx", "Login.aspx");
            return false;
        }, 4000);
    }
    
}

//fail message
function CompanySetUpFail(error) {
    alert('Service failed.')
}



function ValidateCompanyUserFields(username, firstname, lastname, email, password, confirmpassword, active, phonenumber) {
    if ($('#chbxActiveDirectory').is(':checked')) {
        return ValidateADAuthentication();
    }

    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;
    var isValid = true;

    errorMessage += ((username == '') ? 'User Name,\n' : '');
    errorMessage += ((firstname == '') ? 'First Name,\n' : '');
    errorMessage += ((lastname == '') ? 'Last Name,\n' : '');
    errorMessage += ((email == '') ? 'Email,\n' : '');

    errorMessage += ((password == '') ? 'Password,\n' : '');
    errorMessage += ((confirmpassword == '') ? 'Confirm Password,\n' : '');

    if (errorMessage[errorMessage.length - 2] == ',') {
        errorMessage = errorMessage.substring(0, errorMessage.length - 2);
        errorMessage = errorMessage + '.';
    }

    var txtUserName = 'txtUserName';
    var txtFirstName = 'txtFirstName';
    var txtLastName = 'txtLastName';
    var txtEmail = 'txtEmail';
    var txtPassword = 'txtPassword';
    var txtConfirmPassword = 'txtConfirmPassword';

    var isValidEmail = true;
    var isPasswordMatch = true;

    if (username == null || username.length == 0) {
        ShowHideWarningMessage(txtUserName, 'Please specify User Name', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage(txtUserName, 'Please specify User Name', false);
    }
    if (firstname == null || firstname.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtFirstName, 'Please specify First Name', true);
    }
    else {
        if (!CharsValidation(firstname)) {
            isValid = false;
            ShowHideWarningMessage(txtFirstName, 'Please enter a valid First Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtFirstName, 'Please specify First Name', false);
        }
    }

    if (lastname == null || lastname.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtLastName, 'Please specify Last Name', true);
    }
    else {
        if (!CharsValidation(lastname)) {
            isValid = false;
            ShowHideWarningMessage(txtLastName, 'Please enter a valid Last Name, specify only letters', true);
        } else {
            ShowHideWarningMessage(txtLastName, 'Please specify Last Name', false);
        }
    }

    if (email == null || email.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtEmail, 'Please specify a Email', true);
    }
    else {
        if (validateEmail(email)) {
            ShowHideWarningMessage(txtEmail, 'Please specify a Email', false);
        }
        else {
            ShowHideWarningMessage(txtEmail, 'Please specify a valid Email', true);
            isValidEmail = false;
            isValid = false;
        }
    }


    if (phonenumber != null && phonenumber.length > 0) {
        if (!validatePhone('txtPhone')) {
            isValid = false;
            ShowHideWarningMessage('txtPhone', 'Please specify a valid phone number', true);
        }
        else
            ShowHideWarningMessage('txtPhone', 'Please specify a valid phone number', false);
    }
    else
        ShowHideWarningMessage('txtPhone', 'Please specify a valid phone number', false);


    if (password == null || password.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', true);
    }
    else {
        ShowHideWarningMessage(txtPassword, 'Please specify a Password', false);
    }
    if (confirmpassword == null || confirmpassword.length == 0) {
        isValid = false;
        ShowHideWarningMessage(txtConfirmPassword, 'Please specify a Confirm Password', true);
        //isPasswordMatch = false;
    }
    else {
        ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
    }
    if (errorMessage.length != primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 20000);
        return false;
    }
    if (!isValid)
        return true;

    return true;

}

// Open the Test AD Settings popup
function OpenTestSettingsPopup() {
    // clear the input textboxes
    $("#divActiveDirectoryTestDialog").find('input').val('');

    // If any old refeernces exists, clear then and destroy the popup
    $("#dialog:ui-dialog").dialog("destroy");
    
    // Add dialog to the div
    $("#divActiveDirectoryTestDialog").dialog({
        width: 600,
        modal: true,
        draggable: false,
        resizable: false,
        title: "Test the Active Directory Settings"
    });
    
    // Open the DIV as popup
    $('#divActiveDirectoryTestDialog').show();

    // Return false so that the button will not postback
    return false;
}

// Close the AD Test popup
function CloseTestSettingsPopup() {
    // Clear the values
    //$("#divActiveDirectoryTestDialog").find('input').val('');

    // Close the popup
    $("#divActiveDirectoryTestDialog").dialog("close");
}

// Test the authentication for the provided AD Settings and 
function TestSettingsPopup() {
    // Get the values from the AD settings config section
    var _server = $('#txtServer').val();
    var _domain = $('#txtDomain').val();
    var _groupName = $('#txtGroupName').val();
    var _adminUsername = $('#txtTaprootAdmin').val();
    // Popup values
    var _popUsername = $('#txtpUsername').val();
    var _popPassword = $('#txtpPassword').val();

    var activeDirectoryDetails = new ActiveDirectorySettings(_groupName, _domain, _server, _adminUsername, _popUsername, _popPassword);
    _data = JSON.stringify(activeDirectoryDetails);
    _serviceUrl = _baseURL + 'Services/Users.svc/ConnectActiveDirectory';
    AjaxPost(_serviceUrl, _data, eval(ConnectActiveDirectorySuccess), eval(ConnectActiveDirectoryFail));
    
    return false;
}

function ActiveDirectorySettings(GroupName, DomainName, ServerName, AdminUserName, TapAdminUserName, TapAdminPassword) {
    this.groupName = GroupName;
    this.domainName = DomainName;
    //this.serverProtocol = "LDAP://";
    this.serverName = ServerName;
    this.adminUserName = AdminUserName;
    this.tapAdminUserName = TapAdminUserName;
    this.tapAdminPassword = TapAdminPassword;
}

function ConnectActiveDirectorySuccess(result) {
    var res = result.d.split('~');
    if (res[1] == "0") {
        NotificationMessage(res[0], "jSuccess", false, 2000);
        $("#SaveCompanyWithAdmin").removeAttr("disabled", "disabled");
    }
    else {
        NotificationMessage(res[0], "jError", false, 2000);
        $("#SaveCompanyWithAdmin").attr("disabled", "disabled");
    }

    //CloseTestSettingsPopup();
    return false;
}

function ConnectActiveDirectoryFail(error) {
    NotificationMessage(error.responseHTML, "jError", false, 2000);

    CloseTestSettingsPopup();
    $('#SaveCompanyWithAdmin').attr("disabled", "disabled");
}

function ValidateADAuthentication() {
    var txtServer = $('#txtServer');
    var txtDomain = $('#txtDomain');
    var txtGroupName = $('#txtGroupName');
    var txtADUserName = $('#txtADUserName');
    var txtADPassword = $('#txtADPassword');
    var txtTaprootAdmin = $('#txtTaprootAdmin');
    
    var primaryErrorMessage = 'You are missing the following field(s): ';
    var errorMessage = primaryErrorMessage;
    var isValid = true;

    errorMessage += (($(txtServer).val() == '') ? 'Server,\n' : '');
    errorMessage += (($(txtDomain).val() == '') ? 'Domain,\n' : '');
    errorMessage += (($(txtGroupName).val() == '') ? 'Group Name,\n' : '');
    errorMessage += (($(txtADUserName).val() == '') ? 'User Name,\n' : '');
    errorMessage += (($(txtADPassword).val() == '') ? 'Password,\n' : '');
    errorMessage += (($(txtTaprootAdmin).val() == '') ? 'TapRooT&#174; Admin User,\n' : '');
    

    if (errorMessage[errorMessage.length - 2] == ',') {
        errorMessage = errorMessage.substring(0, errorMessage.length - 2);
        errorMessage = errorMessage + '.';
    }

    // Server Name
    if ($(txtServer).val() == null || $(txtServer).val().length == 0) {
        ShowHideWarningMessage($(txtServer).prop('id'), 'Please specify the Server', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtServer).prop('id'), 'Please specify the Server', false);
    }
    // Domain Name
    if ($(txtDomain).val() == null || $(txtDomain).val().length == 0) {
        ShowHideWarningMessage($(txtDomain).prop('id'), 'Please specify the Domain', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtDomain).prop('id'), 'Please specify the Domain', false);
    }
    // Group Name
    if ($(txtGroupName).val() == null || $(txtGroupName).val().length == 0) {
        ShowHideWarningMessage($(txtGroupName).prop('id'), 'Please specify the Group Name', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtGroupName).prop('id'), 'Please specify the Group Name', false);
    }
    // User Name
    if ($(txtADUserName).val() == null || $(txtADUserName).val().length == 0) {
        ShowHideWarningMessage($(txtADUserName).prop('id'), 'Please specify the User Name', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtADUserName).prop('id'), 'Please specify the User Name', false);
    }
    // Password
    if ($(txtADPassword).val() == null || $(txtADPassword).val().length == 0) {
        ShowHideWarningMessage($(txtADPassword).prop('id'), 'Please specify the Password', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtADPassword).prop('id'), 'Please specify the Password', false);
    }
    // TapRooT&#174; Admin User
    if ($(txtTaprootAdmin).val() == null || $(txtTaprootAdmin).val().length == 0) {
        ShowHideWarningMessage($(txtTaprootAdmin).prop('id'), 'Please specify the TapRooT&#174; Admin User', true);
        isValid = false;
    }
    else {
        ShowHideWarningMessage($(txtTaprootAdmin).prop('id'), 'Please specify the TapRooT&#174; Admin User', false);
    }
    
    // Display error message notification
    if (errorMessage.length != primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 20000);
        return false;
    }
    if (!isValid) return true;

    return true;
}