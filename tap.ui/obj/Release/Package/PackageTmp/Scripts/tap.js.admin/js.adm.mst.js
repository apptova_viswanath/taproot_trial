﻿
//Global Variables
var _configTab = '';
_fieldOrderArray = new Array;
_tabOrderArray = new Array;
var _tabEdit = '';

$(document).ready(function () {

    //added to display the First character of the User as capital
    //UpperCaseUserName();

    //To Navigate the  home page onclick of Home  button
    $("#Home").click(function () {
        window.location.href = _baseURL + 'Home';
    });


});

//Add fields for tab
function AddFields(fieldTabId) {

    var data = '&tabId=' + fieldTabId;

    if (fieldTabId != null && fieldTabId != undefined) {

        var serviceURL = _baseURL + 'Services/Fields.svc/GetFieldListByTabID';
        Ajax(serviceURL, data, eval(AddFieldSuccess), eval(AddFieldFail), 'InsertFieldsControls');
    }
}

//On success of add fields for tab
function AddFieldSuccess(result) {

}

//On fail of add fields for tab
function AddFieldFail(result) {
    alert("Error Message Details:" + result.status + ":" + result.statustext);
}


//Method to Change username as Uppercase
function UpperCaseUserName() {
   
   
    var loginUserName = GetUserName();

    if (loginUserName != '' && loginUserName != null && loginUserName != 'undefined') {

        var displayUserName = ' ' + loginUserName.substring(0, 1).toUpperCase() + loginUserName.substring(1, loginUserName.length);
        //$('#lblUser').html(displayUserName);
        $('#lblUser').html(decodeURIComponent(displayUserName));

    }
}

//Method To  Create Static Config Menus
function CreateAdminConfigMenu() {
    
    var adminConfigData = "";

    _configTab = CreateAdminConfigTabs('Custom Tabs', 'AdminMainTabs', null);
    
    adminConfigData += '<ul id="UlAdminConfigBar">';
    adminConfigData += '<li class="LiTabSelect" id=' + 'CustomTabs' + '>';
    adminConfigData += _configTab;
    adminConfigData += '</li>';
    adminConfigData += '</ul>';

    $("#NavAdminConfig").hide();
}

//Create admin config tabs
function CreateAdminConfigTabs(configTabName, tabName, tabId) {

    var configAnchor = '';

    switch (tabName) {

        case "AdminMainTabs":

            configAnchor = '<a id=lnk' + configTabName.replace(' ', '') + ' > ' + configTabName + '</a>';
            AdminSubTabList(GetCompanyId(), configTabName);
            break;

        case "AdminSubtab":

            configAnchor = '<a class="admin-subtabs" onClick=\'javascript:DisplayCustomPage(' + tabId + ',"' +
                            configTabName + '")\'; return false;  id=lnk' + configTabName + '> ' + configTabName + '</a>';
            break;

        case "AdminSubNewtab":

            configAnchor = '<a class="admin-subtabs" onClick=\'javascript:DisplayCustomPage(' + tabId + ',"' +
                            configTabName + '")\'; return false;  id=lnkAdminNewTab> ' + configTabName + '</a>';
            break;

        case "AdminSubFoldertab":

            configAnchor = '<a class="admin-subtabs" onClick=\'javascript:DisplayCustomPage(' + tabId + ',"' +
                            configTabName + '")\'; return false; id=lnkAdminFolderTab> ' + configTabName + '</a>';
            break;

        case "AdminSubSystemListTab":

            configAnchor = '<a class="admin-subtabs" onClick=\'javascript:DisplayCustomPage(' + tabId + ',"' +
                            configTabName + '")\'; return false; id=lnkAdminSystemListTab> ' + configTabName + '</a>';
            break;
    }

    return configAnchor;
}

//To create the Admin SubTabs
function AdminSubTabList(companyId, configTabName) {

    var adminFolderTabData = "";

    if (configTabName == 'Custom Tabs') {

        data = '&companyId=' + GetCompanyId();
        var serviceURL = _baseURL + 'Services/Tabs.svc/GetCustomTabs';
        Ajax(serviceURL, data, eval(LoadAdminDataList), eval(LoadAdminDataListFail), 'AdminSubTabList');

    }
    else if (configTabName == 'System Lists') {

        window.location.href = _baseURL + 'Admin/Configuration/System-Lists';

    }
    else if (configTabName == 'Site Settings') {

        _configTab = CreateAdminConfigTabs('Attachment Folders Setup', 'AdminSubFoldertab', -1)
        
        adminFolderTabData += '<li class="FloatLeft">';
        adminFolderTabData += _configTab;
        adminFolderTabData += '<li>';

        $("#AdminFolderTabs").html(adminFolderTabData);
        $("#AdminFolderTabs").show();
        $("#AdminSubTabs").hide();
        $("#AdminNewTab").hide();
        $("#AdminSystemList").hide();
    }
    else if (configTabName == 'Email Setup') {

        $("#AdminSubTabs").hide();
        $("#AdminNewTab").hide();
        $("#AdminFolderTabs").hide();
        $("#AdminSystemList").hide();

    }
}

//Load admin data list
function LoadAdminDataList(result) {

    var jsonResult = '';
    var adminSubTabCount = 0;
    var adminCustomData = "";
    var adminNewTabData = "";

    if (result != null) {

        jsonResult = JSON.parse(result.d);

        for (var i = 0; i < jsonResult.length; i++) {

            adminSubTabCount++;

            adminCustomData += '<li style="float:left;" id="_' + (i + 1) + '">';
            _returnConfigTab = CreateAdminConfigTabs(jsonResult[i]["TabName"], 'AdminSubtab', jsonResult[i]["TabID"]);
            adminCustomData += _returnConfigTab;
            adminCustomData += '</li>';

        }

        //Build new tab
        _returnConfigTab = CreateAdminConfigTabs('New Tab', 'AdminSubNewtab', 0);
        adminNewTabData += '<li class="FloatLeft">';
        adminNewTabData += _returnConfigTab;
        adminNewTabData += '<li>';

        $("#AdminSubTabs").html(adminCustomData);
        $("#AdminNewTab").html(adminNewTabData);
        $("#AdminSubTabs").show();
        $("#AdminNewTab").show();
        $("#AdminFolderTabs").hide();
    }
}

//Redirect to respected pages
function DisplayCustomPage(tabId, configName) {

    //debugger;
    var pattern = new RegExp(' ');
    
    if (pattern.test(configName)) {
        configName = configName.replace(/ /g, '-');
    }
    if (tabId == 0) {
        window.location.href = _baseURL + 'Admin/Configuration/Custom-Tabs' + '/' + configName + '-' + tabId;
    }
    else if (tabId == -1) {
        window.location.href = _baseURL + 'Admin/Configuration/Site-Settings/Attachment-Folders-Setup';
    }
    else if (tabId == -2) {
        // window.location.href = _baseURL + 'Admin/Configuration/SystemLists';//will be used later
    }
    else {
        window.location.href = _baseURL + 'Admin/Configuration/Custom-Tabs' + '/' + configName + '-' + tabId;
    }
}

//On Fail of loading datalist
function LoadAdminDataListFail(result) {
    alert(result);
}
