﻿$(document).ready(function () {     
  
    _companyId = GetCompanyId();

    GetEmailSettings();

    $('#btnSave').click(function () {
        //SaveEmailSettings();
        //debugger;
        var Status = ValidateEmailFormat('txtEmailAddress');
        var password = TrimString($('#txtPassword').val());
        var confirmPassword = TrimString($('#txtConfirmPassword').val());
        if (password == confirmPassword) {
            if (Status)
                SaveEmailSettings();
            else {
                var errorMessageType = 'jError';
                var errorMessage = 'Invalid Email Address Format';
                NotificationMessage(errorMessage, errorMessageType, true, 3000);
            }
        }else
            var errorMessageType = 'jError';
        var errorMessage = 'Password and Confirm Password Should be same';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    });

    $('#btnTest').click(function () {
        var Status = ValidateEmailFormat('txtEmailAddress');
        var password = TrimString($('#txtPassword').val());
        var confirmPassword = TrimString($('#txtConfirmPassword').val());
        if (password == confirmPassword) {
            if (Status)
                DisplayEmailTestDialog();
            else {
                var errorMessageType = 'jError';
                var errorMessage = 'Invalid Email Address Format';
                NotificationMessage(errorMessage, errorMessageType, true, 3000);
            }
        }
    });

    $('#btnSend').click(function () {

       
        //SendTestEmail();
        var Status = ValidateEmailFormat('txtSendEmailTo');
        if (Status)
            SendTestEmail();
        else {
            var errorMessageType = 'jError';
            var errorMessage = 'Invalid Email Address Format';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
    });

});


function GetEmailSettings() {

    _serviceUrl = _baseURL + 'Services/EmailSettings.svc/GetEmailSettings';
    AjaxPost(_serviceUrl, null, eval(GetEmailSettingsSuccess), eval(DataFail));

}


function ValidateEmailFormat(x) {

    var email = document.getElementById(x).value;
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        //alert("Not a valid e-mail address");
        return false;
    }

    else
        return true
}





function DisplayEmailTestDialog() {

    //Pop up divTestEmail
    $("#divTestEmail").dialog({

        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

}


function CloseEmailTestSettings() {


    $('#txtSendEmailTo').val('');

    $('#divTestEmail').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divTestEmail").dialog("destroy");
    $('#divTestEmail').bind("dialogclose", function (event, ui) {

    });


}


function SendTestEmail() {

    $('#imgLoader').show();

     //Call service method to send email

    var emailData = {};
    emailData.emailTo = $('#txtSendEmailTo').val();

    _data = JSON.stringify(emailData);
    _serviceUrl = _baseURL + 'Services/EmailSettings.svc/TestEmailSettings';
    AjaxPost(_serviceUrl, _data, eval(TestEmailSettingsSuccess), eval(TestEmailSettingsFail));

}

function TestEmailSettingsSuccess(result) {
        
    $('#imgLoader').hide();
    var errorMessageType = 'jSuccess';
    var errorMessage = '';
    if (result != null && result.d != null) {

        if (result.d.indexOf(':') > 0) {
            errorMessageType = 'jError';
            var arrayMsg = result.d.split(':');
            errorMessage = arrayMsg[1];
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
        else {

            errorMessage = result.d;
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
    }

    CloseEmailTestSettings();
}


function TestEmailSettingsFail(data) {

    var errorMessageType = 'jError';
    var errorMessage = '';
    if (result != null && result.d != null) {

        var arrayMsg = result.d.split(':');
        errorMessage = arrayMsg[1];
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    
    }


}
function GetEmailSettingsSuccess(data) {
  
    var emailData = ParseToJSON(data.d);

    $('#txtEmailAddress').val(emailData.EmailFrom);
    $('#txtPassword').val(emailData.EmailPassword);
    $('#txtConfirmPassword').val(emailData.EmailPassword);
    $('#txtServer').val(emailData.HostAddress);
    $('#txtPort').val(emailData.PortNumber);

}


function SaveEmailSettings() {
    var emailServer = TrimString($('#txtServer').val());
    var emailFrom = TrimString($('#txtEmailAddress').val());
    var password = TrimString($('#txtPassword').val());
    var confirmPassword = TrimString($('#txtConfirmPassword').val());
    var portNumber = TrimString($('#txtPort').val());

    if (ValidateEmailDetails(emailServer, emailFrom, password, confirmPassword,portNumber)) {
        var emailDetails = GetEmailDetailsFromPage();
        _data = JSON.stringify(emailDetails);
        _serviceUrl = _baseURL + 'Services/EmailSettings.svc/SaveEmailSettings';
        AjaxPost(_serviceUrl, _data, eval(SaveEmailSettingsSuccess), eval(DataFail));
    }
}
var _message = '';
var _messageType = '';

function ValidateEmailDetails(emailServer, emailFrom, password, confirmPassword, portNumber)
{    
    var errorMessage = '';

    var primaryErrorMessage = 'You are missing the following fields: ';
     errorMessage = primaryErrorMessage;

     errorMessage += ((emailServer == '') ? 'Email Server,\n' : '');
     errorMessage += ((emailFrom == '') ? 'Email From,\n' : '');
     errorMessage += ((password == '') ? 'password,\n' : '');
     errorMessage += ((confirmPassword == '') ? 'Confirm Password,\n' : '');
     errorMessage += ((portNumber == '') ? 'Port Number,\n' : '');

     if (errorMessage.length > primaryErrorMessage.length ) {

         errorMessage = errorMessage.replace(/,(\s+)?$/, '');
         _message = errorMessage;
         _messageType = 'jError';
         NotificationMessage(_message, _messageType, true, 3000);
         return false;
     }
     errorMessage = '';   
       
        if (!validateEmail(emailFrom)) {
            errorMessage = 'Please specify a valid Email.';

       }   
        else if (!ValidatePort('txtPort')) {
            errorMessage = 'Please specify a valid port number. Field must be numeric.';
          
       } 
   
    if (errorMessage != '')
    {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    }

  
    

    return true;
}


function ValidatePort(field) {

    var a = $('#' + field).val();
    var value = a.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    var intRegex = /^\d+$/;
    if (!intRegex.test(value)) {
        //ShowHideWarningMessage(field, 'Please specify a valid port number. Field must be numeric.', false);
        return false;
    }
    else {
        return true;
    }

}


//Get all the data from the email pop up page
function GetEmailDetailsFromPage() {

    var emailData = {};
    emailData.EmailFrom =  $('#txtEmailAddress').val();
    emailData.EmailPassword =  $('#txtPassword').val();
    emailData.HostAddress =  $('#txtServer').val();
    emailData.PortNumber = $('#txtPort').val();

    return emailData;
}

function SaveEmailSettingsSuccess(data)
{

    if (data)
    {

        NotificationMessage('Email settings saved successfully.', 'jSuccess', false, 1000);
    }
    else {

        NotificationMessage('Error occured.', 'jError', false, 1000);
    }
}

function DataFail()
{

}