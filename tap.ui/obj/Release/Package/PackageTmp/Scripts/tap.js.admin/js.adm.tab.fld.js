﻿//A JS CLASS USED BY THE ADMIN > CUTOM TAB PAGE > ADD/EDIT FIELD POP UP

//Global variables
var _listValue = '';
var _editFieldId = '';
var _dataTypeValue = '';
var _maxLength = 0;
var _sortOrder = '';

//Bind events
$(document).ready(function () {

    LoadDataLists('ddDataList');

    $("#ddDataType").bind("change", function () { GetDataType(); });
    $("#ddDataList").bind("change", function () { GetAddNewListType(); });
    $("#ddControlType").bind("change", function () { MaxLengthShow(); });

    //Validate number
    $("#txtMaxLen").keydown(function (event) { ValidateNumber(event); });

    //Default enter Button Click
    $('#txtFldName,#ddDataType,#ddControlType,#ddDataList,#txtMaxLen,#chkIsRequired').bind('keypress', function (e) {
        DefaultButtonClick(e, 'btnSave');
    });

    $('#txtTabName,#chkShowAllTab,#ddlModule,#chkActive').bind('keypress', function (e) {
        DefaultButtonClick(e, 'btnSaveTab');
    });

    $('#txtListName,#txtDescription').bind('keypress', function (e) {
        DefaultButtonClick(e, 'btnNewTreeItem');
    });

    //save the dynamic fields
    $('#btnSave').click(function () {
        InsertCustomField();
    });

    //Delete Dynamic Fields
    $('#btnDelete').click(function () {
        DeleteField();
    });

    //close populate the Fields in parent page
    $('#btnCancel').click(function () {

        ClosePopupAndRefreshPage();
    });

    $('#divAddEditFieldsDialog').hide();
    $('#chkIsActive').attr('checked', true);

    CustomTabsSorting();
    CustomFieldsSorting();

    //Add cursor for draggable custom tabs
    $('#sortorder div a').css('cursor', 'move');
});

function CustomTabsSorting() {
    $("#sortorder").sortable({
        //axis: "x",
        update: function (e, ui) {
            var tabs = "";
            var n = 0;
            $("#sortorder > div > a").each(function (i) {
                tabs += (tabs == "" ? "" : ",") + n + "-" + this.id.replace('lnk', '');
                n = n + 1;
            });
            var moduleid = $('#sortorder').attr('moduleid');

            SaveCustomTabsOrder(moduleid, tabs);

        }
    });
}

function CustomFieldsSorting() {
    $("#FieldInfo").sortable({
        axis: "y",
        update: function (e, ui) {
            var tabs = "";
            var n = 0;
            var sortOrder = [];
            var FieldIds = [];
            $("#FieldInfo > li").each(function (i) {
                sortOrder[i] = n;
                FieldIds[i] = this.id.replace('Field_', '');
                n = n + 1;
            });
            FieldSortUpdate(FieldIds, sortOrder);
        }
    });
}

//Load adat list values in teh drop down
function LoadDataLists(ddDataList) {

    var companyId = GetCompanyId();
    data = '&companyId=' + companyId;
    var serviceURL = _baseURL + 'Services/Fields.svc/DataListSelect';
    Ajax(serviceURL, data, eval(LoadDataList), eval(FailDataList), 'ddDataList');
}


//On Fail of loading datalist
function FailDataList(result) {
    alert(result);
}


//On Success of loading datalist
function LoadDataList(result, ddDataList) {
   
    $('#ddDataList').empty();
    if (result != null) {

        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        $('#ddDataList').append($('<option></option>').val('0').html('<< Select >>'));

        for (var i = 0; i < jsonResponse.length; i++) {
            $("#" + ddDataList).append($("<option></option>").attr("value", jsonResponse[i]["ListID"]).attr("title", jsonResponse[i]["Description"]).text(jsonResponse[i]["ListName"]));
        }

        if (_listValue != '') {
            $('#ddDataList').val(_listValue);
        }
    }
}

//Change the status
function GetAddNewListType() {

    $('#FieldErrorMsg').html('');
    _listValue = $("#ddDataList").val();

}

function AddOptionsDataType() {

    $("#ddControlType").html('');
    $("#ddControlType").append('<option value="Select">Select</option>');
    $("#ddControlType").append('<option value="TextBox">Text Box</option>');
    $("#ddControlType").append('<option value="ParagraphText">Paragraph Text</option>');
    $("#ddControlType").append('<option value="DateField">Date Field</option>');
    $("#ddControlType").append('<option value="RadioButton">Radio Button</option>');
    $("#ddControlType").append('<option value="SelectOneFromList">Select one from list</option>');
    $("#ddControlType").append('<option value="SelectMultipleFromList">Select multiple from list</option>');
}

function RemoveOptionsDataType() {
    $("#ddControlType").html('');
    $("#ddControlType").append('<option value="Select">Select</option>');
}

//Get data type for fields
function GetDataType() {

    //validate for Select
    $('#FieldErrorMsg').html('');

    var dataTypeValue = $("#ddDataType option:selected").text();


    $('#ddControlType>option:eq(0)').attr('selected', true);

    $('#MaxLength').hide();
    $('#DatalistDisplay').hide();
    $('#ControlType').show();
    $('#txtMaxLen').val('');

    if (dataTypeValue == 'Select') {

        $('#ddControlType>option:eq(0)').attr('selected', true);
        $('#DatalistDisplay').hide();

    }
    else if (dataTypeValue == 'Text') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="TextBox">Text Box</option>');
        $("#ddControlType").append('<option value="ParagraphText">Paragraph Text</option>');
        $('#MaxLength').show();

    }
    else if (dataTypeValue == 'Number') {

        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="TextBox">Text Box</option>');
        //$('#MaxLength').show();
    }
    else if (dataTypeValue == 'Yes/No') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="RadioButton">Radio Button</option>');

    }
    else if(dataTypeValue=='Label')
    {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="Label">Label</option>');
    }
    else if (dataTypeValue == 'Date Time' || dataTypeValue == 'Date') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="DateField">Date Field</option>');

    }
    else if (dataTypeValue == 'Choose from a list') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="SelectOneFromList">Select one from list</option>');
        $("#ddControlType").append('<option value="SelectMultipleFromList">Select multiple from list</option>');
        $('#DatalistDisplay').show();

    }
}


//Display Max length
function MaxLengthShow() {

    $('#FieldErrorMsg').html('');
    $('#ddDataList>option:eq(0)').attr('selected', true);

    var ddControlTypeValue = $("#ddControlType option:selected").text();

    if (ddControlTypeValue == 'TextBox' || ddControlTypeValue == 'ParagraphText') {
        $('#MaxLength').show();
    }
}


//Get fields data
function GetFieldsData(fieldTabId, displayName, dataTypeValue, controlTypeValue, maxLength, isRequired, dataListValue, isActive, description) {

    var fieldData = {};

    //for insert
    if (_editFieldId == '') {

        fieldData.fieldId = -1;
        fieldData.sortOrder = _sortOrder + 1;
    }
    else {
        fieldData.fieldId = _editFieldId;  //for update
        fieldData.sortOrder = _editSortOrder;
    }

    fieldData.tabId = fieldTabId;
    fieldData.displayName = displayName;
    fieldData.dataType = dataTypeValue;
    fieldData.controlType = controlTypeValue;

    if (maxLength == '' || maxLength == null)
        fieldData.maxLength = 0;
    else
        fieldData.maxLength = maxLength;

    fieldData.isRequired = isRequired;
    fieldData.listId = dataListValue;
    fieldData.isActive = isActive;
    fieldData.description = description;
    fieldData.userId = $('#hdnUserId').val();

    return fieldData;
}


//Clear data from form
function ClearData() {

    $('#txtFldName').val('');
    $('#ddDataType>option:eq(0)').attr('selected', true);
    $('#ddControlType>option:eq(0)').attr('selected', true);
    $('#ddDataList>option:eq(0)').attr('selected', true);
    $('#chkIsRequired').attr('checked', false);
    $('#txtMaxLen').val('');
    $('#DatalistDisplay').hide();
    $('#MaxLength').hide();

}


//Change the status
function GetAddNewListType() {
    ListVal = $("#ddDataList").val();
}


//Validate number field
function ValidateNumber(event) {

    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
        return;
    }

    else {

        if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
            event.preventDefault();
        }

    }
}


//Get system list data
function GetSystemListData(systemListId, fieldDisplayName) {

    var SystemList = {};

    SystemList.listId = systemListId;
    SystemList.displayName = fieldDisplayName;

    return SystemList;
}


//Reactivate field data
function ReactivateFieldData(fieldId) {

    _editFieldId = fieldId;

    _serviceURL = _baseURL + 'Services/Fields.svc/GetFieldByFieldID';
    _data = '&fieldId=' + fieldId;
    Ajax(_serviceURL, _data, eval(ReactivateField), eval(OpenAddEdidPopupFail), 'ReactivateFieldData');

}


// On success of Reactivate field service method
function ReactivateField(result) {

    var jsonResponse = ParseToJSON(result.d);

    //Confirirmation for reactivating the field.
    if (Confirmation('Are you sure you want to reactivate ' + '"' + jsonResponse[0]["DisplayName"] + '" field?')) {

        _editSortOrder = jsonResponse[0]["SortOrder"];
        _dataTypeValue = jsonResponse[0]["Datatype"];
        _maxLength = jsonResponse[0]["MaxLength"];

        var displayName = jsonResponse[0]["DisplayName"];
        var controlTypeValue = jsonResponse[0]["ControlType"];
        var isRequired = jsonResponse[0]["IsRequired"];
        var isActive = true;
        var fieldTabId = jsonResponse[0]["TabID"];
        var dataListvalue = 0;
        var description = jsonResponse[0]["Description"];

        var fieldData = GetFieldsData(fieldTabId, displayName, _dataTypeValue, controlTypeValue, _maxLength, isRequired, dataListvalue, isActive, description);
        var data = JSON.stringify(fieldData);

        _serviceURL = _baseURL + 'Services/Fields.svc/SaveField';

        AjaxPost(_serviceURL, data, eval(CustomFieldReactiveteSuccess), eval(CustomFieldReactiveteFail));

    }
}


//Service Failed method 
function OpenAddEdidPopupFail() {

}


//On success of Reactivate of fields
function CustomFieldReactiveteSuccess(result) {

    _editFieldId = '';

    AddCustomFields();
    //Temporary solution
    var logDetails = 'Field reactivated successfully.';
    //for activity log
    SaveActivityLog(GetCompanyId(), 0, GetUserId(), 1, logDetails);

    ClearCustomData();
}


//On fail of the field reactivate
function CustomFieldReactiveteFail(result) {
    alert('Fail');
}


//Open add/edit field pop up
function OpenAddEditFieldPopup(fieldId, displayName) {
    
    $('#customTabButton').removeClass('PaddingRight120');
    $('#customTabButton').addClass('PaddingRight80');

    $('#ddControlType>option:eq(0)').attr('selected', true);

    $('#MaxLength').hide();
    $('#DatalistDisplay').hide();
    $('#ControlType').show();
    $('#txtMaxLen').val('');


    //Clear the custom fields. 
    ClearCustomData();

    $("#divHeader").html(displayName);
    $('#ui-dialog-title-divAddEditFieldsDialog').html(displayName);

    //show the delete button   
    $('#btnDelete').show();

    $('#FieldErrorMsg').html('');

    _editFieldId = fieldId;
    _serviceURL = _baseURL + 'Services/Fields.svc/GetFieldByFieldID';
    _data = '&fieldId=' + fieldId;

    Ajax(_serviceURL, _data, eval(AddEdidFieldSuccess), eval(AddEdidFieldFail), 'OpenAddEditFieldPopup');



}

//open Add/Edit Fields popup dialog
function OpenCustomAddEditFieldDialog()
{
    $("#dialog:ui-dialog").dialog("destroy");

    $("#divAddEditFieldsDialog").dialog({
        width: 400,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 200],
        close: function (ev, ui) { ClosePopupAndRefreshPage(); }
    });

   
}


//On success of add/edit field pop up
function AddEdidFieldSuccess(result) {
   
    var jsonResponse = ParseToJSON(result.d);
    if (jsonResponse != null) {

        $('#btnSave')[0].parentElement.className = 'buttonEdit';
        $('#btnDelete')[0].parentElement.className = 'buttonDelete';
        $('#btnCancel')[0].parentElement.className = 'buttonCancel';

        $('#btnSave').val('Apply Changes');
    }
    else {
        $('#btnSave')[0].parentElement.className = 'buttonCenter';
        $('#btnDelete')[0].parentElement.className = 'buttonCenter';
        $('#btnCancel')[0].parentElement.className = 'buttonCenter';

        $('#btnSave').val('Apply Changes');
    }

    _editSortOrder = jsonResponse[0]["SortOrder"];

    $('#txtFldName').val(jsonResponse[0].DisplayName);
    $('#txtDescription').val(jsonResponse[0]["Description"]);

    //Set Data type drop down
    SetDataType(jsonResponse);

    //Set control type drop down
    SetControlType(jsonResponse);
   
    if (jsonResponse[0].MaxLength != "") {

        $('#txtMaxLen').val(jsonResponse[0].MaxLength);
    }
    
    if (jsonResponse[0].IsRequired) {
        $('#chkIsRequired').attr('checked', true);
    }
    else {
        $('#chkIsRequired').attr('checked', false);
    }
    
    if (jsonResponse[0].IsActive) {
        $('#chkIsActive').attr('checked', true);
    }
    else {
        $('#chkIsActive').attr('checked', false);
    }

    //open Add/Edit Fields popup dialog
    OpenCustomAddEditFieldDialog();
    
}


//Set data type
function SetDataType(jsonResponse) {

    if (jsonResponse[0].Datatype == 'String' && jsonResponse[0].ListName == null)
        $('#ddDataType').attr('value', 'Text');

    else if (jsonResponse[0].Datatype == 'Int')
        $('#ddDataType').attr('value', 'Number');

    else if (jsonResponse[0].Datatype == 'DateTime' || jsonResponse[0].Datatype == 'Date')
        $('#ddDataType').attr('value', 'DateTime');

    else if (jsonResponse[0].Datatype == 'Boolean')
        $('#ddDataType').attr('value', 'Yes/No');

    else if (jsonResponse[0].Datatype == 'Choose from a list')
        $('#ddDataType').attr('value', 'ChooseFromAList');
    
    else if (jsonResponse[0].Datatype == 'Label')
        $('#ddDataType').attr('value', 'Label');

    if (jsonResponse[0].Datatype == 'String' && jsonResponse[0].ListName != null) {

        $('#ddDataType').attr('value', 'ChooseFromAList');
        $('#DatalistDisplay').show();
        $('#ddDataList').attr('value', jsonResponse[0].ListID);

    }
    else {
        $('#DatalistDisplay').hide();

    }
}


//Set control type
function SetControlType(jsonResponse) {

    if (jsonResponse[0].ControlType == 'Text Box') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="TextBox">Text Box</option>');
        $("#ddControlType").append('<option value="ParagraphText">Paragraph Text</option>');
        $('#ddControlType').attr('value', 'TextBox');
        $('#MaxLength').show();
        if (jsonResponse[0].Datatype != 'Int')
            $('#MaxLength').show();
    }
    else if (jsonResponse[0].ControlType == 'Date Field' || jsonResponse[0].ControlType == 'DateTime') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="DateField">Date Field</option>');
        $('#ddControlType').attr('value', 'DateField');
    }
    else if (jsonResponse[0].ControlType == 'Paragraph Text') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="TextBox">Text Box</option>');
        $("#ddControlType").append('<option value="ParagraphText">Paragraph Text</option>');
        $('#ddControlType').attr('value', 'ParagraphText');
        $('#MaxLength').show();
    }
    else if (jsonResponse[0].ControlType == 'Radio Button') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="RadioButton">Radio Button</option>');
        $('#ddControlType').attr('value', 'RadioButton');
    }
    else if (jsonResponse[0].ControlType == 'Label') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="Label">Label</option>');
        $('#ddControlType').attr('value', 'Label');
    }
    else if (jsonResponse[0].ControlType == 'Select one from list') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="SelectOneFromList">Select one from list</option>');
        $("#ddControlType").append('<option value="SelectMultipleFromList">Select multiple from list</option>');
        $('#ddControlType').attr('value', 'SelectOneFromList');
    }
    else if (jsonResponse[0].ControlType == 'Select multiple from list') {
        RemoveOptionsDataType();
        $("#ddControlType").append('<option value="SelectOneFromList">Select one from list</option>');
        $("#ddControlType").append('<option value="SelectMultipleFromList">Select multiple from list</option>');
        $('#ddControlType').attr('value', 'SelectMultipleFromList');
    }



}


//On fail of add edit field
function AddEdidFieldFail() {
    alert('Error in Populating _data.');
}


//open pop in page for Field tree view
function OpenFieldTreeviewPopup(systemListId, displayName) {

    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCustomTreeviewDialog").dialog("destroy");
    $('#FieldTreeviewList').jstree('destroy').empty();
    $("#divCustomTreeviewDialog").attr('title', displayName);    

    var loadTreeviewData = TreeViewDetail(systemListId, "0");

    _data = JSON.stringify(loadTreeviewData);
    _serviceURL = _baseURL + 'Services/ListValues.svc/BuildJSTreeView'; //Modified for new Js treeview

    AjaxPost(_serviceURL, _data, eval(LoadFieldTreeListsuccess), eval(LoadFieldTreeListfail));
}

//open Custom Tree view dialog
function OpenCustomTreeviewDialog()
{
    $("#divCustomTreeviewDialog").dialog({

        minHeight: 200,
        maxheight: 400,
        width: 380,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

}



//Get tree view detail
function TreeViewDetail(listId, fullPath) {

    var treeViewDetail = {};

    treeViewDetail.listId = listId;
    treeViewDetail.fullPath = fullPath;

    return treeViewDetail;
}


//On suvccess of load field tree list
function LoadFieldTreeListsuccess(result) {
 
    var jsonData = result.d;
    var jsonResponse = jQuery.parseJSON(jsonData);

    DisplayFieldTreeView(jsonResponse);

    //open Custom Tree view dialog
    OpenCustomTreeviewDialog();
}


//Display Tree
function DisplayFieldTreeView(jsonResponse) {

    if (jsonResponse != null) {
        $("#FieldTreeviewList").jstree({
            "json_data": { "data": jsonResponse },
            rules: { multiple: "ctrl" },
            checkbox: {
                two_state: true
            },
            plugins: ["themes", "json_data", "dnd", "crrm", "ui", "types"]
        });

        $('#FieldTreeviewList').bind('loaded.jstree', function (e, data) {
            $('#FieldTreeviewList').children('ul').children('li').children('ins').first().css('background-position', ' -18px 0');
        });

        ExpandTreeNode();
    }

}


//For opening of all nodes
function ExpandTreeNode() {

    $('#FieldTreeviewList').bind("loaded.jstree", function (event, data) {
        $('#FieldTreeviewList').jstree("open_all");
    });

}


//On fail of load tree list
function LoadFieldTreeListfail() {
    alert('Error in Loading Field tree view.');
}


//Close the Pop up and refresh page
function ClosePopupAndRefreshPage() {

    $('#FieldErrorMsg').html('');
    CloseAddEditFieldPopup();
    AddCustomFields();
    ClearCustomData();

}