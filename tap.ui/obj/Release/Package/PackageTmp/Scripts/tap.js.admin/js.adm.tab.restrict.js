﻿
var _searchRestrictUser = '';

//Bind events
$(document).ready(function () {

    Load();

});

function Load() {

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');

    $('#ancRestrictUser').click(function () {
        OpenRestrictUserPopup();
        return false;
    });

    $('#closeRestrictUserPopUp').click(function () {
        _isDirty = false;
        CloseRestrictUserPopup();
    });

    //user search option 
    $("#txtSearchRestrictUser").keyup(function () {
        RestrictUserSearch();
    });

    //show/hide restrict link on based company
    ShowHideRestrictLnk();
    var splitVal = '';

    if (splitUrl[splitUrl.length - 1].indexOf('-')) {
        splitVal = splitUrl[splitUrl.length - 1].split('-');
        if (splitVal != null && splitVal[0] != null && splitVal[0].toLowerCase() == 'details')//(splitVal[0].toLowerCase() == 'details' || splitVal[1] == 0)
            $('#divRestrictAccess').hide();
        if (splitVal != null && splitVal[0] != null && splitVal[1] == 0)
            $('#ancRestrictUser').hide();
    }

    $('#chkRestrictedAccess').change(function () {

        if ($(this).is(':checked') && splitVal != null && splitVal[0] != null && splitVal[1] != 0)
            $("#ancRestrictUser").show();
        else
            $("#ancRestrictUser").hide();
    });

    //bind grid
    BindRestrictUserListGrid();
}

function ShowHideRestrictLnk() {
    _isSUCompany = $('#hdnSUCompany').val();
    var isSUDivision = $('#hdnSUDivision').val();
    if ((_isSUCompany != null && _isSUCompany == "1") || (isSUDivision != null && isSUDivision == "1"))
        $('#divRestrictAccess').hide();
}

//Restrict User List Grid Binding
function BindRestrictUserListGrid() {

    jQuery("#RestrictUserListGrid").jqGrid({

        datatype: function (parameterData) {
            RestrictUserList(parameterData);
        },
        colNames: ['UserId', 'First Name', 'Last Name', 'hasAccess'

        ],
        colModel: [

            { name: 'UserID', index: 'UserID', width: 200, align: 'left', resizable: false, editable: false, hidden: true },
            { name: 'FirstName', index: 'FirstName', width: 100, align: 'left', editable: false },
            { name: 'LastName', index: 'LastName', width: 100, align: 'center', editable: false },
            { name: 'hasAccess', index: 'hasAccess', width: 50, align: 'center', resizable: false, formatter: restrictChkFormatter },

        ],
        gridview: false,
        rowattr: function (rd) {
        },
        toppager: false,
        emptyrecords: 'There are no Users.',
        // Grid total width and height        
        autowidth: true,

        rowNum: 10,
        hidegrid: false,
        //rowList: [10, 20, 50, 100],
        pginput: "false",
        height: '100%',
        caption: 'Users',
        pager: jQuery('#RestrictUserListGridNavigation'),
        viewrecords: true,
        sortname: 'FirstName',
        sortorder: "asc",
        cmTemplate: { title: false },
    });

    $(window).bind('resize', function () {
        $("#RestrictUserListGrid").setGridWidth($('.RestrictUserListGrid').width());
    }).trigger('resize');

} //grid end


function restrictChkFormatter(cellValue, options, rowObject) {
    if (cellValue == 'False') {
        return "<input type='checkbox' class='chkTeamUserDisplayOnReport' onclick=\"UpdateRestrictUser('" + options.rowId + "' , " + options.pos + ", this, event);\"/>";
    }

    if (cellValue == 'True') {
        return "<input type='checkbox' class='chkTeamUserDisplayOnReport' onclick=\"UpdateRestrictUser('" + options.rowId + "' , " + options.pos + ", this, event);\" checked='checked'/>";
    }
}

function UpdateRestrictUser(rowId, ind, sender, event) {
    var userId = rowId;
    var isRestrited = false;
    var val = $(sender).attr('checked');

    if (val != null && val != 'undefined')
        isRestrited = true;

    if (userId > 0)
        UpdateRestrictedUser(userId, isRestrited);
   
}

//to get Restrict User List
function RestrictUserList(parameterData) {

    var companyId = _companyId;
    var userListData = new Object();
    userListData.page = parameterData.page;
    userListData.rows = parameterData.rows;
    userListData.SearchFilter = _searchRestrictUser;
    userListData.IgnoreCase = 0;
    userListData.sortIndex = parameterData.sidx;
    userListData.sortOrder = parameterData.sord;
    userListData.companyID = companyId;
    userListData.tabId = _customTabId;

    data = JSON.stringify(userListData);
    _serviceUrl = _baseURL + 'Services/RestrictTab.svc/RestrictUsersList';
    AjaxPost(_serviceUrl, data, eval(RestrictUserListSuccess), eval(RestrictUserListFail));
}

// Restrict User List Success
function RestrictUserListSuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jError';
    if (result.d != null) {
        var gridData = jQuery("#RestrictUserListGrid")[0];
        gridData.addJSONData(JSON.parse(result.d));
    }
}

//service fail
function RestrictUserListFail() {
    alert('Service failed.');
}


//Open Pop up page for Restrict User
function OpenRestrictUserPopup() {

    var pageTitle = 'Access Users';
    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divRestrictUserListPopUp").dialog({
        title: pageTitle,
        height: 'auto',
        width: 650,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

    _searchRestrictUser = '';
    $('#txtSearchRestrictUser').val('');
    BindRestrictUserListGrid();
    ReloadGridData();
}

//close Add Restrict User Popup page.
function CloseRestrictUserPopup() {

    $('#divRestrictUserListPopUp').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divRestrictUserListPopUp").dialog("destroy");
    $('#divRestrictUserListPopUp').bind("dialogclose", function (event, ui) {

    });
}

function RestrictUserSearch() {
    _searchRestrictUser = $('#txtSearchRestrictUser').val();
    if (_searchRestrictUser == null || _searchRestrictUser.length == 0) {
        BindRestrictUserListGrid();
        ReloadGridData();

        return;
    }
    BindRestrictUserListGrid();
    ReloadGridData();
}

// Restrict User Reload Grid
function ReloadGridData() {
    jQuery("#RestrictUserListGrid").trigger("reloadGrid");
}

function UpdateRestrictedUser(userId, isRestricted) {

    var tabId = _customTabId;

    if (tabId > 0) {
        var userData = new Object();
        userData.userId = userId;
        userData.tabId = tabId;
        userData.isRestricted = isRestricted;

        data = JSON.stringify(userData);
        _serviceUrl = _baseURL + 'Services/RestrictTab.svc/UpdateRestrictedUser';
        AjaxPost(_serviceUrl, data, eval(UpdateRestrictedUserSuccess), eval(UpdateRestrictedUserFail));
    }
}

function UpdateRestrictedUserSuccess(result) {
    if (result != null && result.d != null) {
        var errorMessage = result.d;
        var errorMessageType = 'jSuccess';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    }
}

function UpdateRestrictedUserFail() {
}

