﻿//A JS CLASS USED BY THE ADMIN > CUTOM TAB PAGE

//Global variables
var _serviceURL;
var _data;
var _tabEdit = '';
var _editSortOrder = 0;
var _tabName = '';
var _tabId = '';
var _activeModuleId = '';
var _activeModuleName = '';
var _customTabId = '';
var _customTabName = '';
var _companyId = '';

//assigning the DatePicker to the DateFields
var dateSelectors = "";

$(document).ready(function () {
    
    //Set for page load action
    SetPageLoadAction();

    //On save tab button click event, save the tab details
    $('#btnSaveTab').click(function () {
        SaveTab();
    });


    //On save tab button click event, save the tab details
    $('#btnDeleteTab').click(function () {
        DeleteTab();
    });


    //On click of the 'Shown in all tab' option checked on
    $("#chkShowAllTab").click(function () {
        ModifyStateOfModuleCheckBoxes();
    });


    //Add field functionality
    $('#btnAddField').click(function () {

        $('#btnSave')[0].parentElement.className = 'buttonCenter';
        $('#btnDelete')[0].parentElement.className = 'buttonCenter';
        $('#btnCancel')[0].parentElement.className = 'buttonCenter';

        $('#btnSave').val('Apply Changes');
        AddFieldForCurrentTab();
    });


    //To hide the popup when ok button is clicked
    $('#btnListTreeOk').click(function () {
        CloseAdminCustomTreeviewPopup();
    });


    //To hide the popup when cancel button is clicked
    $('#btnListTreeCancel').click(function () {
        CloseAdminCustomTreeviewPopup();
    });


    //Set all select checkbox value as true
    $('#chkIncident,#chkInvestigation,#chkAudit,#chkCAP').click(function () {
        SetShowAllTab();
    });

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 1);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 1);
    });
    
   
    //$('#menu').hide();

    //$('#lnknew').click(function () {

    //    window.location.href = _vdir + 'Admin/Configuration/Incident/New-Tab-0';
    //});

    ////$('#lnkCreateNewTab').attr('href', _baseURL + 'Admin/Configuration/Incident/NewTab-0');

    //$('#lnkCreateNewTab').attr('href', $('#lnk_0').find('a').attr('href'));
    //$('#lnk_0').hide();

    $('#lnkCreateNewTab').attr('href', $('#hdAddNewUrl').val());
});


//Some functions getting executed each time when Page Loads
function SetPageLoadAction() {


    //get companyid from js.com file
    //_companyId = GetCompanyId();

    //Display or hide the controls
    VisibleOrInvisibleControls();

    //To hide popup page 
    CloseAddEditFieldPopup();

    //Close Admin Custom Tree view Popup
    CloseAdminCustomTreeviewPopup();

    //to get _customTabId,_customTabName
    GetActiveTabIdAndName();

    //Get Active Module Id And Name
    GetActiveModuleIdAndName();

    //to get _customTabId,_customTabName
    GetSubTabDetailsFromUrl();

    //Populate Details of Tabs
    PopulateCustomTabDetails();

    //Added code for selected Tab
    AdminTabcolour()

    if (_customTabId != '') {
        AddCustomFields();
    }

}



//Add custom fields
function AddCustomFields() {

    _data = '&tabId=' + _customTabId;

    if (_customTabId != null && _customTabId != undefined) {

        var serviceURL = _baseURL + 'Services/Fields.svc/GetFieldListByTabID';
        Ajax(serviceURL, _data, eval(AddCustomFieldSuccess), eval(AddCustomFieldFail), 'InsertCustomFieldsControls');

    }
}


//On fail of add custom field
function AddCustomFieldFail(result) {
    alert("Error Message Details:" + msg.status + ":" + msg.statustext);
}



//On success of custom field add, will optimize later
function AddCustomFieldSuccess(result) {

    var controlType = '';
    var displayName = '';
    var fieldData = "";
    var deletedFieldData = "";
    var fieldCount = 0;
    var fieldId = '';
    var isRequired = '';
    var systemListName = '';
    var systemListID = 0;
    var treeViewType = 0;
    var dropDownCount = 0;
    var fieldDisplayName = '';
    var description = '';
    var dataType = '';

    if (result != null && result != '[]') {
        $('#TabField').show();
        _sortOrder = 0;
        var spn = document.getElementById('spntest');
        if (result != null) {
            var TabFields = JSON.parse(result.d);

            if (TabFields.length > 0) {

                _sortOrder = TabFields[TabFields.length - 1]["SortOrder"];

            }

            for (var i = 0; i < TabFields.length; i++) {
                fieldCount++;
                _fieldOrderArray.push(TabFields[i]["FieldID"]);
                displayName = TabFields[i]["DisplayName"];
                controlType = TabFields[i]["ControlType"];
                fieldId = TabFields[i]["FieldID"];
                isRequired = TabFields[i]["IsRequired"];
                systemListName = TabFields[i]["ListName"];
                systemListID = TabFields[i]["ListID"];
                treeViewType = TabFields[i]["Disptype"];
                description = TabFields[i]["Description"];
                dataType = TabFields[i]["Datatype"];
                _fieldIdList = _fieldIdList + fieldId + ',';
                var ctrl = "";
                var field = CreateDynamicFieldName(displayName, isRequired);
                var editbutton = "";
                var infobutton = CreateInfoButton(description);
               
                if (TabFields[i]["IsActive"]) {
                   
                    ctrl = CreateFieldsDynamic(displayName, controlType, systemListName, systemListID, treeViewType);
                    displayName = displayName.replace(/ /g, '');
                    if (dataType.indexOf('Date') >= 0) {
                        if (dateSelectors != null && dateSelectors.length > 0) {
                            dateSelectors = dateSelectors + ',';
                        }
                        dateSelectors = dateSelectors + "txt" + displayName;

                    }
                    editbutton = CreateEditButton(displayName, fieldId, controlType);

                    fieldData += "<li style='border:0px solid red;' class='MarginTop10 ClearAll' id=Field" + "_" + fieldId + ">";

                    if (controlType == 'Paragraph Text')
                        fieldData += " <div style='border:0px solid green' style='padding-left:10px;'>" + field + "</div>" + "<div style='vertical-align:bottom;'>" + ctrl + "<span style='padding-left:10px;vertical-align:middle;'>" + infobutton + "&nbsp;" + editbutton + "</span></div>";
                    else
                        fieldData += " <div style='border:0px solid green;' style='padding-left:10px;'>" + field + "</div>" + "<div style='vertical-align:bottom;'>" + ctrl + "<span style='padding-left:10px;vertical-align:middle;'>" + infobutton + "&nbsp;" + editbutton + "</span></div>";

                    fieldData += "</li>";
                }
                else {
                    ctrl = CreateDisableFieldsDynamic(displayName, controlType, systemListName, systemListID, treeViewType);
                    editbutton = CreateReactivateButton(displayName, fieldId);

                    deletedFieldData += "<li class='MarginTop10 ClearAll' id=Field" + "_" + fieldId + ">";
                    if (controlType == 'Paragraph Text')
                        deletedFieldData += " <div  style='padding-left:0px;' disabled='true' >" + field + "</div>" + "<div style='border:0px solid green;' style='vertical-align:bottom;'>" + ctrl + "<span style='padding-left:10px;vertical-align:bottom;padding-top:10px;border:0px solid red;'>" + infobutton + "</span><span style='margin-left:10px;'>" + editbutton + "</span></div>";
                    else
                        deletedFieldData += " <div  style='padding-left:0px;' disabled='true' >" + field + "</div>" + "<div style='border:0px solid green;' style='vertical-align:bottom;'>" + ctrl + "<span style='padding-left:10px;vertical-align:bottom;padding-top:5px;border:0px solid red;'>" + infobutton + "</span><span style='margin-left:10px;'>" + editbutton + "</span></div>";

                }

            }
            for (var j = 0; j < TabFields.length; j++) {

                treeViewType = TabFields[j]["Disptype"];
                controlType = TabFields[j]["ControlType"]

                if (treeViewType == '1' && controlType == 'Select one from list') {

                    dropDownCount++;
                    systemListID = TabFields[j]["ListID"];
                    fieldDisplayName = TabFields[j]["DisplayName"];
                    var SystemListData = GetSystemListData(systemListID, fieldDisplayName);
                    var _data = JSON.stringify(SystemListData);
                    var serviceURL = _baseURL + 'Services/ListValues.svc/ListvaluesToBindDDl';
                    AjaxPost(serviceURL, _data, eval(CustomDropDownTreeListsucc), eval(CustomDropDownTreeListfail));

                }
            }

        }
        if (_fieldIdList.lastIndexOf(',') > 0) {
            _fieldIdListTrim = _fieldIdList.substring(0, _fieldIdList.length - 1);
        }

    }
    else {
        $('#TabField').hide();
    }

    DisplayInActiveFieldRegion(deletedFieldData);

    DisplayUserPreviewFieldRegion(fieldData);

    $('#FieldMask, #FieldBox').hide();

    // SiteSettingDateFormat();

    if (dateSelectors.indexOf(',') >= 0) {
        var dateFieldIdSplit = dateSelectors.split(',');
        if (dateFieldIdSplit != null) {
            for (var i = 0; i < dateFieldIdSplit.length; i++) {
                //$('#' + dateFieldIdSplit[i] + '').datepicker({ dateFormat: dateFormat });
                $('#' + dateFieldIdSplit[i] + '').datepicker({ dateFormat: localStorage.dateFormat });
            }
        }
    }
}


//Method to Set the DateFormat based on Sitesetting
function SiteSettingDateFormat() {
    //get companyId and UserId
    _serviceUrl = _baseURL + 'Services/SiteSettings.svc/GetDateFormat';
    Ajax(_serviceUrl, _data, eval(SiteSettingDateFormatSuccess), eval(SiteSettingDateFormatFail), 'GetDateFormat');
}

//success method for SiteSettingDateFormat
function SiteSettingDateFormatSuccess(result) {
    var dateFormat = '';
    if (result != null) {

        switch (result.d) {

            case 'dd/MM/yyyy':
                dateFormat = 'dd/mm/yy';
                break;

            case 'MM/dd/yyyy':
                dateFormat = 'mm/dd/yy';
                break;

            case 'MMM dd yyyy':
                dateFormat = 'M dd yy';
                break;

            case 'dd-MM-yyyy':
                dateFormat = 'dd-mm-yy';
                break;

            case 'militaryformat':
                dateFormat = 'dd-M-yy';
                break;
        }
        dateDisplayFormat = dateFormat;
    }
    if (dateFormat != '') {
        if (dateSelectors.indexOf(',') >= 0) {
            var dateFieldIdSplit = dateSelectors.split(',');
            if (dateFieldIdSplit != null) {
                for (var i = 0; i < dateFieldIdSplit.length; i++) {
                    //$('#' + dateFieldIdSplit[i] + '').datepicker({ dateFormat: dateFormat });
                    $('#' + dateFieldIdSplit[i] + '').datepicker();
                }
            }
        }
        else {
            //$('#'+ dateSelectors +'').datepicker({ dateFormat: dateFormat });
            $('#' + dateSelectors + '').datepicker();
        }

        $("#ui-datepicker-div").addClass("notranslate");
    }
}

//service failed method
function SiteSettingDateFormatFail() {
    alert("Error in loading data.");
}

//******************************
//*** FETCH TAB DETAILS
//******************************

//Populate custom sub tab details
function PopulateCustomTabDetails() {

    if (_customTabId != '') {

        GetActiveModuleIdAndName();

        var data = '&tabId=' + _customTabId + '&moduleId=' + _activeModuleId;
        var serviceURL = _baseURL + 'Services/Tabs.svc/GetTabDetailsByID';
        Ajax(serviceURL, data, eval(PopulateCustomTabDetailsSuccess), eval(PopulateCustomTabDetailsFail), 'GetTabDetailsByID');

    }
}

//On success of populating custom sub tab details
function PopulateCustomTabDetailsSuccess(result) {


    if (result != null) {

        var customSubTabDetail = JSON.parse(result.d);

        if (customSubTabDetail != null) {


            $('#txtTabName').val(customSubTabDetail.TabName);
            $('#btnSaveTab').attr("disabled", false);
            $('#hdCurrentModule').val(customSubTabDetail.ModuleID);
            $('#chkShowAllTab').attr('checked', customSubTabDetail.IsUniversal);
            $('#chkRestrictedAccess').attr('checked', customSubTabDetail.IsRestricted);
            if (customSubTabDetail.IsRestricted)
                $('#ancRestrictUser').show();

            if (!customSubTabDetail.IsUniversal) {
                SetSelectedModule(customSubTabDetail.SelectedModuleName);
            }
            else {
                SetAttributesForModuleCheckBoxes('checked', true);
                SetAttributesForModuleCheckBoxes('disabled', true);
            }

            SetActiveValueAndDeleteTab(customSubTabDetail.Active);

            $('#btnSaveTab').val('Apply Changes');

            $('#btnAddField').show();

            //Details Tab cannot be deleted or inactivated
            if (customSubTabDetail.Companies != null) {

                $('#txtTabName').attr('disabled', true);
                $('#chkActive').attr('disabled', true);
                $('#chkShowAllTab').attr('disabled', true);
                $('#btnDeleteTab').hide();
                $('#btnSaveTab').hide();
                $('#detailsTabBox').show();
                $('#lblIsRequired')[0].innerHTML = 'Required';
            }

        }
        else {

            $('#txtTabName').val();
            $('#chkShowAllTab').attr('checked', false);

            if (_customTabId == 0) {
                $('#chkShowAllTab').attr('disabled', false);
                $('#btnAddField').hide();
            }
            else {
                // $('#chkShowAllTab').attr('disabled', true);
            }

            $('#btnSaveTab').attr("disabled", false);
            $('#ddlModule').val();
            // $('#chkActive').attr('checked', false);
            $('#btnAddField').hide();
        }

    }

}


//Set selected module check boxes
function SetSelectedModule(selectedModuleName) {

    var splittedModules = selectedModuleName.split(',');
    for (var index = 0; index < splittedModules.length; index++) {
        var checkBoxControl = "chk" + splittedModules[index];
        if (splittedModules[index] == 'Action Plan' || splittedModules[index] == 'ActionPlan') {
            $("#chkCAP").attr('checked', true);
        }
        else {
            $("#" + checkBoxControl).attr('checked', true);
        }

    }
    if ($('#chkIncident').is(':checked') && $('#chkInvestigation').is(':checked') && $('#chkAudit').is(':checked') && $('#chkCAP').is(':checked')) {
        $('#chkShowAllTab').attr('checked', true);
        SetAttributesForModuleCheckBoxes('disabled', true);
    }
}


//Set active value and delete tab button property
function SetActiveValueAndDeleteTab(isActive) {

    $('#chkActive').attr('checked', isActive);

    if (isActive) {
        $('#btnDeleteTab').hide();
    }
    else {
        $('#btnDeleteTab').show();
    }
}

//On fail of Populate Custom Tab Details
function PopulateCustomTabDetailsFail() {
    alert('Error in Loading Data.');
}

//Added for colour change of selected Tab
function AdminTabcolour() {

    var tab = '';
    var mainTab = '';

    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    tab = urlArray[urlArray.length - 1];
    tab = tab.substring(tab.lastIndexOf('-') + 1);

    mainTab = urlArray[urlArray.length - 2];

    if (urlArray[urlArray.length - 1] == 'Custom-Tabs') {
        mainTab = 'Incident';
    }
    else if (urlArray[urlArray.length - 1] == 'Incident' || urlArray[urlArray.length - 1] == 'Investigation' ||
            urlArray[urlArray.length - 1] == 'Audit' || urlArray[urlArray.length - 1] == 'CAP') {

        mainTab = urlArray[urlArray.length - 1];

    }

    mainTab = ((mainTab == 'Incident') ? 1 : (mainTab == 'Investigation') ? 2 : (mainTab == 'Audit') ? 3 : (mainTab == 'CAP') ? 4 : 0);

    SelectedAdminTabColor(location);


    $('#lnkmain' + mainTab).removeClass('LiUserTabSelect');
    $('#lnkmain' + mainTab).addClass('LiUserTab');

}


//Added to display the notification
function DisplayNofification(tabName, companyParentId, sortOrder) {
    if (companyParentId != null || sortOrder != 0) {
        var message = tabName + ' is a system tab and cannot be modified';
        var messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
    }

}


//Set checked value for chkShowAllTab
function SetShowAllTab() {

    var allChecked = ($('#chkIncident').attr('checked')) && ($('#chkInvestigation').attr('checked')) &&
                     ($('#chkAudit').attr('checked')) && ($('#chkCAP').attr('checked'));

    if (allChecked) {

        $("#chkShowAllTab").attr('checked', true);
        SetAttributesForModuleCheckBoxes('disabled', true);
    }
}


//Modify State Of Module Check Boxes
function ModifyStateOfModuleCheckBoxes() {

    if ($('#chkShowAllTab').attr('checked')) {

        //On checked, auto-checks the 4 module check boxes and disables them
        SetAttributesForModuleCheckBoxes('checked', true);
        SetAttributesForModuleCheckBoxes('disabled', true);
    }
    else {

        //On uncheck, auto-unchecks all 4 check boxes and enables them
        SetAttributesForModuleCheckBoxes('checked', false);
        SetAttributesForModuleCheckBoxes('disabled', false);
    }
}


//Either  enable or disable the module check boxes
function SetAttributesForModuleCheckBoxes(attributeName, checkedFlag) {

    $('#chkIncident').attr(attributeName, checkedFlag);
    $('#chkInvestigation').attr(attributeName, checkedFlag);
    $('#chkAudit').attr(attributeName, checkedFlag);
    $('#chkCAP').attr(attributeName, checkedFlag);

}


//Method To get Subtabid  and Subtabname
function GetActiveTabIdAndName() {

    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');
    _tabName = GetCustomSubTabDetails(urlArray[urlArray.length - 1], 'name');
    _tabId = GetCustomSubTabDetails(urlArray[urlArray.length - 1], 'id');
}


//Get sub tab details
function GetCustomSubTabDetails(subTabName, val) {

    var subTab = '';
    var arrayName = subTabName.split('-');

    if (val == 'name') {

        if (arrayName.length > 2) {
            subTab = subTabName.substring(0, subTabName.lastIndexOf('-')).replace(/-/g, ' ');
        }
        else {
            subTab = arrayName[0];
        }

    }
    else if (val == "id") {

        subTab = arrayName[arrayName.length - 1];

        if (subTab.indexOf('#') != -1) {
            subTab = subTab.replace('#', "");
        }
    }

    return subTab;
}


//Make the controls visible / invisible
function VisibleOrInvisibleControls() {

    $('#divAddEditFieldsDialog').hide();
    $('#AddEditBox').show();
    $('#btnAddField').hide();
    $('#btnDeleteTab').hide();
    $('#ListMask, .popupWindow').hide();

    //field page
    $('#MaxLength').hide();
    $('#TabField').hide();
    $('#IncidentPreview').hide();
    $('#InvestiagtionPreview').hide();

}


//Close add/edit pop up
function CloseAddEditFieldPopup() {

    $('#divAddEditFieldsDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divAddEditFieldsDialog").dialog("destroy");
    $('#divAddEditFieldsDialog').bind("dialogclose", function (event, ui) {
    });
}


//Close Admin Custom Tree view Popup
function CloseAdminCustomTreeviewPopup() {

    $('#divCustomTreeviewDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divCustomTreeviewDialog").dialog("destroy");


    $('#divCustomTreeviewDialog').bind("dialogclose", function (event, ui) {
    });
}


//Clear all controls
function ClearAllTab() {

    $('#txtTabName').val('');
    $('#chkShowAllTab').attr('checked', false);
    $("#ddlModule option:first").attr('selected', 'selected');
    $('#chkActive').attr('checked', false);

}

//Set active module id
function GetActiveModuleIdAndName() {
    
    var splittedURL = window.location.href.split('/');
    _activeModuleName = splittedURL[splittedURL.length - 2];
    _activeModuleId = (_activeModuleName == "Incident" ? 1 : _activeModuleName == "Investigation" ? 2 : _activeModuleName == "Audit" ? 3 : _activeModuleName == "ActionPlan" ? 4 : 1);

    if (_activeModuleName == 'Corrective Action Plan') {
        _activeModuleName = 'CAP';
    }
}


//Get currently active module link name
function GetCurrentlyActiveModuleLink() {

    var linkName = '#lnkmain' + _activeModuleId;
    return linkName;
}


//common serviceFailed Message
function serviceFailed(msg) {
    alert(msg);
}


//method to redirect Page from Main Tabs
function RedirectPageUrl(result) {
    window.location.href = result;
}


//method To get Subtabid  and Subtabname
function GetSubTabDetailsFromUrl() {

    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    _customTabName = GetCustomSubTabDetails(urlArray[urlArray.length - 1], 'name');
    _customTabId = GetCustomSubTabDetails(urlArray[urlArray.length - 1], 'id');
}

// function for next sub-tab in event centerd arrow.
function NextSubTabClick(location, ind) {

    var tabid = '';
    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    tabid = urlArray[urlArray.length - ind];
    tabid = tabid.substring(tabid.lastIndexOf('-') + 1);


    var linkId = $('#lnk_' + tabid).next().attr('id');
    if (linkId && linkId.indexOf("lnk_") >= 0) {
        var splitLinkId = linkId.split('_');
        tabid = splitLinkId[1];
        var URL = $('#lnk' + tabid).attr('href');
        if (URL != undefined)
            window.location = URL;
    }
}

// function for previous sub-tab in event centerd arrow.
function PreviousSubTabClick(location, ind) {

    var tabid = '';
    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    tabid = urlArray[urlArray.length - ind];
    tabid = tabid.substring(tabid.lastIndexOf('-') + 1);

    var linkId = $('#lnk_' + tabid).prev().attr('id');
    if (linkId && linkId.indexOf("lnk_") >= 0) {
        var splitLinkId = linkId.split('_');
        tabid = splitLinkId[1];
        var URL = $('#lnk' + tabid).attr('href');
        window.location = URL;
    }
}

//To assign css based on Tab selected and deselected of Sub Tabs
function SelectedAdminTabColor(location) {
    var tabid = '';
    var url = jQuery(location).attr('href');
    var urlArray = url.split('/');

    tabid = urlArray[urlArray.length - 1];
    tabid = tabid.substring(tabid.lastIndexOf('-') + 1);

    //For selected subtab color change
    $('#lnk' + tabid).removeClass('sub-tab');
    $('#lnk' + tabid).removeClass('inactive-tab');
    $('#lnk' + tabid).addClass('selected-tab');
    $('#lnk' + tabid).after("<div  class='arrow-down'></div>");
    $('#lnk' + tabid).css('color', '#272727');
    $('#lnk' + tabid).css('border', '1px solid #272727');

    $('#PrevSubTabPage').css('display', 'block');
    $('#NextSubTabPage').css('display', 'block');

    adminSubTabNextPrev(tabid);
}

//admin custom sub-tab next prev arrow enable/disable
function adminSubTabNextPrev(tabId) {
    var prevTabTitle = '';
    //previous sub-tab arrow display/hide
    var prevLinkId = $('#lnk_' + tabId).prev().attr('id');
    if (prevLinkId) {
        prevTabTitle = $('#' + prevLinkId).children('a').html();
        if (prevTabTitle && prevTabTitle.indexOf("&lt;-") >= 0) {
            prevTabTitle = prevTabTitle.replace('&lt;-', '').replace('-&gt;', '');
        }
    }
    var URL = $('#' + prevLinkId).children('a').attr('href');
    if (prevLinkId && prevLinkId.indexOf("lnk_") >= 0 && URL != null) {
        $('#PrevSubTabPage').prop("disabled", false);
        $('#PrevSubTabPage').attr('title', 'Back to ' + prevTabTitle);
    } else {
        $('#PrevSubTabPage').attr("disabled", "disabled");
        $('#PrevSubTabPage').removeClass('arrow-left');
        $('#PrevSubTabPage').addClass('arrow-left-tab-disable');
        $('#PrevSubTabPage').css("cursor", "default");
        $("#PrevSubTabPage").unbind('click');
        $('#PrevSubTabPage').attr('title', '');
    }


    var nextTabTitle = '';
    //next sub-tab arrow display/hide
    var nextLinkId = $('#lnk_' + tabId).next().attr('id');
    if (nextLinkId) {
        nextTabTitle = $('#' + nextLinkId).children('a').html();
        if (nextTabTitle && nextTabTitle.indexOf("&lt;-") >= 0) {
            nextTabTitle = nextTabTitle.replace('&lt;-', '').replace('-&gt;', '');
        }
    }
    var URL = $('#' + nextLinkId).children('a').attr('href');
    if (nextLinkId && nextLinkId.indexOf("lnk_") >= 0 && URL != null) {
        $('#NextSubTabPage').prop("disabled", false);
        $('#NextSubTabPage').attr('title', 'Continue to ' + nextTabTitle);

    } else {
        $('#NextSubTabPage').attr("disabled", "disabled");
        $('#NextSubTabPage').removeClass('arrow-right');
        $('#NextSubTabPage').addClass('arrow-right-tab-disable');
        $('#NextSubTabPage').css("cursor", "default");
        $("#NextSubTabPage").unbind('click');
        $('#NextSubTabPage').attr('title', '');
    }
}