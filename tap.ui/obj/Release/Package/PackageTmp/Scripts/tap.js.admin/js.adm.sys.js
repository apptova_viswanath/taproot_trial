﻿
//*System Lists Page Methods (Deepa)*//

//Global Variables
var _listValueId = '';
var _listName = '';
var _listDescription = '';
var _rootNode = '';
var _rootNodeId = '';
var _serviceUrl;
var _selectedTreeviewItemId = '';
var _selectedTreeviewItemName = '';
var _newItemListName = '';
var _newListParentId = '';
var _nodeAdditionCancelled = false;
var _newNodeName = 'New node';
var _listId = '';
var mveLstTrigger= false
//Page load Events
$(document).ready(function () {

    //on load code
    SetPageLoad();

    $('#btnSystemClear').click(function () {
        ClearSystemListsControls();
    });

    //Add item to treeview
    $('#btnNewSystemTreeItem').click(function () {
        AddNewTreeviewItem();
        _isDirty = false;
    });

    //Delete tree view item
    $('#btnDeleteSystemTreeItem').click(function () {
        DeleteTreeviewItem();
        _isDirty = false;
    });

    // Open add System List pop up box
    $("#spnAddSystemList").click(function () {

        $('#txtSysListID').val('');
        OpenAddNewSystemListPopup(true);
        return false;
    });

    // Open add System List pop up box
    $("#lblAddSystemList").click(function () {

        $('#txtSysListID').val('');
        _isDirty = false;
        OpenAddNewSystemListPopup(true);
    });

    //Add new System List
    $("#btnEditSystemList").click(function () {

        _isDirty = false;
        SaveSystemList();
    });

    //edit System List
    $("#btnAddSystemList").click(function () {
        _isDirty = false;
        AddNewSystemList();
    });

    //close popup page
    $("#btnCancelSystemList").click(function () {

        _isDirty = false;       
        CloseSystemListPopup();
    });

    //for inline editing for List items
    SystemTreeviewItemEdit();//run and see

    $("#btnDeleteList").click(function () {

        DeleteList();
        $('.ui-icon-closethick').click();
    });

    $("#btnInActivateList").click(function () {

        if ($(this).val() == 'Activate') {
            ActivateOrInactivateList("OriginParentList", true);
        } else {
            ActivateOrInactivateList("OriginParentList", false);
        }
        $('.ui-icon-closethick').click();
    });

    $("#btnInActivateListItem").click(function () {

        if ($(this).val() == 'Activate') {
            ActivateOrInactivateList("ListItem", true);
        } else {
            ActivateOrInactivateList("ListItem", false);
        }

    });

    //close popup page
    $("#imgSetting").click(function () {

        if ($("#divSettings").is(":visible")) {

            SetDefaultEditFormSetting();

        } else {

            $("#divSettings").show();
            $("#systemItems").hide();
            $("#imgSetting").attr("src", _baseURL + 'Scripts/jquery/images/edit-list-icon.png');
            $("#imgSetting").attr("title", "Go back to Edit");

        }
    });

});

function ActivateOrInactivateList(listType, updateValue) {

    var listData = new Object();

    listData.listId = (listType != "ListItem") ? parseInt(_listId) : _selectedTreeviewItemId;
    listData.listType = listType;
    listData.updateValue = updateValue;

    if (listData.listId.length == 0) {
        NotificationMessage('Select an item to activate/inactivate.', 'jError', true, 3000);
        return false;
    }

    if (listType != 'OriginParentList') {
        if (updateValue) {
            $("#btnInActivateListItem").attr("value", 'Inactivate');
            $('#' + listData.listId).find('a').css({ "color": "#355779" });
            $('#' + listData.listId).find('td').css({ "color": "#355779" });
            $('#' + listData.listId).attr('active', 'True');
        }
        else {
            $("#btnInActivateListItem").attr("value", 'Activate');
            $('#' + listData.listId).find('a').css({ "color": "darkgray" });
            $('#' + listData.listId).find('td').css({ "color": "darkgray" });
            $('#' + listData.listId).attr('active', 'False');
        }
    }
       
    listData.userId = $('#hdnUserId').val();
    
    var data = JSON.stringify(listData);
    _serviceUrl = _baseURL + 'Services/ListValues.svc/ActivateOrInactivateList';
    PostAjax(_serviceUrl, data, eval(ActivateOrInactivateListSuccess), eval(ActivateOrInactivateListFail), '');
}

function ActivateOrInactivateListSuccess(result) {
    var errorMessage = _selectedTreeviewItemName + ' List deactivated.';

    if (result != null && result.d != null) {

        errorMessage = result.d;
        BindSystemListGrid();
        ReloadGridData();
        var errorMessageType = 'jSuccess';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    }
}

function ActivateOrInactivateListFail() {

}

function SetDefaultEditFormSetting() {

    $("#divSettings").hide();
    $("#systemItems").show();
    $("#imgSetting").attr("src", _baseURL + 'Images/settings-black.png');
    $("#imgSetting").attr("title", "Setting");


}

//on page load Code
function SetPageLoad() {

    //hide the Pop Up box
    $('#divAddEditSystemListDialog').hide();
    $('#divSystemListTreeviewDialog').hide();

    //hide the grid error link
    $("#ReloadGrid").hide();

    //Get System List Grid Count
    SystemListGridCount();
}


// Reload Grid
function ReloadGridData() {
    jQuery("#SystemListGrid").trigger("reloadGrid");
}

//Add node to treeview
function AddNewTreeviewItem() {

    $("#systemTreeviewList").jstree("create");

}

//close System List Popup page.
function CloseSystemListPopup() {

    //Clear the System List Controls
    ClearSystemListControls();

    $('#divAddEditSystemListDialog').hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divAddEditSystemListDialog").dialog("destroy");
    $('#divAddEditSystemListDialog').bind("dialogclose", function (event, ui) {

    });
}

//to get System Lists
function GetSystemLists(parameterData) {

    var systemListData = new Object();
    systemListData.page = parameterData.page;
    systemListData.rows = parameterData.rows;
    systemListData.sortIndex = parameterData.sidx;
    systemListData.sortOrder = parameterData.sord;

    systemListData.companyId = _companyId; //remove hard code

    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        systemListData.companyId = divisionId;

    data = JSON.stringify(systemListData);
    _serviceUrl = _baseURL + 'Services/ListValues.svc/SystemListSelect';
    AjaxPost(_serviceUrl, data, eval(GetSystemListsSuccess), eval(GetSystemListsFail));
}

// System List Success
function GetSystemListsSuccess(result) {
    //debugger;
    var errorMessage = '';
    var errorMessageType = 'jError';
    var gridData = jQuery("#SystemListGrid")[0];
    gridData.addJSONData(JSON.parse(result.d));
    var resultCount = ParseToJSON(result.d);

    if (resultCount.rows.length == 0) {

        $("#ReloadGrid").show();
        errorMessage = 'This is an invalid page number.';
        errorMessageType = 'jError';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    }

}

//service fail
function GetSystemListsFail() {
    alert('Service failed.');
}

//System List Grid Binding
function BindSystemListGrid() {

    jQuery("#SystemListGrid").jqGrid({

        datatype: function (parameterData) { GetSystemLists(parameterData); },
        colNames: ['List Name', 'Description', 'IsSystemList', 'Active'//, 'Edit'        
        ],
        colModel: [
            { name: 'ListName', index: 'ListName', width: 200, align: 'left', editable: false, formatter: TextFormatter, resizable: true },
            { name: 'Description', index: 'Description', width: 300, align: 'left', editable: false, resizable: true },
            { name: 'IsSystemList', index: 'IsSystemList', width: 200, align: 'left', resizable: false, editable: true, hidden: true, resizable: true },
            { name: 'Active', index: 'Active', width: 100, align: 'left', resizable: false, editable: false, hidden: true, resizable: true }
        ],
        gridview: false,
        rowattr: function (rd) {
            if (rd.Active == "False") { 
                return {"class": "rowColor"};
            }
        },
        toppager: false,
        emptyrecords: ' ',
        hidegrid: false,
        // Grid total width and height        
        autowidth: true,
        height: '100%',
        caption: 'Lists',
        pager: jQuery('#SystemListNavigation'),
        viewrecords: true,
        sortname: 'ListName',
        sortorder: "asc",
        forceFit: true,

        afterInsertRow: AddRowInGrid,

        ondblClickRow: function (rowId) {
            var rowData = jQuery(this).getRowData(rowId);
            var listName = $(rowData["ListName"]).html();
            var listDescription = rowData["Description"];
            $('#txtSysListID').val(rowId);

            //Clear the System List Controls
            ClearSystemListControls();

            //Open List Item tree view popup page
            OpenListItemPopup(row[0].id, listName, listDescription);
        }

    });
    jQuery("#SystemListGrid").click(function (e) {
        var el = e.target;
        var grid = jQuery('#SystemListGrid');
        var row = $(el, this.rows).closest("tr.jqgrow");
        var listName = $(grid.jqGrid('getCell', row[0].id, 'ListName')).html();
        var listDescription = grid.jqGrid('getCell', row[0].id, 'Description');
        if ($(el).index() == 0 && $(el).text() != null && $(el).text().length>0) {
            $('#txtSysListID').val(row[0].id);

            //Clear the System List Controls
            ClearSystemListControls();
           
            //Open List Item tree view popup page
            OpenListItemPopup(row[0].id, listName, listDescription);
        }
    });

    $(window).bind('resize', function () {
        $("#SystemListGrid").setGridWidth($('.SystemListGrid').width());
    });

   
} //grid end


//for Inline Edit
function invokeEditRow() {

    $("#SystemListGrid").jqGrid('editRow', lastSelectedRow, true, null, null, null, {},
                null,
                ErrorFunction
      );
}

function TextFormatter(cellvalue, options, rowObject) {

    var result = "<a href='' onclick='return false'>" + cellvalue + "</a>";
    return result;
}

//inline edit error function
function ErrorFunction(rowId, response) {

    // todo: why this does not allow Enter key to continue ase after error:
    restoreAfterError = false;
    $("#SystemListGrid").restoreAfterErorr = false;
    lastSelectedRow = rowId;
    invokeEditRow();
    return true;
}

function EditSystemListForm(systemId, listName, listDescription) {

    $('#validationListNameImg').hide();

    $('#txtNewSystemListName').val(listName);
    $('#txtNewSystemListDescription').val(listDescription);

    //open Edit System List popup dialog
    OpenAddNewSystemListPopup(false);
}

//System List Success function
function DeleteSystemListSuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jError';
    ReloadgridData();

    if (result.d == "") {
        errorMessage = 'List deleted.';
        errorMessageType = 'jSuccess';
    }
    else {
        errorMessage = result.d;// 'System List can not be deleted.';
        errorMessageType = 'jError';
    }

    NotificationMessage(errorMessage, errorMessageType, true, 3000);

}

//service failed
function DeleteSystemListFail() {
    alert('Service Failed.');
}


// Start editing the row 
function InlineEdit(rowId, iRow, column, e) {

    $(this).jqGrid('editRow', rowId, false, function () {

        FocusRow(rowId, column, this);
        SystemListInlineEditSave(rowId);

    });

}

//Insert Row in Grid

function AddRowInGrid(rowId, data) {

    if (data.Active == "False") {
        $(this).jqGrid('setRowData', rowId, false, { color: 'darkgray' });
        $('#' + rowId).find('a').css({ "color": "darkgray" });
    }  
   

}

function DeleteList() {

    var ConfirmDelete = confirm('Are you sure want to delete ' + _listName + '?');
    if (ConfirmDelete) {
        var data = '&listId=' + _listId + '&userId=' + $('#hdnUserId').val();
        _serviceUrl = _baseURL + 'Services/ListValues.svc/DeleteSystemListByField';
        Ajax(_serviceUrl, data, eval(DeleteSystemListSuccess), eval(DeleteSystemListFail), 'DeleteAdmSyslist');
    }
}

function OpenEditForm(rowId, listName, listDescription) {

    var isSystemList = jQuery("#SystemListGrid").jqGrid('getCell', rowId, 'IsSystemList');
    var isActive = jQuery("#SystemListGrid").jqGrid('getCell', rowId, 'Active');
    if (isActive == 'True') {
        $('#btnInActivateList').val('Inactivate');
    }
    else {
        $('#btnInActivateList').val('Activate');
    }

    if (isSystemList == 'True')
        $('#imgSetting').hide();
    else
        $('#imgSetting').show();

    _listId = rowId;

    $("#dialog:ui-dialog").dialog("destroy");
    $("#divSystemListTreeviewDialog").dialog("destroy");
    $('#systemTreeviewList').jstree('destroy').empty();

    SetDefaultEditFormSetting();

    //pre populate List Name and Description
    $('#txtEditSystemListName').val(listName);
    $('#txtEditSystemListDescription').val(listDescription);

    //open System List Treeview popup dialog
    OpenSystemListTreeviewDialog();
}

function OpenListItemPopup(rowId, listName, listDescription)
{


    var inputControl = jQuery('#' + (rowId));
    inputControl.bind("keydown", function (e) {
    });

    lastSelectedRow = rowId;

    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");
    //remove tree view data
    $('#systemTreeviewList').jstree('destroy').empty();

    var isSystemList = jQuery("#SystemListGrid").jqGrid('getCell', rowId, 'IsSystemList');

    if (isSystemList == "True") {

        $("#divDeleteListSection").hide();
    }
    else {

        $("#divDeleteListSection").show();
    }

    OpenEditForm(rowId, listName, listDescription);

    _listValueId = rowId;

    // load the tree view       
    LoadSystemTreeView(rowId);

    
}

//Saving the Edited SystemList on Enter key press
function SystemListInlineEditSave(rowId) {

    $(':input').bind("blur", function (e) {
        _listValueId = rowId;
        SaveSystemList();
    });

    $(':input').bind("keydown", function (e) {
        var enterKeyCode = 13;
        if (e.keyCode === enterKeyCode) {
            _listValueId = rowId;
            SaveSystemList();
        }
    });
}

//On Edit Focus to List Name text box
function FocusRow(rowId, column, table) {

    var element = document.getElementById(rowId + '_' + table.p.colModel[column].name),
        length = element.value.length;
    element.focus();

    if (element.setSelectionRange) { //IE 
        element.setSelectionRange(length, length);
    }
    else if (element.createTextRange) {
        var range = element.createTextRange();
        range.collapse(true);
        range.moveEnd('character', length);
        range.moveStart('character', start);
        range.select();
    }
}


//function to Reload Grid
function ReloadgridData() {

    jQuery("#SystemListGrid").trigger("reloadGrid");
}

//Open Pop up page for add System List
function OpenAddNewSystemListPopup(isNew) {

    var pageTitle = '';
    if (isNew) {
        ClearSystemListControls();
        pageTitle = 'Add List';
        $('#btnAddSystemList')[0].parentElement.className = 'buttonCenter';
        $('#btnCancelSystemList')[0].parentElement.className = 'buttonCenter';
        $('#btnAddSystemList').val('Create');

    }
    else {
        
        pageTitle = 'Edit List';
        $('#btnAddSystemList')[0].parentElement.className = 'buttonEditSave';
        $('#btnCancelSystemList')[0].parentElement.className = 'buttonEditCancel';        
        $('#btnAddSystemList').val('Apply Changes');
    }

    $('#addSystemErrorMsg').html('');

    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");

    $("#divAddEditSystemListDialog").dialog({

        title: pageTitle, 
        width: 380,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 200]
    });
}

//To add new System List
function AddNewSystemList() {
  
    $('#imgSetting').show();

    _listValueId = 0; //remove -1
    var systemListName = $('#txtNewSystemListName').val();
    if (ValidateAddNewSystemList(systemListName) == true) {

        SaveSystemList();
        CloseSystemListPopup();
    }
}

//Clear the System list controls
function ClearSystemListControls() {

    $('#txtNewSystemListName').val('');
    $('#txtNewSystemListDescription').val('');

    var txtListName = 'txtNewSystemListName';
    ShowHideWarningMessage(txtListName, 'Please specify List Name', false);
}

//Validate List Name 
function ValidateAddNewSystemList(systemListName) {

    var primaryErrorMessage = 'You are missing the following field: ';
    var errorMessage = primaryErrorMessage;

    errorMessage += ((systemListName == '') ? 'List Name\n' : '');

    var txtListName = 'txtNewSystemListName';

    if (systemListName == null || systemListName.length == 0) {
        ShowHideWarningMessage(txtListName, 'Please specify List Name', true);
    }
    else {
        ShowHideWarningMessage(txtListName, 'Please specify List Name', false);
    }

    if (errorMessage.length > primaryErrorMessage.length) {
        _message = errorMessage;
        _messageType = 'jError';
        NotificationMessage(_message, _messageType, true, 3000);
        return false;
    }
    return true;

}


//TO SAVE SYSTEM LISTS
function SaveSystemList() {

    var systemListName = '';
    var systemDescription = '';
    if (_listValueId == 0) //need to remove -1
    {
        systemListName = $('#txtNewSystemListName').val();
        systemDescription = $('#txtNewSystemListDescription').val();
        if (systemDescription == '')
            systemDescription = $('#txtNewSystemListName').val();
    }
    else {
        systemListName = $('#txtEditSystemListName').val();
        systemDescription = $('#txtEditSystemListDescription').val();      
    }
    var companyId = GetCompanyId();

    _listName = systemListName;
    _listDescription = systemDescription;

    var listValueID = $('#txtSysListID').val();
    if (listValueID != null && listValueID.length > 0)
        _listValueId = listValueID;

    if (ValidateSystemList(systemListName)) {

        var systemDetail = new SystemDataDetail(systemListName, systemDescription, companyId);
        data = JSON.stringify(systemDetail);
        _serviceUrl = _baseURL + 'Services/ListValues.svc/SaveSystemList';
        AjaxPost(_serviceUrl, data, eval(SystemListSuccess), eval(SytemListFail));
    }
}

//System List Parameters
function SystemDataDetail(systemListName, systemDescription, companyId) {

    var SystemDetails = {};
    if (_listValueId != '' && _listValueId != 0)
        SystemDetails.listId = _listValueId;
    else
        SystemDetails.listId = 0;

    SystemDetails.listName = systemListName;
    SystemDetails.listValueId = 0;
    if (systemListName.substring(0, 5) != "Class" && systemListName.substring(0, 5)!="Locat")
        SystemDetails.isSystemList = 0;
    else
        SystemDetails.isSystemList = 1;
    SystemDetails.isEditable = 1;
    SystemDetails.companyId = companyId;
    SystemDetails.active = 1;
    SystemDetails.description = systemDescription;
    SystemDetails.userId = $('#hdnUserId').val();
    return SystemDetails;
}

//System List Validation
function ValidateSystemList(systemListName) {

    var errorMessage = '';
    var errorMessageType = 'jError';

    if (systemListName == '') {
        errorMessage = 'Please enter List Name.';

        //show the error or success notification.
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
    return true;
}

//TO SAVE SYSTEM LISTS Success
function SystemListSuccess(result) {

    if (result != null) {
        var errorMessage = ''; //declare as global
        var errorMessageType = 'jError';
        if (_listValueId == 0) {
            if (result.d != null && $.isNumeric(result.d)) {

                errorMessage = _listName + ' List added.';
                errorMessageType = 'jSuccess';             

                //pre populate List Name and Description
                $('#txtEditSystemListName').val(_listName);
                $('#txtEditSystemListDescription').val(_listDescription);

                // load the tree view       
                LoadSystemTreeView(result.d);

                $("#btnInActivateListItem").hide();
                $("#btnDeleteSystemTreeItem").hide();
            }
            else
                errorMessage = result.d;

            
        }
        else {
            errorMessage = _listName + ' List updated.';
            errorMessageType = 'jSuccess';
        }

        _listValueId = result.d;

        //reload the grid
        ReloadgridData();

        //show the error or success notification.
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    }
}

function OpenSystemListTreeviewDialog()
{   

    $("#divSystemListTreeviewDialog").dialog({
        height: 420,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 200],
        width: 430

    });
}

//Confirmation for the delete
function SystemListDeleteConfirmation(listName) {

    if (listName != '') {
        if (Confirmation('Are you sure want to delete ' + listName + '?'))
            return true;
    }
    else {
        errorMessage = 'Please create List.Empty record can not be delete.';
        errorMessageType = 'jError';

        //show the error or success notification.
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
}

//Clear the controls
function ClearSystemListsControls() {

    $('#txtSystemListName').val('');
    $('#txtSystemDescription').val('');
    $('#systemItems').hide();
}

//service failed
function SytemListFail() {
    alert('Service failed.');
}

//System list tree view code starts
function LoadSystemTreeView(listValueId) {

    var treeviewdata = TreeViewDetail(listValueId);
    data = JSON.stringify(treeviewdata);
    var serviceUrl = _baseURL + 'Services/ListValues.svc/BuildSystemJSTreeView';
    AjaxPost(serviceUrl, data, eval(LoadTreeViewSuccess), eval(LoadTreeViewFail));

}

function TreeViewDetail(listId) {
    var treeviewdetail = {};
    treeviewdetail.listId = listId;
    treeviewdetail.companyId = _companyId;
    return treeviewdetail;
}

function LoadTreeViewSuccess(result) {

    SetDefaultEditFormSetting();

    if (result != null) {
        var jsonData = jQuery.parseJSON(result.d);
        _rootNode = jsonData[0].data;
        _rootNodeId = jsonData[0].attr.id; //get the root node id
       
        $("#dialog:ui-dialog").dialog("destroy");
        $("#divSystemListTreeviewDialog").dialog("destroy");

        //remove tree view data
        $('#systemTreeviewList').jstree('destroy').empty();

        DispSystemTreeNodes(jsonData);

        //open System List Treeview popup dialog-  //on click of Next button navigate to List Item popu box
        OpenSystemListTreeviewDialog();

    }

}

function LoadTreeViewFail() {
    alert('Service Failed');
}

//For opening of all nodes
function ExpandTreeNode() {

    $('#systemTreeviewList').bind("loaded.jstree", function (event, data) {

        $('#systemTreeviewList').jstree("open_all");
        $('#systemTreeviewList').jstree('select_node', 'ul > li:first');
 
        var treeItem = $("#systemTreeviewList .jstree-icon");
        $(treeItem).each(function () {

            if ($(this).parent().attr("active") == 'False') {
                $(this).parent().children('a').css({ "color": "darkgray" });
            }
        });

    });
}

//Method To bind treeview
function DispSystemTreeNodes(resJson) {

    _selectedTreeviewItemId = "";
    var errorMessage = '';
    var errorMessageType = 'jError';

    if (resJson != null) {

        if (resJson[0].children == undefined) {
            $("#btnInActivateListItem").hide();
            $("#btnDeleteSystemTreeItem").hide();
        } else {
            $("#btnInActivateListItem").show();
            $("#btnDeleteSystemTreeItem").show();
        }

        $("#systemTreeviewList").jstree({
            "json_data": { "data": resJson },
            core: {
                strings : {
                    'new_node': 'New item'
                }
            },
            "crrm": {
                "move": {
                    "check_move": function (data) {
                        if (data.r.attr("id") == resJson[0].attr.id) {                            
                            var retValue = false;

                            if (data.p == "before" || data.p == "first" || data.p == "after") { retValue = false; }
                            else if (data.p == "inside" || data.p == "last") { retValue = true; }
                            
                            return retValue;
                        }
                        else {
                            if (data.p == "before" || data.p == "first" || data.p == "after") { return false; }

                            return {
                                before: false,
                                after: false,
                                inside: true
                            };
                        }
                    }
                }
            },
            rules: { multiple: "ctrl" },
            checkbox: {
                two_state: true
            },

            plugins: ["themes", "json_data", "dnd", "crrm", "ui", "types"]

        });
        ExpandTreeNode();
    }

    //Save new System List Item       
    $('#systemTreeviewList').bind("create.jstree", function (e, data) {
    
        if (_nodeAdditionCancelled == true) {
            _nodeAdditionCancelled = false;
            return;
        }
        _newItemListName = data.rslt.name;
        if (data.rslt.parent != -1) {
            _newListParentId = data.rslt.parent[0].id;
        }
        else if (data.rslt.parent == -1) {
            _newListParentId = _rootNodeId;
        }
        if (_newListParentId != '') {
            SaveSystemTreeItems();
        }
    });

    //To get the selected folder id
    $('#systemTreeviewList').bind("select_node.jstree",
        function (e, data) {

            //To get the id of the selected node id
            _selectedTreeviewItemId = data.rslt.obj[0].id;

            if (data.rslt.e != undefined) {

                //To get the id of the selected node name 
                _selectedTreeviewItemName = TrimString(data.rslt.e.target.textContent);
            }

            if (_rootNodeId == _selectedTreeviewItemId) {
                $("#btnDeleteSystemTreeItem").hide();
                $("#btnInActivateListItem").hide();
            }
            else {
                parentitemid = data.rslt.obj.parents('li:eq(0)')[0].id;
                $("#btnDeleteSystemTreeItem").show();
                $("#btnInActivateListItem").show();
            }

            if (data.rslt.obj.attr("active") == 'True') {
                $("#btnInActivateListItem").attr("value", 'Inactivate');
            } else {
                $("#btnInActivateListItem").attr("value", 'Activate');
            }
        });

    $('#systemTreeviewList').bind("keydown.jstree", function (event) {

        var key = event.keyCode || event.which;
        if (key == 27) {

            _nodeAdditionCancelled = true;
            if (_selectedTreeviewItemName == _newNodeName) {

                DeleteRecentListItem();
                LoadSystemTreeView(_listValueId);
                _selectedTreeviewItemName = '';
            }
        }
        return;
    });

    //For renaming the jstree nodes
    $('#systemTreeviewList').bind("rename.jstree", function (e, data) {
       
        if (_nodeAdditionCancelled == true) {
            _nodeAdditionCancelled = false;

            // Hide the Add New/InActivate/Delete buttons
            $('#btnDeleteSystemTreeItem').css('display', 'inline');
            $('#btnInActivateListItem').css('display', 'inline');
            $('#btnNewSystemTreeItem').css('display', 'inline');

            return;
        }
        
        var newTreeviewItemName = data.rslt.new_name;
        _selectedTreeviewItemName = data.rslt.new_name;

        if (newTreeviewItemName != '') {
            //remove selction 
            var parent = -1;
            var listValueId = data.rslt.obj[0].id;
            var listValueName = data.rslt.new_name;
            var updateListDetails = new UpdateListDataDetails(listValueId, parent, listValueName);
            data = JSON.stringify(updateListDetails);
            _serviceUrl = _baseURL + 'Services/ListValues.svc/ListValuesUpdate';
            AjaxPost(_serviceUrl, data, eval(InlineNodeEditSuccess), eval(InlineNodeEditFail));

            // Hide the Add New/InActivate/Delete buttons
            $('#btnDeleteSystemTreeItem').css('display', 'inline');
            $('#btnInActivateListItem').css('display', 'inline');
            $('#btnNewSystemTreeItem').css('display', 'inline');
        }
        else {

            errorMessage = 'Item cannot be empty.';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }

    });

   
    // For drag and drop nodes
    $('#systemTreeviewList').bind("move_node.jstree", function (event, data) {
        mveLstTrigger = true;
        var parent = data.rslt.r[0].id;
        var listValueId = data.rslt.o[0].id;
        var listValueName = data.rslt.o[0].innerText;
        $("#systemTreeviewList").jstree("deselect_all");
        var updateListDetails = new UpdateListDataDetails(listValueId, parent, listValueName);
        data = JSON.stringify(updateListDetails);
        _serviceUrl = _baseURL + 'Services/ListValues.svc/ListValuesUpdate';
        AjaxPost(_serviceUrl, data, eval(InlineNodeEditSuccess), eval(InlineNodeEditFail));
    });

    $('#systemTreeviewList').bind('loaded.jstree', function (e, data) {

        $('#systemTreeviewList').children('ul').children('li').children('ins').first().css('background-position',' -18px 0');
    });

   
}

//Method to add  node
function UpdateListDataDetails(listValueId, parent, listValueName) {

    this.listValueId = listValueId;
    this.parent = parent;
    this.listValueName = listValueName;
    this.userId = $('#hdnUserId').val();
}

//Service Success method
function InlineNodeEditSuccess(result) {
    
    var errorMessage = '';
    var errorMessageType = 'jError';

        errorMessageType = 'jSuccess';
        errorMessage = _selectedTreeviewItemName + ' Renamed';
        if (!mveLstTrigger) {
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
        else
            mveLstTrigger = false;

    _selectedTreeviewItemName = '';
}

//service fail method
function InlineNodeEditFail() {
    alert('Service Failed.');
}

// save the Tree view Items
function SaveSystemTreeItems() {

    var parent = _newListParentId;
    var listValueId = 0;
    var listId = _listValueId;
    var listValueName = TrimString(_newItemListName);
    var active = 1;
    var systemListDetails = new SystemListItemsDetails(listValueId, listId, parent, listValueName, active);
    data = JSON.stringify(systemListDetails);
    _serviceUrl = _baseURL + 'Services/ListValues.svc/SaveListValues';
    AjaxPost(_serviceUrl, data, eval(SaveSystemTreeItemSuccess), eval(SaveSystemTreeItemsFail));
}

//Tree view parameters
function SystemListItemsDetails(listValueId, listId, parent, listValueName, active) {

    this.listValueId = listValueId;
    this.listId = listId;
    this.parent = parent;
    this.listValueName = listValueName;
    this.active = active;
    this.userId = $('#hdnUserId').val();
}

// save the Tree view Items Success method.
function SaveSystemTreeItemSuccess(result) {

    var errorMessage = '';
    var errorMessageType = 'jError';

        errorMessageType = 'jSuccess';
        errorMessage = TrimString(_newItemListName) + ' List added.';

    //load tree view
    LoadSystemTreeView(_listValueId);
}

//Service fail method.
function SaveSystemTreeItemsFail() {

    alert('Service Failed.');
}

//Delete System List Tree view Items
function DeleteTreeviewItem() {

    var errorMessage = '';
    var errorMessageType = 'jError';

    if (_selectedTreeviewItemId != '') {
        DeleteConfirmItem();
        _selectedTreeviewItemId = '';
    }
    else {
        errorMessage = 'Select an item to delete.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
        return false;
    }
}

function DeleteRecentListItem() {
    if (_selectedTreeviewItemName != TrimString(_rootNode)) {
        DeleteSystemTreeItems(_selectedTreeviewItemId);
    }
}

//Delete System List Tree view Confirmation method 
function DeleteConfirmItem() {

    var errorMessage = '';
    var errorMessageType = 'jError';

    if (_selectedTreeviewItemName != TrimString(_rootNode)) {
        var ConfirmDelete = confirm('Are you sure want to delete ' + _selectedTreeviewItemName + '?');
        if (ConfirmDelete) {
            DeleteSystemTreeItems(_selectedTreeviewItemId);
        }
    }
    else {
        errorMessage = 'Root Node can not be deleted.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
    }
}

//Method to delete tree List Items
function DeleteSystemTreeItems(selectedTreeviewItemId) {

    var data = JSON.stringify({ 'listId': selectedTreeviewItemId, 'userId': $('#hdnUserId').val() });
    _serviceUrl = _baseURL + 'Services/ListValues.svc/ListValuesNodesDelete';
    AjaxPost(_serviceUrl, data, eval(DeleteListTreeItemSuccess), eval(DeleteListTreeItemFail));
}

//Delete Tree List Items Success method
function DeleteListTreeItemSuccess(result) {
    var errorMessage = '';
    var errorMessageType = 'jSuccess';
    if (result.d == "") {

        errorMessage = _selectedTreeviewItemName +' List deleted.';
        NotificationMessage(errorMessage, errorMessageType, true, 3000);
      
        LoadSystemTreeView(_listValueId);
    } else {

        alert(result.d);
    }
}

//service failed
function DeleteListTreeItemFail() {
    alert('Service Failed.');
}

//inline editing for System List items
function SystemTreeviewItemEdit() {

    $('.jstree').live('dblclick', function () {
        if (_selectedTreeviewItemName != TrimString(_rootNode)) {
            $("#systemTreeviewList").jstree("rename");

            // Hide the Add New/InActivate/Delete buttons
            $('#btnDeleteSystemTreeItem').css('display', 'none');
            $('#btnInActivateListItem').css('display', 'none');
            $('#btnNewSystemTreeItem').css('display', 'none');
        }
    });
}

//to get System List Grid count.
function SystemListGridCount() {

    var data = '';
    _serviceUrl = _baseURL + 'Services/ListValues.svc/SystemlistCount';
    Ajax(_serviceUrl, data, eval(SystemlistCountSuccess), eval(SystemlistCountFail), 'SystemlistCount');
}

//Added for grid default size
function SystemlistCountSuccess(result) {
    var jsonData = jQuery.parseJSON(result.d);
    var systemGridCount = parseInt(jsonData);

    if (systemGridCount <= 20) {
        $('#SystemListGrid').setGridParam({ rowNum: systemGridCount });
    }
    else {
        $('#SystemListGrid').setGridParam({ rowNum: 20 });
        $('#SystemListGrid').setGridParam({ rowList: [20, 40] }).trigger("reloadGrid");;
    }

    //Bind the System List Grid
    BindSystemListGrid();

}

//service failed
function SystemlistCountFail() {
    alert("Error in loading data.");
}



