﻿//Methods to Create/Edit Reports -Event's Report/User's Report/System Report by admin

//Global Variables
var _data = '';
var _userSearch = '';

$(document).ready(function () {
    
    //Page Load method
    PageLoadAction();

    //open Report Template Page
    $('#lnkCreateNewReport').click(function () {
        var url = _baseURL + 'CustomReports/Create/Admin/Report-5/0';//for admin eventId will be 0.

        OpenReportTemplatePage(url);
    });


    //Created By search option 
    $("#txtReportSearch").keyup(function () {

        ReportAutoSearchList();
    });

    $('#txtReportSearch').keypress(function (ev) {
        if (ev.which === 13) {
           
            ev.preventDefault();
            ReportAutoSearchList();
        }
    });
   
});

//Page Load Methods
function PageLoadAction()
{
    //bind grid 
    ReportDataBindToGrid();
  //  GetGridColumnSortOrder('reportGrid', 'CustomReport');
}

// Reload the jqgrid data
function ReloadReportListGrid() {
    $("#reportGrid").trigger("reloadGrid");
}

//Function to create grid and bind data
function ReportDataBindToGrid()
{
    var $grid = $("#reportGrid"), gridcol;
    jQuery("#reportGrid").jqGrid({
        datatype: function (pdata) { ReportData(pdata); },
        colNames: ['', 'Report Name', 'Created By', 'Template Type', 'Edit Report', 'View Report'], //define column names
        colModel: [{ name: 'EventId', index: 'EventId', width: 1, align: 'center', editable: false, hidden: true, resizable: true },
                    { name: 'ReportName', index: 'ReportName', width: 6, align: 'left', sortable: true, formatter: TextFormatter, classes: 'notranslate', resizable: true },
                    { name: 'CreatedBy', index: 'CreatedBy', width: 3, align: 'center', sortable: true,  classes: 'notranslate', resizable: true },
                    { name: 'ReportType', index: 'ReportType', width: 3, align: 'center', sortable: true, resizable: true },
                    { name: 'options', index: 'options', width: 3, align: 'center', editable: false, sortable: false, resizable: true, classes: "HandCursor" },
                    { name: 'act', index: 'act', width: 3, align: 'center', editable: false, sortable: false, resizable: true, classes: "HandCursor"}
                   
        ],
        toppager: false,
        emptyrecords: 'There are no Reports Data.',
        // Grid total width and height         
        height: '100%',
        width:'100%',
        caption: 'Report Templates',
        rowNum: 50,
        pager: jQuery('#pageNavigation'),
        viewrecords: true,
        hidegrid: false,
        sortname: 'CreatedDate',
        sortorder: "desc",
        forceFit: true,
        loadComplete: function () {
            jqgridCreatePager('pageNavigation', 'reportGrid', 5);
        },
        //add Edit View Icons
        gridComplete: EditViewReport,
        autowidth: true,

        //Edit or View functionality
        afterInsertRow: ViewEditInRowGrid,

        ondblClickRow: function (rowId) {
            var rowData = jQuery(this).getRowData(rowId);
            var myCellData = rowData["ReportType"];
            var reportType = ((myCellData == 'Event') ? '1' : (myCellData == 'User') ? '2' : '3');

            var url = _baseURL + 'CustomReports/Edit/Admin/' + reportType + '-5/' + rowId;
            OpenReportTemplatePage(url);
            
        },


        
    });

    jQuery("#reportGrid").click(function (e) {

        var viewReportTitle = 'View Report';
        var el = e.target;
        if (el.title != viewReportTitle) {

            var grid = jQuery('#reportGrid');
            var row = $(el, this.rows).closest("tr.jqgrow");
            var myCellData = grid.jqGrid('getCell', row[0].id, 'ReportType');
            if ($(el).index() == 0 || $(el).index() == 1) {

                var reportType = ((myCellData == 'Event') ? '1' : (myCellData == 'User') ? '2' : '3');

                var url = _baseURL + 'CustomReports/Edit/Admin/' + reportType + '-5/' + row[0].id;
                OpenReportTemplatePage(url);

            }
        }
    });


    $(window).bind('resize', function () {
        $("#reportGrid").setGridWidth($('.NotificationListGrid').width() - 15);
    });
}

function TextFormatter(cellvalue, options, rowObject) {
    var result = "<a href='' onclick='return false'>" + cellvalue + "</a>";
    return result;
}

//Edit and View Report link in Grid Rows
function EditViewReport() {

    var ids = jQuery("#reportGrid").getDataIDs();

    for (var i = 0; i < ids.length; i++) {

        //Edit Icon
        var editReportLink = "<img  id='edit" + ids[i] + "' style='margin-top:4px;' src='"+_baseURL + "Scripts/jquery/images/edit-list-icon.png' title='Edit Report'  />";
        jQuery("#reportGrid").setRowData(ids[i], { options: editReportLink });

        //View Icon
        var viewReportLink = "<img  id='view" + ids[i] + "' style='margin-top:4px;' src='" + _baseURL + "images/View.png' title='View Report' />";
        jQuery("#reportGrid").jqGrid('setRowData', ids[i], { act: viewReportLink });
      
    }
}

function ViewEditInRowGrid(rowId, data)
{
   //edit
    $("td:eq(4)", "#" + rowId).click(function () {
      
        var reportType = ((data.ReportType == 'Event') ? '1' : (data.ReportType == 'User') ? '2' : '3');

        var url = _baseURL + 'CustomReports/Edit/Admin/' + reportType + '-5/' + rowId;

        OpenReportTemplatePage(url);

    });

    //view
    $("td:eq(5)", "#" + rowId).click(function () {
   
        //preview Report - if isTempReport is false then open generate report page.    
        var url = _baseURL + 'CustomReportsData/Reports/' + data.EventId + '/' + rowId +  '/' + '3/' + false;

        newwindow = window.open(url, "_blank", 'name', 'directories=no, status=no,width=1200, height=750,top=250,left=120,scrollbars=1');
        if (window.focus) { newwindow.focus() }
      
    });
}


function ReportData(pData)
{
    
    var reportDetails = new ReportDataDetails(pData);
    _data = JSON.stringify(reportDetails);
    serviceUrl = _baseURL + 'Services/CustomReport.svc/GetAllReportsData';
    AjaxPost(serviceUrl, _data, eval(GetAllReportsDataSuccess), eval(GetAllReportsDataFail));
}

//parameter-CompanyId and User Id
function ReportDataDetails(pData) {

    this.page = pData.page;
    this.rows = pData.rows;
    this.sortIndex = pData.sidx;
    this.sortOrder = pData.sord;
    this.userID = GetUserId();
    this.companyID = GetCompanyId();

    this.userSearch = ((_userSearch != '') ? _userSearch : 'false');
}

//Report Data success method
function GetAllReportsDataSuccess(result)
{
    var gridId = jQuery("#reportGrid")[0];
    var jsonResult = JSON.parse(result.d);

    if (result.d != "") {
        gridId.addJSONData(jsonResult);
       
    }
    else {
        var message = 'There are no Reports';
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
    }
    jqgridCreatePager('pageNavigation', 'reportGrid', 5);
}

//service fail method
function GetAllReportsDataFail(msg)
{
    alert('Service Failed.');
}

//Open Report Template page 
function OpenReportTemplatePage(url)
{

    newwindow = window.open(url, 'name', 'directories=no, status=no, menubar=no, scrollbars=yes, resizable=0,height=750,width=1200,top=250,left=120');
    if (window.focus) { newwindow.focus() }

}


//get Event ID from url
function GetParameterFromUrl() {

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    _module = splitUrl[splitUrl.length - 3];
    _eventId = splitUrl[splitUrl.length - 1];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }

}

//Search functionality
function ReportAutoSearchList()
{
     _userSearch= $('#txtReportSearch').val();
    ReportDataBindToGrid();
    ReloadGridData();
}

// Reload Grid
function ReloadGridData() {

    jQuery("#reportGrid").trigger("reloadGrid");
}
