﻿//get all Report Names depends on Company Id and display in link
var _deepa = '';
function DisplayReportTitle() {
    
    var data = {};
    data.companyID = GetCompanyId();
    data.userID = _userId;
    data.eventId = _eventId;
    data = JSON.stringify(data);

    _serviceUrl = _baseURL + 'Services/CustomReport.svc/GetReportTitleNames';
    AjaxPost(_serviceUrl, data, eval(GetReportTitleNamesSuccess), eval(GetReportTitleNamesFail));
}

//GetReportTitleNames success method
function GetReportTitleNamesSuccess(result) {
 
    var systemReport = 'System';
    var eventReport = 'Event';
    var userReport = 'User';
    var jsonData = jQuery.parseJSON(result.d);
    // var divValues = document.getElementById('myReportContent');
    var divValues = document.getElementById('myReportContent');
    _deepa = divValues;
    if (jsonData.length != 0) {

        $('#myReportContent').empty();

        for (var i = 0; i < jsonData.length; i++) {

            var mainDiv = document.createElement('div');
            var url = _baseURL + 'CustomReportsData/Reports/' + _eventId + '/' + jsonData[i].ReportId + '/' + '1/' + false;

            var divTagElement = document.createElement('div');
            var divTagEditElement = document.createElement('div');
            var divTagDeleteElement = document.createElement('div');

            var anchortag = document.createElement('a');
            anchortag.onclick = 'return OpenReportBrowsers(' + url + ')';
            anchortag.setAttribute('href', "javascript:OpenReportBrowsers('" + url + "')");
            anchortag.className = 'anchorheader';
            divTagElement.setAttribute('style', 'float:left;padding-left:30px;');

            var reportType = jsonData[i].ReportType;

            anchortag.innerHTML = jsonData[i].ReportName;

            _reportName = jsonData[i].ReportName;

            var eventType = ((jsonData[i].ReportType == eventReport) ? '1' : (jsonData[i].ReportType == userReport) ? '2' : '3');

            if (reportType != systemReport) {
                var editReportLink = "<img onclick='javascript:OpenReportTemplatePage(" + jsonData[i].ReportId + "," + eventType + ");'  id='edit" + jsonData[i].ReportId + "' class='editImage'  src='../../../Images/edit.gif' title='Edit Report'  />";
                divTagEditElement.innerHTML = editReportLink;
                
                var deleteReportLink = "<img onclick='javascript:DeleteEventReport(" + jsonData[i].ReportId + ",1,this);' reportName='" + jsonData[i].ReportName + "'  id='imgReportDelete" + jsonData[i].ReportId + "' class='editImage'  src='../../../Images/delete.png' title='Delete Report'  />";
                divTagDeleteElement.innerHTML = deleteReportLink;
            }
            divTagElement.appendChild(anchortag);

            mainDiv.setAttribute('style', 'border:0px solid transparent;');
            mainDiv.appendChild(divTagElement);
            mainDiv.appendChild(divTagEditElement);
            mainDiv.appendChild(divTagDeleteElement);
            mainDiv.className = 'clearBoth';

            divValues.appendChild(mainDiv);
        }

        if (divValues.innerHTML.trim() == '') {
            $('#divMyReportSubTitle')[0].innerHTML = "";
            $('#divMyReportSubTitle').hide();
            $('#myReportContent').hide();
        }

    }
    else {
        $('#myReportContent').empty();
        $('#myReportContent')[0].setAttribute('style', 'text-decoration:none')
        $('#myReportContent')[0].innerHTML = "You have no Reports";
    }
}

//service fail method -
function GetReportTitleNamesFail() {
    alert('Service method failed.');
}
