﻿//Contain all common functions that are being used by multiple corrcteive action related pages

//Used to check if the page is editcap or cap tab page

//Global Variable

//Used to hold the current task id value
var _currentTaskID = 1;
var _hidingTextPosition = 250;


var _isCAPPage = false;

//Used to check if the cap created from smartcap page or general cap page
var _isSmartCAP = false;
var _isNormalCAP = false;

var _rootCauses = '';
var _genericCauses = '';


$("img[id^='imgCF']").hide();
$("div[name^='divRCTDictionary']").hide();
$("a[id^='lnkRCTDictionary']").hide();
$("div[id^='divCFAnalyze']").hide();
$("img[id^='imgRCT']").hide();
$("img[id^='imgRCT']").hide();
$('.rctNode').hide();
$('.genericNode').hide();

var _SMARTER = 'SMARTER';
var _subTabId = '';
var _capEventId = '';

function SetSubTabID() {
    
    if (IsCreatePage()) {
     // var splittedURL = document.referrer.split('/');
     var splittedURL = window.location.href.split('/');
     _subTabId = splittedURL[splittedURL.length - 2].split('-')[1];
    _capEventId = splittedURL[splittedURL.length - 1]; 
    _eventId = _capEventId;
    }
}

//Populate causal factors
function PopulateCausalFactors(isCAPPage, isNormalCAP, isSmartCAP) {
    
    _isCAPPage = isCAPPage;
    _isNormalCAP = isNormalCAP;
    _isSmartCAP = isSmartCAP;

    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetCausalFactorDetails';
    var data = {};
    var splittedURL = window.location.href.split('/');
 
    data.eventID = (isCAPPage) ? splittedURL[splittedURL.length - 2] : splittedURL[splittedURL.length - 1];
    data.userId = _userId;

    PostAjax(serviceURL, JSON.stringify(data), eval(LoadCausalFactorSuccess), eval(LoadCausalFactorFail));

}

//On success of loading causal factors
function LoadCausalFactorSuccess(data) {
       
    //make template empty
    $("#rctTemplateResults").empty();
    
    //if (data.d != null) {
    if (data.d != "null") {    

        $("#lblNone").hide();

        //parse the json result to object
        data = ParseToJSON(data.d);

        //set the data returned to the template
        $("#rctTemplate").tmpl(data).appendTo("#rctTemplateResults");

        SetControlsBasedOnPage();


    }
    else {

        $("#lblNone").show();
    }

    if (SetTextByGoogleTranslater()) {
        $('.cfRootCuaseTitleText').removeClass('notranslate');
    }
    else {
        $('.cfRootCuaseTitleText').addClass('notranslate');
    }
}


function SetControlsBasedOnPage() {
    
    var splittedURL = window.location.href.split('/');
    var pageName = splittedURL[splittedURL.length - 2];

    //fix -> display image at rct
    if (pageName.indexOf("Fix-") != -1) {        
        VisibleOrInvisibleControls(false,  true, false);

    }
    else if (pageName.indexOf("TapRooT-") != -1) { //taproot -> image at cf and the link
        VisibleOrInvisibleControls(true,  false, false);

    }
    else {//ca -> only check boxes
        VisibleOrInvisibleControls(false,  false, true);
    }
}


function VisibleOrInvisibleControls(taprootControlsVisibility, fixRCTImageVisibility, capRCTCheckBoxVisibility) {

    //Set visisbility of image beside causal factor item in taproot page    
    if (taprootControlsVisibility) {
        $("img[id^='imgCF']").show();
    }

    //Set visisbility of link below causal factor item in taproot page
    if (taprootControlsVisibility) {
        $("div[id^='divCFAnalyze']").show();
    }

    //Set visisbility of image beside rct node item in fix page
    if (fixRCTImageVisibility) {
        $("img[id^='imgRCT']").show();
    }

    if (fixRCTImageVisibility) {
        $("img[id^='imgGenericRCT']").show();
    }

    //Set visisbility of checkboxes beside rct node item in ca edit and smart ca page
    if (capRCTCheckBoxVisibility) {
        $('.rctNode').show();
        $('.genericNode').show();
    }

    //Set visisbility of checkboxes beside rct node item in ca edit and smart ca page
    if (capRCTCheckBoxVisibility) {
        
        $("a[id^='lnkCAPDictionary']").show();
    }

}



//Page load
function LoadCAPDetails() {

    $('#spnIdentifier').html('');
    var capID = GetCorrcetiveActionID();
    //If edit page then get data by corrective action id and populate data in fields
    if (capID != 0) {

        
        $('#btnSaveCAPandRedirectSmart').show();//NOW!

        $("#btnSaveCAP").val('Apply Changes');
        var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetCorrectiveActionDetails';
        var data = {};
        data.correctiveActionID = capID;
        PostAjax(serviceURL, JSON.stringify(data), eval(LoadCAPDetailsSuccess), eval(LoadCAPDetailsFail));

    }

}


//Get corrective action id
function GetCorrcetiveActionID() {
    var urlSplit = window.location.href.split('?');
    var splittedURL = (urlSplit.length > 1) ? urlSplit[urlSplit.length - 2].split('/') : urlSplit[urlSplit.length - 1].split('/');
   
    var capID = splittedURL[splittedURL.length - 1];
    capID = (capID.indexOf("#") >= 0) ? capID.split('#')[0] : capID;
    return capID;
}

function BindCommonFieldsAndEvents(isCreatePage, isSmartPage) {

    if (isCreatePage) {
        $('#btnSaveCAP').val('Create');
        $("#divTaskHeader").hide()
        $('#btnSaveCAPandRedirectSmart').hide();//NOW!
    }
    else {
        $('#btnSaveCAPandRedirectSmart').show();//NOW!
        $('#btnSaveCAP').val('Apply Changes');
        $("#divTaskHeader").show()
    }

    $('#divEditActionPlanDialog').bind('dialogclose', function (event) {
    });

    //open Task pop up
    $('#lnkCreateTask').click(function () {

        //Clear all fields
        ClearTaskFields();

        OpenTaskPopup(0);
    });

    //close task pop up
    $('#btnCloseTask').click(function () {
        //fix for unsaved changes .
        _isDirty = false;

        CloseTaskPopUp();
    });

    //Save task
    $('#btnSaveTask').click(function () {
        SaveTask();
    });

    //Close corrective action po up
    $('#btnCancel').click(function () {
        CancelCAP();
    });

    //Save corrective actions
    $('#btnSaveCAP').click(function () {
        //parameter getting passed to recognize if the cap is created as smart or not
        SaveCAP(isSmartPage); 
    });

    $('#btnSaveCAPandRedirectSmart').click(function () {
        //parameter getting passed to recognize if the cap is created as smart or not
        
        SaveCAP(isSmartPage);
  
    
        var url = jQuery(location).attr('href');
        var splitUrl = url.split('/');

      
        var output = url.substring(0, url.lastIndexOf('/') + 1);
       
        var url = output + '0';
        window.location = url;
       
    });

   
    //$('#txtDueDate').datepicker({ dateFormat: sessionStorage.dateFormat });
    $('#txtDueDate').datepicker({ dateFormat: localStorage.dateFormat });
    $("#ui-datepicker-div").addClass("notranslate");
}


//On fail of loading data for cap  page
function LoadCAPDetailsFail(data) {
    
}


//On success of loading data for cap  page
function LoadCAPDetailsSuccess(data) {    
    
   
    //Get result data
    data = ParseToJSON(data.d);
    
    _subTabId = data[0].SubTabId; //Used for navigating to Action plan tab page

    $('#divTitle').html(data[0].Identifier);

    //Set data to corresponding field
    $('#txtIdentifier').val(data[0].Identifier);
    $('#txtEventDescription').val(data[0].Description);
    $('#txtBusinessCase').val(data[0].BusinessCase);
    $('#txtReviewNotes').val(data[0].ReviewNotes);
    $('#txtTempAction').val(data[0].TemporaryActions);
    
    if (data[0].Tasks[1] != undefined && $('#spnMPersonResponsible')[0]!=undefined)
        $('#spnMPersonResponsible')[0].innerHTML = data[0].Tasks[1].ResponsiblePerson;

    
    $('#txtMInitialDueDate').val((data[0].CorrectiveAction.M_InitialDueDate != null && data[0].CorrectiveAction.M_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.M_InitialDueDate)) : '');

    $('#txtVerificationPlan').val(data[0].CorrectiveAction.M_VerificationPlan);
    if (data[0].Tasks[0] != undefined && $('#spnAPersonResponsible')[0] != undefined)
        $('#spnAPersonResponsible')[0].innerHTML = data[0].Tasks[0].ResponsiblePerson;

    $('#txtResource').val(data[0].CorrectiveAction.A_ResourceNeeded);

    $('#txtTimelyDueDate').val((data[0].CorrectiveAction.T_InitialDueDate != null && data[0].CorrectiveAction.T_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.T_InitialDueDate)) : '');

    if (data[0].Tasks[2] != undefined && $('#spnMPersonResponsible')[0]!=undefined)
        $('#spnEPersonResponsible')[0].innerHTML = data[0].Tasks[2].ResponsiblePerson;

    $('#txtEffectiveDueDate').val((data[0].CorrectiveAction.E_InitialDueDate != null && data[0].CorrectiveAction.E_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.E_InitialDueDate)) : '');

    $('#txtValidationPlan').val(data[0].CorrectiveAction.E_ValidationPlan);

    $("#taskTemplateResults").empty();

    // Begin Task code changes 
    // Code for setting the Completed/In Completed/Past Due status in the data[0].Tasks 
    // These new values will be used for displaying correct icon next to the information in Tasks
    var todayDate = new Date();
    todayDate.setHours(0, 0, 0, 0);
    var taskDate;
    
    for (var i = 0; i < data[0].Tasks.length; i++) {
        if(!data[0].Tasks[i].TaskStatus)
        {
            taskDate = new Date(ConvertToDate(data[0].Tasks[i].TaskDueDate));
            taskDate.setHours(0, 0, 0, 0);
            if (todayDate > taskDate)
            {
                data[0].Tasks[i].TaskStatus = "PastDue";
            }
            else
                data[0].Tasks[i].TaskStatus = "InCompleted";
        }
    }
    // End Task code changes 
    
    $("#taskTemplate").tmpl(data[0]).appendTo("#taskTemplateResults");



    //Implementation for read more link
    $('.bio').each(function () {

        var text = $(this).html();

        if (text.length > _hidingTextPosition) {

            $(this).html(text.slice(0, _hidingTextPosition) +
                        '<span class="more">... <a class="expand" href="javascript:void(0)">read more</a></span><span class="hidden">' + text.slice(10) + '</span>');

        }

    });


    //On expand click show the hidden text
    $('.expand').on('click', function (event) {
        $(event.target)
            .closest('label')
            .find('.hidden')
                .removeClass('hidden')
                .end()
            .find('.more')
                .remove();
    });

    //Check all rct node selected
    CheckSelectedRCTNodes(data[0].RootCauses);

    CheckSelectedGenericCause(data[0].GenericCauses);

}

//Close  the  correctiveAction pop up
function CloseCapPopUp() {
        
    if (parent.$("#hdnStatusValue").val() == 'true') {

        var identifierName = $('#txtIdentifier').val();
        var message = (identifierName != null && identifierName.length > 0) ? identifierName + " saved successfully." : "Data saved successfully.";
        parent.NotificationMessage(message, 'jSuccess', true, 1000);
        parent.$("#hdnStatusValue").val('false');
        if (parent.$('#divTabCorrectiveAction').length > 0) {

            parent.PopulateCorrectiveActions();
        }

    }

  
    var id = parent.$('.ui-dialog');
    parent.$('.ui-icon-closethick').click();
    id.bind("dialogclose", function (event, ui) {
    });
   

    return false;
}



function CheckSelectedGenericCause(genericNodes) {
    //Implementation for read more link
    $('.genericNode').each(function () {

        var nodeValue = parseInt($(this).val());

        if (NodeExistsForGeneric(genericNodes, nodeValue)) {
            $(this).attr('checked', true);
        }

    });

}

//Iterate through the array returned and checked the corresponding node check box
function CheckSelectedRCTNodes(rctNodes) {
    //Implementation for read more link
    $('.rctNode').each(function () {

        var nodeValue = parseInt($(this).val());

        if (NodeExists(rctNodes, nodeValue)) {
            $(this).attr('checked', true);
        }

    });
}


//Check if the node value is present in the array of selected rct nodes or not
function NodeExistsForGeneric(a, obj) {

    var i = a.length;
    while (i--) {
        if (a[i].GenericCauseID === obj) {
            return true;
        }
    }

    return false;
}

//Check if the node value is present in the array of selected rct nodes or not
function NodeExists(a, obj) {

    var i = a.length;
    while (i--) {
        if (a[i].RCTID === obj) {
            return true;
        }
    }

    return false;
}



//On fail of causal factor
function LoadCausalFactorFail(data) {
}


//Save the modified rct node ids
function SaveRCTModifiedValue(checkBox) {
    
    var prevValue = (!checkBox.checked);
    var rctNodeValue = $(checkBox).val();
    var rctNode = ',' + rctNodeValue + ':' + prevValue;

    if (_rootCauses.indexOf(rctNode) >= 0) {
        _rootCauses = _rootCauses.replace(rctNode, '');
    }

    _rootCauses = _rootCauses + "," + rctNodeValue + ':' + String(checkBox.checked);

}




//Save the modified rct node ids
function SaveGenericModifiedValue(checkBox) {
    
    var prevValue = (!checkBox.checked);
    var genericNodeValue = $(checkBox).val();
    var genericNode = ',' + genericNodeValue + ':' + prevValue;

    if (_genericCauses.indexOf(genericNode) >= 0) {
        _genericCauses = _genericCauses.replace(genericNode, '');
    }

    _genericCauses = _genericCauses + "," + genericNodeValue + ':' + String(checkBox.checked);

}


//Save corrective action
function SaveCAP(isSmartCA) {
    _isSmartCA = isSmartCA;

    if (ValidateForm()) {
        var formData = GetDetailsFromPage();
        formData = JSON.stringify(formData);

        var serviceUrl = '/TFSTaprootSVC/';
        serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/SaveCorrectiveAction';
        AjaxPost(serviceUrl, formData, eval(SaveCAPSuccess), eval(SaveCAPFail));
        document.getElementById("btnSaveCAP").disabled = true;
    }
}

function IsCreatePage() {
    return ($('#btnSaveCAP').val() == 'Create');
}

//On success of save cap
function SaveCAPSuccess() {    
    window.parent.$("#hdnStatusValue").val(true);
    _isDirty = false;
    document.getElementById("btnSaveCAP").disabled = false;
    
    if (IsCreatePage()) {
        if (_isSmartCA) {
            RedirectToFixTab();
            return;
        }

        window.location = localStorage.url;
    }
    else {       
    }
}


function RedirectToFixTab() {
    var url = window.location.href;
    var length = url.split('/').length;
    var splittedUrl = url.split('/');
    var fixSubTabId = splittedUrl[length - 4].split('-')[1];
    var capEventId = splittedUrl[length - 2];

    window.location = _baseURL + 'Event/Investigation/Fix-' + fixSubTabId + '/' + capEventId;
}


//On fail of save cap
function SaveCAPFail() {
}


function CancelCAP() {    
    if (IsCreatePage())
    {
        if (_isSmartCAP) {
            RedirectToFixTab();
            return;
        }

        // window.location = _baseURL + 'Event/ActionPlan/CorrectiveActions-' + _subTabId + '/' + _capEventId;
        RedirectToFixTab()
    }
    else {

        //var urlSplit = window.location.href.split('?');
        //var splittedURL = (urlSplit.length > 1) ? urlSplit[urlSplit.length - 2].split('/') : urlSplit[urlSplit.length - 1].split('/');
        //var eventId = splittedURL[splittedURL.length - 2];
       // window.location = _baseURL + 'Event/ActionPlan/CorrectiveActions-' + _subTabId + '/' + eventId;
        RedirectToFixTab();
    }
    
}

//Validate the form
function ValidateForm() {   
   
    if ($('#txtIdentifier').val() == '') {
        ShowHideWarningMessage('txtIdentifier', 'Please specify Identifier', true);
        parent.NotificationMessage('Please enter Identifier.', 'jError', false, 3000);
        return false;
    }
   
    return true;
}


//Get all details from the cap page
function GetDetailsFromPage() {
   
    var capData = {};
    var urlSplit = window.location.href.split('?');
    var splittedURL = (urlSplit.length > 1) ? urlSplit[urlSplit.length - 2].split('/') : urlSplit[urlSplit.length - 1].split('/');

    capData.eventID = splittedURL[splittedURL.length - 2];
    capData.correctiveActionID = splittedURL[splittedURL.length - 1]; 
    capData.correctiveActionID = (capData.correctiveActionID.indexOf("#") >= 0) ? capData.correctiveActionID.split('#')[0] : capData.correctiveActionID;
    capData.isSmarter = _isSmartCA;
    capData.identifier = $('#txtIdentifier').val();
    capData.description = $('#txtEventDescription').val();
    capData.businessCase = $('#txtBusinessCase').val();
    capData.reviewNotes = $('#txtReviewNotes').val();
    capData.temporaryActions = $('#txtTempAction').val();
    capData.mResponsiblePersonID = (_isSmartCA) ? ($('#spnMPersonResponsible')[0].innerHTML == '') ? null : 0 : null;
    capData.mInitialDueDate = (_isSmartCA) ? $('#txtMInitialDueDate').val() : '';
    capData.mVerificationPlan = (_isSmartCA) ? $('#txtVerificationPlan').val() : '';
    capData.aResponsiblePersonID = (_isSmartCA) ? ($('#spnAPersonResponsible')[0].innerHTML == '') ? null : 0: null;
    capData.aResourceNeeded = (_isSmartCA) ? $('#txtResource').val() : '';
    capData.tInitialDueDate = (_isSmartCA) ? $('#txtTimelyDueDate').val() : '';
    capData.eResponsiblePersonID = (_isSmartCA) ? ($('#spnEPersonResponsible')[0].innerHTML == '') ? null : 0 : null;
    capData.eInitialDueDate = (_isSmartCA) ? $('#txtEffectiveDueDate').val() : '';
    capData.eValidationPlan = (_isSmartCA) ? $('#txtValidationPlan').val() : '';
       
    capData.rootCausesSelected = _rootCauses.substr(1, _rootCauses.length - 1);

    capData.genericCausesSelected = _genericCauses.substr(1, _genericCauses.length - 1);

    //for responsible Person table insertion
    capData.tasksResponsiblePersonId = 0;
    capData.companyId = $('#hdnCompanyId').val();
    capData.userId = $('#hdnUserId').val();
    
    capData.mResponsiblePersonName = (_isSmartCA) ? ($('#spnMPersonResponsible')[0].innerHTML == '') ? null : $('#spnMPersonResponsible')[0].innerHTML : null;
    if (capData.mResponsiblePersonName != null && capData.mResponsiblePersonName.indexOf('(') != -1)
    {
        capData.mResponsiblePersonName = capData.mResponsiblePersonName.substring(0, capData.mResponsiblePersonName.indexOf('(') - 1);
    }
    capData.aResponsiblePersonName = (_isSmartCA) ? ($('#spnAPersonResponsible')[0].innerHTML == '') ? null : $('#spnAPersonResponsible')[0].innerHTML : null;
    if (capData.aResponsiblePersonName != null && capData.aResponsiblePersonName.indexOf('(') != -1) {
        capData.aResponsiblePersonName = capData.aResponsiblePersonName.substring(0, capData.aResponsiblePersonName.indexOf('(') - 1);
    }
    capData.eResponsiblePersonName = (_isSmartCA) ? ($('#spnEPersonResponsible')[0].innerHTML == '') ? null : $('#spnEPersonResponsible')[0].innerHTML : null;
    if (capData.eResponsiblePersonName != null && capData.eResponsiblePersonName.indexOf('(') != -1) {
        capData.eResponsiblePersonName = capData.eResponsiblePersonName.substring(0, capData.eResponsiblePersonName.indexOf('(') - 1);
    }

    capData.isManualMPersonResponsible= _isManualMPersonResponsible;
    capData.isManualEPersonResponsible = _isManualEPersonResponsible;
    capData.isManualAPersonResponsible = _isManualAPersonResponsible;
    
    return capData;
}


//**************************************** TASK RELATED OPERATIONS ************************

//Load task details, task types 
function PopulateTaskPage() {   
    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetAllTaskTypes';
    AjaxPost(serviceURL, null, eval(OnSuccessPopulateTaskTypes), eval(OnFailPopulateTaskTypes));

}


//On success of populate task types
function OnSuccessPopulateTaskTypes(result) {
    
    $('#ddlTaskType').empty();
    
    if (result != null) {

        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        $('#ddlTaskType').append($('<option></option>').val('').html('<< Select >>'));

        for (var i = 0; i < jsonResponse.length; i++) {
            $("#ddlTaskType").append(
                $("<option></option>").attr("value", jsonResponse[i]["TaskTypeID"]).attr("title", jsonResponse[i]["TaskTypeName"]).text(jsonResponse[i]["TaskTypeName"]));
        }

        $("#ddlTaskType option:first").attr('selected', 'selected');
    }

    PopulateTaskPerson(_baseURL);
}


//On fail of populate task types
function OnFailPopulateTaskTypes() {
}


//close pop up
function CloseTaskPopUp() {

    $("#dialog:ui-dialog").dialog("destroy");
    $('#divAddEditTaskDialog').dialog("destroy");
    $('#divAddEditTaskDialog').bind("dialogclose", function (event, ui) {
    });

}


//open Task pop up
function OpenTaskPopup(taskID) {
    
    if ($('#lbl_tasktypeName').length > 0) {
        $('#lbl_tasktypeName').remove();
        $('#ddlTaskType').show();
    }
    
    $('#chkIsCompleted').attr('checked', false);
    $('#divAddEditTaskDialog').removeClass('seasonContentHide');
    $('#divAddEditTaskDialog').addClass('seasonContentShow');

    $("#dialog:ui-dialog").dialog("destroy");


    var modalHeight = (taskID == 0) ? 340 : 440;

    if (taskID > 0) {
        $('#btnSaveTask').val('Apply Changes');

    }
    else {

        $('#btnSaveTask').val('Create');
        OpenTaskPopupDialog(modalHeight);
    } 

    _currentTaskID = taskID; //Set the current task id
     PopulateTaskPage(); //Load page for task pop up by task id

}

//Populate responsible person
function PopulateTaskPerson(_baseURL) {
   
    $('#spnTaskResponsiblePerson')[0].innerHTML = $('#hdnUserName').val();

    //If called from the corrcetive action page then call method to populate task related details
    PopulateTaskDetail();
}


//Populate task pop up with the details for the current task
function PopulateTaskDetail() {
    
    if (_currentTaskID != 0) {
        
        $("#divNotes").show();
        $("#divIsCompleted").show();
        var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetTaskDetailsByID';
        var data = {};
        data.taskID = _currentTaskID;
        AjaxPost(serviceURL, JSON.stringify(data), eval(LoadTaskSuccess), eval(LoadTaskFail));

    }
    else {

        //Clear all fields
        ClearTaskFields();
        $("#divNotes").hide();
        $("#divIsCompleted").hide();
    }

}


//On success of Populate Task details
function LoadTaskSuccess(data) {
    
     var taskData = ParseToJSON(data.d)[0];    
     $('#ddlTaskType').val(taskData.TaskTypeID);
    
    //Replace tasktype dropdown with a label
     _tasktypeId = taskData.TaskTypeID;
     
     $('#divTask table tr>td:first').after(function () {
         return $('<label id=lbl_tasktypeName style="margin-left:20px;"></label>').text($('#ddlTaskType').children("option")[_tasktypeId].text);
     });
    
     $('#ddlTaskType').hide();
    /////////////////////////////////////////////
    
    
    if (taskData.ResponsiblePersonID != null) {
        $('#spnTaskResponsiblePerson')[0].innerHTML= taskData.ResponsiblePersonID;
       
    }
    $('#txtDescription').val(taskData.Description);
    $('#txtDueDate').val(ConvertToDate(taskData.DueDate));
    $('#txtTaskNotes').val(taskData.Notes);
    $('#chkIsCompleted').attr('checked', taskData.IsCompleted);

    OpenTaskPopupDialog(440)
   
}


//On fail of Populate Task details
function LoadTaskFail(data) {
}

function OpenTaskPopupDialog(modalHeight)
{
    $('#divAddEditTaskDialog').dialog({

        height: modalHeight,
        width: 478,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 100]

    });
}


//Clear all task fields
function ClearTaskFields() {
  
    $('#ddlTaskType').val('');
    $('#txtDescription').val('');
    $('#txtDueDate').val('');
    $('#txtTaskNotes').val('');

}


//Save task for a corrective action
function SaveTask() {
    if (ValidateTaskForm()) {

        var formData = GetTaskDetailsFromPage();
        formData = JSON.stringify(formData);
        
        serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/SaveTask';
        AjaxPost(serviceUrl, formData, eval(SaveTaskSuccess), eval(SaveTaskFail));

    }
}


//Validate task form
function ValidateTaskForm() {
    
    var messageText = 'You are missing the fields :';
    var originalMessageLength = messageText.length;

    messageText = ($('#ddlTaskType').val() == '') ? messageText + "Task Type, " : messageText;
    messageText = ($('#spnTaskResponsiblePerson')[0].innerHTML == '') ? messageText + "Responsible Person, " : messageText;
    messageText = ($('#txtDueDate').val() == '') ? messageText + "Due Date, " : messageText;
    messageText = ($('#txtDescription').val() == '') ? messageText + "Description, " : messageText;

    if (messageText.length == originalMessageLength) {
        return true;
    }

    messageText = messageText.substr(0, messageText.length - 2) + '.';
    parent.NotificationMessage(messageText, 'jError', false, 3000);

    return false;
}


//On success of save task
function SaveTaskSuccess(data) {    
   
   
    //Close task pop up
    CloseTaskPopUp();

    //Load details for the cap page
    LoadCAPDetails();

    //NotificationMessage('Data saved successfully.', 'jSuccess', false, 1000);
    //NotificationMessage('Task saved successfully.', 'jSuccess', false, 1000);
    //deepa commented
  //  window.parent.PopulateCorrectiveActions();

}


//On fail of save task
function SaveTaskFail(data) {
}


//Get all the data from the task pop up page
function GetTaskDetailsFromPage() {
   
    var taskData = {};
    taskData.taskID = _currentTaskID;// != 0 ? 0 : _currentTaskID;//change the code
    taskData.correctiveActionID = GetCorrcetiveActionID();
    taskData.correctiveActionID = (taskData.correctiveActionID.indexOf("#") >= 0) ? taskData.correctiveActionID.split('#')[0] : taskData.correctiveActionID;
    taskData.taskTypeID = $('#ddlTaskType').val();
    taskData.responsiblePersonID = $('#spnTaskResponsiblePerson')[0].innerHTML;
    taskData.description = $('#txtDescription').val();
    taskData.dueDate = $('#txtDueDate').val();
    taskData.notes = $('#txtTaskNotes').val();
    taskData.isCompleted = $('#chkIsCompleted').attr('checked') == 'checked' ? 1 : 0;
    taskData.companyId=$('#hdnCompanyId').val();
    taskData.userId=$('#hdnUserId').val();
    taskData.isDirectTask = true;
    return taskData;
}

//On fail of Populate responsible person
function OnFailPopulateResponsiblePerson(result) {
}


//Display the jquery pop up
function JqueryPopUp(url, pageTitle, divID) {
    
    $('#' + divID).removeClass('seasonContentHide');
    $('#' + divID).addClass('seasonContentShow');
    $("#dialog:ui-dialog").dialog("destroy");    
    $('#' + divID).load($('#frameLoadPopup').attr('src', url));

    $('#frameLoadPopup').attr('title', pageTitle);

    $('#' + divID).dialog({


        title: pageTitle,
        height: 870,
        width: 910,
        modal: true,
        draggable: true,
        resizable: false,
        scrollbar: false,
        position: ['middle', 200],
        open: function (ev, ui) {
            $('#' + divID).css('overflow', 'hidden');           
        }
   });
      
}

//Convert the json date format to javascript date format 
function ConvertToDate(dateValue) {

    if (dateValue != null) {
        var jsonDate = new Date(parseInt(dateValue.substr(6)));
        var displayDate = $.datepicker.formatDate(localStorage.dateFormat, jsonDate);//change with site date setting
        return displayDate;
    }
   

}

