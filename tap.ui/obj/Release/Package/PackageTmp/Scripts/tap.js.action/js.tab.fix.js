﻿/*Fix Screen Methods (usha)*/

//ready function
$(document).ready(function () {

    EventTypeFromUrlSelect(3, 1);

    //page load method
    PageLoadAction();

    //open popup onclick 
    $('#lnkSmartCap').click(function () {
        OpenSmarterCorcetiveAction();
        return false;
    });

    $("#NextSubTabPage").click(function () {
        NextSubTabClick(location, 2);
    });

    $("#PrevSubTabPage").click(function () {
        PreviousSubTabClick(location, 2);
    });   

    GetTMAccessWithEventId(_eventId, 'divTabFix');
});
//end ready function


//Page load
function PageLoadAction() {

    //Jquery main Tabs
    JqueryTabs();

   //populate CausalFactors
    PopulateCausalFactors(false, false, false);

    //Causal factor accordian
    $("#divFixActionPlanAccordion").accordion({
        collapsible: true,
        autoHeight: false

    });

    //open multiple accordiaon tab
    $("#divFixActionPlanAccordion").click(function () {

        var id = $(this).attr("class");

        var n_id = "#" + id;

        $(n_id).slideUp();

        if ($(this).parent().next().is(':hidden')) {
            $(this).parent().next().slideDown();
        }
        else {
            
            $(this).parent().next().slideUp();
        }

            return false;

    });
}


//open Smart Corrective Action popup
function OpenSmartCorcetiveActionPopup() {

    var url = _baseURL + 'SmartCorrectiveActionPlan/Create' + '/' + _eventId + '/' + 0;
    JqueryPopUp(url, 'Create a Smarter Corrective Action', 'dialog-smartcap');

}

//open Smart Corrective Action popup
function OpenSmarterCorcetiveAction() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var url = _baseURL + splitUrl[splitUrl.length - 4] + '/' + splitUrl[splitUrl.length - 3] + '/' + splitUrl[splitUrl.length - 2] + '/SMARTER/' +  _eventId +'/0';
    window.location = url;

}

function DisplaySuccessMessage() {
    if ($("#hdnStatusValue").val() == 'true') {
        NotificationMessage('Data saved successfully.', 'jSuccess', true, 1000);
    }
}

