﻿//Contain all common functions that are being used by multiple corrcteive action related pages

//Used to check if the page is editcap or cap tab page

//Global Variable

//Used to hold the current task id value
var _currentTaskID = 0;
var _hidingTextPosition = 250;
var _isCAPPage = false;
//Used to check if the cap created from smartcap page or general cap page
var _isSmartCAP = false;
var _isNormalCAP = false;
var _rootCauses = '';
var _genericCauses = '';

var ACCORDION_CSS_FILE_NAME = '<link id="accordionCss" type="text/css" rel="stylesheet" href="' + _baseURL +'tap.ui.css/tap.stl.cus/FixTab.css"/>';


$("img[id^='imgCF']").hide();
$("div[name^='divRCTDictionary']").hide();
$("a[id^='lnkRCTDictionary']").hide();
$("div[id^='divCFAnalyze']").hide();
$("img[id^='imgRCT']").hide();
$("img[id^='imgRCT']").hide();
$('.rctNode').hide();
$('.genericNode').hide();

var _SMARTER = 'SMARTER';


//Populate causal factors
function PopulateCausalFactors(isCAPPage, isNormalCAP, isSmartCAP) {
    
    _isCAPPage = isCAPPage;
    _isNormalCAP = isNormalCAP;
    _isSmartCAP = isSmartCAP;

    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetCausalFactorDetails';
    var data = {};
    var splittedURL = window.location.href.split('/');
    
    data.eventID = (isCAPPage) ? splittedURL[splittedURL.length - 2] : splittedURL[splittedURL.length - 1];

    PostAjax(serviceURL, JSON.stringify(data), eval(LoadCausalFactorSuccess), eval(LoadCausalFactorFail));

}

//On success of loading causal factors
function LoadCausalFactorSuccess(data) {
  
    
    //make template empty
    $("#rctTemplateResults").empty();
    
    //if (data.d != null) {
    if (data.d != "null") {    

        $("#lblNone").hide();

        //parse the json result to object
        data = ParseToJSON(data.d);

        //set the data returned to the template
        $("#rctTemplate").tmpl(data).appendTo("#rctTemplateResults");

        SetControlsBasedOnPage();


    }
    else {

        $("#lblNone").show();
    }

}


function SetControlsBasedOnPage() {
    //
    
    var splittedURL = window.location.href.split('/');
    var pageName = splittedURL[splittedURL.length - 2];

    //fix -> display image at rct
    if (pageName.indexOf("Fix-") != -1) {        
        VisibleOrInvisibleControls(false,  true, false);

    }
    else if (pageName.indexOf("TapRooT-") != -1) { //taproot -> image at cf and the link
        VisibleOrInvisibleControls(true,  false, false);

    }
    else {//ca -> only check boxes
        VisibleOrInvisibleControls(false,  false, true);
    }
}


function VisibleOrInvisibleControls(taprootControlsVisibility, fixRCTImageVisibility, capRCTCheckBoxVisibility) {

    //Set visisbility of image beside causal factor item in taproot page    
    if (taprootControlsVisibility) {
        $("img[id^='imgCF']").show();
    }

    //Set visisbility of link below causal factor item in taproot page
    if (taprootControlsVisibility) {
        $("div[id^='divCFAnalyze']").show();
    }

    //Set visisbility of image beside rct node item in fix page
    if (fixRCTImageVisibility) {
        $("img[id^='imgRCT']").show();
    }

    if (fixRCTImageVisibility) {
        $("img[id^='imgGenericRCT']").show();
    }

    //Set visisbility of checkboxes beside rct node item in ca edit and smart ca page
    if (capRCTCheckBoxVisibility) {
        $('.rctNode').show();
        $('.genericNode').show();
    }

    //Set visisbility of checkboxes beside rct node item in ca edit and smart ca page
    if (capRCTCheckBoxVisibility) {
        //$("a[id^='lnkHelp']").show();
        $("a[id^='lnkCAPDictionary']").show();
    }

}



//Page load
function LoadCAPDetails() {    

    $('#spnIdentifier').html('');

    var capID = GetCorrcetiveActionID();
    //If edit page then get data by corrective action id and populate data in fields
    if (capID != 0) {

        $("#btnSaveCAP").val('Apply Changes');
        var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetCorrectiveActionDetails';
        var data = {};
        data.correctiveActionID = capID;
        PostAjax(serviceURL, JSON.stringify(data), eval(LoadCAPDetailsSuccess), eval(LoadCAPDetailsFail));

    }

}


//Get corrective action id
function GetCorrcetiveActionID() {

    var splittedURL = window.location.href.split('/');
    var capID = splittedURL[splittedURL.length - 1];
    capID = (capID.indexOf("#") >= 0) ? capID.split('#')[0] : capID;
    return capID;
}

function BindCommonFieldsAndEvents(isCreatePage, isSmartPage) {

    if (isCreatePage) {
        $('#btnSaveCAP').val('Create');
        $("#divTaskHeader").hide()
    }
    else {
        $('#btnSaveCAP').val('Apply Changes');
        $("#divTaskHeader").show();
    }

    $('#divEditActionPlanDialog').bind('dialogclose', function (event) {
    });

    //open Task pop up
    $('#lnkCreateTask').click(function () {

        //Clear all fields
        ClearTaskFields();

        OpenTaskPopup(0);
    });

    //close task pop up
    $('#btnCloseTask').click(function () {

        CloseTaskPopUp();
    });

    //Save task
    $('#btnSaveTask').click(function () {
        SaveTask();
    });

    //Close corrective action po up
    $('#btnCancel').click(function () {
        ////CloseCapPopUp();
        //var length = window.location.href.split('/').length;
        //if (window.location.href.split('/')[length - 3] == _SMARTER)
        //    CancelSmarter();
        //else
        //    CancelNormalCAP();
        CancelCAP();
    });

    //Save corrective actions
    $('#btnSaveCAP').click(function () {
        //parameter getting passed to recognize if the cap is created as smart or not
        SaveCAP(isSmartPage);

    });

    $('#txtDueDate').datepicker();

}


//On fail of loading data for cap  page
function LoadCAPDetailsFail(data) {
}


//On success of loading data for cap  page
function LoadCAPDetailsSuccess(data) {

    

    //Get result data
    data = ParseToJSON(data.d);

    $('#divTitle').html(data[0].Identifier);

    //Set data to corresponding field
    $('#txtIdentifier').val(data[0].Identifier);
    $('#txtEventDescription').val(data[0].Description);
    $('#txtBusinessCase').val(data[0].BusinessCase);
    $('#txtReviewNotes').val(data[0].ReviewNotes);
    $('#txtTempAction').val(data[0].TemporaryActions);

    $('#ddlResponsiblePerson').val(data[0].CorrectiveAction.M_ResponsiblePersonID);

    $('#txtMInitialDueDate').val((data[0].CorrectiveAction.M_InitialDueDate != null && data[0].CorrectiveAction.M_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.M_InitialDueDate)) : '');

    $('#txtVerificationPlan').val(data[0].CorrectiveAction.M_VerificationPlan);
    $('#ddlAccountablePerson').val(data[0].CorrectiveAction.M_ResponsiblePersonID);
    $('#txtResource').val(data[0].CorrectiveAction.A_ResourceNeeded);

    $('#txtTimelyDueDate').val((data[0].CorrectiveAction.T_InitialDueDate != null && data[0].CorrectiveAction.T_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.T_InitialDueDate)) : '');

    $('#ddlEffectiveResponsilePerson').val(data[0].CorrectiveAction.E_ResponsiblePersonID);

    $('#txtEffectiveDueDate').val((data[0].CorrectiveAction.E_InitialDueDate != null && data[0].CorrectiveAction.E_InitialDueDate.length > 0) ? (ConvertToDate(data[0].CorrectiveAction.E_InitialDueDate)) : '');

    $('#txtValidationPlan').val(data[0].CorrectiveAction.E_ValidationPlan);

    $("#taskTemplateResults").empty();
    $("#taskTemplate").tmpl(data[0]).appendTo("#taskTemplateResults");



    //Implementation for read more link
    $('.bio').each(function () {

        var text = $(this).html();

        if (text.length > _hidingTextPosition) {

            $(this).html(text.slice(0, _hidingTextPosition) +
                        '<span class="more">... <a class="expand" href="javascript:void(0)">read more</a></span><span class="hidden">' + text.slice(10) + '</span>');

        }

    });


    //On expand click show the hidden text
    $('.expand').on('click', function (event) {
        $(event.target)
            .closest('label')
            .find('.hidden')
                .removeClass('hidden')
                .end()
            .find('.more')
                .remove();
    });

    //Check all rct node selected
    CheckSelectedRCTNodes(data[0].RootCauses);

    CheckSelectedGenericCause(data[0].GenericCauses);

}
//function CancelSmarter() {
//    CloseCapPopUp();

//    var url = jQuery(location).attr('href');
//    var splitUrl = url.split('/');
//    _eventId = splitUrl[splitUrl.length - 2];
//    var url = _baseURL + splitUrl[splitUrl.length - 6] + '/' + splitUrl[splitUrl.length - 5] +  '/' + splitUrl[splitUrl.length - 4] +  '/' + _eventId;
//    window.location = url;

//}


function CancelCAP() {
    
    window.location = document.referrer;
}

//Close  the  correctiveAction pop up
function CloseCapPopUp() {
        
    if (parent.$("#hdnStatusValue").val() == 'true') {

        var identifierName = $('#txtIdentifier').val();
        var message = (identifierName != null && identifierName.length > 0) ? identifierName + " saved successfully." : "Data saved successfully.";
        parent.NotificationMessage(message, 'jSuccess', true, 1000);
        parent.$("#hdnStatusValue").val('false');
        if (parent.$('#divTabCorrectiveAction').length > 0) {

            parent.PopulateCorrectiveActions();
        }

    }

  
    var id = parent.$('.ui-dialog');
    parent.$('.ui-icon-closethick').click();
    id.bind("dialogclose", function (event, ui) {
    });
   

    return false;
}



function CheckSelectedGenericCause(genericNodes) {
    //Implementation for read more link
    $('.genericNode').each(function () {

        var nodeValue = parseInt($(this).val());

        if (NodeExistsForGeneric(genericNodes, nodeValue)) {
            $(this).attr('checked', true);
        }

    });

}

//Iterate through the array returned and checked the corresponding node check box
function CheckSelectedRCTNodes(rctNodes) {
    //Implementation for read more link
    $('.rctNode').each(function () {

        var nodeValue = parseInt($(this).val());

        if (NodeExists(rctNodes, nodeValue)) {
            $(this).attr('checked', true);
        }

    });
}


//Check if the node value is present in the array of selected rct nodes or not
function NodeExistsForGeneric(a, obj) {

    var i = a.length;
    while (i--) {
        if (a[i].GenericCauseID === obj) {
            return true;
        }
    }

    return false;
}

//Check if the node value is present in the array of selected rct nodes or not
function NodeExists(a, obj) {

    var i = a.length;
    while (i--) {
        if (a[i].RCTID === obj) {
            return true;
        }
    }

    return false;
}



//On fail of causal factor
function LoadCausalFactorFail(data) {
}


//Save the modified rct node ids
function SaveRCTModifiedValue(checkBox) {
    
    var prevValue = (!checkBox.checked);
    var rctNodeValue = $(checkBox).val();
    var rctNode = ',' + rctNodeValue + ':' + prevValue;

    if (_rootCauses.indexOf(rctNode) >= 0) {
        _rootCauses = _rootCauses.replace(rctNode, '');
    }

    _rootCauses = _rootCauses + "," + rctNodeValue + ':' + String(checkBox.checked);

}




//Save the modified rct node ids
function SaveGenericModifiedValue(checkBox) {
    
    var prevValue = (!checkBox.checked);
    var genericNodeValue = $(checkBox).val();
    var genericNode = ',' + genericNodeValue + ':' + prevValue;

    if (_genericCauses.indexOf(genericNode) >= 0) {
        _genericCauses = _genericCauses.replace(genericNode, '');
    }

    _genericCauses = _genericCauses + "," + genericNodeValue + ':' + String(checkBox.checked);

}


//Save corrective action
function SaveCAP(isSmartCA) {
         
    _isSmartCA = isSmartCA;

    if (ValidateForm()) {
        var formData = GetDetailsFromPage();
        formData = JSON.stringify(formData);
        var serviceUrl = '/TFSTaprootSVC/';//GetServicePath();
        serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/SaveCorrectiveAction';
        AjaxPost(serviceUrl, formData, eval(SaveCAPSuccess), eval(SaveCAPFail));

    }
}


//On success of save cap
function SaveCAPSuccess() {
      
    window.parent.$("#hdnStatusValue").val(true);
    //////CloseCapPopUp();
    ////if (_isSmartCA) {
    ////    CancelSmarter();
    ////}
    ////else {
    ////    CancelNormalCAP();
    ////}
    //CancelCAP();

   // 

    if ($('#btnSaveCAP').val() != 'Apply Changes') { // temp code
        DesignEditPageLook();
    }
    else {
        NotificationMessage('Saved successfully.', 'jSuccess', false, 1000);
    }
}

function callback() {
   
    setTimeout(function () {
        $("#divSection").removeAttr("style").hide().slideDown("slow");
    }, 500);

  
    //Load css file
    var file = ACCORDION_CSS_FILE_NAME;
    $('head').append(file);

    //click event of header
    $('#sec_specfic,#sec_measurable,#sec_accountable,#sec_reasonable,#sec_timely,#sec_effective,#sec_reviewed').click(function () {
        ShowHideContent(this.id);
    });

    $("#sec_specfic").appendTo("#divSpecific");

    //Show header
    $('#specificHeader,#measurableHeader,#accountableHeader,#reasonableHeader,#timelyHeader,#effectiveHeader,#reviewedHeader').show();


    $('#sec_measurable').animateAppendTo('#sec_reasonable', 2000);

    $('#btnSaveCAP').val('Apply Changes');
    $("#divTaskHeader").show();
};

// DESIGN THE EDIT PAGE LOOK
function DesignEditPageLook() {  

    $("html, body").animate({ scrollTop: $(document).height() + 1000 }, "fast");
   
    var options = {};
    $("#divSection").effect('drop', options, 2000, callback);
          
   
     
}


$.fn.animateAppendTo = function (sel, speed) {
  
        var $this = this,
        newEle = $this.clone(true).insertBefore(sel),
        newPos = newEle.position();
        newEle.hide();
        $this.css('position', 'absolute').animate(newPos, speed, function () {
            
        newEle.show();
        $this.remove();
        complete: { if (sel == '#sec_reasonable') { $('#sec_reasonable').animateAppendTo('#sec_timely', 2000); } }

    });

    return newEle;
};

//On fail of save cap
function SaveCAPFail() {
}


//Validate the form
function ValidateForm() {   
   
    if ($('#txtIdentifier').val() == '') {
        ShowHideWarningMessage('txtIdentifier', 'Please specify Identifier', true);
        parent.NotificationMessage('Please enter Identifier.', 'jError', false, 3000);
        return false;
    }
   
    return true;
}


//Get all details from the cap page
function GetDetailsFromPage() {
  
    var capData = {};
    var splittedURL = window.location.href.split('/');
    capData.eventID = splittedURL[splittedURL.length - 2];
    capData.correctiveActionID = splittedURL[splittedURL.length - 1]; 
    capData.correctiveActionID = (capData.correctiveActionID.indexOf("#") >= 0) ? capData.correctiveActionID.split('#')[0] : capData.correctiveActionID;
    capData.isSmarter = _isSmartCA;
    capData.identifier = $('#txtIdentifier').val();
    capData.description = $('#txtEventDescription').val();
    capData.businessCase = $('#txtBusinessCase').val();
    capData.reviewNotes = $('#txtReviewNotes').val();
    capData.temporaryActions = $('#txtTempAction').val();
    capData.mResponsiblePersonID = (_isSmartCA) ? ($('#ddlResponsiblePerson').val() == '')?null:$('#ddlResponsiblePerson').val() : null;
    capData.mInitialDueDate = (_isSmartCA) ? $('#txtMInitialDueDate').val() : '';
    capData.mVerificationPlan = (_isSmartCA) ? $('#txtVerificationPlan').val() : '';
    capData.aResponsiblePersonID = (_isSmartCA) ? ($('#ddlAccountablePerson').val() == '') ? null : $('#ddlAccountablePerson').val() : null;
    capData.aResourceNeeded = (_isSmartCA) ? $('#txtResource').val() : '';
    capData.tInitialDueDate = (_isSmartCA) ? $('#txtTimelyDueDate').val() : '';
    capData.eResponsiblePersonID = (_isSmartCA) ? ($('#ddlEffectiveResponsilePerson').val() == '') ? null : $('#ddlEffectiveResponsilePerson').val() : null;
    capData.eInitialDueDate = (_isSmartCA) ? $('#txtEffectiveDueDate').val() : '';
    capData.eValidationPlan = (_isSmartCA) ? $('#txtValidationPlan').val() : '';
       
    capData.rootCausesSelected = _rootCauses.substr(1, _rootCauses.length - 1);

    capData.genericCausesSelected = _genericCauses.substr(1, _genericCauses.length - 1);

    return capData;
}


//**************************************** TASK RELATED OPERATIONS ************************

//Load task details, task types 
function PopulateTaskPage() {
   
    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetAllTaskTypes';
    AjaxPost(serviceURL, null, eval(OnSuccessPopulateTaskTypes), eval(OnFailPopulateTaskTypes));

}


//On success of populate task types
function OnSuccessPopulateTaskTypes(result) {
    
    $('#ddlTaskType').empty();

    if (result != null) {

        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        $('#ddlTaskType').append($('<option></option>').val('').html('<< Select >>'));

        for (var i = 0; i < jsonResponse.length; i++) {
            $("#ddlTaskType").append(
                $("<option></option>").attr("value", jsonResponse[i]["TaskTypeID"]).attr("title", jsonResponse[i]["TaskTypeName"]).text(jsonResponse[i]["TaskTypeName"]));
        }

        $("#ddlTaskType option:first").attr('selected', 'selected');
    }

    PopulateTaskPerson(_baseURL);
}


//On fail of populate task types
function OnFailPopulateTaskTypes() {
}


//close pop up
function CloseTaskPopUp() {

    $("#dialog:ui-dialog").dialog("destroy");
    $('#divAddEditTaskDialog').dialog("destroy");
    $('#divAddEditTaskDialog').bind("dialogclose", function (event, ui) {
    });

}


//open Task pop up
function OpenTaskPopup(taskID) {
    
    $('#chkIsCompleted').attr('checked', false);
    $('#divAddEditTaskDialog').removeClass('seasonContentHide');
    $('#divAddEditTaskDialog').addClass('seasonContentShow');

    $("#dialog:ui-dialog").dialog("destroy");


    var modalHeight = (taskID == 0) ? 340 : 440;

    if (taskID > 0) {
        $('#btnSaveTask').val('Apply Changes');

    }
    else {

        $('#btnSaveTask').val('Create');
        OpenTaskPopupDialog(modalHeight);
    } 

    _currentTaskID = taskID; //Set the current task id
     PopulateTaskPage(); //Load page for task pop up by task id

}

//Populate responsible person
function PopulateTaskPerson(_baseURL) {
    
    var data = {};
    data.companyId = window.parent.$('#hdnCompanyId').val();
    data = JSON.stringify(data);

    var serviceURL = _baseURL + 'Services/Users.svc/GetAllActiveUsers';
    AjaxPost(serviceURL, data, eval(PopulateTaskPersonSuccess), eval(PopulateTaskPersonFail));

}


function PopulateTaskPersonFail() {
}


//On success of Populate responsible person
function PopulateTaskPersonSuccess(result) {
   
    //clear all previous value from the responsible person drop downs
    $('#ddlUsers').empty();

    if (result != null) {

        //Get the result
        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        //Append <<Select>> as first item
        $('#ddlUsers').append($('<option></option>').val('').html('<< Select >>'));

        //Iterate result set to add all the items to drop downs
        for (var i = 0; i < jsonResponse.length; i++) {

            $("#ddlUsers").append($("<option></option>").attr("value", jsonResponse[i]["UserID"]).attr("title", jsonResponse[i]["UserName"]).text(jsonResponse[i]["UserName"]));
        }
        
        //If called from the corrcetive action page then call method to populate task related details
        PopulateTaskDetail();

    }
}



//Populate task pop up with the details for the current task
function PopulateTaskDetail() {

    if (_currentTaskID != 0) {

        $("#divNotes").show();
        $("#divIsCompleted").show();
        var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetTaskDetailsByID';
        var data = {};
        data.taskID = _currentTaskID;
        AjaxPost(serviceURL, JSON.stringify(data), eval(LoadTaskSuccess), eval(LoadTaskFail));

    }
    else {

        //Clear all fields
        ClearTaskFields();
        $("#divNotes").hide();
        $("#divIsCompleted").hide();
    }

}


//On success of Populate Task details
function LoadTaskSuccess(data) {

    var taskData = ParseToJSON(data.d)[0];

    $('#ddlTaskType').val(taskData.TaskTypeID);
    $('#ddlUsers').val(taskData.ResponsiblePersonID);
    $('#txtDescription').val(taskData.Description);
    $('#txtDueDate').val(ConvertToDate(taskData.DueDate));
    $('#txtTaskNotes').val(taskData.Notes);
    $('#chkIsCompleted').attr('checked', taskData.IsCompleted);

    OpenTaskPopupDialog(440)
   
}


//On fail of Populate Task details
function LoadTaskFail(data) {
}

function OpenTaskPopupDialog(modalHeight)
{
    $('#divAddEditTaskDialog').dialog({

        height: modalHeight,
        width: 478,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 100]

    });
}


//Clear all task fields
function ClearTaskFields() {

    $('#ddlTaskType').val('');
    $('#ddlUsers').val('');
    $('#txtDescription').val('');
    $('#txtDueDate').val('');

}


//Save task for a corrective action
function SaveTask() {

    if (ValidateTaskForm()) {

        var formData = GetTaskDetailsFromPage();
        formData = JSON.stringify(formData);

        serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/SaveTask';
        AjaxPost(serviceUrl, formData, eval(SaveTaskSuccess), eval(SaveTaskFail));

    }
}


//Validate task form
function ValidateTaskForm() {

    var messageText = 'You are missing the fields :';
    var originalMessageLength = messageText.length;

    messageText = ($('#ddlTaskType').val() == '') ? messageText + "Task Type, " : messageText;
    messageText = ($('#ddlUsers').val() == '') ? messageText + "Responsible Person, " : messageText;
    messageText = ($('#txtDueDate').val() == '') ? messageText + "Due Date, " : messageText;
    messageText = ($('#txtDescription').val() == '') ? messageText + "Description, " : messageText;

    if (messageText.length == originalMessageLength) {
        return true;
    }

    messageText = messageText.substr(0, messageText.length - 2) + '.';
    parent.NotificationMessage(messageText, 'jError', false, 3000);

    return false;
}


//On success of save task
function SaveTaskSuccess(data) {

    

    //Close task pop up
    CloseTaskPopUp();

    //Load details for the cap page
    LoadCAPDetails();

    //NotificationMessage('Data saved successfully.', 'jSuccess', false, 1000);
    NotificationMessage('Task saved successfully.', 'jSuccess', false, 1000);

    window.parent.PopulateCorrectiveActions();

}


//On fail of save task
function SaveTaskFail(data) {
}


//Get all the data from the task pop up page
function GetTaskDetailsFromPage() {

    var taskData = {};
    taskData.taskID = _currentTaskID;
    taskData.correctiveActionID = GetCorrcetiveActionID();
    taskData.correctiveActionID = (taskData.correctiveActionID.indexOf("#") >= 0) ? taskData.correctiveActionID.split('#')[0] : taskData.correctiveActionID;
    taskData.taskTypeID = $('#ddlTaskType').val();
    taskData.responsiblePersonID = $('#ddlUsers').val();
    taskData.description = $('#txtDescription').val();
    taskData.dueDate = $('#txtDueDate').val();
    taskData.notes = $('#txtTaskNotes').val();
    taskData.isCompleted = $('#chkIsCompleted').attr('checked') == 'checked' ? 1 : 0;

    return taskData;
}

//On fail of Populate responsible person
function OnFailPopulateResponsiblePerson(result) {
}


//Display the jquery pop up
function JqueryPopUp(url, pageTitle, divID) {
    
    $('#' + divID).removeClass('seasonContentHide');
    $('#' + divID).addClass('seasonContentShow');
    $("#dialog:ui-dialog").dialog("destroy");    
    $('#' + divID).load($('#frameLoadPopup').attr('src', url));

    $('#frameLoadPopup').attr('title', pageTitle);

    $('#' + divID).dialog({


        title: pageTitle,
        height: 870,
        width: 910,
        modal: true,
        draggable: true,
        resizable: false,
        scrollbar: false,
        position: ['middle', 200],
        open: function (ev, ui) {
            $('#' + divID).css('overflow', 'hidden');           
        }
   });
      
}

//Convert the json date format to javascript date format 
function ConvertToDate(dateValue) {

    var jsonDate = new Date(parseInt(dateValue.substr(6)));
    var displayDate = $.datepicker.formatDate("mm/dd/yy", jsonDate);//change with site date setting
    return displayDate;

}

