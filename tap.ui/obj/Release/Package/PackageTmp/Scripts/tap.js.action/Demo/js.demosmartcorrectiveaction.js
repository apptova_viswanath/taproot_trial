﻿
/*Smart Corrective Action  Screen Methods (usha)*/

$(document).ready(function ()
{
  
    //debugger;

    _dirtyMsgText = "This SMARTER Corrective Action has not been saved. Do you want to save it now?";
  
    //var formTitle = window.location.href.split('/')[length - 3];
    //var isCreatePage = (formTitle == 'Create');
    var length = window.location.href.split('/').length;
    var capID = window.location.href.split('/')[length - 1];
    var isCreatePage = (capID > 0) ? false : true;

    if (isCreatePage) {

        //$("#accordionCss").attr("disabled", "disabled");

        //$("#sec_timely").prependTo("#sec_measurable");
        //$("#sec_accountable").prependTo("#sec_measurable");
        //$("#sec_reasonable").prependTo("#sec_measurable");

    }
    else {
       

        //To display the detail on header click
        $('#sec_specfic,#sec_measurable,#sec_accountable,#sec_reasonable,#sec_timely,#sec_effective,#sec_reviewed').click(function () {
            ShowHideContent(this.id);
        });

        $("#sec_specfic").appendTo("#divSpecific");
        $('#specificHeader,#measurableHeader,#accountableHeader,#reasonableHeader,#timelyHeader,#effectiveHeader,#reviewedHeader').show();

    }

    BindCommonFieldsAndEvents(isCreatePage, true);

    //Populate causal factor
    //parameter getting passed to recognize if the causal factor  is populated for the tab page or for create/edit cap
    PopulateCausalFactors(true, false, true);

    LoadPageForSmartCAP();

    LoadCAPDetails();
      
    $('#btnReset').click(function () {
        location.reload();
        $('#txtIdentifier').val('Demo CAP');
        $("html, body").animate({ scrollTop: $(document).height() + 1000 }, "fast");

    });
});


//Load page for smart corrcetive action
function LoadPageForSmartCAP() {

    $("#txtEffectiveDueDate").datepicker();
    $("#txtTimelyDueDate").datepicker();
    $("#txtMInitialDueDate").datepicker();

    $("#ui-datepicker-div").addClass("notranslate");

    //Populate responsible person data
    PopulateResponsiblePerson(_baseURL);

}


//Populate responsible person
function PopulateResponsiblePerson(_baseURL) {
    
    var data = {};
    data.companyId = window.parent.$('#hdnCompanyId').val();
    data = JSON.stringify(data);

    var serviceURL = _baseURL + 'Services/Users.svc/GetAllActiveUsers';
    PostAjax(serviceURL, data, eval(OnSuccessPopulateResponsiblePerson), eval(OnFailPopulateResponsiblePerson));

}

//On success of Populate responsible person
function OnSuccessPopulateResponsiblePerson(result) {

  
        $('#ddlResponsiblePerson').empty();
        $('#ddlEffectiveResponsilePerson').empty();
        $('#ddlAccountablePerson').empty();
 
    if (result != null) {

        //Get the result
        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        //Append <<Select>> as first item
     
        $('#ddlResponsiblePerson').append($('<option></option>').val('').html('<< Select >>'));
        $('#ddlEffectiveResponsilePerson').append($('<option></option>').val('').html('<< Select >>'));
        $('#ddlAccountablePerson').append($('<option></option>').val('').html('<< Select >>'));

        //Iterate result set to add all the items to drop downs
        for (var i = 0; i < jsonResponse.length; i++) {
            $("#ddlResponsiblePerson").append($("<option></option>").attr("value", jsonResponse[i]["UserID"]).attr("title", jsonResponse[i]["UserName"]).text(jsonResponse[i]["UserName"]));
            $("#ddlEffectiveResponsilePerson").append($("<option></option>").attr("value", jsonResponse[i]["UserID"]).attr("title", jsonResponse[i]["UserName"]).text(jsonResponse[i]["UserName"]));
            $("#ddlAccountablePerson").append($("<option></option>").attr("value", jsonResponse[i]["UserID"]).attr("title", jsonResponse[i]["UserName"]).text(jsonResponse[i]["UserName"]));

        }       
    }
}


//On fail of Populate responsible person
function OnFailPopulateResponsiblePerson(result) {
}


//Close smart corrcteive action pop up
function CloseSmartCAPPopUp() {
   
    parent.$("#dialog-smartcap").dialog('close');
   
    if (parent.$("#hdnStatusValue").val() == 'true') {

        parent.NotificationMessage('Data saved successfully.', 'jSuccess', true, 1000);
        parent.$("#hdnStatusValue").val('false');

    }
}


//To display the detail on header click using Toggle Event
function ShowHideContent(controlId)
{
    $('#' + controlId + 'Content').slideToggle(500);
}



//Display dictionary pop up
function DisplayPopUp(ctrl, rctTitle) {

    $(ctrl).find('#header').hide();
    $(ctrl).find('.topicheading').hide();


    $(ctrl).find('a').each(function () {

        var anchorText = $(this).html().replace(/(\r\n|\n|\r)/gm, "").replace(/ +/g, "");

        if (anchorText == 'RootCauseTree®Dictionary') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16, id.length);
                DisplayPopUp('#divRCTDictionary' + id, rctTitle);
                ClosePoUp('#divCAPDictionary' + id);
            });
        }

        if (anchorText == 'CorrectiveActionHelper®LiabilityDisclaimer') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                return false;
            });
        }

        if (anchorText == 'CorrectiveActionHelper®') {
            $(this).attr('href', '');
            $(this).live("click", function (event) {
                var id = $(ctrl).attr('id');
                id = id.substring(16, id.length);
                DisplayPopUp('#divCAPDictionary' + id, rctTitle);
                ClosePoUp('#divRCTDictionary' + id);
            });
        }


    });


    $(ctrl).dialog({

        height: 300,
        width: 478,
        modal: true,
        position: ['middle', 100],
        title: rctTitle

    });

    $("ctrl").siblings('.ui-dialog-titlebar').removeClass('ui-widget-header');

    return false;
}


function ClosePoUp(dialog) {
    $(dialog).hide();
    $("#dialog:ui-dialog").dialog("destroy");
    $(dialog).dialog("destroy");
    $(dialog).bind("dialogclose", function (event, ui) {
    });
}