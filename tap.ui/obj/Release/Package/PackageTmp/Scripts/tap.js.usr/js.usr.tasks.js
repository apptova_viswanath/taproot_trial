﻿//ready function
_isShareTab = false;
$(document).ready(function () {
   
    TasksGridCount();
    _isShareTab = false;
    //DataBindToGrid();
    //GetTasks();
    $("#IdShowAllTasks").click(function () {
        //debugger;
        $('#IdShowReadyOnly').show();
        $('#IdShowAllTasks').hide();

        sessionStorage.type = 'All';
        //TasksGridCount();
       
        DataBindToGrid();
        ReloadGridData();

    });
    $("#IdShowReadyOnly").click(function () {
        $('#IdShowAllTasks').show();
        $('#IdShowReadyOnly').hide();
        sessionStorage.type = 'Only';
        DataBindToGrid();
        ReloadGridData();

    });



    $('#txtDueDate').datepicker({ dateFormat: localStorage.dateFormat });
    $("#ui-datepicker-div").addClass("notranslate");

});

function UpdateTasks()
{
    if (ValidateTaskForm()) {

        var formData = GetTaskDetails();
        formData = JSON.stringify(formData);

        // serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/SaveTask';


        serviceUrl = _baseURL + 'Services/CorrectiveAction.svc/UpdateTasks';
        AjaxPost(serviceUrl, formData, eval(UpdateTaskSuccess), eval(UpdateTaskFail));

    }
}





//Validate task form
function ValidateTaskForm() {

    var messageText = 'You are missing the fields :';
    var originalMessageLength = messageText.length;

    messageText = ($('#ddlTaskType').val() == '') ? messageText + "Task Type, " : messageText;
    messageText = ($('#spnTaskResponsiblePerson')[0].innerHTML == '') ? messageText + "Responsible Person, " : messageText;
    messageText = ($('#txtDueDate').val() == '') ? messageText + "Due Date, " : messageText;
    messageText = ($('#txtDescription').val() == '') ? messageText + "Description, " : messageText;

    if (messageText.length == originalMessageLength) {
        return true;
    }

    messageText = messageText.substr(0, messageText.length - 2) + '.';
    parent.NotificationMessage(messageText, 'jError', false, 3000);

    return false;
}


//On success of save task
function UpdateTaskSuccess(data) {


    //Close task pop up
    CloseTaskPopUp();


    DataBindToGrid(); 
    ReloadGridData() ;


    //NotificationMessage('Data saved successfully.', 'jSuccess', false, 1000);
    NotificationMessage('Task saved successfully.', 'jSuccess', false, 1000);
    //deepa commented
    //  window.parent.PopulateCorrectiveActions();

}


//On fail of save task
function UpdateTaskFail(data) {
}



function GetTaskDetails() {
    
    var taskData = {};
    taskData.taskID = _currentTaskID == 0 ? 1 : _currentTaskID;//change the code
    //taskData.correctiveActionID = GetCorrcetiveActionID();
    //taskData.correctiveActionID = (taskData.correctiveActionID.indexOf("#") >= 0) ? taskData.correctiveActionID.split('#')[0] : taskData.correctiveActionID;
    taskData.taskTypeID = $('#ddlTaskType').val();
    taskData.responsiblePersonID = $('#spnTaskResponsiblePerson')[0].innerHTML;
    taskData.description = $('#txtDescription').val();
    taskData.dueDate = $('#txtDueDate').val();// new Date($('#txtDueDate').val());
    taskData.notes = $('#txtTaskNotes').val();
    taskData.isCompleted = $('#chkIsCompleted').attr('checked') == 'checked' ? 1 : 0;
    taskData.companyId = $('#hdnCompanyId').val();
    taskData.userId = $('#hdnUserId').val();
    taskData.isDirectTask = true;

    return taskData;
}
function ReloadGridData() { 
    jQuery("#TasksGrid").trigger("reloadGrid");
} 

function TasksGridCount() {
    var userId = GetUserId();
    var data = '&userID=' + userId;
    if (userId != null && userId != undefined) {
        var serviceUrl = _baseURL + 'Services/Events.svc/TasksCount';
        Ajax(serviceUrl, data, eval(TaskCountSucess), eval(TaskCountFail), 'TasksCount');
    }
}
function TaskCountSucess(result)
{
    var jsonData = jQuery.parseJSON(result.d);
    var TasksGridCount = parseInt(jsonData);

    if (TasksGridCount <= 20) {
        $('#TasksGrid').setGridParam({ rowNum: TasksGridCount });
    }
    else {
        $('#TasksGrid').setGridParam({ rowNum: 20 });
        $('#TasksGrid').setGridParam({ rowList: [20, 40] }).trigger("reloadGrid");;
    }
    sessionStorage.type = 'Only';
    DataBindToGrid();
   // DataBindToGrid('Only');


}


function TaskCountFail()
{
    alert("Error in loading data.");
}
//Bind Grid with tasks
function BindTasksToGrid()
{

}
//Bind Grid 
function DataBindToGrid() {
    var $grid = $("#TasksGrid"), gridcol;
    //
    jQuery("#TasksGrid").jqGrid({

        datatype: function (parameterData) { GetTasks(parameterData); },
        colNames: ['EventId', 'Event Name', 'Corrective Action', 'Task Type', 'Responsible Person', 'Due Date', 'Status', 'Description', 'Edit'],
        colModel: [
             { name: 'EventId', index: 'EventId', width: 40, align: 'center', hidden: true },
            { name: 'EventName', index: 'EventName', width: 150, align: 'center', sortable: true, classes: 'notranslate', resizable: true, sortOrder : 'asc' },
            { name: 'CorrectiveAction', index: 'CorrectiveAction', width: 150, align: 'center', sortable: true, resizable: true, classes: 'notranslate', sortOrder: 'asc' },
            { name: 'TaskTypeName', index: 'TaskTypeName', width: 150, align: 'center', sortable: true, resizable: true, classes: 'notranslate', sortOrder: 'asc' },//, formatter: TextFormatter
                        { name: 'ResponsiblePerson', index: 'ResponsiblePerson', width: 127, align: 'center', sortable: true, resizable: true, classes: 'notranslate', sortOrder: 'asc' },
                       //  { name: 'eventId', index: 'eventId', width: 40, align: 'center', hidden: true, sortable: false, resizable: true },
                        { name: 'DueDate', index: 'DueDate', width: 120, align: 'center', sortable: true, resizable: true, sortOrder: 'asc' },
                        { name: 'Status', index: 'Status', width: 130, align: 'center', sortable: true, resizable: true, sortOrder: 'asc' },
                        { name: 'Description', index: 'Description', width: 300, align: 'center', sortable: true, resizable: true, sortOrder: 'asc' },
                        { name: 'options', index: 'options', width: 40, align: 'center', editable: false, classes: "HandCursor", sortable: false, resizable: true },
                        
        ],
        gridview: false,
        rowattr: function (rd) {
            if (rd.Active == "False") {
                return { "class": "rowColor" };
            }
        },
        toppager: false,
        emptyrecords: 'No Pending Tasks ',
        hidegrid: false,
        rowNum: 30,
        pager: jQuery('#pageNavigation'),
        // Grid total width and height   
        autowidth: true,
        height: '100%',
        caption: 'Tasks',       
        viewrecords: true,
        sortname: 'DueDate',
        sortorder: "asc",
        multiSort: true,
        gridComplete: AddDetailsButton,
        forceFit: true,
        loadComplete: function () {
            jqgridCreatePager('pageNavigation', 'TasksGrid', 5);

        },
        ondblClickRow: function (rowId) {

            DisplayDetailsPopup(rowId);

        },
    });
    
    $(window).bind('resize', function () {
        $("#TasksGrid").setGridWidth($('.tasksContent').width() - 15);
    }).trigger('resize');

}

//Add Edit Button in Grid Rows
function AddDetailsButton() {
    //debugger;
     var ids = jQuery("#TasksGrid").getDataIDs();
    var data = jQuery("#TasksGrid").getRowData();
   
    for (var i = 0; i < ids.length; i++) {
        var imageId = ids[i];
        EditButton = " <img  id='TaskDetails" + imageId + "' style='margin-top:4px;' src='" + _baseURL + "Scripts/jquery/images/edit-list-icon.png' title='Display Task Details' onclick='DisplayDetailsPopup(" + imageId + ")' />";
        jQuery("#TasksGrid").setRowData(ids[i], { options: EditButton });
        var status = data[i].Status;
        var type = sessionStorage.type;
        _eventId = data[i].EventId;
       //if (type == "Only") {
       //     if (status != 'Ready')
       //         $("#" + imageId, "#TasksGrid").css({ display: "none" });
                         
       // }
    }
}

function DisplayDetailsPopup(taskID)
{
    //debugger;
    _eventId = '0';
    _personType = 'TaskResponsiblePerson';
    OpenTaskPopup(taskID);
}

function GetTasks(parameterData) {
   // debugger;

    var systemListData = new Object();
    systemListData.page = parameterData.page;
    systemListData.rows = parameterData.rows;
    systemListData.sortIndex = parameterData.sidx;
    systemListData.sortOrder = parameterData.sord;


  //  systemListData.companyId = _companyId; //remove hard code
    
    systemListData.userId = _userId;
    
    systemListData.companyId = _companyId;
    systemListData.status = sessionStorage.type;

    data = JSON.stringify(systemListData);
    serviceUrl = _baseURL + 'Services/Events.svc/GetTasks';
    AjaxPost(serviceUrl, data, eval(GetTasksSuccess), eval(GetTasksFail));
}

//success method For Eventsdata
function GetTasksSuccess(result) {
   
    var gridId = jQuery("#TasksGrid")[0];
    var jsonResult = JSON.parse(result.d);

    if (result.d != "") {
        gridId.addJSONData(jsonResult);
        var resultCount = ParseToJSON(result.d);
    }
    else {
        var message = 'There are no Tasks';
        messageType = 'jError';
        NotificationMessage(message, messageType, true, 3000);
    }

    //pagination
    jqgridCreatePager('pageNavigation', 'TasksGrid', 5);
  
}
function GetTasksFail() {

    alert('Service failed.');
}