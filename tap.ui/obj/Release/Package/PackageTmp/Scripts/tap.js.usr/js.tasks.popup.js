﻿function OpenTaskPopup(taskID) {
    
    _isShareTab = false;
    $('#chkIsCompleted').attr('checked', false);
    $('#divAddEditTaskDialog').removeClass('seasonContentHide');
    $('#divAddEditTaskDialog').addClass('seasonContentShow');

    $("#dialog:ui-dialog").dialog("destroy");


    var modalHeight = (taskID == 0) ? 340 : 440;

    if (taskID > 0) {
        $('#btnSaveTask').val('Apply Changes');
      
    }
    else {

        $('#btnSaveTask').val('Create');
        OpenTaskPopupDialog(modalHeight);
    }

    _currentTaskID = taskID; //Set the current task id
    PopulateTaskPage(); //Load page for task pop up by task id

}

//close task pop up
$('#btnCloseTask').live("click", function () {
    
    CloseTaskPopUp();
});
//close pop up
function CloseTaskPopUp() {
    

    $("#dialog:ui-dialog").dialog("destroy");
    $('#divAddEditTaskDialog').dialog("destroy");
    $('#divAddEditTaskDialog').bind("dialogclose", function (event, ui) {
    });

}
//Save task
$("#btnSaveTask").live("click", function () {
    
    UpdateTasks();
});


function OpenTaskPopupDialog(modalHeight) {
    $('#divAddEditTaskDialog').dialog({

        height: modalHeight,
        width: 478,
        modal: true,
        draggable: true,
        resizable: false,
        position: ['middle', 100]

    });
}

function PopulateTaskPage() {

    var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetAllTaskTypes';
    AjaxPost(serviceURL, null, eval(OnSuccessPopulateTaskTypes), eval(OnFailPopulateTaskTypes));

}
//On success of populate task types
function OnSuccessPopulateTaskTypes(result) {
    

    $('#ddlTaskType').empty();

    if (result != null) {

        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        $('#ddlTaskType').append($('<option></option>').val('').html('<< Select >>'));

        for (var i = 0; i < jsonResponse.length; i++) {
            $("#ddlTaskType").append(
                $("<option></option>").attr("value", jsonResponse[i]["TaskTypeID"]).attr("title", jsonResponse[i]["TaskTypeName"]).text(jsonResponse[i]["TaskTypeName"]));
        }

        $("#ddlTaskType option:first").attr('selected', 'selected');
    }

    PopulateTaskPerson(_baseURL);
}


//On fail of populate task types
function OnFailPopulateTaskTypes() {
}
//Populate responsible person
function PopulateTaskPerson(_baseURL) {

    var data = {};
    data.companyId = window.parent.$('#hdnCompanyId').val();
    data = JSON.stringify(data);

    var serviceURL = _baseURL + 'Services/Users.svc/GetAllActiveUsers';
    AjaxPost(serviceURL, data, eval(PopulateTaskPersonSuccess), eval(PopulateTaskPersonFail));

}


function PopulateTaskPersonFail() {
}


//On success of Populate responsible person
function PopulateTaskPersonSuccess(result) {
    

    //clear all previous value from the responsible person drop downs
    $('#ddlUsers').empty();

    if (result != null) {

        //Get the result
        var jsonResponse = ParseToJSON(result.d);
        var options = '';

        //Append <<Select>> as first item
        $('#ddlUsers').append($('<option></option>').val('').html('<< Select >>'));

        //Iterate result set to add all the items to drop downs
        for (var i = 0; i < jsonResponse.length; i++) {

            $("#ddlUsers").append($("<option></option>").attr("value", jsonResponse[i]["UserID"]).attr("title", jsonResponse[i]["UserName"]).text(jsonResponse[i]["UserName"]));
        }

        //If called from the corrcetive action page then call method to populate task related details
        PopulateTaskDetail();

    }
}



//Populate task pop up with the details for the current task
function PopulateTaskDetail() {
    
    if (_currentTaskID != 0) {

        $("#divNotes").show();
        $("#divIsCompleted").show();
        var serviceURL = _baseURL + 'Services/CorrectiveAction.svc/GetTaskDetailsByID';
        var data = {};
        data.taskID = _currentTaskID;
        AjaxPost(serviceURL, JSON.stringify(data), eval(LoadTaskSuccess), eval(LoadTaskFail));

    }
    else {

        //Clear all fields
        ClearTaskFields();
        $("#divNotes").hide();
        $("#divIsCompleted").hide();
    }

}
function LoadTaskFail(data) {
}
//Convert the json date format to javascript date format 
function ConvertToDate(dateValue) {
    if (dateValue != null) {
        var jsonDate = new Date(parseInt(dateValue.substr(6)));

        return displayDate = $.datepicker.formatDate(localStorage.dateFormat, jsonDate);//change with site date setting 
    }
}

//On success of Populate Task details
function LoadTaskSuccess(data) {
    
    var taskData = ParseToJSON(data.d)[0];

    $('#ddlTaskType').val(taskData.TaskTypeID);
    $('#ddlUsers').val(taskData.ResponsiblePersonID);
    $('#spnTaskResponsiblePerson')[0].innerHTML = taskData.ResponsiblePersonID;
    $('#txtDescription').val(taskData.Description);
    var datedue = ConvertToDate(taskData.DueDate)
    $('#txtDueDate').val( datedue);
    $('#txtTaskNotes').val(taskData.Notes);
    $('#chkIsCompleted').attr('checked', taskData.IsCompleted);

    OpenTaskPopupDialog(440)

}