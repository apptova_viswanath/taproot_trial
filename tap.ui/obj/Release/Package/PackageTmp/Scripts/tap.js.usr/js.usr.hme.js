﻿
//*Home Page Methods*//

//Global variable
var _searchField = '';
var _basicSearch = '';
var _userName = '';
var _advanceSearch = '';
var _searchLink = '';
var _message = '';
var _messageType = '';
var _isPageLoad = false;
var _data = '';
var _eventId = 0;
var _userId = '';
var _companyId = '';

//ready function
$(document).ready(function () {
    
    if (sessionStorage.Change == 'true')
    {
        window.location.href =  _baseURL + 'ChangePassword';
    }
    else if (sessionStorage.Change == 'truewithmessage') {
        window.location.href = _baseURL + 'ChangePassword/message';
    }

    else
    {
        sessionStorage.Change == "false";
    }

    ClearSearchControls(); 

    //Page load 
    SetPageLoad();   

    //Search functionality 
    //$("#lblTasks").click(function () {
    //    debugger;
       
    //});
   
    //Search functionality 
    $("#SearchLink a").click(function () {
        EventSearch(this);
    });

    //Basic search option 
    $("#txtSearch").keyup(function () {
        BasicSearch();
        _isDirty = false;
    });

    //Advance search
    $("#btnsearch").click(function () {        
        AdvancedSearch(true);
        _isDirty = false;
    });

    //clear button functionality
    $("#btnClearSearch").click(function () {
        ClearSearchControls();
        //OPT_add
        ReloadEventGrid();

        _isDirty = false;
    });

    $("#ReloadGrid").click(function () {
        ReloadEventGrid();
    });
    $("#chkIncDate30").click(function () {
        $('#txtfromdate').val('');
        $('#txttodate').val('');
    });
    $("#chkIncDate60").click(function () {
        $('#txtfromdate').val('');
        $('#txttodate').val('');
    });
        
    $('#lblTasks').attr('href', _baseURL + 'Tasks');

});       //end ready function

//Inilialize the page with Default values
function SetPageLoad() {
    //Company subscription expire days
    SubscriptionExpireMsg();

    _isPageLoad = true;
    $("#divNoRowMessage").hide();
    //OPT- commented because of grid size is congested.
    //$('#Grid').hide(); 

    //Advance Search Box 
    HideAdvanceSearchPopUp();

    _userName = GetUserName();

    _userId = GetUserId();

    _companyId = GetCompanyId();

    $('#txtSearch').focus();

    $('#MenuHeader').hide();

    $("#ReloadGrid").hide();

    $('#lnkEventAll').addClass("AnchorBoldTag");
    $('#lnkEventAll').removeClass("AnchorTag");

    //assigning the DatePicker to the DateFields
    var dateSelectors = "#txtfromdate, #txttodate";
    
    $("#txtfromrecent").datepicker();
    $("#txtfromdate").on('change', function () {
        OnFromChange();
    });
    $("#txttorecent").datepicker();
    $("#ui-datepicker-div").addClass("notranslate");

    $('#lnkSearchMinusIcon').hide();

    //to check which Event is selected
    _searchField = SelectedEvent();

    
    //to get the eventcount
    EventCount();

    // Set system notifications
    DisplaySystemNotifications();
   
    //get 6 top pending tasks and action plans and 3 top event names
    GetTopPendingEventTasks();

    //get top 3 events and display
    GetTopEvents();

    $("#txttodate").on('focus', FocusOnToDate());
}

function FocusOnToDate() {
    if ($('#txtfromdate').val() == '') {
        $("#txttodate").datepicker("destroy");
        $("#txttodate").datepicker();
    }
}

function OnFromChange() {
    var fromDate = new Date($("#txtfromdate").val());
    
    $("#txttodate").datepicker("destroy");
    $("#txttodate").datepicker({
        minDate: $("#txtfromdate").val()
    });
}

    //Search functionality 
    function EventSearch(eventName) {
        _advanceSearch = '';
        _searchField = $(eventName).text();
        _searchField = _searchField.replace(/ /g, '');

        //to hight light selected link
        HightLightSearchLink(eventName);
        DataBindToGrid();
        ReloadGridData();
    }


    //Basic Search functionaity
    function BasicSearch() {
        BindGridOnBasicSearch();
    }

    //Advanced Search functionality
    function AdvancedSearch(isButton) {
        $('#errorSearch').html('');  
    
        if (ValidateAdvanceSearch()) {
            var result = BindGridOnAdvanceSearch();
            if (result != false) {
                if (isButton) {
                    $("#homeAdvanceSearchBox").dialog("destroy");
                    $('#homeAdvanceSearchBox').bind("dialogclose", function (event, ui) {

                    });
                }
            }
        }
    }

    //Clear Controls
    function ClearSearchControls() {
        _advanceSearch = '';
        _basicSearch = '';
        $('#txtSearch').val('');
 
        DeselectSearchLink();
        ClearAdvanceSearchField();
        //OPT_comment --> Can additionally call when is required.
        //ReloadEventGrid(); 
        $('#errorSearch').html('');
        $('#errorSearch').removeClass('WarningMessage');
    }

    //Reload the Grid
    function ReloadEventGrid() {
        _searchField = '';
        _advanceSearch = '';
        ClearAdvanceSearchField();
        DataBindToGrid();
        ReloadGridData();
    }

    //hide the Advance Search Popup Box
    function HideAdvanceSearchPopUp() {

        if (document.getElementById('homeAdvanceSearchBox') != null) {
            document.getElementById('homeAdvanceSearchBox').style.display = 'none'; // IE fixes
        }
        $('#homeAdvanceSearchBox').hide();
        $('#advancedSearchBox').hide();

    }

    //Advanced Search popup
    function OpenAdvnaceSearchPopUp() {
    
        //Clear the basic search data before going to the advanced search.
        // SiteSettingDateFormat()
    
        var dateFormat = localStorage.dateFormat;
        $("#txttodate").datepicker({ dateFormat: dateFormat });
        $("#txtfromdate").datepicker({ dateFormat: dateFormat });
        // $("#txttodate").datepicker();
        // $("#txtfromdate").datepicker();

        $("#ui-datepicker-div").addClass("notranslate");
        $('#errorSearch').html('');
        //madol popup code
        $("#homeAdvanceSearchBox").dialog({
            height: 440,
            width: 500,
            modal: true,
            draggable: true,
            resizable: true,
            position: ['middle', 200]

        });
        setValuesInAdvancedSearch();
    }

    function setValuesInAdvancedSearch() {
        var hdIncident = $('#hiddenAdvancedIncident').val();
        if (hdIncident != null && hdIncident == 1) {
            $('#chkincident').attr('checked', 'checked');
        }
        else {
            $('#chkincident').removeAttr('checked');
        }

        var hdIncvestigation = $('#hiddenAdvancedInvestigation').val();
        if (hdIncvestigation != null && hdIncvestigation == 1) {
            $('#chkinvestigation').attr('checked', 'checked');
        }
        else {
            $('#chkinvestigation').removeAttr('checked');
        }

        var hdAudit = $('#hiddenAdvancedAudit').val();
        if (hdAudit != null && hdAudit == 1) {
            $('#chkAudit').attr('checked', 'checked');
        }
        else {
            $('#chkAudit').removeAttr('checked');
        }

        var hdActionPlan = $('#hiddenAdvancedActionPlan').val();
        if (hdActionPlan != null && hdActionPlan == 1) {
            $('#chkActionPlan').attr('checked', 'checked');
        }
        else {
            $('#chkActionPlan').removeAttr('checked');
        }

        var hdActive = $('#hiddenAdvancedActive').val();
        if (hdActive != null && hdActive == 1) {
            $('#chkActive').attr('checked', 'checked');
        }
        else {
            $('#chkActive').removeAttr('checked');
        }

        var hdCompleted = $('#hiddenAdvancedCompleted').val();
        if (hdCompleted != null && hdCompleted == 1) {
            $('#chkCompleted').attr('checked', 'checked');
        }
        else {
            $('#chkCompleted').removeAttr('checked');
        }

        var hdArchived = $('#hiddenAdvancedArchived').val();
        if (hdArchived != null && hdArchived == 1) {
            $('#chkArchived').attr('checked', 'checked');
        }
        else {
            $('#chkArchived').removeAttr('checked');
        }

        var hdDateTo = $('#hiddenAdvancedDateTo').val();
        if (hdDateTo != null && hdDateTo.length > 0) {
            $('#txttodate').val($('#hiddenAdvancedDateTo').val());
        }
        else {
            $('#txttodate').val('');
        }

        var hdFromTo = $('#hiddenAdvancedDateFrom').val();
        if (hdFromTo != null && hdFromTo.length > 0) {
            $('#txtfromdate').val($('#hiddenAdvancedDateFrom').val());
        }
        else {
            $('#txtfromdate').val('');
        }

        var hdDateRange = $('#hiddenAdvancedDateRange').val();

        if (hdDateRange != null && hdDateRange.length > 0)
            $('#' + hdDateRange).attr('checked', 'checked');
        else {
            $('#chkIncDate30').removeAttr('checked');
            $('#chkIncDate60').removeAttr('checked');
            $('#chkIncDateBetween').removeAttr('checked');
        }
    
    }

    //Method to Set the DateFormat based on Sitesetting
    function SiteSettingDateFormat() {
        //get companyId and UserId
        _serviceUrl = _baseURL + 'Services/SiteSettings.svc/GetDateFormat';
        Ajax(_serviceUrl, _data, eval(SiteSettingDateFormatSuccess), eval(SiteSettingDateFormatFail), 'GetDateFormat');
    }

    //success method for SiteSettingDateFormat

    function SiteSettingDateFormatSuccess(result) {
        var dateFormat = '';
        if (result != null) {
        
            switch (result.d) {

                case 'dd/MM/yyyy':
                    dateFormat = 'dd/mm/yy';
                    break;

                case 'MM/dd/yyyy':
                    dateFormat = 'mm/dd/yy';
                    break;

                case 'MMM dd yyyy':
                    dateFormat = 'M dd yy';
                    break;

                case 'dd-MM-yyyy':
                    dateFormat = 'dd-mm-yy';
                    break;

                case 'dd-M-yy':
                    dateFormat = 'dd-m-yy';
                    break;
                
                default:
                    dateFormat = 'mm/dd/yy';
                    break;
            }
            dateDisplayFormat = dateFormat;
        }
        if (dateFormat != '') {
            $("#txttodate").datepicker({ dateFormat: dateFormat });
            $("#txtfromdate").datepicker({ dateFormat: dateFormat });
        }
    }

    //service failed method
    function SiteSettingDateFormatFail() {
        alert("Error in loading data.");
    }

    // Bind grid on Basic search
    function BindGridOnBasicSearch() {
    
        _basicSearch = $('#txtSearch').val();
        DataBindToGrid();
        ReloadGridData();
    }
    //function to bind grid on advance search on 14-May-2012
    function BindGridOnAdvanceSearch() {
        _advanceSearch = BuildAdvanceSearchFilter();

        if (_advanceSearch != '') {
            DataBindToGrid();
            ReloadGridData();
        }
        else if (_advanceSearch == false)
            return false;
    }

    function SearchOnAdvanceBox() {
        _basicSearch = $('#txtSearch').val();
        $('#errorSearch').html('');
        if (ValidateAdvanceSearch()) {
            var result = BindGridOnAdvanceSearch();
        }
    }

    //Assign filter value
    function EventsData(pdata) {
    
        var params = new Object();
        params.page = pdata.page;
        params.rows = pdata.rows;
        params.sortIndex = pdata.sidx;
        params.sortOrder = pdata.sord;
        params.filter = ((_searchField != '' && _searchField != 'All') ? _searchField : '-1');
        params.basicSearch = ((_basicSearch != '') ? _basicSearch : '-1');
        params.userID = GetUserId();
        params.advanceSearch = ((_advanceSearch != '') ? _advanceSearch : '-1');
        params.companyID = GetCompanyId();
        params.baseURL = _baseURL;
      
        data = JSON.stringify(params);
        serviceUrl = _baseURL + 'Services/Events.svc/EventDetails';
        AjaxPost(serviceUrl, data, eval(EventsDataSuccess), eval(EventsDataFail));
    }

    //success method For Eventsdata
    function EventsDataSuccess(result) {
        var gridId = jQuery("#EventsGrid")[0];
        var jsonResult = JSON.parse(result.d);

        if (jsonResult != null && jsonResult.records > 0) {
            gridId.addJSONData(JSON.parse(result.d));
            var resultCount = ParseToJSON(result.d);
            $('#Grid').show();
      
            $("#divNoRowMessage").hide();
        }
        else
        {
            $('.EventsGrid').hide();
            $('#Grid').hide();
            var eventsMessage = ((jsonResult == null)?'You are not currently working  on any events.':(jsonResult.records == 0)?'There are no events that match your search.':'');
      
            $("#divMessage").html(eventsMessage);
            $("#divNoRowMessage").show();
        }
  
        jqgridCreatePager('pageNavigation', 'EventsGrid', 5);
        _isPageLoad = false;
  
    }

    //get 6 top pending tasks and action plans and 3 top event names
    function GetTopPendingEventTasks() {

        var eventTaskData = new EventTaskDataDetails();
        var data = JSON.stringify(eventTaskData);

        serviceUrl = _baseURL + 'Services/Events.svc/GetTopEventPendingTasks';
        AjaxPost(serviceUrl, data, eval(GetTopEventPendingTasksSuccess), eval(GetTopEventPendingTasksFail));
    }

    function GetTopEvents()
    {

        var eventData = new EventDataDetails();
        var data = JSON.stringify(eventData);

        serviceUrl = _baseURL + 'Services/Events.svc/GetTopEvents';
        AjaxPost(serviceUrl, data, eval(GetTopEventsSuccess), eval(GetTopEventsFail));
    }

    function EventDataDetails() {
        var eventData = {};
        eventData.userId = _userId;
        eventData.companyId = _companyId;
        eventData.baseURL = _baseURL
        return eventData;
    }



    function GetTopEventPendingTasksSuccess(result)
    {
    
        var jsonData = jQuery.parseJSON(result.d);

        var count = 0;
        if (jsonData.length > 0) {

            var getTaskData = '';
        
            for (var i = 0; i < jsonData.length; i++) {
            
                count++;
                //create url and pass to each task - it should redirect to CAP TAB page.           

                var url = _baseURL + 'CorrectiveActionPlan/Edit/' + jsonData[i].eventId + '/' + jsonData[i].correctiveActionId + '?' + jsonData[i].taskId;

                getTaskData += '<br/>';
                getTaskData += 'Task: <a class="anchorTagHome" href="' + url + '">' + jsonData[i].taskTypeName + ' for ' + jsonData[i].correctiveActionName + '</a>';
                getTaskData += '<br/>';
                getTaskData += 'Event Name: ' + jsonData[i].eventName + '</a>';
                getTaskData += '<br/>';
         
                getTaskData += 'Due: ' + ConvertToDate(jsonData[i].taskDueDate);
                getTaskData += '<br/>';           
            }     

            $('#divTasks').html(getTaskData);
            if ($('#rightHomeContent').height() > 900)
                $('#leftSideSection').css('height', $('#rightHomeContent').height());

        }
        else
            //$('#divTasks').html('You do not have any pending tasks.');
            $('#divTasks').html(' ');
    }

    //Convert the json date format to javascript date format 
    function ConvertToDate(dateValue) {
        if (dateValue != null) {
            var jsonDate = new Date(parseInt(dateValue.substr(6)));

            return displayDate = $.datepicker.formatDate(localStorage.dateFormat, jsonDate);//change with site date setting 
        }
    }

    function EventTaskDataDetails()
    {
        var eventTaskData = {};
        eventTaskData.userId = _userId;
        eventTaskData.companyId = _companyId;
        return eventTaskData;
    }

    function GetTopEventPendingTasksFail()
    {
        alert('Service Fail.');
    }

    //service Fail
    function EventsDataFail() {

        alert('Service failed.');
    }

    function GetTopEventsSuccess(result)
    {
        var jsonData = jQuery.parseJSON(result.d);
        var count = 0;
        if (jsonData.length > 0) {
            var getEventData = '';
            for (var i = 0; i < jsonData.length; i++) {
                count++;
                getEventData += '<a class="anchorTagHome" href="' + jsonData[i].RecentEventURL + '">' + jsonData[i].EventName + '</a>';
                getEventData += '<br/>';
            }
            $('#divEvents').html(getEventData);
        }
        else {
            //$('#divEvents').html('You dont have any recent events.');
            $('#divEvents').html('');
        }

    }

    function GetTopEventsFail(msg)
    {
        alert('Service failed.');
    } 

    //Bind Grid 
    function DataBindToGrid() {
        var $grid = $("#EventsGrid"), gridcol;

        jQuery("#EventsGrid").jqGrid({
            datatype: function (pdata) { EventsData(pdata) },
            colNames: ['Name', 'Current Stage', 'Created By', 'Incident Date', 'Last Modified'],
            colModel: [{ name: 'Name', index: 'Name', width: 240, align: 'left', sortable: true, classes: 'notranslate'},//, formatter: TextFormatter
                    { name: 'EventType', index: 'EventType', width: 120, align: 'center', sortable: true, resizable: true },
                            { name: 'CreatedBy', index: 'CreatedBy', width: 127, align: 'center', sortable: true, resizable: true, classes: 'notranslate' },
                            { name: 'EventDate', index: 'EventDate', width: 120, align: 'center', sortable: true, resizable: true },
                            { name: 'DateModified', index: 'DateModified', width: 130, align: 'center', sortable: true, resizable: false },
            ],
            toppager: false,
            emptyrecords: 'There are no events that match your search.',
            // Grid total width and height         
            height: '100%',
            // width:'100%',
            caption: 'Events',
            rowNum: 50,
            pager: jQuery('#pageNavigation'),
            viewrecords: true,
            hidegrid: false,
            sortname: 'DateModified',
            sortorder: "desc",   
            forceFit: true,
            ondblClickRow: function (rowId) {
                var rowData = jQuery(this).getRowData(rowId);
                var title = rowData["Name"];
                var url = $(title).attr('href');
                window.location = url;
            },
            loadComplete: function () {
                jqgridCreatePager('pageNavigation', 'EventsGrid', 5);

            },
            autowidth: true,
 
     
        });

        gridcol = $grid.jqGrid('getGridParam', 'colModel');
        if (gridcol != undefined) {
            $('#gbox_' + $.jgrid.jqID($grid[0].id) +
            ' tr.ui-jqgrid-labels th.ui-th-column').each(function (i) {
                var cmi = gridcol[i], colName = cmi.name;

                if (cmi.sortable !== false) {
                    // show the sorting icons
                    $(this).find('>div.ui-jqgrid-sortable>span.s-ico').show();
                }
            });
        }
        //debugger;
        ////for zoom / Ctrl ++ - screen should fit within the page only
        //var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        //var isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
        ////alert(isSafari);
        //if (is_chrome || isSafari) {
        //    alert("safari");
        //    $(window).bind('resize', function () {

        //        $("#EventsGrid").setGridWidth($('.homeContent').width() - 40 );
        //    });
        //}
        //else {
        $(window).bind('resize', function () {
           
            $("#EventsGrid").setGridWidth($('.homeContent').width() - 15);
        });
        //}
 
    }

    function DeselectSearchLink() {

        $('#' + _searchLink).removeClass("AnchorBoldTag");
        $('#' + _searchLink).addClass("AnchorTag");

        $('#lnkEventAll').addClass("AnchorBoldTag");
        $('#lnkEventAll').removeClass("AnchorTag");

    }

    function Encrypt(val) {
        var actual = val;
        var key = 100; //Any integer value
        var result = "";
        for (i = 0; i < actual.length; i++) {
            result += String.fromCharCode(key ^ actual.charCodeAt(i));
        }
        return result;
    }

    function TextFormatter(cellvalue, options, rowObject) {
        var opt = options.rowId;
        var Data = opt.split(',');

        var result = "<a href=" + _baseURL + "Event/" + Data[1].replace(/ /g, '') + "/Details-1/" + Data[0] + "/Edit >" + cellvalue + "</a>";
        _eventId = 0;
        return result;
    }

    // Redirect to the Screen Based on EventType and EventID
    function RedirectPage(eventTypeId) {
        var eventIdArray = eventTypeId.split(',');
        var rowData = jQuery("#EventsGrid").getRowData(eventTypeId);
        var selectedEventName = rowData['Name'];
        var id = eventIdArray[0];

        if (_virtualDirectoryPath != null) {
            window.location.href = _baseURL + "Event/" + eventIdArray[1].replace(/ /g, '') + "/Details-1/" + id + "/Edit";
        }
    }

    // Reload Grid
    function ReloadGridData() {
        jQuery("#EventsGrid").trigger("reloadGrid");
    }

    // Advance search filter
    function BuildAdvanceSearchFilter() {

        var advanceSearchContent = '';

        var isIncident = $('#chkincident').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedIncident').val(isIncident);
   
        var isInvestigation = $('#chkinvestigation').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedInvestigation').val(isInvestigation);

        var isAudit = $('#chkAudit').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedAudit').val(isAudit);

        var isActionPlan = $('#chkActionPlan').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedActionPlan').val(isActionPlan);

        var isIncdt30 = $('#chkIncDate30').attr('checked') ? 1 : 0;
        var isIncdt60 = $('#chkIncDate60').attr('checked') ? 1 : 0;
        var isIncdtBetween = $('#chkIncDateBetween').attr('checked') ? 1 : 0;

        if (isIncdt30 == 1)
            $('#hiddenAdvancedDateRange').val('chkIncDate30');
        else if (isIncdt60 == 1)
            $('#hiddenAdvancedDateRange').val('chkIncDate60');
        else if (isIncdtBetween == 1)
            $('#hiddenAdvancedDateRange').val('chkIncDateBetween');
      

        //var fromDate = $.datepicker.formatDate("mm/dd/yy", $("#txtfromdate").datepicker("getDate"));
        var fromDate = $("#txtfromdate").val();
        $('#hiddenAdvancedDateFrom').val(fromDate);

        //var toDate = $.datepicker.formatDate("mm/dd/yy", $("#txttodate").datepicker("getDate"));
        var toDate = $("#txttodate").val();
        $('#hiddenAdvancedDateTo').val(toDate);

   

        var isActive = $('#chkActive').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedActive').val(isActive);

        var isCompleted = $('#chkCompleted').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedCompleted').val(isCompleted);

        var isArchieved = $('#chkArchived').attr('checked') ? 1 : 0;
        $('#hiddenAdvancedArchived').val(isArchieved);

        $('#errorSearch').html('');

        //build the logic to show error message if nothing is selected from Advanced search
        if (isIncident == 0 && isAudit == 0 && isIncdt30 == 0 && isIncdt60 == 0 && isIncdtBetween == 0 && isActive == 0 && isCompleted == 0 && isArchieved == 0 && isInvestigation == 0 && isActionPlan == 0) {
     
            $('#errorSearch').html('Please select any search criteria to Search.');
            $('#errorSearch').addClass('WarningMessage');
            return false;
        }
        else {
            //$('#Grid').show();
            $('#ErrShowGrid').html('');
            $('#ErrShowGrid').removeClass('WarningMessage');
            $("#ReloadGrid").hide();

            //For Event type
            advanceSearchContent = ((isIncident != 0) ? advanceSearchContent + 'chkinc' + '#' + '1' + ',' : advanceSearchContent + 'chkinc' + '#' + '0' + ',');
            advanceSearchContent = ((isInvestigation != 0) ? advanceSearchContent + 'chkinv' + '#' + '1' + ',' : advanceSearchContent + 'chkinv' + '#' + '0' + ',');
            advanceSearchContent = ((isAudit != 0) ? advanceSearchContent + 'chkaudit' + '#' + '1' + ',' : advanceSearchContent + 'chkaudit' + '#' + '0' + ',');
            advanceSearchContent = ((isActionPlan != 0) ? advanceSearchContent + 'chkaction' + '#' + '1' + ',' : advanceSearchContent + 'chkaction' + '#' + '0' + ',');

            //For Incident Date

            advanceSearchContent = ((isIncdt30 != 0) ? advanceSearchContent + 'isIncdt30' + '#' + '1' + ',' : advanceSearchContent + 'chkincdt30' + '#' + '0' + ',');
            advanceSearchContent = ((isIncdt60 != 0) ? advanceSearchContent + 'chkincdt60' + '#' + '1' + ',' : advanceSearchContent + 'chkincdt60' + '#' + '0' + ',');
            advanceSearchContent = ((isIncdtBetween != 0) ? advanceSearchContent + 'chkIncDateBetween' + '#' + '1' + ',' : advanceSearchContent + 'chkIncDateBetween' + '#' + '0' + ',');

            //For incident between Date
            advanceSearchContent = ((fromDate != '') ? advanceSearchContent + 'fromdate' + '#' + fromDate + ',' : advanceSearchContent + 'fromdate' + '#' + '-1' + ',');
            advanceSearchContent = ((toDate != '') ? advanceSearchContent + 'todate' + '#' + toDate + ',' : advanceSearchContent + 'todate' + '#' + '-1' + ',');

            var hasStatus = (isActive == 1 || isCompleted == 1 || isArchieved == 1);

            //For Event Status
            if (hasStatus) {

                advanceSearchContent += ((isActive == 1) ? 'status' + '#' + 'Active' + ',' : '');
                advanceSearchContent += ((isCompleted == 1) ? 'status' + '#' + 'Completed' + ',' : '');
                advanceSearchContent += ((isArchieved == 1) ? 'status' + '#' + 'Archived' + ',' : '');
            }
            else {
                advanceSearchContent += 'status' + '#' + '-1' + ',';
            }

            if (advanceSearchContent.lastIndexOf(',') > -1) {
                advanceSearchContent = advanceSearchContent.substring(0, advanceSearchContent.length - 1);
            }
        }
        return advanceSearchContent;
    }

    //validation For Advance search
    function ValidateAdvanceSearch() {
   
        $('#EventErrorMsg').html('');
        $('#EventErrorMsg').removeClass('WarningMessage');
        var isIncidentdt = $('#chkIncDateBetween').attr('checked') ? 1 : 0;

        var fromDate = $('#txtfromdate').val();
        var toDate = $('#txttodate').val();

        var message = '';
        var isDateSelected = true;

        if (isIncidentdt == 1 && (fromDate == '' && toDate == '')) {
            isDateSelected = false;
            message = 'From date and to date is required.'
        }
        else if (isIncidentdt == 0 && (fromDate != '' && toDate != '')) {
            isDateSelected = false;
            message = 'Please select between Date.';
        }
        else if (isIncidentdt == 0 && (fromDate != '' || toDate != '')) {
            isDateSelected = false;
            message = 'Please select between Date.';
        }

        if (!isDateSelected) {
            $('#errorSearch').html(message);
            $('#errorSearch').addClass('WarningMessage');
        }
        return isDateSelected;
    }

    //clear function for advance search
    function ClearAdvanceSearchField() {
   
        CheckAllEventType();

        $('#txtfromdate').val('');
        $('#txttodate').val('');

        $('#chkCompleted').attr('checked', false);
        $('#chkArchived').attr('checked', false);

        $('#chkIncDate30').attr('checked', false);
        $('#chkIncDate60').attr('checked', false);
        $('#chkIncDateBetween').attr('checked', false);

        $('#hiddenAdvancedIncident').val('1');
        $('#hiddenAdvancedInvestigation').val('1');
        $('#hiddenAdvancedAudit').val('1');
        $('#hiddenAdvancedActionPlan').val('1');
        $('#hiddenAdvancedActive').val('1');
        $('#hiddenAdvancedCompleted').val('0');
        $('#hiddenAdvancedArchived').val('0');
        $('#hiddenAdvancedDateTo').val('');
        $('#hiddenAdvancedDateFrom').val('');
        $('#hiddenAdvancedDateRange').val('');
    }

    //function to make search scope checkbox checked in advance search
    function CheckAllEventType() {

        $('#chkincident').attr('checked', 'checked');
        $('#chkinvestigation').attr('checked', 'checked');
        $('#chkAudit').attr('checked', 'checked');
        $('#chkActionPlan').attr('checked', 'checked');
        $('#chkActive').attr('checked', 'checked');
    }


    //Method to get the Eventcount
    function EventCount() {
    
        var userId = GetUserId();
        var data = '&userID=' + userId;
        if (userId != null && userId != undefined) {
            var serviceUrl = _baseURL + 'Services/Events.svc/EventCount';
            Ajax(serviceUrl, data, eval(EventCountSuccess), eval(EventCountFail), 'EventCount');
        }
    }

    //success method for EventCount
    function EventCountSuccess(result) {

        var jsonData = jQuery.parseJSON(result.d);
        $('#lblEventcount').text('Total:' + jsonData[0]);

        //added for grid default size
        var eventCount = parseInt(jsonData[1]);
        if (eventCount <= 50) {
            $('#EventsGrid').setGridParam({ rowNum: eventCount });
        }
        else {
            $('#EventsGrid').setGridParam({ rowNum: 50 });
            $('#EventsGrid').setGridParam({ rowList: [50, 100] }).trigger("reloadGrid");;
        }

        //Bind grid 
        DataBindToGrid();

        //Advance search  
        CheckAllEventType();

    }

    //service fail
    function EventCountFail() {

        alert("Error in loading data.");
    }

    //to hight light selected link
    function HightLightSearchLink(currentId) {

        $('#lnkEventAll').removeClass("AnchorBoldTag");
        $('#lnkEventAll').addClass("AnchorTag");

        if (_searchLink != '' && _searchLink != undefined) {
            var selectedValue = ((_searchLink == 'lnkIncident') ? '#lnkIncident' : (_searchLink == 'lnkInvestigation') ? '#lnkInvestigation' : (_searchLink == 'lnkAudit') ? '#lnkAudit' : (_searchLink == 'lnkCAP') ? '#lnkCAP' : (_searchLink == 'lnkEventAll') ? '#lnkEventAll' : '');
            $(selectedValue).removeClass("AnchorBoldTag");
            $(selectedValue).addClass("AnchorTag");
        }

        jQuery(currentId).addClass("AnchorBoldTag");
        jQuery(currentId).removeClass("AnchorTag");
        _searchLink = $(currentId)[0].id;
    }

    //Get the selected event when the user click the link
    function SelectedEvent() {

        var url = window.location.href;
        var moduleName = '';
        var moduleId = url.split('/')[url.split('/').length - 1];

        if (!isNaN(moduleId)) {       
            moduleName = ((moduleId == 1) ? "UninvestigatedIncidents" : (moduleId == 2) ? "Investigations" : (moduleId == 3) ? "Audits" : (moduleId == 4) ? "ActionPlans" : '');
        }
        return moduleName;
    }

    // Display system notfications
    function DisplaySystemNotifications() {
    
        var companyId = GetCompanyId();

        var NotificaionDetails = new CurrentNotificationData(companyId);

        _data = JSON.stringify(NotificaionDetails);
        serviceUrl = _baseURL + 'Services/SysNotifications.svc/GetCurrentNotifications';
        AjaxPost(serviceUrl, _data, eval(GetSystemNotificationsSuccess), eval(GetSystemNotificationsFail));
    }
    function CurrentNotificationData(companyId) {    
        this.companyId = companyId;
    }

    function GetSystemNotificationsSuccess(result) {

        var jsonResult = jQuery.parseJSON(result.d);

        if (jsonResult != null && jsonResult != [] && jsonResult.rows != [] && jsonResult != undefined) {

            var title = jsonResult.rows[0].cell[0];
            var message = jsonResult.rows[0].cell[1];

            for (var i = 0; i < jsonResult.rows.length; i++) {

                var title = jsonResult.rows[i].cell[0];
                var message = jsonResult.rows[i].cell[1];
                $("#lblBlueContent2").append("<hr><B>" + title + " </B><br>" + message.replace(/\n/g, "<br />") + "<br>");
            }
        }
        NotificationBackground();
    }

    function NotificationBackground() {

        if ($('.BlueContentArea').height() <= 500) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification0');
        }
        else if ($('.BlueContentArea').height() > 400 && $('.BlueContentArea').height() < 700) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification0_5');
        }
        else if ($('.BlueContentArea').height() > 700 && $('.BlueContentArea').height() < 1000) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification1');
        }
        else if ($('.BlueContentArea').height() > 1000 && $('.BlueContentArea').height() < 2000) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification2');
        }
        else if ($('.BlueContentArea').height() > 2000 && $('.BlueContentArea').height() < 3000) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification3');
        }
        else if ($('.BlueContentArea').height() > 3000 && $('.BlueContentArea').height() < 4000) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification4');
        }
        else if ($('.BlueContentArea').height() > 4000 && $('.BlueContentArea').height() < 5000) {
            $('.BlueContentArea').addClass('BlueContentAreaNotification5');
        }
        else {
            $('.BlueContentArea').addClass('BlueContentAreaNotification6');
        }
    }
    function GetSystemNotificationsFail(result) {
    }

    //Clear the basic search box data.
    function BasicSearchClear() {

        $("#txtSearch").attr("value", '');
        _basicSearch = '';
    }

    // Company Subscription Expire days
    function SubscriptionExpireMsg() {
    
        var isSU = $('#hdnSUDivision').val();
        if (isSU == 1) {
            var companyId = GetCompanyId();
            var CompanyDetails = new SubscriptionData(companyId);
            _data = JSON.stringify(CompanyDetails);;

            var serviceUrl = _baseURL + 'Services/CompanyDetail.svc/SubscriptionExpireDays';
            PostAjax(serviceUrl, _data, eval(SubscriptionExpireSuccess), eval(SubscriptionExpireFail));
        }
    }
    function SubscriptionData(companyId) {
        this.companyID = companyId;
    }

    function SubscriptionExpireSuccess(result) {
        if (result != null && result.d != null && result.d <= 30) {
            var days = result.d;
            var dayStr = '';
            dayStr = (days > 1)? days + ' days':days + ' day';
            if (days > 0)
                $("#lblBlueContent2").html("<hr><div  style='background:yellow; color:black; padding:5px;margin-bottom:10px;'><B>Subscription Expiring Soon!</B><br><span>Your subscription to TapRooT® software is expiring in " + dayStr + ". Please contact <a href='mailto:support@taproot.com'>support@taproot.com</a></span></div>");
            else if (days == 0)
                $("#lblBlueContent2").html("<hr><div  style='background:yellow; color:black; padding:5px;margin-bottom:10px;'><B>Subscription Expiring Soon!</B><br><span style='background:yellow; color:black;text-align:justify;'>Your subscription to TapRooT® software will expire today. Please contact <a href='mailto:support@taproot.com'>support@taproot.com</a></span></div>");
            else
                $("#lblBlueContent2").html("<hr><div  style='background:yellow; color:black; padding:5px;margin-bottom:10px;'><B>Subscription Expired!</B><br><span style='background:yellow; color:black;text-align:justify;'>Your subscription to TapRooT® software has been expired. Please contact <a href='mailto:support@taproot.com'>support@taproot.com</a></span></div>");
        }
    }
    function SubscriptionExpireFail(result) {
    }