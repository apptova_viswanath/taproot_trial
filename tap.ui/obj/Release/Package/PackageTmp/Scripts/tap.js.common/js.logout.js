﻿/* Logout functionality  */
$(document).ready(function () {

    $('#anc_logout').click(function () {
            
        NavigateToLogInPage();
        delete sessionStorage.Change;
    });

});

//Navigate to the Login page
function NavigateToLogInPage() {
    window.location = "/TFSTaprootWEB/tap.usr/Login.aspx";
}