﻿var _fields = new Object();
var _undoStack = new Array;

var _isEventData = true;
var _action = 0;
var _locationAction = 0;
var _customListAction = 0;

var DROPDOWNLIST = 'DropdownList';
var SPAN = 'span';
var LABEL = 'label';
var DROPDOWNLIST = 'DropdownList';
var CONTROL_TYPE = 'data-entity-controltype';
var MESSAGE = 'Please specify ';
var fieldId = "";

function SaveFields(e) {

     _onMouseDownCount = 0;

    //Store the initial values to use in UNDO and UNDOALL.
    _initialData = e;

   

    //Get and differentiate the field id based on controls.
    if (e.target.type == "radio") {
        fieldId = e.target.name; //radio button group name.
        _fieldValue = e.target.value;
    }
    else {
        fieldId = e.target.id;
        _fieldValue = $('#' + fieldId).val();
    }

    var isDatePickerField = $('#' + fieldId).hasClass('hasDatepicker');
    var isMonthYearClicked = (e.type == "blur");

    if (isDatePickerField && isMonthYearClicked) {
        return;
    }

    //Restrict the onBlur event fire for particular buttons
    if (fieldId == 'btnSelectEventLocation' || fieldId == 'btnSelectEventClassification' || fieldId == 'lblEventLocationData' || fieldId == "btnEventLocationCancel" || fieldId == "btnEventClassificationCancel" || fieldId == "btnCustomCancel" || fieldId == "btnCustomTreeCancel" || fieldId == "btnUsrFieldTreeOk" || fieldId == 'btnSelectEventClassificationTest')
        return;

   
    if (_fields[fieldId] != undefined) {
        if (_fields[fieldId].newValue[_fields[fieldId].newValue.length - 1] == _fieldValue)
            return;
   

    // Validation for fields
    if (fieldId == 'body_txtEventName' && _fieldValue == "") {
        $('#' + fieldId).val(_fields[fieldId].previousValue[_fields[fieldId].previousValue.length - 1]);
        return;
    }
    if (fieldId == 'body_txtTime' && _fieldValue != "" && FormatMilitaryTime('body_txtTime') == false) {
        return false;
    }

    //phone number validation  
    if (e.target.type == 'text' && $('#' + fieldId)[0] != undefined) {
        // var dataTypeValidation = $('#' + fieldId)[0].dataset.entityDatatype;
        var dataTypeValidation = $('#' + fieldId)[0].getAttribute('data-entityDatatype');
        var fieldLabel = $(fieldIdSelector).parent().prev().find(LABEL);
        var labelText = $(fieldLabel).clone().children().remove().end().text();

        var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');

        if (dataTypeValidation == 'Int') {
            var a = $('#' + fieldId)[0].value;

            //allow only number(s)
            var filter = /^\d+$/;
            if (filter.test(a) || (a == '' && !isRequiredField)) {
                ShowHideWarningMessage(fieldId, 'Please specify a valid number', false);

            }
            else {
                ShowHideWarningMessage(fieldId, 'Please specify a valid number', true);
                return false;
            }
        }
    }

    ////////////////////////

    // Validation for custom fields in details tab
    if ($('#hdnIsDetailsTab').val() == '1') { // This check will indicate - fire for only the required field of details tab, not any other custom tabs

        var fieldIdSelector = '#' + fieldId;
        var fieldLabel = $(fieldIdSelector).parent().prev().find(LABEL);
        var labelText = $(fieldLabel).clone().children().remove().end().text();

        var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');
        var isDropDownField = ($(fieldIdSelector).attr(CONTROL_TYPE) == DROPDOWNLIST);
        var isFieldValueEmpty = (isDropDownField) ? (_fieldValue == "0") : (_fieldValue == "");

        var displayErrorMessage = (isRequiredField && isFieldValueEmpty) ? true : false;
        ShowHideWarningMessage(fieldId, MESSAGE + labelText, displayErrorMessage);

        if (displayErrorMessage) {
            return false;
        }

    }

    //Call for generic save for fields
    GenericSaveFields(_fields, fieldId, _fieldValue);

    //track the changes
    if (_fields[fieldId] != undefined){
        _fields[fieldId].previousValue.push(_fields[fieldId].newValue[_fields[fieldId].newValue.length - 1]);
    }

    //_fields[fieldId].newValue = _fieldValue;
    if (e.target.type == "radio")
        _fields[fieldId].newValue.push(_fieldValue);
    else
        _fields[fieldId].newValue.push($('#' + e.target.id).val());

    //Create an object to pass individual field info into the Undo stack
    var field = {};
    field.fieldName = fieldId;
    field[fieldId] = {};
    field[fieldId] = _fields[fieldId];

    //Pass the field info into the stack
    _undoStack.push(field);
    $('#ancUndo').show();

    //Check for the modified values to show the UNDOALL link
    if (_undoStack.length > 1)
        $('#ancUndoAll').show();
    }
    
}

function GenericSaveFields(fields, currentFieldId, currentFieldValue) {

    if (fields[currentFieldId] != undefined){
    operationMode = fields[currentFieldId].mode;
    entityType = fields[currentFieldId].entitytype;
    entitySet = fields[currentFieldId].tableName;
    fieldName = fields[currentFieldId].fieldName;
    fieldValue = currentFieldValue;
    primaryKeyField = fields[currentFieldId].pkFieldName;
    eventID = _eventId;
    dataType = fields[currentFieldId].dataType;
    queryField = fields[currentFieldId].queryField;
    queryFieldValue = fields[currentFieldId].queryFieldValue;
    secondQueryField = fields[currentFieldId].secondQueryField;

    eventTypePosition = 3;
    eventIdPosition = 1;

    eventType = _eventType == '' ? EventTypeFromUrlSelect(eventTypePosition, eventIdPosition) : _eventType; //Get event type    
    
    var genericSaveData = new GenericSaveDetails(operationMode, eventType, entityType, entitySet, fieldName, fieldValue, primaryKeyField, eventID,
                     queryField, dataType, queryFieldValue, secondQueryField, GetCompanyId());
    var data = JSON.stringify(genericSaveData);
    serviceurl = _baseURL + 'Services/GenericSaveFields.svc/GenericSave';
    AjaxPost(serviceurl, data, eval(GenericSave_Succ), eval(GenericSave_Err));
    }
}

function GenericSaveDetails(operationMode, eventType, entityType, entitySet, fieldName, fieldValue, primaryKeyField, eventID, queryField,
                         dataType, queryFieldValue, secondQueryField, companyId) {

    this.mode = operationMode;
    this.isEventData = _isEventData;
    this.entityType = entityType;
    this.entitySet = entitySet;
    this.fieldName = fieldName;
    this.fieldValue = fieldValue;
    this.primaryKeyField = primaryKeyField;
    this.eventId = eventID;
    this.dataType = dataType;
    this.queryField = queryField;
    this.queryFieldValue = queryFieldValue;
    this.secondQueryField = secondQueryField;
    this.companyID = GetCompanyId();
    this.userId = $('#hdnUserId').val();
}

function GenericSave_Succ() {

}

function GenericSave_Err() {

}

//Functionality for Undo link click
function EventUndo() {

    var field = {};
    var fieldName = {};

    if (_action == 0)
        _action = 1;

    if (_locationAction == 0)
        _locationAction = 1;

    if (_customListAction == 0)
        _customListAction = 1;

    //Remove the latest field info from the stack on UNDO
    field = _undoStack.pop();
    fieldName = field.fieldName;

    /*---------------- Saving the previous changes ---------------------------------------------------  */

    //Saving back the previous changes in database.
    if (field[fieldName].controlType == 'Select multiple from list' || field[fieldName].controlType == 'Select one from list') {

        //Undo based on the Lists.
        SystemListSelection(field, fieldName);
    }
    else {

        //While inserting on undo, delete the record from Database.
        if (_fields[fieldName].mode == 'Insert')
            _fields[fieldName].mode = 'Update'; //Used Update word in the logic to delete the record of custom fields in DB.

        //Rollback the changes in DB
        GenericSaveFields(_fields, fieldName, field[fieldName].previousValue[field[fieldName].previousValue.length - 1]);

        //Navigation breadcrumb Undo.
        if (field[fieldName].controlType == 'TextBox' && fieldName == 'body_txtEventName') {
            $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').text(EventByEventtypeGet(_eventType) + ' > ' + field[fieldName].previousValue[field[fieldName].previousValue.length - 1] + ' > Details');
            $(document).attr('title', 'TapRooT® - ' + field[fieldName].previousValue[field[fieldName].previousValue.length - 1]);
        }


        //Rollback the mode after initial save.
        if (field[fieldName].previousValue.length < 1)
            _fields[fieldName].mode = 'Insert'

    }

    /*---------------- End of saving the previous changes ------------------------------------------  */


    /*---------------- Dispaying the previous changes --------------------------------------------------  */

    //Display back the previous values.
    if (field[fieldName].controlType == 'Select multiple from list' || field[fieldName].controlType == 'Select one from list') {

        //Display the previous values for lists.
        //$('#' + fieldName).html(field[fieldName].previousValue[field[fieldName].previousValue.length - 1]);

        var indexValue = field[fieldName].previousValue.length > 1 ? 2 : 1;
        $('#' + fieldName).html(field[fieldName].previousValue[field[fieldName].previousValue.length - indexValue]);
    }
    else if (field[fieldName].controlType == 'Radio Button') {

        //Remove or (replace the previous) checked value.
        if (field[fieldName].previousValue.length > 0 && field[fieldName].previousValue[field[fieldName].previousValue.length - 1] == '') {
            $("input[name=" + fieldName + "]").removeAttr('checked');
        }
        else
            $("input[name=" + fieldName + "][value=" + field[fieldName].previousValue[field[fieldName].previousValue.length - 1] + "]").attr('checked', 'checked');
    }
    else {
        //Display the previous value on Undo
        $('#' + fieldName).val(field[fieldName].previousValue[field[fieldName].previousValue.length - 1]);
    }

    /*---------------- End of dispaying the previous changes ---------------------------------------------  */
    //Removes the Error Icon if exists--for Details edit page.
    if (field[fieldName].previousValue[field[fieldName].previousValue.length - 1] != '') {

        //Hide the error icon.
        var fieldIdSelector = field.fieldName;
        ShowHideWarningMessage(fieldIdSelector, '', false);

    }
    /////////////////////////////

    /*---------------- Tracking the changes ---------------------------------------------------  */

    //track back the changes.
    _fields[fieldName].previousValue.pop();
    if (_fields[fieldName].previousListId != null)
        _fields[fieldName].previousListId.pop();

    /*---------------- End of tracking the changes -------------------------------------------  */


    //Check for the modified values to hide the links, UNDO and UNDOALL
    if (_undoStack.length == 0) {
        $('#ancUndo').hide();
        $('#ancUndoAll').hide();
    }


}


//Functionality for UndoAll link click
function EventUndoAll() {

    var undo = 'ancUndo';
    var undoall = 'ancUndoAll';

    //Clear all the changes
    UndoAllChanges(_fields, undo, undoall);
}

//For UNDO ALL changes
function UndoAllChanges(fields, undo, undoall) {

    var locationValue = 'lblEventLocationData';
    var classificationValue = 'lblEventClassificationData';

    /* ---------- Saving and displaying the original values of the fields. -------------------*/

    for (var index in fields) {

        if (fields[index].controlType == 'Select multiple from list' || fields[index].controlType == 'Select one from list') { //For Lists 

            //Save the original Location and Classification values in DB.
            if (index == locationValue) {
                SaveLocation(fields[index].originalListId);

                //Assigned the previous location/Classification id to check mark on in the treeview. 
                _locationCheckId = _fields[locationValue].originalListId;
            }

            if (index == classificationValue) {
                SaveClassification(fields[index].originalListId);

                _classificationIdsArray = [];
                _classificationIdsArray.push(_fields[classificationValue].originalListId);
            }

            if (index != locationValue && index != classificationValue) {
                
                GenericSaveFields(fields, index, fields[index].originalListId);  //Restore the original custom list value in DB.              
                $('#' + fields[index].hiddenFieldId).val(fields[index].originalListId);

                $('#hdn' + _treeFieldId).val(fields[index].originalListId);
            }

            //Replace the original values.
            _fields[index].previousValue = [];
            _fields[index].previousListId = [];

            _fields[index].previousValue.push(_fields[index].originalValue);
            _fields[index].previousListId.push(_fields[index].originalListId);

            //Displaying the original values for static lists.
            $('#' + index).html(fields[index].originalValue);

        }
        else { //For Input controls

            //Rollback to the original records in DB.
            GenericSaveFields(fields, index, fields[index].originalValue);

            //Replace with the original values.
            fields[index].previousValue = [];
            fields[index].previousValue.push(fields[index].originalValue);

            //Display the original values for input controls.
            if (fields[index].controlType == 'Radio Button') { //For Radio buttons

                //Remove or (replace the original) checked value .
                if (fields[index].originalValue == "")
                    $("input[name=" + index + "]").removeAttr('checked');
                else
                    $("input[name=" + index + "][value=" + fields[index].originalValue + "]").attr('checked', 'checked');

            }
            else {//For textbox and textarea       

                if (fields[index].controlType == 'TextBox' && index == 'body_txtEventName') {
                    $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').text(EventByEventtypeGet(_eventType) + ' > ' + fields[index].originalValue + ' > Details');
                    $(document).attr('title', 'TapRooT® - ' + fields[index].originalValue);
                }

                $('#' + index).val(fields[index].originalValue);
            }


        } //End of else part.

    } //End of for loop.

    /* ---------- End of saving and displaying the original values of the fields. -------------------*/

    //Hide the links, UNDO and UNDOALL
    $('#' + undo).hide();
    $('#' + undoall).hide();

    //Clear the stack.
    _undoStack = [];

} //End of UNDO ALL changes

function OnDateChange(dateSelectors) {

    $(dateSelectors).change(function (e) {

        //Auto save the time on Edit mode.        
        if (_eventId != '0' && _eventId != -1) {

            SaveFields(e);
        }

    });
}

//called on evt.js and js.cust.fld.js page load.
function GetInitialValues(elements) {

    //Modified the logic to suit for Radio buttons also.
    for (var index = 0; index < elements.length; index++) {
        var found = false;
        var id = "";

        if (elements[index].type == "radio") {
            id = elements[index].name;

            //Logic for assigning properties of radio button group.
            if (_fields[elements[index].name] != undefined) {
                found = true;
            }
        }
        else
            id = elements[index].id;

        if (!found) { // To restrict the loop run only once for a group of radio buttons.

            _fields[id] = {};
            _fields[id].previousValue = [];
            _fields[id].newValue = [];
            _fields[id].previousListId = [];

            if (elements[index].type == "radio") {

                var initialValue = $("input[name='" + id + "']:checked").val() == undefined ? "" : $("input[name='" + id + "']:checked").val();
                _fields[id].originalValue = initialValue;
                _fields[id].previousValue.push(initialValue);
                _fields[id].newValue.push(initialValue);
            }
            else {
                _fields[id].originalValue = $("#" + id).val();
                _fields[id].previousValue.push($("#" + id).val());
                _fields[id].newValue.push($("#" + id).val());
            }

            _fields[id].mode = elements[index].attributes["data-mode"].textContent;
            _fields[id].type = elements[index].attributes["data-type"].textContent;
            _fields[id].tableName = elements[index].attributes["data-entity-set"].textContent;
            _fields[id].entitytype = elements[index].attributes["data-entity-type"].textContent;
            _fields[id].pkFieldName = elements[index].attributes["data-entity-property-pk"].textContent;
            _fields[id].fieldName = elements[index].attributes["data-entity-property"].textContent;
            _fields[id].dataType = elements[index].attributes["data-entity-datatype"].textContent;
            _fields[id].queryField = elements[index].attributes["data-entity-queryfield"].textContent;
            _fields[id].queryFieldValue = elements[index].attributes["data-entity-queryfield-value"].textContent;
            _fields[id].secondQueryField = elements[index].attributes["data-entity-sec-queryfield"].textContent;
            _fields[id].controlType = elements[index].attributes["data-entity-controltype"].textContent;

            //For custom lists.
            if (elements[index].tagName == "LABEL") {

                if (_fields[id].previousValue.length > 0 && _fields[id].previousValue[0] == '')
                    _fields[id].previousValue.pop();

                _fields[id].originalValue = elements[index].innerHTML;
                _fields[id].previousValue.push(elements[index].innerHTML);

                if (elements[index].attributes['data-entity-hiddenFieldId'] != null) {
                    _fields[id].hiddenFieldId = elements[index].attributes["data-entity-hiddenFieldId"].textContent;
                }

                //if no values for custom fields in DB
                if (_fields[id].mode == 'Update') {

                    _fields[id].originalListId = elements[index].attributes["data-entity-displayId"].textContent;
                    _fields[id].previousListId.push(elements[index].attributes["data-entity-displayId"].textContent);
                }

                if (_fields[id].mode == 'Insert' && _fields[id].previousValue[0] == '' && elements[index].attributes["data-entity-displayId"] == undefined) {
                    _fields[id].previousListId.push('');
                    _fields[id].originalListId = '';
                }

            }
        }

    }
    return _fields;
}

//Calling blur event for only the input fields
function OnBlurCheck(inputControls) {

    $(inputControls).blur(function (e) {
        //Check for the empty text and remove the warning class if exists.
        //Exception for the required fields-Deepa and Kasi commented
        var fieldIdSelector = '#' + e.target.id;
        var fieldLabel = $(fieldIdSelector).parent().prev().find(LABEL);
        var isRequiredField = ($(fieldLabel).find(SPAN).text() == '*');


        if (!isRequiredField && e.target.value == '') {
            ShowHideWarningMessage(e.target.id, '', false);

        }

        SaveFields(e);
    });
}


function SystemListSelection(field, fieldName) {

    if (fieldName == 'lblEventLocationData') {

        if (_locationAction == 1) {
            _fields[fieldName].previousListId.pop();
            _fields[fieldName].previousValue.pop();
        }

        //Rollback the Location changes in DB      
        _locationCheckId = field[fieldName].previousListId[field[fieldName].previousListId.length - 1];
        SaveLocation(field[fieldName].previousListId[field[fieldName].previousListId.length - 1]);
        _locationAction++;
    }
    else if (fieldName == 'lblEventClassificationData') {

        if (_action == 1) {
            _fields[fieldName].previousListId.pop();
            _fields[fieldName].previousValue.pop();
        }

        //Rollback the classification changes in DB
        _classificationIdsArray = [];
        //Push the previous classification id to check mark on in the classification treeview.       
        _classificationIdsArray.push(field[fieldName].previousListId[field[fieldName].previousListId.length - 1]);

        SaveClassification(field[fieldName].previousListId[field[fieldName].previousListId.length - 1]);
        _action++;
    }
    else {
        //For custom lists.
        //if (_customListAction == 1) {

            //_fields[fieldName].previousListId.pop();
            //_fields[fieldName].previousValue.pop();
        //var fieldValue = field[fieldName].previousListId[field[fieldName].previousListId.length - 1];

        var indexValue = _fields[fieldName].previousListId.length > 1 ? 2 : 1;
        var fieldValue = field[fieldName].previousListId[field[fieldName].previousListId.length - indexValue];

            if (fieldValue == undefined) {
                fieldValue = '';
            }
        //}

        GenericSaveFields(_fields, fieldName, fieldValue);
        $('#' + _fields[fieldName].hiddenFieldId).val(fieldValue);

        //GenericSaveFields(_fields, fieldName, field[fieldName].previousListId[field[fieldName].previousListId.length - 1]);
        //$('#' + _fields[fieldName].hiddenFieldId).val(field[fieldName].previousListId[field[fieldName].previousListId.length - 1]);

        _customListAction++;
    }
}

