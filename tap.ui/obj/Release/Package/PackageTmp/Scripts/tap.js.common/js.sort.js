﻿$(document).ready(function () {

    //For drag and drop the popup
   // $('#FieldDialog').draggable();

    //For Sorting Custom Subtabs
    //$("#CustomSubTabs").sortable();

    //for bind the sort order of subtabs
    //BindTabsSortOrder();

    //For sorting the Fields
    //$('#FieldInfo').sortable();

    //for bind the sort order of Fields
    //BindFieldsSortOrder();
});   //ends ready

//for bind the subtabs sort order
function BindTabsSortOrder() {
    $("#CustomSubTabs").bind("sortupdate", function (event, ui) {
        arrSubTabSortOrder = new Array;
        $("#CustomSubTabs li").each(function (event, ui) {
            arrSubTabSortOrder.push(ui.id[1]);
        });
        CustomSubTabSortUpdate(_tabOrderArray, arrSubTabSortOrder);
    });
}

//For binding the fields sort order
function BindFieldsSortOrder() {
    $("#FieldInfo").bind("sortupdate", function (event, ui) {
        arrSortOrder = new Array;
        $("#FieldInfo li").each(function (event, ui) {
            arrSortOrder.push(ui.id[1]);
        });
        FieldSortUpdate(_fieldOrderArray, arrSortOrder);
    });
}

//Ajax post call for Updating sort subtabs
function CustomSubTabSortUpdate(tabOrderArray, subTabSortOrderArray) {

    var data = JSON.stringify({ 'tabId': tabOrderArray, 'subTabOrder': subTabSortOrderArray });
    var serviceURL = _baseURL + 'Services/Tabs.svc/SubTabsSortUpdate';
    AjaxPost(serviceURL, data, eval(SubTabSortDataSuccess), eval(SubTabFailSortData));
    _tabOrderArray = [];
}

function SortDataSuccess(object) {
    if (object != null && object.d != null) {
        _message = 'Custom Fields sort order saved successfully.';
        _messageType = 'jSuccess';
        NotificationMessage(_message, _messageType, false, 2000);
    }
}
function FailSortData(object) { alert('Failed sorting.'); }

//Ajax post call for Updating sort of Fields
function FieldSortUpdate(fieldOrderArray, sortOrderArray) {
    var data = JSON.stringify({ 'fieldId': fieldOrderArray, 'sortOrder': sortOrderArray });
    var serviceURL = _baseURL + 'Services/Fields.svc/FieldSortOrderUpdate';
    AjaxPost(serviceURL, data, eval(SortDataSuccess), eval(FailSortData));

}

function SubTabSortDataSuccess(object) {
    if (object != null && object.d != null) {
        NotificationMessage('Tabs order saved successfully.', 'jSuccess', true, 1000);
    }
}
function SubTabFailSortData(object) { alert('Failed sorting tabs.'); }

function SaveGridColumnSortOrder(gridId, gridName, pageName, sortOrder) {
    var userid = GetUserId();
    var gridid=1;

    var gridDetails = new GridDetailData(userid, gridId, gridName, pageName, sortOrder);

    _data = JSON.stringify(gridDetails);
    var serviceURL = _baseURL + 'Services/UserCustomGrid.svc/SaveGridColumnSortOrder';
    AjaxPost(serviceURL, _data, eval(GridColumnSortDataSuccess), eval(FailGridColumnSort));
}

function GridDetailData(userId, gridId, gridName, pageName, sortOrder) {
    this.userID = userId;
    this.gridID = gridId;
    this.gridName = gridName;
    this.pageName = pageName;
    this.sortOrder = sortOrder;
}

function GridColumnSortDataSuccess(object) {
    if (object != null && object.d != null) {
        _message = 'Custom Grid column sort order saved successfully.';
        _messageType = 'jSuccess';
        NotificationMessage(_message, _messageType, false, 2000);
    }
}
function FailGridColumnSort(object) { alert('Failed sorting.'); }

//function GetGridColumnSortOrder(gridId, gridName, pageName) {
//    var userid = GetUserId();
//    var gridid = 1;
//    var gridDetails = new SetGridData(userid, gridId, gridName, pageName);
//    _gridName = gridName;
//    _data = JSON.stringify(gridDetails);
//    var serviceURL = _baseURL + 'Services/UserCustomGrid.svc/GetGridColumnSortOrder';
//    AjaxPost(serviceURL, _data, eval(GetGridColumnOrderSuccess), eval(FailGetGridColumnOrder));
//}

//function SetGridData(userId, gridId, gridName, pageName) {
//    this.userID = userId;
//    this.gridID = gridId;
//    this.gridName = gridName;
//    this.pageName = pageName;
//}

//function GetGridColumnOrderSuccess(object) {
//    if (object != null && object.d != null) {
//        var orderArray = [];
//        var orderSplit = object.d.split(',');
//        for (var i = 0; i < orderSplit.length; i++) {
//            orderArray.push(orderSplit[i]);
//        }
//        jQuery("#" + _gridName).jqGrid("remapColumns", orderArray, true, false);
//    }

//    _gridName = '';
//}
//function FailGetGridColumnOrder(object) { alert('Failed sorting.'); }