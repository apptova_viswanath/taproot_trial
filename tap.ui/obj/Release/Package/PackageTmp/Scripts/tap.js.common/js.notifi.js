﻿
var _minWidth = 1000;
var _timeShown = 7000;
var _showTimeEffect = 200;
var _hideTimeEffect = 400;

function NotificationMessage(alertMsg, msgType, isContentPage, timeShow) {
 
    //remove last comma if it is there
    alertMsg = alertMsg.replace(/,(\s+)?$/, '');

    if (!isContentPage) {
        _minWidth = 790;
        _timeShown = 2000;
        _showTimeEffect = 200;
        _hideTimeEffect = 200;
    }
    
    if (timeShow != null) {
        _timeShown = timeShow;
    }

    if (alertMsg.length < 25) {
        _timeShown = 2000;
    }
    else if (alertMsg.length > 25 && alertMsg.length < 50) {
        _timeShown = 3000;
    }
    else if (alertMsg.length > 50 && alertMsg.length < 100) {
        _timeShown = 4000;
    }
    else if (alertMsg.length > 100 && alertMsg.length < 150) {
        _timeShown = 5000;
    }
    else if (alertMsg.length > 150 && alertMsg.length < 200) {
        _timeShown = 6000;
    }
    else if (alertMsg.length > 200 && alertMsg.length < 250) {
        _timeShown = 7000;
    }
    else {
        _timeShown = 10000;
    }

    eval(msgType)(
               alertMsg + '<a id="jnotify-close" onclick="CloseNotification(this)"  class="jnotify-close"></a>',
            {
                autoHide: true,
                clickOverlay: false,
                MinWidth: _minWidth,//1000,
                TimeShown: _timeShown,//7000,
                HorizontalPosition: 'center',
                VerticalPosition: 'top',
                ShowOverlay: false
            }
        );
    $('#' + msgType).show();
    $('#' + msgType).css('top', '15px');
    $("html, body").animate({ scrollTop: 0 }, "slow");
}


function CloseNotification(sender)
{
    $(sender).parent().fadeOut('200');
    return false;
}


function NotificationMessageWithFixTime(alertMsg, msgType, isContentPage, timeShow) {

    alertMsg = alertMsg.replace(/,(\s+)?$/, '');

    if (!isContentPage) {
        _minWidth = 790;
        _timeShown = 2000;
        _showTimeEffect = 200;
        _hideTimeEffect = 200;
    }

    if (timeShow != null) {
        _timeShown = timeShow;
    }
    eval(msgType)(
               alertMsg + '<a id="jnotify-close" onclick="CloseNotification(this)"  class="jnotify-close">×</a>',
            {
                autoHide: true,
                clickOverlay: false,
                MinWidth: _minWidth,
                TimeShown: _timeShown,
                HorizontalPosition: 'center',
                VerticalPosition: 'top',
                ShowOverlay: false
            }
        );
}


