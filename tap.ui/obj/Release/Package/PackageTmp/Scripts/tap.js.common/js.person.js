﻿var _personType = '';
var _isPersonManualAdd = false;

$(document).ready(function () {

    _isSUCompany = $('#hdnSUCompany').val();
    _isSUDivisionId = $('#hdnSUDivision').val();

    //hide the grid and manually add controls
    HideControls();
    $('#txtNewUserName').html('');
    $('#txtSearchTeamUser').val('');

    //team user search option 
    $("#txtSearchTeamUser").unbind('keyup').keyup(function (event) {
       
        
        var isBackSpacePressed = (event.keyCode == 8);
        TeamUserSearch(isBackSpacePressed);
        event.preventDefault();

    });

    $('#ancAddTeamMemberManually').click(function () {

        if (_isShareTab)
            isExistTMUser();
        else
            AddPersonResponsible(_personType, _isSUCompany, _isSUDivisionId);
        return false;

    });

    //$('#closeTeamMemberpopup').click(function () {
    //    CloseAddTeamUserPopup();
    //});
});


function HideControls() {

    $('#divTeamUserListGrid').hide();
    $('#divPersonAddManually').hide();
    $('#ancAddTeamMemberManually').hide();
    $('#divMsgForNoUsers').hide();
}

//Open Pop up page for Add team Member
function OpenAddTeamUserPopup(type) {
    
    _personType = type;
    var pageTitle = 'Add a Person';
    //madol popup code

    $("#dialog:ui-dialog").dialog("destroy");
    $("#divTeamUserListGridPopUp").dialog({
        title: pageTitle,
        height: 'auto',
        width: 650,
        modal: true,
        draggable: true,
        position: ['middle', 200],
        resizable: false
    });

    _searchTeamUser = '';
    _isPersonAssigned = false;
    HideControls();
    $('#txtNewUserName').html('');
    $('#txtSearchTeamUser').val('');

}


//close Add team Member Popup page.
function CloseAddTeamUserPopup() {

    $("#divTeamUserListGridPopUp").dialog("destroy");
    $("#dialog:ui-dialog").dialog("destroy");
    $('#divTeamUserListGridPopUp').bind("dialogclose", function (event, ui) {

    });
    $('#divTeamUserListGridPopUp').hide();

    _searchTeamUser = '';
}

//on click of search textbox
function TeamUserSearch(isBackSpacePressed) {
   
    _searchTeamUser = $('#txtSearchTeamUser').val();

    if ((_isSUCompany != null && _isSUCompany == "1") || (_isSUDivisionId != null && _isSUDivisionId == "1")) {

        (!_isShareTab) ? CallSharePersonGrid(isBackSpacePressed) : ShowManualAddControls();     //if it SU/ENT then show the grid for both Person and for Share show the grid for ENT only

    }
    else {

        CallSharePersonGrid(isBackSpacePressed);
    }
}

//common method for both Share and Person to call grid functionality
function CallSharePersonGrid(isBackSpacePressed) {
    
    var records = 0;
    var textToSearch = $('#txtSearchTeamUser').val();
    var personFoundOnPreviousKeyupEvent = !($('#divPersonAddManually').is(":visible"));
    var noTextForSearch = (textToSearch.length == 0);
    
    var initiateSearchUsers = noTextForSearch ? false : personFoundOnPreviousKeyupEvent ? true : isBackSpacePressed ? true : false;
    
    if (initiateSearchUsers)
    {

        BindTeamUsersListGrid();
        ReloadTeamUserGridData();

        records = jQuery("#TeamUserListGrid").jqGrid('getGridParam', 'records');
    }


    if (records == 0) {

        if (noTextForSearch)
        {
            $('#divTeamUserListGrid').hide();
            $('#ancAddTeamMemberManually').hide();
            $('#divPersonAddManually').hide();
            $('#txtNewUserName').html(textToSearch);
            //$('#spnPersonName')[0].innerHTML = textToSearch;
            return;
        }
        ShowManualAddControls();
    }
    else {
        $('#divTeamUserListGrid').show();
    }
}

// Team User Reload Grid
function ReloadTeamUserGridData() {
    jQuery("#TeamUserListGrid").trigger("reloadGrid");
}

//show Add button to add manually team memeber/person
function ShowManualAddControls() {
    $('#divPersonAddManually').show();
    $('#ancAddTeamMemberManually').show();
    $('#txtNewUserName').html(_searchTeamUser);

   // $('#spnPersonName')[0].innerHTML = _searchTeamUser;
}

//popup page Team Member List Grid Binding- manually adding persons
function BindTeamUsersListGrid() {
    
    jQuery("#TeamUserListGrid").jqGrid({

        datatype: function (parameterData) {            
            GetTeamUserLists(parameterData);
        },
        colNames: ['UserId', 'UserName', 'Name','Email', 'View', 'Edit', 'Display on Report', '<img src="' + _baseURL + 'Scripts/jquery/images/user-icon.png">'
            //, 'Share'
        ],
        colModel: [

            { name: 'UserID', index: 'UserID', width: 200, align: 'left', resizable: false, editable: false, hidden: true },
            { name: 'UserName', index: 'UserName', width: 200, align: 'left', resizable: false, editable: false, hidden: true },
            { name: 'FullName', index: 'FullName', width: 100, align: 'left', resizable: false, editable: false },
             { name: 'Email', index: 'Email', width: 200, align: 'left', resizable: false, editable: false },
            { name: 'View', index: 'View', width: 50, align: 'center', resizable: false, formatter: userTeamChkFormatter },
            { name: 'Edit', index: 'Edit', width: 50, align: 'center', resizable: false, formatter: userTeamChkFormatter },
            { name: 'Display on Report', index: 'isDisplayOnReport', width: 100, align: 'center', resizable: false, formatter: userTeamChkFormatter },
             { name: 'Icon', index: 'Icon', width: 50, align: 'center', resizable: false, editable: false, hiddden: true, formatter: usericonFmatter },

        ],
        gridview: false,
        rowattr: function (rd) {
        },
        toppager: false,
        emptyrecords: 'There is no People.',
        // Grid total width and height        
        autowidth: true,

        rowNum: 10,
        hidegrid: false,
        pginput: "false",
        height: '100%',
        caption: 'People',
        pager: jQuery('#TeamUserListGridNavigation'),
        viewrecords: true,
        //sortname: 'FullName', //Commented for filtering the grid based on Full name as well as Email also
        sortorder: "asc",
        cmTemplate: { title: false }

    });

    $(window).bind('resize', function () {
        $("#TeamUserListGrid").setGridWidth($('.TeamUserListGrid').width());
    }).trigger('resize');

} //grid end

function usericonFmatter(cellvalue, options, rowObject) {
    if (rowObject[6] == null || rowObject[6] == 'False') {
        return "<img  id='TMUserIcon' style='margin-top:2px; height:10px;margin-left: -12px;' src='" + _baseURL + "Scripts/jquery/images/user-icon.png' title='User'  />";
    }
    else {
        return "";
    }
}


function userTeamChkFormatter(cellValue, options, rowObject) {

    //to Hide/Show the Checkboxes for Share/Person Grid
    if (!_isShareTab) {
        jQuery("#TeamUserListGrid").jqGrid('showCol', ["Icon"]);
        jQuery("#TeamUserListGrid").jqGrid('hideCol', ["View", "Edit"]);

    }
    else {        
        jQuery("#TeamUserListGrid").jqGrid('showCol', ["View", "Edit"]);
        jQuery("#TeamUserListGrid").jqGrid('hideCol', ["Icon"]);        
    }

    var fullnamewihEmail = rowObject[2] + " (" + rowObject[3] + ")";
    if (cellValue == 'False') {
        if (options.pos == 6)
            return "<input type='checkbox' class='chkTeamUserDisplayOnReport' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\"/>";

        if (options.pos == 5)
            return "<input type='checkbox' class='chkTeamUserEdit' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\"/>";
        if (options.pos == 4)
            return "<input type='checkbox' class='chkTeamUserView' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\"/>";
    }

    if (cellValue == 'True') {
        if (options.pos == 6)
            return "<input type='checkbox' class='chkTeamUserDisplayOnReport' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\" checked='checked'/>";

        if (options.pos == 5)
            return "<input type='checkbox' class='chkTeamUserEdit' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\" checked='checked'/>";
        if (options.pos == 4)
            return "<input type='checkbox' class='chkTeamUserView' onclick=\"ClickOnChkTeamUser('" + options.rowId + "' , " + options.pos + ", this, event,'" + fullnamewihEmail + "');\" checked='checked'/>";
    }

}

//checkbox functionality for Popup Grid for both Team Member and Person
function ClickOnChkTeamUser(rowId, ind, sender, event, userName) {
    
    _teamUserId = rowId;
    var val = $(sender).attr('checked');


    var record = jQuery("#TeamUserListGrid").jqGrid('getGridParam', 'records');

    if (_isShareTab) {

        //For View
        if (ind == 4 && val != null && val != 'undefined') { //if (record > 1 && ind == 4 && val != null && val != 'undefined')

            $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").removeAttr("checked"); //Removes edit access
            $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").attr("checked", "checked");
            //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").removeAttr("checked");
            //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").attr("checked", "checked");
        }
        //else if (record == 1 && ind == 4) {
        //    $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
        //    var errorMessage = 'There must be at least one user with Edit access.';
        //    var errorMessageType = 'jError';
        //    NotificationMessage(errorMessage, errorMessageType, true, 3000);
        //    return false;
        //}

        //For Edit
        if (ind == 5 && val != null && val != 'undefined') {
            $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").removeAttr("checked"); //Removes view access
            $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").attr("checked", "checked");
            //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
            //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").attr("checked", "checked");
        }

        //var edit = $("tr.jqgrow > td > input.chkTeamUserEdit", jQuery("#TeamUserListGrid")[rowId]);

        //if (!edit.is(':checked')) {
        //    var errorMessage = 'There must be at least one user with Edit access.';
        //    var errorMessageType = 'jError';
        //    NotificationMessage(errorMessage, errorMessageType, true, 3000);

        //    $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
        //    $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").attr("checked", "checked");

        //    event.preventDefault();
        //    return false;
        //}

        var isEdit = false;
        var isView = false;
        var isDisplayOnReport = false;

        //For Display on report
        if (ind == 6) {
            if (val != null && val != 'undefined') {
                isDisplayOnReport = true;
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(6) > input").attr("checked", "checked");
                //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").attr("checked", "checked");
            }
            else {
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(6) > input").removeAttr("checked");
                //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").removeAttr("checked");
            }
        }
        else {
            var displayOnReport = $('#TeamUserListGrid').getCell(rowId, 6);
            //var displayOnReport = $('#TeamUserListGrid').getCell(rowId, 5);
            isDisplayOnReport = $(displayOnReport).is(':checked');
        }

        if (ind == 5) {
            if (val != null && val != 'undefined') {
                isEdit = true;
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").attr("checked", "checked");
                //$("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").attr("checked", "checked");
            }
            else {
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(4) > input").removeAttr("checked");
            }
        }
        else {
            var edit = $('#TeamUserListGrid').getCell(rowId, 5);
            //var edit = $('#TeamUserListGrid').getCell(rowId, 4);
            isEdit = $(edit).is(':checked');
        }

        if (ind == 4) {
            if (val != null && val != 'undefined') {
                isView = true;
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").attr("checked", "checked");
            }
            else {
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(3) > input").removeAttr("checked");
            }
        }
        else {
            var view = $('#TeamUserListGrid').getCell(rowId, 4);
            //var view = $('#TeamUserListGrid').getCell(rowId, 3);
            isView = $(view).is(':checked');
        }

        if (isView == true || isEdit == true || isDisplayOnReport == true) {
            //MakeTeamMemberOnChk(rowId, ind, sender);
            AddTeamMember(isEdit, isView, isDisplayOnReport);

        } else {
            event.preventDefault();
            var errorMessage = 'Can not remove this setting. Team memeber should have atleast one privillege.';
            var errorMessageType = 'jError';
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
            return false;
        }
    }
    else {
        if (ind == 6) {
            if (val != null && val != 'undefined') {
                isDisplayOnReport = true;
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").attr("checked", "checked");
            }
            else {
                $("#TeamUserListGrid  tr[id='" + rowId + "'] > td:eq(5) > input").removeAttr("checked");
            }
        }
        else {
            var displayOnReport = $('#TeamUserListGrid').getCell(rowId, 5);
            isDisplayOnReport = $(displayOnReport).is(':checked');

        }
       // _personType = 'TaskResponsiblePerson';
        //add person - system users parent page of CAP.
        AddPersonSystemUser(rowId, userName);
    }

}
var _isManualMPersonResponsible = false;
var _isManualEPersonResponsible = false;
var _isManualAPersonResponsible = false;
var _isSystemUserAssigned = false;

function AddPersonSystemUser(userId, displayName) {

    $('#spn' + _personType)[0].innerHTML = displayName;

    ((_personType == 'MPersonResponsible') ? (_isManualMPersonResponsible = false) : (_personType == 'EPersonResponsible') ? (_isManualEPersonResponsible = false) : (_personType == 'APersonResponsible') ? (_isManualAPersonResponsible = false) : true);

    $('#txtNewUserName').html('');
    $('#txtSearchTeamUser').val('');
    CloseAddTeamUserPopup();

}

//to get Team User Lists
function GetTeamUserLists(parameterData) {

    var companyId = _companyId;
    var userListData = new Object();
    userListData.page = parameterData.page;
    userListData.rows = parameterData.rows;
    userListData.SearchFilter = _searchTeamUser;
    userListData.IgnoreCase = 0;
    userListData.sortIndex = parameterData.sidx;
    userListData.sortOrder = parameterData.sord;
    userListData.companyID = companyId;
    userListData.eventId = _eventId;
    userListData.isShareTab = _isShareTab;
    
    data = JSON.stringify(userListData);
    _serviceUrl = _baseURL + 'Services/TeamMembers.svc/GetTeamUsersDetails';
    AjaxPost(_serviceUrl, data, eval(GetTeamUserListsSuccess), eval(GetTeamUserListsFail));
}

// Team User List Success
function GetTeamUserListsSuccess(result) {

    HideControls();

    var errorMessage = '';
    var errorMessageType = 'jError';
    if (result.d != null && result.d != "") {

        var gridData = jQuery("#TeamUserListGrid")[0];
        gridData.addJSONData(JSON.parse(result.d));
        var resultCount = ParseToJSON(result.d);

        if (resultCount.rows.length != 0) {
            $('#divTeamUserListGrid').show();
            $('#ancAddTeamMemberManually').hide();
            $('#divPersonAddManually').hide();
        }
        else {
            
            $('#ancAddTeamMemberManually').show();
            $('#divPersonAddManually').show();
            $('#txtNewUserName').html(_searchTeamUser);
           // $('#spnPersonName')[0].innerHTML = _searchTeamUser;
        }

    }


}

//service fail
function GetTeamUserListsFail() {
    alert('Service failed.');
}

function isExistTMUser() {

    var fullName = $('#txtSearchTeamUser').val().trim();
    var details = new SetDetailsisExistTMUser(fullName);
    var _data = JSON.stringify(details);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/isUserExistTM';
    AjaxPost(serviceURL, _data, eval(isExistTMUserSuccess), eval(isExistTMUserFails));
}

function isExistTMUserSuccess(result) {

    var errorMessage = 'This person is already added in Team Members.';
    var errorMessageType = 'jError';
    if (result != null) {
        if (result.d == true) {
            NotificationMessage(errorMessage, errorMessageType, true, 3000);
        }
        else {

            AddTeamMemberManually();
        }
    }
}

function isExistTMUserFails() {

    alert('Service Failed.');
}

function SetDetailsisExistTMUser(fullName) {

    this.name = fullName;
    this.eventId = _eventId;
    this.companyId = _companyId;
}

var _isPersonAssigned = false;

// on popup close populate Person name in span control in parent page- system user
function AddPersonResponsible(personType, _isSUCompany, _isSUDivisionId) {
    
    //_isManualMPersonResponsible = true;
    //_isManualEPersonResponsible = true;
    //_isManualAPersonResponsible = true;

    var personName = $('#txtSearchTeamUser').val().trim();

    ((personType == 'MPersonResponsible') ? (_isManualMPersonResponsible = true) : (personType == 'EPersonResponsible') ? (_isManualEPersonResponsible = true) : (personType == 'APersonResponsible') ? (_isManualAPersonResponsible = true) : false);

    $('#spn' + personType)[0].innerHTML = personName;
    if (!_isPersonAssigned) {
        CloseAddTeamUserPopup();
        _isPersonAssigned = true;
    }

}

//check if manually added person exist or not-not using
function isExistManualPerson() {
    var displayName = $('#txtSearchTeamUser').val().trim();
    var existPersonDetails = new GetPersonDetails(displayName);
    var data = JSON.stringify(existPersonDetails);
    var serviceURL = _baseURL + 'Services/TeamMembers.svc/isExistManualPerson';
    AjaxPost(serviceURL, data, eval(isExistManualPersonSuccess), eval(isExistManualPersonFails));
}

function GetPersonDetails(displayName) {
    this.displayName = displayName;
    this.companyId = _companyId;
}

function isExistManualPersonSuccess(result) {
    alert('Success');
}

function isExistManualPersonFails() {
    alert('Service Failed.');
}

