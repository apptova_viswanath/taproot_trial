﻿//Global Variables
var _moduleName = '';
var _moduleId = '';
var _enterKeyCode = 13;
var _svcURL = '';
var _virtualDirectoryPath = '';
var _servicePath = '';
var _userId = '';
var _userName = '';
var _companyId = '';
var _companyName = '';
var _newEventId = '';
var _urlForAdminMainTab = '';
var _confirmChecked = false;

var _springSeason = '';
var _summerSeason = '';
var _AutumnSeason = '';
var _winterSeason = '';
var _AutumnCompleteSeason = '';
var _isTapRootTab = false;

var _isDirty = false;
var _userResponseCount = 0;
var _divTeamMemberId = '';
var _passwordRegularExpression = '';
var _passwordPolicyDetails = '';
var _dirtyMsgText = "Are you sure you want to leave this page and lose your unsaved changes?";

var SPRING_SEASON = "1";
var SUMMER_SEASON = "2";
var AUTUMN_SEASON = "3";
var WINTER_SEASON = "4";

var _englishLanguage = 'en';
var _spanishLanguage = 'es';
var _russianLanguage = 'ru';
var _frenchLanguage = 'fr';
var _germanLanguage = 'de';
var _portugueseLanguage = 'pt';


$(document).ready(function () {

    //TODO : Move this to other place
    //Change the name "Sharing" to "Team Members" in SU app.
    SUSharingTabNameChange();

    DirtyMessage();

    GetQueryStringFromUrl();

    SetGlobalVariables();

    //Create ids for seasons
    SeasonsIdsByNameCreate();

    DateFormatSettingGet();

    //OPT- commented no need to check in all the pages.
    //PasswordExpression(); 

    PasswordOutOfPolicy();

    PasswordExpired();

    $("#userPasswordPolicy").click(function () {
        OpenUserPswPolicyPopUp('divUserPasswordPolicy');
        return false;
    });

    $("#btnUserPswPolicyCancel").click(function () {
        CloseUserPswPolicyPopUp('divUserPasswordPolicy');
        return false;
    });
});

//Change the name "Sharing" to "Team Members" in SU app . 
function SUSharingTabNameChange() {
      
    if ($('#hdnSUCompany').val() == 1)
        $('.SUTeamMembers').text('Team Members');
}

function DirtyMessage() {
    
    var searchInputs = 'txtSearchTeamUser,txtSearchDivision,txtReportSearch,txtSearchUser,txtSearch,txtNewSystemListName,txtTitle,txtNotifyMessage,txtNewSystemListDescription';
    // JqueryTabs();// ISSUERELATEDTOIE
   
    $("input[type='text'],input[type='radio'],select,textarea").bind('propertychange change', function () {
       
        var splitInp = searchInputs.split(',');
        for (var i = 0; i < splitInp.length; i++) {
            if ($(this).attr('id') == splitInp[i])
                return;
        }
        //if ($(this).hasClass('userPreview'))
        if ($(this).hasClass('userPreview') || $(this).hasClass('trackCustomChanges')) //skip the custom fields from tracking .
            return;

        
        if ($(this).parent().parent("div").attr("id") == 'divAddEditFieldsDialog' || $(this).parent().parent().parent("div").attr("id") == 'divAddEditFieldsDialog'
            || ($(this).parents().eq(1).attr("id") == 'divTask'))
        {
            return false;
        }

        var value = TrimString($(this).val());
        if (value != null && value.length > 0)
            _isDirty = true;
        else
            _isDirty = false;
    });


    //This is for the implementation of displaying the confirm box if any recommended fields are not filled in
    //while moving to any other link, it will check if the field marked with * is filled up
    //If not display a confirm box and after user choose yes it will redirect to the clicked location

    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    
    $("#form a").off('click').on('click', function (e) {
        
        if ($(this).parent('h2').hasClass("seasonContentHide"))
            return;

        if ($(this)[0] != undefined) {
            if ($(this)[0].id == 'ancAddTeamMemberManually')
                _isDirty = false;
        }
        if (splitUrl.length > 2 && splitUrl[splitUrl.length - 1].indexOf('Home') > -1)
            _isDirty = false;
        
        if (splitUrl.length > 4 && splitUrl[splitUrl.length - 3].indexOf('Configuration') > -1 && splitUrl[splitUrl.length - 1].indexOf('Details') > -1) {
            _isDirty = false;
            return;
        }

        if (splitUrl.length > 5 && splitUrl[splitUrl.length - 5].indexOf('Event') > -1 && splitUrl[splitUrl.length - 3].indexOf('Details') > -1 && splitUrl[splitUrl.length - 2] != '0')
            _isDirty = false;

        if (splitUrl.length > 1 && splitUrl[splitUrl.length - 1].indexOf('Messages') > -1)
            _isDirty = false;
      
        if ($(this).attr('id') != 'ancUndo' && $(this).attr('id') != 'ancUndoAll' && $(this).attr('id') != 'lnkCreateNewNotification' && $(this).attr('id') != 'PswPolicyHelpIcon' && $(this).attr('id') != 'userPasswordPolicy' && $(this).attr('id') != 'ancChangeLanguage') {
            if (!$("#ancUndo").is(":visible")) { // check for not firing during auto save
               
                if (_isDirty) {

                    //_userResponseCount is handled to restrict two times firing event for the time being
                    // _userResponseCount = _userResponseCount + 1;
                   
                    if (_userResponseCount <= 1) {
                        var userResponse = confirm(_dirtyMsgText);
                        isRecommendedFieldsFilledUp = false;
                        return userResponse;
                    }
                    else {
                        _userResponseCount = 0;
                    }

                }
            }
            else {
                _isDirty = false;
            }
            //If already confirmation checked on click of any event main tabs, then will not execute the confirm again
            if (!_confirmChecked) {
                return ConfirmFromUserAndRedirect();
            }

        }

    });
}

//Redirect the url for Main Tabs
function RedirectUrl(result) {

    _urlForAdminMainTab = result;

    //IE fixes, sometime the click event not fired for 'form > a generic click event', hence merged here
    _confirmChecked = true;
    return ConfirmFromUserAndRedirect();

}

// Confirm from user wheter user wants to fill the recommended fields or not and 
// based on the user response redirect to the next page or stay at the same current page
function ConfirmFromUserAndRedirect() {
    var href = $(this).attr("href");
  
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
   

    var userResult = true;
    if (href != '#') {
        //Check if all the required field present inside the usercontrol used for the public custom tab page are filled in
        var isRecommendedFieldsFilledUp = CheckIfRecommendedFieldsFilledUp();

        if (splitUrl.length > 5 && splitUrl[splitUrl.length - 5].indexOf('Event') > -1 && splitUrl[splitUrl.length - 3].indexOf('Details') > -1)
            isRecommendedFieldsFilledUp = true;

        //If not filled in
        if (!isRecommendedFieldsFilledUp) {
            userResult = confirm("Some recommended fields are not filled out. \r\nAre you sure you want to leave this page?");
        }

        //for some of the link the href property is not set and on click of those link the url is set in _urlForAdminMainTab
        //for those links this check is provided
        if (userResult && (_urlForAdminMainTab != '')) {
            window.location.href = _urlForAdminMainTab;
        }

    }

    return userResult;

}

//Check if all the required field present inside the usercontrol used for the public custom tab page are filled in
function CheckIfRecommendedFieldsFilledUp() {

    var isRecommendedFieldsFilledUp = true;                
    var controlId = '';
    var fieldId = '';

    //Get all the child controls from the parent div
    var childControls = $('#ulCustomField').children();

    if (childControls != null && childControls != undefined) {

        //Iterate all child items
        $('#ulCustomField').each(function () {

            $(this).find('span').each(function () {

                controlId = $(this).attr('id');

                if (controlId != undefined && controlId != '') {

                    fieldId = controlId.substring(3, controlId.length);
                    //Check if the related label text contains *
                    if ($(this).text() == '*') {

                        if ($('#txt' + fieldId).val() == '' || $('#txtarea' + fieldId).html() == '') //Check If field value is null, for the text and text area
                        {
                            isRecommendedFieldsFilledUp = false;
                            return isRecommendedFieldsFilledUp;
                        }
                        else if ($('#rbY' + fieldId).length > 0) { //Check for the radio button

                            if ($('#rbY' + fieldId).attr('checked') != 'checked' && $('#rbN' + fieldId).attr('checked') != 'checked') {

                                isRecommendedFieldsFilledUp = false;
                                return isRecommendedFieldsFilledUp;
                            }
                        }
                        else if ($('#hdn' + fieldId).val() == '') { //Check for the location

                            isRecommendedFieldsFilledUp = false;
                            return isRecommendedFieldsFilledUp;
                        }
                        else if ($('#ddl' + fieldId).val() == '' || $('#ddl' + fieldId).val() == '0') { //Check for the location

                            isRecommendedFieldsFilledUp = false;
                            return isRecommendedFieldsFilledUp;
                        }
                    }
                }
            });
        });
    }

    return isRecommendedFieldsFilledUp;
}

//Set Global variables
function SetGlobalVariables() {
    _userId = $('#hdnUserId').val();
    _virtualDirectoryPath =  $('#hdnVirtualDirectoryPath').val();
    _svcURL = $('#hdnServicePath').val();//change naming convention to _virtualServicePath
    _companyId = $('#hdnCompanyId').val();
    _userName = $('#hdnUserName').val();
    _companyName = $('hdnCompanyName').val();
}

//Get CompanyId
function GetCompanyId() {
    _companyId = $('#hdnCompanyId').val();
    var divisionId = $('#hdnDivisionId').val();
    if (divisionId > 0)
        _companyId = divisionId;

    return _companyId;
}


//function ReadCookie(cookieName) {
//    var theCookie = " " + document.cookie;
//    var ind = theCookie.indexOf(" " + cookieName + "=");
//    if (ind == -1) ind = theCookie.indexOf(";" + cookieName + "=");
//    if (ind == -1 || cookieName == "") return "";
//    var ind1 = theCookie.indexOf(";", ind + 1);
//    if (ind1 == -1) ind1 = theCookie.length;
//    return unescape(theCookie.substring(ind + cookieName.length + 2, ind1));
//}

//Get UserId
function GetUserId() {
    _userId = $('#hdnUserId').val();
    return _userId;
}

function GetServicePath() {
    _svcURL = $('#hdnServicePath').val();
    return _svcURL;
}

function GetUserName() {
    //return $('#hdnUserName').val();
    return $('#hdnFirstName').val();
}

//get Virtual Directory
function GetVirtualDirectory() {
    _virtualDirectoryPath = $('#hdnVirtualDirectoryPath').val();
    return _virtualDirectoryPath;
}

//Ajax call for Get Method
function Ajax(url, arguments, onSuccess, onFail, optionalParameter) {

    $.ajax({ type: "GET",
        cache: false,
        url: url,
        data: arguments,
        success: function (Msg) {
            onSuccess(Msg, optionalParameter);
        },
        fail: onFail
    });
}

//Ajax call for Post Method
function AjaxPost(url, arguments, onSuccess, onFail) {

    $.ajax({
        type: "POST", 
        cache: false,
        url: url,
        data: arguments,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (Msg) {
            onSuccess(Msg);
        },
        fail: onFail
    });
}

function AjaxPost(url, arguments, onSuccess, onFail, isAsync) {
    $.ajax({
        type: "POST",
        cache: false,
        async: isAsync,
        url: url,
        data: arguments,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (Msg) {
            onSuccess(Msg);
        },
        fail: onFail
    });
}

//Ajax call for Get Method with async false
function GetAjax(url, arguments, onSuccess, onFail, optionalParameter) {

    $.ajax({
        type: "GET",
        cache: false,
        url: url,
        async:false,
        data: arguments,
        success: function (Msg) {
            onSuccess(Msg, optionalParameter);
        },
        fail: onFail
    });
}

//Ajax call for Post Method with async false
function PostAjax(url, arguments, onSuccess, onFail) {
    try {
        $.ajax({
            type: "POST",
            cache: false,
            url: url,
            async: false,
            data: arguments,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (Msg) {
                onSuccess(Msg);
            },
            fail: onFail
        });
    } catch (e) {
    }
}


//Convert to Json Object
function ParseToJSON(object) {

    var jsonData = null;
    if (object != null) {
        jsonData = JSON.parse(object);
    }
    return jsonData;
}

//Default button enter
function DefaultButtonClick(e, buttonName) {

    if (e.which == _enterKeyCode) {
        $("#" + buttonName).click();
    }
}

//Confirmation message function
function Confirmation(message) {

    return confirm(message) ? true : false;
}

//Get Module Name and Module Id //USED IN js.inv.prc.js WHICH IS NOT IN USE NOW
function GetQueryStringFromUrl() {

    var url = window.location.href;
    _moduleName = url.split('/')[5];
    _newEventId = url.split('/')[7];
    //get the module id
    _moduleId = (_moduleName == "Incident" ? 1 : _moduleName == "Investigation" ? 2 : _moduleName == "Audit" ? 3 : 4);
}


//Get the module type based on the URL
function EventTypeFromUrlSelect(eventTypePosition, eventIdPosition) 
{
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var evType = splitUrl[splitUrl.length - eventTypePosition];
    _eventId = splitUrl[splitUrl.length - eventIdPosition];

    if (_eventId.lastIndexOf('#') != '-1') {
        _eventId = _eventId.replace('#', '');
    }
    _eventId = ((_eventId == 0) ? -1 : _eventId);

    _eventType = ((evType == 'Incident' || evType == 'New-Incident') ? 1
                 : (evType == 'Investigation' || evType == 'New-Investigation') ? 2
                 : (evType == 'Audit' || evType == 'New-Audit') ? 3 
                 : (evType == 'CAP' || evType == 'CorrectiveActionPlan' || evType == 'Corrective-Action-Plan') ? 4 :'');

    return _eventType;
}

//Get the Event id based on the URL
function EventIdFromUrlSelect(eventIdPosition) {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    return splitUrl[splitUrl.length - eventIdPosition];
}


//Jquery Main Tabs
function JqueryTabs() {
    //Jquery Tabs
    $(function () {

        //generate tabs
        $('#tabs').tabs(        
        );

        $('#body_ucevent_divMainTabs').addClass('ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all');
    });
}

//Give ids to the seasons
function SeasonsIdsByNameCreate() {
    _springSeason = 1;
    _summerSeason = 2;
    _AutumnSeason = 3;
    _winterSeason = 4;
    _AutumnCompleteSeason = 4;
}

//to show/hide validation messages in forms
function ShowHideWarningMessage(fieldId, imageTitle, isValid) {
        
    if (isValid) {
         
        $('#' + fieldId).addClass('bordered');
        var nextImage = $('#' + fieldId).next('img');
        nextImage.remove();
        var imageSrc = _baseURL + 'Images/warning.png';
        $('#' + fieldId).after('<img src="' + imageSrc + '" title="' + imageTitle + '" class="warningImage" />');
        if (fieldId == 'lnkEventLocation' || fieldId == 'lnkEventClassification' || fieldId =='lblCAP' ) {
            $('#' + fieldId).removeClass('bordered');
        }
    }
    else {
        var nextImage = $('#' + fieldId).next('img');
        $('#' + fieldId).removeClass('bordered');
        nextImage.remove();
    }
}

//to show/hide validation messages in forms
function ShowHideWarningMessageForRadio(radioField, imageTitle, isValid) {

    if (isValid) {
               
        var nextImage = $(radioField).next('img');
        nextImage.remove();
        var imageSrc = _baseURL + 'Images/warning.png';
        $(radioField).after('<img style="margin-top:2px;" src="' + imageSrc + '" title="' + imageTitle + '" class="warningImage" />');
       
    }
    else {
        var nextImage = $(radioField).next('img');
        nextImage.remove();
    }
}

// restrict special chars in input fields
function CharsValidation(val) {

    var regex = new RegExp("^[a-zA-Z ]+$");
    if (val != null && val.length > 0) {
        if (!regex.test(val)) {
            return false;
        } else {
            return true;
        }
    }
}

// restrict special chars in input fields
function UserNameCharsValidation(val) {
    var regex = new RegExp("^[a-zA-Z0-9.]+$");
    if (val != null && val.length > 0) {
        if (!regex.test(val)) {
            return false;
        } else {
            return true;
        }
    }
}

// function for next sub-tab in event centerd arrow.
function NextSubTabClick(val, ind) {

    var url = jQuery(val).attr('href');
    var splitUrl = url.split('/');
    var subTab = splitUrl[splitUrl.length - ind];
    subTab = subTab.substring(subTab.lastIndexOf('-') + 1);
    var linkId = $('#anc_' + subTab).next().attr('id');
    if (linkId && linkId.indexOf("anc_") >= 0) {
        var splitLinkId = linkId.split('_');
        subTab = splitLinkId[1];
        var URL = $('#anc' + subTab).attr('href');
        RedirectUrl(URL);
        //window.location = URL;
    }
}

// function for previous sub-tab in event centerd arrow.
function PreviousSubTabClick(val, ind) {

    var url = jQuery(val).attr('href');
    var splitUrl = url.split('/');
    var subTab = splitUrl[splitUrl.length - ind];
    subTab = subTab.substring(subTab.lastIndexOf('-') + 1);
    var linkId = $('#anc_' + subTab).prev().attr('id');
    if (linkId && linkId.indexOf("anc_") >= 0) {
        var splitLinkId = linkId.split('_');
        subTab = splitLinkId[1];
        var URL = $('#anc' + subTab).attr('href');
        RedirectUrl(URL);
        //window.location = URL;
    }
}

//To assign css based on Tab selected and deselected of Sub Tabs
function TabColor(val, ind) {

    var url = jQuery(val).attr('href');
    var splitUrl = url.split('/');
    var subTab = splitUrl[splitUrl.length - ind];
    subTab = subTab.substring(subTab.lastIndexOf('-') + 1);

    //For Subtab color change
    $('#anc' + subTab).removeClass('sub-tab');
    $('#anc' + subTab).addClass('selected-tab');
    $('#anc' + subTab).after("<div  class='arrow-down'></div>");
    $('#anc' + subTab).css('color', '#272727');
    $('#anc' + subTab).css('border', '1px solid #272727');
    
    $('#PrevSubTabPage').css('display','block');
    $('#NextSubTabPage').css('display', 'block');
    
    subTabNextPrev(subTab);
}

//sub-tab next prev arrow enable/disable
function subTabNextPrev(tabId) {

    //previous sub-tab arrow display/hide
    var prevLinkId = $('#anc_' + tabId).prev().attr('id');
    var prevTabTitle = $('#' + prevLinkId).children('a').html();

    if (prevLinkId && prevLinkId.indexOf("anc_") >= 0) {
        $('#PrevSubTabPage').prop("disabled", false);
        $('#PrevSubTabPage').attr('title', 'Back to ' + prevTabTitle);
    } else {
        $('#PrevSubTabPage').attr("disabled", "disabled");
        $('#PrevSubTabPage').removeClass('arrow-left');
        $('#PrevSubTabPage').addClass('arrow-left-tab-disable');
        $('#PrevSubTabPage').css("cursor", "default");
        $("#PrevSubTabPage").unbind('click');
        $('#PrevSubTabPage').attr('title', '');
    }

    //next sub-tab arrow display/hide
    var nextLinkId = $('#anc_' + tabId).next().attr('id');
    var nextTabTitle = $('#' + nextLinkId).children('a').html();

    if (nextLinkId && nextLinkId.indexOf("anc_") >= 0) {
        $('#NextSubTabPage').prop("disabled", false);
        $('#NextSubTabPage').attr('title', 'Continue to ' + nextTabTitle);
    } else {
        $('#NextSubTabPage').attr("disabled", "disabled");
        $('#NextSubTabPage').removeClass('arrow-right');
        $('#NextSubTabPage').addClass('arrow-right-tab-disable');
        $('#NextSubTabPage').css("cursor", "default");
        $("#NextSubTabPage").unbind('click');
        $('#NextSubTabPage').attr('title', '');
    }
}

//check email validation
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function SetEventId() {
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var evType = splitUrl[splitUrl.length - 4];

    _eventId = splitUrl[splitUrl.length - 2];
}

//to get Team Member Access
function GetTeamMemberAccess(divId) {
    SetEventId();
    var userListData = new Object();
    userListData.userId = _userId;
    userListData.eventId = _eventId;
    userListData.companyId = _companyId;
    _divTeamMemberId = divId;
    if (_eventId != "0") {
        data = JSON.stringify(userListData);
        _serviceUrl = _baseURL + 'Services/TeamMembers.svc/GetTeamMemberAccess';
        AjaxPost(_serviceUrl, data, eval(GetTeamMemberAccessSuccess), eval(GetTeamMemberAccessFail));
    }
}

//to get Team Member Access
function GetTMAccessWithEventId(eventId, divId) {
    var userListData = new Object();
    userListData.userId = _userId;
    userListData.eventId = eventId;
    userListData.companyId = _companyId;
    _divTeamMemberId = divId;
    if (_eventId != "0") {
        data = JSON.stringify(userListData);
        _serviceUrl = _baseURL + 'Services/TeamMembers.svc/GetTeamMemberAccess';
        AjaxPost(_serviceUrl, data, eval(GetTeamMemberAccessSuccess), eval(GetTeamMemberAccessFail));
    }
}

// Team Member Access Success
function GetTeamMemberAccessSuccess(result) {
    var isView;
    var isEdit;
    var isDisplayOnReport;
   
    if (result.d != null) {
        isView = result.d[0];
        isEdit = result.d[1];
        isDisplayOnReport = result.d[2];
        if (isView == 'True' && isEdit == 'False')
            ReadOnlyFieldsEventDetails();
        _divTeamMemberId = '';
    }
    return result.d;
}

//service fail
function GetTeamMemberAccessFail() {
    alert('Service failed.');
}

function ReadOnlyFieldsEventDetails() {
    $('#' + _divTeamMemberId).attr('disabled', true);
    $('#' + _divTeamMemberId).unbind();

    $('#' + _divTeamMemberId).children().attr('readonly', true);
    $('#' + _divTeamMemberId).children().attr('disabled', true);
    $('#' + _divTeamMemberId).children().attr('onclick', false);
    $('#' + _divTeamMemberId + ' a').unbind('click');
    $('#' + _divTeamMemberId + ' a').removeAttr('onclick');
    $('#' + _divTeamMemberId + ' img').unbind('click');
    $('#' + _divTeamMemberId + ' img').removeAttr('onclick');
    $('#' + _divTeamMemberId + ' div').unbind('click');
    $('#' + _divTeamMemberId + ' div').removeAttr('onclick');
    $('#' + _divTeamMemberId + ' div').children().removeAttr('onclick');

    if (_divTeamMemberId != 'divTabReport') {
        $('#' + _divTeamMemberId + ' a').removeAttr('href');
    }
    else {
        $('#lnkWinter').removeAttr('href');
    }

    if (_divTeamMemberId == 'divTabCorrectiveAction') {
        $('#btnEdit').removeAttr('onclick');
    }

    $('#' + _divTeamMemberId + ' input').attr("disabled", true);
    $('#' + _divTeamMemberId + ' textarea').attr("disabled", true);
    $('#' + _divTeamMemberId + ' input').removeAttr('onclick');
    var nodes = document.getElementById(_divTeamMemberId).getElementsByTagName('*');
    for (var i = 0; i < nodes.length; i++) {
        nodes[i].setAttribute('disabled', true);
    }

}

// Function to find the user id from url
function GetBaseUrl() {
    var url = jQuery(location).attr('href');
    var baseUrl = url;
    return baseUrl;
}

function onClickCancel() { return false; };

//Get Event by passing event type (integer).
function EventByEventtypeGet(eventType) {
    return _eventType == 1 ? 'Incident' :
            _eventType == 2 ? 'Investigation' :
            _eventType == 3 ? 'Audit' : 'ActionPlan';
}

//Method to Set the DateFormat based on SiteSetting
function DateFormatSettingGet() {
    var dateFormat = localStorage.dateFormat;
    if (dateFormat != '') {

        $.datepicker.setDefaults({
            dateFormat: dateFormat,
            yearRange: '1950:2050',
            maxDate: '+30Y',
            changeMonth: true,
            changeYear: true
        });

    }
    else {
        $.datepicker.setDefaults({
            dateFormat: 'dd-m-yy',
            yearRange: '1950:2050',
            maxDate: '+30Y',
            changeMonth: true,
            changeYear: true
        });
    }

    //_svcURL = GetServicePath();
    //_serviceUrl = _baseURL + 'Services/SiteSettings.svc/GetDateFormat';
    //Ajax(_serviceUrl, null, eval(DateFormatSuccess), eval(DateFormatFail), 'GetDateFormat');
}

//success method for SiteSettingDateFormat
function DateFormatSuccess(result) {

    var dateFormat = '';
    if (result != null && result.d != null && result.d !='') {
        dateFormat = result.d;
        //debugger;
        switch (result.d) {
            case 'MMM dd yyy':
                dateFormat = 'M dd yy'
                break;
            case 'dd/MM/yy':
                dateFormat = 'dd/mm/yy'
                break;
            case 'MM/dd/yy':
                dateFormat = 'mm/dd/yy'
                break;
            case 'dd-MM-yy':
                dateFormat = 'dd-mm-yy'
                break;
            case 'dd-M-yy':
                dateFormat = 'dd-m-yy'
                break;
        }
    }
    //dateFormat = dateFormat.replace('yyyy', 'yy').replace('yyy', 'yy').toLowerCase();
    dateFormat = 'd M yy'
    if (dateFormat != '') {
        
            $.datepicker.setDefaults({
                dateFormat: dateFormat,
                yearRange: '1950:2050',
                maxDate: '+30Y',
                changeMonth: true,
                changeYear: true
            });
        
    }
    else {
        $.datepicker.setDefaults({
            dateFormat: 'dd-m-yy',
            yearRange: '1950:2050',
            maxDate: '+30Y',
            changeMonth: true,
            changeYear: true
        });
    }
}

//service failed method
function DateFormatFail() {
    alert("Error in loading data.");
}


function UpdateStepText() {
    
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');
    var module = splitUrl[splitUrl.length - 3];

    var planStepText = (module == 'Investigation') ? '1 - Plan your investigation using Spring <span class="notranslate"><a class="snp">SnapCharT®</a></span>.' : '1 - Plan your audit using Spring <span class="notranslate"><a class="snp">SnapCharT®</a></span>.';

    var planOptionalText = (module == 'Investigation') ?
   '<li>Reference the <span class="notranslate">Root Cause Tree®</span> to guide data collection and determine what questions to ask.</li><li>Use <span class="notranslate">Equifactor®</span> to develop your equipment troubleshooting plan.</li>'
   : '<li>Reference the <span class="notranslate">Root Cause Tree®</span> or <span class="notranslate">Equifactor®</span> to guide data collection and determine what questions to ask.</li>';

    var investigateStepText = (module == 'Investigation') ? '2 - Determine the sequence of events using Summer <a class="snp">SnapCharT®</a>.' : '2 - Determine the process flow using Summer <span class="notranslate"><a class="snp">SnapCharT®</a></span>.';

    var analyzeStep3Text = (module == 'Investigation') ? '3 - Define Causal Factors using  Autumn <span class="notranslate"><a class="snp">SnapCharT®</a></span>.' : '3 - Define Significant Issues using  Autumn <span class="notranslate"><a class="snp">SnapCharT®</a></span>.';

    var analyzeStep4Text = (module == 'Investigation') ? '4 -  Analyze each Causal Factor for root causes using the <span class="notranslate">Root Cause Tree®</span>.' : '4 - Analyze each Significant Issue for root causes using the <span class="notranslate">Root Cause Tree®</span>.';

    var analyzeOptional3Text = (module == 'Investigation') ?
    'Use Safeguards <span class="notranslate">Analysis®</span> to help define Causal Factors on the <span class="notranslate"><a class="snp">SnapCharT®</a></span>.' : 'Use Safeguards <span class="notranslate">Analysis®</span> to help define Significant Issues on the <span class="notranslate"><a class="snp">SnapCharT®</a></span>.';


    $("#lblPlanStep").html(planStepText);
    $("#lblPlanOptional").html(planOptionalText);

    $("#lblInvestigateStep").html(investigateStepText);

    $("#lblAnalyzeStep3").html(analyzeStep3Text) ;
    $("#lblAnalyzeStep4").html(analyzeStep4Text);
    
    $("#lblAnalyzeOptional3").html(analyzeOptional3Text);

}


//validation for phone number
function validatePhone(idPhone) {
    var a = $('#' + idPhone).val();
    //var filter = /^[0-9-+()]+$/;
    var filter = /^((\+)?[1-9]{1,2})?([-\s\.])?((\(\d{1,4}\))|\d{1,4})(([-\s\.])?[0-9]{1,12}){1,2}(\s*(ext|x)\s*\.?:?\s*([0-9]+))?$/;
    if (filter.test(a)) {
        ShowHideWarningMessage(idPhone, 'Please specify a valid phone number', false);
        return true;
    }
    else {
        ShowHideWarningMessage(idPhone, 'Please specify a valid phone number', true);
        return false;
    }
}

function WebsiteValidation(idInput) {
    var txt = TrimString($('#' + idInput).val());
    var re = /^(www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/
    if (re.test(txt)) {
        return true;
    }
    return false;
}

//To get diff between two dates
function getDateDiff(date1, date2, interval) {
    var second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24,
    week = day * 7;
    date1 = new Date(date1).getTime();
    date2 = (date2 == 'now') ? new Date().getTime() : new Date(date2).getTime();
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years":
            return date2.getFullYear() - date1.getFullYear();
        case "months":
            return ((date2.getFullYear() * 12 + date2.getMonth()) - (date1.getFullYear() * 12 + date1.getMonth()));
        case "weeks":
            return Math.floor(timediff / week);
        case "days":
            return Math.floor(timediff / day);
        case "hours":
            return Math.floor(timediff / hour);
        case "minutes":
            return Math.floor(timediff / minute);
        case "seconds":
            return Math.floor(timediff / second);
        default:
            return undefined;
    }
}

//Subscription validation global function
function SubscriptionValidation(startId, endId) {
    
    var startDate =$('#' + startId).datepicker({ dateFormat: 'mm/dd/yy' }).val();// TrimString($('#' + startId).val());
    var endDate = $('#' + endId).datepicker({ dateFormat: 'mm/dd/yy' }).val();//TrimString($('#' + endId).val());
    var errorMessage = '';
    if (startDate != null && startDate.length > 0 && endDate != null && endDate.length > 0) {
        if (getDateDiff(fromDatefrmatToDate(startDate), fromDatefrmatToDate(endDate), 'days') > 0) {
            ShowHideWarningMessage(endId, 'Please specify a valid subscription end date', false);
        }
        else {
            errorMessage = 'Subscription End Date can not be less or equal to start date: Please re-enter';
            ShowHideWarningMessage(endId, 'Subscription end date can not be less or equal to start date', true);
        }
    }
    return errorMessage;
}

//Allow only numbers
function AllowOnlyNumer(inputId) {
    $('#' + inputId).keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
            || event.keyCode == 27 || event.keyCode == 13
            || (event.keyCode == 65 && event.ctrlKey === true)
            || (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
}

//Trim Function
function TrimString(val) {
    
    if (val != null && val != undefined && val.length > 0)
        return val.replace(/^\s+|\s+$/g, '');
    else
        return val;
}

function PasswordExpression() {
    
    var siteSettingData = new Object();
    if (GetCompanyId() != null && GetCompanyId() != undefined) {

        siteSettingData.companyId = GetCompanyId();
        var data = JSON.stringify(siteSettingData);
        
        var serviceUrl = _baseURL + 'Services/Password.svc/GetPasswordPolicyData';
        AjaxPost(serviceUrl, null, eval(PasswordExpressionSuccess), eval(PasswordExpressionFail));
    }
}

function PasswordExpressionSuccess(result) {
    
    if (result != null && result.d != null && TrimString(result.d).length > 0) {

        var resultData = ParseToJSON(result.d);

        if (resultData != null && resultData[0] != undefined) {

            _passwordRegularExpression = '^';
            var minChars = resultData[0].MinimumCharacters;
            var maxChars = resultData[0].MaximumCharacters;
            var countUPPERCASE = resultData[0].UpperCaseLetter;
            var countLOWERCASE = resultData[0].LowerCaseLetter;
            var countNumber = resultData[0].Number;
            var countSpecial = resultData[0].SpecialCharacter;

            if (resultData[0].Number != null)
                _passwordRegularExpression += '(?=(.*[0-9]){' + resultData[0].Number + ',})';
            if (resultData[0].UpperCaseLetter != null)
                _passwordRegularExpression += '(?=(.*[A-Z]){' + resultData[0].UpperCaseLetter + ',})';
            if (resultData[0].LowerCaseLetter != null)
                _passwordRegularExpression += '(?=(.*[a-z]){' + resultData[0].LowerCaseLetter + ',})';
            if (resultData[0].SpecialCharacter != null)
                _passwordRegularExpression += '(?=(.*[#@!^%$*=+_]){' + resultData[0].SpecialCharacter + ',})';
            if (resultData[0].MinimumCharacters != null)
                _passwordRegularExpression += '.{' + resultData[0].MinimumCharacters;
            if (resultData[0].MaximumCharacters != null)
                _passwordRegularExpression += ',' + resultData[0].MaximumCharacters + '}';
            else
                _passwordRegularExpression += ',}';

            _passwordRegularExpression += '$';
            if (maxChars == null)
                _passwordPolicyDetails = 'Password must be contains minimum ' + minChars + ' characters in length, at least ' + countUPPERCASE + ' UPPERCASE letter, ' + countUPPERCASE + ' LOWERCASE letter, ' + countUPPERCASE + ' number';
            else if (minChars != null)
                _passwordPolicyDetails = 'Password must be contains between ' + minChars + ' - ' + maxChars + ' characters in length, at least ' + countUPPERCASE + ' UPPERCASE letter, ' + countUPPERCASE + ' LOWERCASE letter, ' + countUPPERCASE + ' number';

            if (countSpecial != null && countSpecial > 0)
                _passwordPolicyDetails += ', ' + countSpecial + ' special characters';

            $('#userPasswordPolicy').show();

            if (_passwordPolicyDetails != null && _passwordPolicyDetails.length > 0)
            {
                //$('#divUserPasswordPolicyContent').html(_passwordPolicyDetails);

                $('#spnCountLowerCase').html(countLOWERCASE);
                $('#spnCountUpperrCase').html(countUPPERCASE);
                $('#spnCountNumer').html(countNumber);
                $('#spnLength').html(minChars);
                $('#spnMaxCharacterCount').html(maxChars);
                $('#spnSpecialCharacterCount').html(countSpecial);
                
                if (!countLOWERCASE > 0)
                    $('#lowerCase').hide();

                if (!countUPPERCASE > 0)
                    $('#upperCase').hide();
    
                if (!countNumber > 0)
                    $('#number').hide();
    
                if (!minChars > 0)
                    $('#length').hide();

                if (!maxChars > 0)
                    $('#maxCharacter').hide();

                if (!countSpecial > 0)
                    $('#specialCharacter').hide();
                
               
            }
                

        }
    }
}

function PasswordExpressionFail() {
    alert('Service failed.');
}

function CheckUserPasswordWithPolicy(password, userId) {
    if (password.match(_passwordRegularExpression) == null) {

        window.location.href = _baseURL + 'Admin/Users/' + userId + '/Edit';
    }

}

function PasswordOutOfPolicy() {
    var value = $('#hdnIsPasswordOutOfPolicy').val();
    if (value == "1") {
        _messageType = 'jError';

        message = "Your password does not meet minimum standards. <a style=' text-decoration:underline;' href='" + _baseURL + "Admin/Users/" + $('#hdnUserId').val() + "/Edit'>Click here</a> to change your password";
        NotificationMessageWithFixTime(message, _messageType, true, 12000);
    }
}

function PasswordExpired() {
    var value = $('#hdnIsPasswordExpired').val();
    var isPswOutOfPolicy = $('#hdnIsPasswordOutOfPolicy').val();

    if (isPswOutOfPolicy == "0" && value == "1") {

        _messageType = 'jError';

        message = "Your password has expired. <a style='text-decoration:underline;' href='" + _baseURL + "Admin/Users/" + $('#hdnUserId').val() + "/Edit'>Click here</a> to change your password";

        NotificationMessageWithFixTime(message, _messageType, true, 12000);
    }
}

function OpenUserPswPolicyPopUp(divId) {
    //madol popup code
    $("#dialog:ui-dialog").dialog("destroy");

    $('#' + divId).dialog({
        //height: 400,
        minHeight: 200,
        maxheight: 400,
        width: 450,
        modal: true,
        draggable: true,
        resizable: false
    });
}

function CloseUserPswPolicyPopUp(divId) {
    $("#dialog:ui-dialog").dialog("destroy");
    $('#' + divId).dialog("destroy");
    $('#' + divId).bind("dialogclose", function (event, ui) {
    });
}


// SNAPCHART RELATED

//success method
function SnapChartCompletedSuccess(result) {
    
    ShowHideImagePadding();

    var completedSeasons = GetCompletedSeasonName(result);
    var splitCompletedSeason = completedSeasons.split(',');

    for (var i = 0; i < splitCompletedSeason.length; i++) {

        var season = splitCompletedSeason[i];

        if (season != '') {

            $('#div' + season).removeClass('snapChartDivPadding');
            $('#div' + season).addClass('snapChartDivPaddingCompleted');

            $('#img' + season).removeClass('seasonContentHide');
            $('#img' + season).addClass('seasonContentShow');
           
        }

    }

}

function GetCompletedSeasonName(seasonId) {

    var seasonname = (seasonId == SUMMER_SEASON) ? 'Spring'
                                                 : (seasonId == AUTUMN_SEASON) ? 'Spring,Summer'
                                                 : (seasonId == WINTER_SEASON) ? 'Spring,Summer,Autumn' : '';
    return seasonname;

}


//show  or Hide the padding of Completed image
function ShowHideImagePadding() {

    $('#divSpring').removeClass('snapChartDivPaddingCompleted');
    $('#divSpring').addClass('snapChartDivPadding');

    $('#divSummer').removeClass('snapChartDivPaddingCompleted');
    $('#divSummer').addClass('snapChartDivPadding');

    $('#divAutumn').removeClass('snapChartDivPaddingCompleted');
    $('#divAutumn').addClass('snapChartDivPadding');

}

//Deprecated one
function InternetExplorerVersionGet()
    // Returns the version of Internet Explorer or a -1
    // (indicating the use of another browser).
{    
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

//Currently using this.
function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    // If IE, return version number.
    if (Idx > 0) 
        return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

        // If IE 11 then look for Updated user agent string.
    else if (!!navigator.userAgent.match(/Trident\/7\./)) 
        return 11;

    else
        return 0; //It is not IE
}


//*************************************SNAPCHART PAGE OPENING COMMON FUNCTION ***************//
function OpenSnapChartPage(url) {
    newWindow = window.open(url, 'SnapCharT', 'height=660,width=1300,resizable=yes,scrollbars=yes');
    //newWindow = window.open(url, 'SnapCharT', 'height=660,width=1300,resizable=yes');    
    if (window.focus) {
        newWindow.focus()
    }

    return false;
}

function SnapchartUrl(isWinter) {

    //This will make the snapchart appear as winter snapchart
    _isWinter = isWinter; 

    var url = _baseURL + 'Event/' + _module + '/SnapCharT/' + _eventId;
    return url;
}


// This is the callback function invoked if the Web service  failed. It accepts the error object as a parameter.
function FailedCallback(error) {

    // Display the error
    $('#UserMessage').innerHTML = "Service Error: " + error.get_message();
    $('#UserMessage').show();

}


function SetTextByGoogleTranslater()
{

    var setTextByGoogleTranslater = true;
    var langaugeCode = $.cookie('googtrans');

    if (langaugeCode != null) {
        
        langaugeCode = langaugeCode.substring(langaugeCode.lastIndexOf('/') + 1);

        if (langaugeCode == _englishLanguage && langaugeCode == _spanishLanguage && langaugeCode == _frenchLanguage && langaugeCode == _russianLanguage
            && langaugeCode == _germanLanguage && langaugeCode == _portugueseLanguage)
        {
            setTextByGoogleTranslater = false;
        }      
    }

    return setTextByGoogleTranslater;
}

function GetLanguageCode() {

    var langaugeCode = '';
    if ($.cookie != undefined && $.cookie('googtrans') != null) {

        langaugeCode = $.cookie('googtrans');
        langaugeCode = langaugeCode.substring(langaugeCode.lastIndexOf('/') + 1);

    }

    return langaugeCode;
}
