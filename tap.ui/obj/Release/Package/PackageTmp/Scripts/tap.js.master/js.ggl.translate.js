﻿//global variables
_langaugeName = '';
_langaugeCode = '';
$(document).ready(function () {    
    var languageDetails = ParseToJSON(localStorage["languageDetails"]);
    
    //get the language from cookie and set.  
    if ($.cookie !=undefined && $.cookie('googtrans') != null)    {
        _langaugeCode = $.cookie('googtrans');
        _langaugeCode = _langaugeCode.substring(_langaugeCode.lastIndexOf('/') + 1);

        //fetch the language name and display.
        SetLanguage(_langaugeCode, languageDetails[2]);
    }
    else
        if ($('#spnLanguageName')[0] != undefined) {
            $('#spnLanguageName')[0].innerHTML = languageDetails[2];
        }

    $('#google_translate_element select').live("change", function (event) {     
        _langaugeCode = $("select.goog-te-combo option:selected").val();
        _langaugeName = $("select.goog-te-combo option:selected").text();
        $('#spnLanguageName')[0].innerHTML = _langaugeName;
       
        if (_langaugeCode != '')
            InsertLangaugeDetails(_langaugeCode);
        else {
            _messageType = 'jSuccess';
            NotificationMessage('Please select Language to save.', _messageType, true, 1000);
        }
    });

});

//open Google Tranlation pop up page.
function OpenGoogleTranslatePage()
{
    $("#dialog:ui-dialog").dialog("destroy");

    $("#divEditLaungaugeDialog").dialog({
        width: 450,
        height: 150,
        modal: true,
        draggable: false,
        resizable: false

    });
    $('.ui-dialog-title').text('Specify Language');
    $('#divEditLaungaugeDialog').show();
}


//on change of dropdown - save the language code in Users table.
function InsertLangaugeDetails(langaugeCode) {

    var languageData = new LanguageDataDetail(langaugeCode, $('#hdnCompanyId').val(), $('#hdnUserId').val());
    var data = JSON.stringify(languageData);
    
    // Change the localStorage values, update the existing language with the new selected language 
    var langDetails = ParseToJSON(localStorage["languageDetails"]);
    langDetails[1] = langaugeCode;
    langDetails[2] = $('.goog-te-combo').find('option:selected').text();
    localStorage["languageDetails"] = JSON.stringify(langDetails);

    _serviceUrl = _baseURL + 'Services/Users.svc/InsertLangaugeDetails';
    AjaxPost(_serviceUrl, data, eval(InsertLangaugeDetailsSuccess), eval(InsertLangaugeDetailsFail));

}

function InsertLangaugeDetailsSuccess(result) {
   
    _messageType = 'jSuccess';
    NotificationMessage(_langaugeName +' preferred language saved successfully.', _messageType, true, 1000);
    $('#spnLanguageName')[0].innerHTML = _langaugeName;
}

function InsertLangaugeDetailsFail() {
    alert('Service Failed.');
}

function LanguageDataDetail(langaugeCode, companyId, userId) {

    this.langaugeCode = langaugeCode;
    this.companyId = companyId;
    this.userId = userId;
}

//Logo navigation to home page
function TaprootLogoNavigation() {

    window.location.href = _baseURL + 'Home';
}

function SetLanguage(langaugeCode) {

    var languageName = new languageNameDetail($('#hdnCompanyId').val(), $('#hdnUserId').val(), langaugeCode);
    var data = JSON.stringify(languageName);
    _serviceUrl = _baseURL + 'Services/Users.svc/GetLangaugeDetails';
    AjaxPost(_serviceUrl, data, eval(GetLangaugeDetailsSuccess), eval(GetLangaugeDetailsFail));

}

function languageNameDetail(companyId, userId, langaugeCode) {
    this.companyId = companyId;
    this.userId = userId;
    this.langaugeCode = langaugeCode;
}

function GetLangaugeDetailsSuccess(result) {
   
    if (result.d != '[]') {
        var jsonData = ParseToJSON(result.d);
        $('#spnLanguageName')[0].innerHTML = jsonData;
    }
    else
        $('#spnLanguageName')[0].innerHTML = 'English';

}

function GetLangaugeDetailsFail() {
    alert("Service Failed.");
}

function SetLanguage(langCode, langName)
{
    evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    $('#spnLanguageName').text(langName);
    
    if ($('.goog-te-combo').length > 0) {        
        setTimeout(function () {
            $('.goog-te-combo').val(langCode)[0].dispatchEvent(evt);
        }, 3000);        
    }
}