﻿
$(document).ready(function () {

    

    $('#anchSignOut').click(function () {        
        sessionStorage.Change = 'undefined';
        //clear javascript session values
        
        sessionStorage.clear();
        localStorage.clear();
    });

    $('#TapRootLogoLink').attr('href', _baseURL + 'Home');
  

    $('#ConfigurationTabs, #SystemTabs').mouseenter(function (event) {
        var x = $(this).offset().top;
        var y = $('#AdminTabs').offset().top;
        var z = $(this).height();
        var topValue = x - (y + z);
        $(this).children('ul').css({ 'top': topValue });
    });

    //Apply css class on click
    $('#TapRooTHeader_Menu_navTabs ul li').click(function () {
        $('#TapRooTHeader_Menu_navTabs ul li').children('ul').removeClass("subtabstyleonclick");
        $(this).children('ul').addClass("subtabstyleonclick");

    });

    //Apply css class on mouse leave
    $('#TapRooTHeader_Menu_navTabs ul').mouseleave(function () {
        $('#TapRooTHeader_Menu_navTabs ul li').children('ul').removeClass("subtabstyleonclick");
        $(this).children('ul').removeClass("subtabstyleonclick");
        $('#ConfigurationTabs').children('ul').removeClass("configurationsubtabstyleonclick");
        $('#SystemTabs').children('ul').removeClass("systemsubtabstyleonclick");

    });

    //Apply css class for the related ul on click of configuration menu item
    $('#ConfigurationTabs').click(function () {
        $('#TapRooTHeader_Menu_navTabs ul li').children('ul').removeClass("subtabstyleonclick");
        $('#SystemTabs').children('ul').removeClass("systemsubtabstyleonclick");
        $(this).children('ul').addClass("configurationsubtabstyleonclick");

    });

    //Apply css class for the related ul on click of system menu item
    $('#SystemTabs').click(function () {
        $('#TapRooTHeader_Menu_navTabs ul li').children('ul').removeClass("subtabstyleonclick");
        $('#ConfigurationTabs').children('ul').removeClass("systemsubtabstyleonclick");
        $(this).children('ul').addClass("systemsubtabstyleonclick");

    });

    //Apply css class on mouse click of ul > li tag of ConfigurationTabs
    $('#ConfigurationTabs ul li').click(function () {
        $(this).children('ul').addClass("configurationsubtabstyleonclick");
        $('#SystemTabs').children('ul').removeClass("configurationsubtabstyleonclick");
    });


    $('#SystemTabs ul li').click(function () {
        $(this).children('ul').addClass("systemsubtabstyleonclick");
        $('#ConfigurationTabs').children('ul').removeClass("configurationsubtabstyleonclick");
    });

    //Apply css class on mouse leave of ul tag of ConfigurationTabs
    $('#ConfigurationTabs ul').mouseleave(function () {
        $('#ConfigurationTabs ul li').children('ul').removeClass("configurationsubtabstyleonclick");
        $(this).children('ul').removeClass("configurationsubtabstyleonclick");
        $('#ConfigurationTabs ul').removeClass("configurationsubtabstyleonclick");
    });

    //Apply css class on mouse leave of ul tag of SystemTabs
    $('#SystemTabs ul').mouseleave(function () {
        $('#SystemTabs ul li').children('ul').removeClass("systemsubtabstyleonclick");
        $(this).children('ul').removeClass("systemsubtabstyleonclick");

    });

    //Apply css class on mouse leave of ul tag of ConfigurationTabs
    $('#ConfigurationTabs').mouseenter(function (event) {
        $('#SystemTabs').children('ul').removeClass("systemsubtabstyleonclick");
    });

    //Apply css class on mouse leave of ul tag of SystemTabs
    $('#SystemTabs').mouseenter(function (event) {
        $('#ConfigurationTabs').children('ul').removeClass("configurationsubtabstyleonclick");
    });


});


