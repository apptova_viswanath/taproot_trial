﻿
//** Investigation Process Page Methods **// NOT IN USE NOW

//Page load Events
$(document).ready(function () {

    //highlight the tab color
    TabColor();

    //Jquery Main Tabs
    JqueryTabs();

});

//highlight the tab color
function TabColor() {
    //debugger;
    var url = jQuery(location).attr('href');
    var splitUrl = url.split('/');

    var tabId = splitUrl[splitUrl.length - 2]; //get Sub Tab Id
    tabId = tabId.substring(tabId.lastIndexOf('-') + 1);

    //For Subtab color change
    $('#anc' + tabId).removeClass('event-subtabs');
    $('#anc' + tabId).addClass('event-subtabs-selected');

}

//Jquery Main Tabs
function JqueryTabs() {
    //Jquery Tabs
    $(function () {

        //generate tabs
        $('#tabs').tabs();

        GetQueryStringFromUrl();

        $('#ancmain' + _moduleId)[0].offsetParent.className = 'ui-state-default ui-corner-top ui-tabs-selected ui-state-active';

        //remove default Tab selection color
        if (_moduleId != 1) {
            $('#tabs li')[0].className = '';
            $('#tabs li')[0].className = 'ui-state-default ui-corner-top';
        }

    });

}
