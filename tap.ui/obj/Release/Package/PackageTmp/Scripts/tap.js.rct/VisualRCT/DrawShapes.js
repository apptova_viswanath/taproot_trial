﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
function DrawCircle(context, canvas, radius) {

    // save state
    context.save();

    // translate contexttext
    context.translate(canvas.width / 2, canvas.height / 2);

    // scale contexttext horizontally
    context.scale(2, 1);

    // draw circle which will be stretched into an oval
    context.beginPath();

    //context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.arc(-20, 5, radius, 0, Math.PI * 2, false);

    // restore to original state
    context.restore();

    // apply styling
    context.fill();

}


//Draw rectangle  for near and basic RCT nodes
function DrawRectangle(container, context, canvas, isLastRectangle, isFirstRectangle, drawCurvedRect) {

    var lineContext = canvas.getContext("2d");
    lineContext.beginPath();
    //ClearShadow(context); // UNCOMMENT THESE 2 LINES FOR ADDING SHADOW TO ONLY RECTANGLES
    //ClearShadow(lineContext);

    //for the first node connector will come from the  parent and for all other nodes it will connected to that line between first node and parent
    //var x = 50;
    //var y = (isFirstRectangle) ? 0 : 10;
        
    var x = 50;
    var y = ($(canvas).attr('id') == CANVAS + RCID_TOLERABLE_FAILURE) ? 10 : (isFirstRectangle) ? 0 : 10; // For TOLERABLE_FAILURE, no need of connector from parent node

    
    if ($(canvas).attr('id') == CANVAS + RCID_DESIGN_SPECIFICATIONS) {

        lineContext.moveTo(x, y + 10);
        lineContext.lineTo(x, 30);
        lineContext.stroke();
        lineContext.closePath();
    }

    else if (!drawCurvedRect) {

        lineContext.moveTo(x, y);
        lineContext.lineTo(x, 30);
        lineContext.stroke();
        lineContext.closePath();
    }

    if ($(canvas).attr('id') == CANVAS + RCID_DESIGN) {// connecting line between design and child node
        lineContext.beginPath();
        lineContext.moveTo(x, y + 90);
        lineContext.lineTo(x, 90);
        lineContext.stroke();
        lineContext.closePath();
    }

    //Draw the upper horizontal line from that connector[line between first node and parent] to the last rct node in teh same line
    if (!isLastRectangle) {

        if ($(canvas).attr('id') == CANVAS + RCID_DESIGN_SPECIFICATIONS) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 600, 10);
            lineContext.stroke();
            lineContext.closePath();
        }
        else if ($(canvas).attr('id') == CANVAS + RCID_DESIGN_NOT_TO_SPECIFICATIONS) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 130, 10);
            lineContext.stroke();
            lineContext.closePath();
        }

        else if ($(canvas).attr('id') == CANVAS + RCID_DESIGN) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 420, 10);
            lineContext.stroke();
            lineContext.closePath();
        }
        else if ($(canvas).attr('id') == CANVAS + RCID_NO_PM_FOR_EQUIPMENT) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 105, 10);
            lineContext.stroke();
            lineContext.closePath();
        }
        else if ($(canvas).attr('id') == CANVAS + RCID_SPECIFICATIONS_NI) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 130, 10);
            lineContext.stroke();
            lineContext.closePath();
        }
        else if (($(canvas).attr('id') == CANVAS + RCID_EQUIPMENT_PARTS_DEFECTIVE) || ($(canvas).attr('id') == CANVAS + RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE)) {
            lineContext.beginPath();
            lineContext.moveTo(x, 10);
            lineContext.lineTo(x + 230, 10);
            lineContext.stroke();
            lineContext.closePath();
        }
        else {
            if (!drawCurvedRect) {
                lineContext.beginPath();
                lineContext.moveTo(x, 10);
                lineContext.lineTo(x + 150, 10);
                lineContext.stroke();
                lineContext.closePath();
            }
        }

    }   

    lineContext.beginPath();
    lineContext.moveTo(x, 10);
    lineContext.lineTo(x, 10);
    lineContext.stroke();
    lineContext.closePath();


    if (drawCurvedRect) {

        // REGION LINES - START - swapped the line and curved rectangle blocks - solved the green selection issue on quadratic curves

        context.beginPath();
        context.moveTo(5, 25);
        context.lineTo(10, 25);
        context.stroke();
        context.closePath();

        if (!isLastRectangle) {

            context.beginPath();
            context.moveTo(5, 25);
            context.lineTo(5, 42);
            context.save();
            context.stroke();
            context.closePath();

        }

        if (isFirstRectangle) {

            lineContext.beginPath();
            lineContext.moveTo(5, -15);
            lineContext.lineTo(5, 25);
            lineContext.stroke();
            lineContext.closePath();
        }
        else {

            lineContext.beginPath();
            lineContext.moveTo(5, -30);
            lineContext.lineTo(5, 25);
            lineContext.stroke();
            lineContext.closePath();
        }

        lineContext.beginPath();
        lineContext.moveTo(x, 10);
        lineContext.lineTo(x, 10);
        lineContext.stroke();
        lineContext.closePath();

        // REGION LINES - END

        // REGION CURVED RECTANGLE - START

        context.beginPath();

        //FIRST LINE
        context.moveTo(20, 10);
        context.lineTo(135, 10);

        //TOP RIGHT CURVE
        context.quadraticCurveTo(135 + 10, 10, 135 + 10, 20);

        //RIGHT LINE     
        context.lineTo(135 + 10, 30);

        //BOTTOM RIGHT CURVE
        context.quadraticCurveTo(135 + 10, 40, 135, 40);

        //BOTTOM LINE
        context.lineTo(20, 40);

        //BOTTOM CURVE
        context.quadraticCurveTo(10, 40, 10, 30);

        //LEFT LINE
        context.lineTo(10, 20);

        //TOP LEFT CURVE
        context.quadraticCurveTo(10, 10, 20, 10);


        context.stroke();

        // REGION CURVED RECTANGLE - END
       
    }
    else {

        var isManagementSystemNodes = ($(canvas).parent().parent().attr('id')  == 'div' + RCID_HPD_MANAGEMENT_SYSTEM);
        var isCommunicationNodes = ($(canvas).parent().parent().attr('id')  == 'div' + RCID_COMMUNICATIONS);
        var isDesignSpecChildNodes = ($(canvas).parent().parent().attr('id') == 'divWrapper' + RCID_DESIGN_SPECIFICATIONS);
        var isProcedureChildNodes = ($(canvas).parent().parent().attr('id') == 'div' + RCID_PROCEDURES);
        var isIndependentReviewNINode = $(canvas).attr('id') == CANVAS + RCID_INDEPENDENT_REVIEW_NI;
        var isPreventivePredictiveMaintainanceNode = $(canvas).attr('id') == CANVAS + RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE;


        if (isDesignSpecChildNodes || isIndependentReviewNINode || isProcedureChildNodes) {
            context.rect(1, 30, 120, 69);
        }
        else if(isPreventivePredictiveMaintainanceNode)
        {
            context.rect(1, 30, 130, 60);
        }

        else if (isManagementSystemNodes || isCommunicationNodes) {
            context.rect(1, 30, 130, 71);
        }
        else if (($(canvas).attr('id') == CANVAS + RCID_NO_PM_FOR_EQUIPMENT) || ($(canvas).attr('id') == CANVAS + RCID_PM_FOR_EQUIPMENT_NI)) {
            context.rect(1, 30, 100, 60);
        }
        else {
           
            //context.shadowBlur = 5  // UNCOMMENT THESE 4 LINES FOR ADDING SHADOW TO ONLY RECTANGLES
            //context.shadowColor = '#999';
            //context.shadowOffsetX = 3;
            //context.shadowOffsetY = 3;
            context.rect(1, 30, 120, 60);
        }

      
    }
    //context.stroke();  // UNCOMMENT FOR ADDING SHADOW TO ONLY RECTANGLES

}

function DrawConnectorLineBetweenTexts(context)
{

    //Line connector between texts
   // if (type == PLAIN_TEXT) {
        context.beginPath();
        context.moveTo(10, 55);
        context.lineTo(10 + 15, 55);
        context.stroke();
        context.closePath();

  //  }
}