﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
function DrawText(context, radius, text, type, font, rcId) {

    // draw font in red
    context.fillStyle = "black";
    context.font = font;
    var h = context.canvas.height;
    var w = radius;
    var x = context.canvas.clientLeft;
    var y = context.canvas.clientTop;
    var fh = 14;
    var spl = 2;

    var lines = SplitLines(context, w, context.font, text, type);
    // Block of text height
    var both = lines.length * (fh + spl);
    if (both >= h) {
        // We won't be able to wrap the text inside the area
        // the area is too small. We should inform the user 
        // about this in a meaningful way
    } else {

        // We determine the y of the first line
        var ly = (h - both) / 2 + y + spl * lines.length;
        var lx = 0;

        //Line connector between texts
         if (type == PLAIN_TEXT) {
            context.beginPath();
            context.moveTo(10, 55);
            context.lineTo(10 + 15, 55);
            context.stroke();
            context.closePath();


            //var image = document.createElement("img");
            //image.setAttribute('src', _baseURL + 'Images/GreenTick1.png');
            //context.drawImage(image,10 + 10, 35, 25, 25);
        }

        for (var j = 0, ly; j < lines.length; ++j, ly += fh + spl) {

            lx = x + w / 2 - context.measureText(lines[j]).width / 2;
            if (type == CIRCLE_SHAPE) {
                context.fillText(lines[j], lx + 85, ly + 13);
            }
            else if (type == PLAIN_TEXT){
                context.fillText(lines[j], 0 + 35, ly - 10);
            }
            else if (type == QUADRATIC_CURVE_SHAPE) {
               
               if (rcId == RCID_QUALITY_CONTROL) {
                   context.fillText(lines[j], lx + 4, ly + 7);
               }
               else {
                   context.fillText(lines[j], lx + 5, ly - 40);
               }
            }
            else {

                if ( rcId == RCID_DESIGN_SPECIFICATIONS 
                    || rcId == RCID_DESIGN_REVIEW || rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE || rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE 
                    || rcId == RCID_REPEAT_FAILURE || rcId == RCID_MANAGEMENT_SYSTEM || rcId == RCID_PM_NEEDS_IMPROVEMENT || rcId == RCID_DESIGN)
                {
                    context.fillText(lines[j], lx + 4, ly + 8);
                }
                else if (rcId == RCID_NO_PM_FOR_EQUIPMENT || rcId == RCID_PM_FOR_EQUIPMENT_NI) {
                     context.fillText(lines[j], lx - 1, ly + 8);
                }
              
                else {
                    context.fillText(lines[j], lx + 5, ly - 15);
                }
                
            }
        }
    }
}


function SplitLines(ctx, mw, font, text, type) {

    // We give a little "padding"
    mw = mw - 10;
    if (type == CIRCLE_SHAPE) {
        mw = 52;
    }
    
    // We setup the text font to the context (if not already)
    ctx.font = font;

    // We split the text by words 
    var words = text.split(' ');
    var new_line = words[0];
    var lines = [];
    
    for (var i = 1; i < words.length; ++i) {
        if (ctx.measureText(new_line + " " + words[i]).width < mw) {
            new_line += " " + words[i];
        }
        else {
            lines.push(new_line);
            new_line = words[i];
        }
    }

    lines.push(new_line);

    return lines;
}



function GetYPosition(context, radius, text, type, font, rcId, leftMargin, rightMargin) {

       
    var yPosition = 0;
    var h = context.canvas.height;
    var w = radius;
    var x = context.canvas.clientLeft;
    var y = context.canvas.clientTop;
    var fh = 14;
    var spl = 2;

    var lines = SplitLines(context, w, context.font, text, type);
    var both = lines.length * (fh + spl);

    if (both >= h) {
        // We won't be able to wrap the text inside the area
        // the area is too small. We should inform the user 
        // about this in a meaningful way
    } else {

        // We determine the y of the first line
        var ly = (h - both) / 2 + y + spl * lines.length;
        var lx = 0;

        if (type == CIRCLE_SHAPE) {
            yPosition = ly + 13;
        }
        else if (type == PLAIN_TEXT) {
                yPosition = ly - 10;
        }
        else if (type == QUADRATIC_CURVE_SHAPE) {

            if (rcId == RCID_QUALITY_CONTROL) {
                    yPosition = ly + 10;
            }
            else {
                    yPosition = ly - 37;
            }
        }
        else {
          
            if (rcId == RCID_DESIGN_REVIEW || rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE || rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE
                || rcId == RCID_REPEAT_FAILURE || rcId == RCID_PM_NEEDS_IMPROVEMENT || rcId == RCID_DESIGN) {

                yPosition = (lines.length == 1) ? ly + 22 : ly + 18;

            }           
            else if (rcId == RCID_DESIGN_SPECIFICATIONS || (rcId == RCID_MANAGEMENT_SYSTEM)) {
                
                yPosition = ly + 15;
            }
            else if ((rcId == RCID_TOLERABLE_FAILURE) || (rcId == RCID_NO_INSPECTION) || (rcId == RCID_CORRECTIVE_ACTION) ) {
                yPosition = ly;
            }
            else if (rcId == RCID_NO_PM_FOR_EQUIPMENT || rcId == RCID_PM_FOR_EQUIPMENT_NI) {
                yPosition = ly + 20;
            }
            else {
                //yPosition = (lines.length == 1) ? ly  : ly ;
                yPosition = (lines.length == 1) ? ly  : ly - 5;
            }
        }
    }

    return yPosition;
}