﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
//Hide if any Option tool bar is showing for any node, because at a time we are displaying only one/current option tool bar on the page.
function HideOptionToolBarAndShowCurrent(divTool) {
   
    $('[id^="divTools"]').hide();
    $(divTool).slideToggle("fast");

}


//Define the images which are need to be created for each RCT node
function DrawTools(container, rcId, nodeType, text) {
    
    //var div = document.createElement("div");
    //div.setAttribute("id", "divTools" + rcId);

    //var style = OPTION_TOOL_BAR_DIV_STYLE + ((rcId == RCID_PROCUREMENT) || (rcId == RCID_MANUFACTURING) || (rcId == RCID_HANDLING) || (rcId == RCID_STORAGE))
    //                                        ? MARGIN_LEFT_14PX + MARGIN_TOP_120PX
    //                                        : (rcId == RCID_QUALITY_CONTROL) ?  MARGIN_LEFT_14PX + MARGIN_TOP_25PX
    //                                        : (rcId == RCID_CORRECTIVE_ACTION) ?  MARGIN_LEFT_28PX + MARGIN_TOP_84PX
    //                                        : (nodeType == TREE_NODE_TYPE) ? MARGIN_LEFT_28PX + MARGIN_TOP_24PX
    //                                        : MARGIN_LEFT_14PX + MARGIN_TOP_90PX

    //div.setAttribute("style", style);


    //div.appendChild(CreateImageTools('imgSelectGreen' + rcId, 'Select', 'guidance select', _baseURL + 'Images/tick.png', 'SaveNodeStatus(' + rcId + ',' + true + ');'));
    //div.appendChild(CreateImageTools('imgAllQuestionToNo' + rcId, 'Set All Questions to NO', 'guidance allNo', _baseURL + 'Images/Delete.png', 'SaveNodeStatus(' + rcId + ',' + false + ');'));
    //div.appendChild(CreateImageTools('imgErase' + rcId, 'Erase', 'guidance erase', _baseURL + 'Images/Eraser.png', 'SaveNodeStatus(' + rcId + ',' + null + ');'));
    //div.appendChild(CreateImageTools('imgAnalysisComments' + rcId, 'Analysis Comments', 'guidance analysisStatus', _baseURL + 'Images/GreyStatus.png', 'DisplayAnalysisPopUpVR(' + rcId + ');'));
    //div.appendChild(CreateImageTools('imgQuestion' + rcId, 'Click to see Questions', 'guidance', _baseURL + 'Images/Question.png', 'DisplayGuidancePopUp(' + rcId + ',"RCTQuestionLink");'));
    //div.appendChild(CreateImageTools('imgGuidance' + rcId, 'Click to see Guidance', 'guidance', _baseURL + 'Images/lightbulb.png', 'DisplayGuidancePopUp(' + rcId + ',"RCTDictionaryLink");'));

    //container.appendChild(div);


    ////////////////NEED TO REMOVE
    //
    var guidanceCount = _guidanceCount;
    var div = document.createElement("div");
    div.setAttribute("id", "divTools" + rcId);

    var style =  (guidanceCount == 0) ? 145 : 175;
    style = "width:" + style + "px !important;";

  

    //styles based on root cause node type
    if (nodeType == TREE_NODE_TYPE) {
        style = style +  "z-index: 3;height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 28px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -24px !important;";
       // div.setAttribute("style",);
    }
    else {
       
        style = style + "z-index: 3 !important;height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -90px !important;";
        //div.setAttribute("style", );
    }
    //styles based on root cause node type

    //styles based on root cause node

    if ((rcId == RCID_PROCUREMENT) || (rcId == RCID_MANUFACTURING) || (rcId == RCID_HANDLING) || (rcId == RCID_STORAGE)) {
        style = style + "z-index: 3;height: 16px; padding-bottom: 8px; margin-top: -120px !important; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);"
       // div.setAttribute("style", );

    }
    else if (rcId == RCID_QUALITY_CONTROL) {
        style = style + "z-index: 3;height: 16px; padding-bottom: 8px; margin-top: -25px !important; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);"
       // div.setAttribute("style", );
    }
    else if (rcId == RCID_CORRECTIVE_ACTION) {
        style = style + "z-index: 3;height: 16px; padding-bottom: 8px; margin-top: -84px !important; margin-bottom: 3px; margin-left: 28px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);"
      //  div.setAttribute("style", );
    }
    //styles based on root cause node

    div.setAttribute("style", style);

    if (nodeType == TREE_NODE_TYPE) 
    {
        div.appendChild(CreateImageTools('imgSelectGreen' + rcId, 'Select', 'guidance select', _baseURL + 'Images/tick.png', 'UpdateStatusAndSectionForMainTreeNode(' + rcId + ',' + true + ');'));
        div.appendChild(CreateImageTools('imgAllQuestionToNo' + rcId, 'Set All Questions to NO', 'guidance allNo', _baseURL + 'Images/Delete.png', 'UpdateStatusAndSectionForMainTreeNode(' + rcId + ',' + false + ');'));
        div.appendChild(CreateImageTools('imgErase' + rcId, 'Erase', 'guidance erase', _baseURL + 'Images/Eraser.png', 'UpdateStatusAndSectionForMainTreeNode(' + rcId + ',' + null + ');'));

    }
    else{
        div.appendChild(CreateImageTools('imgSelectGreen' + rcId, 'Select', 'guidance select', _baseURL + 'Images/tick.png', 'SaveNodeStatus(' + rcId + ',' + true + ', null);'));
        div.appendChild(CreateImageTools('imgAllQuestionToNo' + rcId, 'Set All Questions to NO', 'guidance allNo', _baseURL + 'Images/Delete.png', 'SaveNodeStatus(' + rcId + ',' + false + ', null);'));
        div.appendChild(CreateImageTools('imgErase' + rcId, 'Erase', 'guidance erase', _baseURL + 'Images/Eraser.png', 'SaveNodeStatus(' + rcId + ',' + null + ', null);'));

    }

    div.appendChild(CreateImageTools('imgAnalysisComments' + rcId, 'Analysis Comments', 'guidance analysisStatus', _baseURL + 'Images/GreyStatus.png', 'DisplayAnalysisPopUpVR(' + rcId + ');'));
    div.appendChild(CreateImageTools('imgQuestion' + rcId, 'Click to see Questions', 'guidance', _baseURL + 'Images/Question.png', 'DisplayGuidancePopUp1(' + rcId + ',"RCTQuestionLink");'));

    if (guidanceCount != 0) {
        div.appendChild(CreateImageTools('imgGuidance' + rcId, 'Click to see Guidance', 'guidance', _baseURL + 'Images/lightbulb.png', 'DisplayGuidancePopUp(' + rcId + ',"RCTDictionaryLink");'));
    }

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute('id', 'hdnRootCauseNodeType' + rcId);
    hiddenField.setAttribute('value', nodeType);
    div.appendChild(hiddenField);

    container.appendChild(div);

}

function GetQuestionForRCTShapesSuccess(data) {
    
    //parse the json result to object
    data = ParseToJSON(data.d);

    $('#divRootCauseQuestions').show();

    //make template empty
    $("#rctQuestionTemplateResults").empty();

    //set the data returned to the template
    $("#rctQuestionTemplate").tmpl(data).appendTo("#rctQuestionTemplateResults");

    //DisplayDialog('#divRootCauseQuestions', 'xyz');
    $('#divRootCauseQuestions').dialog({

        height: 300,
        width: 478,
        modal: true,
        draggable: true,
        resizable: true,
        position: ['middle', 100],
        title: data[0].RCTTitle
    });

    var title = $('#divRootCauseQuestions').prev();
    $(title).removeClass('ui-widget-header');
    $(title).addClass('popupTitle');

    
    $('#rcType').val($('#hdnRootCauseNodeType' + data[0].RCID).val());
        
    var nodeStatus =  data[0].RCTValue;

    SetRadioButtonsInQuestionPopup(nodeStatus);

   

}


function SetRadioButtonsInQuestionPopup(nodeStatus) {
    
    if (nodeStatus == false) {

        $('#divRootCauseQuestions').find("input:radio[id^='rbNo']").prop('checked', true);
        $('#divRootCauseQuestions').find("input:radio[id^='rbYes']").prop('checked', false);

    }   
    else {

        var radioGroups =   $('#divRootCauseQuestions').find('#divRadioButtons').find('.userSelect');

        $(radioGroups).each(function () {

            var rbYes = $(this).find('#rbYes');
            var rbNo = $(this).find('#rbNo');

            var questionResponse = $(this).find('#hdnQuestionResponse').val();

            //Get the status for the radio buttons
            var rbYesCheckedStatus = (questionResponse == RESPONSE_TRUE) ? true : false;
            var rbNoCheckedStatus = (questionResponse == RESPONSE_FALSE) ? true : false;

            // reset radio button status
            $(rbYes).attr(CHECKED_ATTR, rbYesCheckedStatus);
            $(rbNo).attr(CHECKED_ATTR, rbNoCheckedStatus);

        });
    }
    
}


function DisplayGuidancePopUp1(rcId, title)
{    

    //Declare the parameter object
    var rctData = {};
    //debugger;
    rctData.rootCauseID = parseInt(rcId);
    rctData.causalFactorID = GetCausalFactorID();;
    rctData.userId = _userId
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetQuestionForRCTShapes';
    AjaxPost(serviceURL, rctData, eval(GetQuestionForRCTShapesSuccess), eval(FailedCallback));

}


//Create the analysis, select , select all no, erase, help and questions images for each node
//These will be hidden state and will be displayed on click of related RCT nodeDrawNearBasicRCTNode
function CreateImageTools(imageId, title, cssClass, imagePath, onClickEvent) {

    var image = document.createElement("img");
    image.setAttribute("id", imageId);
    image.setAttribute('title', title);
    image.setAttribute('class', cssClass);
    image.setAttribute('src', imagePath);
    image.setAttribute('border', '0');
    image.setAttribute('onclick', onClickEvent);
    image.setAttribute("style", OPTION_TOOL_BAR_IMAGE_STYLE);

    return image;

}


//Save the node status into database as per the user made changes [selected/unselected/erased]
function SaveNodeStatus(rctId, status, senderRadioButton) {
    
    _allQuestionUnselected = false;
    var effectSet = UpdateNodeStatusAndSection(rctId, status);

    if (effectSet) {

        var rctData = {};
        var questionResponse = '';

        //Get causal factor id
        rctData.causalFactorID = GetCausalFactorID();

        //Get rct id
        rctData.rootCauseID = parseInt(rctId);

        //get the user response
        rctData.checkedValue = status;

        
        var questionResponse = '';
        
        if (senderRadioButton != null) {

            var radioButtonGroup = $(senderRadioButton).parent();
            //var questionId = $(radioButtonGroup).parent().find('#rbYes').prop('name');

            var questionId = $(senderRadioButton).prop('name');
            var response = ($(radioButtonGroup).find('#rbYes').prop('checked')) ? true
                                : ($(radioButtonGroup).find('#rbNo').prop('checked')) ? false : null;
            questionResponse = questionResponse + questionId + ':' + response;

        }

        //Set the parameter values
        rctData.questionResponse = questionResponse;

        rctData.isVisualRCT = true;

        rctData.allQuestionUnselected = _allQuestionUnselected;

        rctData = JSON.stringify(rctData);


        //Call the service
        var serviceURL = _baseURL + 'Services/RootCause.svc/SaveRCTQuestions';
        AjaxPost(serviceURL, rctData, eval(SaveRCTQuestionsSuccess), eval(FailedCallback));

    }
    else {
        PreventDefault(senderRadioButton);
    }
}
