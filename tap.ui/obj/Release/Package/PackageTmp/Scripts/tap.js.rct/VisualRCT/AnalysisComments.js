﻿/********************************************************************************
 *  File Name   : VisualRCT.js
 *  Description : This file contains all the Visual RCT page functionalities
 *  Created on  : Jan 15 2015
 *  Created by  : Tapaswini Mohanty
 *******************************************************************************/
//function SetAnalysisComments(container, rcId, analysisComments) {

//    //Set Analysis Comments
//    var hiddenField = document.createElement("input");
//    hiddenField.setAttribute('type', 'hidden');
//    hiddenField.setAttribute("id", "hdnAnalysisComments" + rcId);
//    hiddenField.setAttribute('value', analysisComments);

//    container.appendChild(hiddenField);

//    if (analysisComments != '' && analysisComments != null) {

//        $('#imgAnalysisComments' + rcId)[0].prop('src', _baseURL + 'Images/GreenStatus.png');

//        SetCommentsImage(rcId, true);
//    }

//}


function SetCommentsImage(rootCauseID, displayImage) {


    if (displayImage) {

        var divTools = '#divTools' + rootCauseID;
        var commentsImage = document.createElement("img");
        commentsImage.setAttribute("id", "imgComments" + rootCauseID);

        var mainRCTAnalysisStyle = 'margin-left:-287px; margin-top:40px !important; position:absolute !important;';
        var basicRCTAnalysisStyle = 'margin-left:-15px; margin-top:-30px !important; position:absolute !important;';
        var nearRCTAnalysisStyle = 'margin-left:-315px; margin-top:22px !important; position:absolute !important;';
        var actualRCTAnalysisStyle = 'margin-left:-287px; margin-top:40px !important; position:absolute !important;';

        var rctNodeType = $('#hdnNodeType' + rootCauseID).val();

        var style = (rctNodeType == TREE_NODE_TYPE) ? mainRCTAnalysisStyle :
                    (rctNodeType == BASIC_ROOT_NODE_TYPE) ? basicRCTAnalysisStyle :
                    (rctNodeType == NEAR_ROOT_NODE_TYPE) ? nearRCTAnalysisStyle :
                    (rctNodeType == ACTUAL_ROOT_NODE_TYPE) ? actualRCTAnalysisStyle : '';


        commentsImage.setAttribute("style", style);
        var imageUrl = _baseURL + 'Images/comment.gif';
        commentsImage.setAttribute('src', imageUrl);


        $(commentsImage).insertAfter(divTools);

        //$(commentsImage).hide();

    }
    else {

        $('#imgAnalysisComments' + rootCauseID)[0].prop('src', _baseURL + 'Images/GreyStatus.png');
        $("#imgComments" + rootCauseID).remove();

    }

}



function DisplayAnalysisPopUpVR(rcId) {

    var dialog = $('#divanalysisComments'); //GetAnalysisCommentDiv(sender);
    $(dialog).find('#txtAnalysisComments').val($('#hdnAnalysisComments' + rcId).val());

    $(dialog).find('#btnSaveAnalysisComments').attr('title', '');
    $(dialog).find('#btnCancelAnalysisComments').attr('title', '');

    $(dialog).find('#hdnAnalysisComments').val($('#hdnAnalysisComments' + rcId).val());
    $(dialog).find('#hdnRCIDForAnalysisComments').val(rcId);

    OpenPopup(dialog, 'Analysis Comments');

}
