﻿
//GLOBAL VARIABLES
var _parent;
var _rcID = null;
var _guidanceCount = 0;
var _allQuestionUnselected = false;


// CONSTANT VARIABLES USED IN VISUALRCT PAGE
var TREE_NODE_TYPE = 5;
var BASIC_ROOT_NODE_TYPE = 2;
var ACTUAL_ROOT_NODE_TYPE = 3;
var NEAR_ROOT_NODE_TYPE = 1;
var QUESTION_NODE_TYPE = 4;

var CANVAS = 'canvas';
var CIRCLE_SHAPE = 'circle';
var PLAIN_TEXT = 'onlytext';
var RECTANGLE_SHAPE = 'rectangle';
var QUADRATIC_CURVE_SHAPE = 'quadratic';

var GREEN_COLOR = 'green';
var RED_COLOR = 'red';
var WHITE_COLOR = 'white';
var CONTEXT_DEFAULT_FILL_COLOR = 'white';
var CONTEXT_DEFAULT_STROKE_COLOR = 'black';

var GREEN_TICK_IMAGE = _baseURL + 'Images/GreenTick1.png';
var DELETE_RED_IMAGE = _baseURL + 'Images/DeleteRed.png'


//var SELECTED_STYLE = "'background-color': '#52C55A !important', 'color': '#FFFFCC !important'";
//var UN_SELECTED_STYLE = "'background-color': '#E32441 !important', 'color': 'FFBBBB !important'";
//var ERASED_STYLE = "'background-color': '#000000 !important', 'color': '#ededed !important'";

var FONT_11PX = "11px arial";
var FONT_12PX = "12px arial";
var FONT_13PX = "13px arial";

var GREEN_STATUS_STYLE = "margin-top: 34px !important;position: absolute !important;margin-left:25px !important; height:25px !important;;width:25px !important;";
var RED_STATUS_STYLE = "margin-top: 47px !important;position: absolute !important;margin-left:20px !important; height:20px !important;width:20px !important;";

/*In Equipment section, some nodes are found to be different shape even though they have same rootcause type as of other nodes.Example: Specification NI,
  which is of 'Root Cause' type , but diplsaying in rectangle in v5. while all the related 'Root Cause' type in the back page of V5 RCT are showing as plain text.
  Some of the nodes again in quadratic shape and rectangle shape even though they are all 'Near Root Cause' type. 
  Hence some function of VisualRCT code got dependent on static Root Cause ID.*/
var RCID_HUMAN_PERFORMANCE_DIFFICULTY = 2;
var RCID_PROCEDURES = 21
var RCID_TRAINING = 48
var RCID_COMMUNICATIONS = 70
var RCID_HPD_QUALITY_CONTROL = 61;
var RCID_HUMAN_ENGINEERING = 107;
var RCID_WORK_DIRECTION = 133;
var RCID_SUPERVISION_DURING_WORK = 148;


var RCID_EQUIPMENT_DIFFICULTY = 151;
var RCID_HPD_MANAGEMENT_SYSTEM = 84;
var RCID_TOLERABLE_FAILURE = 152;
var RCID_DESIGN = 153;
var RCID_DESIGN_SPECIFICATIONS = 154;
var RCID_SPECIFICATIONS_NI = 155
var RCID_DESIGN_NOT_TO_SPECIFICATIONS = 156;
var RCID_PROBLEM_NOT_ANTICIPATED = 157;
var RCID_EQUIPMENT_ENVIRONMENT_NOT_CONSIDERED = 158;
var RCID_DESIGN_REVIEW = 159;
var RCID_INDEPENDENT_REVIEW_NI = 160;
var RCID_EQUIPMENT_PARTS_DEFECTIVE = 163;
var RCID_PROCUREMENT = 164
var RCID_MANUFACTURING = 165;
var RCID_HANDLING = 166;
var RCID_STORAGE = 167;
var RCID_QUALITY_CONTROL = 168
var RCID_NO_INSPECTION = 169;
var RCID_QC_NI = 173;
var RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE = 177;
var RCID_PM_NEEDS_IMPROVEMENT = 178;
var RCID_NO_PM_FOR_EQUIPMENT = 179;
var RCID_PM_FOR_EQUIPMENT_NI = 180;
var RCID_REPEAT_FAILURE = 181;
var RCID_MANAGEMENT_SYSTEM = 182;
var RCID_CORRECTIVE_ACTION = 183;

var CANVAS_CONTAINER_STYLE = 'background-color:lightgrey;margin-bottom:5px';
var RECTANGLE_CANVAS_CONTAINER_STYLE = 'background-color:lightgrey;width:280px;height:auto;';
var BASIC_NODE_OF_15Q_OPTION_TOOLBAR_STYLE = "height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 10px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -8px !important;";


var DESIGN_STYLE = "width: 420px !important; display: inline-block !important;";
var EQUIPMENT_PARTS_DEFECTIVE_STYLE = "width: 160px; display: inline-block;";
var PREVENTIVE_PREDICTIVE_MAINTAINANCE_STYLE = "width: 230px; display: inline-block;";
var REPEAT_FAILURE_STYLE = "width: 150px;display: inline-block;";
var TOLERABLE_FAILURE_STYLE = "width: 150px; display: inline-block;float: left; ";
var DEFAULT_STYLE = "width: 150px; float: left; display: inline;";
var SUPERVISION_DURING_WORK_STYLE = "width: 150px; display: inline-block;";

var NO_WRAP_NEAR_NODE_STYLE = "width: 150px !important; display: inline-block !important;float: left;"; //NEED TO ADD FLOAT:LEFT ONLY FOR FIRST CHILD ITEM

var DESIGN_SPECIFICATIONS_STYLE = "width: 400px; float: left; display: inline;margin-top: -14px !important; margin-left: -150px !important;";
var DESIGN_SPECIFICATION_SUBNODE_STYLE = "width: 130px; float: left; display: inline;margin-top: -14px !important;";
var DESIGN_REVIEW_STYLE = "width: 150px; float: left; display: inline;margin-top: -14px !important;";

var QUALITY_CONTROL_STYLE = "width: 300px; margin-top: -114px !important; float: left; display: inline;";
var PM_NEEDS_IMPROVEMENT_STYLE = "width: 235px; float: left; display: inline;margin-top: -15px !important;";
var QUALITY_CONTROL_SUBNODE_STYLE = "width: 150px; float: left; display: inline; margin-top: -20px !important;";
var EQUIPMENT_PARTS_DEFECTIVE_SUBNODE_STYLE = "width: 130px; margin-top: -114px !important; float: left; display: inline;";
var EQUIPMENT_ROOT_CAUSE_NODE_STYLE = "width: 130px; float: left; display: inline;margin-top: -74px !important;";
var CORRECTIVE_ACTION_NODE_STYLE = "width: 130px; float: left; display: inline;margin-top: -14px !important;";
var PROCUREMENT_ROOT_CAUSE_NODE_STYLE = "width: 130px; float: left; display: inline;margin-top: -16px !important;";
var MANAGEMENT_ROOT_CAUSE_NODE_STYLE = "width: 130px; float: left; display: inline;margin-top: -16px !important;";

var ACTUAL_ROOT_CAUSE_STYLE = "width:20px;height:60px;display:block";
var FIRST_ROOT_CAUSE_STYLE = "width:20px;height:60px;display:block;margin-top:-80px";
var MANAGEMENT_SYSTEM_FIRST_ROOT_CAUSE_STYLE = "width:20px;height:60px;display:block;margin-top:-70px";
var PM_NI_SUBNODE_STYLE = "width: 105px; float: left; display: inline;margin-top: -14px !important;"

var PROCEDURE_FIRST_ROOT_CAUSE_STYLE = "width:20px;height:60px;display:block;margin-top:-70px";

var OPTION_TOOL_BAR_DIV_STYLE = "z-index: 2;height: 16px; padding-bottom: 8px; margin-bottom: 3px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);";
var OPTION_TOOL_BAR_IMAGE_STYLE = "display:inline;float:left;height:13px;width:13px";
var MARGIN_LEFT_28PX = "margin-left: 28px;";
var MARGIN_LEFT_14PX = "margin-left: 14px;";
var MARGIN_TOP_24PX = "margin-top: -24px !important;";
var MARGIN_TOP_90PX = "margin-top: -90px !important;";
var MARGIN_TOP_120PX = "margin-top: -120px !important;";
var MARGIN_TOP_25PX = "margin-top: -25px !important;";
var MARGIN_TOP_84PX = "margin-top: -84px !important;";

var _userLanguage = 'en';
var _germanLanguage = 'de';
var _frenchLanguage = 'fr';
var _spanishLanguage = 'es';
var _russianLanguage = 'ru';
var _portugueseLanguage = 'pt';

//===================================================================================================================================================================================
//===================================================================================================================================================================================

//Get first 4 nodes in the page
$(document).ready(function () {      
   

    GetMainTreeNodes();
      

    $('.nrct').live("click mouseover", function (event) {
        
        //CHROME FIX
        $('[id^="divTools"]').hide();
       
        var rcId = $(this).attr('id');
        var id = parseInt(rcId.substr(2, rcId.length - 1));
        var divTool = $(this).parent().find('#divTools' + id);
        $(divTool).attr("style", "z-index:3;height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 10px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none ; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -8px !important;");
       
        $(divTool).slideToggle("fast");

    });

    $('#lblSubHeader').hide();    


    $(document).keydown(function (e) {
        // ESCAPE key pressed
        if (e.keyCode == 27) {
            $('[id^="divTools"]').hide();
        }
    });



    $(document).click(function (event) {
        var target = $(event.target);

        if (target.attr('id') != undefined){
            if (!target.attr('id').match(/^divBlock/) && !target.attr('id').match(/^divTools/)) {
                $('[id^="divTools"]').hide();
            }
        }
        
    });

    $(document).ajaxComplete(function () {
        
        if(($.active - 1) == 0)
        {
            $('#DivloadingImage').hide();
        }

        //$('#DivloadingImage').hide();
    });
   
        //function checkPendingRequest() {
        //    if ($.active > 0) {
        //        window.setTimeout(checkPendingRequest, 1000);
        //        //Mostrar peticiones pendientes ejemplo: $("#control").val("Peticiones pendientes" + $.active);
        //    }
        //    else {

        //        alert("No hay peticiones pendientes");

        //    }
        //};

        //window.setTimeout(checkPendingRequest, 1000);
});


function StoreInHiddenField(container, id, value) {

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute("id", id);
    hiddenField.setAttribute('name', id);
    hiddenField.setAttribute('value', value);

    container.appendChild(hiddenField);

}



//// Hide and delete tab data
//function HideOrDeleteTabDataForVisualRCT(resultedArray) {

//    var nodeIdList = '';
//    var nodeId = '';

//    //Remove tab style and get the rct node id lists
//    for (i = 0; i <= resultedArray.length - 1; i++) {

//        nodeId = $(resultedArray[i]).attr('id');
//        nodeIdList = nodeIdList + ',' + nodeId.substr(2, nodeId.length - 1);

//        jQuery(resultedArray[i]).parent().fadeOut(1000);
//        if ((!IsVisualRCTPage())) {
//            jQuery(resultedArray[i]).removeAttr('style');
//        }
//    }

//    //Delete all node ids
//    if (nodeIdList != "") {
//        DeleteTabData(nodeIdList);
//    }

//}

//function RemoveRCTSection(resultedArray, radioButton) {

//    var removeTab = true;
//    var message = USER_MESSAGE;

//    //Iterate the hiding tab array list and update the message
//    for (i = 0; i <= resultedArray.length - 1; i++) {

//        //If 15 question tab is green marked/ iterated
//        //var tabStyle = jQuery(resultedArray[i]).attr('style');

//        //if ((tabStyle == TAB_ITERATED_STYLE) || (tabStyle == TAB_ITERATED_STYLE_IE)) {
//        if ($(resultedArray[i]).hasClass('selectedBasicNode'))
//        {
//            message = message + $(resultedArray[i]).html() + ', ';
//        }
               
//        //}

//    }

//    //Check if message is updated i.e there is tab to hide
//    if (message.length != USER_MESSAGE.length) {

//        // Add texts to message
//        message = message.substr(0, message.length - 2);
//        message = message + '  tab. Are you sure you want continue?';

//        //Confirm from user
//        removeTab = confirm(message);

//    }

//    if (removeTab) {

//        // Hide and delete tab data
//        HideOrDeleteTabDataForVisualRCT(resultedArray);

//    }
//    else {
//        PreventDefault(radioButton);
//    }

//    return removeTab;


//}


//If any of the node is selected, call this function for the rct id and get all teh child nodes related to it
//Reder those in canvas based on RCT category type
function RenderRCTSections(rcID, container, causalFactorID) {
        
    if (rcID != undefined) {

        _rcID = parseInt(rcID.substr(2, rcID.length - 1));
        _parent = container[0];
                   
        if(_rcID != RCID_EQUIPMENT_DIFFICULTY){
            $('#rc' + _rcID).addClass('nrct');    
        }

       //  if (($('#div' + _rcID).children('div').length == 0)) {
        if (!($('#div' + _rcID).children('div').is(':visible'))) {
            
            var rctData = {};
            rctData.rootCauseID = _rcID;
            rctData.causalFactorID = causalFactorID;
            rctData.userId = $('#hdnUserId').val();
            rctData = JSON.stringify(rctData);

            var serviceURL = _baseURL + 'Services/RootCause.svc/GetVisualRCT';
            PostAjax(serviceURL, rctData, eval(GetVisualRCTSuccesss), eval(FailedCallback));

        }
       
    }
}


//Get the child nodes/related sections of the selected RCT id and Draw the rct sections based on RCT category type
function GetVisualRCTSuccesss(data)
{
    var data = (data.d != null) ? ParseToJSON(data.d)[0] : data;

    var rcId = data.RCID;
    var nodeSelection = data.RCTValue;
    _guidanceCount = data.GuidanceCount;
    
    DrawStylesForBasicNodes(rcId, nodeSelection);

    DrawRCTSections(data.Children);
    SetAnalysisComments($('#div' + data.RCID)[0], data.RCID, data.AnalysisComments);
}


// This is the callback function invoked if the Web service  failed. It accepts the error object as a parameter.
function FailedCallback(error) {
    $('#UserMessage').innerHTML = "Service Error: " + error.get_message();
    $('#UserMessage').show();
}


function Shape(x, y, w, h, fill) {

    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.fill = fill;
}


//Create the analysis, select , select all no, erase, help and questions images for each node
//These will be hidden state and will be displayed on click of related RCT nodeDrawNearBasicRCTNode
function CreateImageTools(imageId,title,cssClass,imagePath,onClickEvent)
{
    var image = document.createElement("img");
    image.setAttribute("id", imageId);
    image.setAttribute('title', title);
    image.setAttribute('class', cssClass);
    image.setAttribute('src', imagePath);
    image.setAttribute('border', '0');
    image.setAttribute('onclick', onClickEvent);
    image.setAttribute("style", "display:inline;float:left;height:13px;width:13px");
    return image;

}


////Define the images which are need to be created for each RCT node
//function DrawTools(container, rcId, nodeType, text)
//{
    ////
    //var div = document.createElement("div");
    //div.setAttribute("id", "divTools" + rcId);

    ////styles based on root cause node type
    //if (nodeType == TREE_NODE_TYPE) {
    //    div.setAttribute("style", "z-index: 2;height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 28px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -24px !important;");
    //}
    //else{
    //    div.setAttribute("style", "z-index: 2;height: 16px; padding-bottom: 8px; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221); margin-top: -90px !important;");
    //}     
    ////styles based on root cause node type

    ////styles based on root cause node
   
    // if ((rcId == RCID_PROCUREMENT) || (rcId == RCID_MANUFACTURING) || (rcId == RCID_HANDLING) || (rcId == RCID_STORAGE))
    // {
    //     div.setAttribute("style", "z-index: 2;height: 16px; padding-bottom: 8px; margin-top: -120px !important; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);");

    // }
    // else if (rcId == RCID_QUALITY_CONTROL)
    // {
    //     div.setAttribute("style", "z-index: 2;height: 16px; padding-bottom: 8px; margin-top: -25px !important; margin-bottom: 3px; margin-left: 14px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);");
    // }
    // else if (rcId == RCID_CORRECTIVE_ACTION) {
    //     div.setAttribute("style", "z-index: 2;height: 16px; padding-bottom: 8px; margin-top: -84px !important; margin-bottom: 3px; margin-left: 28px; border-top-color: #9bbcdd; border-right-color: #9bbcdd; border-bottom-color: #9bbcdd; border-left-color: #9bbcdd; border-top-width: 1px; border-right-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-top-style: outset; border-right-style: outset; border-bottom-style: outset; border-left-style: outset; display: none !important; position: absolute !important; background-color: rgb(155, 188, 221);");
    // }
    ////styles based on root cause node


    // div.appendChild(CreateImageTools('imgSelectGreen' + rcId, 'Select', 'guidance select', _baseURL + 'Images/tick.png', 'SaveNodeStatus(' + rcId + ',' + true + ');'));
    // div.appendChild(CreateImageTools('imgAllQuestionToNo' + rcId, 'Set All Questions to NO', 'guidance allNo', _baseURL + 'Images/Delete.png', 'SaveNodeStatus(' + rcId + ',' +  false + ');'));
    // div.appendChild(CreateImageTools('imgErase' + rcId, 'Erase', 'guidance erase', _baseURL + 'Images/Eraser.png', 'SaveNodeStatus(' + rcId + ',' + null + ');'));
    // div.appendChild(CreateImageTools('imgAnalysisComments' + rcId, 'Analysis Comments', 'guidance analysisStatus', _baseURL + 'Images/GreyStatus.png', 'DisplayAnalysisPopUpVR(' + rcId + ');'));
    // div.appendChild(CreateImageTools('imgQuestion' + rcId, 'Click to see Questions', 'guidance', _baseURL + 'Images/Question.png', 'DisplayGuidancePopUp(' + rcId + ',"RCTQuestionLink");'));
    // div.appendChild(CreateImageTools('imgGuidance' + rcId, 'Click to see Guidance', 'guidance', _baseURL + 'Images/lightbulb.png', 'DisplayGuidancePopUp(' + rcId + ',"RCTDictionaryLink");'));

    // container.appendChild(div);

//}


function DisplayAnalysisPopUpVR(rcId)
{
    
    var dialog = $('#divanalysisComments'); //GetAnalysisCommentDiv(sender);
    var analysisComments = $('#hdnAnalysisComments' + rcId).val();
    
    analysisComments = ((analysisComments == null) || (analysisComments == "null"))  ? '' : analysisComments;

    $(dialog).find('#txtAnalysisComments').val(analysisComments);

    $(dialog).find('#btnSaveAnalysisComments').attr('title', '');
    $(dialog).find('#btnCancelAnalysisComments').attr('title', '');

    $(dialog).find('#hdnAnalysisComments').val($('#hdnAnalysisComments' + rcId).val());
    $(dialog).find('#hdnRCIDForAnalysisComments').val(rcId);

    OpenPopup(dialog, 'Analysis Comments');

}



//function RegisterClickEventForMainTreeNode(canvas) {


//    $(canvas).live("click", function (event) {


//        var rcTitle = $(this).next().next().val();
//        if (rcTitle == HUMAN_PERFORMANCE_DIFFICULTY) {
//            if (($('#divRCTQuestions').find('.greenCheck').length > 0) || ($('#divRCTQuestions').find('.redCross').length > 0)) {
//                alert(FIFTEEN_QUESTION_MESSAGE);
//                return false;

//            }
//        }


//        var canvasCtx = $(this)[0].getContext('2d');
//        var strokeStyle = canvasCtx.strokeStyle == "#90ee90" ? CONTEXT_STROKE_COLOR : 'lightgreen';

//        canvasCtx.strokeStyle = strokeStyle;
//        canvasCtx.save();
//        canvasCtx.restore();
//        canvasCtx.stroke();

//        var isSelected = strokeStyle == 'lightgreen' ? true : false;
//        var rcID = $(this).next().val();

//        SaveResponseAndGetQuestions(rcID, rcTitle, isSelected);

//    });
//}


function ClearCanvasColor(context)
{
    context.strokeStyle = WHITE_COLOR;
    context.save();
    context.restore();
    context.stroke();


    context.strokeStyle = WHITE_COLOR;
    context.save();
    context.restore();
    context.stroke();


    context.strokeStyle = WHITE_COLOR;
    context.save();
    context.restore();
    context.stroke();

  
}

function RePaintCanvas(context, newColor)
{
   //   
    ClearCanvasColor(context);

    context.lineWidth = (newColor == GREEN_COLOR) ? 3 : 1;

    context.strokeStyle = newColor;
    //canvas.save();
    //canvas.restore();
    context.stroke();

     
}



function CreateRedCrossCanvas(rcId)
{    

    var rcType = $('#hdnNodeType' + rcId).val();

    

        rcType = ((rcId == RCID_TOLERABLE_FAILURE) || (rcId == RCID_DESIGN) || (rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE) || (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE)
                 || (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE) || (rcId == RCID_REPEAT_FAILURE) || (rcId == RCID_SPECIFICATIONS_NI) || (rcId == RCID_DESIGN_NOT_TO_SPECIFICATIONS)
                 || (rcId == RCID_PROBLEM_NOT_ANTICIPATED) || (rcId == RCID_INDEPENDENT_REVIEW_NI) || (rcId == RCID_QUALITY_CONTROL) || (rcId == RCID_NO_PM_FOR_EQUIPMENT)
                 || (rcId == RCID_PM_FOR_EQUIPMENT_NI) || (rcId == RCID_CORRECTIVE_ACTION)) ? NEAR_ROOT_NODE_TYPE : rcType;

        if ((rcType == NEAR_ROOT_NODE_TYPE || rcType == TREE_NODE_TYPE || rcType == BASIC_ROOT_NODE_TYPE) ){

            var isQuadraticShape = IsCurvedRectangle(rcId);
            var isRectangulatShape = (rcType == NEAR_ROOT_NODE_TYPE);
            var isCircleShape = (rcType == TREE_NODE_TYPE);
            var isSmallRectangleShape = (rcId == RCID_NO_PM_FOR_EQUIPMENT || rcId == RCID_PM_FOR_EQUIPMENT_NI);
            var isBigRectangulatShape = (rcType == BASIC_ROOT_NODE_TYPE);
            var isTraining = (rcId == RCID_TRAINING);
            var isProcedure = (rcId == RCID_PROCEDURES);
            var isQuality = (rcId == RCID_HPD_QUALITY_CONTROL);
            var isCommunication = (rcId == RCID_COMMUNICATIONS);
            var isWorkDirection = (rcId == RCID_WORK_DIRECTION);
            var isHumanEngineering = (rcId == RCID_HUMAN_ENGINEERING);

            var isPreventivePredictiveMaintainance = (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE);
            var isManagementSystemChildNodes = ($('#canvas' + rcId).parent().parent().attr('id') == 'div' + RCID_HPD_MANAGEMENT_SYSTEM)
                                               || ($('#canvas' + rcId).parent().parent().attr('id') == 'div' + RCID_COMMUNICATIONS);

            var isManagementSystem = (rcId == RCID_HPD_MANAGEMENT_SYSTEM);

            var isDesignSpecChildNodes = ($('#canvas' + rcId).parent().parent().attr('id') == 'divWrapper' + RCID_DESIGN_SPECIFICATIONS);
            var isProcedureChildNodes = ($('#canvas' + rcId).parent().parent().attr('id') == 'div' + RCID_PROCEDURES);
            var isIndependentReviewNINode = (rcId == RCID_INDEPENDENT_REVIEW_NI); 
            var isMidHeightNode = (isDesignSpecChildNodes || isProcedureChildNodes || isIndependentReviewNINode);

        var leftTopPointX = isQuadraticShape ? 15 : isRectangulatShape ? 0 : isCircleShape ? 26 : isBigRectangulatShape ? 1: 0;
        var leftTopPointY = isQuadraticShape ? 11 : isRectangulatShape ? 30 : isCircleShape ? 53 : isBigRectangulatShape ? 2 : 0;
                    
        var rightBottomPointX = isPreventivePredictiveMaintainance? 131 : isQuadraticShape ? 146 : isSmallRectangleShape ? 100 : isRectangulatShape ? 122 : isCircleShape ? 196 : isBigRectangulatShape ? 192 : 0;
        var rightBottomPointY = isMidHeightNode? 100 : isQuadraticShape ? 39 : isRectangulatShape ? 90 : isCircleShape ? 106 : isBigRectangulatShape ? 30 : 0;

        var leftBottomPointX = isQuadraticShape ? 15 : isRectangulatShape ? 0 : isCircleShape ? 23 : isBigRectangulatShape ? 1 : 0;
        var leftBottomPointY = isMidHeightNode? 100 : isQuadraticShape ? 39 : isRectangulatShape ? 90 : isCircleShape ? 105 : isBigRectangulatShape ? 30 : 0;

        var rightTopPointX = isPreventivePredictiveMaintainance ? 131 : isQuadraticShape ? 146 : isSmallRectangleShape ? 100 : isRectangulatShape ? 122 : isCircleShape ? 194 : isBigRectangulatShape ? 190 : 0;
        var rightTopPointY = isQuadraticShape ? 11 : isRectangulatShape ? 30 : isCircleShape ? 53 : isBigRectangulatShape ? 2 : 0;
       
        if (isBigRectangulatShape || isManagementSystemChildNodes) {

            rightTopPointX = isManagementSystemChildNodes ? rightTopPointX + 10 : isManagementSystem ? 212 : isHumanEngineering ? 312: isProcedure ? 201 : isTraining ? 171 : isQuality ? 232 : isCommunication ? 181 : isWorkDirection ? 211 : rightTopPointX;
            rightBottomPointX = isManagementSystemChildNodes ? rightBottomPointX + 10 : isManagementSystem ? 212 : isHumanEngineering ? 312 : isProcedure ? 201 : isTraining ? 171 : isQuality ? 232 : isCommunication ? 181 : isWorkDirection ? 211 : rightBottomPointX;
            leftBottomPointY = rightBottomPointY = isManagementSystemChildNodes ? 100 : leftBottomPointY ;

        }


        var canvasWidth = (isHumanEngineering || isManagementSystem) ? 320 : (isQuality || isWorkDirection) ? 222 : isPreventivePredictiveMaintainance ? 138 : isManagementSystemChildNodes ? 130 : isQuadraticShape ? 142 : isRectangulatShape ? 120 : 210;
        var canvasheight = isRectangulatShape ? 100 : 150;

        var lineWidth = isRectangulatShape ? 1.5 : 2;

        var redCanvasId = 'canvas' + rcId + 'RedCross';
        var redCanvas = document.createElement("canvas");

        if ($('#' + redCanvasId).length == 0) {

            redCanvas.setAttribute('id', redCanvasId);
            $(redCanvas).prop('style', 'display:block;z-index:1;position:absolute; cursor: pointer;');
            redCanvas.setAttribute('style', 'display:block;z-index:1 !important;position:absolute !important; cursor: pointer;');
            redCanvas.setAttribute('height', canvasheight);
            redCanvas.setAttribute('width', canvasWidth);

            if (rcType == BASIC_ROOT_NODE_TYPE) {
                $(redCanvas).insertBefore($('#rc' + rcId));
                
            }
            else {
                $(redCanvas).insertBefore($('#canvas' + rcId)[0]);
            }

        }
        else {
            redCanvas = $('#' + redCanvasId)[0];
        }


        $(redCanvas).show();

        
        $(redCanvas).live("click mouseover", function (event) {
            
      //  $(redCanvas).live("click", function (event) {
            if (rcType != BASIC_ROOT_NODE_TYPE) {
                if (rcType == TREE_NODE_TYPE) {
                    $('#canvas' + rcId)[0].click();
                }
            }
            else {
               // $('#rc' + rcId).click();
            }
        
            });
        

        var context1 = redCanvas.getContext("2d");
        ClearCanvasColor(context1);
        context1.fillStyle = RED_COLOR;
        context1.strokeStyle = RED_COLOR;

        context1.beginPath();    

        context1.moveTo(leftTopPointX, leftTopPointY); //left top point    
        context1.lineTo(rightBottomPointX, rightBottomPointY); //right bottom point
    
        context1.moveTo(rightTopPointX, rightTopPointY); //right top point           
        context1.lineTo(leftBottomPointX, leftBottomPointY);//left bottom point        

        context1.lineWidth = lineWidth;
        context1.stroke();
        context1.closePath();

    }
}


function EraseRedCrossCanvas(rcId) {
    
    var redCanvasId = 'canvas' + rcId + 'RedCross';
    if ($('#' + redCanvasId).length != 0) {
    
        $('#' + redCanvasId).hide();
    }

}



function SetAnalysisComments(container, rcId, analysisComments) {

    //Set Analysis Comments
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute('type', 'hidden');
    hiddenField.setAttribute("id", "hdnAnalysisComments" + rcId);
    hiddenField.setAttribute('value', analysisComments);

    container.appendChild(hiddenField);

    if (analysisComments != '' && analysisComments != null) {

        if ($('#imgAnalysisComments' + rcId)[0] != undefined){
            $('#imgAnalysisComments' + rcId)[0].setAttribute('src', _baseURL + 'Images/GreenStatus.png');
        }
        
        SetCommentsImage(rcId,true );
    }

}


function SetCommentsImage( rootCauseID, displayImage)
{
    

    if (displayImage) {

        //var divTools = '#divTools' + rootCauseID;
        //var commentsImage = document.createElement("img");
        //commentsImage.setAttribute("id", "imgComments" + rootCauseID);

        //var mainRCTAnalysisStyle = 'margin-left:-115px; margin-top:34px !important; position:absolute !important;';
        //var basicRCTAnalysisStyle = 'margin-left:145px; margin-top:-45px !important; position:absolute !important;';
        //var nearRCTAnalysisStyle = 'margin-left:105px; margin-top:-140px !important; position:absolute !important;';
        //var actualRCTAnalysisStyle = 'margin-left:13px; margin-top:-116px !important; position:absolute !important;';
        
        //var isQuadraticShapeWithOutQualityControl = (rootCauseID == RCID_PROCUREMENT || rootCauseID == RCID_MANUFACTURING || rootCauseID == RCID_HANDLING || rootCauseID == RCID_STORAGE);

        //var rctNodeType = GetRootCauseTpeOfRCTNode(rootCauseID);

        ////var rctNodeType = $('#hdnNodeType' + rootCauseID).val();

        //var style = (rctNodeType == TREE_NODE_TYPE) ? mainRCTAnalysisStyle :
        //            (rctNodeType == BASIC_ROOT_NODE_TYPE) ? basicRCTAnalysisStyle :
        //            (rctNodeType == NEAR_ROOT_NODE_TYPE) ? nearRCTAnalysisStyle :
        //            (rctNodeType == ACTUAL_ROOT_NODE_TYPE) ? actualRCTAnalysisStyle : '';

        //var isProcedureAndTraining = (rootCauseID == RCID_PROCEDURES) || (rootCauseID == RCID_TRAINING);
        //var isQuality = (rootCauseID == RCID_HPD_QUALITY_CONTROL);
        //var isCommunication = (rootCauseID == RCID_COMMUNICATIONS);
        //var isWorkDirection = (rootCauseID == RCID_WORK_DIRECTION);
        //rightTopPointX = isProcedureAndTraining ? 121 : isQuality ? 181 : isCommunication ? 181 : isWorkDirection ? 161 : 190;

        //style = (rootCauseID == RCID_QUALITY_CONTROL) ? 'margin-left: -175px; margin-top:1px !important;position: absolute !important;'
        //    : isQuadraticShapeWithOutQualityControl ? 'margin-left:125px; margin-top:-154px !important; position:absolute !important;' :
        //    (rootCauseID == RCID_PROCEDURES) || (rootCauseID == RCID_TRAINING) ? 'margin-left:107px; margin-top:-45px !important; position:absolute !important;' :
        //    (rootCauseID == RCID_HPD_QUALITY_CONTROL) || (rootCauseID == RCID_COMMUNICATIONS) ? 'margin-left:165px; margin-top:-45px !important; position:absolute !important;' :
        //    (rootCauseID == RCID_WORK_DIRECTION) ? 'margin-left:145px; margin-top:-45px !important; position:absolute !important;' :
        //    (rootCauseID == RCID_HUMAN_ENGINEERING) || (rootCauseID == RCID_HPD_MANAGEMENT_SYSTEM) ? 'margin-left:178px; margin-top:-45px !important; position:absolute !important;' :

        //        (rootCauseID == RCID_DESIGN) || (rootCauseID == RCID_DESIGN_SPECIFICATIONS) || (rootCauseID == RCID_DESIGN_REVIEW)
        //        || (rootCauseID == RCID_EQUIPMENT_PARTS_DEFECTIVE) || (rootCauseID == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE) || (rootCauseID == RCID_REPEAT_FAILURE)
        //        || (rootCauseID == RCID_MANAGEMENT_SYSTEM)
        //        || (rootCauseID == RCID_PM_NEEDS_IMPROVEMENT)  ? 'margin-left: 105px;  margin-top: -90px !important;position: absolute !important;' 
        //        :(rootCauseID == RCID_NO_PM_FOR_EQUIPMENT) || (rootCauseID == RCID_PM_FOR_EQUIPMENT_NI) ?
        //        'margin-left: 80px;  margin-top: -90px !important;position: absolute !important;'
        //        : style;


        var divTools = '#divTools' + rootCauseID;
        var commentsImage = document.createElement("img");
        commentsImage.setAttribute("id", "imgComments" + rootCauseID);

        var mainRCTAnalysisStyle = 'margin-left:-115px; margin-top:34px !important; position:absolute !important;';
        var basicRCTAnalysisStyle = 'margin-left:145px; margin-top:-45px !important; position:absolute !important;';
        var nearRCTAnalysisStyle = 'margin-left:108px; margin-top:-138px !important; position:absolute !important;';
        var actualRCTAnalysisStyle = 'margin-left:13px; margin-top:-116px !important; position:absolute !important;';

        var rctNodeType = GetRootCauseTpeOfRCTNode(rootCauseID);

        var style = (rctNodeType == TREE_NODE_TYPE) ? mainRCTAnalysisStyle :
                    (rctNodeType == BASIC_ROOT_NODE_TYPE) ? basicRCTAnalysisStyle :
                    (rctNodeType == NEAR_ROOT_NODE_TYPE) ? nearRCTAnalysisStyle :
                    (rctNodeType == ACTUAL_ROOT_NODE_TYPE) ? actualRCTAnalysisStyle : '';

        if (rctNodeType == BASIC_ROOT_NODE_TYPE)
        {

            var isProcedure = (rootCauseID == RCID_PROCEDURES);
            var isTraining = (rootCauseID == RCID_TRAINING);
            var isCommunication = (rootCauseID == RCID_COMMUNICATIONS);
            var isWorkDirectionAndManagementSystem = (rootCauseID == RCID_WORK_DIRECTION) || (rootCauseID == RCID_HPD_MANAGEMENT_SYSTEM);
            var isQualityControlCommunication = (rootCauseID == RCID_HPD_QUALITY_CONTROL) || (rootCauseID == RCID_COMMUNICATIONS);
            var isHumanEngineering = (rootCauseID == RCID_HUMAN_ENGINEERING);

            var procedureAnalysisStyle = 'margin-left:188px; margin-top:-45px !important; position:absolute !important;';
            var trainingAnalysisStyle = 'margin-left:108px; margin-top:-45px !important; position:absolute !important;';
            var qualityControlCommunicationAnalysisStyle = 'margin-left:168px; margin-top:-45px !important; position:absolute !important;';
            var workDirectionAnalysisStyle = 'margin-left:198px; margin-top:-45px !important; position:absolute !important;';
            var humanEngineeringAnalysisStyle = 'margin-left:298px; margin-top:-45px !important; position:absolute !important;';
            
            style = isProcedure ? procedureAnalysisStyle : isTraining
                                ? trainingAnalysisStyle : isQualityControlCommunication
                                ? qualityControlCommunicationAnalysisStyle : isWorkDirectionAndManagementSystem
                                ? workDirectionAnalysisStyle : isHumanEngineering
                                ? humanEngineeringAnalysisStyle
                                : style;
        }

        if (rctNodeType == NEAR_ROOT_NODE_TYPE) {

            var isQuadraticShapeWithOutQualityControl = (rootCauseID == RCID_PROCUREMENT || rootCauseID == RCID_MANUFACTURING || rootCauseID == RCID_HANDLING || rootCauseID == RCID_STORAGE);
            var isEuipmentQualityControl = (rootCauseID == RCID_QUALITY_CONTROL);
            var pmForEquipmentNodes = (rootCauseID == RCID_NO_PM_FOR_EQUIPMENT) || (rootCauseID == RCID_PM_FOR_EQUIPMENT_NI);
            var equipmentRelatedNode = (rootCauseID == RCID_DESIGN) || (rootCauseID == RCID_DESIGN_SPECIFICATIONS) || (rootCauseID == RCID_DESIGN_REVIEW)
                                       || (rootCauseID == RCID_EQUIPMENT_PARTS_DEFECTIVE) || (rootCauseID == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE)
                                       || (rootCauseID == RCID_REPEAT_FAILURE) || (rootCauseID == RCID_MANAGEMENT_SYSTEM) || (rootCauseID == RCID_PM_NEEDS_IMPROVEMENT);

            var qualityControlAnlysisStyle = 'margin-left: -175px; margin-top:1px !important;position: absolute !important;';
            var quadraticShapeWithOutQualityControlAnlysisStyle = 'margin-left:125px; margin-top:-154px !important; position:absolute !important;';
            var equipmentRelatedNodeAnalysisStyle = 'margin-left: 108px;  margin-top: -88px !important;position: absolute !important;';
            var equipmentPMAnalysisStyle = 'margin-left: 80px;  margin-top: -88px !important;position: absolute !important;'
            
            style = isEuipmentQualityControl ? qualityControlAnlysisStyle : isQuadraticShapeWithOutQualityControl
                                             ? quadraticShapeWithOutQualityControlAnlysisStyle : equipmentRelatedNode
                                             ? equipmentRelatedNodeAnalysisStyle : pmForEquipmentNodes
                                             ? equipmentPMAnalysisStyle
                                             : style;
        }
       

        commentsImage.setAttribute("style", style);
        var imageUrl = _baseURL + 'Images/comment.gif';
        commentsImage.setAttribute('src', imageUrl);


        $(commentsImage).insertAfter(divTools);
       // $("#imgComments" + rootCauseID).hide();

   }
   else {
        
       $('#imgAnalysisComments' + rootCauseID)[0].setAttribute('src', _baseURL + 'Images/GreyStatus.png');
       $("#imgComments" + rootCauseID).remove();

    }

}



function doSomething()
{
    alert('1');
}


function AppendElementsAndStoreID(container, canvas,  rcID) {
    
    container.appendChild(canvas);
    StoreRCTID(rcID, container);
}


//Assign RCT id related to each shape in a hidden field next to each shape
function StoreRCTID(rcID, container) {
    
    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute('name', 'hdn' + rcID);
    hiddenField.setAttribute('value', rcID);
    container.appendChild(hiddenField);

}


//Assign RCT id related to each shape in a hidden field next to each shape
function StoreRCTTitle(text, container) {

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute('name', 'hdn' + text);
    hiddenField.setAttribute('value', text);
    container.appendChild(hiddenField);

}


//Get Root Cause type of the specific RCT node, which was alreday stored in a hidden variable while the corresponding section is rendered.
function GetRootCauseTpeOfRCTNode(rcId) {

    var rctNodeType = IfEquipmentRelatedSpecificNodes(rcId) ? NEAR_ROOT_NODE_TYPE : $('#hdnNodeType' + rcId).val();
    return rctNodeType;

}

/*Check if the current node belongs to those specific nodes from Eqipment section, which may or may not be near rct type, but is being rendered as Rectangle in v5 RCT.
Some nodes in Equipment Section which are of main tree node type [RCID_PROBLEM_NOT_ANTICIPATED, RCID_INDEPENDENT_REVIEW_NI, RCID_CORRECTIVE_ACTION], but shapes rendering 
in v5 are rectangles and some other nodes in Equipment SEction which are of Actual root node type [RCID_SPECIFICATIONS_NI, RCID_DESIGN_NOT_TO_SPECIFICATIONS, 
RCID_NO_PM_FOR_EQUIPMENT, RCID_PM_FOR_EQUIPMENT_NI], but shapes rendering in v5 are rectangles.
*/
function IfEquipmentRelatedSpecificNodes(rcId) {

    var isEquipmentRelatedSpecificNodes =  ((rcId == RCID_TOLERABLE_FAILURE) || (rcId == RCID_DESIGN) || (rcId == RCID_EQUIPMENT_PARTS_DEFECTIVE) || (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE)
   || (rcId == RCID_PREVENTIVE_PREDICTIVE_MAINTAINANCE) || (rcId == RCID_REPEAT_FAILURE) || (rcId == RCID_SPECIFICATIONS_NI) || (rcId == RCID_DESIGN_NOT_TO_SPECIFICATIONS)
   || (rcId == RCID_PROBLEM_NOT_ANTICIPATED) || (rcId == RCID_INDEPENDENT_REVIEW_NI) || (rcId == RCID_QUALITY_CONTROL) || (rcId == RCID_NO_PM_FOR_EQUIPMENT)
   || (rcId == RCID_PM_FOR_EQUIPMENT_NI) || (rcId == RCID_CORRECTIVE_ACTION));

    return isEquipmentRelatedSpecificNodes;

}


function UpdateStatusAndSetEffects(radioButton, event, rcId)
{
    
    var questionContent = $(radioButton).parents().eq(1);
    var userResponse = (questionContent.find("input:radio[id^='rbYes']:checked").length > 0) ? true
                          : (questionContent.find("input:radio[id^='rbNo']:checked").length == questionContent.find("input:radio[id^='rbNo']").length) ? false : null;

    var nodeType = $('#rcType').val();
    
    if (nodeType == TREE_NODE_TYPE) 
    {
        UpdateStatusAndSectionForMainTreeNode(rcId, userResponse, radioButton);
    }
    else {
        SaveNodeStatus(rcId, userResponse, radioButton);
    }
        
}

