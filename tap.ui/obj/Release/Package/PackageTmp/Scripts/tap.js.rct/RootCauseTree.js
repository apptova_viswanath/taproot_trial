﻿// THIS FILE HAS BEEN CREATED TO HOLD ALL THE FUNCTION RELATED TO ROOT CAUSE TREE MODULE


// Constants
var USER_RESPONSE_YES = 'Yes';
var USER_RESPONSE_NO = 'No';
var USER_RESPONSE_NULL = '';
var USER_SELECTED_IMAGE_CLASS = 'greenCheck';
var USER_UNSELECTED_IMAGE_CLASS = 'redCross';
var USER_SELECTED_LINK_CLASS = 'selectPath';
var USER_UNSELECTED_LINK_CLASS = 'unSelectPath';
var USER_MESSAGE = 'Unchecking this option will cause you to lose data on the ';
var HIGHERITEM_ELIMINATED_MESSAGE = 'This item can not be modified because a higher level item has been eliminated.';
var CHILDITEM_SELECTED_MESSAGE = "One or more causes are currently selected at a lower level.\nRemoving this cause will also remove them. Do you want to continue?";
var FIFTEEN_QUESTION_MESSAGE = "Human Performance Difficulty cannot be changed because there are answers selected on the 15 Questions";
var CHECKED_ATTR = 'checked';
var RESPONSE_TRUE = "true";
var RESPONSE_FALSE = "false";
var GETTING_STARTED_TAB_TEXT = 'Getting Started';
var FIFTEEN_QUESTION_TAB_TEXT = '15 Questions';
var ROOT_CAUSE_TREE_TAB_TEXT = 'RootCauseTree';
var EQUIPMENT_TAB_TEXT = 'Equipment Difficulty';
var GETTING_STARTED_TAB_ID = 'rc2';
var TAB_ITERATED_STYLE = "background-color: rgb(82, 197, 90); color: rgb(255, 255, 204);";
var TAB_ITERATED_STYLE_IE = "color: rgb(255, 255, 204); background-color: rgb(82, 197, 90);"
var FIFTEEN_QUESTION_TAB_TEXT_SPANISH = '15 Preguntas';

var _selectedLanguage = 'en';
//var _germanLanguage = 'de';
//var _frenchLanguage = 'fr';
//var _spanishLanguage = 'es';
//var _russianLanguage = 'ru';

var genericCauseCount = 0;

// Refresh parent page
function RefreshParent() {
    if (window.parent.opener != null || window.parent.opener != undefined) {
        window.parent.opener.PopulateCausalFactors(false, false, false);
    }
}

//window.onresize = function () {
//    // your code
//    var hgt = $('#divCausalFactor').height();
//    //document.getElementById('tabs').style["margin-top"] = hgt + "px";
//    $('#rctWindow').css("margin-top", hgt + 20 + "px");
//};


// Ready function
$(document).ready(function () {
    
    _userId = $('#hdnUserId').val();
    $('.googlefrenchNote').hide();
    $('.missingFrench').hide();
    $('.missingSpanish').hide();
    $('.missingFrench').parent().prev().hide();


    $('#divGenericCausesContent').hide();
    
    //hardcode translated messages for french and spanish
    var langaugeCode = $.cookie('googtrans');
    if (langaugeCode != null) {
        langaugeCode = langaugeCode.substring(langaugeCode.lastIndexOf('/') + 1);
        if (langaugeCode == 'fr') {
            FIFTEEN_QUESTION_MESSAGE = 'Difficulté de la performance humaine ne peut pas être modifié car il ya des réponses sélectionnées sur les 15 questions';
            HIGHERITEM_ELIMINATED_MESSAGE = 'Cet article ne peut pas être sélectionné car un élément de niveau supérieur a été éliminé.';
            CHILDITEM_SELECTED_MESSAGE = "Une ou plusieurs causes sont actuellement sélectionnés à un niveau inférieur. Retrait cette cause également les supprimer. Voulez-vous continuer?";
            USER_MESSAGE = 'Décochant cette option vous fera perdre des données sur le ';
        }
        else
            if (langaugeCode == _spanishLanguage) {
                FIFTEEN_QUESTION_MESSAGE = 'Dificultad Rendimiento Humano no se puede cambiar porque hay respuestas seleccionadas en las 15 preguntas';
                HIGHERITEM_ELIMINATED_MESSAGE = 'Este elemento no se puede seleccionar porque un elemento de nivel superior ha sido eliminado.';
                CHILDITEM_SELECTED_MESSAGE = 'Uno o más causas son seleccionados actualmente en un nivel inferior. La eliminación de esta causa también se eliminarán. ¿Quieres continuar?';
                USER_MESSAGE = 'Si se desactiva esta opción hará que la pérdida de datos en el ';
            }
        else
            if (langaugeCode == _russianLanguage) {
            FIFTEEN_QUESTION_MESSAGE = 'Человек Производительность Сложность не может быть изменена , потому что есть ответы , выбранные на 15 вопросов';
            HIGHERITEM_ELIMINATED_MESSAGE = 'Этот пункт не может быть изменен , потому чтовыше уровня пункт был исключен.';
            CHILDITEM_SELECTED_MESSAGE = 'Один или несколько причин в настоящее время выбран на более низком уровне . \n Removing эту причину также удалить их . Вы хотите продолжить?';
            USER_MESSAGE = 'Снятие этого параметра может привести к потере данных на ';
        }
    }
    var splittedURL = window.location.href.split('/');
    splittedURL[splittedURL.length - 2] = 'VisualRCT-' + splittedURL[splittedURL.length - 2].split('-')[1] ;
    var url = '';
    
    for (i = 0; i <= splittedURL.length -1; i++) {
       
        url += splittedURL[i];
        if(i != splittedURL.length - 1)
        {
            url += "/";
        }
    }
    

    //$('#lnkVisualRCT').attr('href', url);
  
    if (!(IsVisualRCTPage())) {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie < 0) {
            $('html, body').css('overflow', 'hidden');
        }

        //Display the rct questions on click of each header link, except generic cause link
        $('#divLinkContent a').live("click", function (event) {

            if ($(this).attr('id') != 'lnkGenericCause') {
                DisplayRCTNodeQuestions(this);
                $('#divGenericCausesContent').hide();
            }
        });
    }
  
    
    //Display the rct questions on click of each header link, except generic cause link
    $('#chkComplete').live("change", function (event) {
        UpdateRCTStatus($(this).attr('checked'));
    });

    
    LoadForm();

});
// End ready function



// Update the RCT selected/ unselected status into database
function UpdateRCTStatus(status)
{
    var causalFactorData = {};

    //Get causal factor id
    causalFactorData.causalFactorId = GetCausalFactorID();
    causalFactorData.status = status;

    causalFactorData = JSON.stringify(causalFactorData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/UpdateRCTStatus';
    AjaxPost(serviceURL, causalFactorData, eval(UpdateRCTStatusSuccess), eval(FailedCallback));

}


// On success of UpdateRCTStatus, show the success notification
function UpdateRCTStatusSuccess() {

    NotificationMessage('Status updated.', 'jSuccess', true, 3000);
    //window.opener.AllRCTComplete();
    //window.opener.PopulateCausalFactors(false, false, false);
    window.parent.opener.GetInvestigationSession();

}


//Load the page 
function LoadForm() {
        
    
    //Hide generic cause section
    $('#divGenericCauseLink').hide();    

    //Set Header
    $('#TapRooTHeader_BreadCrumb_navigationBreadCrumb').html('Root Cause Tree®');
    
    //Get the causal factor for which rct will be designed and the status of user tab selections
    GetCausalFactorNameAndTabStatus();

    //Design the tabs
    JqueryTabs();

    var control = $("#ulSubTabs").find("div > a:contains('TapRooT®')");
    control.removeClass('event-subtabs');
    control.removeClass('subTabsMainDiv');
    control.addClass('event-subtabs-selected');

    //Display only getting started tab
    $("#divLinkContent").find(jQuery("div > a[name=rc1-]")).parent().show();

    $('.focused').next('.arrow-down').show();
     
}


//Display generic cause tab if any of YES is selected and if nothing is selected from the tab, remove the traversed state from the current tab
function ResetTopLevelTabState(data) {

    if (($('#divAccordion').find('.greenCheck').length == 0) && ($('#divAccordion').find('.redCross').length == 0)) {
        jQuery('.focused').removeAttr('style');
    }   

    //if (data.d > 0) {
    //    $('#divGenericCauseLink').show();
    //}
    //else {
    //    $('#divGenericCauseLink').hide();
    //}

    //Create service method parameter
    var causalFactorId = GetCausalFactorID();
    var causalFactorData = {};
    causalFactorData.causalFactorID = causalFactorId;
    causalFactorData = JSON.stringify(causalFactorData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetGenericCauseCount';
    AjaxPost(serviceURL, causalFactorData, eval(GetGenericCauseCountSuccess), eval(FailedCallback));
}


// If user has created generic causes then show the generic cause div, otherwise hide it
function GetGenericCauseCountSuccess(data) {
    
    if (data.d > 0) {
        //$('#divGenericCauseLink').show();
        $('#divGenericCauseLink').fadeIn(1000);

        if (IsVisualRCTPage()) {
            // if (!($('#divGenericCauseLink').is(':visible')))
            //if (genericCauseCount != data.GenericCausesCount)
            //{
            // alert('Populate');
            DisplayGenericCauseQuestions();
            //}

        }
    }
    else {
        //$('#divGenericCauseLink').hide();
        $('#divGenericCauseLink').fadeOut(1000);
    }



}


// Get title header div
function GetTitleHeader(divStatusImage, isFromRadioButton, radioButton) {

    var titleHeader = '';

    if (isFromRadioButton) {

        var questionContent = $(radioButton).parents().eq(1);
        titleHeader = questionContent.parent().parent().find('ul').first();

    }
    else {
        titleHeader = $(divStatusImage).next('div').children('#divRCTTitle');
    }

    return titleHeader;
}


// Get the corresponding rct node id, related to user response for any rct question
function GetCorrespondingRCTNodeID(parentDiv) {

    //Get the rct id of the element selected/unselected
    var rctNodeID = parentDiv.find('input:hidden').attr('id');

    parentDiv.find('input:hidden').attr("title", USER_RESPONSE_YES);

    rctNodeID = rctNodeID.substr(3, rctNodeID.length - 1); //for ex: get rc1 from hdnrc1

    return rctNodeID;

}


//Save user response for rct question node 
function SaveRCTQuestionNode(correspondingLinkID, userResponse, isNearRootCauseUpdate) {

    
    var rctData = {};
    var questionResponse = '';

    //Get causal factor id
    rctData.causalFactorID = GetCausalFactorID();

    //Get rct id
    rctData.rootCauseID = parseInt(correspondingLinkID.substr(2, correspondingLinkID.length - 1));

    //get the user response
    rctData.checkedValue = (userResponse == USER_RESPONSE_YES) ? true : (userResponse == USER_RESPONSE_NO) ? false : null;

    var radioGroups = $('#hdn' + correspondingLinkID).parent().next().children().first().find('.userSelect');

    if (!isNearRootCauseUpdate) {

        // Get all the radio button response: question id and user response
        $(radioGroups).each(function () {

            var questionId = $(this).find('#rbYes').prop('name');

            var response = ($(this).find('#rbYes').prop('checked')) ? true
                                : ($(this).find('#rbNo').prop('checked')) ? false : null;

            if (questionResponse != '') {
                questionResponse = questionResponse + ',';
            }

            questionResponse = questionResponse + questionId + ':' + response;

        });
    }

    //Set the parameter values
    rctData.questionResponse = questionResponse;
    rctData.isVisualRCT = false;
    rctData.AllQuestionUnselected = false;
    
    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/SaveRCTQuestions';
    AjaxPost(serviceURL, rctData, eval(SaveRCTQuestionsSuccess), eval(FailedCallback));

}


//On success of  SaveRCTQuestionNode, reset the top level tabs and refresh parent
function SaveRCTQuestionsSuccess(data) {
                
    ResetTopLevelTabState(data);
    RefreshParent();

}


//Make the accordion as multiple div in expandable mode
function SetAsMultipleDivExpandableAccordion(ui) {

    // The accordion believes a panel is being opened
    if (ui.newHeader[0]) {

        var currHeader = ui.newHeader;
        var currContent = currHeader.next('.ui-accordion-content');

    }
    else { // The accordion believes a panel is being closed

        var currHeader = ui.oldHeader;
        var currContent = currHeader.next('.ui-accordion-content');

    }

    // Since we've changed the default behavior, this detects the actual status
    var isPanelSelected = currHeader.attr('aria-selected') == 'true';

    // Toggle the panel's header
    currHeader.toggleClass('ui-corner-all', isPanelSelected)
                .toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

    // Toggle the panel's icon
    currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

    // Toggle the panel's content
    currContent.toggleClass('accordion-content-active', !isPanelSelected)
    if (isPanelSelected) { currContent.slideUp(); } else { currContent.slideDown(); }

}


// Display rct nodes and related questions for the current clicked tab
function DisplayRCTNodeQuestions(topHeader) {

    $('.arrow-down').hide();

    //Get the id related to current tab
    var focusedTabID = $(topHeader).attr('id');

    //remove the old focused class set at prev element
    $('.focused').removeClass('focused');

    $(topHeader).addClass('focused');

    //Hide the content specific to getting started
    $('.focused').next('.arrow-down').show();


    //Get questions for the current/focused link
    GetQuestions(focusedTabID);

}


//Get causal factor name and all top header tab status
function GetCausalFactorNameAndTabStatus() {    
    
    //Create service method parameter
    var causalFactorId = GetCausalFactorID();
    var causalFactorData = {};

    causalFactorData.causalFactorId = causalFactorId;
    causalFactorData.userId = parent.$('#hdnUserId').val();
    causalFactorData = JSON.stringify(causalFactorData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetCausalFactorNameAndTabStatus';
    AjaxPost(serviceURL, causalFactorData, eval(GetCausalFactorNameAndTabStatusSuccess), eval(FailedCallback));

}

function IsVisualRCTPage()
{
    var splittedURL = window.location.href.split('/');
    return ((splittedURL[splittedURL.length - 2].split('-')[0]).toUpperCase() == 'VISUALRCT');
}

//On success of Get Causal Factor Name And Tab Status
function GetCausalFactorNameAndTabStatusSuccess(data) {
    
    //Parse the json result to object
    data = ParseToJSON(data.d)[0];

    _userLanguage = data.RCTLanguageName;

    $('#chkComplete').attr('checked', data.IsCompleted);

    //Set causal factor label
    $("#lblCausalFactor").html($("#lblCausalFactor").html() + data.CausalFactorName);
    
    var hgt =  $('#divCausalFactor').height();
    document.getElementById('rctWindow').style["margin-top"] = hgt + "px";
    $('#rctWindow').css("margin-top", hgt + 20 + "px !important"); 

    //Display generic cause
    if (data.GenericCausesCount > 0) {
      
        //if (genericCauseCount == 0) {
        //    genericCauseCount = data.GenericCausesCount;
        //}

        if(IsVisualRCTPage())
        {
            // if (!($('#divGenericCauseLink').is(':visible')))
            //if (genericCauseCount != data.GenericCausesCount)
            //{
               // alert('Populate');
                DisplayGenericCauseQuestions();
            //}
            
        }

        $('#divGenericCauseLink').show();
    }

  //  if (!IsVisualRCTPage()) {
        SetHeaderTextByLanguage(data);
    //}

    //based on tab status display each tab
    var tabStatus = data.TabStatus;

    for (i = 0 ; i <= tabStatus.length - 1; i++) {

        var id = 'rc' + tabStatus[i].RootCauseID;       
            
        if (tabStatus[i].ParentTitle == '15 Questions') {
            $('#rc3').css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });
        }

        if (tabStatus[i].ParentTitle == ROOT_CAUSE_TREE_TAB_TEXT) {
            $('#rc1').css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });
        }
        if (tabStatus[i].ParentTitle == 'Equipment Difficulty') {
            $('#rc151').css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });
        }
      
        if (tabStatus[i].Title != 'Equipment Difficulty') {

            if (tabStatus[i].IsSelected != null) {

                if (tabStatus[i].IsSelected) {
                    $('#' + id).css({ 'background-color': '#52C55A !important', 'color': '#FFFFCC !important' });
                }
                else {
                    $('#' + id).css({ 'background-color': '#E32441 !important', 'color': '#FFBBBB !important' });
                }
            }
          
        }                  

        if (tabStatus[i].IsSelected) {

            jQuery("[name*=" + id + "-]").parent().show();
            
            if (IsVisualRCTPage()) {

                                
                if ((tabStatus[i].RootCauseCategoryID == 4)) { // 5 is for equipement difficulty

                    var sections = jQuery("[name*=" + id + "-]");

                    for (j = 0 ; j <= sections.length - 1; j++) {

                        var id = $(sections[j]).attr('id');
                        var sectionHeader = $(sections[j]).parent();

                        var causalFactorID = GetCausalFactorID();

                        RenderRCTSections(id, sectionHeader, causalFactorID);
                    }
                }
                
            }
        }
    }
        
    //Get all the questions for the getting started tab
    GetQuestions($('.focused').attr('id'));

}


function SetHeaderTextByLanguage(data)
{

    //Assign the multi language values
    if (_userLanguage != _germanLanguage && _userLanguage != _frenchLanguage && _userLanguage != _spanishLanguage && _userLanguage != _russianLanguage) {

        $('.headerTab').removeClass('notranslate');
    }
    else {

        $('.headerTab').addClass('notranslate');
       
        $('#rc151').html(data.EquipmentText);
        $('#rc21').html(data.ProcedureText);
        $('#rc48').html(data.TrainingText);
        $('#rc61').html(data.QualityControlText);
        $('#rc70').html(data.CommunicationText);
        $('#rc84').html(data.ManagementText);
        $('#rc107').html(data.HumanEngineeringText);
        $('#rc133').html(data.WorkDirectionText);

    }
}


//Get causal factor id from url
function GetCausalFactorID() {

    var splittedURL = window.location.href.split('/');
    return parseInt(splittedURL[splittedURL.length - 2].split('-')[1]);
    //return 2378;
}


//Get all questions related to focused link
function GetQuestions(focusedLinkID) {

    if (focusedLinkID != undefined)
    {
        $('#DivloadingImage').show();
        $('#divGettingStartedHeader').hide();
        
        $('#divParent').hide();
        $('#dicToolTitles').hide();
        var rctData = {};
              
        //Pass the parameters
        rctData.rootCauseID = parseInt(focusedLinkID.substr(2, focusedLinkID.length - 1)); //remove rc from the id [rc151]
        rctData.causalFactorID = GetCausalFactorID();
        var languageId =_userId;
        rctData.userId = parent.$('#hdnUserId').val();
      
        rctData = JSON.stringify(rctData);

        //Call the service
        var serviceURL = _baseURL + 'Services/RootCause.svc/GetRCTQuestions';
        AjaxPost(serviceURL, rctData, eval(GetQuestionsSuccess), eval(FailedCallback));

    }
}


//Set accordion
function SetAccordion(sender) {
    
    $(sender).accordion();
    $(sender).accordion('destroy').accordion({

        header: " div > ul", autoHeight: false, collapsible: true,

        beforeActivate: function (event, ui) {

            SetAsMultipleDivExpandableAccordion(ui);
            return false; // Cancels the default action
        }

    });

}


//Hide the first accordion header
function HideFirstAccrodionHeader(isGettingStartedTab, is15QuestionTab, firstTitle)
{

    if (isGettingStartedTab || is15QuestionTab) {

        $(firstTitle).parent().parent().children().last().css({
            'margin-left': '-35px',
            'margin-top': '-40px'
        });

        $(firstTitle).parent().parent().prev().hide();
        $(firstTitle).text('');

        $("#divAccordion").find('.guidance').first().hide();
        $("#divAccordion").find('.blank').first().hide();
        $("#divAccordion").find('.allNo').first().hide();
        $("#divAccordion").find('.erase').first().hide();
        $("#divAccordion").find('.select').first().hide();

    }
    else {

        $('#divParent').first().addClass('container');
        $('#divRCTTitle').first().addClass('basicCauseHeader');
    }

}


// Unbind all events from the first header
function UnBindAllEventsFromHeader() {

    var divTitle = $("#divAccordion").find('.parent').first().children().first();

    $(divTitle).removeClass();
    $(divTitle).children().first().removeClass();
    $(divTitle).removeAttr('class');
    $(divTitle).removeAttr('role');
    $(divTitle).removeAttr('aria-controls');
    $(divTitle).removeAttr('tabindex');

    $(divTitle).unbind("click");
    $(divTitle).unbind("hover");

    $("#divAccordion").find('.parent').first().children().last().removeClass('ui-widget-content');

}


//Set Header text
function SetHeaderText(isGettingStartedTab, is15QuestionTab) {

    $('#divGettingStartedHeader').show();

    var hedaerText = isGettingStartedTab ? 'Getting Started ' : is15QuestionTab ? 'Human Performance Troubleshooting Guide' : '';
    var subHeaderText = isGettingStartedTab ? 'Was this causal factor related to:' : '';

    $("#lblHeader").html(hedaerText);
    $("#lblSubHeader").html(subHeaderText);

    if (subHeaderText != null) {
        $("#lblSubHeader").show();
    }
    else {
        $("#lblSubHeader").hide();
    }

}


// Set accordion styles for 15 questions tab
function SetAccordionFor15QuestionTab() {       

    var allAccordions = $('.parent');


    var ipLabelText = (_selectedLanguage == _frenchLanguage) ? 'Performance individuelle' : (_selectedLanguage == _spanishLanguage) ? 'Desempeño Individual' : (_selectedLanguage == _russianLanguage) ? 'Индивидуальный Производительность' : 'Individual Performance';
    var tmLabelText = (_selectedLanguage == _frenchLanguage) ? "Performance d'équipe" : (_selectedLanguage == _spanishLanguage) ? 'Actuación del Equipo' : (_selectedLanguage == _russianLanguage) ? 'Команда Производительность' : 'Team Performance';
    var msLabelText = (_selectedLanguage == _frenchLanguage) ? 'Système de gestion' : (_selectedLanguage == _spanishLanguage) ? 'Sistema de Gestión' : (_selectedLanguage == _russianLanguage) ? 'СИСТЕМА УПРАВЛЕНИЯ' : 'Management System';


    var individualSection = allAccordions.slice(1, 9).parent();
    SetAccordionHeaders(individualSection, ipLabelText);

    var teamSection = allAccordions.slice(9, 12).parent();
    SetAccordionHeaders(teamSection, tmLabelText);

    var managementSection = allAccordions.slice(12, 16).parent();
    SetAccordionHeaders(managementSection, msLabelText);
    $(managementSection).last().addClass('bottomSection');

    $('.ui-accordion .ui-accordion-header').addClass('accordionHeader');
    $('.ui-accordion .ui-accordion-content').addClass('accordioncontent');        

}


// Set accordion styles for equipment tab
function SetAccordionForEquipmentTab() {

    var ulHeader = $('#divRCTTitle').first();
    $(ulHeader).parent().prevAll().hide();
    $(ulHeader).next().children().first().hide();
    $(ulHeader).next().children().last().css({
        'margin-left': '-15px',
        'margin-top': '-10px'
    });

}

function PopulateRCTQuestions(data) {
    //var germanLanguage = 'de';
    //var frenchLanguage = 'fr';
    //var spanishLanguage = 'es';

    var selectedLanguage = data[0].RCTLanguageName;
    _selectedLanguage = selectedLanguage;
    

    if (selectedLanguage != _germanLanguage && selectedLanguage != _frenchLanguage && selectedLanguage != _spanishLanguage && selectedLanguage != _russianLanguage)
    {
        $('#divAccordion').removeClass('notranslate');
        $('#divTraslateQuestions').removeClass('notranslate');
    }
    else
    {
        $('#divAccordion').addClass('notranslate');
        $('#divTraslateQuestions').addClass('notranslate');       
    }


    $('#questions').show();

    //make template empty
    $("#divAccordion").empty();

    //set the data returned to the template
    $("#rctTemplate").tmpl(data).appendTo("#divAccordion");

    //Set accordion
    SetAccordion("#divAccordion");

    var firstTitle = $("#divAccordion").find('.parent').first().children().first().find('.title').first();
    //var firstTitleText = $(firstTitle).text();
  //  debugger;
    var firstTitleText = data[0].EnglishTitle[0];

    var isGettingStartedTab = (firstTitleText == ROOT_CAUSE_TREE_TAB_TEXT);
    //var is15QuestionTab = (firstTitleText == FIFTEEN_QUESTION_TAB_TEXT);
    var is15QuestionTab = (firstTitleText == FIFTEEN_QUESTION_TAB_TEXT) || (firstTitleText == FIFTEEN_QUESTION_TAB_TEXT_SPANISH);

    var isEquipmentTab = (firstTitleText == EQUIPMENT_TAB_TEXT);

    //Hide the first accordion header
    HideFirstAccrodionHeader(isGettingStartedTab, is15QuestionTab, firstTitle);

    // Unbind all events from the first header
    UnBindAllEventsFromHeader();

    //Set divStatusImage class as per the current tab fetched rct node values
    SetStatusImage();

    //Set Header text
    SetHeaderText(isGettingStartedTab, is15QuestionTab);

    //Set analysis status images
    SetAnalysisStatus();

    // Set different styles for sub accordions of 15 question tab
    if (is15QuestionTab) {
        SetAccordionFor15QuestionTab();
    }

    if (isGettingStartedTab) {
        $("#divAccordion").accordion({ active: 1 });
    }

    if (!(isGettingStartedTab || is15QuestionTab)) {
        $('#divRCTTitle').first().addClass('basicCauseHeader');
    }

    // Set different styles for equipment tab
    if(isEquipmentTab) {
        SetAccordionForEquipmentTab();
    }

    $('.userSelect').find('#rbYes').attr('title', USER_RESPONSE_NULL);
    $('.missing userSelect').find('#rbNo').attr('title', USER_RESPONSE_NULL);
    $(".googlefrenchNote").hide();

}

//On success of GetQuestions
function GetQuestionsSuccess(data) {
    
    //parse the json result to object
    data = ParseToJSON(data.d);

    PopulateRCTQuestions(data);
    // $('#divRCTQuestions').removeClass('DisableDiv');
    $('#divParent').show();
    $("#DivloadingImage").hide();
    $('#dicToolTitles').show();
    $('.missingFrench').hide();
    $('.missingSpanish').hide();
    $('.missingFrench').parent().prev().hide();
    $('.missingSpanish').parent().prev().hide();
    $('.googlefrench').hide();
    $('.googlefrench').parent().prev().hide();
  
    $('.missingEnglish').hide();
    $('.missingEnglish').parent().prev().hide();

    $('.missingRussian').hide();
    $('.missingRussian').parent().prev().hide();

    $('.missingGerman').hide();
    $('.missingGerman').parent().prev().hide();

    var hgt = $('#divCausalFactor').height();
    //document.getElementById('tabs').style["margin-top"] = hgt + "px";
    $('#rctWindow').css("margin-top", hgt + 20 + "px !important");
}


// Set accordion headers for 15 question tab
function SetAccordionHeaders(section, title) {

    $(section).addClass('side');
    $(section).first().addClass('top');
    $(section).last().addClass('bottom');
    $(section).first().find('#lblTextHeader').show();
    $(section).first().find('#lblTextHeader').html(title);

}


//Set divStatusImage class as per the current tab fetched rct node values
function SetStatusImage() {

    var statusImages = $('.statusContent');
    var imageClass = '';
    var title = '';
    var cssClass = '';
    var correspondingLinkID = '';

    $(statusImages).each(function () {

        //Set the background image
        title = $(this).attr('name');
        imageClass = (title == RESPONSE_TRUE) ? USER_SELECTED_IMAGE_CLASS : title == RESPONSE_FALSE ? USER_UNSELECTED_IMAGE_CLASS : USER_RESPONSE_NULL;
        $(this).removeClass().addClass(imageClass);

        //Get related title value fetched for the corresponding rct id
        cssClass = (title == RESPONSE_TRUE) ? USER_SELECTED_LINK_CLASS : title == RESPONSE_FALSE ? USER_UNSELECTED_LINK_CLASS : USER_RESPONSE_NULL;

        //Get rct node id for that div image 
        correspondingLinkID = GetCorrespondingRCTNodeID($(this).parent());

        //Set the class for the top main links, if not answered by the user before
        if (jQuery("[name*=" + correspondingLinkID + "-]").attr('style') == undefined) {
            jQuery("[name*=" + correspondingLinkID + "-]").addClass(cssClass);
        }
            
        var questionContent = $(this).next('div').children('#divRCTTitle').next().children().eq(0);

        if (title == RESPONSE_FALSE) {

            questionContent.find("input:radio[id^='rbNo']").prop(CHECKED_ATTR, true);
            questionContent.find("input:radio[id^='rbYes']").prop(CHECKED_ATTR, false);

        }
        else {

            var radioGroups = $(questionContent).find('.userSelect');

            $(radioGroups).each(function () {

                var rbYes = $(this).find('#rbYes');
                var rbNo = $(this).find('#rbNo');

                var questionResponse = $(this).find('#hdnQuestionResponse').val();

                //Get the status for the radio buttons
                var rbYesCheckedStatus = (questionResponse == RESPONSE_TRUE) ? true : false;
                var rbNoCheckedStatus = (questionResponse == RESPONSE_FALSE) ? true : false;

                // reset radio button status
                $(rbYes).attr(CHECKED_ATTR, rbYesCheckedStatus);
                $(rbNo).attr(CHECKED_ATTR, rbNoCheckedStatus);
                   
            });
        }
    });
}


// This is the callback function invoked if the Web service  failed. It accepts the error object as a parameter.
function FailedCallback(error) {

    // Display the error
    $('#UserMessage').innerHTML = "Service Error: " + error.get_message();
    $('#UserMessage').show();

}


//Set analysis status image
function SetAnalysisStatus() {
    
    $('.analysisStatus').each(function () {

        var dialog = GetAnalysisCommentDiv(this);
        var analysisComments = $(dialog).find('#hdnAnalysisComments').val();

        if (analysisComments != '' && analysisComments != undefined && analysisComments != null) {
            $(this).attr('src', _baseURL + 'Images/GreenStatus.png');
        }

    });

}