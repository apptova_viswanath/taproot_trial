﻿
function pageY(elem) {
    return elem.offsetParent ? (elem.offsetTop + pageY(elem.offsetParent)) : elem.offsetTop;
}

function resizeIframe() {
    var height = document.documentElement.clientHeight;
    height -= pageY(document.getElementById('frameLoadPopup')) + buffer;
    height = (height < 0) ? 0 : height;
    document.getElementById('frameLoadPopup').style.height = height + 'px';
}


function GetRCTURL(checkStatus) {
    var splittedURL = window.location.href.split('/');
    var causalFactorId = parseInt(splittedURL[splittedURL.length - 2].split('-')[1]);
    var eventId = splittedURL[splittedURL.length - 1];

    var rctURL = _baseURL + 'Investigation' + '/RCT-' + causalFactorId + '/' + eventId;
    var visualRCTURL = _baseURL + 'Investigation' + '/VisualRCT-' + causalFactorId + '/' + eventId;

    var url = (checkStatus) ? visualRCTURL : rctURL;

    return url;

}

function CheckIfRCTClassicMode() {
    var rctData = {};
    rctData.userId = parent.$('#hdnUserId').val();

    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/CheckIfRCTClassicMode';
    AjaxPost(serviceURL, rctData, eval(CheckIfRCTClassicModeSuccess), eval(FailedCallback));
}


function CheckIfRCTClassicModeSuccess(data) {

    DisplayRCTPage(data.d);

    $('#divSwitch.switch input').switchButton({

        off_label: 'NEW VIEW',
        on_label: 'CLASSIC VIEW',
        checked: data.d
      
    });

    _isLoad = false;
}

function SaveRCTClassicMode(checkStatus) {

    var rctData = {};
    rctData.userId = parent.$('#hdnUserId').val();
    rctData.isRCTClassicMode = checkStatus == 'checked' ? 1 : 0;

    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/SaveRCTMode';
    AjaxPost(serviceURL, rctData, eval(SaveRCTModeSuccess), eval(FailedCallback));
}

function SaveRCTModeSuccess(data) {
    DisplayRCTPage(data.d);
}



function IsTapRooTSupportedLanguage(langaugeCode)
{
    var isTapRooTSupportedLanguage = false;

    //var langaugeCode = window.opener._langaugeCode;//GetLanguageCode();
    //_userLanguage = langaugeCode;


    if (langaugeCode == _englishLanguage || langaugeCode == _germanLanguage || langaugeCode == _frenchLanguage || langaugeCode == _spanishLanguage || langaugeCode == _russianLanguage
       || (langaugeCode == _portugueseLanguage)) {

        
        isTapRooTSupportedLanguage = true;

    }

    return isTapRooTSupportedLanguage;
  
}

function CheckIfTapRooTSupportedLanguage()
{

    var rctData = {};
    rctData.userId = parent.$('#hdnUserId').val();

    rctData = JSON.stringify(rctData);

    //Call the service
    var serviceURL = _baseURL + 'Services/RootCause.svc/GetUserLanguage';
    AjaxPost(serviceURL, rctData, eval(GetUserLanguageSuccess), eval(FailedCallback));

}

function GetUserLanguageSuccess(data)
{

    var isTapRooTSupportedLanguage = IsTapRooTSupportedLanguage(data.d);

    if (isTapRooTSupportedLanguage) {

        $('#divSwitch').show();

        _isLoad = true;

        $('#divSwitch.switch input').switchButton({

            off_label: 'NEW VIEW',
            on_label: 'CLASSIC VIEW',
            checked: false

        });

        CheckIfRCTClassicMode();

        $('#divSwitch.switch input').live("change", function (event) {

            if (!(_isLoad)) {

                var checkStatus = $(this).attr('checked');
                SaveRCTClassicMode(checkStatus);
            }

        });

    }
    else {
        $('#divSwitch').hide();
        DisplayRCTPage(false);
    }


}

$(function () {

    CheckIfTapRooTSupportedLanguage();

})

function DisplayRCTPage(checkStatus) {


    var url = GetRCTURL(checkStatus);    
    $('#frameLoadPopup').attr('src', url);

    
}