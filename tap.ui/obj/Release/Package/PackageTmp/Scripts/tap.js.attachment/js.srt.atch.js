﻿/* Admin side attachment folder set up */

//Global Variables
var _hireLevelId = '';
var _hireLevelName = "";
var _nodeID = '';
var _nodeName = '';
var _newNodeParentID = '';
var _newNodeName = '';
var _parentNodeId = '';
var _rootNodeName = '';
var _snapCaps = 'SnapCaps';
var _adminAttachmentTreeId = 'TreeFolderList';
var _companyId = '';
var queue = [];


//Page load Events
$(document).ready(function () {

    SetPageLoad();    

    //To create the new folder
    CreateFolder();

    //To Delete the folder
    DeleteFolder();

    //Rename the foldername
    FolderRename();

});        //ends ready

function SetPageLoad() {

    //Get the company Id
    _companyId = GetCompanyId();
   
    //Get the hire level
    //Hard coded the root node id for Attachments
    _hireLevelId = 1;

    //Get the root folder name
    //Hard coded the Attachment root folder name
    _rootNodeName = 'Attachments';

    //For Populating the Attachment Folder setup tree   
    LoadAttachmentTreeList();

    //Removes the black color marker line from the Attachments tree.
    MarkerLineRemove();

}

//To create the new folder
function CreateFolder() {

    $('#btnNewTreeFolder').click(function () {

        $("#" + _adminAttachmentTreeId).jstree("create");
    });
}

//To Delete the folder
function DeleteFolder() {

    $('#btnDeleteTreeFolder').click(function () {
    //Check for selected folder to delete if not notify the user.
        if (_nodeID != '')
            DeleteAttachmentFolderItems(_nodeName);
        else {           
            var message = 'Select a folder to delete.';
            var msgtype = 'jError';
            NotificationMessage(message, msgtype, true, 3000);
        }        

    });
}



//Rename the foldername
function FolderRename() {
    //For Tree view Inline editing-on double click 
    $('.jstree a:not(:eq(0))').live('dblclick', function (index, item) {
        if ($(this).text().trim() == _snapCaps)
            return false;

        $("#TreeFolderList").jstree("rename");
    });

    //$('.jstree a:eq(0)')
}

//For displaying the treeview for Attachments Folder Set up
function AdminAttachmentFolderSetUpTree(_treeviewid, _resjson) {
   
    $("#" + _treeviewid).jstree({
        "json_data": { "data": _resjson },
        rules: { multiple: "ctrl" },
        "themes": {
            "dots": false,
            "icons": true
        },
        "ui": {
            "initially_select": ["1"]
        },
        core: {
            strings: { new_node: "New folder" }
        },
        plugins: ["themes", "json_data", "dnd", "crrm", "ui", "types"]
    });

    //To get the selected folder id
    $("#" + _adminAttachmentTreeId).bind("select_node.jstree",
        function (e, data) {
           
            $('#btnNewTreeFolder').show();
            //To get the id of the selected node id
            _nodeID = data.rslt.obj[0].id;

            //To get the id of the selected node name 
            if (data.rslt.e != undefined)             
                _nodeName = data.rslt.e.target.textContent.trim();
           
            //Logic to get the parent folder id 
            if (data.rslt.obj.parents('li:eq(0)')[0] == null || data.rslt.obj.parents('li:eq(0)')[0] == undefined)
                parentFolderId = _nodeID;
            else 
                parentFolderId = data.rslt.obj.parents('li:eq(0)')[0].id;            

            if (_nodeName == 'Attachments' || _nodeID ==1) {
                //Hide the delte button.
                $('#btnDeleteTreeFolder').hide();
            }
            else if (_nodeName != '' && _nodeName.length > 0 && _nodeName!=_snapCaps) {
                $('#btnDeleteTreeFolder').show();
            }
            if(_nodeName==_snapCaps)
                $('#btnNewTreeFolder').hide();
        });
       
    //For opening of all nodes in tree.
        $("#" + _adminAttachmentTreeId).bind("loaded.jstree", function (event, data) {

            $("#" + _adminAttachmentTreeId).jstree("open_all");

            if ($("#" + _adminAttachmentTreeId).find('li').length > 1)
                $('#btnDeleteTreeFolder').show();
            else
                $('#btnDeleteTreeFolder').hide();


            $("#" + _adminAttachmentTreeId).jstree('select_node', 'ul > li:first');
            $('#btnDeleteTreeFolder').hide();
            //Hide the root folder.
        });

    //Get new node info
        $("#" + _adminAttachmentTreeId).bind("create.jstree", function (e, data) {
           
            _newNodeName = data.rslt.name;

            //Logic to get the parent node of newly created node.
            if (data.rslt.parent != -1)
                _newNodeParentID = data.rslt.parent[0].id;            
            else {
                $("#" + _adminAttachmentTreeId).jstree('select_node', 'li:first');
                _newNodeParentID = _nodeID;
            }
           
            //Check the newly created node name and save it.
            //Hard coded "Attachments"
            if (_newNodeParentID != '' && _newNodeName != _rootNodeName && _newNodeName != _snapCaps)
                SaveFolderTreeNode(_newNodeParentID);
            
        });

    //Updating Tree view node-Attachment Folder
        $("#" + _adminAttachmentTreeId).bind("rename.jstree", function (e, data) {
           
            var newName = data.rslt.new_name;
            var oldName = data.rslt.old_name;

            if (newName != oldName && oldName.trim() != _snapCaps) {
                _hirnodeparent = -1;
                hireNodeId = data.rslt.obj[0].id;
                hireNodeTitle = data.rslt.new_name;
                _parentNodeId = parentFolderId;
                if (_hireLevelId != '') {
                    _nodeID = '';
                    editHireLevelId = _hireLevelId;
                    var EditItemData = new EditItemDetails(editHireLevelId, hireNodeId, _parentNodeId, hireNodeTitle);
                    data = JSON.stringify(EditItemData);
                    serviceurl = _baseURL + 'Services/AdminAttachments.svc/AttachmentFoldersValueUpdate';
                    AjaxPost(serviceurl, data, eval(HirNodeUpdate), eval(HirNodeUpdateFail));
                }
            }
            else
            {
                LoadAttachmentTreeList();
                $("#" + _adminAttachmentTreeId).jstree("refresh");
            }
        });

    //Drag and drop the folders
        $("#" + _adminAttachmentTreeId).bind("move_node.jstree", function (event, data) {
        _parent = data.rslt.r[0].id;
        folderId = data.rslt.o[0].id;
        folderName = data.rslt.o[0].textContent.trim();

        var draggedFolder = new DraggedFolderInfo(_parent, folderId, folderName);
        data = JSON.stringify(draggedFolder);
        serviceurl = _baseURL + 'Services/AdminAttachments.svc/MoveFoldersUpdate';
        AjaxPost(serviceurl, data, eval(DragFolderUpdateSuccess), eval(DragFolderUpdateFail));
    });

        function DragFolderUpdateSuccess(msg) {
            //if (msg.d == false) {
            if (msg.d != null && msg.d.length>0) {
                //var message = 'Folder already exists.';
                var message = msg.d;
                var msgtype = 'jError';
                NotificationMessage(message, msgtype, true, 3000);
                LoadAttachmentTreeList();
                $("#" + _adminAttachmentTreeId).jstree("refresh");
            }
            else {
                var message = 'Folder moved successfully.';
                var msgtype = 'jSuccess';
                NotificationMessage(message, msgtype, true, 1000);
                LoadAttachmentTreeList();
                $("#" + _adminAttachmentTreeId).jstree("refresh");
            }
        }
        function DragFolderUpdateFail() { }

    //Dragged Folder and Target information
        function DraggedFolderInfo(_parent, folderId, folderName) {
            
            this.parent = _parent;
            this.folderId = folderId;
            this.folderName = folderName;
            this.userId = $('#hdnUserId').val();
            this.companyId = GetCompanyId();
        }
   
    //get parameter
        function EditItemDetails(editHireLevelId, hireNodeId, _hirnodeparent, hireNodeTitle) {
        var EditItemData = {};
        EditItemData.hirLevelId = editHireLevelId;
        EditItemData.hirNode = hireNodeId;
        EditItemData.hirNodeParent = _hirnodeparent;
        EditItemData.hirNodeTitle = hireNodeTitle;
        EditItemData.parentNodeId = _parentNodeId;
        EditItemData.companyId = GetCompanyId();
        return EditItemData;
    }

    //Rename service success
    function HirNodeUpdate(result) {
        if (result.d) {
            //var message = hireNodeTitle + ' folder already exist.';
            var message = result.d;
            CallRedNotification(message);
        }
        else {
            var message = hireNodeTitle + ' renamed successfuly.';
            var msgtype = 'jSuccess';
            NotificationMessage(message, msgtype, true, 1000);

           //For deselecting the node 
           var clickedNode = $('#TreeFolderList').find(".jstree-clicked");
           $.jstree._reference(clickedNode[0]).deselect_node(clickedNode[0]);
        }
    }


    //Rename service fail
    function HirNodeUpdateFail(msg) {
        var message = 'Update Attachment Tree view node failed.'
        var msgtype = 'jError';
        NotificationMessage(message, msgtype, true, 3000);
    }

} //End of AdminAttachmentFolderSetUpTree


//display Attachment Tree view
function LoadAttachmentTreeList() {
    
    var AttachmentTreeListData = AttachmentTreeListDetail();
    var data = JSON.stringify(AttachmentTreeListData);
    serviceurl = _baseURL + 'Services/AdminAttachments.svc/BuildAttachmentJSTreeView';
    AjaxPost(serviceurl, data, eval(LoadAttachmentData), eval(AttachmentServiceFailed));
}

//parameters for Tree view-hirlevelid and comapny id
function AttachmentTreeListDetail() {
    AttachmentTreeListData = {};
    AttachmentTreeListData.hirLevelId = _hireLevelId;
    AttachmentTreeListData.companyID = GetCompanyId();
    return AttachmentTreeListData;
}

//Attachment folder setup tree success
function LoadAttachmentData(result) {
  
    if (result != null && result != "") {
        var _resjson = jQuery.parseJSON(result.d);
        AdminAttachmentFolderSetUpTree("TreeFolderList", _resjson); //DispAttachTreeNodes
    }
}

//Attachment folder setup tree fail
function AttachmentServiceFailed(msg) {
    alert('Attachment Tree view failed.');
}

function FolderItemDetails(hireNode, hireNodeParent, brief, description, active, displayOrder, _companyId) {
    var ParentFolderData = {};
    ParentFolderData.hirNode = hireNode;
    ParentFolderData.hirLevelId = _hireLevelId;
    ParentFolderData.hirNodeParent = hireNodeParent;
    ParentFolderData.hirLevelName = _hireLevelName;
    ParentFolderData.brief = brief;
    ParentFolderData.description = description;
    ParentFolderData.active = active;
    ParentFolderData.displayOrder = displayOrder;
    ParentFolderData.companyId = GetCompanyId();
    ParentFolderData.userId = $('#hdnUserId').val();
    return ParentFolderData;
}

function ItemsFolderSave(result) {
    if (result.d) {
        // var message = _nodeName + ' folder already exists.';
        var message = result.d;
        // var msgtype = 'jNotify';
        var msgtype = 'jError';
        NotificationMessage(message, msgtype, true, 3000);
        LoadAttachmentTreeList();
        $("#" + _adminAttachmentTreeId).jstree("refresh");

    }
    else {
        LoadAttachmentTreeList();
        var message = _nodeName +' folder saved successfully.';
        var msgtype = 'jSuccess';
        NotificationMessage(message, msgtype, true, 1000);
    }
}

function ItemsFolderSaveFail(msg) {
    var message = 'Save Attachment Tree view failed.';
    var msgtype = 'jError';
    NotificationMessage(message, msgtype, true, 3000);
}

//Delete Attachment Folder Items in Tree view
function DeleteAttachmentFolderItems(_nodeName) {

    queue.push(_nodeName);

  

//    if (ConfirmDelete) {
        
        if ($("#" + _nodeID).children().length > 2) {
            
            //alert('Folder can not be deleted. It has sub folders associated with it.');
            alert(_nodeName + " can not be deleted because it has folders inside it.\nPlease move or delete the sub-folders in order to delete " + _nodeName + ".");
            return false;
        }

        var data = JSON.stringify({ 'folderId': _nodeID });
        serviceurl = _baseURL + 'Services/AdminAttachments.svc/CheckIfAttachmentsFileExists';
        AjaxPost(serviceurl, data, eval(CheckIfAttachmentsFileExistsSuccess), eval(CheckIfAttachmentsFileExistsFail));
    //}
}

//Delete Attachment Folder multiple items in Tree view
function DeleteFolderTreeNode(_nodeID) {       
    var data = JSON.stringify({ 'nodeId': _nodeID, 'userId': $('#hdnUserId').val() });
    serviceurl = _baseURL + 'Services/AdminAttachments.svc/AttachmentFolderDelete';
    AjaxPost(serviceurl, data, eval(DeleteAttachTreeNodesSuccess), eval(DeleteAttachTreeNodesFail));
   
}

function CheckIfAttachmentsFileExistsSuccess(result) {
  
    if (result.d) {
        var ConfirmDelete = confirm("Attchment is already associated with " + _nodeName + ". \nWould you like to delete it?");
        if (ConfirmDelete) {
            DeleteFolderTreeNode(_nodeID);
        }
    }
    else {
        var ConfirmDelete = confirm('Are you sure want to delete ' + _nodeName + '?');
        if (ConfirmDelete) {
            DeleteFolderTreeNode(_nodeID);
        }
    }
}


function CheckIfAttachmentsFileExistsFail() {
}

function DeleteAttachTreeNodesSuccess(result) {
    if (result.d) {
        //var message = queue.shift() + ' folder deleted successfully.';
        var message = _nodeName + ' folder deleted successfully.';
        var msgtype = 'jSuccess';
        NotificationMessage(message, msgtype, true, 1000);
    }
    else {
        var message = 'Root Node can not be deleted.';
        var msgType = 'jNotify';
        NotificationMessage(message, msgType, true, 3000);
    }
    LoadAttachmentTreeList();
    _currentFolderName = '';
}

function DeleteAttachTreeNodesFail(msg) {
    var message = 'Delete Attachment Tree view failed.';
    var msgType = 'jError';
    NotificationMessage(message, msgType, true, 3000);
}

//For Saving the created folder
function SaveFolderTreeNode(_hirnodeparentid) {
    var hireNodeParent = _hirnodeparentid;
    var hireNode = -1;
  
    _hireLevelName = _newNodeName;
    _nodeName = _newNodeName;
    var brief = _newNodeName;
    var description = _hireLevelName;
    var active = 1;
    var displayorder = 1;
    
    var FolderItemData = new FolderItemDetails(hireNode, hireNodeParent, brief, description, active, displayorder, _companyId);
    data = JSON.stringify(FolderItemData);
    serviceurl = _baseURL + 'Services/AdminAttachments.svc/SaveAttachmentFoldersValues';
    AjaxPost(serviceurl, data, eval(ItemsFolderSave), eval(ItemsFolderSaveFail));
}

//Error Notification.
function CallRedNotification(message) {   
    var msgType = 'jError';
    NotificationMessage(message, msgType, true, 3000);
}

//Removes the black color marker line from the Attachments tree.  
function MarkerLineRemove() {
    $("#jstree-marker-line").css({
        "height": "0px",
        "top": "0px",
        "background-color": "red",
        "z-index": "10",
        "font-size": "0px",
        "width": "0px"
        //#jstree-marker-line { padding:0; margin:0; line-height:0%; font-size:1px; overflow:hidden; height:1px;
        //width:100px; position:absolute; top:-30px; z-index:10000; background-repeat:no-repeat; display:none; background-color:#456c43; 
    });
}