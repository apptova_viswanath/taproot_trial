﻿var txtPassword = '';
var txtConfirmPassword = '';
var txtCompanyId = '';
var CompanyId;
var PasswordPolicySetting;
var minChars = null, maxChars = null, countUPPERCASE = null, countLOWERCASE = null, countNumber = null, countSpecial = null;
var _passwordRegularExpression = '';


$(document).ready(function () {
   
    var ForgotPasswordAttr = JSON.parse(jQuery('#ForgotPasswordJS').html());

    txtConfirmPassword = document.getElementById(ForgotPasswordAttr['txtConfirmPassword']);
    txtPassword = document.getElementById(ForgotPasswordAttr['txtPassword']);
    txtCompanyId = document.getElementById('txtCompanyIdCnange');

   
    //Password Policy
    if (txtConfirmPassword != null && txtPassword != null) {
        if (txtCompanyId != null) {
            if(txtCompanyId.value != null && txtCompanyId.value != '')
            {
                companyId = txtCompanyId.value;
            }
        }
        else
            companyId =  sessionStorage.companyId;
        if (companyId == null || companyId == undefined)
            companyId = 0;

       
        if (companyId != null && companyId != undefined) {
            var companyDetails = {};
            companyDetails.compId = companyId;
            var data = JSON.stringify(companyDetails);

            var _serviceUrl = _baseURL + 'Services/Password.svc/GetPasswordPolicyData';
            AjaxPost(_serviceUrl, data, eval(GetPasswordPolicyTypeSuccess), eval(GetPasswordPolicyTypeFail));
        }
        // txtPassword.key
        //Password Policy
        $('#'+txtPassword.id+'').keyup(function () {
            //// 
            if (sessionStorage.PasswodPolicy == 'true')
                ValidatePassWordPolicy(txtPassword.value);
            else
                $('#pswd_info').hide();


        }).focus(function () {
           if (sessionStorage.PasswodPolicy == 'true') {
                ValidatePassWordPolicy(txtPassword.value);
                $('#pswd_info').show();
           }
            else
           $('#pswd_info').hide();

        }).blur(function () {

            //ValidateConfirmPassword(txtPassword.value, txtConfirmPassword.value);
            $('#pswd_info').hide();

        });
        $('#' + txtConfirmPassword.id + '').blur(function () {
            $('#pswd_info').hide();
            ValidateConfirmPassword(txtPassword.value, txtConfirmPassword.value);
        });
        
    }
    
});

function GetPasswordPolicyTypeSuccess(result)
{
   
    if (result != null && result.d != null && TrimString(result.d).length > 0 && result.d != '') {
        sessionStorage.PasswodPolicy = true;
        var resultData = ParseToJSON(result.d);

        if (resultData != null && resultData[0] != undefined) {

            _passwordRegularExpression = '^';
            if (resultData[0].Number != null)
                _passwordRegularExpression += '(?=(.*[0-9]){' + resultData[0].Number + ',})';
            if (resultData[0].UpperCaseLetter != null)
                _passwordRegularExpression += '(?=(.*[A-Z]){' + resultData[0].UpperCaseLetter + ',})';
            if (resultData[0].LowerCaseLetter != null)
                _passwordRegularExpression += '(?=(.*[a-z]){' + resultData[0].LowerCaseLetter + ',})';
            if (resultData[0].SpecialCharacter != null)
                _passwordRegularExpression += '(?=(.*[#@!^%$*=+_]){' + resultData[0].SpecialCharacter + ',})';
            if (resultData[0].MinimumCharacters != null)
                _passwordRegularExpression += '.{' + resultData[0].MinimumCharacters;
            if (resultData[0].MaximumCharacters != null)
                _passwordRegularExpression += ',' + resultData[0].MaximumCharacters + '}';
            else
                _passwordRegularExpression += ',}';

            if (resultData[0].Number != null)
                countNumber = resultData[0].Number;
            if (resultData[0].UpperCaseLetter != null)
                countUPPERCASE = resultData[0].UpperCaseLetter
            if (resultData[0].LowerCaseLetter != null)
                countLOWERCASE = resultData[0].LowerCaseLetter;
            if (resultData[0].SpecialCharacter != null)
                countSpecial = resultData[0].SpecialCharacter;
            if (resultData[0].MinimumCharacters != null)
                minChars = resultData[0].MinimumCharacters;
            if (resultData[0].MaximumCharacters != null)
                maxChars = resultData[0].MaximumCharacters;
            $('#spnCountLowerCase').html(countLOWERCASE);
            $('#spnCountUpperrCase').html(countUPPERCASE);
            $('#spnCountNumer').html(countNumber);
            $('#spnLength').html(minChars);
            $('#spnMaxCharacterCount').html(maxChars);
            $('#spnSpecialCharacterCount').html(countSpecial);
            if (!countLOWERCASE > 0)
                $('#lowerCase').hide();

            if (!countUPPERCASE > 0)
                $('#upperCase').hide();

            if (!countNumber > 0)
                $('#number').hide();

            if (!minChars > 0)
                $('#length').hide();

            if (!maxChars > 0)
                $('#maxCharacter').hide();

            if (!countSpecial > 0)
                $('#specialCharacter').hide();
            // $('#pswd_info').show();
            
            if (countLOWERCASE <= 1)
                $('#lowerS').hide();

            if (countUPPERCASE <= 1)
                $('#upperS').hide();

            if (countNumber <= 1)
                $('#numberS').hide();

            if (minChars <= 1)
                $('#minS').hide();

            if (maxChars <= 1)
                $('#maxS').hide();

            if (countSpecial <= 1)
                $('#specialS').hide();
        }
    }
    else
    {
        sessionStorage.PasswodPolicy = false;
       // $('#pswd_info').hide();
      
    }
}
function GetPasswordPolicyTypeFail()
{

}



function ValidatePassWordPolicy(pswd) {

    if (minChars != null) {
        if (pswd.length < $('#spnLength').html()) {
            $('#length').removeClass('valid').addClass('invalid');
        } else {
            $('#length').removeClass('invalid').addClass('valid');
        }
    }
    else
        document.getElementById("length").style.display = "none";
       
    

    //validate upper case letter
    if (countUPPERCASE != null) {
        if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
            $('#upperCase').removeClass('invalid').addClass('valid');
        } else {
            $('#upperCase').removeClass('valid').addClass('invalid');
        }
    }
    else
        document.getElementById("upperCase").style.display = "none";

    //validate lower case letter
    if (countLOWERCASE != null) {
        if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
            $('#lowerCase').removeClass('invalid').addClass('valid');
        } else {
            $('#lowerCase').removeClass('valid').addClass('invalid');
        }
    }
    else
        document.getElementById("lowerCase").style.display = "none";

    //validate number
    if (countNumber != null) {
        var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
        if (pswd.match(numberPattern)) {
            $('#number').removeClass('invalid').addClass('valid');
        } else {
            $('#number').removeClass('valid').addClass('invalid');
        }
    }
    else
        document.getElementById("number").style.display = "none";

    //validate special character

    if (countSpecial != null) {
        var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
        if (pswd.match(specialCharPattern)) {
            $('#specialCharacter').removeClass('invalid').addClass('valid');
        } else {
            $('#specialCharacter').removeClass('valid').addClass('invalid');
        }
    }
    else
        document.getElementById("specialCharacter").style.display = "none";

    if (maxChars != null) {
        if (pswd.length > $('#spnMaxCharacterCount').html()) {
            $('#maxCharacter').removeClass('valid').addClass('invalid');
        } else {
            $('#maxCharacter').removeClass('invalid').addClass('valid');
        }
    }
    else
        document.getElementById("maxCharacter").style.display = "none";
}


function ValidateConfirmPassword(Password, cPassword) {

    
   // _passwordRegularExpression = '^(?=(.*[0-9]){1,})(?=(.*[A-Z]){1,})(?=(.*[a-z]){1,}).{6,}$';
    if (Password != null && Password.match(_passwordRegularExpression) != null) {

        if (Password != cPassword) {

            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            ShowHideWarningMessage(txtPassword.id, 'Password and Confirm Password must be match', true);
            ShowHideWarningMessage(txtConfirmPassword.id, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;

        } else {

            ShowHideWarningMessage(txtPassword.id, 'Please specify a Password', false);
            ShowHideWarningMessage(txtConfirmPassword.id, 'Password and Confirm Password must be match', false);
            //return false;
        }
       
    }
    else {
        if (sessionStorage.PasswodPolicy == true || sessionStorage.PasswodPolicy == "true") {
            ShowHideWarningMessage(txtPassword.id, 'Invalid password, see password policy', true);
            //$('#pswd_info').show();
            return false;
        }
    }

    return true;
}

