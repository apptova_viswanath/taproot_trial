﻿var txtPassword = 'body_Adminbody_txtPassword';
var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';

$(document).ready(function () {

//Password Policy
$('#body_Adminbody_txtPassword').keyup(function () {
    //alert('1');
    //ValidatePassWordPolicy($('#' + txtPassword).val());
    //UpdateStatus(txtPassword);
    //ValidateConfirmPwd();
    ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val(), true, false);


}).focus(function () {

   

    //if ($('#' + txtPassword).val() == '') {
    //    ShowHideWarningMessage(txtPassword, 'Please specify a Password', true);
    //    $('#divPwdStatus').removeClass('valid');
      
    //}
    //else {
    //    ValidatePassWordPolicy($('#' + txtPassword).val());
    //    $('#pswd_info').show();
    //}

    //ValidatePassWordPolicy($('#' + txtPassword).val());
    //UpdateStatus(txtPassword);
    //$('#pswd_info').show();
    ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val(), true, false);



}).blur(function () {

    //alert('2');
//    debugger;
    
    ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val(), true, false);
    $('#pswd_info').hide();
});


//Password Policy
$('#body_Adminbody_txtConfirmPassword').keyup(function () {
    
    //ValidatePasswords($('#body_Adminbody_txtPassword').val(), $('#body_Adminbody_txtConfirmPassword').val(), false);
    ValidateConfirmPwd();
    $('#pswd_info').hide();
    //ValidateConfirmPwd();
    //if ($(this).val() == $('#' + txtPassword).val()) {
    //    UpdateStatus(txtPassword);
    //}

});

});


function UpdateStatus(txtPassword) {

    var x = true;
    if ($('#' + txtPassword).val() != '') {


        if ($('#ulPolicyList').find('.invalid').length == 0) {
            $('#divPwdStatus').addClass('valid');
            ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', false);
        }
        else {
            $('#divPwdStatus').removeClass('valid');
            ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', true);
            x = false;
        }
    }
    else {
        $('#divPwdStatus').removeClass('valid');
        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', false);
    }

    return x;
}


///PASSWORD POLICY RULES

function ValidatePassWordPolicy(pswd) {

    
    if (pswd.length < $('#spnLength').html()) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }

    //validate upper case letter
    if (pswd.match('(?=(.*[A-Z]){' + $('#spnCountUpperrCase').html() + '})')) {
        $('#upperCase').removeClass('invalid').addClass('valid');
    } else {
        $('#upperCase').removeClass('valid').addClass('invalid');
    }

    //validate lower case letter
    if (pswd.match('(?=(.*[a-z]){' + $('#spnCountLowerCase').html() + '})')) {
        $('#lowerCase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowerCase').removeClass('valid').addClass('invalid');
    }

    //validate number
    var numberPattern = '(?=(.*[0-9]){' + $('#spnCountNumer').html() + '})';
    if (pswd.match(numberPattern)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }


    //validate special character
    var specialCharPattern = '[~!@#$%^&*()_+{}:\"<>?]{' + $('#spnSpecialCharacterCount').html() + '}';
    if (pswd.match(specialCharPattern)) {
        $('#specialCharacter').removeClass('invalid').addClass('valid');
    } else {
        $('#specialCharacter').removeClass('valid').addClass('invalid');
    }


    if (pswd.length > $('#spnMaxCharacterCount').html()) {
        $('#maxCharacter').removeClass('valid').addClass('invalid');
    } else {
        $('#maxCharacter').removeClass('invalid').addClass('valid');
    }

    $('#pswd_info').show();
}



function ValidateConfirmPwd() {

    if ($('#' + txtConfirmPassword).val() != '') {

        if ($('#' + txtPassword).val() != $('#' + txtConfirmPassword).val()) {

            $('#divConfirmPwdStatus').removeClass('valid');


            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            if ($('#' + txtPassword).next().attr('class') != 'warningImage') {
                ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
                $('#divPwdStatus').removeClass('valid');
            }
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;

        }
        else {
            $('#divConfirmPwdStatus').addClass('valid');
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        }
    }
}



function ValidatePasswords(Password, cPassword, chkPolicy, isButtonEvent) {
    //debugger;
    var txtPassword = 'body_Adminbody_txtPassword';
    var txtConfirmPassword = 'body_Adminbody_txtConfirmPassword';
    
    var isPwdNull = (Password == '');
    var isCPwdNull = (cPassword == '');

    var isPasswordMatch = true;
    var isPasswordPolicyValidate = true;

    $('#divPwdStatus').removeClass('valid');
    $('#divConfirmPwdStatus').removeClass('valid');

    ShowHideWarningMessage(txtPassword, 'Please specify a Password', isPwdNull);
    ShowHideWarningMessage(txtConfirmPassword, 'Please specify Confirm Password', isCPwdNull);
    //debugger;
      
    if (chkPolicy && !isPwdNull)
    {
        ValidatePassWordPolicy($('#' + txtPassword).val());
        
        if(!isButtonEvent){
            $('#pswd_info').show();
        }
        else {
            $('#pswd_info').hide();
        }
        isPasswordPolicyValidate = UpdateStatus(txtPassword);
        if(!isPasswordPolicyValidate)
        {
           // return false;
        }
        
    }
   
    if (!isPwdNull && !isCPwdNull && Password != cPassword) {

            $('#divPwdStatus').removeClass('valid');
            $('#divConfirmPwdStatus').removeClass('valid');

            _message = 'Password and Confirm password are not matching. Please re-enter';
            _messageType = 'jError';

            //debugger;
            if ($('#' + txtPassword).next().attr('class') != 'warningImage')
            {
                ShowHideWarningMessage(txtPassword, 'Password and Confirm Password must be match', true);
                $('#divPwdStatus').removeClass('valid');
            }
            
            ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', true);

            NotificationMessage(_message, _messageType, true, 3000);
            return false;

    }

    //if (!isPwdNull && !isCPwdNull && Password == cPassword) {

        
    //    ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
    //    $('#divConfirmPwdStatus').addClass('valid');
       
    //}
    //else{


    //    ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
    //    $('#divConfirmPwdStatus').addClass('valid');

    //    UpdateStatus(txtPassword);

    //}
   // debugger;
    if (isPasswordMatch && isPasswordPolicyValidate && !isPwdNull && !isCPwdNull) {

        ShowHideWarningMessage(txtConfirmPassword, 'Password and Confirm Password must be match', false);
        ShowHideWarningMessage(txtPassword, 'Invalid password, see password policy', false);

        $('#divConfirmPwdStatus').addClass('valid');
        $('#divPwdStatus').addClass('valid');
       
    }
  
    return true;

}