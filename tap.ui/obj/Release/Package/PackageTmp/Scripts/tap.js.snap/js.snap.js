﻿
//Global variables for Shape Defaults popup

//Check for IE .
var isIE = false || !!document.documentMode;
var _selectedSeason = 0;
var _DefaultShape = new Object();
var _DefaultShapeStyle = new Object();

_DefaultShape.Event = '';
_DefaultShape.Condition = '';
_DefaultShape.Incident = '';
_DefaultShape.Text = '';
_DefaultShapeStyle.FontSize = '';
_DefaultShapeStyle.Bold = '';
_DefaultShapeStyle.Underline = '';
_DefaultShapeStyle.Italic = '';
_DefaultShapeStyle.FontColor = '';
_DefaultShapeStyle.BackgroundColor = '';
_DefaultShapeStyle.FontFamily = '';
_DefaultShapeStyle.FontStyle = '';
_DefaultShapeStyle.isToSave = false;

var _previewShape = '';
var _currentSelection = '';
var _colorPickerDefaultShape = false;
var _fontColorDefaultShape = false;
var _fontBGColorDefaultShape = false;

//Combination for Bold, Italic and Underline 
var _combFontStyleBIU = 7;
var _combFontStyleBI = 3;
var _combFontStyleIU = 6;
var _combFontStyleBU = 5;
var _combFontStyleB = 1;
var _combFontStyleI = 2;
var _combFontStyleU = 4;
//End Shape Defaults popup

//Commented by deepthi on 11th july 2014
//$(document).ready(function () {

//Bind click event with shape in Shape Defaults pop up
// ClickDefaultShape();
//});

//Global variables
_snapchartId = '';
_causalfactorName = '';

var _timeInterval;

var SPRING_SEASON = 1;
var SUMMER_SEASON = 2;
var AUTUMNG_SEASON = 3;
var WINTER_SEASON = 4;

_causalfactorsList = new Array;
_causalfactorsRemoveList = new Array;

_causalfactorsIdList = new Array;
_causalfactorsRemoveIdList = new Array;

_causalfactorsIdListUndo = new Array;
_causalfactorsRemoveIdListUndo = new Array;

_causalfactorsRemoveListUndo = new Array;
_causalfactorsListUndo = new Array;

_user = '';
_editor = '';

//For default shapes.
_shapes = {};

_defaultShapeActions = {
    BackgroundColor: "BackgroundColor",
    FontColor: "FontColor",
    FontSize: "FontSize",
    FontFamily: "FontFamily",
    Bold: "Bold",
    Italic: "Italic",
    Underline: "Underline",
    FontStyle: "FontStyle",
    None: "None"
};

//Initialize the _defaultShapeAction with None value.
_defaultShapeAction = _defaultShapeActions.None;
_isFontSize = '';
_isFontFamily = '';
_isFontStyle = false;

//Track the causal factor for UNDO
_cfCreated = 0;


function SnapchartAjaxSave(xmlgragh, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate, currentGraph) {
    newSeasonId = seasonId;
    SnapChartActiveSeasonSave(eventId, newSeasonId, xmlgragh, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate, currentGraph);
}

function SnapChartActiveSeasonSave(eventId, newSeasonId, xmlgragh, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate, currentGraph) {
    graph = currentGraph;
    currentMode = mode;
    isBackgroundPageUpdate = isParentUpdate;
    SaveStatusDisplay('Saving....');
    var snapchartInfo = new snapchartDetails(eventId, newSeasonId, xmlgragh, _causalfactorsList, mode, isComplete, _causalfactorsIdList, isRevision);
    var data = JSON.stringify(snapchartInfo);
    serviceurl = _baseURL + 'Services/SnapCharT.svc/SnapChartMostSeasonSave';
    AjaxPost(serviceurl, data, eval(SnapChartActiveSeasonSaveSuccess), eval(SnapChartActiveSeasonSaveFail));
}

function snapchartDetails(eventId, newSeasonId, xmlgragh, _causalfactorsList, mode, isComplete, _causalfactorsIdList, isRevision) {

    this.eventId = eventId;
    this.seasonId = newSeasonId;
    this.snapChartXmlGragh = xmlgragh;
    this.causalfactors = _causalfactorsList;
    this.mode = mode;
    this.isComplete = isComplete; //!(cbComplete.style.visibility = "hidden") ? cbComplete.checked : isComplete;
    this.causalfactorsId = _causalfactorsIdList;
    this.isRevision = isRevision;
    this.userId = userId;
}

function SnapChartActiveSeasonSaveSuccess(result) {

    //Display the save changes in save status bar.
    SaveStatusDisplay('Changes saved.');

    //Make negative the graph modifications.
    if (_editor != null)
        _editor.modified = false;

    if (result.d == 'Causal factor deleted' || result.d == 'Causal factor and RCT deleted') {

        _causalfactorsRemoveIdList = [];
        _causalfactorsRemoveList = [];
    }
    
    //Display the auto save information, 5 seconds after saving the graph.
    setTimeout(function () {
        SaveStatusDisplay(AutoSaveInfoSet(JSON.parse(readCookie('autosaveOption'))));
    }, 5000);

    //Update the season status.
    if (result.d == 'SeasonUpdated') {

        //seasonId == AUTUMNG_SEASON ? seasonId++ : seasonId;
        SeasonDisplay(graph);
    }

    if (result.d == 'SeasonRevisioned') {
        if (seasonId > SPRING_SEASON) {

            seasonId--;
            SnapchartSeasonStatusShow(seasonId, SeasonNameBySeasonIdGet(seasonId));

            //disable the causalfactor if it enabled.
            if (seasonId == SUMMER_SEASON) {
                //CausalfactorDisable();
                CausalfactorBySelectionDisplay('causalfactorIcon', false);
            }
                

        }
    }

    //Partially refresh the parent page.
    if (isBackgroundPageUpdate) {
        BackgroundPagePartialRefresh();
    }


    //if (seasonId == AUTUMNG_SEASON) {
    //    DisplayOrHideComplete(true);
    //}
    //else {
    //    DisplayOrHideComplete(false);
    //}
    if (seasonId == WINTER_SEASON)
        $('#lnkWinter', opener.document).html('Open Winter SnapCharT®');
}


function SnapChartActiveSeasonSaveFail() {
    alert('Failed to save the changes. Please contact support@taproot.com ');
}

//Mark the selected cell(s) as causalfactor.
function CausalfactorSelectedCellMark(graph, mxUtils, editor) {
    
    var currentCell = graph.getSelectionCell();
    if (currentCell.value != '') {

        CausalfactorsTrack(graph, 'mark');

        //Make causal factor style.    
        CausalfactorCellStyleMake(graph);

        //Save the causal factor.
        SnapchartAjaxSave(mxUtils.getXml(editor.getGraphXml()), _causalfactorsIdList, _causalfactorsList, 'Save', false, false, true);

        _causalfactorsList = [];
        _causalfactorsIdList = [];
    }
    else {
        var warningMessage = 'Warning : Please enter a description for this shape before marking it as a Causal Factor.';
        //CausalfactorDisable();
        CausalfactorBySelectionDisplay('causalfactorIcon', false);
        alert(warningMessage);
        //Make the shape editable.
        graph.cellEditor.startEditing(currentCell);

    }
}

//Remove the selected cell(s) from causalfactor.
function CausalfactorSelectedCellRemove(graph, mxUtils, editor, cellToRemove) {
    
    thisGraph = graph;

    CausalfactorsTrack(graph, 'remove');

    //Check the Root cause tree exists for the causal factor or not.
    CausalFactorWithRootCauseTreeCheck(_causalfactorsRemoveIdList, thisGraph, editor, cellToRemove);    
}

//Check the Root cause tree exists for the causal factor or not.
function CausalFactorWithRootCauseTreeCheck(_causalfactorsRemoveIdList, thisGraph, thisEditor, cellToRemove) {
    currentGraph = thisGraph;
    editor = thisEditor;
    currentCellToRemove = cellToRemove;

    var causalfactorInfo = new CausalFactorCheckDetails(eventId, seasonId, _causalfactorsRemoveIdList);
    var data = JSON.stringify(causalfactorInfo);
    serviceurl = _baseURL + 'Services/SnapCharT.svc/CausalFactorWithRootCauseTreeCheck';
    AjaxPost(serviceurl, data, eval(CausalFactorWithRootCauseTreeCheckSuccess), eval(CausalFactorWithRootCauseTreeCheckFail));
}

//Details for the Causal factor to check for the Root cause tree.
function CausalFactorCheckDetails(eventId, seasonId, _causalfactorsIdList) {

    this.eventID = eventId;
    this.seasonId = seasonId;
    this.causalfactorsId = _causalfactorsIdList;
}

function CausalFactorWithRootCauseTreeCheckSuccess(result) {

    if (result.d == 'RootCauseTreeExists') {
        var message = 'Warning: This Causal Factor has an associated Root Cause Tree® that will be deleted. Are you sure you want to remove both this Causal Factor and the Root Cause Tree®?';

        if (!Confirmation(message))
            return;
    }
    
    //Replace the causal factor style with the previous style.
    CausalfactorCellStyleRevert(currentGraph, currentCellToRemove);

    //Pass the graph object to revert the style of CF.
    SnapchartAjaxSave(mxUtils.getXml(editor.getGraphXml()), _causalfactorsRemoveIdList, _causalfactorsRemoveList, 'Delete', false, false, true, currentGraph);
}

function CausalFactorWithRootCauseTreeCheckFail() {

}

function SelectedShapeChange(graph, shape) {    
    
    //Get the selected cell
    var selectedCell = graph.getSelectionCells();
    var selectedCellId = selectedCell[0].getId();
    var fromStyle = selectedCell[0].style;
    var toStyle = '';

    ////Do not allow CF shape to change circle shape
    //if (shape == 'tocircle' && selectedCell[0].getChildCount() == 1)
    //    return false;

    //Safeguard the change to Event and Condition icons
    if (!(fromStyle.match('rectangle;') || fromStyle.match(/rounded=1;?/))) { return;}

    //Set the geometry for the selected shape.
    SelectedCellGeometryChange(selectedCell, shape);

    //Change the selected cell to respective shapes
    if (shape == 'torectangle') {

        toStyle = ChangeToRectangle(fromStyle, selectedCell, shape);        

        //Get default shape style.
        toStyle = DefaultShapeApply('rectangle', graph, toStyle);
    }

    if (shape == 'tocurvedrectangle') {

        toStyle = ChangeToOval(fromStyle, selectedCell, shape);        

        //Get default shape style.
        toStyle = DefaultShapeApply('oval', graph, toStyle);
    }

    //Removes the INCIDENT option from Change Shape menu
    //if (shape == 'tocircle') {

    //    toStyle = ChangeToCircle(fromStyle, selectedCell, shape);

    //    //Get default shape style.
    //    toStyle = DefaultShapeApply('circle', graph, toStyle);
    //}
    //Set the cell style.
    SelectedCellStyleSet(graph, toStyle);

    //Change the Connecting line(s) of the shape after changing the shape.
    if (selectedCell[0].edges != null && selectedCell[0].edges.length > 0) {

        for (var i = 0; i <= selectedCell[0].edges.length - 1; i++) {
            SnapchartConnectionChange(selectedCell[0].edges[i]);
        }
    }

    graph.refresh();

}

function ChangeToRectangle(fromStyle, selectedCell, shape) {

    var changedShape = 'rectangle;';//For rectangle shape

    if (fromStyle != null) {
        changedShape = fromStyle.match(/ellipse;?/) ? fromStyle.replace(/ellipse(;)?/, changedShape) :
                     fromStyle.match(/rounded=1;?/) ? fromStyle.replace(/rounded=1(;)?/, changedShape) : fromStyle;
    }
    return changedShape;
}

function ChangeToOval(fromStyle, selectedCell, shape) {

    var changedShape = 'rounded=1;';

    if (fromStyle != null) {
        changedShape = fromStyle.match(/ellipse;?/) ? fromStyle.replace(/ellipse(;)?/, changedShape) :
                         fromStyle.match('rectangle;') ? fromStyle.replace('rectangle;', changedShape) :
                         fromStyle.match(/rounded=1;?/) ? fromStyle : changedShape + fromStyle;
    }
    return changedShape;
}

function ChangeToCircle(fromStyle, selectedCell, shape) {

    var changedShape = 'ellipse;';

    if (fromStyle != null) {
        changedShape = fromStyle.match(/rounded=1;?/) ? fromStyle.replace(/rounded=1(;)?/, changedShape) :
                         fromStyle.match('rectangle;') ? fromStyle.replace('rectangle;', changedShape) :
                         fromStyle.match(/ellipse;?/) ? fromStyle : changedShape + fromStyle;

    }
    return changedShape;
}

//Set the geometry for the selected shape.
function SelectedCellGeometryChange(selectedCell, shape) {

    var currentGeometry = selectedCell[0].getGeometry();

    if (shape == 'torectangle' || shape == 'tocurvedrectangle')
        SetGeometryWithCustomValues(currentGeometry);

    if (shape == 'tocircle') //TODO : Resize the shape based on the shape's text .
        currentGeometry.width = currentGeometry.height = currentGeometry.height.between(60, 80) ? currentGeometry.height + 20 : currentGeometry.height;

    selectedCell[0].setGeometry(currentGeometry);
}

function SetGeometryWithCustomValues(currentGeometry) {
    //Is it circle ?
    if (currentGeometry.height == currentGeometry.width) {

        if (currentGeometry.height <= 60) {
            currentGeometry.width = 2 * currentGeometry.height;
        }
        else {
            currentGeometry.height = currentGeometry.height - 20;
            currentGeometry.width = 2 * (currentGeometry.width - 20);
        }
    }
}

//Display the season name in menu bar.
function SnapchartSeasonStatusShow(currentSeasonId, seasonName) {
    
    //Display colors based on season.
    seasonColour = currentSeasonId == SPRING_SEASON ? '#EFEA72'
                                        : currentSeasonId == SUMMER_SEASON ? '#60E237'
                                        : currentSeasonId == AUTUMNG_SEASON ? '#F6B144' : '#EFEFEF';       
    
    //Design style to display the season complete status.
    displayStyle = 'width:100px;height:20px;padding:2px 4px 2px 4px;border:1px solid black;text-align:center;float:left;margin:0px 10px 0px 10px ; ';
    //displayStyle = 'width:150px;height:20px;padding:2px 4px 2px 4px;border:1px solid black;text-align:center;float:left;margin:0px 10px 0px 10px ; ';
    //displayStyle = 'width:200px;height:20px;padding:2px 4px 2px 4px;border:2px solid black;';
  
    var cursorStyle = 'cursor:pointer;';
    var spanclose = '</span>';
    //var forthElement = '<span id="forwardSpan"';
    //var backElement = '<span id="backwardSpan"';
    //var elementStyle = 'style=' + cursorStyle + '';
    
    //var forwardStyle = ' ' + forthElement + elementStyle + '>' + '>>' + spanclose;
    //var backwardStyle = backElement + elementStyle + '>' + '<<' + spanclose + ' ';

    
    var floatStyle = 'float:left';
    var leftDirectionimageUrl = _baseURL + 'Images/safeguards/left.png';
    var rightDirectionimageUrl = _baseURL + 'Images/safeguards/right.png';
    backwardStyle = '<image id="backwardDirection" title ="Backward Season" style ="' + cursorStyle + floatStyle + '" src ="' + leftDirectionimageUrl + '" ></image>';
    forwardStyle = '<image id="forwardDirection" title ="Forward Season" style ="' + cursorStyle + floatStyle + '"  src ="' + rightDirectionimageUrl + '" ></image>';

    currentSeasonId == SPRING_SEASON ? backwardStyle = ''
                         : currentSeasonId == AUTUMNG_SEASON ? forwardStyle = '' : currentSeasonId;

    if (currentSeasonId == WINTER_SEASON) {        
        backwardStyle = '';
        forwardStyle = '';
    }

    //var spanSnapchartSeason = '<span style="float:left">SnapCharT® Season :</span>';

    seasonStatus =  backwardStyle;
    //seasonStatus = 'SnapCharT® Season : ' + backwardStyle;
    seasonStatus += '<span id="divStatus" style="' + displayStyle + 'background:' + seasonColour + '" >' + seasonName + spanclose;
    seasonStatus += forwardStyle;

    editui.seasonContainer.innerHTML = seasonStatus;

    $("#forwardDirection").click(function () {
        changeSeason("Forward", _editor);
    });

    $("#backwardDirection").click(function () {
        changeSeason("Backward", _editor);
    });

    //$("#forwardSpan").click(function () {
    //    changeSeason("Forward", _editor);
    //});

    //$("#backwardSpan").click(function () {
    //    changeSeason("Backward", _editor);
    //});

    //Change the title of the causal factor based on the season.
    CausalFactorTitleSet(currentSeasonId);

    //if (seasonId == AUTUMNG_SEASON) {
    //    DisplayOrHideComplete(true);
    //}
    //else {
    //    DisplayOrHideComplete(false);
    //}
}

function changeSeason(status, editor) {

    //Save the graph and update/revision the season.
    var xmlGraph = mxUtils.getXml(editor.getGraphXml());
    var mode = 'Save';
    var isComplete = status == "Forward" ? true : false;
    var isRevision = status == "Backward" ? true : false;
    var isParentUpdate = true;

    SnapchartAjaxSave(xmlGraph, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate, editor.graph);
}

//Undo the shapes style.
function ShapesGeometryUndoReset(cand, graph) {
   
    if (cand.length > 0) {
        //Revert the shapes geometry.
        //TODO : Use regular expression to optimize this OR condition.
        if (!(cand[0].style != undefined && cand[0].style.match(/ellipse;?/) || cand[0].style != undefined && cand[0].style.match(/shape=mxgraph.flowchart.off-page_reference;?/))
            && (cand[0].geometry != undefined && cand[0].geometry.width == cand[0].geometry.height)) {

            cand[0].geometry.height = cand[0].geometry.height - 20;
            cand[0].geometry.width = 2 * (cand[0].geometry.width - 20);
        }
        else { //Revert the circle geometry.
            if ((cand[0].style != undefined && cand[0].style.match(/ellipse;?/)) && (cand[0].geometry != undefined && cand[0].geometry.width != cand[0].geometry.height))
                cand[0].geometry.width = cand[0].geometry.height = cand[0].geometry.height.between(60, 80) ? cand[0].geometry.height + 20 : cand[0].geometry.height;

        }

        ////Revert the circle geometry.
        ////Is it just dragged/selected circle from sidebar ?
        //if (cand[0].style != 'ellipse' && cand[0].geometry.width == cand[0].geometry.height) {
        //    SetGeometryWithCustomValues(cand[0].getGeometry());
        //} //Revert rectangle and oval geometry. //Is it just dragged/selected rectangle (or) oval from sidebar ?
        //else if (cand[0].style != 'rounded=1' && cand[0].style != null && (cand[0].style == 'ellipse;' || cand[0].style == 'ellipse') && cand[0].geometry.width != cand[0].geometry.height) {
        //    cand[0].geometry.width = cand[0].geometry.height;
        //}

        //UNDO the shape's corresponding connecting lines.
        if (cand[0].edges != null && cand[0].edges.length >= 1) {
            for (var index = 0; index < cand[0].edges.length; index++) {

                SnapchartConnectionChange(cand[0].edges[index]);
            }
        }

        graph.refresh();
    }
}

//Dynamically change the season name and season id.
function SeasonDisplay(graph) {

    if (seasonId < _AutumnSeason)
        seasonId = seasonId + 1;

    seasonName = SeasonNameBySeasonIdGet(seasonId);

    //seasonName = seasonName == 'Spring' ? 'Summer' : seasonName == 'Summer' ? 'Autumn' : 'Autumn (Completed456)';
    SnapchartSeasonStatusShow(seasonId, seasonName);
}

//Get the season name by using season id.
function SeasonNameBySeasonIdGet(currentSeasonId) {

    return currentSeasonId == SPRING_SEASON ? 'Spring' :
            currentSeasonId == SUMMER_SEASON ? 'Summer' :
            currentSeasonId == AUTUMNG_SEASON ? 'Autumn' : 'Winter';

}

//Undo the cuasal factors.
function CausalFactorStyleUndo(previousCell, graph, xmlgraph) {

    //DONE: Removing the causal factor from database.
    //TODO: Implement saving the causal factor in database.   

    //Remove the causal factor from database.
    SnapchartAjaxSave(xmlgraph, _causalfactorsRemoveIdListUndo, _causalfactorsRemoveListUndo, 'Delete', false, false, true, graph);

    _causalfactorsRemoveIdListUndo.pop();
    _causalfactorsRemoveListUndo.pop();
    return "causalFactorUndo";
    
    //if ((previousCell.previous != null && previousCell.previous.id == _causalfactorsRemoveIdListUndo[_causalfactorsRemoveIdListUndo.length - 1])
    //    && (previousCell.child != undefined && previousCell.child.getChildCount() == 0)) {        
    //    //Remove the causal factor from database.
    //    SnapchartAjaxSave(xmlgraph, _causalfactorsRemoveIdListUndo, _causalfactorsRemoveListUndo, 'Delete', false, false, true, graph);

    //    _causalfactorsRemoveIdListUndo.pop();
    //    _causalfactorsRemoveListUndo.pop();
    //    return "causalFactorUndo";
    //}    
    //return '';
    //////------------------------------------------------------------------------------------------------------------------------------------/////
}

//Track the causal factor information.
function CausalfactorsTrack(graph, option) {

    var cellId = graph.getSelectionCell().id;
    var cellValue = graph.getSelectionCell().value;

    if (!graph.getSelectionCell().edge) {

        if (option == 'mark') {

            _causalfactorsIdList.push(cellId);
            _causalfactorsList.push(cellValue);

            _causalfactorsRemoveIdListUndo.push(cellId);
            _causalfactorsRemoveListUndo.push(cellValue);
        }
        else {

            _causalfactorsRemoveIdList.push(cellId);
            _causalfactorsRemoveList.push(cellValue);

            _causalfactorsIdListUndo.push(cellId);
            _causalfactorsListUndo.push(cellValue);
        }
    }
}

//Remove the causalfactor cell.
function CausalfactorCellStyleRevert(graph, currentCellToRemove) {

    graph.model.remove(currentCellToRemove);
    graph.refresh();
}

//Create causal factor shape.
function CausalfactorCellStyleMake(graph) {

    ////Append the causal factor style to the existing style.
    //style = "shape=label;image=" + _baseURL + "Images/safeguards/causal-factor.png;"
    //    + "spacing=2;imageAlign=right;imageVerticalAlign=top;shadow=1;strokeColor=#F4A83D;strokeWidth=2";
    ////style = "shape=label;image=" + location.protocol + "//" + location.hostname + "/TFSTaprootWEB/Images/safeguards/CausalFactor.gif;"
    ////        + "spacing=2;spacingTop=20;imageAlign=right;imageVerticalAlign=top;shadow=1;strokeColor=#F4A83D;strokeWidth=2";

    ////Is null coalescing ?
    //style = style + ';' + NullCoelscingCheck(graph.getSelectionCell().style, 'rectangle;');

    ////Change the style of the selected shape.
    ////SelectedCellStyleSet(graph, style);
    
    var selectedCell = graph.getSelectionCells();
    if (selectedCell != null) {

        //Selected cell.
        var parentCell = selectedCell[0];

        //style for the causal factor to insert into child node. (non selectable, editable)
        var cfImageStyle = "shape=image;image=" + _baseURL + "Images/safeguards/causal-factor.png;" + "spacingLeft=18;spacingTop=2;selectable=0;editable=0";

        //Track the causal factor to UNDO
        _cfCreated = 1;

        //causalfactor child cell.
        var cfCell = graph.insertVertex(parentCell, null, '', 1, 0, 20, 20, cfImageStyle, true);

        //Restrict the causal factor from connecting and selection.
        cfCell.setConnectable(!graph.connectionHandler.isEnabled());

        
    }
}

//Add sub items for Snapchart menu.
function SnapchartMenuItemsAdd(graph, thismenu, menu, parent) {

    //Display the save option.
    //thismenu.addMenuItems(menu, ['save'], parent);

    //In some webkit browsers, Save option is disabled.Implementing alternative solution for that.
    thismenu.addMenuItems(menu, ['webkitSave'], parent);

    //Display Auto Save sub menu in Snapchart menu.    
    thismenu.addSubmenu('snapchartAutosave', menu, parent);

    menu.addSeparator();

    //Display the page set up dialog.
    thismenu.addMenuItems(menu, ['pageSetup'], parent);

    //Capture mode.
    thismenu.addMenuItem(menu, 'capture', parent);



    ////Test option
    //menu.addItem(mxResources.get('test'), null, mxUtils.bind(this, function () {

    //    // Get the image.
    //    var img = document.createElement('image');
    //    var imageSource = CAPTURE_IMAGE_PATH + '/convertedGraph.jpg';
    //    img.setAttribute("src", imageSource);

    //    //Set the height and width of the dialog.
    //    if (img != null && img.width != 0 && img.height != 0) {            
    //        thismenu.editorUi.showDialog(new CaptureDialog(thismenu.editorUi, imageSource).container, img.width, img.height, true, true);            
    //    }
    //    img = null;

    //}), parent);

    //Make as icons to use .
    //thismenu.addMenuItems(menu, ['fitWindow'], parent);
    //thismenu.addSubmenu('linewidth', menu, parent);
    //thismenu.addMenuItem(menu, 'style', parent);

    menu.addSeparator();

    //Display Change Shape and Preferences sub menus in Snapchart menu.
    // Single selected cell and is not an edge.
    if (!graph.isSelectionEmpty() && graph.getSelectionCount() == 1 && !graph.getSelectionCell().edge) {
        //Display Change Shape sub menu in Snapchart menu.
        //Restrict the safeguards,off-page reference and label control from changing the shapes.
        if (graph.getSelectionCell().style == null || (graph.getSelectionCell().style != null && !graph.getSelectionCell().style.match(/shape=image;?/)
            && !graph.getSelectionCell().style.match(/shape=mxgraph.flowchart.off-page_reference;?/))) {
            //Restrict the label control from changing the shape.
            if (!graph.getSelectionCell().style.match(/text;?/)) {
                thismenu.addSubmenu('changeshape', menu, parent);
            }

        }

        ////Date and time   //Inprogress
        //thismenu.addMenuItems(menu, ['datetime']);
    }
    thismenu.addSubmenu('preferences', menu, parent);

    menu.addSeparator();
}
//Set the selected cell style.
function SelectedCellStyleSet(graph, style) {
    graph.setCellStyle(style, '');
}

//Make landscapeCheckBox checked as default.
function LandscapeCheckBoxDefaultChecked(landscapeCheckBox) {
    landscapeCheckBox.setAttribute('checked', 'checked');
}

//Auto align the cells after connecting.
function AutoAlignCellsAfterConnect(edge, thisedge) {

    cells = CurrentObjectForAlignmentModify(edge);
    align = AlignmentCellsByGeometry(edge, thisedge.graph);

    if (align != '') {

        //When connecting rectangle, rectangle should not move.
        if (edge.target.style.match(/rectangle;?/)) {
            align = RectangleAlignmentChange(align);
        }

        thisedge.graph.alignCells(align, cells, null);
    }
    return align;
}

//Get align position from the cells geometry.
function AlignmentCellsByGeometry(edge, graph) {

    //Geometrical x and y co-ordinates for source and target cells.
    ys = edge.source.geometry.y;
    yt = edge.target.geometry.y;
    //xs = edge.source.geometry.x;
    //xt = edge.target.geometry.x;
    
    //Create an object,  edgePoint with essential properties.
    var edgePoint = EdgePointCreate(edge);

    //Select the edge
    graph.setSelectionCell(edge);

    var quad = ys < yt ? 'down' : 'up';
    var aliPos = AlignPositionGet(edgePoint, quad);

    if (aliPos == '') {
        EdgeGeometryModify(graph, edge, edgePoint, quad);
    }

    return aliPos;
}

//Change the alignment based on the rectangle shape.
function RectangleAlignmentChange(align) {
    if (align == 'top' || align == 'bottom')
        align = align == 'top' ? 'bottom' : 'top';
    if (align == 'left' || align == 'right')
        align = align == 'left' ? 'right' : 'left';
    return align;
}

//Align the shape after moving .
function AlignShapeOnMove(aliPos, edge, cellsGeoInfo, quad, graph) {
    /* Optimize with the help of functions, 
    AlignmentValueGet(align, target, source) 
    AlignmentDifferenceSet(align, target, value) */

    //Geometrical x and y co-ordinates for source and target cells.
    var Targeo = edge.target.getGeometry();
    var Sougeo = edge.source.getGeometry();

    var ys = Sougeo.y;
    var yt = Targeo.y;
    var xs = Sougeo.x;
    var xt = Targeo.x;

    //Select the attribute to calculate the target co-ordinates .
    var attr = aliPos == 'top' || aliPos == 'bottom' ? 'height' : aliPos == 'left' || aliPos == 'right' ? 'width' : '';

    if (attr != '') {

        var add = 'append';
        var substract = 'deduct';

        //Align the cells based on cell's width.
        if (attr == 'width') {

            var srcX = xs + cellsGeoInfo.mpSouW;
            var trgX = xs + cellsGeoInfo.mpTarW; // Assume/Make target's x co-ordinates same as source's x co-ordinates.

            var operator = trgX > srcX ? substract : add;
            var mpDiff = trgX > srcX ? trgX - srcX : srcX - trgX;

            //Here used xs instead of xt , assuming that xs and xt's x co-ordinates are same.
            Targeo.x = DifferenceValue(xs, mpDiff, operator);
        }

        //Align the cells based on cell's height. 
        if (attr == 'height') {

            var srcY = ys + cellsGeoInfo.mpSouH;

            // Assume/Make target's y co-ordinates same as source's y co-ordinates.
            var trgY = ys + cellsGeoInfo.mpTarH;

            var operator = trgY > srcY ? substract : add;
            var mpDiff = trgY > srcY ? trgY - srcY : srcY - trgY;

            //Here used ys instead of yt , assuming that ys and yt's y co-ordinates are same.
            Targeo.y = DifferenceValue(ys, mpDiff, operator);
        }

        graph.refresh();

    }
}

//Create the array, cells by current informative object, edge for allignment of cells after connect.
function CurrentObjectForAlignmentModify(edge) {
    var cells = new Array;
    cells[0] = edge.source;
    cells[1] = edge.target;
    cells[2] = edge;
    return cells;
}

//Snapchart Auto save based on user selected Auto save option.
function SnapchartAutoSaveDuration(graph, mxUtils, editor, autosaveOption) {
    
    // Store the auto save option with user id in a cookie. 
    var cookieValue = null;
    var cookieValueSplit = JSON.parse(readCookie('autosaveOption'));
    if (cookieValueSplit != null) {
        cookieValue = cookieValueSplit.split('|')[0];
    }
    (cookieValue === null || cookieValue != autosaveOption) ? createCookie('autosaveOption', JSON.stringify(autosaveOption + '|' + _user), 5000) : cookieValue;    
    
    //var cookieValue = readCookie('autosaveOption');
    //(cookieValue === null || cookieValue != autosaveOption) ? createCookie('autosaveOption', autosaveOption, 5000) : cookieValue;

    //Save the autosave option in database, UserSettings table.
    SnapchartAutoSaveDurationSave(editor, mxUtils, autosaveOption);
}

function SnapchartAutosaveSet(editor, mxUtils, autosaveDuration) {
    
    duration = AutoSaveSwitch(autosaveDuration);

    if (duration == 0)
        clearInterval(_timeInterval);
    else {
        _timeInterval = setInterval(function () {
            SnapchartAjaxSave(mxUtils.getXml(editor.getGraphXml()), new Array, new Array, 'Save', false, false, false);
        }, duration);
    }

    //Display the save status  in the save status bar.
    SaveStatusDisplay(AutoSaveInfoSet(autosaveDuration));
}

//Calculate time period based on the auto save option.
function AutoSaveSwitch(autosaveOption) {
    var autosaveDuration;
    switch (autosaveOption) {
        case "every1":
            autosaveDuration = 60000;
            break;
        case "every5":
            autosaveDuration = 5 * 60000;
            break;
        case "every10":
            autosaveDuration = 10 * 60000;
            break;
        case "every20":
            autosaveDuration = 20 * 60000;
            break;

        default: autosaveDuration = 0;
    }

    return autosaveDuration;
}

//added for displaying modified menu for preferences
function SnapchartMenuItemsAddPreferences(graph, thismenu, menu, parent) {

    thismenu.addMenuItems(menu, ['defaultShapes'], parent);
    //Display Auto Save sub menu in Snapchart menu.    
    thismenu.addSubmenu('snapchartAutosave', menu, parent);
    //Display the page set up dialog.
    thismenu.addMenuItems(menu, ['pageSetup'], parent);

    //thismenu.addMenuItem(menu, 'style', parent);

    //Remove the Change shape option from the Settings icon .
    ////Display Change Shape and Preferences sub menus in Snapchart menu.
    //// Single selected cell and is not an edge.
    //if (!graph.isSelectionEmpty() && graph.getSelectionCount() == 1 && !graph.getSelectionCell().edge) {
    //    //Display Change Shape sub menu in Snapchart menu.
    //    //Restrict the safeguards,off-page reference and label control from changing the shapes.
    //    if (graph.getSelectionCell().style == null || (graph.getSelectionCell().style != null && !graph.getSelectionCell().style.match(/shape=image;?/)
    //        && !graph.getSelectionCell().style.match(/shape=mxgraph.flowchart.off-page_reference;?/))) {
    //        //Restrict the label control from changing the shape.
    //        if (!graph.getSelectionCell().style.match(/text;?/)) {
    //            thismenu.addSubmenu('changeshape', menu, parent);
    //        }

    //    }
        
    //    ////Date and time   //Inprogress
    //    //thismenu.addMenuItems(menu, ['datetime']);
    //    //thismenu.addMenuItem(menu, 'style', parent);
    //}    
}
//Display all Snapchart related menus.
function SnapchartMenusPut(graph, thismenu) {

    //Main Menu with Options
    thismenu.put('preferences', new Menu(mxUtils.bind(this, function (menu, parent) {

        //Add sub items for Snapchart menu.        
        //SnapchartMenuItemsAdd(graph, thismenu, menu, parent);


        //add sub menus under preferences
        SnapchartMenuItemsAddPreferences(graph, thismenu, menu, parent);
    })));

    //Main Menu with Arrange
    thismenu.put('arrange', new Menu(mxUtils.bind(this, function (menu, parent) {

        //Add sub items for Arrange menu.  

        thismenu.addMenuItems(menu, ['toFront', 'toBack', '-'], parent);
        thismenu.addSubmenu('direction', menu, parent);
        thismenu.addSubmenu('layout', menu, parent);
        thismenu.addSubmenu('align', menu, parent);
        menu.addSeparator(parent);
        thismenu.addSubmenu('layers', menu, parent);
        thismenu.addSubmenu('navigation', menu, parent);
        thismenu.addMenuItems(menu, ['-', 'group', 'ungroup', 'removeFromGroup', '-', 'lockUnlock', '-', 'autosize'], parent);
    })));

    //Change the selected shape to the required shape.
    thismenu.put('changeshape', new Menu(mxUtils.bind(this, function (menu, parent) {
        //To rectangle
        menu.addItem(mxResources.get('torectangle'), null, mxUtils.bind(this, function () {
            SelectedShapeChange(graph, 'torectangle');
        }), parent);

        //To rounded rectangle
        menu.addItem(mxResources.get('tocurvedrectangle'), null, mxUtils.bind(this, function () {
            SelectedShapeChange(graph, 'tocurvedrectangle');
        }), parent);

        //Removes the INCIDENT option from the Change shape menu .
        ////Is the shape is of causal factor ?  //Restrict Circle from making as causal factors.
        //if (graph.getSelectionCell().style == null || (graph.getSelectionCell().style != null && !graph.getSelectionCell().style.match(/shape=label;image=?/))) {

        //    //To circle
        //    menu.addItem(mxResources.get('tocircle'), null, mxUtils.bind(this, function () {
        //        SelectedShapeChange(graph, 'tocircle');
        //    }), parent);
        //}
    })));

    //Auto save of Snapchart.
    thismenu.put('snapchartAutosave', new Menu(mxUtils.bind(this, function (menu, parent) {

        //var cookieValue = readCookie('autosaveOption');
        var cookieValues = JSON.parse(readCookie('autosaveOption'));
        
        if (cookieValues != null && cookieValues.split('|')[0] != 'off')
            menu.addItem(mxResources.get('autosaveOff'), null, mxUtils.bind(this, function () {
                //Display if snapchart auto save is on.
                SnapchartAutoSaveDuration(graph, mxUtils, thismenu.editorUi.editor, 'off');
            }), parent);

        thismenu.addSubmenu('autosaveOn', menu, parent);

    })));

    //Auto save on with different durations.
    thismenu.put('autosaveOn', new Menu(mxUtils.bind(this, function (menu, parent) {
        thismenu.addMenuItems(menu, ['every1'], parent);
        thismenu.addMenuItems(menu, ['every5'], parent);
        thismenu.addMenuItems(menu, ['every10'], parent);
        thismenu.addMenuItems(menu, ['every20'], parent);
    })));

    //Preferences for the shapes.
    //thismenu.put('preferences', new Menu(mxUtils.bind(this, function (menu, parent) {

    //    //Default action for the selected shapes.
    //    //thismenu.addSubmenu('', menu, parent);
    //    thismenu.addMenuItems(menu, ['defaultShapes'], parent);
    //    //Display Auto Save sub menu in Snapchart menu.    
    //    thismenu.addSubmenu('snapchartAutosave', menu, parent);
    //    //Display the page set up dialog.
    //    thismenu.addMenuItems(menu, ['pageSetup'], parent);

    //})));

    //thismenu.put('defaultShapes', new Menu(mxUtils.bind(this, function (menu, parent) {


    // commented code for new default shape popup
    //Default shape color.
    //menu.addItem(mxResources.get('defaultColor'), null, mxUtils.bind(this, function () {
    //    _defaultShapeAction = _defaultShapeActions.BackgroundColor;
    //    thismenu.pickColor(mxConstants.STYLE_FILLCOLOR);
    //}), parent);

    //Default shape font color.        
    //menu.addItem(mxResources.get('defaultFontColor'), null, mxUtils.bind(this, function () {
    //    _defaultShapeAction = _defaultShapeActions.FontColor;
    //    thismenu.pickColor(mxConstants.STYLE_FONTCOLOR);
    //}), parent);

    //Default shape font size.        
    //thismenu.addSubmenu('fontSize', menu, parent, true);
    //thismenu.addSubmenu('fontSize', menu, parent);

    //Default shape font family.        
    //thismenu.addSubmenu('fontFamily', menu, parent, true);

    //Default font, Bold.        
    //thismenu.addMenuItem(menu, 'bold', parent);

    //Default font, Italic. 
    //thismenu.addMenuItem(menu, 'italic', parent);

    //Default font, Underline. 
    //thismenu.addMenuItem(menu, 'underline', parent);

    // })));

}

//Actions for the snapchart related menus.

//Actions for the snapchart related menus.
function SnapchartMenusAddAction(thisAction, graph, mxUtils, editor) {

    //thisAction.addAction('datetime', function () {
    //    //Display Date and time on the shape. //In progress
    //    var selectedCell = graph.getSelectionCells();
    //    graph.insertVertex(selectedCell[0], null, 'DateTime here', 0.5, -0.2, 0, 0, null, true);

    //}, null, null, '');

    //Manually save the graph without using the library Save function.
    thisAction.addAction('webkitSave', function () {

        var mode = 'Save';
        var isComplete = false;
        var isRevision = false;
        var isParentUpdate = false;
        var xml = mxUtils.getXml(editor.getGraphXml());

        ////Clear the causal factor array.
        //_causalfactorsIdList = [];
        //_causalfactorsList = [];

        SnapchartAjaxSave(xml, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate);

    }, null, null, '');

    thisAction.addAction('every1', function () {
        SnapchartAutoSaveDuration(graph, mxUtils, editor, 'every1');
    }, null, null, '');

    thisAction.addAction('every5', function () {
        SnapchartAutoSaveDuration(graph, mxUtils, editor, 'every5');
    }, null, null, '');

    thisAction.addAction('every10', function () {
        SnapchartAutoSaveDuration(graph, mxUtils, editor, 'every10');
    }, null, null, '');

    thisAction.addAction('every20', function () {
        SnapchartAutoSaveDuration(graph, mxUtils, editor, 'every20');
    }, null, null, '');

    thisAction.addAction('capture', function () {

        ImageSave(graph, mxUtils, editor);

    }, null, null, '');

    thisAction.addAction('defaultShapes', function () {

        //Open Shape Defaults popup        
        OpenShapeDefaultsPopUp();
        //After Click shape defaults menu
        //Added by deepthi on 11th july 2014
        ClickDefaultShape();
        _DefaultShapeStyle.isToSave = false;
    }, null, null, '');

}

//Save the user settings for snapchart auto save in database.
function SnapchartAutoSaveDurationSave(editor, mxUtils, autosaveOption) {
    //Get user id.
    //_user = parseInt($('#hdnUserId').val());
    duration = autosaveOption == 'off' ? 0 : autosaveOption.replace(autosaveOption.substring(0, 5), '');
    var snapchartAutoSaveInfo = new SnapchartAutoSaveDetails(_user, duration);
    var data = JSON.stringify(snapchartAutoSaveInfo);
    serviceURL = _baseURL + 'Services/UserSettings.svc/SnapChartAutoSaveDurationSet';
    AjaxPost(serviceURL, data, eval(SnapchartAutoSaveDurationSaveSuccess), eval(SnapchartAutoSaveDurationSaveFail));

    function SnapchartAutoSaveDurationSaveSuccess(result) {

        //Call the snapchart auto save based on user selection.
        SnapchartAutosaveSet(editor, mxUtils, autosaveOption);
    }

    function SnapchartAutoSaveDurationSaveFail(result) {

    }

    function SnapchartAutoSaveDetails(userId, duration) {
        this.userId = userId;
        this.autosaveDuration = duration;
    }
}

//Get the last Snapchart AutoSave Duration from database
function SnapchartAutosaveDurationGet(editor, mxUtils) {
    
    //Get user id.
    _user = parseInt($('#hdnUserId').val());
    serviceURL = _baseURL + 'Services/UserSettings.svc/SnapChartAutoSaveDurationGet';
    var data = JSON.stringify({ userId: _user });
    AjaxPost(serviceURL, data, eval(SnapchartAutosaveDurationGetSuccess), eval(SnapchartAutosaveDurationGetFail));

    function SnapchartAutosaveDurationGetSuccess(result) {
        var snapchartAutosave = 'every5'; //Default autosave duration.
        if (result.d != -1) {
            snapchartAutosave = result.d == 0 ? 'off' : 'every' + result.d;
        }
        SnapchartAutosaveSet(editor, mxUtils, snapchartAutosave);

        //var snapchartAutosave;
        ////Call the snapchart auto save based on user id in database, UserSettings table.
        //if (result.d != 0) {
        //    snapchartAutosave = 'every' + result.d;
        //    SnapchartAutosaveSet(editor, mxUtils, snapchartAutosave);
        //}
        //else
        //    snapchartAutosave = 'off';

        //Set the cookie        
        createCookie('autosaveOption', JSON.stringify(snapchartAutosave + '|' + _user), 5000);
        //createCookie('autosaveOption', snapchartAutosave, 5000);
    }

    function SnapchartAutosaveDurationGetFail(result) {

    }
}

//Double click to render the selected shape in the side bar.
function DoubleClickHandler(thisSide, elt, ds) {
    var graph = thisSide.editorUi.editor.graph;
    var md = 'dblclick';
    //var md = (mxClient.IS_TOUCH) ? 'touchstart' : 'dblclick';
    var first = null;
    mxEvent.addListener(elt, md, function (evt) {

        first = new mxPoint(mxEvent.getClientX(evt), mxEvent.getClientY(evt));

        if (!mxEvent.isPopupTrigger(evt) && this.currentGraph == null && first != null) {
            var tol = graph.tolerance;

            if (Math.abs(first.x - mxEvent.getClientX(evt)) <= tol &&
				Math.abs(first.y - mxEvent.getClientY(evt)) <= tol) {
                var gs = graph.getGridSize();
                ds.drop(graph, evt, null, gs, gs);

                //Make cell editable soon after selecting a shape in sidebar.
                graph.cellEditor.startEditing(graph.getSelectionCells()[0]);
            }
        }
        first = null;

    });
}

//Render the selected shape on double click on the graph.
function DoubleClickGraphToShapeRender(graph, evt, dragSource) {
    
    var point = PointXYGet(graph, evt);
    dragSource.drop(graph, evt, null, point.x, point.y);
    graph.startEditing(graph.getSelectionCells()[0]);
}

//Create the Cookie .
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

//Read the cookie .
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

//Display the save status.
function SaveStatusDisplay(value) {
    editui.saveStatusContainer.innerHTML = value;
}

//Set the value based on auto save information.
function AutoSaveInfoSet(autosaveDuration) {
    
    autoSave = 'Auto save: ';
    every = 'every ';
    value = autosaveDuration != 'off' ? AutosaveDurationGet(autosaveDuration) : autosaveDuration;
    //value = autosaveDuration != 'off' ? autosaveDuration.replace(autosaveDuration.substring(0, 5), '') : autosaveDuration;
    return value == 'off' ? autoSave + value : value == 1 ? autoSave + every + value + ' minute' : autoSave + every + value + ' minutes';
}

//Get the exact value to display in Save status bar.
function AutosaveDurationGet(duration) {
    
    if (duration.match(/off/) == null)
        duration = duration.replace(duration.substring(0, 5), '');

    var durationList = duration.split('|'); //duration.split('|')[0]
    if (durationList.length > 1) {        
        return durationList[0];
    }
    return duration;
}

//NOT IN USE
function GetHeight(area) {
    var d = 8 * area;
    var h = Math.sqrt(d) / 4;
    h = Math.ceil(h);
    return h;
    // remember, w = 2h;      
}

//Auto re-size the shape.
function ShapeResize(geo, cell) {

    //Connector shape resize. //Temporarily not resizing.
    if (cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/)) {

        //if (ShapeHeightGet(geo) > 60) {

        //    CircleWithMoreWidthAdjust(geo);
        //    return;
        //}

        geo.width = geo.height = 60;
        return;
    }

    if (cell.style != null)
        cell.style.match(/ellipse;?/) ? CircleAutoResize(geo) : SquareRectangleAutoResize(geo, cell);
}

//Adjust the height and width to make a circle.
function CircleWithMoreWidthAdjust(geo) {

    var height = ShapeHeightGet(geo);
    var fixedCircleValue = 80;
    var initialValue = 55;
    var jsB = 55, jsC = 60, jsD = 75, jsE = 90, jsF = 105, jsG = 150;

    var appendValue = height.between(jsB, jsC) ? 25 : height.between(jsC, jsD) ? 45 :
                       height.between(jsD, jsE) ? 50 : height.between(jsE, jsF) ? 70 :
                        height.between(jsF, jsG) ? 95 : 115;

    geo.width = geo.height = height >= initialValue ? height + appendValue : fixedCircleValue;
    //geo.width = geo.height = height > 80 ? (2 * height) + 20 : 80;
}

//Calculate the height of the shape.
function ShapeHeightGet(geo) {

    return (Math.ceil(Math.sqrt((geo.height * geo.width) / 2)));
}

//Adjust the height and width to make a square, rounded rectangle.
function SquareRectangleMoreWidthAdjust(geo, cell) {
    
    var shapeHeight = ShapeHeightGet(geo);
    
    //Fix: Text touches the border of the shape .
    var padHeight = cell.style.match(/rounded=1;?/) ? 10 : 5;
    return shapeHeight > 60 ? shapeHeight + padHeight : 60;

    //return shapeHeight > 60 ? shapeHeight + 3 : 60;
}

//Adjust the size for the circle.
function CircleAutoResize(geo) {

    geo.width > 80 ? CircleWithMoreWidthAdjust(geo) : geo.height = geo.width = 80;
}

//Adjust the size for square and rounded rectangle .
function SquareRectangleAutoResize(geo, cell) {

    //Check for the safeguard image size.
    if (cell.style.match(/shape=image;?/) || cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/))
        return;
    
    geo.height = geo.width > 120 ? SquareRectangleMoreWidthAdjust(geo, cell) : 60;
    geo.width = 2 * geo.height;
    
    //Fix: for touch/overflow the text of the shape's border .
    //geo.height = geo.height + 14;
    
}

//Auto re-size the shape after changing the font size.
function ShapeAutoResize(graph) {
    var cell = graph.getSelectionCells();
    for (var i = 0; i < cell.length; i++) {
        graph.cellSizeUpdated(cell[i]);
    }
}

//Modified for the label control in the sidebar to set the text editor.
function LabelTextEditorModify(cellStyle) {
    width = 30;
    height = 40;
    if (cellStyle != null && cellStyle.match(/text;?/)) {
        width = cellStyle.match(/text;?/) ? 200 : 30;
        height = cellStyle.match(/text;?/) ? 30 : 40;
    }
    //width = cellStyle.match(/text;?/) ? 200 : 30;
    //height = cellStyle.match(/text;?/) ? 30 : 40;
    return new mxRectangle(0, 0, width, height);
    //return new mxRectangle(0, 0, 200, 30);
    // return new mxRectangle(0, 0, 30, 40);
}

//Check for the label control and dynamically change the size of the cell.
function LabelControlSizeChange(graph, cell) {

    if (cell.style.match(/text;?/)) {
        cell.geometry.width = 200;//120
        cell.geometry.height = 30;//60

        //if (cell.style == 'text;align=center;whiteSpace=wrap;') {
        //    cell.style = cell.style + 'html=1';
        //    SelectedCellStyleSet(graph, cell.style);
        //}
    }
}

//Update the causal factor data.
function CausalFactorValueUpdate(cell) {
    
    if (cell.getChildCount() > 0 && cell.children[0].style.match(/shape=image;image=/)) {

        var causalfactorInfo = new causalfactorDetails(eventId, seasonId, cell.id, cell.value);
        var data = JSON.stringify(causalfactorInfo);
        serviceurl = _baseURL + 'Services/SnapCharT.svc/CausalFactorUpdate';
        AjaxPost(serviceurl, data, eval(CausalFactorValueUpdateSuccess), eval(CausalFactorValueUpdateFail));
    }

    function causalfactorDetails(eventId, seasonId, causalfactorId, causalfactorName) {
        this.eventId = eventId;
        this.seasonId = seasonId;
        this.causalfactorId = causalfactorId;
        this.causalfactorName = causalfactorName;
    }

    function CausalFactorValueUpdateSuccess() { BackgroundPagePartialRefresh(); }
    function CausalFactorValueUpdateFail() { }

}

//Creation of fit window icon and its functionality.
function FitWindowInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'FitWindowICon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/fit-hover.gif');
    //elem.setAttribute("src", _baseURL + 'Images/safeguards/fit.gif');
    elem.setAttribute("alt", "Fit Window");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Fit Window');
    //elem.setAttribute("onmouseover", 'FitWindowHoverChange(this)');

    elem.setEnabled = function (value) {
        elem.enabled = value;
    };
    elem.setEnabled(true);

    var action = editui.toolbar.editorUi.actions.get('fitWindow');

    if (action != null) {
        editui.toolbar.addClickHandler(elem, action.funct);
    }

    return elem;
    //thisTool.container.appendChild(elem);
}

//creation of seperator in toolbar
function SeperatorInToolbarCreate() {
    var elt = document.createElement('div');
    elt.className = 'geSeparator';
    elt.setAttribute('id', 'Separator');
    return elt;
}

//Creation of Settings button in toolbar
function SettingsInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'settingsIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/settings-black.png');    
    elem.setAttribute("alt", "Settings of SnapCharT®");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Settings');    

    //elem.setAttribute('onclick', 'SettingsSnapChart(editui.editor);'); // for FF
    //elem.onclick = function () { SettingsSnapChart(editui.editor); }; // for IE

    return elem;

}

//Creation of Save button in toolbar
function SaveInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'saveIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/save-hover.png');
    //elem.setAttribute("src", _baseURL + 'Images/safeguards/save.png');
    elem.setAttribute("alt", "Save SnapCharT®");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Save SnapCharT®');
    //elem.setAttribute("onmouseover", 'SaveHoverChange(this)');

    elem.setAttribute('onclick', 'SaveSnapChart(editui.editor);'); // for FF
    elem.onclick = function () { SaveSnapChart(editui.editor); }; // for IE

    return elem;

}

//creation of capture button in toolbar
function CaptureInToolbarCreate() {

    var elem = document.createElement("img");
    elem.setAttribute('id', 'CaptureIcon');
    elem.setAttribute('href', 'javascript:void(0);');
    elem.setAttribute("src", _baseURL + 'Images/safeguards/snapcap-hover.png');
    //elem.setAttribute("src", _baseURL + 'Images/safeguards/snapcap.png');
    elem.setAttribute("alt", "Image Capture");
    elem.className = 'geButton';
    elem.setAttribute('title', 'Image Capture');
    //elem.setAttribute("onmouseover", 'SnapcapHoverChange(this)');

    elem.setAttribute('onclick', 'TakeSnapCap(editui.editor.graph, mxUtils, editui.editor);'); // for FF
    elem.onclick = function () { TakeSnapCap(editui.editor.graph, mxUtils, editui.editor); }; // for IE
    return elem;


}

//Not in use
//Changes the snapcap icon on hover.
function SnapcapHoverChange(element) {
    element.setAttribute("src", _baseURL + 'Images/safeguards/snapcap-hover.png');   
}

//Not in use
//Changes the Fit window icon on hover.
function FitWindowHoverChange(element) {

    element.setAttribute("src", _baseURL + 'Images/safeguards/fit-hover.gif');
    //styleSet = 'border:1px solid gray; border-radius:2px;opacity:1;_filter:none !important;';
    //element.setAttribute("style", styleSet);
}

//Not in use
//Changes the Save icon on hover.
function SaveHoverChange(element) {

    element.setAttribute("src", _baseURL + 'Images/safeguards/save-hover.png');
}


//Changes the status bar style in menubar.
function StatusBarStyleChange(container) {

    container.setAttribute("style", "float:right");
}

//Create html for checkbox and label.
function DataContainer(name) {
    var container = document.createElement('div');
    container.appendChild(CheckboxCreate(name));
    container.appendChild(LabelCreate(name));

    return container.innerHTML;
}

//Create checkbox.
function CheckboxCreate(name) {

    var checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.id = 'cb' + name;
    //if (name != 'Complete') {
    checkbox.disabled = true;
    // }
    checkbox.label = LabelCreate(name);

    if (name == 'Assumption' || name == 'CausalFactor') {
        checkbox.setAttribute("onClick", "CheckboxToggle(this)");
    }
    //if (name == 'Complete') {
    //    checkbox.setAttribute("onChange", "CheckboxToggle(this)");
    //}

    return checkbox;
}

//Toggling the Checkbox, Check/Uncheck. 
function CheckboxToggle(obj) {

    if (obj != null) {

        name = obj.id.substring(2, obj.id.length);
        name == 'Assumption' ? CellAssumptionMake() : CellCausalfactorMake();

        //if (name == 'Assumption') {
        //    CellAssumptionMake();
        //}
        //else if (name == 'CausalFactor') {
        //    CellCausalfactorMake();
        //}
        //else {
        //    CompleteSeason();
        //}
    }
}

//function CompleteSeason() {

//    //Save the graph and update/revision the season.
//    isComplete = cbComplete.checked;
//    SnapchartAjaxSave(mxUtils.getXml(_editor.getGraphXml()), _causalfactorsIdList, _causalfactorsList, 'Save', isComplete, false, true, _editor.graph);
//}

//Make the cell as Assumption.
function CellAssumptionMake() {
    editui.editor.graph.toggleCellStyles(mxConstants.STYLE_DASHED);
    AssumptionsFlag();
}

//Make the cell as Causalfactor.
function CellCausalfactorMake() {
    
    if (!(seasonId == _AutumnSeason)) { return }    
    CausalfactorsFlag();

    var cells = editui.editor.graph.getSelectionCells();
    if (cells != null) {

        var cell = cells[0];
        
        if (cell.getChildCount() > 0) {

            if (cell.children[0].style.match(/shape=image;image=/)) {
                CausalfactorSelectedCellRemove(editui.editor.graph, mxUtils, editui.editor, cell.children[0]);                
                return;
            }
            
            if (cell.children[1] != undefined && cell.children[1].style.match(/shape=image;image=/)) {
                CausalfactorSelectedCellRemove(editui.editor.graph, mxUtils, editui.editor, cell.children[1]);
                return;
            }

            CausalfactorSelectedCellMark(editui.editor.graph, mxUtils, editui.editor);
            return;
        }

        CausalfactorSelectedCellMark(editui.editor.graph, mxUtils, editui.editor);

        

        //(cell.style != null && cell.style.match(/image=?/)) ? CausalfactorSelectedCellRemove(editui.editor.graph, mxUtils, editui.editor)
        //                                                        : CausalfactorSelectedCellMark(editui.editor.graph, mxUtils, editui.editor);
        //(cell.getChildCount() == 1) ? CausalfactorSelectedCellRemove(editui.editor.graph, mxUtils, editui.editor)
        //                                                : CausalfactorSelectedCellMark(editui.editor.graph, mxUtils, editui.editor);

    }
}

//Create label.
function LabelCreate(name) {
    var label = document.createElement('label');
    label.id = 'label' + name;
    label.htmlFor = "cb" + name;
    label.setAttribute("style", "color:gray");

    if (name == 'CausalFactor')
        name = 'Causal Factor';

    label.appendChild(document.createTextNode(name));

    return label;
}

//Add custom listener for the mouse click.
function SnapchartGraphClick() {

    //Commented some pieces of code because of removing Causalfactor and Assumption checkboxes from the menubar .
    //ToDo : Make the click event generalize for Assumption and Causal factor.
    editui.editor.graph.addListener(mxEvent.CLICK, function (sender, evt) {
        var cell = evt.getProperty('cell');
       
        //Enable and disable the date icon.
        DateIconBySelectionDisplay('dateIcon', false);        
        AssumptionIconBySelectionDisplay('assumptionIcon', false);
        CausalfactorBySelectionDisplay('causalfactorIcon', false);
        ToolbarIconsByShapeSelectionDisplay('eventIcon', false);
        ToolbarIconsByShapeSelectionDisplay('conditionIcon', false);

        ToolbarIconsByShapeSelectionDisplay('cutIcon', false);
        ToolbarIconsByShapeSelectionDisplay('copyIcon', false);

        //Restrict safeguards,off-page reference, circle shape and label control from Assumption and causal factor.
        if (cell != null && cell.style != null) {

            //Enable and disable the date icon.
            DateIconBySelectionDisplay('dateIcon', true);
            AssumptionIconBySelectionDisplay('assumptionIcon', true);
            EventConditionIconsDisplay(editui.editor.graph);
            CutCopyIconsDisplay(editui.editor.graph);
              
            AssumptionIconsToggle(false);
            if (cell.style.match(/dashed=1/)) {
                AssumptionIconsToggle(true);
            }

            CausalfactorIconsToggle(false);
            if (cell.getChildCount() > 0) {

                if (cell.children[0].style.match(/shape=image;image=/)) {
                    CausalfactorIconsToggle(true);
                }
            }
            
            if (cell.style.match(/shape=image;?/) || cell.style.match(/text;?/) || cell.style.match(/shape=mxgraph.flowchart.off-page_reference;?/)
                 || cell.style.match(/ellipse;?/) != null) {
                //Disable the Assumption and Causal factor .
                //AssumptionDisable();
                //CausalfactorDisable();
                AssumptionIconBySelectionDisplay('assumptionIcon', false);
                CausalfactorBySelectionDisplay('causalfactorIcon', false);
                ToolbarIconsByShapeSelectionDisplay('eventIcon', false);
                ToolbarIconsByShapeSelectionDisplay('conditionIcon', false);
                return;
            }
        }

        ////Disable the Assumption and Causal factor.
        //AssumptionDisable();
        //CausalfactorDisable();

        if (evt.isConsumed() || !evt.isConsumed() && cell != null) {

            //Consume the event.
            if (!evt.isConsumed()) {
                evt.consume();
            }

            //Forcefully make the cell selected if not been selected.
            if (!editui.editor.graph.isCellSelected(cell)) {
                editui.editor.graph.setSelectionCell(cell);
            }
            
            if (cell != null && cell.isEdge() != 1) {
                ////Enable the Assumption.
                //Assumption.children.cbAssumption.disabled = false;
                //Assumption.children.labelAssumption.style.color = "#000000";

                //Enable the Causal factor in Autumn season.
                if (seasonId == _AutumnSeason) {
                    //CausalFactorEnable();
                    CausalfactorBySelectionDisplay('causalfactorIcon', true);
                }

                ////Checkmark the assumption if the cell has dashed boundary.                        
                //if (cell.style != null && cell.style.match(/dashed=1/)) {
                //    //Make Assumption checkbox checked.
                //    Assumption.children.cbAssumption.checked = true;
                //}
                
                ////TODO : Optimize - only one if condition is enough 3 if conditions not required.
                ////Checkmark the causal factor if the cell has marked as causal factor.
                //if (cell.getChildCount() > 0) {

                //    //Checkmark the causal factor based on the style NOT on child count.
                //    if (cell.children[0].style.match(/shape=image;image=/)) {
                //        CausalFactor.children.cbCausalFactor.checked = true;
                //    }
                //    if (cell.children[1] != undefined && cell.children[1].style.match(/shape=image;image=/)) {
                //        CausalFactor.children.cbCausalFactor.checked = true;
                //    }
                //}

                //Checkmark the causal factor if the cell has marked as causal factor.
                //if (cell.style != null && cell.style.match(/image=?/)) {
                //    CausalFactor.children.cbCausalFactor.checked = true;
                //}

                //Disable Assumption and Causal factor for multiple selection of shapes.
                if (editui.editor.graph.getSelectionCount() > 1) {
                    //AssumptionDisable();
                    //CausalfactorDisable();                    
                    DateIconBySelectionDisplay('dateIcon', false);//Enable and disable the date icon.
                    AssumptionIconBySelectionDisplay('assumptionIcon', true);
                    
                }
            }
        }
    });
}

//Override/Replace the library functions.
//Customize the menubar.
function MenubarCustomize() {    

    //Display the snapchart menu, Options in the menubar.
    $(".geItem").hide();

    //Add Snapchart menu items.
    SnapchartMenusPut(editui.editor.graph, editui.menus);

    //Display Snapchart [Options] menu.
    SnapchartOptionsMenuDisplay();
    
    $(".geMenuContainer, .geStatus").show();

    //LVC
    //Fix: Firefox menu containers are disappearing.
    $("div").removeClass("geMenubar");    
}

//Display the menu, SnapChart[Options] 
function SnapchartOptionsMenuDisplay() {

    var menus = ['preferences'];
    //var menus = ['preferences', 'arrange'];
    for (var i = 0; i < menus.length; i++) {
        editui.menubar.addMenu(mxResources.get(menus[i]), editui.menus.get(menus[i]).funct);
    }
}

////Make Causal factor container in the Menubar.
//function AssumptionContainerMake() {
//    //LVC
//    //Fix: Firefox menu containers are disappearing.
//    var AssumptionBar = DataContainerMake();
//    AssumptionBar.setAttribute("style", "float:right;");
//    editui.assumptionContainer = AssumptionBar;    
//    //editui.assumptionContainer = DataContainerMake();
//    editui.menubar.container.appendChild(editui.assumptionContainer);
//}

////Make Causal factor container in the Menubar.
//function CausalFactorContainerMake() {
//    //LVC
//    //Fix: Firefox menu containers are disappearing.
//    var CausalFactorBar = DataContainerMake();
//    CausalFactorBar.setAttribute("style", "float:right;");
//    editui.causalFactorContainer = CausalFactorBar;
//    //editui.causalFactorContainer = DataContainerMake();
//    editui.menubar.container.appendChild(editui.causalFactorContainer);
//}

////Make Causal factor container in the Menubar.
//function CompleteContainerMake() {
//    editui.completeContainer = DataContainerMake();
//    editui.menubar.container.appendChild(editui.completeContainer);
//}

//Make Save status bar container in the menubar.
function SaveStatusContainerMake() {
    var saveStatusBar = DataContainerMake();
    //LVC   
    //saveStatusBar.setAttribute("style", "color:gray; float:right; width:180px");
    //editui.saveStatusContainer = saveStatusBar;
    //editui.menubar.container.appendChild(editui.saveStatusContainer);

    //Moves the autosave status bar to lower left of the snapchart .
    saveStatusBar.setAttribute("style", "color:gray;position:absolute;bottom:0;");
    editui.saveStatusContainer = saveStatusBar;
    editui.sidebar.container.appendChild(editui.saveStatusContainer);   
}

//Make season bar container in the menubar.
function SeasonBarContainerMake() {
    var seasonBar = DataContainerMake();
    seasonBar.setAttribute("style", "color:gray; float:right;width:200px;");
    //newsc s    
    seasonBar.setAttribute("class", "geItem oldSeasonContainer");
    //newsc e
    editui.seasonContainer = seasonBar;
    editui.sidebar.container.appendChild(editui.seasonContainer);
    //editui.menubar.container.appendChild(editui.seasonContainer); 
}

// Create Data container for Assumption and Causalfactor.
function DataContainerMake() {
    var container = document.createElement('a');

    //LVC    
    container.className = 'geItem';//'geItem geMenuContainer';
    return container;
}

//Hide the library's status bar...
function StatusBarHide() {
    $('.geStatus').hide();
}

//Customize the sidebar.
function SidebarCustomize() {
    $('.geSidebar, .geTitle').hide();
}

//Display only single page in the grph.
function PageBreakLineDisplay(graph, editor, mxUtils) {

    //graph.pageVisible = !graph.pageVisible; //using the variable, graph.pageVisible for displaying the page break lines. 
    graph.pageVisible = true;
    graph.pageBreaksVisible = graph.pageVisible;
    graph.preferPageSize = graph.pageBreaksVisible;
    graph.view.validate();
    graph.sizeDidChange();
    
    editor.updateGraphComponents();
    editor.outline.update();

    if (mxUtils.hasScrollbars(graph.container)) {
        if (graph.pageVisible) {
            graph.container.scrollLeft -= 20;
            graph.container.scrollTop -= 20;
        }
        else {
            graph.container.scrollLeft += 20;
            graph.container.scrollTop += 20;
        }
    }
}

//Toggle selection in the sidebar.
function SidebarSelectionToggle() {

    $('.geSidebar .geItem').click(function () {

        $(".geSidebar").find('.sideBarShapeSelected ').each(function () {
            if (this.className == "geItem sideBarShapeSelected")
                $(this).removeClass("sideBarShapeSelected");
        });

        $(this).addClass("sideBarShapeSelected");
    });
}

//Default rectangle, on double click on the graph.
function DefaultShapeRender(graph, evt) {

    var parent = graph.getDefaultParent();
    var point = PointXYGet(graph, evt);
    var shape = 'rectangle';
    var defaultShapeRectangleStyle = 'rectangle;' + shapeStyle;
    var newCell = graph.insertVertex(parent, null, '', point.x, point.y, 120, 60, DefaultShapeApply(shape, graph, defaultShapeRectangleStyle));
    graph.setSelectionCell(newCell);
    graph.startEditingAtCell(newCell, evt);
}

//Gets X and Y co-ordinates to display the graph
function PointXYGet(graph, evt) {
    
    var offset = mxUtils.getOffset(graph.container);
    var origin = mxUtils.getScrollOrigin(graph.container);
    
    var x = mxEvent.getClientX(evt) - offset.x + origin.x;
    var y = mxEvent.getClientY(evt) - offset.y + origin.y;

    var PointSet = {};
    PointSet.x = x;
    PointSet.y = y;

    return PointSet;
}

//Change the connecting lines between particular shapes.
function SnapchartConnectionChange(edge) {    
    
    var edgeStyle = 'edgeStyle=orthogonalEdgeStyle'; //Flexible connecting line
    var squareCorner = ';rounded=0';//Square corners for the connecting lines
    var htmlValue = ';html=1;';
    ArrowConnection = '';    
    plainConnection = 'endArrow=none;';

    (edge.source.style.match(/rounded=1;?/) || edge.source.style.match(/text;?/) || edge.source.style.match(/shape=image;?/)) ? PlainlineConnect(edge)
        : RectangleCircleShapeConnection(edge);
       
    edge.style = edge.style + edgeStyle + squareCorner + htmlValue;
}

//Replace the connecting arrow with line.
function PlainlineConnect(edge) {
    edge.style = plainConnection;
}

//Replace the connecting line with an arrow.
function ArrowConnect(edge) {
    edge.style = ArrowConnection;
}

//Change the connecting line for Rectangle,Circle and off-page reference.
//Remove multiple matches and work on Single regular expression with OR condition.
function RectangleCircleShapeConnection(edge) {
    
    if (edge.source.style.match(/rectangle;?/) || edge.source.style.match(/ellipse;?/) || edge.source.style.match(/shape=mxgraph.flowchart.off-page_reference;?/)) {
        //if (edge.target.style.match(/rounded=1;?/) || edge.target.style.match(/text;?/) || edge.target.style.match(/shape=image;?/))
        //    PlainlineConnect(edge);
        (edge.target.style.match(/rounded=1;?/) || edge.target.style.match(/text;?/) || edge.target.style.match(/shape=image;?/)) ? PlainlineConnect(edge) : ArrowConnect(edge);

        if (edge.source.style.match(/shape=mxgraph.flowchart.off-page_reference;?/) && edge.target.style.match(/shape=mxgraph.flowchart.off-page_reference;?/))
            PlainlineConnect(edge);
    }
}

//Set the default shapes data
function DefaultShapesDataGet() {
    //Get user id.    
    _user = parseInt($('#hdnUserId').val());
    var data = '&userId=' + _user;
    var serviceURL = _baseURL + 'Services/SnapCharT.svc/DefaultShapesByUserIdGet';
    GetAjax(serviceURL, data, eval(DefaultShapesByUserIdGetSuccess), eval(DefaultShapesByUserIdGetFail), 'GetDefaultShapesInformation');

}

// DefaultShapesDataGet Success
function DefaultShapesByUserIdGetSuccess(result) {

    if (result != null && result.d != null && result.d != undefined) {
        var defaultShapesData = JSON.parse(result.d);

        //Store the default shape data for further use.
        if (defaultShapesData.length > 0) {
            //_shapes = {};
            for (var i = 0; i < defaultShapesData.length ; i++) {
                var type = defaultShapesData[i].shapeType;
                _shapes[type] = {};
                _shapes[type].shape = defaultShapesData[i].shapeType;
                _shapes[type].color = defaultShapesData[i].color;
                _shapes[type].fontFamily = defaultShapesData[i].fontFamily;
                _shapes[type].fontSize = defaultShapesData[i].fontSize;
                _shapes[type].fontColor = defaultShapesData[i].fontColor;
                _shapes[type].bold = defaultShapesData[i].bold;
                _shapes[type].italic = defaultShapesData[i].italic;
                _shapes[type].underline = defaultShapesData[i].underline;
                _shapes[type].fontStyle = defaultShapesData[i].fontStyle;

            }
        }

    }

}

// DefaultShapesDataGet Fail
function DefaultShapesByUserIdGetFail() {
    alert('failed');
}

// Get the selected shape type.
function CurrentShapeGet(style) {

    return style.match(/ellipse;?/) ? 'circle' :
                    style.match(/text;?/) ? 'label' :
                    style.match(/rounded=1;?/) ? 'oval' :
                    style.match(/rectangle;?/) ? 'rectangle' : '';
}

//Save the default shape data in databaseusing Ajax post call.
function DefaultShapeSave(shape, key, value) {

    currentShape = shape;
    currentKey = key;
    currentValue = value;

    //Get user id.
    _user = parseInt($('#hdnUserId').val());

    var defaultShapeData = new DefaultShapeData(_user, shape, key, value);
    var data = JSON.stringify(defaultShapeData);
    serviceurl = _baseURL + 'Services/SnapCharT.svc/DefaultShapeSave';
    AjaxPost(serviceurl, data, eval(DefaultShapeSaveSuccess), eval(DefaultShapeSaveFail));
}

// Default shape data details.
function DefaultShapeData(userId, shapeType, key, value) {
    this.userId = userId;
    this.shapeType = shapeType;
    this.key = key;
    this.value = value;
}

// DefaultShapeSave success.
function DefaultShapeSaveSuccess(result) {

    //Update the default shape action in the _shapes.    
    if (_shapes[currentShape] != null || _shapes[currentShape] == undefined) {

        //Adds the shape if doesn't exists in the _shapes.
        if (_shapes[currentShape] == undefined)
            _shapes[currentShape] = {};

        switch (currentKey) {
            case "BackgroundColor":
                _shapes[currentShape].color = currentValue;
                break;
            case "FontColor":
                _shapes[currentShape].fontColor = currentValue;
                break;
            case "FontSize":
                _shapes[currentShape].fontSize = currentValue;
                break;
            case "FontFamily":
                _shapes[currentShape].fontFamily = currentValue;
                break;
            case "Bold":
                _shapes[currentShape].bold = currentValue;
                break;
            case "Italic":
                _shapes[currentShape].italic = currentValue;
                break;
            case "Underline":
                _shapes[currentShape].underline = currentValue;
                break;
            case "FontStyle":
                _shapes[currentShape].fontStyle = currentValue;
                break;
            default:
                break;
        }
    }
    _defaultShapeAction = _defaultShapeActions.None;
}

// DefaultShapeSave Fail.
function DefaultShapeSaveFail() {

}

//Set the default background color and font color to the current shape.
function DefaultColorToShapeSet(graph, shape, select, key, value) {
    key = key + '=';
    var sel = select != null && select.length > 0 ? select + ';' : select;
    select = sel + key + value;

    return select;
}

//Set the default shape actions for the shape.
function DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, select) {
    var style = '';
    var key = "None";
    var value = "None";

    switch (defaultAction) {
        case "BackgroundColor":
            key = "fillColor";
            value = _shapes[shape].color;
            style = DefaultColorToShapeSet(graph, shape, select, key, value);
            break;
        case "FontColor":
            key = "fontColor";
            value = _shapes[shape].fontColor;
            style = DefaultColorToShapeSet(graph, shape, select, key, value);
            break;
        case "FontSize":
            key = "fontSize";
            value = _shapes[shape].fontSize;
            style = DefaultColorToShapeSet(graph, shape, select, key, value);
            break;
        case "FontFamily":
            key = "fontFamily";
            value = _shapes[shape].fontFamily;
            style = DefaultColorToShapeSet(graph, shape, select, key, value);
            break;
        case "FontStyle":
            key = "fontStyle";
            value = _shapes[shape].fontStyle;
            style = DefaultColorToShapeSet(graph, shape, select, key, value);
            break;
        default:
            break;
    }

    return style;
}

//Set the default style to the current shape.
function DefaultShapeApply(shape, graph, style) {
    if (shape != '') {
        var defaultAction = "None";
        var defaultShapeStyle = style;

        if (_shapes != null && _shapes[shape] != null) {
            if (_shapes[shape].color != null) {
                defaultAction = _defaultShapeActions.BackgroundColor;
                defaultShapeStyle = DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, defaultShapeStyle);
            }
            if (_shapes[shape].fontColor != null) {

                defaultAction = _defaultShapeActions.FontColor;
                defaultShapeStyle = DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, defaultShapeStyle);
            }
            if (_shapes[shape].fontSize != null) {
                defaultAction = _defaultShapeActions.FontSize;
                defaultShapeStyle = DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, defaultShapeStyle);
            }
            if (_shapes[shape].fontFamily != null) {
                defaultAction = _defaultShapeActions.FontFamily;
                defaultShapeStyle = DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, defaultShapeStyle);
            }
            if (_shapes[shape].fontStyle != null) {
                defaultAction = _defaultShapeActions.FontStyle;
                defaultShapeStyle = DefaultShapeActionInitialSwitch(defaultAction, graph, shape, _shapes, defaultShapeStyle);
            }

        }

        return defaultShapeStyle;
    }
}

//Display snapchart graph.
function SnapchartActiveGraphDisplay(snapData, editui) {

    if (snapData != '') {
        var snapchartModel = mxUtils.parseXml(snapData);
        editui.editor.setGraphXml(snapchartModel.documentElement);

        //Check the causal factors in the spring and summer season and remove it.
        //SpringSummerCausalfactorRemove(editui);
    }

}

//Winter Snapchart display.
function WinterSnapchartDisplay() {

    if (window.opener != null && window.opener._isWinter == true) {

        seasonId = 4;

        if (_isWinterChartExists == "True") {

            //Display the Winter snapchart.            
            SnapchartActiveGraphDisplay(winterData, editui);
            editui.editor.modified = false;
        }
        else {

            var mode = 'Save';
            var isComplete = false;
            var isRevision = false;
            var isParentUpdate = false;
            var xml = mxUtils.getXml(editui.editor.getGraphXml());

            //Clear the causal factor array.
            _causalfactorsIdList = [];
            _causalfactorsList = [];

            SnapchartAjaxSave(xml, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate);
        }
    }
}

//Snapchart customization first part.
function SnapchartFirstCustomize() {

    //STRICTLY HERE
    //Override the function, EditorUi.prototype.createKeyHandler to handle the cursor/direction keys and to maintain proper connection . 
    CreateKeyHandlerOverride();

    //Override the function, Toolbar.prototype.addEnabledState to make simple the toolbar icon Selection/Hover view .
    AddEnabledStateOverride();

    //Override the native java script for numbers.
    Number.prototype.between = between;

    //Globalize the seasonName.
    seasonName = SeasonNameBySeasonIdGet(seasonId);

    //Override the function, createUndoManager for Undo the CF in db and undo the shapes style.    
    CreateUndoManagerOverride();

    //Override the function, mxCellEditor.prototype.startEditing to stop showing the scrollbar while typing in the shape.
    MxCellEditorStartEditingOverride();    

    

    //Instance for editor.
    editui = new EditorUi(new Editor());
    
    //Show the titles for the sidebar shapes .
    editui.sidebar.sidebarTitles = true; //settings
    editui.sidebar.sidebarTitleSize = 10; //settings

    //Stops disconnecting  the connecting lines from the shapes on move .
    editui.editor.graph.setDisconnectOnMove(false); //settings
    
    //Stops duplicating the shape while connecting to the other shapes .
    editui.editor.graph.connectionHandler.setCreateTarget(false); //settings

    //STRICT
    //Make Landscape page view as default.
    LandscapeDefaultMake(editui.editor, mxConstants.PAGE_FORMAT_A4_LANDSCAPE);    

    //Display snapchart graph.
    SnapchartActiveGraphDisplay(snapData, editui);

    //Fake file name to skip, displaying the dialog box for saving.
    editui.editor.filename = 'SnapCharT_1.xml';

    //Implies no changes in the graph.
    editui.editor.modified = false;
    
    
    //Disables the hover feature on the shape.
    mxConstraintHandler.prototype.enabled = false; //settings   

    //Override the function, Sidebar.prototype.createVertexTemplateFromCells to implement double click on the shape in the sidebar.
    //LVC
    //CreateVertexTemplateFromCellsOverride(); //In latest version they have moved the code.
    CreateItemOverride();
    
    //Override the function, addClickHandler
    /* 1. Restrict rendering of the shape on graph , by clicking on the shape in the sidebar.
       2. Used global variables for using in the double click functionality. */
    AddClickHandlerOverride();

    //Override the function, createDropHandler for text label control functionality.    
    CreateDropHandlerOverride();

    //Override the function, mxDragSource.prototype.mouseUp for making the cell editable soon after dragging the shape from sidebar.
    DragSourceMouseUpOverride();

    //Increase the cornor radius for the rounded-rectangle.
    mxConstants.RECTANGLE_ROUNDING_FACTOR = 0.25;

    //Override the function, mxGraphHandler.prototype.mouseMove to stop displaying the pixels while moving the shape.
    GraphHandlerMouseMoveOverride();

    //Override the function, mxVertexHandler.prototype.mouseMove to stop displaying the pixels while resizing the shape.
    VertexHandlerMouseMoveOverride();

    //Override the function, mxEdgeHandler.prototype.mouseMove to stop displaying the pixels while changing the selected edge from one target to another target .
    EdgeHandlerMouseMoveOverride();

    //Override the function, mxEdgeHandler.prototype.connect to change the independent connecting line based on the target shape .
    MxEdgeHandlerConnectOverride();
   
    //------------------- Customize the tool bar  -----------------------------------------------------//
    
    //Adds Settings Icon and Its functionality in tool bar
   // editui.toolbar.container.appendChild(SettingsInToolbarCreate())

    //Adds Fitwindow icon and its functionality in the tool bar.
    editui.toolbar.container.appendChild(FitWindowInToolbarCreate());

    //Adds separator icon in the toolbar.
    editui.toolbar.container.appendChild(SeperatorInToolbarCreate()); 

    // Set the new icons exactly in the required position.
    $('#FitWindowICon').insertAfter('a[title="Actual size"]');
    $('#Separator').insertBefore('a[title="Zoom in"]'); 

    //Adds Save Icon and Its functionality in tool bar
    editui.toolbar.container.appendChild(SaveInToolbarCreate());
    
    //Adds date icon in the toolbar.
    editui.toolbar.container.appendChild(DatetimeInToolbarCreate(editui));   

    //Adds Event icon in the toolbar.
    editui.toolbar.container.appendChild(EventInToolbarCreate());

    //Adds Condition icon in the toolbar.
    editui.toolbar.container.appendChild(ConditionInToolbarCreate());

    //Adds Assumption icon in the toolbar.
    editui.toolbar.container.appendChild(AssumptionInToolbarCreate(editui));
    
    //Adds Causalfactor icon in the toolbar.
    editui.toolbar.container.appendChild(CausalfactorInToolbarCreate(editui));

    //Adds separator icon in the toolbar.
    editui.toolbar.container.appendChild(SeperatorInToolbarCreate());

    //Adds Take Snapcaps in tool bar
    editui.toolbar.container.appendChild(CaptureInToolbarCreate());

    //Adds Font color in the tool bar
    editui.toolbar.container.appendChild(FontColorInToolbarCreate());

    //Adds cut icon in the tool bar
    editui.toolbar.container.appendChild(ToolbarIconCreate('cutIcon', 'Images/safeguards/cut.png', 'Cut Operation', 'Cut', 'geButton mxDisabled mxIconDisabled', 'CutApply(editui.editor.graph)'));
    //Adds copy icon in the tool bar
    editui.toolbar.container.appendChild(ToolbarIconCreate('copyIcon', 'Images/safeguards/copy.gif', 'Copy Operation', 'Copy', 'geButton mxDisabled mxIconDisabled', 'CopyApply(editui.editor.graph)'));
    //Adds paste icon in the tool bar
    editui.toolbar.container.appendChild(ToolbarIconCreate('pasteIcon', 'Images/safeguards/paste.png', 'Paste Operation', 'Paste', 'geButton', 'PasteApply(editui.editor.graph)'));
    //Adds print icon in the tool bar 
    editui.toolbar.container.appendChild(ToolbarIconCreate('printIcon', 'Images/safeguards/print.png', 'Print Operation', 'Print', 'geButton', 'PrintApply(editui)'));

    //Rearrange the toolbar icons
    ToolbarIconsRearrange();
   

    //------------------- Customize the tool bar ends -----------------------------------------------------//

    //Display page break lines by default.
    //LVC  //Giving issues
    //TODO : fix this issue.
    //PageBreakLineDisplay(editui.editor.graph, editui.editor, mxUtils);

    //Auto re-size the shape.
    //editui.editor.graph.setHtmlLabels(true);
    editui.editor.graph.setAutoSizeCells(true);

    //Hide the edge, on top of the shape.
    mxGraph.prototype.ordered = false;

    //STRICT
    MenubarCustomize();

    //Hide the library's status bar...
    StatusBarHide();

    //Create save status bar container.
    SaveStatusContainerMake();

    //Create Season bar container.
    SeasonBarContainerMake();

    ////Create CausalFactor and Assumption Containers.
    //CausalFactorContainerMake();
    //AssumptionContainerMake();
    ////CompleteContainerMake();

    //Short form.
    Assumption = editui.assumptionContainer;
    CausalFactor = editui.causalFactorContainer;
    //Complete = editui.completeContainer;    
    
    //editui.causalFactorContainer.innerHTML = DataContainer('CausalFactor');
    //editui.assumptionContainer.innerHTML = DataContainer('Assumption');
    ////editui.completeContainer.innerHTML = DataContainer('Complete');

    //if (seasonId == AUTUMNG_SEASON) {
    //    DisplayOrHideComplete(true);
    //}
    //else {
    //    DisplayOrHideComplete(false);
    //}    

    //Customize the sidebar.    
    SidebarCustomize();

    //Override the function, Sidebar.prototype.createThumb to remove the titles for some sidebar shapes. 
    //display names for Event, Condition and Incident.
    //LVC  --> commented
    //CreateThumbOverride();

    //For Winter Snapchart .
    WinterSnapchartDisplay();

    //Merge the menubar into the toolbar .
    ToolbarMenusMerge(editui);

    //Change the Preferences menu item as settings icon .
    PreferencesAsSettingsChange();

}

//Snapchart customization second part.
function SnapchartSecondCustomize() {    

    //Globalize the editor for season revisioning.    
    _editor = editui.editor;
    _user = parseInt($('#hdnUserId').val());
    //Graph listener for Click event.    
    SnapchartGraphClick();

    //Display the event name in the menubar
    EventNameDisplay();

    //Add the background color to the menubar
    MenubarBackgroundcolorChange();

    //Increase the font size of the left side bar section titles
    LeftsideBarTitlesSizeIncrease();

    //newsc s

    //Get the season div id and  
    //Display the current season .
    SnapchartSeasonShow(seasonId, SeasonDivIdBySeasonId(seasonId), true);

    //Initial display of the completed season with green check mark .
    GreenCheckmarkDisplay(seasonId);

    //Hides the old season bar .
    $('.oldSeasonContainer').hide();
    
    if ($("#newseasonbarContainer").parent().length > 0) {

        $("#newseasonbarContainer").parent().removeClass('geSidebar');
        $("#newseasonbarContainer").removeClass('geItem');
        $("#newseasonbarContainer").parent().css({ "border-bottom": "1px solid #e5e5e5" });
        
    }
    //newsc e

    //To display the current season name.
    SnapchartSeasonStatusShow(seasonId, SeasonNameBySeasonIdGet(seasonId));
   
    //Auto save duration with cookies.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Clear the cookie for some browsers
     //To clear the old cookie values which are not in JSON format.
    createCookie('autosaveOption', '', -1);

    //Call the snapchart auto save based on cookie value.
    var autosaveDuration = null;
    var cookieUserId = null;
    
    //var autosaveDuration = readCookie('autosaveOption');    
    var cookieValuesList = JSON.parse(readCookie('autosaveOption'));
    
    if(cookieValuesList != null){
        autosaveDuration = cookieValuesList.split('|')[0];
        cookieUserId = parseInt(cookieValuesList.split('|')[1]);
    }
    
    //Check for the auto save duration from the cookie based on user id.
    if (autosaveDuration != null && cookieUserId == _user) {
        if (autosaveDuration != 'off')
            SnapchartAutosaveSet(editui.editor, mxUtils, autosaveDuration);
       
        //Display the save status  in the save status bar.
        SaveStatusDisplay(AutoSaveInfoSet(autosaveDuration));
    }
    else {//Get from database.
        SnapchartAutosaveDurationGet(editui.editor, mxUtils);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Override the function, cellsRemoved to restrict the causal factors from deletion.
    CellsRemovedOverride();

    //Add custom actions for the Snapchart [Options].
    SnapchartMenusAddAction(editui.actions, editui.editor.graph, mxUtils, editui.editor);

    //Override the function, styleChange for auto resizing the shape after changing the font style.
    StyleChangeOverride();

    //Override the function, dblClick for rendering the selected shape on double click on the plain graph.    
    DblClickOverride();

    //Override the function, mxConnectionHandler.prototype.connect for auto allign the shapes after connecting.    
    ConnectOverride();
    
    //Override the function, mxGraph.prototype.cellSizeUpdated
    /* 1.Auto resize the shape
       2.Update the causal factor data
       3.Modify the super long word
       4.Condition for editing Date and Time label.
    */    
    CellSizeUpdatedOverride();

    //Override the function, EditorUi.prototype.save  for saving the graph in the database.
    SaveOverride();

    //Override the function, ColorDialog.prototype.createApplyFunction to save the default shape selected color.
    CreateApplyFunctionOverride();

    //Get the shapes information from the database
    DefaultShapesDataGet();

    //Override the function, Menus.prototype.addSubmenu to track default shape's font size and font color.
    AddSubmenuOverride();

    //Override the function, mxGraph.prototype.setCellStyles to save the default shape's font size and font color.
    SetCellStylesOverride();

    //Override the function, Menus.prototype.addMenuItem to track the font styles.
    AddMenuItemOverride();

    //Override the function, mxGraph.prototype.toggleCellStyleFlags to save default shape's font styles.
    ToggleCellStyleFlagsOverride();

    //Override the function, mxGraphHandler.prototype.moveCells to change the connecting lines on move of a shape.
    GraphHandlerMoveCellsOverride();

    //Overrides clipboard from copying and paste the causalfactor. 
    //CausalfactorCopyRestrict(editui);

    //Override Cell selection function
    //CellSelectedOverride();

    //Override clipboard copy and paste functionality the non causal factor
    //ShapeMenuPasteOverriade();

    //Override the function, mxGraph.prototype.canExportCell to restrict the causal factor shape from copying .
     CanExportCellOverrride();

    //Shape Label change override
    LabelChangeOverride();

    //Override the function, mxUndoManager.prototype.undo for updating the parent's page causal factor title on undo .
    UndomxUndoManagerOverride();

    //Override the function, mxGraph.prototype.resizeCells to align all the connected shapes after resize of a shape containing multiple connections.
    MxGraphResizeCellsOverride();

    //LVC
    //Override the function, mxRubberband.prototype.repaint Due to google language translate 
    //the selection starts from bottom of the arrow instead from the tip of the arrow pointer.
    RepaintOverride();

    //Override the function, Menus.prototype.createPopupMenu to remove unnecessary context menu options .
    MenusCreatePopupMenuOverride();

    //Override the function, Graph.prototype.setLinkForCell to change the shape of the new link shape .
    SetLinkForCellOverride();

    ////mxCellEditor stop editing override
    //Fix: Making the shape not to edit.
    //StopEditingOverride();
}

//click functionality for save button
function SaveSnapChart(editor) {
    var mode = 'Save';
    var isComplete = false;
    var isRevision = false;
    var isParentUpdate = false;
    var xml = mxUtils.getXml(editor.getGraphXml());

    ////Clear the causal factor array.
    //_causalfactorsIdList = [];
    //_causalfactorsList = [];

    SnapchartAjaxSave(xml, _causalfactorsIdList, _causalfactorsList, mode, isComplete, isRevision, isParentUpdate);
}

//click functionality for takesnapcap button
function TakeSnapCap(graph, mxUtils, editor) {

    ImageSave(graph, mxUtils, editor);

    ////Get all the common types.
    //GetCommonType();
}

//Default Shape Pop Up

//Open pop up to set Default Shape styles
function OpenShapeDefaultsPopUp() {

    var labelTextMarkUp = '<a id="ancTextDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px;"><svg style="width: 34px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 32px; top: 10px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="1" y="1" width="28" height="18" fill="none" stroke="white" pointer-events="stroke" visibility="hidden" stroke-width="7"></rect><rect x="1" y="1" width="28" height="18" fill="none" stroke="none" pointer-events="all"></rect></g><g style="visibility: visible;"><g transform="translate(2,6)scale(0.6956521739130435)"><foreignObject pointer-events="all" width="36" height="14"><div style="display:inline-block;font-size:16px;font-family:Verdana;color:#000000;line-height:14px;vertical-align:top;width:36px;white-space:normal;text-align:center;">Text</div></foreignObject></g></g></g><g></g></g></svg><div style="font-size: 9px; text-align: center; white-space: nowrap; padding-top: 4px;"></div></a>';
    var otherShapesMarkUp = '<div class="geSidebar" id="divShapeDefaultsShape" style="border-bottom:0px;"><a id="ancEventDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px; text-decoration:none;"><svg style="width: 90px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 14px; top: 5px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="0" y="0" width="70" height="30" fill="#ffffff" stroke="#000000" pointer-events="all"></rect></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Event</div></a><a id="ancConditionDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px; text-decoration:none;"><svg style="width: 90px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 14px; top: 5px; text-decoration:none;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="0" y="0" width="70" height="30" rx="4" ry="4" fill="#ffffff" stroke="#000000" pointer-events="all"></rect></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Condition</div></a><a id="ancIncidentDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px;text-decoration:none;"><svg style="width: 34px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 34px; top: 4px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><ellipse cx="15" cy="15" rx="14.883720930232558" ry="14.883720930232558" fill="#ffffff" stroke="#000000" pointer-events="all"></ellipse></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Incident</div></a>';

    //Change the markup for label text control for IE.
    if (isIE) {
        labelTextMarkUp = '<a id="ancTextDefaultShape" class="geItem" style="padding: 2px; width: 80px; height: 51px; overflow: hidden;" href="javascript:void(0);">' +
            '<div style="left: 15px; top: 12px; height: 34px; overflow: hidden; position: relative; cursor: pointer; -ms-touch-action: none;">' +
                '<svg xmlns="http://www.w3.org/2000/svg" style="width: 100%; height: 100%; display: block; min-height: 20px; min-width: 29px;"><g><g /><g>' +
                '<g style="visibility: visible;" transform="translate(0.5 0.5)">' +
                '<rect visibility="hidden" fill="none" pointer-events="stroke" stroke="white" stroke-width="7" x="1" y="1" width="28" height="18" />' +
                '<rect fill="none" pointer-events="all" stroke="none" x="1" y="1" width="28" height="18" /></g></g><g /></g></svg>' +

                '<div style="transform-origin: 0% 0%; left: 23px; top: 6px; text-align: center; color: rgb(0, 0, 0); line-height: 14px; ' +
                'font-family: Verdana; font-size: 18px; vertical-align: top; white-space: normal; position: absolute; transform: scale(0.695652) translate(-50%, 0%);">' +
            'Text</div></div><div style="text-align: center; padding-top: 4px; font-size: 9px; white-space: nowrap;">' +
            '</div></a>';
    }

    //Add shape inside the popup
    var shapeIcons = otherShapesMarkUp + labelTextMarkUp + '</div>';

    //var shapeIcons = '<div class="geSidebar" id="divShapeDefaultsShape" style="border-bottom:0px;"><a id="ancEventDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px; text-decoration:none;"><svg style="width: 90px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 14px; top: 5px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="0" y="0" width="70" height="30" fill="#ffffff" stroke="#000000" pointer-events="all"></rect></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Event</div></a><a id="ancConditionDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px; text-decoration:none;"><svg style="width: 90px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 14px; top: 5px; text-decoration:none;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="0" y="0" width="70" height="30" rx="4" ry="4" fill="#ffffff" stroke="#000000" pointer-events="all"></rect></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Condition</div></a><a id="ancIncidentDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px;text-decoration:none;"><svg style="width: 34px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 34px; top: 4px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><ellipse cx="15" cy="15" rx="14.883720930232558" ry="14.883720930232558" fill="#ffffff" stroke="#000000" pointer-events="all"></ellipse></g></g><g></g></g></svg><div style="font-size: 12px; text-align: center; white-space: nowrap; padding-top: 4px;">Incident</div></a>'
    //    +'<a id="ancTextDefaultShape" href="javascript:void(0);" class="geItem" style="overflow: hidden; width: 100px; height: 51px; padding: 2px;"><svg style="width: 34px; height: 34px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 32px; top: 10px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="1" y="1" width="28" height="18" fill="none" stroke="white" pointer-events="stroke" visibility="hidden" stroke-width="7"></rect><rect x="1" y="1" width="28" height="18" fill="none" stroke="none" pointer-events="all"></rect></g><g style="visibility: visible;"><g transform="translate(2,6)scale(0.6956521739130435)"><foreignObject pointer-events="all" width="36" height="14"><div style="display:inline-block;font-size:16px;font-family:Verdana;color:#000000;line-height:14px;vertical-align:top;width:36px;white-space:normal;text-align:center;">Text</div></foreignObject></g></g></g><g></g></g></svg><div style="font-size: 9px; text-align: center; white-space: nowrap; padding-top: 4px;"></div></a>'
    //    +'</div>';

    //Add font size selection
    var fontSize = '<div style=" float:left;margin-top: 5px;"><label>Font Size:</label><select style="width: 70px;" id="cmbfontSizeSelect"><option value="6">6</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="14">14</option><option value="18">18</option><option value="24">24</option><option value="36">36</option><option value="48">48</option><option value="72">72</option></select></div>';

    //Add font-family selection
    var fontFamily = '<div style="margin-top: 5px; margin-left: 5px; float: left;"><label>Font-Family:</label><select style="width: 130px;" id="fontfamilySelect"><option value="Helvetica">Helvetica</option><option value="Verdana">Verdana</option><option value="Times New Roman">Times New Roman</option><option value="Garamond">Garamond</option><option value="Comic Sans MS">Comic Sans MS</option><option value="Courier New">Courier New</option><option value="Georgia">Georgia</option><option value="Lucida Console">Lucida Console</option><option value="Tahoma">Tahoma</option><option value="Arial">Arial</option></select></div>';

    //Add font-color selection
    var fontColorIcon = '<div style="width:50px; float:left;"><a class="colorpicker" id="ancFontColorDefaultShape" href="javascript:void(0);" class="geButton" tabindex="0" title="Font color..."><div class="geSprite geSprite-fontcolor"></div></a></div>';

    //Add background color selection
    var backgroundColorIcon = '<div style="width:50px; float:left;"><a class="colorpicker" id="ancFontBGColorDefaultShape" href="javascript:void(0);" class="geButton" tabindex="0" title="Line color..."><div class="geSprite geSprite-strokecolor"></div></a></div>';

    //Add separator
    var separator = '<div class="geSeparator"></div>';

    //Add color selection
    var colorPopUp = '<div class="geDialog" style="width: 220px; height: 400px; top:-150px; z-index:9999; display:none;"><div><div style="width: 230px; height: 100px; position: relative; padding-bottom: 10px;"><div style="display: block; position: absolute; right: 0px; top: 0px; border-width: 1px; border-style: solid; border-color: threedshadow threedhighlight threedhighlight threedshadow;"><div style="overflow: hidden; width: 16px; height: 101px;"><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(255, 255, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(246, 246, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(236, 236, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(226, 226, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(216, 216, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(206, 206, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(196, 196, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(187, 187, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(177, 177, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(167, 167, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(157, 157, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(147, 147, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(137, 137, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(128, 128, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(118, 118, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(108, 108, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(98, 98, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(88, 88, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(78, 78, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(68, 68, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(59, 59, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(49, 49, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(39, 39, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(29, 29, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(19, 19, 0);"></div><div style="height: 4px; font-size: 1px; line-height: 0; background-color: rgb(9, 9, 0);"></div></div></div><div style="display: block; position: absolute; right: 0px; top: 0px; width: 25px; height: 103px; cursor: pointer; background-image: url(http://localhost:52240/Scripts/tap.js.snap/js.mxgraph/baseresource/grapheditor/www/jscolor/arrow.gif); background-position: 0px -4px; background-repeat: no-repeat;"></div><div style="position: absolute; left: 0px; top: 0px; border-width: 1px; border-style: solid; border-color: threedshadow threedhighlight threedhighlight threedshadow;"><div style="width: 181px; height: 101px; background-image: url(http://localhost:52240/Scripts/tap.js.snap/js.mxgraph/baseresource/grapheditor/www/jscolor/hs.png); background-position: 0px 0px; background-repeat: no-repeat;"></div></div><div style="position: absolute; left: 0px; top: 0px; width: 190px; height: 103px; cursor: crosshair; background-image: url(http://localhost:52240/Scripts/tap.js.snap/js.mxgraph/baseresource/grapheditor/www/jscolor/cross.gif); background-position: 24px -6px; background-repeat: no-repeat;"></div><div style="display: none; position: absolute; left: 0px; bottom: 0px; padding: 0px 15px; height: 18px; border-width: 1px; border-style: solid; border-color: threedhighlight threedshadow threedshadow threedhighlight; color: buttontext; font-style: normal; font-variant: normal; font-weight: normal; font-size: 12px; line-height: normal; font-family: sans-serif; text-align: center; cursor: pointer;"><span style="line-height: 18px;">Close</span></div></div><input id="inputGetColor" autocomplete="off" style="margin-bottom: 10px; width: 216px; color: rgb(0, 0, 0); background-image: none; background-color: rgb(255, 255, 0);"><br><center><table cellspacing="0" style="border-collapse: collapse; margin-bottom: 8px;"><tbody><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(230, 208, 222);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(205, 162, 190);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(181, 115, 157);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(225, 213, 231);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(195, 171, 208);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(166, 128, 184);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(212, 225, 245);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(169, 196, 235);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(126, 166, 224);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(213, 232, 212);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(154, 199, 191);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(103, 171, 159);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(213, 232, 212);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(185, 224, 165);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(151, 208, 119);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 242, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 229, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 217, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 244, 195);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 206, 159);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 181, 112);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(248, 206, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(241, 156, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(234, 107, 102);"></td></tr></tbody></table><table cellspacing="0" style="border-collapse: collapse; margin-bottom: 16px;"><tbody><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background: url(http://localhost:52240/Scripts/tap.js.snap/js.mxgraph/baseresource/grapheditor/www/images/nocolor.png);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(230, 230, 230);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 204, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(179, 179, 179);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 153, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(128, 128, 128);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 102, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(77, 77, 77);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 51, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(26, 26, 26);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 0);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 204, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 230, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(230, 255, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 255, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 255, 230);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 229, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 204, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(229, 204, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 204, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 204, 230);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 153, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 204, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 255, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 255, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 255, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 204, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 153, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 153, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 153, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 153, 204);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 102, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 179, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(179, 255, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 255, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 255, 179);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 178, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 102, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(178, 102, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 102, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 102, 179);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 51, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 153, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 255, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 255, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 255, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 153, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 51, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 51, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 51, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 51, 153);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 0, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 128, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 255, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(128, 255, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 255, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 255, 128);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 255, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 127, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(127, 0, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 0, 255);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(255, 0, 128);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 0, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 102, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 204, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 204, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 204, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 204, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 204, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 102, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 0, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 0, 204);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(204, 0, 102);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 0, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 76, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 153, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(77, 153, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 153, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 153, 77);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 153, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 76, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(76, 0, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 0, 153);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(153, 0, 77);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 0, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 51, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 102, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 102, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 102, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 102, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 102, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 51, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 0, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 0, 102);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(102, 0, 51);"></td></tr><tr><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 0, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 26, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 51, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(26, 51, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 51, 0);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 51, 26);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 51, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 25, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(0, 0, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(25, 0, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 0, 51);"></td><td style="border: 1px solid black; padding: 0px; width: 16px; height: 16px; background-color: rgb(51, 0, 26);"></td></tr></tbody></table></center><div style="text-align: right; white-space: nowrap;"><button id="btnApplyColorPickerBox">Apply</button><button id="btnCancelColorPickerBox">Cancel</button></div></div></div>';

    //Add bold
    var boldIcon = '<div style="width:30px; float:left;"><a id="ancFontBoldDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Bold"><div class="geSprite geSprite-bold"></div></a></div>';

    //Add italic
    var italicIcon = '<div style="width:30px; float:left;"><a id="ancFontItalicDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Italic"><div class="geSprite geSprite-italic"></div></a></div>';

    //Add underline
    var underlineIcon = '<div style="width:30px; float:left;"><a id="ancUnderlineDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Underline"><div class="geSprite geSprite-underline"></div></a></div></br></br>';

    var previewShape = '<div id="divPreviewShape" style="display:none;margin-top:50px;  text-align:center; height:130px;"></div>';

    var cutomizeStylesIcons = '<div class="geToolbarContainer" id="divShapeDefaultsActionIcons" style="left: 0px; right: 0px; margin-top:10px; height: 34px;">' + fontSize + fontFamily + '<div class="geToolbar"><div class="geSeparator"></div><a id="ancFontBoldDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Bold"><div class="geSprite geSprite-bold"></div></a><a id="ancFontItalicDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Italic"><div class="geSprite geSprite-italic"></div></a><a id="ancFontUnderlineDefault" href="javascript:void(0);" class="geButton" tabindex="0" title="Underline"><div class="geSprite geSprite-underline"></div></a><a id="ancFontColorDefaultShape" href="javascript:void(0);" class="geButton" tabindex="0" title="Font color..."><div class="geSprite geSprite-fontcolor"></div></a><a id="ancFontBGColorDefaultShape" href="javascript:void(0);" class="geButton" tabindex="0" title="Fill color..."><div class="geSprite geSprite-fillcolor"></div></a></div></div>';

    //Making pop up
    //var dynamicDialog = $('<div id="divDefaultShapePopUp" style="font-size:14px;">Select shape to apply defaults </br></br>' + shapeIcons + '</br><div id="divDefaultShapeStyleOption" style="display:block;">' + fontSize + fontFamily + separator + bold + italic + underline + fontColor + backgroundColor + '</div></div>');
   
    var dynamicDialog = $('<div id="divDefaultShapePopUp" style="font-size:14px; height:130px;">Select shape to apply defaults </br></br>' + shapeIcons + '<div id="divDefaultShapeStyleOption" style="display:none;">' + cutomizeStylesIcons + '</div>' + previewShape + '</div>');
    dynamicDialog.dialog({
        title: "Set Shape Defaults",
        modal: true,
        width: 550,
        //height:300,
        buttons: [{ text: "Apply Changes", id: "btnApplyShapes", click: function () { SaveDefaultShapeStyle(); } }, { text: "Cancel", id: "btnCancelShapes",  click: function () { DefaultShapePopUpClose(); } }]
    });
    // for adding default custom button style by deepthi
    $("#btnApplyShapes, #btnCancelShapes").removeClass().addClass("btn btn-primary btn-small");
    $("#btnApplyShapes, #btnCancelShapes").button().hover(function (e) {
        $(this).removeClass("ui-state-hover");
    });
    //no hover effect in IE9 as its not proper
    if (isIE9() != 9) {
  
    $("#btnApplyShapes, #btnCancelShapes").hover(function () {
        $(this).css('background-color', '#0044cc');
    });
    } 
 
    //Bind click functionality with shape
    //ClickDefaultShape();

    //Bind change functionality with font size drop-down
    SelectFontSizeDefault();
    
    //Bind change functionality with font family drop-down
    SelectFontFamilyDefault();

    //Bind select functionality with font bold
    SelectFontBoldDefault();

    //Bind select functionality with font italic
    SelectFontItalicDefault();


    //Bind select functionality with font underline
    SelectFontUnderlineDefault();

    //Bind color and background icon to open color picker
    BindColorIconsShapeDefaultsPopUp();

}
function isIE9() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
//Bind color and background icon to open color picker
function BindColorIconsShapeDefaultsPopUp() {
    
    //Bind open functionality of color picker
    $('#ancFontColorDefaultShape').live('click', function (e) {
        
        if (editui.dialogs != null && editui.dialogs.length > 0) { editui.hideDialog(true); }
        _DefaultShapeStyle.isToSave = true;
        _colorPickerDefaultShape = true;
        _fontColorDefaultShape = true;
        editui.menus.pickColor(mxConstants.STYLE_FONTCOLOR);
        //$('.geDialog').css('z-index', '10999');

    });

    //Bind open functionality of color picker
    $('#ancFontBGColorDefaultShape').live('click', function (e) {
        
        if (editui.dialogs != null && editui.dialogs.length > 0) { editui.hideDialog(true); }
        _DefaultShapeStyle.isToSave = true;
        _colorPickerDefaultShape = true;
        _fontBGColorDefaultShape = true;
        editui.menus.pickColor(mxConstants.STYLE_FILLCOLOR);
        //$('.geDialog').css('z-index', '10999');

    });

}

//Bind click functionality with shape
function ClickDefaultShape() {

    $('#ancEventDefaultShape').live('click', function (e) {
        _currentSelection = 'rectangle;';
        $('#divDefaultShapePopUp').find('a').css('border', '0px');
        $(this).parent('div').find('a').css('border', '0px');
        $(this).css('border', '1px solid gray');
        $(this).parent('div').next().show();
        $(this).parent('div').next().next().show();
        var previewShape = $(this).parent('div').next().next();
        $(previewShape).html('<svg style="width: 200px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 180px; top: 5px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="20" y="20" width="120" height="60" fill="#ffffff" stroke="#000000" pointer-events="all"></rect><text x="80" y="50" text-anchor="middle" alignment-baseline="middle">Event</text></g></g><g></g></g></svg>');
        _previewShape = previewShape;

        ApplyExistStyleOnPreviewShape('rectangle');

        //Hide/show the other shapes.
        OtherDefaultShapesHideORShow('rectangle');
    });

    $('#ancConditionDefaultShape').live('click', function (e) {

        _currentSelection = 'rounded=1;';
        $('#divDefaultShapePopUp').find('a').css('border', '0px');
        $(this).parent('div').find('a').css('border', '0px');
        $(this).css('border', '1px solid gray');
        $(this).parent('div').next().show();

        $(this).parent('div').next().next().show();
        var previewShape = $(this).parent('div').next().next();
        $(previewShape).html('<svg style="width: 200px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 180px; top: 5px; text-decoration:none;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="20" y="20" width="120" height="60" rx="4" ry="4" fill="#ffffff" stroke="#000000" pointer-events="all"></rect><text x="80" y="50" text-anchor="middle" alignment-baseline="middle">Condition</text></g></g><g></g></g></svg>');
        _previewShape = previewShape;

        ApplyExistStyleOnPreviewShape('oval');

        //Hide/show the other shapes.
        OtherDefaultShapesHideORShow('oval');

    });

    $('#ancIncidentDefaultShape').live('click', function (e) {

        _currentSelection = 'ellipse;';
        $('#divDefaultShapePopUp').find('a').css('border', '0px');
        $(this).parent('div').find('a').css('border', '0px');
        $(this).css('border', '1px solid gray');
        $(this).parent('div').next().show();

        $(this).parent('div').next().next().show();
        var previewShape = $(this).parent('div').next().next();
        $(previewShape).html('<svg style="width: 150px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 180px; top: 4px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><ellipse cx="60" cy="50" rx="44.883720930232558" ry="44.883720930232558" fill="#ffffff" stroke="#000000" pointer-events="all"></ellipse><text x="60" y="50" text-anchor="middle" alignment-baseline="middle">Incident</text></g></g><g></g></g></svg>');
        _previewShape = previewShape;

        ApplyExistStyleOnPreviewShape('circle');

        //Hide/show the other shapes.
        OtherDefaultShapesHideORShow('circle');
    });

    $('#ancTextDefaultShape').live('click', function (e) {

        var textSelectionMarkUp = '<svg style="width: 100px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 200px; top: 20px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"></g><g style="visibility: visible;"><g transform="translate(2,6)scale(0.6956521739130435)"><foreignObject pointer-events="all" width="100" height="70"><div style="display:inline-block;font-size:32px;font-family:Verdana;color:#000000;line-height:70px;vertical-align:top;width:100px;white-space:normal;text-align:center;">Text</div></foreignObject></g></g></g><g></g></g></svg>';
        //var textSelectionMarkUp = '<svg style="width: 200px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 200px; top: 20px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"></g><g style="visibility: visible;"><g transform="translate(2,6)scale(0.6956521739130435)"><foreignObject pointer-events="all" width="200" height="70"><div style="display:inline-block;font-size:32px;font-family:Verdana;color:#000000;width:200px;height:35px;white-space:normal;text-align:center;">Text</div></foreignObject></g></g></g><g></g></g></svg>';

        //Change the mark up if it is IE.
        if (isIE) {
            //textSelectionMarkUp = '<div style="transform-origin: 0% 0%; left: 250px; top: 200px; width: 200px; text-align: center; padding-top: 17px; height: 35px; color: rgb(0, 0, 0); line-height: 14px; white-space: normal; position: absolute; transform: scale(0.695652) translate(-50%, 0%);">Text</div></div>';
            textSelectionMarkUp = '<div style="transform-origin: 0% 0%; left: 250px; top: 200px; width: 100px; text-align: center; padding-top: 40px; height: 50px; color: rgb(0, 0, 0); line-height: 14px; white-space: normal; position: absolute; transform: scale(0.695652) translate(-50%, 0%);">Text</div></div>';
        }

        _currentSelection = 'text;';
        $('#divDefaultShapePopUp').find('a').css('border', '0px');
        $(this).parent('div').find('a').css('border', '0px');
        $(this).css('border', '1px solid gray');
        $(this).parent('div').next().show();

        $(this).parent('div').next().next().show();
        var previewShape = $(this).parent('div').next().next();

        $(previewShape).html(textSelectionMarkUp);
        //$(previewShape).html('<svg style="width: 100px; height: 100px; display: block; position: relative; overflow: hidden; cursor: pointer; left: 200px; top: 20px;"><g><g></g><g><g transform="translate(0.5,0.5)" style="visibility: visible;"><rect x="1" y="1" width="28" height="18" fill="none" stroke="white" pointer-events="stroke" visibility="hidden" stroke-width="7"></rect><rect x="1" y="1" width="28" height="18" fill="none" stroke="none" pointer-events="all"></rect></g><g style="visibility: visible;"><g transform="translate(2,6)scale(0.6956521739130435)"><foreignObject pointer-events="all" width="100" height="70"><div style="display:inline-block;font-size:32px;font-family:Verdana;color:#000000;line-height:70px;vertical-align:top;width:100px;white-space:normal;text-align:center;">Text</div></foreignObject></g></g></g><g></g></g></svg>');

        _previewShape = previewShape;

        ApplyExistStyleOnPreviewShape('label');

        //Hide/show the other shapes.
        OtherDefaultShapesHideORShow('label');
    });

}

//Apply Defaults exist style on preview shape
function ApplyExistStyleOnPreviewShape(shape) {

    //Get Shape Defaults From DB
    //commneted by deepthi on 11th july 2014
    //DefaultShapesDataGet();

    if (shape == null || shape.length == 0 || _shapes[shape] == undefined)
        return;

    $(_previewShape).find('text').attr('fill', _shapes[shape].fontColor);
    $(_previewShape).find('rect').attr('fill', _shapes[shape].color);
    $(_previewShape).find('ellipse').attr('fill', _shapes[shape].color);

    $(_previewShape).find('div').css('color', _shapes[shape].fontColor);
    $(_previewShape).find('div').css('background-color', _shapes[shape].color);

    $(_previewShape).find('text').css('font-size', _shapes[shape].fontSize + 'px');
    $(_previewShape).find('div').css('font-size', _shapes[shape].fontSize + 'px');

    $(_previewShape).find('text').css('font-family', _shapes[shape].fontFamily);
    $(_previewShape).find('div').css('font-family', _shapes[shape].fontFamily);

    //Set exist font style in preview shape
    SetExistFontStyleOnPreviewShape(shape);

    //Set exist font style in actions based on shape
    SetFontSizeFamilyActionShapeDefaults(shape);
}

//Set exist font style in preview shape (bold, italic, and underline) combination
function SetExistFontStyleOnPreviewShape(shape) {

    _DefaultShapeStyle.Bold = '0';
    _DefaultShapeStyle.Underline = '0';
    _DefaultShapeStyle.Italic = '0';

    if (_shapes[shape].fontStyle == _combFontStyleBIU) {

        _DefaultShapeStyle.Bold = '1';
        _DefaultShapeStyle.Underline = '1';
        _DefaultShapeStyle.Italic = '1';

        $(_previewShape).find('text').css('font-weight', 'bold');
        $(_previewShape).find('div').css('font-weight', 'bold');

        $(_previewShape).find('text').css('font-style', 'italic');
        $(_previewShape).find('div').css('font-style', 'italic');

        $(_previewShape).find('text').css('text-decoration', 'underline');
        $(_previewShape).find('div').css('text-decoration', 'underline');

    }
    else if (_shapes[shape].fontStyle == _combFontStyleBI) {

        _DefaultShapeStyle.Bold = '1';
        _DefaultShapeStyle.Italic = '1';

        $(_previewShape).find('text').css('font-weight', 'bold');
        $(_previewShape).find('div').css('font-weight', 'bold');

        $(_previewShape).find('text').css('font-style', 'italic');
        $(_previewShape).find('div').css('font-style', 'italic');
    }
    else if (_shapes[shape].fontStyle == _combFontStyleIU) {

        _DefaultShapeStyle.Underline = '1';
        _DefaultShapeStyle.Italic = '1';

        $(_previewShape).find('text').css('font-style', 'italic');
        $(_previewShape).find('div').css('font-style', 'italic');

        $(_previewShape).find('text').css('text-decoration', 'underline');
        $(_previewShape).find('div').css('text-decoration', 'underline');
    }
    else if (_shapes[shape].fontStyle == _combFontStyleBU) {

        _DefaultShapeStyle.Bold = '1';
        _DefaultShapeStyle.Underline = '1';

        $(_previewShape).find('text').css('font-weight', 'bold');
        $(_previewShape).find('div').css('font-weight', 'bold');

        $(_previewShape).find('text').css('text-decoration', 'underline');
        $(_previewShape).find('div').css('text-decoration', 'underline');
    }
    else if (_shapes[shape].fontStyle == _combFontStyleB) {
        _DefaultShapeStyle.Bold = '1';
        $(_previewShape).find('text').css('font-weight', 'bold');
        $(_previewShape).find('div').css('font-weight', 'bold');
    }
    else if (_shapes[shape].fontStyle == _combFontStyleI) {

        _DefaultShapeStyle.Italic = '1';

        $(_previewShape).find('text').css('font-style', 'italic');
        $(_previewShape).find('div').css('font-style', 'italic');

    }
    else if (_shapes[shape].fontStyle == _combFontStyleU) {

        _DefaultShapeStyle.Underline = '1';

        $(_previewShape).find('text').css('text-decoration', 'underline');
        $(_previewShape).find('div').css('text-decoration', 'underline');
    }
}

//Set exist font style in actions based on shape
function SetFontSizeFamilyActionShapeDefaults(shape) {
    $(_previewShape).prev('div').find('#cmbfontSizeSelect').val(_shapes[shape].fontSize);
    $(_previewShape).prev('div').find('#fontfamilySelect').val(_shapes[shape].fontFamily);
}



//Set Font Style combination(bold, italic, underline)
function FontStyleCombination() {

    _DefaultShapeStyle.FontStyle = 0;

    if (_DefaultShapeStyle.Bold > 0 && _DefaultShapeStyle.Italic > 0 && _DefaultShapeStyle.Underline > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleBIU;
    }
    else if (_DefaultShapeStyle.Bold > 0 && _DefaultShapeStyle.Italic > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleBI;
    }
    else if (_DefaultShapeStyle.Italic > 0 && _DefaultShapeStyle.Underline > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleIU;
    }
    else if (_DefaultShapeStyle.Bold > 0 && _DefaultShapeStyle.Underline > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleBU;
    }
    else if (_DefaultShapeStyle.Bold > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleB;
    }
    else if (_DefaultShapeStyle.Italic > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleI;
    }
    else if (_DefaultShapeStyle.Underline > 0) {
        _DefaultShapeStyle.FontStyle = _combFontStyleU;
    }
}

//Fill color in preview shape
function FillColorInPreviewShape() {

    if (_fontColorDefaultShape) {
        $(_previewShape).find('text').attr('fill', _DefaultShapeStyle.FontColor);
        $(_previewShape).find('div').css('color', _DefaultShapeStyle.FontColor);
    }
    else if (_fontBGColorDefaultShape) {
        $(_previewShape).find('rect').attr('fill', _DefaultShapeStyle.BackgroundColor);
        $(_previewShape).find('ellipse').attr('fill', _DefaultShapeStyle.BackgroundColor);
        $(_previewShape).find('div').css('background-color', _DefaultShapeStyle.BackgroundColor);
    }

}


//Bind change functionality with font size drop-down
function SelectFontSizeDefault() {

    $('#cmbfontSizeSelect').live('change', function (e) {
        _DefaultShapeStyle.FontSize = $(this).val();
        _DefaultShapeStyle.isToSave = true;
        $(_previewShape).find('text').css('font-size', $(this).val() + 'px');
        $(_previewShape).find('div').css('font-size', $(this).val() + 'px');

    });
}

//Bind change functionality with font size drop-down
function SelectFontFamilyDefault() {

    $('#fontfamilySelect').live('change', function (e) {//.change(function () {
        _DefaultShapeStyle.FontFamily = $(this).val();
        _DefaultShapeStyle.isToSave = true;
        $(_previewShape).find('text').css('font-family', $(this).val());
        $(_previewShape).find('div').css('font-family', $(this).val());
    });
}

//Bind change functionality with font bold drop-down
function SelectFontBoldDefault() {

    $('#ancFontBoldDefault').die('click').live('click', function (e) {
        //var shape = CurrentShapeGet(_currentSelection);
        //if ((_shapes[shape].fontStyle == _combFontStyleBIU || _shapes[shape].fontStyle == _combFontStyleBI || _shapes[shape].fontStyle == _combFontStyleBU ||
        //    _shapes[shape].fontStyle == _combFontStyleB ) && _DefaultShapeStyle.Bold > 0) {
        if (_DefaultShapeStyle.Bold > 0) {
            _DefaultShapeStyle.Bold = '';
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('font-weight', 'normal');
            $(_previewShape).find('div').css('font-weight', 'normal');
        }
        else {
            _DefaultShapeStyle.Bold = 1;
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('font-weight', 'bold');
            $(_previewShape).find('div').css('font-weight', 'bold');
        }
    });
}

//Bind change functionality with font italic drop-down
function SelectFontItalicDefault() {

    $('#ancFontItalicDefault').die('click').live('click', function (e) {

        // var shape = CurrentShapeGet(_currentSelection);

        //if (_shapes[shape].fontStyle == _combFontStyleBIU || _shapes[shape].fontStyle == _combFontStyleBI || _shapes[shape].fontStyle == _combFontStyleIU ||
        //  _shapes[shape].fontStyle == _combFontStyleI || _DefaultShapeStyle.Italic > 0) {
        if (_DefaultShapeStyle.Italic > 0) {
            _DefaultShapeStyle.Italic = '';
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('font-style', 'normal');
            $(_previewShape).find('div').css('font-style', 'normal');
        }
        else {
            _DefaultShapeStyle.Italic = 1;
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('font-style', 'italic');
            $(_previewShape).find('div').css('font-style', 'italic');
        }


    });
}

//Bind change functionality with font underline drop-down
function SelectFontUnderlineDefault() {

    $('#ancFontUnderlineDefault').die('click').live('click', function (e) {
        //var shape = CurrentShapeGet(_currentSelection);
        //if (_shapes[shape].fontStyle == _combFontStyleBIU || _shapes[shape].fontStyle == _combFontStyleIU || _shapes[shape].fontStyle == _combFontStyleBU ||
        //  _shapes[shape].fontStyle == _combFontStyleU || _DefaultShapeStyle.Underline > 0) {
        if (_DefaultShapeStyle.Underline > 0) {
            _DefaultShapeStyle.Underline = '';
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('text-decoration', 'none');
            $(_previewShape).find('div').css('text-decoration', 'none');
        }
        else {
            _DefaultShapeStyle.Underline = 1;
            _DefaultShapeStyle.isToSave = true;
            $(_previewShape).find('text').css('text-decoration', 'underline');
            $(_previewShape).find('div').css('text-decoration', 'underline');
        }


    });
}


//Save Default Shape styles in pop up
function SaveDefaultShapeStyle() {
   
    if (_currentSelection != null && _currentSelection.length > 0) {

        //if (_DefaultShapeStyle.isToSave == true) {
            // show all the shapes.
            OtherDefaultShapesHideORShow('showAllOtherShapes');

            var shape = CurrentShapeGet(_currentSelection);
            var styles = SetStylesForDefault();

            //Save
            SaveShapeDefaults(shape, styles);
        //}
        //else
        //    alert('No Changes to save');
    }
    else
        alert('Please select a shape');
}

//Make comma separated styles to save in DB
function SetStylesForDefault() {

    var styles = '';

    //Set Font Style combination
    FontStyleCombination();

    if (_DefaultShapeStyle.FontSize != '')
        styles += 'FontSize:' + _DefaultShapeStyle.FontSize + ',';
    if (_DefaultShapeStyle.FontFamily != '')
        styles += 'FontFamily:' + _DefaultShapeStyle.FontFamily + ',';
    if (_DefaultShapeStyle.FontColor != '')
        styles += 'FontColor:' + _DefaultShapeStyle.FontColor + ',';
    if (_DefaultShapeStyle.BackgroundColor != '')
        styles += 'BackgroundColor:' + _DefaultShapeStyle.BackgroundColor + ',';

    //if (_DefaultShapeStyle.FontStyle != '') //Check and fix this, here currently font style is mandatory for every hit.
    styles += 'FontStyle:' + _DefaultShapeStyle.FontStyle;
    //styles += 'FontStyle:' + _DefaultShapeStyle.FontStyle + ',';

    return styles;
}


//Clean all selected styles for default shape
function CleanDefaultShapeStylVal() {
    _DefaultShape.Event = '';
    _DefaultShape.Condition = '';
    _DefaultShape.Incident = '';
    _DefaultShape.Text = '';
    _DefaultShapeStyle.FontSize = '';
    _DefaultShapeStyle.Bold = '';
    _DefaultShapeStyle.Underline = '';
    _DefaultShapeStyle.Italic = '';
    _DefaultShapeStyle.FontColor = '';
    _DefaultShapeStyle.BackgroundColor = '';
    _DefaultShapeStyle.FontFamily = '';
    _DefaultShapeStyle.FontStyle = '';
    _currentSelection = '';
}


//Close Default Shape pop up
function DefaultShapePopUpClose() {
     
    //$('#divDefaultShapePopUp').hide();
    _DefaultShapeStyle.isToSave = false;
    $('#divDefaultShapePopUp').remove();
    $("#dialog:ui-dialog").dialog("destroy");
    $("#divDefaultShapePopUp").dialog("destroy");
    $('#divDefaultShapePopUp').bind("dialogclose", function (event, ui) {
    });
}


//Save the default shape data in databaseusing Ajax post call.
function SaveShapeDefaults(shape, styles) {

    currentShape = shape;
    //if (styles != null && styles.length > 0)
    {

        //Get user id.
        _user = parseInt($('#hdnUserId').val());
        var defaultShapeData = new DefaultShapeDataAll(_user, shape, styles);
        var data = JSON.stringify(defaultShapeData);
        serviceurl = _baseURL + 'Services/SnapCharT.svc/DefaultShapeSaveAll';
        AjaxPost(serviceurl, data, eval(DefaultShapeSaveSuccessAll), eval(DefaultShapeSaveFailAll));

    }
    //else
    //alert('Please select styles for the shape');
}

// Default shape data details.
function DefaultShapeDataAll(userId, shapeType, styles) {
    this.userId = userId;
    this.shapeType = shapeType;
    this.style = styles;
}

// DefaultShapeSaveAll Fail.
function DefaultShapeSaveFailAll() {

}

// DefaultShapeSaveAll success.
function DefaultShapeSaveSuccessAll(result) {

    //Update the default shape action in the _shapes.    
    if (_shapes[currentShape] != null || _shapes[currentShape] == undefined) {

        if (_shapes[currentShape] == undefined)
            _shapes[currentShape] = {};

        if (_DefaultShapeStyle.FontSize != '')
            _shapes[currentShape].fontSize = _DefaultShapeStyle.FontSize;
        if (_DefaultShapeStyle.FontFamily != '')
            _shapes[currentShape].fontFamily = _DefaultShapeStyle.FontFamily;
        if (_DefaultShapeStyle.Bold != '')
            _shapes[currentShape].bold = _DefaultShapeStyle.Bold;
        if (_DefaultShapeStyle.Italic != 0)
            _shapes[currentShape].italic = _DefaultShapeStyle.Italic;
        if (_DefaultShapeStyle.Underline != '')
            _shapes[currentShape].underline = _DefaultShapeStyle.Underline;
        if (_DefaultShapeStyle.FontColor != '')
            _shapes[currentShape].fontColor = _DefaultShapeStyle.FontColor;
        if (_DefaultShapeStyle.BackgroundColor != '')
            _shapes[currentShape].color = _DefaultShapeStyle.BackgroundColor;
        //if (_DefaultShapeStyle.FontStyle != '')
        _shapes[currentShape].fontStyle = _DefaultShapeStyle.FontStyle;
        _DefaultShapeStyle.isToSave = false;

    }

    //Clean all selected styles for default shape
    CleanDefaultShapeStylVal();
    $('#divDefaultShapeStyleOption').hide();
    $('#divPreviewShape').hide();
    $('#divShapeDefaultsShape').find('a').css('border', '0px');

    var message = 'Changes applied';

    var messageType = 'jSuccess';

    NotificationMessage(message, messageType, true, 3000);
}
