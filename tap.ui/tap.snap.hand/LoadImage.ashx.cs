﻿//---------------------------------------------------------------------------------------------
// File Name 	: tap.img.hand.ashx

// Date Created  : N/A

// Description   : Load the Image

// Javascript File Used : js.cst.report.js
//----------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace tap.snap.hand
{
    /// <summary>
    /// Summary description for tap_img_hand
    /// </summary>
    public class tap_img_hand : IHttpHandler
    {

        int _bannerLogo = 1;
        int _companyLogo = 2;
        int _reportLogo = 3;

        #region "Load the image and display in Report Page"
        public void ProcessRequest(HttpContext context)
        {
            string elementId = Convert.ToString(context.Request.QueryString["elementId"]);

           // hit dbase and bring image bytes to laod custom images
            if (elementId != null && elementId != string.Empty)
            {
                int reportElementId = Convert.ToInt32(elementId.Remove(0, 3));
                tap.dat.ReportElement reportElement = new tap.dat.ReportElement();
                tap.dat.EFEntity entity = new tap.dat.EFEntity();
                reportElement = entity.ReportElement.FirstOrDefault(a => a.ElementID == reportElementId);
                byte[] arrayFormat = reportElement.ReportImageData;

                //conevert bit values to byte array and create image
                context.Response.BinaryWrite((byte[])arrayFormat);
            }
            else if (context.Request.QueryString["CompanyId"] != null && context.Request.QueryString["LogoType"] != null)
            {
                int logoType = Convert.ToInt32(context.Request.QueryString["LogoType"]);
                tap.dom.adm.Company company = new tap.dom.adm.Company();
                int companyId = Convert.ToInt32(context.Request.QueryString["CompanyId"]);
                //get company logo for company profile
                if (context.Request.QueryString["LogoType"] != null && logoType.Equals(1))
                {
                    byte[] bannerLogo = company.GetBannerLogo(companyId);
                    if (bannerLogo != null && bannerLogo.Length > 0)
                    {
                        context.Response.BinaryWrite(bannerLogo);
                    }
                }

                //get report logo for company profile
                if (context.Request.QueryString["LogoType"] != null && logoType.Equals(3))
                {
                    byte[] reportLogo = company.GetReportLogo(companyId);
                    if (reportLogo != null && reportLogo.Length > 0)
                    {
                        context.Response.BinaryWrite(reportLogo);
                    }
                }
            }
            else if (context.Request.QueryString["CompanyId"] != null)
            {
                int companyId = Convert.ToInt32(context.Request.QueryString["CompanyId"]);
                tap.dom.adm.Company company = new tap.dom.adm.Company();
                byte[] bannerLogo = company.GetBannerLogo(companyId);
                if (bannerLogo != null)
                {
                    context.Response.BinaryWrite(bannerLogo);
                }

            }
            else
            {
                //call method to get bit values from companies table 
                tap.dat.Companies companyDetails = tap.dom.gen.CustomReport.GetCompanyDetails();
              
                byte[] arrayFormat = companyDetails.ReportLogo;

                if (arrayFormat != null)
                {
                    //conevert bit values to byte array and create image
                    context.Response.BinaryWrite((byte[])arrayFormat);
                }
            }           
       
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}