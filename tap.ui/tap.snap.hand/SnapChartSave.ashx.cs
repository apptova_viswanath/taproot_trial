﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tap.ui;
using tap.dat;
using System.Xml;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using com.mxgraph;
using System.Web.Script.Serialization;

namespace tap.snap.hand
{
    /// <summary>
    /// Summary description for SnapChartSave
    /// </summary>
    public class SnapChartSave : IHttpHandler
    {

        EFEntity _db = new EFEntity();
        tap.dom.snap.SnapChart _snapchart = new tap.dom.snap.SnapChart();
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/json";
                var jsonString = String.Empty;
                context.Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    jsonString = "[" + inputStream.ReadToEnd() + "]";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                RootObject[] imageData = js.Deserialize<RootObject[]>(jsonString);

                //Assign the image data information.
                string xml = imageData[0].xml;
                string bg = imageData[0].bg;
                string format = imageData[0].format;
                int width = imageData[0].w;
                int height = imageData[0].h;
                int userId = imageData[0].userId;
                int imagePad = imageData[0].imagePad;
                //////////
                string imageName = imageData[0].fileName;

                //Converts the xml into an image.
                XmlTextReader xmlReader = new XmlTextReader(new StringReader(xml));
                Color? background = (bg != null && !bg.Equals(mxConstants.NONE)) ? ColorTranslator.FromHtml(bg) : (Color?)null;
                Image image = mxUtils.CreateImage(width, height, background);
                //Image image = mxUtils.CreateImage(width + imagePad, height + imagePad, background);
                Graphics g = Graphics.FromImage(image);
                g.SmoothingMode = SmoothingMode.HighQuality;
                mxSaxOutputHandler handler = new mxSaxOutputHandler(new mxGdiCanvas2D(g));
                handler.Read(xmlReader);

                //Gets the path of the temp folder.
                string hostingPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                //check directory exists or not -if not create it.
                if (Directory.Exists(hostingPath) == false)
                {
                    Directory.CreateDirectory(hostingPath);
                }

                //string imageName = userId + "_convertedGraph.jpg";
                string imagePath = hostingPath + imageName;

                //Deletes the previous image if exists.            
                if (System.IO.File.Exists(imagePath))
                {
                    System.IO.File.Delete(imagePath);
                }

                //Saves the converted image into temp folder.            
                image.Save(imagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                image.Dispose();
                //if (image != null && image.Height > 500 && image.Width > 800)
                //{
                //    int minW = 1000;
                //    int minH = 800;
                //    string fmt = "jpg";
                //    ResizeImageFreeSize(imagePath, imagePath, minW, minH, fmt);
                //}

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        public class RootObject
        {
            public string format { get; set; }
            public int w { get; set; }
            public int h { get; set; }
            public string bg { get; set; }
            public string xml { get; set; }
            public int userId { get; set; }
            public string fileName { get; set; }
            public int imagePad { get; set; }
        }

        public static void ResizeImageFreeSize(string OriginalFile, string NewFile, int MinWidth, int MinHeight, string FileExtension)
        {
            var NewHeight = MinHeight;
            var NewWidth = MinWidth;
            // var OriginalImage = System.Drawing.Image.FromFile(OriginalFile); // This statlement along with generate error as file is locked so -->//GDI+ keeps a lock on files from which an image was contructed.  To avoid the lock, construct the image from a MemorySteam:

            MemoryStream ms = new MemoryStream(File.ReadAllBytes(OriginalFile));
            var OriginalImage = System.Drawing.Image.FromStream(ms);

            // If the image dimensions are more than min width and height 
            NewHeight = (OriginalImage.Height > MinHeight ? MinHeight : OriginalImage.Height);
            NewWidth = (OriginalImage.Width > MinWidth ? MinWidth : OriginalImage.Width);

            // Just resample the Original Image into a new Bitmap
            var ResizedBitmap = new System.Drawing.Bitmap(OriginalImage, NewWidth, NewHeight);

            // Saves the new bitmap in the same format as it's source image
            FileExtension = FileExtension.ToLower().Replace(".", "");

            ImageFormat Format = null;

            //Delete the existing image.
            if (System.IO.File.Exists(OriginalFile))
            {
                System.IO.File.Delete(OriginalFile);
            }

            switch (FileExtension)
            {
                case "jpg":
                    Format = ImageFormat.Jpeg;

                    Encoder quality = Encoder.Quality;
                    var ratio = new EncoderParameter(quality, 100L);
                    var codecParams = new EncoderParameters(1);
                    codecParams.Param[0] = ratio;
                    ResizedBitmap.Save(NewFile, GetEncoder(ImageFormat.Jpeg), codecParams);
                    break;
                case "gif":
                    Format = ImageFormat.Gif;
                    ResizedBitmap.Save(NewFile, Format);
                    break;
                case "png":
                    Format = ImageFormat.Png;
                    ResizedBitmap.Save(NewFile, Format);
                    break;
                default:
                    Format = ImageFormat.Png;
                    ResizedBitmap.Save(NewFile, Format);
                    break;
            }

            ResizedBitmap.Save(NewFile, Format);


            // Clear handle to original file so that we can overwrite it if necessary
            OriginalImage.Dispose();
            ResizedBitmap.Dispose();
        }

        //encode the image
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;
            return null;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}