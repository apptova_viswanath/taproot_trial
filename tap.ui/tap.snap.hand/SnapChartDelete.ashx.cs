﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace tap.snap.hand
{
    /// <summary>
    /// Summary description for SnapChartDelete
    /// </summary>
    public class SnapChartDelete : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";
            var json = String.Empty;
            context.Request.InputStream.Position = 0;

            using (var inputStream = new StreamReader(context.Request.InputStream))
            {
                json = "[" + inputStream.ReadToEnd() + "]";
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            ImageInfo[] imageDetails = js.Deserialize<ImageInfo[]>(json);

            string orgFileName = imageDetails[0].fileName;
            string filePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/temp/"), orgFileName);

            if (File.Exists(filePath))
            {
                try
                {

                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public class ImageInfo
        {
            public string fileName { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}