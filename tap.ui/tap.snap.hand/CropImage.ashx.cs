﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.snap.hand
{
    /// <summary>
    /// Summary description for CropImage
    /// </summary>
    public class CropImage : IHttpHandler
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                tap.dat.SnapCaps snapcap = null;
                context.Response.ContentType = "text/json";
                var json = String.Empty;
                context.Request.InputStream.Position = 0;

                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    json = "[" + inputStream.ReadToEnd() + "]";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                CropObject[] cropImageData = js.Deserialize<CropObject[]>(json);

                //Image properties
                int x = cropImageData[0].x;
                int y = cropImageData[0].y;
                int w = cropImageData[0].w;
                int h = cropImageData[0].h;

                int userId = cropImageData[0].userId;
                string orgFileName = cropImageData[0].fileName;
                string filePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/temp/"), orgFileName);
                //string filePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/temp/"), userId + "_convertedGraph.jpg");
                string cropFileName = "";
                string cropFilePath = "";

                if (File.Exists(filePath))
                {
                    System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);

                    Rectangle CropArea = new Rectangle(
                        Convert.ToInt32(x),
                        Convert.ToInt32(y),
                        Convert.ToInt32(w),
                        Convert.ToInt32(h));
                    try
                    {
                        Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                        using (Graphics g = Graphics.FromImage(bitMap))
                        {
                            g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                        }

                        cropFileName = userId + "_cropped_convertedGraph.jpg";
                        cropFilePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/temp/"), cropFileName);

                        //Temporarily save in temp folder.
                        bitMap.Save(cropFilePath);

                        //Snapcaps instance.
                        //snapcap = new tap.dat.SnapCaps();

                        tap.dat.SnapCaps Usnapcap = null;
                        int evtId = Convert.ToInt32(_cryptography.Decrypt(cropImageData[0].eventId));
                        int uId = cropImageData[0].userId;
                        var scCoTypeId = cropImageData[0].SnapCapCtypeId;
                        //Snapcaps instance for update.
                        Usnapcap = _db.SnapCaps.FirstOrDefault(a => a.UserID == uId && a.EventID == evtId && a.SnapCapCommonTypeId == scCoTypeId);
                        bool isAvailable = false;
                        if (Usnapcap != null)
                        {//update

                            isAvailable = true;
                            Usnapcap.SnapCapData = File.ReadAllBytes(cropFilePath);
                        }
                        else
                        {
                            //Insertion
                            //Snapcaps instance for insertion
                            snapcap = new tap.dat.SnapCaps();
                            if (cropImageData[0].isCommonType == false)
                                snapcap.SnapCapName = cropImageData[0].snapcapName;
                            else
                            {
                                snapcap.SnapCapCommonTypeId = scCoTypeId;
                                snapcap.SnapCapName = cropImageData[0].snapcapName;
                            }

                            snapcap.UserID = cropImageData[0].userId;

                            snapcap.EventID = Convert.ToInt32(_cryptography.Decrypt(cropImageData[0].eventId));

                            //converts the cropped image into bytes and saves in database.
                            snapcap.SnapCapData = File.ReadAllBytes(cropFilePath);
                            _db.AddToSnapCaps(snapcap);
                        }

                        _db.SaveChanges();

                        //Deletes the cropped image.
                        if (File.Exists(cropFilePath))
                            System.IO.File.Delete(cropFilePath);

                        //Dispose the image.
                        orgImg.Dispose();

                        //if (System.IO.File.Exists(filePath))
                        //{
                        //    System.IO.File.Delete(filePath);
                        //}
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public class CropObject
        {
            public int x { get; set; }
            public int y { get; set; }
            public int w { get; set; }
            public int h { get; set; }
            public int userId { get; set; }
            public string fileName { get; set; }
            public string eventId { get; set; }
            public string snapcapName { get; set; }
            public int SnapCapCtypeId { get; set; }
            public bool isCommonType { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}