﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Tabs.aspx.cs" Inherits="tap.ui.adm.Custom_Tabs"
    MasterPageFile="~/tap.mst/Master.Master" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Tabs.aspx.cs" Inherits="tap.ui.adm.Custom_Tabs"
    MasterPageFile="~/tap.mst/Admin.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucAdminTabs.ascx" TagName="Tab" TagPrefix="ucTab" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
        /*remove folder icon*/
        .jstree-default a ins.jstree-icon
        {
            display: none;
        }
    </style>
    <script type="text/javascript" language="javascript">
        //debugger;
       
        $(document).ready(function () {
          

        });

    </script>
    <!-- For Sorting and Dragging plugin files -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.create.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.js") %>" type="text/javascript"></script>
   
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fld.fn.js") %>" type="text/javascript"></script>

       <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
   
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.tab.fn.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/usr.ctrl.syslist.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>"
        rel="stylesheet" type="text/css" />
    <style>
        .ui-widget-content
        {
            background: url("images/ui-bg_flat_75_ffffff_40x100.png") repeat-x scroll 50% 50% #FFFFFF;
            border: 0px solid #AAAAAA;
            color: #222222;
        }
        .div-sortable-order {
            padding: 0px 2px 0px 2px;margin: 0px 0px 0px 0px;min-height: 23.1px;border: none;position: relative;float: left;
        }

    </style>
    <div class="siteContent adminSiteContent">
        <div id="adminTabs">
            <div >                                                    
                            <div id="addIcon" class="addDiv">
                                <a id="lnkCreateNewTab" style="color: rgb(90, 140, 174);"><img id="addtabimage" class="addImage" src="../../Images/add.png" alt="Demo">Add New Tab</a>
                            </div>
                        </div>

            <ucTab:Tab ID="adminTab" runat="server" />
            <div id="ulCustomMsg">
                <div>
                    <a href="javascript:void(0)" id="lnknew" class="AnchorTag" style="display: none">Click here to add a
                        new tab</a>
                </div>
                <div class="ClearAll MarginTop10">
                    <label id="lblEditMsg" class="UserLabel" style="display: none">
                        Click on the tab you want to edit</label>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
