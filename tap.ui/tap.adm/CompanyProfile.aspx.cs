﻿/********************************************************************************
 *  File Name   : CompanyProfile.aspx.cs
 *  Description : Company Profile page to upload company banner, report logo
 *  Created on  : N/A
 *******************************************************************************/


# region NameSpace
using System;
using System.Globalization;
using System.IO;
using System.Web.Script.Serialization;
using tap.dom.gen;
#endregion

namespace tap.adm
{

    //Company Profile page to upload company banner, report logo
    public partial class CompanyProfile : System.Web.UI.Page
    {
        int _companyBannerLogo = 1;
        int _companyLogo = 2;
        int _companyReportLogo = 3;
        JavaScriptSerializer _js = new JavaScriptSerializer();
        dom.adm.Company company = new dom.adm.Company();
        dom.adm.SiteSettings ss = new dom.adm.SiteSettings();//NOW
        protected void Page_Load(object sender, EventArgs e)
        {  

            int companyId = Convert.ToInt32(Session["CompanyId"]);
            int divisionId = 0;
            if (Page.RouteData.Values["DivisionID"] != null)
                int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
            if (divisionId > 0)
            {
                companyId = divisionId;
                hidden_DivisionId.Value = divisionId.ToString();
            }

            if (companyId > 0)
            {
                byte[] companyBannerLogo = company.GetBannerLogo(companyId);
                if (companyBannerLogo == null)
                {
                    FileUploadBannerLogo.Visible = true;
                    removeBannerLogo.Visible = false; bannerLogoPreview.Visible = false;
                    BannerLogoChange.Visible = false;
                }
                else
                {
                    imgBannerLogoPreview.ImageUrl = "~/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(companyId) + "&LogoType=1";
                }

                //byte[] companyLogo = company.GetCompanyLogo(companyId);
                //if (companyLogo == null)
                //{
                //    removeCompanyLogo.Visible = false; companyLogoPreview.Visible = false;
                //}
                //else
                //{
                //    imgCompanyLogoPreview.ImageUrl = "~/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(Session["CompanyId"]) + "&LogoType=2";
                //}

                byte[] companyReportLogo = company.GetReportLogo(companyId);
                if (companyReportLogo == null)
                {
                    FileUploadReportLogo.Visible = true;
                    removeReportLogo.Visible = false; reportLogoPreview.Visible = false;
                    ReportLogoChange.Visible = false;
                }
                else
                {
                    imgReportLogoPreview.ImageUrl = "~/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(companyId) + "&LogoType=3";
                }
            }

            if (!IsPostBack)
            {
                PopulateCompanyDetails();
                Session["CheckRefresh"] =  Server.UrlDecode(System.DateTime.Now.ToString());
            }
                
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ViewState["CheckRefresh"] = Session["CheckRefresh"];
        }
        /// <summary>
        /// Upload company banner and report logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnUpload_Click(object sender, EventArgs e)
        {

            try
            {
                if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
                {
                    //Get company id
                    int companyId = Convert.ToInt32(Session["CompanyId"]);
                    int divisionId = 0;
                    if (Page.RouteData.Values["DivisionID"] != null)
                        int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
                    if (divisionId > 0)
                        companyId = divisionId;

                    //to check files for submit
                    if ((FileUploadBannerLogo.PostedFile != null && FileUploadBannerLogo.PostedFile.ContentLength > 0) || (FileUploadReportLogo.PostedFile != null && FileUploadReportLogo.PostedFile.ContentLength > 0))// || (FileUploadCompanyLogo.PostedFile != null && FileUploadCompanyLogo.PostedFile.ContentLength > 0)
                    {
                        if (hasImageExtension(FileUploadBannerLogo.FileName) || hasImageExtension(FileUploadReportLogo.FileName))// || hasImageExtension(FileUploadCompanyLogo.FileName)
                        {
                            //Save logos
                            //dom.adm.Company company = new dom.adm.Company();
                            company.SaveLogo(companyId, FileUploadBannerLogo.PostedFile, FileUploadReportLogo.PostedFile);//, FileUploadCompanyLogo.PostedFile

                            if (FileUploadBannerLogo.PostedFile != null && FileUploadBannerLogo.PostedFile.ContentLength > 0)
                            {
                                removeBannerLogo.Visible = true;
                                bannerLogoPreview.Visible = true;
                                BannerLogoChange.Visible = true;
                                imgBannerLogoPreview.ImageUrl = "~/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(companyId) + "&LogoType=1";
                            }

                            if (FileUploadReportLogo.PostedFile != null && FileUploadReportLogo.PostedFile.ContentLength > 0)
                            {
                                removeReportLogo.Visible = true;
                                reportLogoPreview.Visible = true;
                                ReportLogoChange.Visible = true;
                                imgReportLogoPreview.ImageUrl = "~/tap.snap.hand/LoadImage.ashx?CompanyId=" + Convert.ToString(companyId) + "&LogoType=3";
                            }

                            //Display success notification message
                            //ClientScript.RegisterClientScriptBlock(GetType(), "sas",
                            //                                       "<script> alert('Inserted successfully');</script>", true);

                            DisplayNotification("Upload");
                        }
                        else
                        {
                            if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
                            {
                                ClientScript.RegisterStartupScript(Page.GetType(), "clientScript",
                                                                   "<script type='text/javascript'>ExtErrorDisplayNotification();</script>");
                            }
                        }
                    }
                    else
                    {
                        if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "clientScript",
                                                              // "<script type='text/javascript'>ErrorDisplayNotification();</script>");
                                                              "<script type='text/javascript'></script>");
                        }
                    }
                    Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
               }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        //remove banner logo
        protected void removeBannerLogo_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
                {
                    string removeMsg = "Remove";
                    //Get company id
                    int companyId = Convert.ToInt32(Session["CompanyId"]);
                    int divisionId = 0;
                    if (Page.RouteData.Values["DivisionID"] != null)
                        int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
                    if (divisionId > 0)
                        companyId = divisionId;
                    else
                        removeMsg += "-" + companyId.ToString();


                    //Delete logo
                    company.DeleteLogo(companyId, _companyBannerLogo);

                    removeBannerLogo.Visible = false;
                    bannerLogoPreview.Visible = false;

                    FileUploadBannerLogo.Visible = true;

                    BannerLogoChange.Visible = false;

                    //DisplayNotification("Remove");
                    DisplayNotification(removeMsg);
                    Session["CheckRefresh"] =  Server.UrlDecode(System.DateTime.Now.ToString());
                }
               
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
       
        //remove report logo
        protected void removeReportLogo_Click(object sender, EventArgs e)
        {

            try
            {
                if (Session["CheckRefresh"].ToString() == ViewState["CheckRefresh"].ToString())
                {
                    //Get company id
                    int companyId = Convert.ToInt32(Session["CompanyId"]);
                    int divisionId = 0;
                    if (Page.RouteData.Values["DivisionID"] != null)
                        int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
                    if (divisionId > 0)
                        companyId = divisionId;

                    //Delete logo
                    // dom.adm.Company company = new dom.adm.Company();
                    company.DeleteLogo(companyId, _companyReportLogo);

                    removeReportLogo.Visible = false;
                    reportLogoPreview.Visible = false;

                    FileUploadReportLogo.Visible = true;
                    ReportLogoChange.Visible = false;

                    DisplayNotification("Remove");
                    Session["CheckRefresh"] = Server.UrlDecode(System.DateTime.Now.ToString());
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageType"></param>
        public void DisplayNotification(string messageType)
        {
            if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "clientScript",
                                                    "<script type='text/javascript'>DisplayNotification('" + messageType + "');</script>");
            }
        }

        //check image extension
        protected static bool hasImageExtension(string filename)
        {
            // add other possible extensions here
            return Path.GetExtension(filename).Equals(".jpg", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(filename).Equals(".jpeg", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(filename).Equals(".png", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(filename).Equals(".gif", StringComparison.InvariantCultureIgnoreCase)
                || Path.GetExtension(filename).Equals(".bmp", StringComparison.InvariantCultureIgnoreCase);
        }

        public void PopulateCompanyDetails()
        {
            // Need to get values from database and populate into the Ui controls here.
            try
            {
                int companyId = Convert.ToInt32(Session["CompanyId"]);
                int divisionId = 0;
                if (Page.RouteData.Values["DivisionID"] != null)
                    int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
                if (divisionId > 0)
                    companyId = divisionId;

                if (companyId.Equals(0))
                    return;
                //dom.adm.Company company = new dom.adm.Company();

                tap.dat.Companies companyDetails = company.GetCompanyData(companyId);

                //var dslCompanyDetails = _js.Deserialize<dynamic>(CompanyDetails);

                if (!string.IsNullOrEmpty(companyDetails.CompanyName))
                    companyName.Value = companyDetails.CompanyName;

                if (!string.IsNullOrEmpty(companyDetails.PhoneNo))
                    companyPhoneNumber.Value = companyDetails.PhoneNo;

                if (!string.IsNullOrEmpty(companyDetails.Website))
                    companyWebsite.Value = companyDetails.Website;

                if (!string.IsNullOrEmpty(companyDetails.Address))
                    companyAddress.Value = companyDetails.Address;

                if (!string.IsNullOrEmpty(companyDetails.Email))
                    companyEmail.Value = companyDetails.Email;

                //kvs
                //Displaying only the date , no time required .
                string ssDateFormat = Session["DateFormat"].ToString() ;

                //Get sitesettings data
                if (ssDateFormat == "")
                    ssDateFormat = ss.DateFormatGet(companyId);                
                

                if (companyDetails.SubscriptionStart != null)
                    txtSubscriptionStart.Value = Convert.ToDateTime(companyDetails.SubscriptionStart).ToString(ssDateFormat, CultureInfo.InvariantCulture);



                if (companyDetails.SubscriptionEnd != null)
                    txtSubscriptionEnd.Value = Convert.ToDateTime(companyDetails.SubscriptionEnd).ToString(ssDateFormat, CultureInfo.InvariantCulture);
                lblSubscriptionDate.InnerHtml = txtSubscriptionEnd.Value;
             
                //if (companyDetails.SubscriptionStart != null)
                //    txtSubscriptionStart.Value = Convert.ToDateTime(companyDetails.SubscriptionStart).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                       
             

                //if (companyDetails.SubscriptionEnd != null)
                //    txtSubscriptionEnd.Value = Convert.ToDateTime(companyDetails.SubscriptionEnd).ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
                //    lblSubscriptionDate.InnerHtml = txtSubscriptionEnd.Value;

                var securityStr = companyDetails.CheckADSecurity.ToString();
                if (!string.IsNullOrEmpty(securityStr))
                {
                    if (securityStr == "True")
                    {
                        hidden_CheckADSecurity.Value  = "1";
                    }
                    else
                    {
                        hidden_CheckADSecurity.Value  = "0";
                    }
                }
                else
                {
                    hidden_CheckADSecurity.Value = "0";
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }


        protected void btnSetdateformat_Click(object sender, EventArgs e)
        {

            

           ////// PopulateCompanyDetails();
            int companyId = Convert.ToInt32(Session["CompanyId"]);
            int companyIdAdmin = Convert.ToInt32(Session["CompanyId"]);
            int userId = Convert.ToInt32(Session["UserId"]);

            int divisionId = 0;
            if (Page.RouteData.Values["DivisionID"] != null)
                int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
            if (divisionId > 0)
                companyId = divisionId;


            if(companyIdAdmin == companyId)
            {
                Session["DateFormat"] = cmbDateFormat.Value == "" ? "MMM dd yyyy" : cmbDateFormat.Value;
                //SessionService.DateFormat = cmbDateFormat.Value == "" ? "MMM dd yyyy" : cmbDateFormat.Value;
            }
            tap.dat.Companies companyDetails = company.GetCompanyData(companyId);
            //kvs
            //Displaying only the date , no time required .
            string ssDateFormat = Session["DateFormat"].ToString();

            //Get sitesettings data
            if (ssDateFormat == "")
                ssDateFormat = ss.DateFormatGet(companyId);


            if (companyDetails.SubscriptionStart != null)
                txtSubscriptionStart.Value = Convert.ToDateTime(companyDetails.SubscriptionStart).ToString(ssDateFormat, CultureInfo.InvariantCulture);



            if (companyDetails.SubscriptionEnd != null)
                txtSubscriptionEnd.Value = Convert.ToDateTime(companyDetails.SubscriptionEnd).ToString(ssDateFormat, CultureInfo.InvariantCulture);
            lblSubscriptionDate.InnerHtml = txtSubscriptionEnd.Value;


            //if (companyDetails.SubscriptionStart != null)
            //    txtSubscriptionStart.Value = Convert.ToDateTime(companyDetails.SubscriptionStart).ToString(Session["DateFormat"].ToString());


            //if (companyDetails.SubscriptionEnd != null)
            //{
            //    txtSubscriptionEnd.Value = Convert.ToDateTime(companyDetails.SubscriptionEnd).ToString(Session["DateFormat"].ToString());
            //    lblSubscriptionDate.InnerHtml = txtSubscriptionEnd.Value;
            //}
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "asddas", "getjquerydate();", true);

            //tap.dom.usr.Users usr = new tap.dom.usr.Users();
            //if (!usr.CheckIfGlobalAdminProfile(userId, companyId, divisionId))
            //{
            //    txtSubscriptionEnd.Visible = true;
            //    txtSubscriptionEnd.Visible = true;
            //    // divSubscription.Style = "display:block;";
            //}

        }

    }
}