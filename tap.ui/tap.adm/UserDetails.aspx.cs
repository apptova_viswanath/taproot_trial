﻿////---------------------------------------------------------------------------------------------
//// File Name 	: Event.aspx.cs

//// Date Created  : N/A

//// Description   : Event Page Methods (Incident,Investigations,Audid and CAP Events)

//// Javascript File Used : js.evt.js
////----------------------------------------------------------------------------------------------

//#region "Namespaces"
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using tap.dom;
//using System.Web.Script.Serialization;
//using tap.dat;
//using tap.dom.hlp;
//using tap.dom.adm;
//using tap.dom.usr;
//using System.Configuration;
//using tap.dom.gen;
//using System.Globalization;

//#endregion
//namespace tap.users
//{
//    public partial class UserDetails : System.Web.UI.Page
//    {
//        public string _isSU = ConfigurationManager.AppSettings["IsSU"];

//        tap.dom.usr.Users _users = new dom.usr.Users();
//        JavaScriptSerializer _js = new JavaScriptSerializer();
//        tap.dat.EFEntity _db = new EFEntity();
//        tap.dom.adm.SiteSettings _siteSettings = new dom.adm.SiteSettings();
//        tap.dat.EFEntity db = new tap.dat.EFEntity();
//        tap.dom.gen.Cryptography _crypt = new tap.dom.gen.Cryptography();
//        private int _UserID = 0;

//        #region For Page_load 
//        /// <summary>
//        /// Page Load Event
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            try
//            {
//                ShowErrorMessage(false);
//                var companyID = Session["CompanyId"].ToString();
//                txtCompanyID.Value = Convert.ToInt32(companyID).ToString();

//                int divisionId = 0;
//                if (Page.RouteData.Values["DivisionID"] != null)
//                    int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
//                if (divisionId > 0)
//                    txtCompanyID.Value = divisionId.ToString();

//                int.TryParse(Page.RouteData.Values["UserID"].ToString(), out _UserID);
//                hiddenUserId.Value = _UserID.ToString();

//                if (_UserID > 0)
//                {
//                    tap.dom.usr.Users user = new tap.dom.usr.Users();
//                    int comp = user.GetCompanyIdOnUserId(_UserID);
//                    txtCompanyID.Value = user.GetCompanyIdOnUserId(_UserID).ToString();
//                }

//                if (_UserID.ToString() != null && _UserID.ToString().Length > 0)
//                {
//                    tap.dom.adm.UserDetails userDetail = new dom.adm.UserDetails();
//                    bool isHasEvent = userDetail.IsUserHasEvent(_UserID.ToString());

//                    if (isHasEvent)
//                        hiddenIsUserHasEvent.Value = "True";
//                    else
//                        hiddenIsUserHasEvent.Value = "False";
//                }
//                if (!IsUserExists(_UserID))
//                {
//                    ShowErrorMessage(true);
//                    return;
//                }
//                string action = ((Page.RouteData.Values["Action"] != null) ? Page.RouteData.Values["Action"].ToString() : string.Empty);
//            }
//            catch (Exception)
//            {
                
//            }            

//            if (!IsPostBack)
//                PopulateUserDetails();
//        }
//        #endregion

//        public void PopulateUserDetails()
//        {

//            DateFormat dt = new DateFormat();

//            // Need to get values from database and populate into the Ui controls here.
//            try
//            {
//                //hide password expiry details foe create new user 
//                  string url = HttpContext.Current.Request.Url.AbsoluteUri;
//                  if (url.Contains("/New"))
//                  {
                      
//                      string[] encryptUrl = url.Split('/');
//                      int index = encryptUrl.Length;
//                      string ifNewUser = encryptUrl[encryptUrl.Length - 1].ToString();
//                      if (ifNewUser == "New")
//                          passwordExpiryDetails.Style["display"] = "none";
//                      else
//                          passwordExpiryDetails.Style["display"] = "inline";
//                  }
//                divSubscription.Visible = false;
//                divSubscriptionDate.Visible = false;
//                string dateFormat = string.Empty;

//                tap.dom.usr.Users user = new tap.dom.usr.Users();
//                var UserDetails = user.GetUserDetails(_UserID);

//                IList<tap.dat.Companies> companyData = user.GetUserData(_UserID); // Need Refractor after build

//                var dslUserDetails = _js.Deserialize<dynamic>(UserDetails);

//                if (!string.IsNullOrEmpty(dslUserDetails[0]["FirstName"]))
//                    txtFirstName.Value = dslUserDetails[0]["FirstName"];

//                if (!string.IsNullOrEmpty(dslUserDetails[0]["LastName"]))
//                    txtLastName.Value = dslUserDetails[0]["LastName"];

//                if (!string.IsNullOrEmpty(dslUserDetails[0]["UserName"]))
//                    txtUserName.Value = dslUserDetails[0]["UserName"];

//                if (!string.IsNullOrEmpty(dslUserDetails[0]["Password"]))
//                    txtPassword.Value = _crypt.Decrypt(dslUserDetails[0]["Password"]);

//                if (!string.IsNullOrEmpty(dslUserDetails[0]["Email"]))
//                    txtEmail.Value = dslUserDetails[0]["Email"];

//                if (!string.IsNullOrEmpty(Convert.ToString(dslUserDetails[0]["SubscriptionStart"])))
//                    //txtSubscriptionUserStart.Value = Convert.ToDateTime(dslUserDetails[0]["SubscriptionStart"]).ToString(Convert.ToString(Session["DateFormat"]),CultureInfo.InvariantCulture);                                         
//                    //txtSubscriptionUserStart.Value = dslUserDetails[0]["SubscriptionStart"].ToString(SessionService.DateFormat);
//                    txtSubscriptionUserStart.Value = Convert.ToDateTime(companyData.FirstOrDefault().SubscriptionStart).ToString(SessionService.DateFormat);

                
//                if (!string.IsNullOrEmpty(Convert.ToString(dslUserDetails[0]["SubscriptionEnd"])))
//                    //txtSubscriptionUserEnd.Value = dslUserDetails[0]["SubscriptionEnd"].ToString(SessionService.DateFormat);
//                    txtSubscriptionUserEnd.Value = Convert.ToDateTime(companyData.FirstOrDefault().SubscriptionEnd).ToString(SessionService.DateFormat);

//                if (UserDetails != null)
//                {
//                    int companyId = Convert.ToInt32(dslUserDetails[0]["CompanyID"]);
//                    tap.dat.Companies company = new Companies();
//                    dateFormat = Session["DateFormat"].ToString();
//                    //dateFormat = _siteSettings.GetDateFormat();
//                    company = _db.Companies.Where(a => a.CompanyID == companyId).FirstOrDefault();          
                    
//                    if (company != null && _isSU != null && _isSU.Equals("1") && company.SubscriptionEnd != null)
//                    {
//                        divSubscription.Visible = true;
//                        divSubscriptionDate.Visible = true;
//                        DateTime subscriptionDate = Convert.ToDateTime(company.SubscriptionEnd);
//                        lblSubscriptionDate.InnerHtml = subscriptionDate.ToString("MMM dd, yyyy");
//                    }

//                    if (!Convert.ToBoolean(company.CheckADSecurity))
//                        CalulatePasswordexpiry(_UserID);
//                    else if (ConfigurationManager.AppSettings["IsSU"] == "0" && Convert.ToBoolean(company.CheckADSecurity))
//                    {
//                        // Hide the password expiration info
//                        passwordExpiryDetails.Style["display"] = "none";
//                        PasswordNotExpired.Style["display"] = "none";
//                        PasswordExpired.Style["display"] = "none";

//                        // Hide the subscription info for AD users
//                        divSubscriptiondetails.Visible = false;

//                        // Disable the password modification input fields
//                        txtConfirmPassword.Disabled = true;
//                        txtPassword.Disabled = true;
//                        divPassword.Visible = false;
//                    }
//                    else
//                    {
//                        // Hide the password expiration info
//                        passwordExpiryDetails.Style["display"] = "inline";
//                        PasswordNotExpired.Style["display"] = "inline";
//                        PasswordExpired.Style["display"] = "inline";

//                        // Hide the subscription info for AD users
//                        divSubscriptiondetails.Visible = true;

//                        // Disable the password modification input fields
//                        txtConfirmPassword.Disabled = false;
//                        txtPassword.Disabled = false;
//                        divPassword.Visible = true;
//                    }

//                    SetPage(Convert.ToBoolean(company.CheckADSecurity));
//                }
                
//                var status = Convert.ToBoolean(dslUserDetails[0]["Active"]);
//                chkActive.Checked = status;

//                hiddenIsUserActive.Value = dslUserDetails[0]["Active"].ToString();

//                var admin = Convert.ToBoolean(dslUserDetails[0]["Admin"]);
//                chkAdmin.Checked = admin;

//                var viewEvents = Convert.ToBoolean(dslUserDetails[0]["ViewEvents"]);
//                chkViewAllEvents.Checked = viewEvents;

//                var editEvents = Convert.ToBoolean(dslUserDetails[0]["EditEvents"]);
//                chkEditAllEvents.Checked = editEvents;

//                //txtPassword.Value = dslUserDetails[0]["Password"].ToString();
//                //txtConfirmPassword.Value = dslUserDetails[0]["Password"].ToString();
//                //txtPassword.Attributes.Add("value", "yourPassword");

//                //txtPassword.Attributes["value"] = "I'm set";
//                //txtConfirmPassword.Attributes["value"] = "I'm set";

//                hiddenPassword.Value = _crypt.Decrypt(dslUserDetails[0]["Password"].ToString());
//            }
//            catch (Exception ex)
//            {
//                string msg = ex.Message;
//            }
//        }
//        public void CalulatePasswordexpiry(int userID)
//        {
//            Password password = new Password();
//            int PasswordexpiresIn = Convert.ToInt16(password.PasswordExpiresInDays(userID));

//            //Session["PasswordexpiresIn"] = PasswordexpiresIn;
//            //string PasswordexpiresIn =                             Session["PasswordexpiresIn"].ToString();
//            if (Convert.ToInt32(PasswordexpiresIn) > 0)
//            {
//                PasswordExpired.Style["display"] = "none";
//                PasswordNotExpired.Style["display"] = "inline";
//                //PasswordExpired.Visible = false;
//                //PasswordNotExpired.Visible = true;
//                lblPasswordexpiresIn.InnerHtml = PasswordexpiresIn + " Days";
//            }
//            else
//            {
//                PasswordExpired.Style["display"] = "inline";
//                PasswordNotExpired.Style["display"] = "none";
//                //PasswordExpired.Visible = true;
//                //PasswordNotExpired.Visible = false;
//                lblPasswordexpiredIn.InnerHtml = Math.Abs(PasswordexpiresIn).ToString();
//            }
//        }
//        public bool IsUserExists(int userID)
//        {
//            bool exists = true;
//            var user = db.Users.Where(x => x.UserID == userID).FirstOrDefault();
//            if (user == null)
//                exists = false;
//            return exists;
//        }

//        public void ShowErrorMessage(bool show)
//        {
//            if (!show)
//            {
//                divErrorMessage.Visible = false;
//                tabs.Visible = true;
//            }
//            else
//            {
//                divErrorMessage.Visible = true;
//                tabs.Visible = false;
//            }
//        }

//        protected void calculateexirydays_Click(object sender, EventArgs e)
//        {
//            CalulatePasswordexpiry(_UserID);
//            //Password password = new Password();
//            //int PasswordexpiresIn = Convert.ToInt16(password.PasswordExpiresInDays(_UserID));

//            ////Session["PasswordexpiresIn"] = PasswordexpiresIn;
//            ////string PasswordexpiresIn =                             Session["PasswordexpiresIn"].ToString();
//            //if (Convert.ToInt32(PasswordexpiresIn) > 0)
//            //{
//            //    PasswordExpired.Visible = false;
//            //    PasswordNotExpired.Visible = true;
//            //    lblPasswordexpiresIn.InnerHtml = PasswordexpiresIn + " Days";
//            //}
//            //else
//            //{
//            //    PasswordExpired.Visible = true;
//            //    PasswordNotExpired.Visible = false;
//            //    lblPasswordexpiredIn.InnerHtml = Math.Abs(PasswordexpiresIn).ToString();
//            //}
//        }

//        //public bool UserHasPermission(string incidentType)
//        //{
//        //    bool result = true;
//        //    try
//        //    {
//        //        string usrId = Session["UserId"].ToString();
//        //        int groupId = Convert.ToInt32(usrId);
//        //        var groupNavs = db.GroupNavigation.Where(x => x.GroupId == groupId).ToList();
//        //        var groupNavLinks = (from x in groupNavs select x.NavId).ToList();

//        //        string str = "";
//        //        switch (incidentType)
//        //        {
//        //            case "incident":
//        //                str = "/Event/Incident/Details-1/0/New";
//        //                break;

//        //            case "investigation":
//        //                str = "/Event/Investigation/Details-1/0/New";
//        //                break;

//        //            case "audit":
//        //                str = "/Event/Audit/Details-1/0/New";
//        //                break;

//        //            case "correctiveactionplan":
//        //                str = "/Event/Corrective-Action-Plan/Details-1/0/New";
//        //                break;
//        //        }


//        //        var Navs = db.TopNav.Where(x => x.LinkURL == str).ToList();
//        //        var NavLink = Navs.FirstOrDefault().TopNavID;

//        //        if (!groupNavLinks.Contains(NavLink))
//        //        {
//        //            //Response.Redirect(Request.UrlReferrer.ToString());
//        //            result = false;
//        //        }
//        //    }
//        //    catch(Exception ex)
//        //    {
//        //        Response.Redirect("~/tap.usr/login.aspx");
//        //    }

//        //    return result;
//        //}

//        private void SetPage(bool isADCompany)
//        {
//            if (_isSU.Equals("0"))
//            {
//                divSubscriptiondetails.Visible = false;
//            }
//            else
//            { divSubscriptiondetails.Visible = true; }
//        }
//    }
//}

//---------------------------------------------------------------------------------------------
// File Name 	: Event.aspx.cs

// Date Created  : N/A

// Description   : Event Page Methods (Incident,Investigations,Audid and CAP Events)

// Javascript File Used : js.evt.js
//----------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;
using System.Web.Script.Serialization;
using tap.dat;
using tap.dom.hlp;
using tap.dom.adm;
using tap.dom.usr;
using System.Configuration;
using tap.dom.gen;
using System.Globalization;

#endregion
namespace tap.users
{
    public partial class UserDetails : System.Web.UI.Page
    {
        public string _isSU = ConfigurationManager.AppSettings["IsSU"];

        tap.dom.usr.Users _users = new dom.usr.Users();
        JavaScriptSerializer _js = new JavaScriptSerializer();
        tap.dat.EFEntity _db = new EFEntity();
        tap.dom.adm.SiteSettings _siteSettings = new dom.adm.SiteSettings();
        tap.dat.EFEntity db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _crypt = new tap.dom.gen.Cryptography();
        private int _UserID = 0;

        #region For Page_load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ShowErrorMessage(false);
                var companyID = Session["CompanyId"].ToString();
                txtCompanyID.Value = Convert.ToInt32(companyID).ToString();

                int divisionId = 0;
                if (Page.RouteData.Values["DivisionID"] != null)
                    int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
                if (divisionId > 0)
                    txtCompanyID.Value = divisionId.ToString();

                int.TryParse(Page.RouteData.Values["UserID"].ToString(), out _UserID);
                hiddenUserId.Value = _UserID.ToString();

                if (_UserID > 0)
                {
                    tap.dom.usr.Users user = new tap.dom.usr.Users();
                    int comp = user.GetCompanyIdOnUserId(_UserID);
                    txtCompanyID.Value = user.GetCompanyIdOnUserId(_UserID).ToString();
                }

                if (_UserID.ToString() != null && _UserID.ToString().Length > 0)
                {
                    tap.dom.adm.UserDetails userDetail = new dom.adm.UserDetails();
                    bool isHasEvent = userDetail.IsUserHasEvent(_UserID.ToString());

                    if (isHasEvent)
                        hiddenIsUserHasEvent.Value = "True";
                    else
                        hiddenIsUserHasEvent.Value = "False";
                }
                if (!IsUserExists(_UserID))
                {
                    ShowErrorMessage(true);
                    return;
                }
                string action = ((Page.RouteData.Values["Action"] != null) ? Page.RouteData.Values["Action"].ToString() : string.Empty);
            }
            catch (Exception)
            {

            }

            if (!IsPostBack)
                PopulateUserDetails();
        }
        #endregion

        public void PopulateUserDetails()
        {

            DateFormat dt = new DateFormat();

            // Need to get values from database and populate into the Ui controls here.
            try
            {
                //hide password expiry details foe create new user 
                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                if (url.Contains("/New"))
                {

                    string[] encryptUrl = url.Split('/');
                    int index = encryptUrl.Length;
                    string ifNewUser = encryptUrl[encryptUrl.Length - 1].ToString();
                    if (ifNewUser == "New")
                        passwordExpiryDetails.Style["display"] = "none";
                    else
                        passwordExpiryDetails.Style["display"] = "inline";
                }
                divSubscription.Visible = false;
                divSubscriptionDate.Visible = false;
                string dateFormat = string.Empty;

                tap.dom.usr.Users user = new tap.dom.usr.Users();
                var UserDetails = user.GetUserDetails(_UserID);

                IList<tap.dat.Companies> companyData = user.GetUserData(_UserID); // Need Refractor after build

                var dslUserDetails = _js.Deserialize<dynamic>(UserDetails);

                if (!string.IsNullOrEmpty(dslUserDetails[0]["FirstName"]))
                    txtFirstName.Value = dslUserDetails[0]["FirstName"];

                if (!string.IsNullOrEmpty(dslUserDetails[0]["LastName"]))
                    txtLastName.Value = dslUserDetails[0]["LastName"];

                if (!string.IsNullOrEmpty(dslUserDetails[0]["UserName"]))
                    txtUserName.Value = dslUserDetails[0]["UserName"];

                if (!string.IsNullOrEmpty(dslUserDetails[0]["Password"]))
                    txtPassword.Value = _crypt.Decrypt(dslUserDetails[0]["Password"]);

                if (!string.IsNullOrEmpty(dslUserDetails[0]["Email"]))
                    txtEmail.Value = dslUserDetails[0]["Email"];
                //kvs
                //Removes subscription part in any user page  
                //if (!string.IsNullOrEmpty(Convert.ToString(dslUserDetails[0]["SubscriptionStart"])))
                //    //txtSubscriptionUserStart.Value = Convert.ToDateTime(dslUserDetails[0]["SubscriptionStart"]).ToString(Convert.ToString(Session["DateFormat"]),CultureInfo.InvariantCulture);                                         
                //    //txtSubscriptionUserStart.Value = dslUserDetails[0]["SubscriptionStart"].ToString(SessionService.DateFormat);
                //    txtSubscriptionUserStart.Value = Convert.ToDateTime(companyData.FirstOrDefault().SubscriptionStart).ToString(SessionService.DateFormat);


                //if (!string.IsNullOrEmpty(Convert.ToString(dslUserDetails[0]["SubscriptionEnd"])))
                //    //txtSubscriptionUserEnd.Value = dslUserDetails[0]["SubscriptionEnd"].ToString(SessionService.DateFormat);
                //    txtSubscriptionUserEnd.Value = Convert.ToDateTime(companyData.FirstOrDefault().SubscriptionEnd).ToString(SessionService.DateFormat);
                //kve
                
                if (UserDetails != null)
                {
                    int companyId = Convert.ToInt32(dslUserDetails[0]["CompanyID"]);
                    tap.dat.Companies company = new Companies();
                    dateFormat = Session["DateFormat"].ToString();
                    //dateFormat = _siteSettings.GetDateFormat();
                    company = _db.Companies.Where(a => a.CompanyID == companyId).FirstOrDefault();



                    if (company != null && _isSU != null && _isSU.Equals("1") && company.SubscriptionEnd != null)
                    {
                        divSubscription.Visible = true;
                        divSubscriptionDate.Visible = true;
                        DateTime subscriptionDate = Convert.ToDateTime(company.SubscriptionEnd);
                        lblSubscriptionDate.InnerHtml = subscriptionDate.ToString("MMM dd, yyyy");


                    }
                    CalulatePasswordexpiry(_UserID);
                }

                var status = Convert.ToBoolean(dslUserDetails[0]["Active"]);
                chkActive.Checked = status;

                hiddenIsUserActive.Value = dslUserDetails[0]["Active"].ToString();

                var admin = Convert.ToBoolean(dslUserDetails[0]["Admin"]);
                chkAdmin.Checked = admin;

                var viewEvents = Convert.ToBoolean(dslUserDetails[0]["ViewEvents"]);
                chkViewAllEvents.Checked = viewEvents;

                var editEvents = Convert.ToBoolean(dslUserDetails[0]["EditEvents"]);
                chkEditAllEvents.Checked = editEvents;

                //txtPassword.Value = dslUserDetails[0]["Password"].ToString();
                //txtConfirmPassword.Value = dslUserDetails[0]["Password"].ToString();
                //txtPassword.Attributes.Add("value", "yourPassword");

                //txtPassword.Attributes["value"] = "I'm set";
                //txtConfirmPassword.Attributes["value"] = "I'm set";

                hiddenPassword.Value = _crypt.Decrypt(dslUserDetails[0]["Password"].ToString());

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
        public void CalulatePasswordexpiry(int userID)
        {
            Password password = new Password();
            int PasswordexpiresIn = Convert.ToInt16(password.PasswordExpiresInDays(userID));

            //Session["PasswordexpiresIn"] = PasswordexpiresIn;
            //string PasswordexpiresIn =                             Session["PasswordexpiresIn"].ToString();
            if (Convert.ToInt32(PasswordexpiresIn) > 0)
            {
                PasswordExpired.Style["display"] = "none";
                PasswordNotExpired.Style["display"] = "inline";
                //PasswordExpired.Visible = false;
                //PasswordNotExpired.Visible = true;
                lblPasswordexpiresIn.InnerHtml = PasswordexpiresIn + " Days";
            }
            else
            {
                PasswordExpired.Style["display"] = "inline";
                PasswordNotExpired.Style["display"] = "none";
                //PasswordExpired.Visible = true;
                //PasswordNotExpired.Visible = false;
                lblPasswordexpiredIn.InnerHtml = Math.Abs(PasswordexpiresIn).ToString();
            }
        }
        public bool IsUserExists(int userID)
        {
            bool exists = true;
            var user = db.Users.Where(x => x.UserID == userID).FirstOrDefault();
            if (user == null)
                exists = false;
            return exists;
        }

        public void ShowErrorMessage(bool show)
        {
            if (!show)
            {
                divErrorMessage.Visible = false;
                tabs.Visible = true;
            }
            else
            {
                divErrorMessage.Visible = true;
                tabs.Visible = false;
            }
        }

        protected void calculateexirydays_Click(object sender, EventArgs e)
        {
            CalulatePasswordexpiry(_UserID);
            //Password password = new Password();
            //int PasswordexpiresIn = Convert.ToInt16(password.PasswordExpiresInDays(_UserID));

            ////Session["PasswordexpiresIn"] = PasswordexpiresIn;
            ////string PasswordexpiresIn =                             Session["PasswordexpiresIn"].ToString();
            //if (Convert.ToInt32(PasswordexpiresIn) > 0)
            //{
            //    PasswordExpired.Visible = false;
            //    PasswordNotExpired.Visible = true;
            //    lblPasswordexpiresIn.InnerHtml = PasswordexpiresIn + " Days";
            //}
            //else
            //{
            //    PasswordExpired.Visible = true;
            //    PasswordNotExpired.Visible = false;
            //    lblPasswordexpiredIn.InnerHtml = Math.Abs(PasswordexpiresIn).ToString();
            //}
        }

        //public bool UserHasPermission(string incidentType)
        //{
        //    bool result = true;
        //    try
        //    {
        //        string usrId = Session["UserId"].ToString();
        //        int groupId = Convert.ToInt32(usrId);
        //        var groupNavs = db.GroupNavigation.Where(x => x.GroupId == groupId).ToList();
        //        var groupNavLinks = (from x in groupNavs select x.NavId).ToList();

        //        string str = "";
        //        switch (incidentType)
        //        {
        //            case "incident":
        //                str = "/Event/Incident/Details-1/0/New";
        //                break;

        //            case "investigation":
        //                str = "/Event/Investigation/Details-1/0/New";
        //                break;

        //            case "audit":
        //                str = "/Event/Audit/Details-1/0/New";
        //                break;

        //            case "correctiveactionplan":
        //                str = "/Event/Corrective-Action-Plan/Details-1/0/New";
        //                break;
        //        }


        //        var Navs = db.TopNav.Where(x => x.LinkURL == str).ToList();
        //        var NavLink = Navs.FirstOrDefault().TopNavID;

        //        if (!groupNavLinks.Contains(NavLink))
        //        {
        //            //Response.Redirect(Request.UrlReferrer.ToString());
        //            result = false;
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        Response.Redirect("~/tap.usr/login.aspx");
        //    }

        //    return result;
        //}
    }
}
