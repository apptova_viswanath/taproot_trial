﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true"
    CodeBehind="SystemNotifications.aspx.cs" Inherits="tap.notify.SystemNotifications" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />


    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.sys.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.sys.notify.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>
                <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>

    <style>
        input[type="checkbox"] {
            margin: 3px 0;
        }

        #body_txtNotifyMessage {
            resize: none !important;
            height: 100px !important;
            width: 390px !important;
        }

        #body_txtTitle {
            width: 390px !important;
        }

        #divHomeMessageDialog {
            height: 294.8px !important;
            margin-top: 10px !important;
        }

        #body_txtFromDate, #body_txtToDate {
            width: 150px !important;
        }

        #body_lblToDate {
            padding: 0 0 0 36px !important;
        }

        .buttonEdit {
            width: 23%;
            float: right;
        }

        .buttonDelete {
            width: 13%;
            float: right;
        }

        .buttonCancel {
            width: 37%;
            float: right;
        }
             #ui-datepicker-div {
            font-size: 12px;
        }
    </style>
    <asp:HiddenField ID="hdCurrentModule" ClientIDMode="Static" runat="server" />

    <div class="NotificationListGrid">

        <div class="NotificationListGrid">
            <div class="PaddingTop5">
                <div class="addDiv" id="addIcon">
                    <a id="lnkCreateNewNotification" href="">
                        <img class="addImage" src="<%=ResolveUrl("~/Images/add.png")%>" alt="Demo">Add Message</a>
                </div>
            </div>
            <div id="Grid">
                <table id="NotificationsGrid" class="scroll ClearAll notranslate">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="pageNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>

            <div class="ui-jqgrid-view NotificationsGrid hidden" id="divNoRowMessage">
                <div class="ui-jqgrid-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
                    <span class="ui-jqgrid-title">Home Page Messages</span>
                </div>
                <div id="divMessage" class="ui-widget-content jqgrow ui-row-ltr">
                    There is currently no message added.
                </div>

            </div>

        </div>
    </div>


    <%--  code for popup--%>
    <div class="demo hidden">
        <div id="divHomeMessageDialog" title="Edit System Notification" class="seasonContentHide">
            <div class="MarginTop10">
                <label id="lblName" class="ContentUserLabel min-width120">
                    Title<span id="spnName" style="color: Red">*</span></label>
            </div>
            <div class="MarginTop10">

                <input id="txtTitle" class="ContentInputTextBox WidthP55 marginleft0px" type="text" />
            </div>
            <div class="MarginTop10">
                <label id="lblFromDate" class="ContentUserLabel EventTypeDate min-width120">
                    From
                </label>
            </div>
            <div class="MarginTop10">

                <input id="txtFromDate" style="width: 19%;" class="DateInputTextBox marginleft0px" type="text" readonly="readonly" />
                <label id="lblToDate" class="ContentDateTimeLabel" runat="server">To</label>

                <input id="txtToDate" style="width: 19%;" class="DateInputTextBox trackChanges" type="text" readonly="readonly" />
            </div>
            <div class="MarginTop10">
                <label id="lblMessage" class="ContentUserLabel min-width120">Message<span id="Span1" style="color: Red">*</span></label>
            </div>
            <div class="MarginTop10">
                <textarea id="txtNotifyMessage" class="ContentInputTextBox WidthP55 marginleft0px"></textarea>
            </div>
            <div class="MarginTop10">
                <label class="ContentUserLabel min-width120">Active</label>
                <input type="checkbox" class="" id="chkActiveSystemNotification" />
            </div>

            <div style="width: 100%; margin-top: 10px;">
                <div class="buttonCenter">
                    <input type="button" class="btn btn-primary btn-small btn-float" style="text-align: left; float: left" id="btnCancelNotification" value="Cancel" />
                </div>
                <div class="buttonCenter">
                    <input type="button" id="btnDeleteNotification" style="float: left; text-align: right" value="Delete" class="btn btn-primary btn-small btn-float" />
                </div>
                <div class="buttonCenter">
                    <input type="button" id="btnSaveNofication" style="float: right; text-align: right" value="Create" class="btn btn-primary btn-small btn-float" />
                </div>

            </div>

            <div style="display: none">
                <input id="txtSysID" hidden="true" type="text" />
            </div>
        </div>
    </div>
    <%--  code for popup--%>
</asp:Content>
