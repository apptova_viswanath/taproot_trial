﻿//----------------------------------------------------------------------------------------------------
// File Name 	: Custom-Tabs.aspx.cs

// Date Created  : N/A

// Description   : Admin Custom Tabs Page (Display existing Custom Tabs.)

// Javascript File Used : js.adm.tab.js and js.adm.cst.js
//-----------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace tap.ui.adm
{
    public partial class Custom_Tabs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}