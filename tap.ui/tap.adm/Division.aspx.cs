﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.adm
{
    public partial class Division : System.Web.UI.Page
    {
        private int _divisionID = 0;
        int _companyBannerLogo = 1;
        int _companyLogo = 2;
        int _companyReportLogo = 3;
        JavaScriptSerializer _js = new JavaScriptSerializer();
        dom.adm.Divisions division = new dom.adm.Divisions();

        protected void Page_Load(object sender, EventArgs e)
        {

            int locationListId = 0;
            int classListId = 0;

            var companyID = Session["CompanyId"].ToString();
            if (Page.RouteData.Values["DivisionID"] != null)
            {
                int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out _divisionID);

                string listIds = division.SystemListIdGet(_divisionID);
                if (listIds.Contains(":"))
                {
                    string[] arrListId = listIds.Split(':');
                    if (arrListId != null)
                    {
                        hidden_LocationListId.Value = arrListId[1];
                        hidden_ClassListId.Value = arrListId[0];
                    }
                }
            }

            hidden_DivisionId.Value = _divisionID.ToString();
            if (!IsPostBack)
            {
                //PopulateDivisionDetails();
                //PopulateUserDetails();
            }
        }
        
    }
}