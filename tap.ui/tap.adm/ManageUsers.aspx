﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="ManageUsers.aspx.cs" Inherits="tap.users.ManageUsers" %>--%>
<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true"
    CodeBehind="ManageUsers.aspx.cs" Inherits="tap.users.ManageUsers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">

        <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 
        <style type="text/css">
        #txtSearchUser {
            width: 50%;
        }

    </style>
      <%--  <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>" rel="stylesheet" type="text/css" />--%>
        <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
    
        <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.man.usr.js") %>" type="text/javascript"></script>
        <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>
         <script type="text/javascript">
             var _baseURL = '<%= ResolveUrl("~/") %>';
       
        </script>
    <asp:HiddenField ID="hdCurrentModule" ClientIDMode="Static" runat="server" />

    <div class="siteContent adminSiteContent">
        <div class="bodyArea">
            <div class="userArea" id="divManagerUserSection" style="display:none;">
                <div class="divtxtSearchUser">
                       <%-- <input id="txtSearchInput" placeholder="Search Users" type="text" onkeyup="ValueChanged()" class="span2 bootstrp1"/>--%>
                        <%--<input id="chkIgnoreCase" type="checkbox" hidden="hidden" disabled="disabled" onchange="ValueChanged()" style=" display:none;"/>--%>
                </div>
              
                <div class="DivUsersManageSection">
                    <table>
                        <tr>
                            <td><a id="lnkCreateNewUser"><img class="addImageUser" src="<%=ResolveUrl("~/Images/add.png")%>" alt="Demo">Add User</a></td>
                            <td><a id="ancUploadUsers" ><img class="uploadImageUser" src="<%= ResolveUrl("~/Images/upload.gif") %>" alt="Demo">Upload Users</a></td>
                            <td><a id="ancEventAccess" ><img class="eventAccessImageUser" src="<%= ResolveUrl("~/Images/useraccess.png") %>" alt="Demo">Event Access</a></td>
                        </tr>
                 
                    </table>
                   <%-- <div id="divAddNewUser" class="divAddNewUser">                                                    
                            <div id="addIcon" class="addDiv">
                                <a id="lnkCreateNewUser"><img class="addImageUser" src="<%=ResolveUrl("~/Images/add.png")%>" alt="Demo">Add User</a>
                            </div>
                    </div>
                    <div id="divUploadUsers" class="divAddNewUser">                                                    
                            <div id="Div2" class="addDiv">
                                <a id="ancUploadUsers" ><img class="uploadImageUser" src="<%= ResolveUrl("~/Images/upload.gif") %>" alt="Demo">Upload Users</a>
                            </div>
                    </div>
                    <div id="divUserEventAccess" class="divAddNewUser">                                                    
                            <div id="Div4" class="addDiv">
                                <a id="ancEventAccess" ><img class="eventAccessImageUser" src="<%= ResolveUrl("~/Images/useraccess.png") %>" alt="Demo">Event Access</a>
                            </div>
                   </div>--%>
                </div>
                   <br />      
           </div>
             <div class="divSearchUser">
                 
                    <input  id="txtSearchUser" placeholder="Search Users" type="text" onkeyup="ValueChanged()" class="span2 bootstrp1"/>
                    <%--<input id="btnEncryptPassword" type="button" title="Encrypt all user passwords" value="Encrypt Passwords" class="btn btn-primary pull-right"/>--%>

                </div>
            <div class="UserGrid" style="margin-top:20px;">
                <table id="UsersGrid" class="scroll notranslate">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="pageNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>

             <div id="divEventAccessPopup"  class="hidden" title="Event Access">
                    <div>
                        <label id="addTeamMemberErrorMsg">
                        </label>
                    </div>

                    <div id="divEventUserAccessGrid" style="" class="UserGrid">
                    <input type="text" class="span2 bootstrp1" id="txtSearchUserEventAccess" placeholder="" title="Search" autofocus style="width:50%; margin-bottom:5px;"/>

                        <table id="EventUserAccessGrid" class="scroll">
                            <tr>
                                <td />
                            </tr>
                        </table>
                        <div id="EventUserAccessGridNavigation" class="scroll" style="text-align: center;">
                        </div>
                    </div>
                    <div class="MarginLeft10" id="divEventAccessOptions" style="display:none;">
                       <div>
                           <label id="lblAddListName" class="ContentUserLabel eventAccessTitle">
                                Event Access Options:
                           </label>
                       </div>
                       <div style="width:90%;" class="MarginTop30">
                           <input id="chkEventAccessView" class="MarginLeft20" type="checkbox" name="" />
                            <label id="lblADSecurity" class="CheckBoxLabel">
                                View All Event for This Division</label>
                            <br />
                           <input id="chkEventAccessEdit" class="MarginLeft20" type="checkbox" name="" />
                            <label id="lblCustomSecurity" class="CheckBoxLabel">
                                Edit All Event for This Division</label>
                            
                       </div>
                    </div>

                    <div class="MarginTop30 ClearAll">
                        <table class="eventAccessBtnTable">
                            <tr>
                               
                                <td class="eventAccessTableTd"> 
                                    <input type="button" class="btn btn-primary btn-small" id="btnEventAccessNext" value="Next" />
                                     <input type="button" class="btn btn-primary btn-small" id="btnEventAccessCancel" value="Cancel" />
                                     <input type="button" class="btn btn-primary btn-small" id="btnEventAccessBack" value="Back" style="display:none;"/>
                                     
                                    <input type="button" class="btn btn-primary btn-small" id="btnEventAccessSave" value="Save" style="display:none;"/>
                                </td>
                            </tr>
                        </table>
           
          
                    </div>
                </div>

            </div>
         <div id="uploadUsersModal" class="dialogDiv" title="Upload Users">
                <iframe id="uploadAttachment" src="<%=ResolveUrl("~/Tools/UploadUserFile") %>"
                    style="width: 270px; height:200px; border: 0px; margin: 10px 0px 0px 00px;" class="NoOverflow">
                </iframe>
                <input type="button" class="ContentInputButton" style="visibility: hidden" id="btnUsersPopupClose"
                    value="Close" />
            </div>
    </div>
</asp:Content>
