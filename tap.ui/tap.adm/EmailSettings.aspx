﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Tabs-Edit.aspx.cs"
    Inherits="tap.ui.adm.Custom_Tabs_Edit" MasterPageFile="~/tap.mst/Master.Master" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailSettings.aspx.cs"
    Inherits="tap.ui.adm.EmailSettings" MasterPageFile="~/tap.mst/Admin.Master" %>


<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>


    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.custom.css") %>"
        rel="stylesheet" type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    
   
    <script type="text/javascript" lang="javascript">

        $(document).ready(function () {
            $('#menu').hide();
        });

    </script>
    <!-- For Sorting and Dragging plugin files -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.email.js") %>" type="text/javascript"></script>

    <div class="siteContent adminSiteContent">

        <div id="divTestEmail" class="dialogDiv hidden" title="Test Email">
            <br />
            <div class="MarginTop10">
                <label id="lblSendEmailTo" style="margin-left:-7px" class="ContentCustomLabel">
                    Send Test Email To 
                </label>
                  <img id="imgLoader" style="display:none;margin-top:-18px" src="<%=ResolveUrl("~/images/ajax-loader.gif")%>" />
                                      

            </div>
            <div class="MarginTop10">
                <input id="txtSendEmailTo" style="margin-top:3px"   class="Width300 fieldFontFamily" name="name"
                    autofocus="" type="text">
            </div>
                       
            <br />
           
            <div class="MarginTop10"  style="text-align:center">
                 <input type="button" class="btn btn-primary btn-small" value="Send" id="btnSend" />
            </div>
           


        </div>
        <div id="adminTabs">          
           
            <div class="ClearAll ContentBorder">
                <div class="blueHeader">
                    Email Settings
                </div>
                <div class="ContentWidth MarginRight20">
                    <div>
                        <label id="TabErrorMsg">
                        </label>
                    </div>
                    <div class="MarginTop10">
                        <label id="lblEmailServer" class="ContentCustomLabel" >
                            Email Server <span style="color: Red" id="spnEmailServer">*</span></label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtServer" class="Width300 fieldFontFamily" name="name" 
                            autofocus="" type="text">
                    </div>
                    <div class="MarginTop10">
                        <label id="lblEmailAddress" class="ContentCustomLabel" >
                            Email Address (To be used to send emails from) <span style="color: Red" id="spnEmailAddress">*</span> </label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtEmailAddress" class="Width300 fieldFontFamily" name="name" 
                            autofocus="" type="text">
                       
                    </div>
                    <br />
                    <div class="MarginTop10">
                        <label id="lblPassword " class="ContentCustomLabel" >
                            Password  <span style="color: Red" id="spnPassword">*</span></label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtPassword" type="password" class="Width300 fieldFontFamily" name="name" 
                            autofocus="">
                    </div>

                     <div class="MarginTop10">
                        <label id="lblConfirmPassword" class="ContentCustomLabel" >
                            Confirm Password  <span style="color: Red" id="spnConfirmPassword">*</span></label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtConfirmPassword" type="password" class="Width300 fieldFontFamily" name="name" 
                            autofocus="" >
                    </div>

                       <div class="MarginTop10">
                        <label id="lblPort" class="ContentCustomLabel" >
                            Port <span style="color: Red" id="spnPort">*</span> </label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtPort" class="Width300 fieldFontFamily" name="name" 
                            autofocus="" type="text">
                    </div>
                   
                   
                    <div class="MarginTop30" style="padding-left: 270px;" >
                        <input type="button" class="btn btn-primary btn-small" <%--style="width:80px"--%> value="Test" id="btnTest" />
                        <input type="button" class="btn btn-primary btn-small" <%--style="width:80px"--%> value="Apply Changes" id="btnSave" />
                       
                    </div>

                </div>
            </div>

        
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>      



    </div>
    

</asp:Content>
