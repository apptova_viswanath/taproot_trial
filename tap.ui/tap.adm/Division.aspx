﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.master" AutoEventWireup="true" CodeBehind="Division.aspx.cs" Inherits="tap.adm.Division" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Adminbody" runat="server">
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.division.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />

            <script id="ForgotPasswordJS" type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.policy/ValidPasswordPolicy.js") %>">
        {
            "txtPassword":"body_Adminbody_txtPassword", 
            "txtConfirmPassword":"body_Adminbody_txtConfirmPassword"

        }
</script>
    <!--[if IE]>
        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/StepProgressIE.css") %>" rel="stylesheet" type="text/css" />
    <![endif]-->

    <script src="<%=ResolveUrl("~/Scripts/tap.js.attachment/js.srt.atch.js") %>" type="text/javascript"></script>
    
        <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.validate/PasswordPolicy.css") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #divUploadButton {
            width: 200px;
            margin-left: 250px;
        }

        .MarginTop10 a {
            text-decoration: underline;
            color: blue;
            cursor: pointer;
        }

        #divLogoPreviewDialog {
            width: 50%;
            height: 50%;
        }

        #body_adminbody_imgBannerLogoPreview, #body_adminbody_imgCompanyLogoPreview, #body_adminbody_imgReportLogoPreview {
            height: 70%;
            width: 80%;
            display: none;
        }

        fieldset {
            border: 1px solid gray;
            width: 90%;
        }

        legend {
            display: block;
            padding: 0;
            font-size: 14px;
            line-height: 36px;
            color: #333333;
            border: 0;
            width: auto;
            margin-left: 50px;
            padding-left: 5px;
            padding-right: 5px;
            font-family: Tahoma,sans-serif, Arial;
            font: 11pt "Lucida Sans Unicode", "Trebuchet Ms",Helvetica,Arial;
            margin-bottom: 0px;
        }

        #body_adminbody_companyAddress {
            width: 470px;
        }

        .MarginTop10 label {
            min-width: 180px;
        }

        .MarginTop10 textarea {
            width: 300px;
        }
        /*#divDivisionSubscriptionEnd, #divDivisionSubscriptionStrt {
            display:none;
        }*/

         /*#pswd_info {
	position:absolute;	
    margin-left:17%;
	width:250px;
	padding:15px;
	background:#fefefe;
	font-size:.875em;
	border-radius:5px;
	box-shadow:0 1px 3px #ccc;
	border:1px solid #ddd;
}
#pswd_info::before {
	content: "\25B2";
	position:absolute;
	top:-12px;
	left:45%;
	font-size:14px;
	line-height:14px;
	color:#ddd;
	text-shadow:none;
	display:block;
}
.invalid {
	background:url('../../../Images/Delete.png') no-repeat 0 50%;
	padding-left:22px;
	line-height:24px;
	color:#ec3f41;
}
.valid {
	background:url('../../../Images/green-check.png') no-repeat 0 50%;
	padding-left:22px;
	line-height:24px;
	color:#3a7d34;
}
#pswd_info {
	display:none;
}*/
    </style>

    <input type="hidden" id="hidden_CheckADSecurity" value="0" runat="server" />
    <input type="hidden" id="hidden_DivisionId" value="0" runat="server" />
    <input type="hidden" id="hidden_LocationListId" value="0" runat="server" />
    <input type="hidden" id="hidden_ClassListId" value="0" runat="server" />
    <input type="hidden" id="hidden_Password" value="0" runat="server" />

    <div class="siteContent adminSiteContent" id="divisionSiteContent">
        <div class="ContentWidth ClearAll">
            <div id="divforProgressbar" class="ClearAll" style="margin: 0 auto; width: 80%; margin-bottom: 20px; font-size: 14px;">
                <ol class="progtrckr" data-progtrckr-steps="2">
                    <li id="liCreateDivsion" class="progtrckr-todo">Create Division</li>
                    <li id="liDivisionCreateUsers" class="progtrckr-todo">Create User</li>
                </ol>
            </div>
            <div id="newCreation" class="newdivisionmessage">
                You are about to create a new division. 
                 Please enter some basic division information and create an admin user account for this division.
            </div>

            <div id="divMainDivisionUser" style="padding-left: 195px;">
                <div id="divNewDivision">
                    <div class="ClearAll" style="margin-top: 5%;">
                        <div style="text-align: left; display: none;">
                            <a id="ancBackDivisionsList">Back to Division List</a>&nbsp;
                        </div>

                        <div class="MarginTop10">
                            <label id="Label5" class="ContentUserLabel Width220">
                                Name:<span id="spnCompanyName" style="color: Red">*</span>
                            </label>

                            <input id="divisionName" type="text" class="Width300" runat="server" autofocus="" />
                        </div>
                        <div class="MarginTop10" id="divParentCompany" style="display: none; height: 25px;">
                            <label id="lblLocation" class="ContentUserLabel">
                                Parent Company<span id="Span2" style=""></span>

                            </label>

                            <label id="lblParentCompanyData" runat="server" clientidmode="Static" class="EventImageLabel" style="padding-left: 0px;">
                            </label>

                            <div class="FloatLeft Width120">
                                <img alt="OpenLink" class="HandCursor" id="lnkParentCompSelect" src="<%=ResolveUrl("../Images/OpenLink.png") %>" tabindex="4" />
                            </div>
                        </div>
                        <div class="MarginTop10">
                            <label id="" class="ContentUserLabel Width220">
                                Phone Number:
                            </label>
                            <input id="divisionPhoneNumber" type="text" class="Width300" runat="server" />
                        </div>
                        <div class="MarginTop10">
                            <label id="Label6" class="ContentUserLabel Width180">
                                Email:
                            </label>
                            <input id="divisionEmail" type="text" class="Width300" runat="server" />

                        </div>
                        <div class="MarginTop10">
                            <label id="Label3" class="ContentUserLabel Width180">
                                Website:
                            </label>
                            <input id="divisionWebsite" type="text" class="Width300" runat="server" />
                        </div>
                        <div class="MarginTop10">
                            <label id="Label4" class="ContentUserLabel Width180">
                                Address:
                            </label>
                            <textarea id="divisionAddress" runat="server"></textarea>
                        </div>
                        <div class="MarginTop10 hide" id="divDivisionSubscriptionStrt">
                            <label id="Label14" class="ContentUserLabel Width180">
                                Subscription Start :<span id="Span4" style="color: Red">*</span>
                            </label>
                            <div class="MarginTop10">
                                <input id="txtSubscriptionStart" class="Width300" type="text" />
                               
                            </div>

                        </div>
                        <div class="MarginTop10 hide" id="divDivisionSubscriptionEnd">
                            <label id="Label15" class="ContentUserLabel Width180">
                                Subscription End :<span id="Span5" style="color: Red">*</span>
                            </label>
                            <div class="MarginTop10">
                                <input id="txtSubscriptionEnd" class="Width300" type="text" />
                              
                            </div>

                        </div>
                    </div>

                </div>
                <div id="divCreateUsers" style="display: none; margin-top: 10px;">

                    <div class="divLabel ClearAll">
                        <label id="lblName" class="ContentUserLabel">First Name<span id="spnFirstName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtFirstName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" runat="server" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label7" class="ContentUserLabel">Last Name<span id="spnLastName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtLastName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" runat="server" />
                    </div>

                    <div class="divLabel ClearAll">
                        <label id="Label8" class="ContentUserLabel">Email<span id="spnEmail" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtEmail" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" runat="server" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label9" class="ContentUserLabel">Phone</label>
                    </div>
                    <div class="divLabel">
                        <input id="txtPhone" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" runat="server" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label10" class="ContentUserLabel">User Name<span id="spnUserName" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtUserName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" runat="server" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label11" class="ContentUserLabel">Password<span id="spanPassword" style="color: Red">*</span></label>
                    </div>
                    <div class="divLabel">
                        <input id="txtPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" runat="server" />
                        <%--<a href="" id="userPasswordPolicy" style="display:none;">Password Policy</a>--%>
                          <div id="pswd_info" style="margin-left:210px">
        <h4>Password must meet the following requirements:</h4><br />
        <ul>
            
            <li id="lowerCase" class="invalid">At least <strong> <span id="spnCountLowerCase">1</span> lowercase letter<span id="lowerS" >s</span></strong></li>
            <li id="upperCase" class="invalid">At least <strong><span id="spnCountUpperrCase">1</span> uppercase letter<span id="upperS" >s</span></strong></li>
            <li id="number" class="invalid">At least <strong><span id="spnCountNumer">1</span> number<span id="numberS" >s</span></strong></li>
            <li id="length" class="invalid">Be at least <strong><span id="spnLength">6</span> character<span id="minS" >s</span></strong></li>
            <li id="maxCharacter" class="invalid">No longer than <strong><span id="spnMaxCharacterCount">6</span> character<span id="maxS" >s</span></strong></li>
            <li id="specialCharacter" class="invalid">At least <strong><span id="spnSpecialCharacterCount">6</span> special character<span id="specialS" >s</span></strong></li>
        </ul>
    </div>
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="Label12" class="ContentUserLabel">Confirm Password<span id="span6" style="color: Red">*</span></label></div>
                    <div class="divLabel">
                        <input id="txtConfirmPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" runat="server" />
                    </div>
                    <div class="divLabel ClearAll">
                        <label id="lblStatus" class="ContentUserLabel">Active<span id="Span1" style="color: Red"></span></label>

                    </div>
                    <div class="divLabel">
                        <input type="checkbox" checked="checked" id="chkActive" style="margin-left: 20px;" disabled="disabled" />

                    </div>
                    <div class="divLabel ClearAll" style="display: none;">
                        <label id="Label13" class="ContentUserLabel">Admin<span id="Span3" style="color: Red"></span></label>

                    </div>
                    <div class="divLabel" style="display: none;">
                        <input type="checkbox" runat="server" id="Checkbox1" style="margin-left: 20px;" />

                    </div>
                </div>
                <div class="ClearAll divisionContinueMsg" id="toContinueText">
                    To continue, click Next
                </div>
                <div class="MarginTop10 divisionPrevNextBtn">
                    <input type="button" id="divisionPreviousBtn" class="btn btn-primary btn-small divisionBackBtn" value="< Back" />
                    <input type="button" id="divisionNextBtn" class="btn btn-primary btn-small" value="Next >" />
                    <img src="<%=ResolveUrl("~/images/progress2.gif")%>" id="divisionWorkProgressImage" />
                    <input type="button" id="SaveDivisionDetails" class="btn btn-primary btn-small" value="Create" />
                </div>
            </div>
        </div>
        <div class="demo hidden">
            <div id="divUserPasswordPolicy" title="Password Policy">
                
                <div class="CenterTree">
                   <%--<h4>Password Policy</h4><br />--%>
                    <div id="divUserPasswordPolicyContent">


                    </div>
                </div>
                <div class="MarginTop30" style="text-align:center;">
                    <input type="button" class="btn btn-primary btn-small" value="Cancel" id="btnUserPswPolicyCancel" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
