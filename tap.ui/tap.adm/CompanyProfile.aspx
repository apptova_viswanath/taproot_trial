﻿<%--<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/tap.mst/Master.Master" CodeBehind="CompanyProfile.aspx.cs" Inherits="tap.adm.CompanyProfile" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/tap.mst/Admin.master" CodeBehind="CompanyProfile.aspx.cs" Inherits="tap.adm.CompanyProfile" %>

<asp:Content ContentPlaceHolderID="Adminbody" runat="server">
    <%--<script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>--%>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.org/js.comp.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.site.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
            <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>
         <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>
    <style type="text/css">
        #divUploadButton {
            width: 200px;
            margin-left: 250px;
        }

        .MarginTop10 a {
            text-decoration: underline;
            color: blue;
            cursor: pointer;
        }

        #divLogoPreviewDialog {
            width: 50%;
            height: 50%;
        }

        #body_imgBannerLogoPreview, #body_imgCompanyLogoPreview, #body_imgReportLogoPreview {
            height: 70%;
            width: 80%;
            display: none;
        }

        fieldset {
            border: 1px solid gray;
            width: 90%;
        }

        legend {
            display: block;
            padding: 0;
            font-size: 14px;
            line-height: 36px;
            color: #333333;
            border: 0;
            width: auto;
            margin-left: 10px;
            padding-left: 5px;
            padding-right: 5px;
            margin-bottom: 0px;
            font-size: 15px;
            font-weight: bold;
        }

        #body_companyAddress {
            width: 470px;
         
        }

        .MarginTop10 label {
            min-width: 220px;
        }

        .MarginTop10 textarea {
            width: 300px;
            margin-left:0px;
        }

        .ContentMargin {
            margin: 20px 20%;
            float: left;
            min-width: 160px;
        }
         #ui-datepicker-div {
            font-size: 12px;
        }
    </style>
    <input type="hidden" id="hidden_CheckADSecurity" value="0" runat="server" />
    <input type="hidden" id="hidden_DivisionId" value="0" runat="server" />

    <div class="siteContent adminSiteContent">
        <div class="ContentWidth ClearAll">
            <div class="MarginTop10" style="width: 100%; font-weight: bold;">
                    <label id="lblHeadCompDetails" class="ContentUserLabel blue" style="color: #4779ae;">
                        Company Details
                    </label>
                </div>
            <br />
            <div class="MarginTop10">
                <label id="lblcompanyName" class="ContentUserLabel Width220">
                    Organization Name<span id="spnCompanyName" style="color: Red">*</span>
                </label>
                <input id="companyName"  onKeyPress="if (event.which == 13) return false;" type="text" class="Width300 fieldFontFamily" runat="server" autofocus="" />
            </div>
            <div class="MarginTop10">
                <label id="" class="ContentUserLabel Width220">
                    Phone Number
                </label>
                <input id="companyPhoneNumber" onKeyPress="if (event.which == 13) return false;" type="text" class="Width300 fieldFontFamily" runat="server" />
            </div>
            <div class="MarginTop10">
                <label id="Label6" class="ContentUserLabel Width180">
                    Email
                </label>
                <input id="companyEmail" onKeyPress="if (event.which == 13) return false;" type="text" class="Width300 fieldFontFamily" runat="server" />
            </div>
            <div class="MarginTop10">
                <label id="Label3" class="ContentUserLabel Width180">
                    Website
                </label>
                <input id="companyWebsite" onKeyPress="if (event.which == 13) return false;" type="text" class="Width300 fieldFontFamily" runat="server" />
            </div>
            <div class="MarginTop10">
                <label id="Label4" class="ContentUserLabel Width180">
                    Address
                </label>
                <textarea id="companyAddress" runat="server" class="fieldFontFamily"></textarea>
            </div>
           
            <div class=" ClearAll">
                <div class="MarginTop10" style="width: 100%; font-weight: bold;">
                    <label id="Label9" class="ContentUserLabel blue" style="color: #4779ae;">
                        Site Settings
                    </label>
                </div>
                <br />
                <div class="MarginTop10">
                    <label id="Label5" class="ContentUserLabel Width180 ">
                        Date Format 
                    </label>
                    <select id="cmbDateFormat" class="WidthP15 fieldFontFamily notranslate" runat="server" autofocus="">
                        <option value="">-- Select --</option>
                        <option value="dd/MM/yyyy">dd/MM/yyyy</option>
                        <option value="MM/dd/yyyy">MM/dd/yyyy</option>
                        <option value="MMM dd yyyy">MMM dd yyyy</option>
                       <%-- <option value="dd-MM-yy">dd-MM-yy</option>
                        <option value="dd-M-yy">dd-M-yy</option>--%>
                            
                     </select>
                    <span id="spanExampleDateFormat" >Example:
                        <label id="DateFormatExample"></label>
                    </span>
                </div>
                <div class="MarginTop10">
                    <label id="Label7" class="ContentUserLabel Width180">
                        Time Format 
                    </label>
                    <%--<input id="chkTimeFormat" type="checkbox" runat="server" class="fieldFontFamily" />--%>
                     <select id="cmdTimefFormat" class="WidthP15 fieldFontFamily notranslate" runat="server" autofocus="">
                        <option value="0">-- Select --</option>
                        <option value="12">12 hour</option>
                        <option value="24">24 hour</option>
                    </select>
                </div>
                <div class="MarginTop10" id="DivTimeout">
                    <label id="Label8" class="ContentUserLabel Width180">
                        Application Time Out 
                    </label>
                    <div class="MarginTop10">
                        <input id="txtTimeOut" onKeyPress="if (event.which == 13) return false;" class="fieldFontFamily" type="text" runat="server" />
                    </div>

                </div>
           <br />


                
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
          
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">

                    <ContentTemplate>
                        <div id="divSubscription" runat="server" clientidmode="Static" class=" ClearAll">

                            <div class="MarginTop10 ClearAll" style="width: 100%; font-weight: bold; margin-bottom: 10px;">
                                <label id="Label9" class="ContentUserLabel blue" style="color: #4779ae;">
                                    Subscription Details
                                </label>
                            </div>
                            <br />



                            <div class="MarginTop10 ClearAll" id="divSubscriptionStartDate">
                                <label id="Label10" class="ContentUserLabel Width180">
                                    Subscription Start<span id="Span1" style="color: Red">*</span>
                                </label>
                                <div class="MarginTop10">
                                    <input id="txtSubscriptionStart" onkeypress="if (event.which == 13) return false;" class="" type="text" runat="server" />
                                    <%--<label id="lblstartDate" class="spnSubsEndDate">(d M yyyy)  </label>--%>
                                </div>
                            </div>
                            <div class="MarginTop10  ClearAll" id="divSubscriptionStartEnd">
                                <label id="Label11" class="ContentUserLabel Width180">
                                    Subscription End<span id="Span2" style="color: Red">*</span>
                                </label>
                                <div class="MarginTop10">
                                    <input id="txtSubscriptionEnd" onkeypress="if (event.which == 13) return false;" class="mydatepickerclass" type="text" runat="server" />
                                    <%--<label id="lblEndDate" class="spnSubsEndDate">(d M yyyy)  </label>--%>
                                </div>
                            </div>

                        </div>




                        <asp:Button ID="btnSetdateformat" AutoPostback="false" ClientIDMode="Static" runat="server" Text="" Style="display: none" Height="0px" Width="0px" OnClick="btnSetdateformat_Click" />
                    </ContentTemplate>

                </asp:UpdatePanel>
           
                
                          <div id="divPolicySettings">
                    <div class="MarginTop10">
                        <label id="Label13" class="ContentUserLabel Width180 ">
                            Password Policy
                        </label>
                        <select id="cmbPasswordPolicy" class="WidthP15 fieldFontFamily" runat="server" autofocus="">
                            <option value="0">-- Select --</option>
                            <option value="1">Strong</option>
                            <option value="2">Strict</option>
                        </select>
                         <span id="span3">
                            <a href="javascript:void(0)" id="PswPolicyHelpIcon"><img src="<%=ResolveUrl("~/Images/Info.png") %>" /></a>
                        </span>
                    </div>
                    <div class="MarginTop10">
                        <label id="Label15" class="ContentUserLabel Width180 ">
                            Password Expiration 
                        </label>
                        <select id="cmbPasswordExpiration" class="WidthP15 fieldFontFamily" runat="server" autofocus="">
                            <option value="0">-- Select --</option>
                            <option value="30">30 days</option>
                            <option value="60">60 days</option>
                            <option value="90">90 days</option>
                            <option value="180">6 months</option>
                            <option value="365">1 year</option>
                            <option value="0">Never</option>
                        </select>
                    </div>
                </div>


                <div class="MarginTop10" id="divSubscriptionNotify" style="display: none;">
                    <label id="Label12" class="ContentUserLabel Width180">
                        Subscription  
                    </label>
                    <div class="MarginTop10 contentSubscription">
                        Your subscription is active until 
                        <label id="lblSubscriptionDate" runat="server" style="color: blue;"></label>
                    </div>

                </div>

      
                
                
                
                
                <div class="ContentMargin" style="display: none;">
                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnSave" value="Save" />
                </div>
            </div>


            <div class="MarginTop10" style="padding-left: 270px;">
                <input type="button" class="btn btn-primary btn-small" value="Apply Changes" id="saveCompanyDetails" />


            </div>
        </div>

        <div class="ContentWidth ClearAll">
            <fieldset>
                <legend class="blue" style="color: #4779ae;">Upload company related logos</legend>
                <div class="MarginTop10">
                    <label id="Label1" class="ContentUserLabel">
                        Banner Logo:
                    </label>
                    <div class="MarginTop10">
                        <asp:FileUpload ID="FileUploadBannerLogo" CssClass="WidthP40 fieldFontFamily" runat="server" />

                        <a href="" id="BannerLogoChange" runat="server" class="compLogoChangePreview">Change</a>
                        <a href="" id="BannerLogoCancel" runat="server" class="compLogoChangePreview">Cancel</a>
                        <asp:Button onkeypress="return event.keyCode != 13" ID="removeBannerLogo" runat="server" CssClass="compRemoveBtn" OnClick="removeBannerLogo_Click" Text="Remove" />
                        <a href="" id="bannerLogoPreview" runat="server" class="compLogoPreviewLink">Preview</a>
                        <br />
                        <asp:RegularExpressionValidator ID="UploadBannerLogoValidator" CssClass="marginleft250" runat="server" ControlToValidate="FileUploadBannerLogo" ForeColor="Red" Font-Bold="true" EnableClientScript="true"
                            ErrorMessage="Please select image file [jpg, gif, bmp, png]."
                            ValidationExpression=".*(\.jpg|\.JPG|\.gif|\.GIF|\.png|\.PNG|\.tif|\.TIF|\.raw|\.RAW|\.bmp|\.BMP|\.jpeg|\.JPEG)$"></asp:RegularExpressionValidator>

                    </div>
                </div>

                <div class="MarginTop10">
                    <label id="Label2" class="ContentUserLabel">
                        Report Logo:
                    </label>

                    <div class="MarginTop10">
                        <asp:FileUpload ID="FileUploadReportLogo" CssClass="WidthP40 fieldFontFamily" runat="server" />


                        <a href="" id="ReportLogoChange" runat="server" class="compLogoChangePreview">Change</a>
                        <a href="" id="ReportLogoCancel" runat="server" class="compLogoChangePreview">Cancel</a>
                        <asp:Button onkeypress="return event.keyCode != 13" ID="removeReportLogo" runat="server" CssClass="compRemoveBtn" OnClick="removeReportLogo_Click" Text="Remove" />
                        <a href="" id="reportLogoPreview" runat="server" class="compLogoPreviewLink">Preview</a>
                        <br />
                        <asp:RegularExpressionValidator ID="UploadReportLogoValidator" runat="server" CssClass="marginleft250" ControlToValidate="FileUploadReportLogo" ForeColor="Red" Font-Bold="true" EnableClientScript="true"
                            ErrorMessage="Please select image file [jpg, gif, bmp, png]."
                            ValidationExpression=".*(\.jpg|\.JPG|\.gif|\.GIF|\.png|\.PNG|\.tif|\.TIF|\.raw|\.RAW|\.bmp|\.BMP|\.jpeg|\.JPEG)$"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="MarginTop20 ClearAll">
                    <div id="divUploadButton">
                        <asp:Button onkeypress="return event.keyCode != 13" ID="btnUpload" runat="server" CssClass="btn btn-primary btn-small PaddingLeft100" OnClick="btnUpload_Click" Text="Apply changes" />
                    </div>
                </div>
                <br />
            </fieldset>
        </div>


        <div class="demo" style="display: none;">
            <div id="divLogoPreviewDialog" title="Logo Preview">
                <div style="text-align: center;">
                    <asp:Image ID="imgBannerLogoPreview" runat="server" />
                   
                    <asp:Image ID="imgReportLogoPreview" runat="server" />
                </div>

            </div>
        </div>
         <div class="demo hidden">
            <div id="divPswPolicyDialog" title="Password Policy">
                <div>
                    <label id="ErrClassification">
                    </label>
                </div>
                <div class="CenterTree">
                    <h4 class="modal-title">Strong Password</h4>
                        Must be 6 characters in length<br />
                        Contain at least one UPPERCASE letter<br />
                        Contain at least one LOWERCASE letter <br />  
                        Contain at least one numbers (0-9)<br />
                        Must NOT include any spaces<br /><br />

                    <h4 class="modal-title">Strict Password</h4>
                        Must be 15 to 30 characters in length<br />
                        Contain at least two UPPERCASE letters<br />
                        Contain at least two lowercase letters<br />
                        Contain at least two numbers (0-9)<br />
                        Contain at least two of the following special characters:<br />
                        # (pound or number sign)<br />
                        @ (at sign)<br />
                        $ (dollar sign)<br />
                        = (equal sign)<br />
                        + (plus sign)<br />
                        % (percent sign)<br />
                        ^ (caret)<br />
                        ! (exclamation)<br />
                        * (asterisk)<br />
                        _ (underline/underscore)<br />
                        Must NOT include any spaces<br />
 
                         
                </div>
                <div class="MarginTop10" style="text-align:center;">
                    <input type="button" class="btn btn-primary btn-small" value="Cancel" id="btnPswPolicyPopUpCancel" />
                </div>
            </div>
        </div>

    </div>
</asp:Content>
