﻿using System;
using System.Globalization;
using System.Web.UI;

namespace tap.ui.usr
{
    public partial class Report_Print_Data : System.Web.UI.Page
    {
     

        protected void Page_Load(object sender, EventArgs e)
        {
            //if session expires then close the Report Data popup page and redirect to Login page.
            if ((Session.Count == 0) || (Convert.ToString(Session["CompanyId"]) == string.Empty))
            {
                Response.Write("<script>window.close();</" + "script>");
                //refresh parent page to get saved report
                Response.Write("<script>window.opener.location.reload(true);</" + "script>");
                Response.End();              
            }

            //added to get CompanyId and UserId
            hdnCompanyId.Value = Convert.ToString(Session["CompanyId"]);
            hdnUserId.Value = Convert.ToString(Session["UserId"]);
            hdnDivisionId.Value = Convert.ToString(Session["DivisionId"]);
         
            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            if (Session["EventId"] != null)
                hdnEventId.Value = _cryptography.Encrypt(Convert.ToString(Session["EventId"]));
            else
                hdnEventId.Value = "0";

            dom.gen.Security security = new dom.gen.Security();
            int userId = Convert.ToInt32(hdnUserId.Value);

            string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();                    

            if (!IsPostBack)
            {
               // spnCompanyName.InnerHtml = SessionService.CompanyName;
                spnCompanyName.InnerHtml = Convert.ToString(Session["CompanyName"]);
                spnCreatedDate.InnerHtml = DateTime.Now.Date.ToString(Session["DateFormat"].ToString(), CultureInfo.InvariantCulture);
            }

            int eventId = 0;

            if (Page.RouteData.Values["EventId"] != null && !string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()))
            {
                string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
                 eventId = int.Parse(strEventId);
            }

            //Page Title
            tap.dom.usr.Event _event = new dom.usr.Event();
            if (Page.RouteData.Values["EventId"] != null && !string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()))
            {
                string EventName = _event.getEventName(Page.RouteData.Values["EventId"].ToString());
                Page.Title = "Report" + ((EventName != "") ? " - " + EventName : "");
               
                //pass eventid,userid and cookies to check User have autorise to access this page
                if (userId != 0 && eventId!=0)
                    security.CheckEventAccess(eventId, userId, Response);
            }
            else if (Page.RouteData.Values["ReportId"] != null && !string.IsNullOrEmpty(Page.RouteData.Values["ReportId"].ToString()))
            {
                int reportId = Convert.ToInt32(Page.RouteData.Values["ReportId"]);
                int reportType = Convert.ToInt32(Page.RouteData.Values["ReportType"]);
                 //pass eventid,userid and cookies to check User have autorise to access this page
                if (userId != 0 && reportId>0)
                    security.CheckReportAccess(reportId, userId, Convert.ToInt32(hdnCompanyId.Value), reportType,eventId, Response);
            }
           
        }
    }
}