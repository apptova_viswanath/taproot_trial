﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="Attachment.aspx.cs" Inherits="tap.ui.usr.Attachment" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.attachment/js.atch.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <!--move dialogDiv to content.css-->

      <!-- Notification plugin (modified plugin files) ------------->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/js.noty/jquery.notify.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/themes/default.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/layouts/center.js") %>" type="text/javascript"></script>
    <style>
        .jstree-default.jstree-focused {
            background: #ffffff;
        }

        .jstree a {
            color: #355779;
        }

        .ui-widget-content {
            background: url("/images/ui-bg_flat_75_ffffff_40x100.png") repeat-x scroll 50% 50% #FFFFFF;
            border: 0px solid #AAAAAA;
            color: #222222;
        }

        .ui-widget {
            font-size: 12px;
        }

        .dialogDiv {
            display: none;
        }

        .userAttachmentTreeview-margin {
            margin-top: 100px;
        }

        #UserAttachmentTreeview {
           
            width: 80%;
        }
        #divNoFolderMsg {
            float:left;
            padding-left:5px;
            margin-top:10px;
        }
    </style>

    <div class="siteContent NoOverflow">
        <div id="tabs">
            <uc:ucEvent ID="ucevent" runat="server" />
            <div id="divAttachmentTab" class="ContentWidth ClearAll">
                <div class="hidden" id="divNoFolderMsg">
                    There are no attachment folders set up.
                </div>

                <div id="AttachmentTreeviewBox" class="PaddingLeft20">
                    <div>
                        <label id="lblFolderItems" class="ContentLabel">
                        </label>
                    </div>
                    <div id="UserAttachmentTreeview" class="userAttachmentTreeview-margin notranslate">
                    </div>
                </div>

                <div class="FloatLeft PaddingLeft50 MarginTop10">
                    <a class="btn btn-primary btn-small" href="" id="btnUserUpload">Upload File</a>
                    <a class="btn btn-primary btn-small" href="" id="btnUserDelete">Delete File</a>
                </div>
            </div>
        </div>

        <div class="tab-next-prev">
            <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
        </div>
        <div id="uploadAttachmentModal" class="dialogDiv" title="Upload Files">
            <iframe id="uploadAttachment" src="<%=ResolveUrl("~/Tools/UploadFile/EventId/Folder-{FolderId}") %>"
                style="width: 270px; border: 0px; margin: 10px 0px 0px 00px; min-height: 200px;" class="NoOverflow"></iframe>
            <input type="button" class="ContentInputButton" style="visibility: hidden" id="btnAttachClose"
                value="Close" />
        </div>

    </div>
</asp:Content>
