﻿<%--<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true" CodeBehind="Custom-Manage-Report.aspx.cs" Inherits="tap.ui.usr.Custom_Manage_Report" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Admin.Master" AutoEventWireup="true" CodeBehind="Custom-Manage-Report.aspx.cs" Inherits="tap.ui.usr.Custom_Manage_Report" %>


<asp:Content ID="Content2" ContentPlaceHolderID="Adminbody" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <!--JQGrid Script -->
    <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>


    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>


    <div class="TapRooTContent">
        <!--Auto Search and Advanced Search -->
       <div class="NotificationListGrid">
            <div style="margin-bottom:10px;">
                <div id="divCreateNewReport" class="taskHeader">
                    <a id="lnkCreateNewReport" href="javascript:void(0)" class="MarginLeft20">Add Report</a>
                </div>
                <div>
                    <input type="text" class="span2 bootstrp1 " id="txtReportSearch" placeholder="Search Reports" title="Search Reports" autofocus />
                </div>
            </div>


            <div>

                <table id="reportGrid" class="scroll ClearAll">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="pageNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>
       </div>
    </div>
    <!--Page Script -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.mng.rpt.js") %>"></script>
</asp:Content>

