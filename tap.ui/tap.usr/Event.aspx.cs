﻿//---------------------------------------------------------------------------------------------
// File Name 	: Event.aspx.cs

// Date Created  : N/A

// Description   : Event Page Methods (Incident,Investigations,Audid and CAP Events)

// Javascript File Used : js.evt.js
//----------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;
using System.Web.Script.Serialization;
using tap.dat;
using tap.dom.gen;
using tap.dom.hlp;
using tap.dom.adm;
#endregion
namespace tap.usr.evt
{
    public partial class Event : System.Web.UI.Page
    {

        #region For Global Variables
        tap.dom.usr.Event _events = new dom.usr.Event();
        JavaScriptSerializer _js = new JavaScriptSerializer();
        tap.dat.EFEntity _db = new EFEntity();
        tap.dom.adm.SiteSettings _siteSettings = new dom.adm.SiteSettings();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.adm.Divisions _division = new dom.adm.Divisions();
        

        int _eventId;
        int _userId;
        int _module;

        bool _hasInvestigation = false;
        bool _hasCap = false;

        const string DATE_FORMAT = "01/01/1900";
        const string MM_DD_YYYY = "MM/dd/yyyy";
        const string DD_MM_YYYY = "dd/MM/yyyy";
        const string MMM_DD_YYYY = "MMM dd yyyy";

        string[] dynamicFieldIds;
        string[] dynamicFieldValues;
        string[] dynamicControlsNames;
        #endregion

        #region For Page_load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            _eventId = int.Parse(strEventId);
            _userId = Convert.ToInt32(Session["UserId"]);

            string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();

            //string securityUrl = String.Format(baseURL + "/Security/Access-Denied/");
            //dom.gen.Security security = new dom.gen.Security();

            ////pass eventid,userid and cookies to check User have authorise to access this page
            // if (_eventId != 0 && _userId != 0)
            //    security.CheckEventAccess(_eventId, _userId, Response, securityUrl);

          
            if (Page.RouteData.Values["Action"] != null)
            {
                if (Page.RouteData.Values["Action"].ToString().ToLower() == "new")
                    txtEventName.Focus();
            }

            string[] dynamicFieldIds = hdnDynamicFieldIds.Value.Split(',');
            string[] dynamicFieldValues = hdnDynamicFieldValues.Value.Split(',');
            string[] dynamicControlsNames = hdnDynamicControlNames.Value.Split(',');

            if (_userId != 0)
            {
                string listIds = _division.SystemListIdGet(Convert.ToInt32(Session["CompanyId"].ToString()));
                if (listIds != null && listIds.Contains(":"))
                {
                    string[] arrListId = listIds.Split(':');
                    if (arrListId != null)
                    {
                        hdnClassListId.Value = arrListId[0];
                        hdnLocationListId.Value = arrListId[1];
                    }
                }

                switch (Page.RouteData.Values["Module"].ToString().ToLower())
                {
                    case "incident":
                        _module = (int)dom.hlp.Enumeration.Modules.Incident;
                        ShowHideInvestigation(false);
                        ShowHideCAP(false);
                        break;

                    case "investigation":
                        _module = (int)dom.hlp.Enumeration.Modules.Investigation;
                        ShowHideInvestigation(true);
                        ShowHideCAP(false);
                        break;

                    case "audit":
                        _module = (int)dom.hlp.Enumeration.Modules.Audit;
                        ShowHideInvestigation(false);
                        ShowHideCAP(false);
                        lblDate.InnerHtml = "Audit Date" + "<span id='spnAudDate' style='color: Red' >*</span>";
                        lblEventTime.InnerHtml = "Audit Time";
                        break;

                    case "corrective-action-plan":
                    case "cap":
                    case "correctiveactionplan":
                    case "action-plan":
                    case "actionplan":
                        _module = (int)dom.hlp.Enumeration.Modules.ActionPlan;
                        ShowHideInvestigation(true);
                        ShowHideCAP(true);
                        break;
                }
            }

            if (!IsPostBack)
                PopulateEventData();

            #region Added for New investigation Modification
            string action = ((Page.RouteData.Values["Action"] != null) ? Page.RouteData.Values["Action"].ToString() : string.Empty);
            if (((_module == (int)tap.dom.hlp.Enumeration.Modules.Investigation && !_hasInvestigation)) && _eventId != 0)
            {//new-changes
                SaveEvent();
                RedirectTapRooTabPage(baseURL);
                
            }
            if ((_module == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan && !_hasCap && _eventId != 0))
            {
                SaveEvent();
                RedirectCAPage(baseURL);
            }
          
            #endregion
        }
        #endregion

        #region "Redirect to TapRooT Tab page"
        public void RedirectTapRooTabPage(string baseURL)
        {
            int taprootTabId = 0;
            int companyId = 0;
            if (Session["CompanyId"] != null)
                companyId = Convert.ToInt32(Session["CompanyId"].ToString());

            if (companyId > 0)
                taprootTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true && a.TabName.ToLower().Contains("taproot")).FirstOrDefault().TabID;

            // string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
            string url = String.Format(baseURL + "/Event/{0}/{1}/{2}", Page.RouteData.Values["Module"].ToString(), "TapRooT-" + taprootTabId, Page.RouteData.Values["EventId"].ToString());
            Response.Redirect(url);
        }
        #endregion

        #region "Redirect to CAP page"
        public void RedirectCAPage(string baseURL)
        {
            int actionPlanTabId = 0;
            int companyId = 0;
            if (Session["CompanyId"] != null)
                companyId = Convert.ToInt32(Session["CompanyId"].ToString());

            if (companyId > 0)
                actionPlanTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true && a.TabName.ToLower().Contains("Corrective Actions")).FirstOrDefault().TabID;

            // string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
            string url = String.Format(baseURL + "/Event/{0}/{1}/{2}", Page.RouteData.Values["Module"].ToString(), "CorrectiveActions-" + actionPlanTabId, Page.RouteData.Values["EventId"].ToString());
            Response.Redirect(url);
        }
        #endregion

        #region For PopulateEventData
        /// <summary>
        /// Populating the Event Data in the controls
        /// </summary>
        public void PopulateEventData()
        {
            string dateFormat = string.Empty;
            int companyId = 0;
                        
            if (Session["CompanyId"] != null)
            {
                dateFormat = Session["DateFormat"].ToString() ;
               // dateFormat = _siteSettings.GetDateFormat();
                companyId = Convert.ToInt32(Session["CompanyId"].ToString());
                dateFormat = _db.SiteSettings.Where(a => a.CompanyID == companyId).Select(a => a.DateFormat).FirstOrDefault();
            }


            bool? isTwelveHourFormat = _siteSettings.GetTimeFormat(companyId);

            TimeSpan tspan = new TimeSpan();

            //temporary solution for displaying default date
            if (dateFormat == string.Empty)
                dateFormat = MMM_DD_YYYY;


            bool hasDateFormat = (dateFormat == MM_DD_YYYY || dateFormat == DD_MM_YYYY || dateFormat == MMM_DD_YYYY);

            if (_eventId == 0)
                ddlStatus.Value = "1";

            var eventData = _events.GetEventDataByEventType(_eventId, (int)_module);
            _hasInvestigation = _events.HasInvestigation(_eventId);
            _hasCap = _events.HasCorrectiveAction(_eventId);

            if (!string.IsNullOrEmpty(eventData) && eventData != "[]")
            {
                var dslEventData = _js.Deserialize<dynamic>(eventData);

                if (!string.IsNullOrEmpty(dslEventData[0]["Name"]))
                    txtEventName.Value = dslEventData[0]["Name"];

                if (dslEventData[0]["EventDateTime"] != null)
                {
                    tap.dat.Incidents inc = _db.Incidents.FirstOrDefault(a => a.EventID == _eventId);//nw
                    var audit = _db.Audits.FirstOrDefault(a => a.EventID == _eventId);
                    DateTime dt = new DateTime();

                    if (inc != null)
                        dt = inc.IncidentDatetime.Value;
                    else if (audit != null)
                    {
                        dt = audit.AuditDatetime.Value;
                        lblDate.InnerHtml = "Audit Date";
                        ShowHideInvestigation(false);
                    }
                    txtDate.Value =  dt.ToString(dateFormat,CultureInfo.InvariantCulture);
                    //txtDate.Value = (dateFormat == string.Empty) ? dt.ToString(SessionService.CurrentCulture.DateTimeFormat.ShortDatePattern) : dt.ToString(dateFormat);

                    txtTime.Value = "";

                    try
                    {
                        if (dslEventData[0]["IncidentTime"] != null)
                        {
                            tspan = new TimeSpan(dslEventData[0]["IncidentTime"]["Ticks"]);

                            txtTime.Value = (isTwelveHourFormat == null) ? Convert.ToDateTime(tspan.ToString()).ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern)
                                                                     : (Convert.ToBoolean(isTwelveHourFormat)) ? Convert.ToDateTime(tspan.ToString()).ToString("hh:mm tt")
                                                                     : Convert.ToDateTime(tspan.ToString()).ToString("HH:mm");
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.LogException(ex);
                    }
                }
                if (!string.IsNullOrEmpty(dslEventData[0]["Location"]))
                {
                    Session["LocationId"] = dslEventData[0]["Location"];
                    hdnLocationId.Value = dslEventData[0]["Location"];
                }

                ddlStatus.Value = ((dslEventData[0]["Status"] != null) ? ((int)((tap.dom.hlp.Enumeration.EventStatus)dslEventData[0]["Status"])).ToString() : "0");

                if (dslEventData[0]["InvestigationDateTime"] != null)
                {
                    if (dslEventData[0]["InvestigationDateTime"].ToString() != string.Empty)
                    {
                        
                        var invDate = _db.Investigations.FirstOrDefault(a => a.EventID == _eventId).InvestigationDatetime;

                        txtInvDate.Value = invDate.Value.ToString(dateFormat, CultureInfo.InvariantCulture);  

                        try
                        {
                            if (dslEventData[0]["InvestigationDateTime"] != null)
                            {
                                tspan = new TimeSpan(dslEventData[0]["InvestigationTime"]["Ticks"]);

                                txtInvTime.Value = (isTwelveHourFormat == null) ? Convert.ToDateTime(tspan.ToString()).ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern)
                                                                         : (Convert.ToBoolean(isTwelveHourFormat)) ? Convert.ToDateTime(tspan.ToString()).ToString("hh:mm tt")
                                                                                                                  : Convert.ToDateTime(tspan.ToString()).ToString("HH:mm");
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.LogException(ex);
                        }
                    }
                }
                //changes done for Capdate and CapTime
                if (dslEventData[0]["CapDatetime"] != null)
                {
                    if (dslEventData[0]["CapDatetime"].ToString() != string.Empty)
                    {
                        
                        var capDate = _db.CorrectiveActionPlans.FirstOrDefault(a => a.EventID == _eventId).CapDatetime;

                        txtCapDate.Value = capDate.Value.ToString(dateFormat, CultureInfo.InvariantCulture);  

                        try
                        {

                            if (dslEventData[0]["capTime"] != null)
                            {
                                tspan = new TimeSpan(dslEventData[0]["capTime"]["Ticks"]);
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog.LogException(ex);
                        }
                    }
                }
                txtEventDescription.Value = ((!string.IsNullOrEmpty(dslEventData[0]["Description"])) ? dslEventData[0]["Description"] : string.Empty);
                txtEventFileNumber.Value = ((!string.IsNullOrEmpty(dslEventData[0]["FileNumber"])) ? dslEventData[0]["FileNumber"] : string.Empty);
                                                
                    

                #region For Classification

                var lstClassification = _db.FieldListValues.Where(a => a.EventId == _eventId);
                string classficationIds = string.Empty;
                string classficationTexts = string.Empty;
                tap.dom.adm.ListValues listValues = new tap.dom.adm.ListValues();
                tap.dat.ListValues classification = new tap.dat.ListValues();

                if (lstClassification != null)
                {
                    foreach (var a in lstClassification)
                    {
                        classification = _db.ListValues.Where(b => b.ListValueID == a.ValueId).FirstOrDefault();
                        if (classification != null)
                        {
                        classficationIds += a.ValueId + ",";
                        classficationTexts += listValues.FormattedFullpath(classification.FullPath) + ",";
                        }
                    }
                    if (classficationIds.LastIndexOf(",") > -1)
                    {
                        classficationIds = classficationIds.Remove(classficationIds.LastIndexOf(",")).ToString();
                        classficationTexts = classficationTexts.Remove(classficationTexts.LastIndexOf(",")).ToString().Replace(",", "<br/>");
                    }
                    
                    hdnClassificationId.Value = classficationIds;
                    lblEventClassificationData.InnerHtml = classficationTexts;
                    int locationID = Convert.ToInt32(hdnLocationId.Value);

                    tap.dat.ListValues location = _db.ListValues.Where(c => c.ListValueID == locationID).FirstOrDefault();
                    if (location != null)
                    {
                        lblEventLocationData.InnerHtml = listValues.FormattedFullpath(location.FullPath);
                    }
                    
                }
                #endregion

            }
        }
        #endregion

        #region For Show Hide  investigation DateTime
        /// <summary>
        /// ShowHideInvestigation
        /// </summary>
        /// <param name="hasInvestigation"></param>
        public void ShowHideInvestigation(bool hasInvestigation)
        {
            if (hasInvestigation)
            {
                lblinvDateTime.Visible = true;
                lblInvestigationTIme.Visible = true;
                txtInvDate.Visible = true;
                txtInvTime.Visible = true;
            }
            else
            {
                lblinvDateTime.Visible = false;
                lblInvestigationTIme.Visible = false;
                txtInvDate.Visible = false;
                txtInvTime.Visible = false;
            }

        }
        #endregion

        #region For Show Hide Cap Date Time
        /// <summary>
        /// ShowHideCAP
        /// </summary>
        /// <param name="hasCAP"></param>
        public void ShowHideCAP(bool hasCAP)
        {
            if (hasCAP)
            {
                lblCapDate.Visible = true;
                txtCapDate.Visible = true;
                lblCapTime.Visible = true;
                txtCapTime.Visible = true;
            }
            else
            {
                lblCapDate.Visible = false;
                txtCapDate.Visible = false;
                lblCapTime.Visible = false;
                txtCapTime.Visible = false;
            }

        }
        #endregion

        #region Time Format
        /// <summary>
        /// TimeFormat()
        /// Description-Its used to format the given time in 12 or 24 hour format.
        /// </summary>
        /// <param name="dtDate">Time to be formatted</param>
        /// <returns>Formatted time</returns>
        private string TimeFormat(DateTime? dtDate)
        {
            string sTime = string.Empty;

            try
            {
                //check not null for the date
                if (dtDate != null)
                {
                    //Get the time format from the site settings
                    bool blIsTwelveHrFormat = Convert.ToBoolean( _siteSettings.GetTimeFormat());

                    //make not nullabe                   
                    DateTime dtDateTime = dtDate ?? DateTime.Now;

                    //check the 12 hour format
                    if (blIsTwelveHrFormat)
                        sTime = dtDateTime.ToString("hh:mm tt");
                    else//for the 24 hour format
                        sTime = dtDateTime.ToString("HH:mm");

                }
            }
            catch (Exception ex)
            {
                //Error log
                ErrorLog.LogException(ex);
            }

            return sTime;
        }

        #endregion

        #region For SaveEvent
        /// <summary>
        /// save the Event 
        /// </summary>
        public void SaveEvent()
        {
            string companyID = Session["CompanyId"].ToString();
            string userID = Session["UserId"].ToString();
            string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
           
            tap.dom.usr.Event evnt = new dom.usr.Event();
            string[] subTabContent = evnt.SaveEvent(_eventId, txtEventName.Value, txtDate.Value, 
                                                    txtTime.Value, hdnLocationId.Value, hdnClassificationId.Value, 
                                                    Convert.ToInt32(ddlStatus.Value), userID, _module, txtInvDate.Value, txtInvTime.Value,
                                                    txtEventDescription.Value, txtEventFileNumber.Value, txtCapDate.Value, txtCapTime.Value, companyID, baseURL, dynamicFieldIds, dynamicFieldValues, dynamicControlsNames);

        }
        #endregion

    }
}
