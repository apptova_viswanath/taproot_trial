﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorrectiveActionTab.aspx.cs"
    Inherits="tap.ui.usr.CorrectiveActionTab" MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucActionPlan.ascx" TagPrefix="uc" TagName="ucActionPlan" %>


<asp:Content ID="taprootTabContent" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.tab.correctiveaction.js") %>" type="text/javascript"> </script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />

    <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';         
    </script>

    <div class="siteContent">

        <div id="tabs">

            <uc:ucEvent ID="ucEventTabs" runat="server" />
            <asp:HiddenField ID="hdnStatusValue" Value="false" runat="server" ClientIDMode="Static" />

            <div id="divTabCorrectiveAction" class="ContentWidth ClearAll marginTop50">

                <div>
                    If you are performing a TapRooT® investigation, you should CREATE your corrective
                    actions from the Investigation Data > Fix tab.
                    <br />
                    If you create a corrective action manually from here, you will not be able to use
                    SMARTER.
                    <br />
                    <br />

                </div>


                <div  class="actionCAAdd  taskHeader">
                    <a id="lnkCreateCap" href="">Add Corrective Action</a>
                    <div class="statusPanel">
                        <div class="greenSelectImage">
                            Completed
                        </div>
                        <div class=" warningYellowImage">
                            Not Completed
                        </div>
                        <div class=" warningRedImage">
                            Past Due
                        </div>
                    </div>
                </div>
              

                  <div id="divCausalFactorContent" class="ClearAll causalContentBorder">
                  
                          <div class="blueHeader">
                              Corrective Actions
                          </div>
                          <div class="ContentWidth">  <uc:ucActionPlan runat="server" ID="ucActionPlan" /></div>
                       
                </div>
            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>

        <%--  code for popup--%>
        <div class="demo">
            <div id="divEditActionPlanDialog" title="Edit Corrective Action" class="seasonContentHide">
                <div id="divContent"></div>
                <iframe id="frameLoadPopup" scrolling="auto" height="810" width="900" frameborder="0"></iframe>
            </div>
        </div>
        <%--  code for popup--%>
    </div>

</asp:Content>
