﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorrectiveAction.aspx.cs" MasterPageFile="~/tap.mst/Master.Master"
    Inherits="tap.ui.usr.CorrectiveAction" %>

<%@ Register Src="~/tap.usr.ctrl/ucRootCauseTree.ascx" TagName="ucRCT" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucTask.ascx" TagName="ucTask" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucPerson.ascx" TagPrefix="uc" TagName="ucPerson" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Master.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>

      <%--jq Grid--%> 
     <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.common.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.action/js.correctiveaction.js") %>" type="text/javascript"> </script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
       <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.person.js") %>" type="text/javascript"></script>
    <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';
        _isShareTab = false;
        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

    </script>
    <style>
        #form1 {
            margin: 0 0 0px;
        }
    </style>

     <div class="siteContent" style="padding-left:5%;">  
        <div class="PageTitle" id="divTitle">
            New Corrective Action
        </div>
        <div class="ClearAll causalFactorContentBorder">
            <div class="blueHeader">
                Root or Generic Causes to Address with this Corrective Action
            </div>

            <uc:ucRCT ID="ucRCT" runat="server" />

        </div>

        <div class="ClearAll causalFactorContentBorder marginTop30">
            <div class="blueHeader">
                Corrective Action Details
            </div>
            <div class="ContentWidth">
                <div>
                    <div class="caDetailTitle marginRight20">
                        <strong>Identifier:</strong><span id="spnIdentifierName" style="color: Red">*</span>
                    </div>
                    <input type="text" id="txtIdentifier" autofocus="" />
                    <span id="lblIdentifier">A name or number to identify this corrective action</span>
                </div>
                <div class="MarginTop10">
                    <div class="caDetailTitle">
                        <strong>Description:</strong>
                    </div>
                    <textarea id="txtEventDescription" class="ContentInputTextBox WidthP40"></textarea>
                </div>
                <div class="MarginTop10">
                    <div class="caDetailTitle">
                        <strong>Business Case:</strong>
                    </div>
                    <textarea id="txtBusinessCase" class="ContentInputTextBox WidthP40"></textarea>
                </div>
                <div class="MarginTop10">
                    <div class="caDetailTitle">
                        <strong>Review Notes:</strong>
                    </div>
                    <textarea id="txtReviewNotes" class="ContentInputTextBox WidthP40"></textarea>
                </div>
                <div class="MarginTop10">
                    <div class="caDetailTitle">
                        <strong>Temporary Action:</strong>
                    </div>
                    <textarea id="txtTempAction" class="ContentInputTextBox WidthP40"></textarea>
                </div>
            </div>
        </div>

        <uc:ucTask ID="UcTask" runat="server"></uc:ucTask>
          <uc:ucPerson runat="server" ID="ucPerson" />
        <div class="divButtons" style="margin-right:20%;">
        
            <input type="button" class="btn btn-primary btn-small" id="btnSaveCAP" value="Create" />
            <input type="button" class="btn btn-primary btn-small" id="btnCancel" value="Cancel" />
        </div>
         
  </div>

 </asp:Content>