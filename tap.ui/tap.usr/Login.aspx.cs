﻿//---------------------------------------------------------------------------------------------
// File Name 	: Login.aspx.cs

// Date Created  : N/A

// Description   : Login page : Authentication methods ( Redirecting to Home page once athentication succeeded.)

// Javascript File Used : js.usr.js
//---------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using tap.dom.hlp;
using tap.dom.gen;
using System.Globalization;
using System.Web.Configuration;
using System.Configuration;
using System.Web.Routing;
using tap.dom.usr;
#endregion

namespace tap.ui.usr
{
    public partial class Login : System.Web.UI.Page
    {
        public string _isSU = ConfigurationManager.AppSettings["IsSU"];
        tap.dom.adm.Company _company = new dom.adm.Company();
        tap.dom.adm.SiteSettings _sitesetting = new dom.adm.SiteSettings();

        #region "Page Load Events"
        /// <summary>
        /// Store the comapnyid and virtual location in hidden fields 
        /// for use in the js files while calling the service methods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                if (!Page.IsPostBack)
                {
                    bool isCompanySetUp = _company.isCompanySetUp();
                  

                    if (!isCompanySetUp)
                    {
                        //THE BELOW WILL NOT WORK AS REDIRECTROUTE TABLE ALWAYS AUTHENTICATE THE PAGE AND HENCE A INFINITE LOOP WILL OCCURS AT LOGIN PAGE
                        Response.Redirect("~/tap.usr/SetUp.aspx");                       
                    }
                  
                    if ((Request.QueryString["message"] == "1"))
                    {

                        var queryString = Request.QueryString["ReturnUrl"] == null ? string.Empty : Request.QueryString["ReturnUrl"];                        

                        //if (queryString.Contains("Signin") == false)
                        var directPageAccessWhileNotLoggedIn = !(User.Identity.IsAuthenticated && (Session.Count > 0));

                        //var directPageAccessWhileNotLoggedIn = (!(User.Identity.IsAuthenticated) && (Session.Count == 0));
                        var signOutLinkClicked = (queryString.Contains("Signin"));
                        
                        if ((!signOutLinkClicked) && (!directPageAccessWhileNotLoggedIn))
                        { 

                            if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
                            {
                                ClientScript.RegisterStartupScript(Page.GetType(), "clientScript",
                                                        "<script type='text/javascript'>DisplayMessage();</script>");
                            }
                               FormsAuthentication.SignOut();
                        }
                    }
                    else if (Request.QueryString["message"] == "2")
                    {
                        if ((!ClientScript.IsStartupScriptRegistered("clientScript")))
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "clientScript",
                                                    "<script type='text/javascript'>LinkExpired();</script>");
                        }
                        FormsAuthentication.SignOut();
                    }
                    else if (User.Identity.IsAuthenticated && (Session.Count > 0))
                    {
                        Response.RedirectToRoute("homepage");
                    }
                    
                }
              
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Login.aspx.cs", "Page_Load");
            }

        }
        #endregion

        #region "Authentication Button Event"

        /// <summary>
        /// btnAuthentication_Click
        /// Getting User Id,Company Id and Virtual Directoy Name and storing in Session
        /// Redirecting to Home page once athentication succeeded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAuthentication_Click(object sender, EventArgs e)
        {


            try
            {
                string userID = hdnUserID.Value;
                Application["userID"] = userID;
                string companyID = hdnCompanyID.Value;
                Application["companyID"] = companyID;
                string companyName = hdnCompanyName.Value;
                int userId = 0;
                int companyId = 0;
                string virtualDirectoryPath = string.Empty;
                string returnUrl = Request.QueryString["ReturnUrl"];
                string transactionMessage = string.Empty;



                if (int.TryParse(userID, out userId) && int.TryParse(companyID, out companyId))
                {

                    Session["UserId"] = userId;
                    Session["CompanyId"] = companyId;
                    Session["UserName"] = hdnUserName.Value;
                    Session["FirstName"] = hdnFirstName.Value;
                    Session["CompanyName"] = hdnCompanyName.Value;

                    bool isSUCompany = _company.isSUCompany(companyId);
                    bool isSUDivision = _company.isSUDivision(companyId);

                    //SessionService.CompanyId = companyId;
                    //SessionService.UserId = userId;
                    //SessionService.CompanyName = companyName;
                    //SessionService.BaseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
                    CultureInfo[] cultures = new CultureInfo[Request.UserLanguages.Length];
                    //SessionService.CurrentCulture = CultureInfo.CurrentCulture;


                    Session["DateFormat"] = hdnDateFormat.Value == "" ? "MMM dd yyyy" : hdnDateFormat.Value;
                    //SessionService.DateFormat = hdnDateFormat.Value == "" ? "MMM dd yyyy" : hdnDateFormat.Value;


                    string userrole = string.Empty;
                    SetFormAuthenticationSecurity(hdnUserName.Value, userrole, companyId);

                    string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();

                    transactionMessage = hdnUserName.Value + " user logged in.";

                    if (transactionMessage != string.Empty)
                    {
                        tap.dom.transaction.Transaction.SaveTransaction(0, userId, companyId, Enumeration.TransactionCategory.Create, transactionMessage);
                    }
                    var hdnIsPasswordOutOfPolicy = "";
                    var hdnIsPasswordExpired = "";
                    Password password = new Password();
                    bool isPasswordMatched = password.MatchPasswordPolicy(userId);
                    //if (!isPasswordMatched)
                    //{

                    //    hdnIsPasswordOutOfPolicy= "1";
                    //    Response.Redirect("~/Admin/Users/" + userId + "/Edit");
                    //}
                    //else
                    //    hdnIsPasswordOutOfPolicy = "0";

                    bool isPasswordExpired = password.PasswordExpired(userId);
                    //if (isPasswordExpired)
                    //    hdnIsPasswordExpired = "1";
                    //else
                    //    hdnIsPasswordExpired = "0";

                    if (isPasswordExpired)
                    {
                        // Session["Username"] = hdnUserName.Value;
                        Response.Redirect("~/ChangePassword");
                    }
                    else if (!isPasswordMatched)
                    {
                        //Response.Redirect("~/Admin/Users/" + userId + "/Edit");
                        Response.Redirect("~/ChangePassword/message");
                        return;
                    }


                    if (Request.QueryString["setup"] == "1")
                    {
                        Response.RedirectToRoute("AdminSetting");
                    }
                    else if (returnUrl != null)
                    {
                        if (returnUrl == virtualDirectoryPath + "Logout")
                            Response.RedirectToRoute("homepage");
                        else
                            Response.Redirect(returnUrl);
                    }
                    else
                        Response.RedirectToRoute("homepage");



                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Login.aspx.cs", "btnAuthentication_Click");
            }
        }

        /// <summary>
        /// This sub is used to set the FormAuthentication Security
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userRole"></param>
        /// <remarks></remarks>
        public void SetFormAuthenticationSecurity(string username, string userRole, int companyId)
        {

            try
            {

                tap.dom.adm.SiteSettings siteSettings = new tap.dom.adm.SiteSettings();
                int applicationTimeOut = siteSettings.GetApplicationTimeOut(companyId);

                ////If no value set at database , get from config file
                //if (applicationTimeOut == 0)
                //{
                //    System.Configuration.Configuration configuration = WebConfigurationManager.OpenWebConfiguration("/aspnetTest");

                //    // Get the external Authentication section.
                //    AuthenticationSection authenticationSection = (AuthenticationSection)configuration.GetSection("system.web/authentication");

                //    // Get the external Forms section .
                //    FormsAuthenticationConfiguration formsAuthentication = authenticationSection.Forms;

                //    applicationTimeOut = Convert.ToInt32(formsAuthentication.Timeout.TotalMinutes);

                //    Session.Timeout = applicationTimeOut;
                //}
                //else
                //{
                //    Session.Timeout = applicationTimeOut;
                //}

                 if (applicationTimeOut != 0)
                 {
                     Session.Timeout = applicationTimeOut;
                 }

                FormsAuthenticationTicket authenticationTicket =
                    new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(Session.Timeout + 10), false, userRole);
                                
                string encryptedTicket = string.Empty;
                encryptedTicket = FormsAuthentication.Encrypt(authenticationTicket);

                HttpCookie authenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                Response.Cookies.Add(authenticationCookie);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Login.aspx.cs", "SetFormAuthenticationSecurity");
            }

        }
        #endregion
    }
}