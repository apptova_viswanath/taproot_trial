﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="UploadFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>File Upload </title>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet"
        type="text/css" />
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
      <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
   
     <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
   
    <style type="text/css">
        .error {
            color:red;
            margin-left:20px;
        }
        /*Google translator top bar hide*/
        .goog-te-banner-frame.skiptranslate {display: none !important;} 
        body { top: 0px !important; overflow:hidden;}

    </style>
    <script type="text/javascript">
       
        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }
        $(document).ready(function () {
            $('#btnCancel').click(function () {
                var id = parent.$('#uploadAttachmentModal');
                id.bind("dialogclose", function (event, ui) {
                });

                self.close();
                parent.$('#uploadAttachmentModal').hide();
                parent.$("#dialog:ui-dialog").dialog("destroy");
                parent.$("#uploadAttachmentModal").dialog("destroy");
            });

            //this code will be executed when a new file is selected
            $('#file').bind('change', function () {
              
                //converts the file size from bytes to MB
                var fileSize = this.files[0].size;

                //gets the full file name including the extension
                var fileName = this.files[0].name;

                //checks whether the file less than 1 GB
                if (fileSize > 30000000) {
                    $('#btnUpload').attr("disabled", "disabled");
                    _message = 'There is a 30 MB attachment size limit.'
                    _messageType = 'jSuccess';
                    parent.NotificationMessage(_message, _messageType, true, 1000);
                                      
                }
                else {
                    $('#lblErrorMessage').hide();
                    $('#btnUpload').removeAttr("disabled");
                }
            });
        });

       

    </script>
     <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="NoOverflow">
        <div>
            <asp:Label ID="lblErrorMessage" runat="server" CssClass="ContentLabel"></asp:Label>
        </div>
        <div class="MarginTop10">
            <asp:RegularExpressionValidator ID="uplValidator" runat="server" ControlToValidate="file" CssClass="error" EnableClientScript="true"
                 ErrorMessage="For security reasons, you must zip files that contain executable code." 
                 ValidationExpression="^(?!.*\.([eE][xX][eE]|[dD][lL][lL]|[mM][sS][uU]|[bB][aA][tT]|[mM][sS][iI])$).*"></asp:RegularExpressionValidator>
            <input id="file" class="ContentInputTextBox WidthP40" style="width: 80%" type="file" name="files[]"
                runat="server" />
        </div>
        <br />
        <div class="MarginTop20 ClearAll textCenter">
            <asp:Button runat="server" ID="btnUpload" CssClass="btn btn-primary btn-small" Text="Upload"
                OnClick="btnUpload_Click" />
            <%--<asp:Button runat="server" ID="btnAttachClosePopup" CssClass="btn btn-primary btn-small" ValidationGroup="unvalidatedControls" 
                Text="Cancel" OnClick="btnAttachClosePopup_Click" />--%>

           <input id="btnCancel" type="button" class="btn btn-primary btn-small" value="Cancel" />

        </div>
    </div>
    </form>
</body>
</html>
