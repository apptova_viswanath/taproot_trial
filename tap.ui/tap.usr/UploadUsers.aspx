﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadUsers.aspx.cs" Inherits="UploadUsers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>File Upload </title>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>--%>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.usrupload.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet"
        type="text/css" />
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
    <style type="text/css">
        .error {
            color:red;
            margin-left:20px;
        }

    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div class="NoOverflow">
        <div>
            <asp:Label ID="lblErrorMessage" runat="server" CssClass="ContentLabel"></asp:Label>
        </div>
        <div class="MarginTop10">
             <asp:RegularExpressionValidator ID="uplUserValidator" runat="server" ControlToValidate="file" CssClass="error" EnableClientScript="true"
                 ErrorMessage="Please select excel file to upload." 
                 ValidationExpression=".*(\.xls|\.XLS|\.xlsx|\.XLSX)$"></asp:RegularExpressionValidator>

            <input id="file" class="ContentInputTextBox WidthP40" style="width: 80%" type="file" name="files[]"
                runat="server" />
        </div>
        <br />
        <div class="MarginTop20 ClearAll textCenter">
            <asp:Button runat="server" ID="btnUpload" CssClass="btn btn-primary btn-small" Text="Upload"
                OnClick="btnUpload_Click" />
            <%--<asp:Button runat="server" ID="btnUploadUserClose" CssClass="btn btn-primary btn-small"
                Text="Cancel" OnClick="btnUploadUserClose_Click" />--%>
            <input id="btnUserUploadCancel" type="button" class="btn btn-primary btn-small" value="Cancel" />

        </div>
    </div>
    </form>
</body>
</html>
