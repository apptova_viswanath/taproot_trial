﻿//---------------------------------------------------------------------------------------------
// File Name 	: Report-Tab.aspx.cs

// Date Created  : N/A

// Description   : Report System Tab functionality

// Javascript File Used : js.tab.report.js
//----------------------------------------------------------------------------------------------

#region NameSpace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dat;
#endregion

namespace tap.ui.usr
{
    public partial class Report_Tab : System.Web.UI.Page
    {

        #region Global Variables
        
        public int? _seasonId = 1;
        public bool _isComplete = false;
        public bool _isAnyRCTCompleted = false;
        public bool _isAllRCTCompleted = false;
        const string AUTUMN_SEASON = "Autumn";

        EFEntity _db = new EFEntity();

        #endregion

        #region PageLoad
        /// <summary>
        /// PageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        
            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            int eventId = int.Parse(strEventId);
 

            if (eventId != 0)
            {
                var snapchartInfo = (from snapchart in _db.SnapCharts where snapchart.EventID == eventId select snapchart).AsEnumerable();

                if (snapchartInfo.ToList().Count() != 0)
                {
                    _seasonId = snapchartInfo.LastOrDefault().SeasonID;
                    _isComplete = snapchartInfo.FirstOrDefault().IsComplete;

                    //Check for causal factors exists for this snapchart id.
                    var isSC = _db.CausalFactors.FirstOrDefault(a => a.SnapChartID == snapchartInfo.FirstOrDefault().SnapChartID);
                    var season = snapchartInfo.LastOrDefault().SeasonID;

                    var autumnSeasonID = (from season1 in _db.Seasons where season1.SeasonName == AUTUMN_SEASON select season1).FirstOrDefault().SeasonID;

                    if (isSC != null && season == autumnSeasonID)
                    {

                        _isAnyRCTCompleted = (from x in _db.CausalFactors
                                              where x.SnapChartID == snapchartInfo.FirstOrDefault().SnapChartID && (x.IsComplete == true)
                                              select x).AsEnumerable().Count() > 0 ? true : false;

                        _isAllRCTCompleted = (from x in _db.CausalFactors
                                              where x.SnapChartID == snapchartInfo.FirstOrDefault().SnapChartID && (x.IsComplete == null || x.IsComplete == false)
                                              select x).AsEnumerable().Count() == 0 ? true : false;
                    }
                    else
                    {

                        _seasonId = snapchartInfo.FirstOrDefault().SeasonID;
                    }
                }                
            }
        }
        #endregion
    }
}