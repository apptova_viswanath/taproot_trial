﻿<%@ Page Title="TapRooT®" Language="C#" MasterPageFile="~/tap.mst/Master.master" AutoEventWireup="true"
    CodeBehind="HomePage.aspx.cs" Inherits="tap.ui.usr.HomePage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">

    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <!---Generalized Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.dtfrmt.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--JQGrid Script -->
    <%--<link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.css") %>" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/Scripts/js.jqgrid/ui.jqgrid.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/grid.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery_002.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.jqgrid/jquery.jqGrid.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Date Picker Script for Advanced Search -->
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>

    <!--Page Script -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.hme.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sort.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #pageNavigation_center {
            display: none;
        }

        .ui-dialog-title {
            font: 10pt "Lucida Sans Unicode", "Trebuchet Ms",Helvetica,Arial;
            margin: 0px;
        }

        td.myPager a {
            text-decoration: underline !important;
        }

        gbox_EventsGrid .ui-widget {
            font-size: 1.1em;
        }

        #ui-datepicker-div {
            font-size: 12px;
        }
    </style>
    <input type="hidden" id="hiddenAdvancedIncident" value="1" />
    <input type="hidden" id="hiddenAdvancedInvestigation" value="1" />
    <input type="hidden" id="hiddenAdvancedAudit" value="1" />
    <input type="hidden" id="hiddenAdvancedActionPlan" value="1" />
    <input type="hidden" id="hiddenAdvancedActive" value="1" />
    <input type="hidden" id="hiddenAdvancedCompleted" value="0" />
    <input type="hidden" id="hiddenAdvancedArchived" value="0" />
    <input type="hidden" id="hiddenAdvancedDateRange" value="" />
    <input type="hidden" id="hiddenAdvancedDateFrom" value="" />
    <input type="hidden" id="hiddenAdvancedDateTo" value="" />

    <div class="homeContent" id="leftSideSection">
        <!--Search links -->
        <div>
            <div id="homeErrror">
                <label id="EventErrorMsg">
                </label>
            </div>
            <div class="NavigationBreadCrumb">
            </div>

        </div>
        <!--Auto Search and Advanced Search -->
        <div class="ClearAll">

            <input type="text" class="span2 bootstrp1" id="txtSearch" placeholder="Search Events" title="Search Events" />
            <a class="btn btn-primary style-a" href="javascript:void(0)" onclick="OpenAdvnaceSearchPopUp();return false;">Filter <span class="caret"></span></a>

        </div>
        <div class="ClearAll popupWindowFieldTreeview" id="advancedSearchBox">
            <div class="advanceSearchHeader">
                Advanced Search
            </div>
        </div>
        <div class="backgroundMask">
        </div>
        <!--Advance Search popup ends-->
        <!--Search links Ends -->
        <!--Grid -->
        <div id="Grid" style="margin: 30px 15px 0px 5px;">
            <div class="EventGrid" style="/*margin: 0px;*/">
                <table id="EventsGrid" class="scroll">
                    <tr>
                        <td />
                    </tr>
                </table>
                <div id="pageNavigation" class="scroll" style="text-align: center;">
                </div>
            </div>
        </div>

        <div class="ui-jqgrid-view EventGrid hidden" id="divNoRowMessage">
            <div class="ui-jqgrid-titlebar ui-widget-header ui-corner-top ui-helper-clearfix">
                <span class="ui-jqgrid-title">Events</span>
            </div>
            <div id="divMessage" class="ui-widget-content jqgrow ui-row-ltr">
                You are not currently working  on any events.
            </div>

        </div>

        <div class="ClearAll MarginTop30">
            <label id="ErrShowGrid">
            </label>
        </div>
        <div class="ClearAll MarginTop30 PaddingLeft50">
        </div>
    </div>
    <div class="backgroundPopup">
    </div>
    <!--Rigt side Content starts -->
    <div class="homecontentArea" id="rightHomeContent">
        <div>
            <div class="BlueContentArea" id="divSubscriptionExpire">
                <div>
                    <label class="ContentHeaderLabel" id="Label2">
                        Subscription Expire</label>
                </div>
                <div class="ClearAll">
                    <label id="subscriptionExpireMsg" class="HomeContentLabel ">
                    </label>
                </div>
            </div>
            <div class="BlueContentArea">
                <div>
                    <label class="ContentHeaderLabel" id="lblSystemNotification">
                        Notifications</label>
                </div>
                <div class="ClearAll">
                    <label id="lblBlueContent2" class="HomeContentLabel notranslate">
                    </label>
                </div>
            </div>
            <div class="GrayContentArea MarginTop10 ">
                <div>
                    <a id="lblTasks" class="ContentHeaderLabel" href="">
                        Tasks</a>
                </div>
                <div class="MyActionContent MarginTop10" style="margin-top: 30px">

                    <div class="ClearAll">
                        <div id="divTasks" class="HomeContentLabel notranslate">
                        </div>
                    </div>
                </div>
                <div class="GrayContent">
                    <label id="lblMyInvestigationCount" class="ContentHeaderLabel">
                        Recent Events</label>
                    <div id="divEvents" class="ClearAll HomeContentLabel notranslate">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Rigt side Content ends -->

    <div id="homeAdvanceSearchBox" class="hidden" title="Filters">
        <div>
            <label id="errorSearch">
            </label>
        </div>    
        <div class="SearchStatus FloatLeft">
            <div class="SearchHeader">
                Status
            </div>
            <div class="SearchControls">
                <input type="checkbox" id="chkActive" />
                <label id="lblactive">
                    Active Events</label>
            </div>
            <div class="SearchControls">
                <input type="checkbox" id="chkCompleted" />
                <label id="lblcompleted">
                    Completed</label>
            </div>
            <div class="SearchControls">
                <input type="checkbox" id="chkArchived" />
                <label id="lblarchived">
                    Archived</label>
            </div>
        </div>
        <div class="SearchScope FloatRight">
            <div class="SearchHeader">
                Event Types
            </div>
            <div class="SearchControls">
                <input type="checkbox" id="chkincident" />
                Incidents                          
                    <br />
                <input type="checkbox" id="chkinvestigation" />
                Investigations                       
                    <br />
                <input type="checkbox" id="chkAudit" />
                Audits
                    <br />
                <input type="checkbox" id="chkActionPlan" />
                Action Plans                    
            </div>
        </div>
        <div class="MarginTop20 SearchDateRange ClearAll ">
            <div class="SearchHeader">
                Date Range
            </div>
            <div class="ClearAll SearchControls">
                <input type="radio" id="chkIncDate30" name="rdbdate" />
                <label id="lblincdate">
                    Last 30 Days</label>
            </div>
            <div class="SearchControls">
                <input type="radio" id="chkIncDate60" name="rdbdate" />
                <label id="lblinvdate">
                    Last 60 Days</label>
            </div>
            <div class="SearchControls">
                <input type="radio" id="chkIncDateBetween" name="rdbdate" />
                <label id="lblCAPDate">
                    Between Date</label>
            </div>
            <br />
            <div class="SearchDateControls ">
                From:
                    <input type="text" id="txtfromdate" readonly="readonly" />&nbsp;&nbsp; To:
                    <input type="text" id="txttodate" readonly="readonly"/>
            </div>
        </div>
        <div class="MarginTop20 PaddingRight150">

            <input type="button" class="btn btn-primary btn-small btn-float" value="Reset" id="btnClearSearch" />
            <input type="button" class="btn btn-primary btn-small btn-float" value="Filter" id="btnsearch" />
        </div>
    </div>



</asp:Content>
