﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace tap.ui.usr
{
    public partial class ReportInPDF : System.Web.UI.Page
    {

        tap.dom.gen.CustomReport _customReport = new tap.dom.gen.CustomReport();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        JavaScriptSerializer _jsSerializer = new JavaScriptSerializer();

        string strEventId = string.Empty;
        int reportId = 0;
        string reportTitle = string.Empty;
        string reportName = string.Empty;
        ArrayList arrImagePath = new ArrayList();

        protected void Page_Load(object sender, EventArgs e)
        {
            btnPrint_Click();
        }

        #region Print PDF

        protected void btnPrint_Click()
        {
            strEventId = Convert.ToString(RouteData.Values["eventId"]);
            reportId = Convert.ToInt32(RouteData.Values["reportId"]);

            if (strEventId == "0" || strEventId == null || strEventId == "" || strEventId == string.Empty)
                strEventId = _cryptography.Encrypt(_customReport.GetEventID(reportId));

            try
            {
                //get the data from Data base
                var printReportData = _customReport.GenerateReport(strEventId, reportId).ToString();

                //build the string builder
                BuildStringBuilder(printReportData, strEventId);
            }
            catch (Exception ex)
            {
            }
        }

        public void BuildStringBuilder(string jsonReportData, string eventId)
        {
            string reportName = string.Empty;
            string reportTitle = string.Empty;
            string reportLogoName = string.Empty;
            //take temp path from server
            string tempPath = Server.MapPath("~/temp/");

            //check directory exists or not -if not create it.
            if (Directory.Exists(tempPath) == false)
            {
                Directory.CreateDirectory(tempPath);
            }

            var jss = new JavaScriptSerializer();

            bool isHeaderDetails = false;

            //desrialize the data
            dynamic data = jss.Deserialize<dynamic>(jsonReportData);

            //create string builder
            StringBuilder sb = new StringBuilder();
            sb.Append("<table class='table' style='font-family:arial;font-size:10px;'>");
            var count = 0;

            // Build the header based on the values in the
            //  first data item.
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i]["ReportName"] != null)
                    reportName = Convert.ToString(data[i]["ReportName"]);
                if (data[i]["ReportTitle"] != null)
                    reportTitle = Convert.ToString(data[i]["ReportTitle"]);
                if (data[i]["ReportLogoName"] != null)
                    reportLogoName = Convert.ToString(data[i]["ReportLogoName"]);

                ArrayList reportDataList = new ArrayList();
                ArrayList imageDataList = new ArrayList();
                string singleValues = string.Empty;
                foreach (var val in data[i].Values)
                {

                    string getValue = Convert.ToString(val);
                    if (getValue != "System.Object[]")
                        reportDataList.Add(val);
                    else
                    {
                        if (val.Length == 1 && (val[0].IndexOf('.') == -1))
                        {
                            reportDataList.Add(val[0]);//tree view object values
                        }
                        else if ((reportDataList[reportDataList.Count - 1] != "Root Causes" || reportDataList[reportDataList.Count - 1] != "Tasks" || reportDataList[reportDataList.Count - 1] != "Action Plans" || reportDataList[reportDataList.Count - 1] != "Causal Factors") && reportDataList[reportDataList.Count - 1] != "")
                        {
                            for (int j = 0; j < val.Length; j++)
                            {
                                singleValues += val[j];
                            }
                        }
                        else
                        {
                            for (int j = 0; j < val.Length; j++)
                            {
                                imageDataList.Add(val[j]);//image object file names

                            }
                        }
                    }
                }

                string imageURL = string.Empty;

                if (!isHeaderDetails)
                {
                    //  isCompanyLogoUsed,isCompanyNameUsed values are selected or not
                    tap.dat.Report reportDetails = tap.dom.gen.CustomReport.GetReportDetails(reportId);

                    bool isCompanyLogo = Convert.ToBoolean(reportDetails.IsCompanyLogoUsed);
                    bool isCompanyName = Convert.ToBoolean(reportDetails.isCompanyNameUsed);
                    bool isReportTitle = Convert.ToBoolean(reportDetails.isReportTitleUsed);
                    bool isDateCreated = Convert.ToBoolean(reportDetails.isCreatedDateUsed);

                    //Display header Details
                    sb.Append("<tr class='width100'>\n");

                    if (isCompanyLogo)
                    {
                        sb.AppendFormat("<td width='20%'>");
                        //if (reportDataList[11] != null && reportDataList[11] != "")
                        if (!String.IsNullOrEmpty(reportLogoName))
                            sb.AppendFormat("<img src='" + System.Web.Hosting.HostingEnvironment.MapPath("~/temp/") + reportLogoName + "' />"); //image
                        sb.AppendFormat("</td>");
                    }

                    if (isReportTitle)
                    {
                        if (!isCompanyLogo && !isCompanyName)
                        {
                            if (reportDataList.Count > 12)
                                sb.AppendFormat("<td width='60%'  style='{1}' class='reportTitlePdf'>{0}</td>", reportTitle, reportDetails.ReportTitleStyle + "text-align:center;");//title
                            else
                                sb.AppendFormat("<td width='60%'  style='{1}' class='reportTitlePdf'>{0}</td>", reportTitle, reportDetails.ReportTitleStyle + "text-align:center;");//title
                        }
                        else
                            if (reportDataList.Count > 12)
                                sb.AppendFormat("<td width='60%' style='{1}' class='reportTitlePdf'>{0}</td>", reportTitle, reportDetails.ReportTitleStyle + "text-align:center;");//title
                            else
                                sb.AppendFormat("<td width='60%' style='{1}' class='reportTitlePdf'>{0}</td>", reportTitle, reportDetails.ReportTitleStyle + "text-align:center;");//title

                    }

                    if (isCompanyName)
                    {
                        sb.AppendFormat("<td class='reportCompanyName' style=' float:right;text-align:right; vertical-align:top;'>{0}<br/>{1}</td>", Convert.ToString(Session["CompanyName"]), DateTime.Now.Date.ToString("MM/dd/yyyy"));//created by name
                    }


                    if (!isCompanyLogo && !isReportTitle && !isCompanyName)
                        sb.AppendFormat("<td colspan='3'></td>");

                    sb.Append("</tr>");

                    //if (isDateCreated)
                    //{
                    //    sb.Append("<tr style='width:100%;clear:both;'>\n");
                    //    sb.AppendFormat("<td colspan='3' class='reportDatePdf'  style='float:right;text-align:right;'>Date: {0}</td>", DateTime.Now.Date.ToString("MM/dd/yyyy"));//date
                    //    sb.Append(" </tr>\n");
                    //}

                    //reportName = reportDataList[1].ToString();
                    isHeaderDetails = true;
                }

                string columnValue = reportDataList[3].ToString();

                if (columnValue != "Image")
                {
                    sb.AppendFormat("<tr style='clear:both;'>");
                }

                if (columnValue == "Image")//remove hard code
                {
                    //if images
                    //append </table> to end the current table
                    sb.Append("</table>");

                    imageURL = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/") + imageDataList[count];

                    sb.AppendFormat("<img src='" + imageURL + "' />");
                    count++;
                    arrImagePath.Add(imageURL);

                    //add append <table> to start a new table
                    sb.Append("<table><tr><td colspan='3'></td>");
                }
                else if (columnValue == "Line")
                {
                    sb.Append("<td colspan='4'><span style='color:#ffffff;font-size:5px;'><table border='1' cellpadding='10' cellspacing='10' width='100%'><tr><td>&nbsp;</td></tr></table> T</span></td>");

                }
                else if (columnValue == "Line Break")
                {
                    sb.Append("<td colspan='4'><br/></td>");
                    sb.Append("<td colspan='4'><br/></td>");
                }
                else if (columnValue == "Page Break")
                {
                    sb.Append("<td colspan='4' >Page Break</td>");

                }
                else if (columnValue == "Integrate")//integrate values
                {
                    // string integrateType=hdnIntegrateType.Value.ToString();


                    tap.dom.gen.CustomReport _customReport = new tap.dom.gen.CustomReport();

                    if (data[i]["IntegrateType"] == "Causal Factor")
                    {
                        var byCausalFactor = _customReport.GetPrintIntegrateValues(eventId, "Causal Factor");
                        sb.AppendFormat(byCausalFactor.ToString());
                        sb.Append("<tr border='0' cellpadding='0' height='1%' style='text-align:center;'><td colspan='3' style='color:#ffffff'>add break</td></tr>/n");//added extra row when we will fetch all 3 integrate values it was joing all the rows
                        //hdnIntegrateTypeCF.Value = "";
                    }
                    else if (data[i]["IntegrateType"] == "Root Cause")
                    {
                        var byRootCause = _customReport.GetPrintIntegrateValues(eventId, "Root Cause");
                        sb.AppendFormat(byRootCause.ToString());
                        sb.Append("<tr border='0' cellpadding='0' height='1%' style='text-align:center;'><td colspan='3' style='color:#ffffff'>add break</td></tr>/n");
                        //hdnIntegrateTypeRCT.Value = "";
                    }
                    else if (data[i]["IntegrateType"] == "Action Plan")
                    {
                        var byActionPlan = _customReport.GetPrintIntegrateValues(eventId, "Action Plan");
                        sb.AppendFormat(byActionPlan.ToString());
                        sb.Append("<tr border='0' cellpadding='0' height='1%' style='text-align:center;'><td colspan='3' style='color:#ffffff'>add break</td></tr>/n");
                        //hdnIntegrateTypeCAP.Value = "";
                    }
                }
                else if (columnValue == "Root Causes" || columnValue == "Tasks" || columnValue == "Action Plans" || columnValue == "Causal Factors")
                {
                    sb.AppendFormat("<td border='0' colspan='1' style='{1}'>{0}: </td>", columnValue, "margin:5px 0px 0px 5px;text-align:left;");
                    if (reportDataList[5] == "")
                        reportDataList[4] = "";
                    object singleRCTValues = ((singleValues != "") ? singleValues : (reportDataList[4] != "") ? reportDataList[4] : string.Empty);

                    sb.AppendFormat("<td border='0' colspan='2' style='{1}'>{0}</td>", singleRCTValues, "margin:5px 0px 0px 5px;text-align:left;width:auto;");
                }
                else
                {
                    sb.AppendFormat("<td border='0' colspan='1' style='{1}'>{0}: </td>", columnValue, reportDataList[5] + ";text-align:left;");
                    if (reportDataList[5] == "")
                        reportDataList[4] = "";
                    sb.AppendFormat("<td border='0' colspan='2' style='{1}'>{0}</td>", reportDataList[4], reportDataList[5] + ";text-align:left;width:auto;");
                }

                sb.Append(" </tr>");
            }

            sb.Append("   </table>");

            //convert html to pdf file- pass string builder values
            ConvertHTMLToPDF(sb, reportName, eventId);

        }

        protected void ConvertHTMLToPDF(StringBuilder sb, string reportName, string eventId)
        {

            string HTMLCode = sb.ToString();
            HttpContext context = HttpContext.Current;

            //Render PlaceHolder to temporary stream
            System.IO.StringWriter stringWrite = new StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

            StringReader reader = new StringReader(HTMLCode);
            StyleSheet css = new StyleSheet();

            //Create PDF document
            iTextSharp.text.Document doc = new Document(PageSize.LETTER);

            string path = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/") + "TapR00T_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + ".pdf";
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(path, FileMode.Create));

            HTMLWorker parser = new HTMLWorker(doc);
            parser.SetStyleSheet(css);

            doc.Open();

            // LineSeparator line = new LineSeparator(0.5f, 50f, BaseColor.BLACK, Element.ALIGN_CENTER, -1);

            bool isNewPage = true;
            int curPage = 0;

            //split page breaks
            string[] values = sb.ToString().Split(new string[] { "Page Break" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < values.Length; i++)
            {
                //add html tags manually else runtime error will get
                string sb2 = string.Empty;
                if (values.Length != 1)
                {
                    if (i == 0)
                    {
                        sb2 = values[i].ToString() + "</td> </tr>   </table>";
                    }
                    else if (values[i].ToString().LastIndexOf("</table>") > -1)
                        sb2 = "<table><tr><td>" + values[i].ToString();
                    else
                        sb2 = "<table><tr><td>" + values[i].ToString() + "</td> </tr>   </table>";

                }
                else
                    sb2 = values[i].ToString();

                /********************************************************************************/
                foreach (IElement element in HTMLWorker.ParseToList(new StringReader(sb2), css))
                {
                    if (element.GetType() == typeof(PdfPTable))
                    {
                        PdfPTable table = (PdfPTable)element;
                        table.TotalWidth = 652f;

                        table.SplitLate = false;
                        table.KeepRowsTogether(0, table.Rows.Count);
                        table.SpacingAfter = 5f;

                        isNewPage = (curPage != writer.CurrentPageNumber);

                        if (!isNewPage)
                        {
                            if (writer.GetVerticalPosition(true) > table.TotalHeight + doc.BottomMargin + 5f)
                            {
                                table.SpacingBefore = 5f;
                                // doc.Add(new Chunk(line));
                            }
                        }

                        doc.Add(table);
                        curPage = writer.CurrentPageNumber;
                    }
                    else
                        doc.Add(element);
                }

                doc.NewPage();
            }

            doc.Close();

            //this.ClientScript.RegisterStartupScript(this.GetType(), "OpenPdfReports", "<script language=javascript> window.open('" + path + "','window','HEIGHT=600,WIDTH=820,top=50,left=50,toolbar=yes,scrollbars=yes,resizable=yes');</script> ");

            string pdf = path;
            Response.Clear();

            Response.ClearContent();
            Response.ClearHeaders();

            Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", string.Format("inline;filename={1}; size={0}", pdf.Length, reportName + "-" + eventId + ".pdf"));
            Response.AddHeader("content-disposition", "inline");
            Response.Write(pdf);
            //Response.Write(sb);

            WebClient objWC = new WebClient();
            Byte[] FileBuffer = objWC.DownloadData(path);

            if (FileBuffer != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "inline");
                Response.BinaryWrite(FileBuffer);
            }

            Response.Flush();
            
            System.IO.File.Delete(path);

            if (arrImagePath.Count > 0)
            {
                for (int m = 0; m < arrImagePath.Count; m++)
                {
                    System.IO.File.Delete(arrImagePath[m].ToString());
                }
            }
            
            Response.End();

        }

        #endregion
    }
}