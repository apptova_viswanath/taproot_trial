﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.ui.usr
{
    public partial class OpenFile : System.Web.UI.Page
    {
        #region "Properties"
        public int FileId
        {
            get
            {
                string id = Request.Params["id"];
                int outId = 0;
                int.TryParse(id, out  outId);
                return outId;
            }
            set { }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (FileId > 0)
            {
                SelectedFileOpen();
            }
        }

        #region "Open the selected file"

        private void SelectedFileOpen()
        {
            try
            {

                tap.dat.Attachments attachments = tap.dom.adm.Attachment.GetAttachment(FileId);
                if (attachments == null)
                    return;
                //for opening the file in the same page.
                byte[] buffer = attachments.AttachmentData.ToArray();
                if (buffer != null)
                {
                    //Response.ContentType = "application/pdf";
                    Response.ContentType = ReturnExtension(attachments.Extension.ToLower());

                    if (CheckEditFile(attachments.Extension.ToLower()))
                    {
                        MemoryStream memoryStream = new MemoryStream(buffer);
                        memoryStream.Flush();
                        memoryStream.Close();
                        Response.AppendHeader("content-disposition", "attachment; filename=\"" + attachments.DocumentName.Replace("\"", "'") + "\"");

                        Response.BinaryWrite(memoryStream.ToArray());

                        Response.Flush();
                        Response.End();
                    }
                    else
                    {
                        Response.AddHeader("content-length", buffer.Length.ToString());
                        Response.BinaryWrite(buffer);
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        #endregion

        private bool CheckEditFile(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".doc":
                case ".docx":
                case ".xls":
                case ".xlsx":
                    return true;
                
                default:
                    return false;
            }
        }

        private string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".txt":
                    return "text/plain";
                case ".pdf":
                    return "application/pdf";
                case ".doc":
                case ".docx":
                    return "application/ms-word";
                case ".xls":
                    return "application/vnd.ms-excel";
                case ".xlsx":
                    return "application/vnd.xls";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".png":
                    return "image/png";
                //case ".wav":
                //    return "audio/wav";
                //case ".ppt":
                //    return "application/mspowerpoint";
                //case ".dwg":
                //    return "image/vnd.dwg";
                default:
                    return "application/octet-stream";
            }
        }
    }
}