﻿//---------------------------------------------------------------------------------------------
// File Name 	: TapRoot-Tab.aspx.cs

// Date Created  : N/A

// Description   : Taproot System Tab functionality

// Javascript File Used : js.tab.taproot.js
//----------------------------------------------------------------------------------------------


#region Namespace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace tap.ui.usr
{
    public partial class TapRoot_Tab : System.Web.UI.Page
    {
        #region PageLoad
        /// <summary>
        /// PageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        #endregion
    }
}