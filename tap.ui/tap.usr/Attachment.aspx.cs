﻿//---------------------------------------------------------------------------------------------
// File Name 	: Attachment.aspx.cs

// Date Created  : N/A

// Description   : Attachment Page Methods (display Attachment Treeview with Folders)

// Javascript File Used : js.atch.js
//----------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace tap.ui.usr
{
    public partial class Attachment : System.Web.UI.Page
    {
        #region PageLoad
        /// <summary>
        /// PageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void Page_Load(object sender, EventArgs e)
        {
            tap.dom.gen.Attachments _attachment = new tap.dom.gen.Attachments();
            tap.dom.gen.Cryptography _cryptography = new tap.dom.gen.Cryptography();

            string eventID = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            int companyId = 0;
            int userId = 0;

            if (Session["CompanyId"] != null)
            {
                companyId = Convert.ToInt32(Session["CompanyId"].ToString());
            }
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
            }
            if (!IsPostBack)
            {               
                _attachment.SnapCapAttachment(eventID, companyId, userId);
                 
            }
        }
        #endregion

    }
}