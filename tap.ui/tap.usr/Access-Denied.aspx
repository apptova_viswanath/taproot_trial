﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true" CodeBehind="Access-Denied.aspx.cs" Inherits="tap.ui.usr.Access_Denied" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CustomReport.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~//Scripts/tap.js.common/js.sec.access.js") %>" type="text/javascript"></script>
    <div class="siteContent">
        <div style="padding-left:20px;height:590px;">
            <div class="divWarningSecurityMessage ">
                Oops! 
            </div>
            <div>It looks like you are trying to access a page that you don't have permission to see.</div>
            <div>
                If you think you need access to this page, please contact the event creator to request access:
            </div>
            <div id="divSecurityAccess" runat="server"></div>
            <div style="margin-top: 5px; float: left;">
                <input type="button" id="btnSecurityGoBack" class="btn btn-primary btn-small btn-float" value="Go Back" />
            </div>
        </div>
        <br />

    </div>
</asp:Content>

