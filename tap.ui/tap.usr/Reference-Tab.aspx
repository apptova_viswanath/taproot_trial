﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reference-Tab.aspx.cs"
    Inherits="tap.ui.usr.Reference_Tab" MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="taprootTabContent" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/ReferenceTab.css") %>" rel="stylesheet"
        type="text/css" />
    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.tab.reference.js") %>" type="text/javascript"> </script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />

    <style>

        .header div
        {
            float: left;
            font-size: 18px;
            margin-bottom:10px;              
        }

        .refOptionalText label
        {
            clear:both;
        }

        .refTaskText label
        {
            clear:both;
        }

        .header
        {
            float:left;  
        }

        .phase
        {
            width: 170px; margin:10px 0px 0px 10px;
        }

        .step
        {
            width: 320px; margin:10px 0px 0px 50px;
        }

        .optional
        {
            width: 400px;margin:10px 0px 0px 50px;
        }

    </style>

    <div class="siteContent" style="overflow-x:hidden;">
        
        <div id="tabs">
           
            <uc:ucEvent ID="ucEventTabs" runat="server" />
            <br />
            
            <div id="divTabReference" class="ContentWidth MarginTop40" >


                <div class="header" style="width: 1000px; margin: 0px 0px 10px 0px">

                    <label class="refStepText ">Phase</label>
                    <label class="refTaskText" style="font-size: 16px">Step</label>
                    <label class="refOptionalText" style="font-size: 16px">Optional</label>

                </div>

                <div class="refSpringBox">
                    <div class="PaddingTop10">
                        <div>
                            <label class="refStepText ">
                                Plan
                            </label>
                            <label id="lblPlanStep" class="refTaskText">
                            </label>
                            <label id="lblPlanOptional" class="refOptionalText">
                            </label>
                        </div>
                        <div class="TaskBox">

                            <label class="refStepText">
                                Investigate
                            </label>
                            <label id="lblInvestigateStep" class="refTaskText">
                            </label>
                            <label id="lblInvestigateOptional" class="refOptionalText">
                                Use <span class="notranslate">Equifactor®</span>, <span class="notranslate">CHAP®</span>, or Change <span class="notranslate">Analysis®</span> to add events and conditions to the <span class="notranslate"><a class="snp">SnapCharT®</a></span>
                            </label>
                        </div>
                        <div class="TaskBox">
                            <label class="refStepText">
                                Analyze
                            </label>
                            <div id="divAnalyzeStep" class="refTaskText">
                                <label id="lblAnalyzeStep3"></label>
                                <br />
                                <br />
                                <label id="lblAnalyzeStep4"></label>
                                <br />
                                <br />
                                <label id="lblAnalyzeStep5">5 - Analyze each root cause for generic causes</label>
                            </div>
                            <div id="divAnalyzePlan" class="refOptionalText">
                                <label id="lblAnalyzeOptional3"></label>
                                <br />
                                <br />
                                <label id="lblAnalyzeOptional4"></label>
                                <br />
                                <br />
                                <label id="lblAnalyzeOptional5">Reference <span class="notranslate"><a class="capHelper">Corrective Action Helper®</a></span> to guide generic cause determination</label>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="refSummerBox summer">
                    <div class="PaddingTop10">
                        <div class="refStepText ">
                           Fix
                        </div>
                        <div class="refTaskText">
                         6 - Develop fixes using SMARTER and <span class="notranslate"><a class="capHelper">Corrective Action Helper®</a></span>
                        </div>
                        <div class="refOptionalText">
                           Use Safeguards <span class="notranslate">Analysis®</span> to develop fixes
                        </div>
                    </div>
                </div>
              
                <div class="refWinterBox winter">
                    <div class="PaddingTop10">
                        <div class="refStepText ">
                            Report
                        </div>
                        <div class="refTaskText">
                           7 - Present/Report your results using Winter <span class="notranslate"><a class="wntSnp">SnapCharT®</a></span> and Report Builder
                        </div>
                        <div class="refOptionalText">
                          
                        </div>
                    </div>
                </div>
            
            </div>

             <div class="tab-next-prev">
                 <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>

        </div>

    </div>

</asp:Content>
