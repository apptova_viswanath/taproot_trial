﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;

namespace tap.ui.usr
{
    public partial class Access_Denied : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int eventId = Convert.ToInt32(Page.RouteData.Values["EventId"]);
            tap.dom.gen.Security securityAddress = new tap.dom.gen.Security();
            //get the adress of present login company
            string address = securityAddress.GetCompanyAddress(Convert.ToInt32(Session["CompanyId"]), Convert.ToInt32(Session["UserId"]), eventId);

            divSecurityAccess.InnerHtml = address;
        }
    }
}