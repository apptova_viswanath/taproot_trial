﻿//---------------------------------------------------------------------------------------------
// File Name 	: Reference-Tab.aspx.cs

// Date Created  : N/A

// Description   : Reference System Tab functionality

// Javascript File Used : js.tab.reference.js
//----------------------------------------------------------------------------------------------

#region Namespace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
#endregion

namespace tap.ui.usr
{
    public partial class Reference_Tab : System.Web.UI.Page
    {
        #region Page Load
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            //dom.gen.Security security = new dom.gen.Security();

            //string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            //int eventId = int.Parse(strEventId);
            //int userId = Convert.ToInt32(Session["UserId"]);

            //string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
            //string securityUrl = String.Format(baseURL + "/Security/Access-Denied/");

            ////pass eventid,userid and cookies to check User have autorise to access this page
            //if (userId != 0)
            //    security.CheckEventAccess(eventId, userId, Response, securityUrl);
        }
        #endregion
    }
}