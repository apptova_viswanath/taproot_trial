﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.usr
{
    public partial class SetUp : System.Web.UI.Page
    {

        public string _isSU = ConfigurationManager.AppSettings["IsSU"];

        protected void Page_Load(object sender, EventArgs e)
        {

            if (_isSU != null && _isSU.Equals("1"))
                hdnSUCompanyWelcome.Value = "1";
            else
                hdnSUCompanyWelcome.Value = "0";
        }
    }
}