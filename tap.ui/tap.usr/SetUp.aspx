﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetUp.aspx.cs" Inherits="tap.usr.SetUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Welcome TapRooT®</title>
    <script type="text/javascript">
        var _baseURL = '<%= ResolveUrl("~/") %>';
    </script>
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <!--Page Scripts -->
    
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.wel.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/master.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/SetUpPage.css") %>" rel="stylesheet"
        type="text/css" />
      <script id="ForgotPasswordJS" type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.policy/ValidPasswordPolicy.js") %>">
        {
            "txtPassword":"txtPassword", 
            "txtConfirmPassword":"txtConfirmPassword"

        }
          </script>
    <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />
    <style>
        #pswd_info {
            position: absolute;
            margin-left: 20%;
            width: 250px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
        }

            #pswd_info::before {
                content: "\25B2";
                position: absolute;
                top: -12px;
                left: 45%;
                font-size: 14px;
                line-height: 14px;
                color: #ddd;
                text-shadow: none;
                display: block;
            }

        .invalid {
            background: url('../../../Images/Delete.png') no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #ec3f41;
        }

        .valid {
            background: url('../../../Images/green-check.png') no-repeat 0 50%;
            padding-left: 22px;
            line-height: 24px;
            color: #3a7d34;
        }

        #pswd_info {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form" runat="server">
        <asp:HiddenField ClientIDMode="Static" ID="hdnSUCompanyWelcome" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnEUCompanyWelcome" runat="server" />

        <div class="wrapper">

            <div id="TapRooTHeader" runat="server">
                <div>
                    <!-- top menu -->
                    <div class="TapRooTHeader">
                        <div>
                            <a href="<%= ResolveUrl("~/")%>" class="TapRooTLogo" title="Home" id="TapRootLogoLink"></a>

                            <!-- navigation tabs starts -->
                        </div>
                    </div>


                </div>
                <div class="Blue-Bar">
                </div>

            </div>
            <!-- site body-->
            <div class="welcomeTapRooTMsg">
                <div id="LoginHeader">
                    Welcome to TapRooT® Software
                </div>

                <div class="siteContent divSteps">
                    <div class="ClearAll subDivSteps">
                        <ol class="progtrckr" data-progtrckr-steps="2">
                            <li id="liCreateDivsion" class="progtrckr-todo">Company Profile</li>
                            <li id="liDivisionCreateUsers" class="progtrckr-todo">Authentication</li>
                        </ol>
                    </div>
                    <div id="divNewDivision" class="divNewOrganization">
                        <div class="creatMessage">
                            You are about to create a new company. Please enter some basic company information.
                        </div>
                        <div class="ClearAll">

                            <div class="MarginTop10">
                                <label id="Label5" class="ContentUserLabel Width220">
                                    Company Name:<span id="spnCompanyName" style="color: Red">*</span>
                                </label>
                                <input id="companyName" type="text" class="Width300" autofocus="" />
                            </div>

                            <div class="MarginTop10">
                                <label id="" class="ContentUserLabel Width220">
                                    Phone Number:
                                </label>
                                <input id="companyPhoneNumber" type="text" class="Width300" runat="server" />
                            </div>
                            <div class="MarginTop10">
                                <label id="Label6" class="ContentUserLabel Width220">
                                    Email:
                                </label>
                                <input id="companyEmail" type="text" class="Width300" />
                            </div>
                            <div class="MarginTop10">
                                <label id="Label3" class="ContentUserLabel Width220">
                                    Company Website:
                                </label>
                                <input id="companyWebsite" type="text" class="Width300" runat="server" />
                            </div>
                            <div class="MarginTop10">
                                <label id="Label4" class="ContentUserLabel Width220">
                                    Company Address:
                                </label>
                                <textarea id="companyAddress" class="Width300" runat="server"></textarea>
                            </div>
                            <div class="MarginTop10">
                                <label class="ContentUserLabel Width220">Users will be managed by:</label>
                                <div class="ContentUserLabel">
                                    <label class="ContentUserLabel Width220">Active Directory</label>
                                    <input type="radio" id="chbxActiveDirectory" name="AD" class="ActiveDirectoryInput" value="0" />
                                    <br />
                                    <label class="ContentUserLabel Width220">TapRooT&#174; Software</label>
                                    <input type="radio" id="chbxTapRooTSoftware" name="AD" class="ActiveDirectoryInput" checked value="1" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divCreateUsers" class="divNewAdmin">
                        <div class="creatMessage">
                            You are about to create an admin user account for this company.
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="lblName" class="ContentUserLabel  Width220">First Name<span id="spnFirstName" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtFirstName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label7" class="ContentUserLabel Width220">Last Name<span id="spnLastName" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtLastName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label8" class="ContentUserLabel Width220">Email<span id="spnEmail" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtEmail" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label9" class="ContentUserLabel Width220">Phone</label>
                        </div>
                        <div class="divLabel">
                            <input id="txtPhone" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label10" class="ContentUserLabel Width220">User Name<span id="spnUserName" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtUserName" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="text" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label11" class="ContentUserLabel Width220">Password<span id="spanPassword" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" />
                                                 <div id="pswd_info" style="display: none">
        <h4>Password must meet the following requirements:</h4><br />
        <ul>
            
            <li id="lowerCase" class="invalid">At least <strong> <span id="spnCountLowerCase">1</span> lowercase letter<span id="lowerS" >s</span></strong></li>
            <li id="upperCase" class="invalid">At least <strong><span id="spnCountUpperrCase">1</span> uppercase letter<span id="upperS" >s</span></strong></li>
            <li id="number" class="invalid">At least <strong><span id="spnCountNumer">1</span> number<span id="numberS" >s</span></strong></li>
            <li id="length" class="invalid">Be at least <strong><span id="spnLength">6</span> character<span id="minS" >s</span></strong></li>
            <li id="maxCharacter" class="invalid">No longer than <strong><span id="spnMaxCharacterCount">6</span> character<span id="maxS" >s</span></strong></li>
            <li id="specialCharacter" class="invalid">At least <strong><span id="spnSpecialCharacterCount">6</span> special character<span id="specialS" >s</span></strong></li>
        </ul>
                         </div>
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="Label12" class="ContentUserLabel Width220">Confirm Password<span id="span2" style="color: Red">*</span></label>
                        </div>
                        <div class="divLabel">
                            <input id="txtConfirmPassword" class="ContentInputTextBox Width300 trackChanges marginBottom10" type="password" />
                        </div>
                        <div class="divLabel ClearAll" style="display: none;">
                            <label id="lblStatus" class="ContentUserLabel">Status<span id="Span1" style="color: Red"></span></label>

                        </div>
                        <div class="divLabel" style="display: none;">
                            <input type="checkbox" runat="server" checked="checked" id="chkActive" style="margin-left: 20px;" />

                        </div>
                        <div class="divLabel ClearAll" style="display: none;">
                            <label id="Label13" class="ContentUserLabel">Admin<span id="Span3" style="color: Red"></span></label>

                        </div>
                        <div class="divLabel" style="display: none;">
                            <input type="checkbox" runat="server" id="Checkbox1" style="margin-left: 20px;" />

                        </div>
                    </div>
                    <div id="divActiveDirectorySetting" class="divActiveDirectorySettings">
                        <div class="creatMessage">Provide Active Directory Settings</div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="lblServer" class="ContentUserLabel  Width220">Server<span id="spnServer1" style="color: Red">*</span></label>
                            <input type="text" name="txtServer" id="txtServer" class="ContentInputTextBox Width300 trackChanges marginBottom10"
                                runat="server" onkeydown="if (event.keyCode == 13) return false;" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="lblDomain" class="ContentUserLabel  Width220">Domain<span id="spnDomain" style="color: Red">*</span></label>
                            <input type="text" name="txtDomain" id="txtDomain" class="ContentInputTextBox Width300 trackChanges marginBottom10"
                                runat="server" onkeydown="if (event.keyCode == 13) return false;" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="lblGroupName" class="ContentUserLabel  Width220">Group(s) Name<span id="spnGroupName" style="color: Red">*</span></label>
                            <input type="text" name="txtGroupName" id="txtGroupName" class="ContentInputTextBox Width300 trackChanges marginBottom10"
                                runat="server" onkeydown="if (event.keyCode == 13) return false;" />
                        </div>
                        <div class="divLabel ClearAll MarginTop10">
                            <label id="lblTaprootAdmin" class="ContentUserLabel  Width220">TapRooT&#174; Admin User<span id="spnAdminUser" style="color: Red">*</span></label>
                            <input type="text" name="txtTaprootAdmin" id="txtTaprootAdmin" class="ContentInputTextBox Width300 trackChanges marginBottom10"
                                runat="server" onkeydown="if (event.keyCode == 13) return false;" />
                        </div>
                        <hr />
                        <div class="MarginTop10">
                                <label id="" class="ContentUserLabel Width220">
                                    AD Query User:
                                </label>
                                <input id="txtpUsername" type="text" class="Width300" runat="server" />
                            </div>
                            <div class="MarginTop10">
                                <label id="" class="ContentUserLabel Width220">
                                    AD Query Password:
                                </label>
                                <input id="txtpPassword" type="password" class="Width300" runat="server" />
                            </div>
                        <%--<div class="divLabel ClearAll MarginTop10">
                            <label id="lblTaprootAdminPassword" class="ContentUserLabel  Width220">Password<span id="spnAdminPassword" style="color: Red">*</span></label>
                            <input type="password" name="txtTaprootAdminPass" id="txtTaprootAdminPass" class="ContentInputTextBox Width300 trackChanges marginBottom10"
                                runat="server" onkeydown="if (event.keyCode == 13) return false;" />
                        </div>--%>
                        <br />
                    </div>
                    <div class="ClearAll divContinueMsg" id="toContinueText">
                        To continue, click Next
                    </div>
                    <div class="ClearAll divNextPrevBtn">

                        <button id="divisionPreviousBtn" class="btn btn-small btn-primary divBackBtn">< Back</button>
                        <button id="divisionNextBtn" class="btn btn-small btn-primary">Next ></button>
                        <button id="btnpTest" class="btn btn-primary btn-small btnADTestBtn">Test Active Directory Settings</button>
                        <img src="<%=ResolveUrl("~/images/progress2.gif")%>" id="orgWorkProgressImage" />
                        <input type="button" id="SaveCompanyWithAdmin" class="btn btn-small btn-primary" value="Create" style="display: none;">
                    </div>


                    <%-- Test Active Directory settings --%>
                    <div class="demo hidden" style="overflow: hidden;">
                        <div id="divActiveDirectoryTestDialog" title="Test Active Directory Settings" class="width220" 
                            style="display:none">
                            
                            <div class="ClearAll MarginTop10" style="text-align:center;">
                                <button class="btn btn-primary btn-small" id="btnpCancel">Cancel</button>
                                <button class="btn btn-primary btn-small" id="btnTestSettings">Test</button>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <div id="TapRooTFooter" runat="server">
                        <div class="TapRooTFooter">
                            <div class="copyright">
                                <p>
                            © Copyright 2015  System Improvements Inc. All Rights Reserved.
                                </p>
                            </div>
                        </div>
                    </div>
        </div>
    </form>
</body>
</html>