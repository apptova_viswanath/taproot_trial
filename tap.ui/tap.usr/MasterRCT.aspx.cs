﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.ui.usr
{
    public partial class MasterRCT : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            //on click of RCT popup link from TapRooT tab - check if session expires then close the popup window and redirec to Login Page.
            if ((Session.Count == 0) || (Convert.ToString(Session["CompanyId"]) == string.Empty))
            {
                Response.Write("<script>window.close();</" + "script>");

                //refresh parent page to get saved report
                Response.Write("<script>window.opener.location.reload(true);</" + "script>");
                Response.End();
            }


            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            dom.gen.Security security = new dom.gen.Security();
            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            int eventId = int.Parse(strEventId);
            int userId = Convert.ToInt32(Session["UserId"]);
            hdnUserId.Value = Convert.ToString(Session["UserId"]);

            //pass eventid,userid and cookies to check User have autorise to access this page
            if (userId != 0)
                security.CheckEventAccess(eventId, userId, Response);

            //Page Title
            tap.dom.usr.Event _event = new dom.usr.Event();
            if (strEventId != null && !strEventId.Equals("0"))
            {
                string EventName = _event.getEventName(Page.RouteData.Values["EventId"].ToString());
                Page.Title = "Root Cause Tree®" + ((EventName != "") ? " - " + EventName : "");
            }
        }
    }
}