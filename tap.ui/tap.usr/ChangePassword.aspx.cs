﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom.gen;

namespace tap.ui.usr
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //hdnCompanyId.Value = Convert.ToString(Session["CompanyId"]); 

            tap.dom.usr.Users user = new tap.dom.usr.Users();


            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            if (url.Contains('?'))
            {
                tap.dom.gen.Cryptography _cryptography = new Cryptography();
                string[] encryptUrl = url.Split('?');
                string DecryptURL = Convert.ToString(_cryptography.Decrypt(encryptUrl[1]));
                string[] splitDecryptUrl = DecryptURL.Split(',');
                string dt = Convert.ToString(splitDecryptUrl[1]);
                string CompanyId = Convert.ToString(splitDecryptUrl[2]);
                string[] datetimeSplit = dt.Split(' ');

                DateTime expiryDate = Convert.ToDateTime(dt);

                bool expireTime = ((DateTime.Compare(expiryDate, DateTime.Now) <= 0) ? true : false);
                if (expireTime)
                    Response.Redirect("~/tap.usr/Login.aspx?message=2");
                else
                {
                    txtChnPwdUserName.Value = splitDecryptUrl[0];
                    txtCompanyIdCnange.Value = CompanyId;
                }


            }
            else
            {
             
                txtCompanyIdCnange.Value = Session["companyid"].ToString();
            }
                


  


        }

        //protected void btnclearSessions_Click(object sender, EventArgs e)
        //{

        //    //Clears the session
        //    Session.Abandon();
        //    Session.Clear();

        //   // FormsAuthentication.SignOut();

        //    //Redirects to the login page.                
        //    System.Web.Security.FormsAuthentication.SignOut();
        //    System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        //}
    }
}