﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custom-Report-Template.aspx.cs" Inherits="tap.ui.usr.Report_Print_Data" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title></title>
    <!---Generalized  Function scripts -->
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
        <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/root.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <!-- Activity Logging --->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <!--  For notifying  --------------->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <!-- Notification plugin (modified plugin files) ------------->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/js.noty/jquery.notify.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/themes/default.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/layouts/center.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CustomReport.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

        function ShowProcess() {

            $('#lblProcessing').show();
        }
    </script>
    <style>
        html {
            background-color: #ffffff;
            background-image: none;
        }

        .ui-accordion .ui-accordion-header{padding:0px;}

        /*custom theme is overiding some classes of Accordians*/
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    background: url("images/ui-bg_highlight-soft_75_c7c7c7_1x100.png") repeat-x scroll 50% 50% #c7c7c7;
    color: #4d4d4d;
    font-weight: normal;
}
        .ui-dialog-title{font-size:12px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <asp:HiddenField ClientIDMode="Static" ID="hdnDivisionId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnCompanyId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnEventId" runat="server" />

        <asp:HiddenField ClientIDMode="Static" ID="hdnUserId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnImageAccordianSize" runat="server" />
         <asp:HiddenField ClientIDMode="Static" ID="hdnCompanyName" runat="server" />
        <div class="siteContent">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="ContentWidth ClearAll">
                <div class="divAccordian">
                    <div id="divAccordian" style="clear: both">
                    </div>
                </div>
                <div class="divMainToolBox">
                    <div id="divToolBox"></div>
                    <div id="divSavePreview">
                        <div id="divSaveIcon" title="Save Report"></div>
                        <div id="divPreviewicon" title="Preview Report"></div>
                    </div>
                    <br />
                    <div id="colorCop"></div>
                </div>
                <div class="headerReportArea">
                    <div id="divHeader" class="height15">
                        <div id="divCompanyLogo" class="divCompanyLogo hide">
                            <asp:Image ID="imgCompanyLogo" runat="server" />
                        </div>
                        <div id="divTitle" class="notranslate">
                            <span class="ReportTitle CustomReportTitle hidden " id="spnReportTitle" style="min-height:25px;"></span>
                            <input type="text" class="ReportTitle CustomReportTitle hidden" style="min-height:25px;" autocomplete="off" id="txtRptTitle" />
                        </div>
                        <div id="divPageTitle">
                            <div id="divCompany">
                                <span runat="server" id="spnCompanyName"></span>
                            </div>
                            <div id="divDate">
                                <label>Date : </label>
                                <span runat="server" id="spnCreatedDate"></span>
                            </div>
                        </div>
                    </div>

                    <div id="reportArea" class="margin14 notranslate" style="padding-left:10px;">
                    </div>
                </div>

            </div>

            <div class="demo hidden" style="overflow: hidden;">
                <div id="divCreateEditReportDialog" class="width100">
                    <div>
                        <label id="lblReportName" class="ContentUserLabel">Report Name<span id="spnReportName" style="color: Red">*</span></label>
                        <input type="text" id="txtReportName" class="ContentInputTextBox " />
                    </div>
                    <div>
                        <label id="lblReportDescription" class="ContentUserLabel">Description</label>
                        <textarea id="txtReportDescription" class="ContentInputTextBox"></textarea>
                    </div>

                    <div class="centerButton">
                        <input type="button" class="btn btn-primary btn-small btn-float" id="btnReportTitleClose" value="Cancel" />
                        <input type="button" class="btn btn-primary btn-small btn-float" id="btnReportName" />
                    </div>
                </div>

            </div>

            <div class="demo hidden" style="overflow: hidden;">
                <div id="divReportIntegrateDialog" title="TapRooT® Items">

                    <div>
                        <input type="radio" id="rdCausalFactor" checked="checked" name="rdIntegrate" value="Causal Factor" />
                        <span>By Causal Factor</span>

                    </div>
                    <div>

                        <input type="radio" id="rdRootCause" name="rdIntegrate" value="Root Cause" />
                        <span>By Root Cause</span>

                    </div>
                    <div>
                        <input type="radio" id="rdActionPlan" name="rdIntegrate" value="Action Plan" />
                        <span>By Action Plan</span>
                    </div>
                    <div class="centerButton" style="width: 50%; margin-top: 20px;">

                        <input type="button" class="btn btn-primary btn-small btn-float" id="btnIntegrateOK" value="OK" />
                        <input type="button" class="btn btn-primary btn-small btn-float" id="btnIntegrateClose" value="Cancel" />
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="report-image-madal" class="hidden" style="overflow: hidden;" title="Add Report Image">
                        <iframe id="ifReportImage" style="overflow: hidden; height: 430px; width: 400px" marginwidth="0"  marginheight="0" align="top" scrolling="No" frameborder="0" hspace="0" vspace="0"></iframe>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>



    </form>

    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.report.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.report/js.cst.com.rpt.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.cst.toolbox.js") %>" type="text/javascript"></script>

    <script type="text/javascript">
        //load function
        $(document).ready(function () {

            $('#colorCop').hide();

            //load image buttons
            ShowButtons();

            //load color cop codes
            ShowColorCops();

        });
    </script>
</body>
</html>
