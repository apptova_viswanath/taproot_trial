﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true"
    CodeBehind="InvestigationProcess.aspx.cs" Inherits="tap.ui.usr.InvestigationProcess" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <!--Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.process/js.inv.prc.js") %>" type="text/javascript"> </script>
    <div class="siteContent">
        <div id="tabs">
            <uc:ucEvent ID="ucevent" runat="server" />
            <div class="ContentWidth">
                <div class="ClearAll">
                </div>
                <div class="ContentHeaderInvProcess">
                    Investigation Process Flow:</div>
                <div class="PaddingLeft20">
                    <label id="lblInvestiagtionTitle" class="UserLabel">
                        Investigation:</label>
                    <label id="lblInvestiagationName" class="UserLabel" runat="server">
                    </label>
                </div>
                <div class="InvProcessStep  ClearAll MarginTop40">
                    <ul class="InvProcessHeader">
                        <li style="width: 240px; border: 0px solid red; text-align: center">
                            <label id="lblStepCompleted" class="InvProcessLabel">
                                Step Completed</label></li>
                        <li style="width: 320px; border: 0px solid red; text-align: center">
                            <label id="lblProcessStep" class="InvProcessLabel">
                                Process Step</label></li>
                        <li style="width: 200px; border: 0px solid red; text-align: right">
                            <label id="lblTechniques" class="InvProcessLabel">
                                Techniques</label>
                        </li>
                        <li class="InvProcessNo" style="width: 165px; border: 0px solid red; text-align: right">
                            <label id="lblManageDocs" class="InvProcessLabel">
                                Manage Documents</label>
                        </li>
                    </ul>
                    <li>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkInvGetStarted" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                1 </li>
                            <li class="InvProcessSteps">
                                <label id="lblInvGetStarted">
                                    <br />
                                    Plan Investigation - Get started</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblInvGetStart">
                                    Spring SnapCharT<br />
                                    Root Cause Tree<br />
                                    Equifactor</label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows" style="min-height: 100px">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkSeqEvents" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                2 </li>
                            <li class="InvProcessSteps">
                                <label id="lblSeqEvents">
                                    <br />
                                    Determine Sequence of Events</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblSeqEvent">
                                    Summer SnapCharT<br />
                                    Equifactor<br />
                                    CHAP<br />
                                    Change Analysis</label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkCausalFact" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                3 </li>
                            <li class="InvProcessSteps">
                                <label id="lblCausalFacts">
                                    <br />
                                    Define Causal Factors</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblCausalFact">
                                    Autumn SnapCharT<br />
                                    Equifactor<br />
                                    Safegaurds Ananlysis</label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkRootCausal" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                4 </li>
                            <li class="InvProcessSteps">
                                <label id="lblRootCausals">
                                    <br />
                                    Analyze each Causal Factor's Root Causes</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblRootCausal">
                                    <br />
                                    RooT Cause Tree</label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkGenericCause" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                5 </li>
                            <li class="InvProcessSteps">
                                <label id="lblGenericCauses">
                                    <br />
                                    Analyze each Root Cause's Generic Causes</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblGenericCause">
                                    <br />
                                    RooT Cause Tree<br />
                                    Corrective Action Helper</label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkEvalCAP" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                6 </li>
                            <li class="InvProcessSteps">
                                <label id="lblEvalCAPs">
                                    <br />
                                    Develop & Evaluate Corrective Actions</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblEvalCAP">
                                    Corrective Action Helper<br />
                                    SMARTER MATRIX<br />
                                    Safegauds Analysis
                                </label>
                            </li>
                        </ul>
                        <ul class="InvProcessRows">
                            <li class="InvProcessCheckBox">
                                <br />
                                <input type="checkbox" id="chkImplementCAP" />
                            </li>
                            <li class="InvProcessNo">
                                <br />
                                7 </li>
                            <li class="InvProcessSteps">
                                <label id="lblImplementCAPs">
                                    <br />
                                    Present/Report & Implement Corrective Actions</label></li>
                            <li class="InvProcessTechniques">
                                <label id="lblImplementCAP">
                                    <br />
                                    Winter SnapCharT<br />
                                    Software Reports
                                </label>
                            </li>
                        </ul>
                    </li>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
