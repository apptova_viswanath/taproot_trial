﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom.gen;

namespace tap.usr
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tap.dom.usr.Users user = new tap.dom.usr.Users();
            

            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            if (url.Contains('?'))
            {
                tap.dom.gen.Cryptography _cryptography = new Cryptography();
                string[] encryptUrl = url.Split('?');
                string DecryptURL = Convert.ToString(_cryptography.Decrypt(encryptUrl[1]));
                string[] splitDecryptUrl = DecryptURL.Split(',');
                DateTime expiryDate = Convert.ToDateTime(splitDecryptUrl[1]);
                bool expireTime = ((DateTime.Compare(expiryDate, DateTime.Now) <= 0) ? true : false);
                ShowHideEmailDetails(expireTime);
                             
                hdnUserName.Value = splitDecryptUrl[0];           

            }
            else
            {
                ShowHideEmailDetails();               
            }
        }
        public void ShowHideEmailDetails(bool isEmailDetailsShow)
        { 
            if(!isEmailDetailsShow)
            {
                 divMessage.Visible = false;
                divForgotPsw.Visible = false;
                 divForgotPasswordImage.Visible = false;
                 divResetPassword.Visible = true;
                divFgtSubmitBtn.Visible=false;
            }
            else
            {
                divMessage.Visible = true;
                divForgotPsw.Visible = true;
                divForgotPsw.Visible = true;
                divForgotPasswordImage.Visible = false;
                divResetPassword.Visible = false;
            }
        }
        public void ShowHideEmailDetails()
        {
            divMessage.Visible = false;
            divForgotPsw.Visible = true;
            divForgotPasswordImage.Visible = false;
            divResetPassword.Visible = false;
        }

        
    }
}