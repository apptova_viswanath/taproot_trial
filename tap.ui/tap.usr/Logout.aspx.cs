﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom.hlp;
using System.Web.Security;
#endregion

namespace tap.ui.usr
{
    public partial class Logout : System.Web.UI.Page
    {
        #region "Page Load event"
        protected void Page_Load(object sender, EventArgs e)
        {

            // Creates the Activity Log Object.
            ActivityLog activityLog = new ActivityLog();
            int companyId = Convert.ToInt32(Session["UserId"]);
            int userId = Convert.ToInt32(Session["CompanyId"]);
            string transactionMessage = string.Empty; 
            try
            {
                //Checks for session and saves the logout activity in the activity log.
                if (companyId != 0 && userId != 0)
                {                  

                    transactionMessage = Session["UserName"].ToString() + " Logged out.";

                    if (transactionMessage != string.Empty)
                    {
                        tap.dom.transaction.Transaction.SaveTransaction(0, userId, companyId, Enumeration.TransactionCategory.Create, transactionMessage);
                    }

                }

                //Clears the session
                Session.Abandon();
                Session.Clear();
               //clear all cookies
                string[] cookies = Request.Cookies.AllKeys;
                foreach (string cookie in cookies)
                {
                    
                    Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
                }

                FormsAuthentication.SignOut();

                //Redirects to the login page.                
                Response.RedirectToRoute("UserSignIn");
            }
            catch (Exception ex)
            {
                ErrorLog.LogException(ex);
            }


        }
        #endregion
    }
}