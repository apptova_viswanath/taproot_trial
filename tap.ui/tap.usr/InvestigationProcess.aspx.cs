﻿//---------------------------------------------------------------------------------------------
// File Name 	: InvestigationProcess.aspx.cs

// Date Created  : N/A

// Description   : 7 Steps page - Custom Tab (Dispaly Investigation Name)

// Javascript File Used : js.inv.prc.js
//---------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using tap.dom;
using tap.dom.hlp;
#endregion

namespace tap.ui.usr
{
    public partial class InvestigationProcess : System.Web.UI.Page
    {
        #region "Glogbal variables"
       
        JavaScriptSerializer _jsSerializer= new JavaScriptSerializer();
        int _eventId;
        #endregion

        #region "Page Load Events"

        /// <summary>
        ///Page_Load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            dom.gen.Security security = new dom.gen.Security();
            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            int eventId = int.Parse(strEventId);
            int userId = Convert.ToInt32(Session["UserId"]);

            //string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
            //string securityUrl = String.Format(baseURL + "/Security/Access-Denied/");

            //pass eventid,userid and cookies to check User have autorise to access this page
            if (userId != 0)
                security.CheckEventAccess(eventId, userId, Response);

            if (!IsPostBack)
            {
                PopulateEventName();
            }
        }

        #endregion

        #region "To populate Investigation Name"

        /// <summary>
        ///  PopulateEventName()
        /// </summary>
        public void PopulateEventName()
        {
            tap.dom.usr.Event _events = new dom.usr.Event();
            int.TryParse(Page.RouteData.Values["EventId"].ToString(), out _eventId);
            var eventData = _events.GetEventDataByEventType(_eventId, (int)Enumeration.Modules.Investigation);

            if (!string.IsNullOrEmpty(eventData) && eventData != "[]")
            {
                var jsonEventData = _jsSerializer.Deserialize<dynamic>(eventData);

                if (!string.IsNullOrEmpty(jsonEventData[0]["Name"]))
                    lblInvestiagationName.InnerHtml = jsonEventData[0]["Name"];
            }
        }

        #endregion
    }
}