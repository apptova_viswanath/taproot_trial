﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tap.ui.usr
{
    public partial class SampleCropImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string x = Request["x"];
            string y = Request["y"];
            string w = Request["w"];
            string h = Request["h"];
            try{
                
                string filePath = Path.Combine(Server.MapPath("~/images"), "dance.jpg");
                string cropFileName = "";
                string cropFilePath = "";

                if (File.Exists(filePath))
                {
                    System.Drawing.Image orgImg = System.Drawing.Image.FromFile(filePath);
                    Rectangle CropArea = new Rectangle(
                        Convert.ToInt32(x),
                        Convert.ToInt32(y),
                        Convert.ToInt32(w),
                        Convert.ToInt32(h));
                    try
                    {
                        Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
                        using (Graphics g = Graphics.FromImage(bitMap))
                        {
                            g.DrawImage(orgImg, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
                        }
                        cropFileName = "crop_dance.jpg";
                        cropFilePath = Path.Combine(Server.MapPath("~/images"), cropFileName);
                        bitMap.Save(cropFilePath);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
            }



        }
        
    }
}