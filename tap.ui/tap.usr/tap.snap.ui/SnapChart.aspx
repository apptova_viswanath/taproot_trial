﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SnapChart.aspx.cs" Inherits="tap.ui.usr.SnapChart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>SnapCharT®</title>

    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet" type="text/css" />
    <%-- Customized theme from jquery theme roller --%>
<%--    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />--%>   
     <%-- Customized theme ends  --%> 
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>" type="text/javascript"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery") %>/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"></script>
     <!-- jQuery UI Datepicker 1.8.16 --->
    <script src="<%=ResolveUrl("~/Scripts/js.date/ui/jquery.ui.datepicker.js") %>" type="text/javascript"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.common") %>/js.com.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.common") %>/js.native.ovrd.js"></script>

    <!--Notification Plugin -->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />

    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/styles/grapheditor.css" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Scripts/tap.js.snap/tap.stl.snap") %>/SnapchartStyles.css" />

    <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />

    <%--JCrop library for cropping the image.--%>
    <script src="<%=ResolveUrl("~/Scripts/jCrop/jquery.Jcrop.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jCrop/jquery.color.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jCrop/jquery.Jcrop.css") %>" rel="stylesheet" type="text/css" />
     
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />  
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet"  type="text/css" />
    <style>
      .ui-dialog-title, .dialogFonts{font-size:12px;}
      
    </style>
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        //Global variable for fixing graph selection pointer issue.
        var isGoogleTranslateOn = false;

        //google translation
        function googleTranslateElementInit() {

            //Confirmation for google translate is on
            isGoogleTranslateOn = true;
            //-------------------------------

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

        //Get the event id.
        var splittedURL = window.location.href.split('/');
        var eventId = splittedURL[splittedURL.length - 1];

        var MAX_REQUEST_SIZE = 10485760;
        var MAX_WIDTH = 6000;
        var MAX_HEIGHT = 6000;
        var DpCTypes;
        var _snapcapCTypes;
        // URLs for save and export
        var EXPORT_URL = '/export';

        //var SAVE_URL = '< %=ResolveUrl("~/tap.snap.hand") %>/SnapChartSave.ashx?EventId=' + eventId + '&seasonId='+ seasonId +'';  
        //var CAPTURE_URL = '< %=ResolveUrl("~/tap.snap.hand") %>/SnapChartSave.ashx';
        var OPEN_URL = '/open';
        var RESOURCES_PATH = '<%=ResolveUrl("~/Scripts/tap.js.snap") %>/tap.snap.res';
        var RESOURCE_BASE = RESOURCES_PATH + '/SnapchartResources';
        var STENCIL_PATH = '<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/stencils';
        var IMAGE_PATH = '<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/images';
        var STYLE_PATH = '<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/styles';
        var CSS_PATH = '<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/styles';
        var OPEN_FORM = 'open.html';
        var CAPTURE_URL = '<%=ResolveUrl("~/tap.snap.hand") %>/SnapChartSave.ashx';
        

        var tapAndHoldStartsConnection = true;
        var showConnectorImg = true;

        var urlParams = (function (url) {
            var result = new Object();
            var idx = url.lastIndexOf('?');

            if (idx > 0) {
                var params = url.substring(idx + 1).split('&');

                for (var i = 0; i < params.length; i++) {
                    idx = params[i].indexOf('=');

                    if (idx > 0) {
                        result[params[i].substring(0, idx)] = params[i].substring(idx + 1);
                    }
                }
            }

            return result;
        })(window.location.href);

        mxLoadResources = false;
        mxBasePath = "<%=ResolveUrl("~/Scripts/tap.js.snap") %>/js.mxgraph/src";
        mxLanguage = urlParams['lang'];
        mxLanguages = ['de'];

    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap") %>/js.snap.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap") %>/js.snap.ovrd.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap") %>/js.snap.sprt.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.common/js.fld.val.js") %>" ></script>
     <%------------------------LVC---------------------------------%>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/jscolor/jscolor.js"></script>
    <%-- Adds new reference --%>
    <%-- //OVC --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/sanitizer/sanitizer.min.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/figureout/js/mxClient.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/EditorUi.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Editor.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Sidebar.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Graph.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Shapes.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Actions.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Menus.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Toolbar.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Dialogs.js"></script>

    <%-------------------------LVC end----------------------------%>

    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/figureout/js/mxClient.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Toolbar.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Actions.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Dialogs.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Editor.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/EditorUi.js"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Sidebar.js"></script>--%>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Graph.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Menus.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Shapes.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/js/Sidebar.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/jscolor/jscolor.js"></script>--%>
    <%-- Adds new reference --%>
    <%-- //OVC --%>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/tap.js.snap/js.mxgraph") %>/baseresource/grapheditor/www/sanitizer/sanitizer.min.js"></script>--%>

</head>

<body>
    <form id="form1" runat="server">
        <asp:HiddenField ClientIDMode="Static" ID="hdnDivisionId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnCompanyId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnUserId" runat="server" />
        <asp:HiddenField ClientIDMode="Static" ID="hdnDateFormat" runat="server" />

        <input type="hidden" id="x" name="x" />
        <input type="hidden" id="y" name="y" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />

        <script type="text/javascript">

            // Extends EditorUi to update I/O action states
            (function () {

              
                var editorUiInit = EditorUi.prototype.init;

                EditorUi.prototype.init = function () {

                    editorUiInit.apply(this, arguments);
                    this.actions.get('export').setEnabled(false);

                    // Updates action states which require a backend
                    // if (!useLocalStorage) {
                    if (useLocalStorage) {

                        mxUtils.post(OPEN_URL, '', mxUtils.bind(this, function (req) {
                            var enabled = req.getStatus() != 404;
                            this.actions.get('open').setEnabled(enabled || fileSupport);
                            this.actions.get('import').setEnabled(enabled || fileSupport);
                            this.actions.get('save').setEnabled(enabled);
                            this.actions.get('saveAs').setEnabled(enabled);
                            this.actions.get('export').setEnabled(enabled);
                        }));
                    }
                };
            })();

            var snapData = '<% = SnapData %>';
            var seasonId = '<% = _season %>';
            seasonId = parseInt(seasonId);
            var isComplete = '<% = _isComplete %>';
            var _isWinterChartExists = '<% = _isWinterChart %>';
            var winterData = '<% = WinterData %>';

            //Global variables
            var seasonName = '';
            _snapcapNames = new Array;
            _presetTitleList = new Array;
            _snapcapCount = 1;
            _fileName = '';
            _natWidth = 0;
            _natHeight = 0;
            //Enable resources encoded to accept the unicode keys for symbols.
            mxResources.resourcesEncoded = true;
           
            //LVC
            //The graph extends in any direction.
            //Initially the graph is showing the scrollbars that too in the middle.
            Editor.prototype.extendCanvas = false;

            //Snapchart customization first part.
            SnapchartFirstCustomize();
            
            mxGraph.prototype.isCellSelectable = function (cell) {
                var state = this.view.getState(cell);
                var style = (state != null) ? state.style : this.getCellStyle(cell);
                return this.isCellsSelectable() && !this.isCellLocked(cell) && style['selectable'] != 0;
            };

            //newsc s
            //Container for Seasons in the sidebar .
            NewSeasonContainerInSidebar(editui);
            //newsc e

            //Add shapes and safeguards to the sidebar.
            editui.sidebar.addPalette('taprootshapes', 'Shapes', true, function (content) {

                var esb = editui.sidebar;
                var separator = ';';
                shapeStyle = 'whiteSpace=wrap'; //Auto wrap the text of the cell.
                //shapeStyle = 'whiteSpace=wrap;html=1'; //Auto wrap the text of the cell.

                //Safeguard variables.
                var sgw = 31;//Hard coded the safeguard width.
                var prefix = 'safeguard-';
                var shapeImage = 'shape=image;';
                var imagePath = 'image=<%=ResolveUrl("~/Images/safeguards") %>/';
                var bottomText = 'verticalLabelPosition=bottom;verticalAlign=top;';
                bottomText = bottomText + shapeStyle + separator;

                //Page Connector variables.
                var packageName = 'mxgraph.flowchart.';
                var stencilName = 'Off-page_Reference';
                var connStyle = ';fillColor=#ffffff;strokeColor=#000000;strokeWidth=2;' + shapeStyle;
                var connector = 'shape=' + packageName + stencilName.toLowerCase() + connStyle;
                
                //LVC
                //Adds the new parameter (true) at last, Show title .
                //Shapes of Rectangle, rectangle with rounded edges and circle.
                content.appendChild(esb.createVertexTemplate('rectangle' + separator + shapeStyle, 120, 60, '', 'Event', true, true));//Rectangle
                content.appendChild(esb.createVertexTemplate('rounded=1' + separator + shapeStyle, 120, 60, '', 'Condition', true, true));//rounded Rectangle
                content.appendChild(esb.createVertexTemplate('ellipse' + separator + shapeStyle, 80, 80, '', 'Incident', true, true));//Circle
                content.appendChild(esb.createVertexTemplate('text;align=center;whiteSpace=wrap;', 40, 26, 'Text', '', true, false));//Label control

                //Images of  good,new and fail safeguards.
                content.appendChild(esb.createVertexTemplate(bottomText + shapeImage + imagePath + prefix + 'good.gif', sgw, 80, '', 'Good Safeguard', true, false));
                content.appendChild(esb.createVertexTemplate(bottomText + shapeImage + imagePath + prefix + 'new.gif', sgw, 80, '', 'New Safeguard', true, false));
                content.appendChild(esb.createVertexTemplate(bottomText + shapeImage + imagePath + prefix + 'failed.gif', sgw, 80, '', 'Fail Safeguard', true, false));

                //Page Connector Shape (off-page reference).
                content.appendChild(esb.createVertexTemplate(connector, 60, 60, '', 'Page Connector', true, false));
            });

            //Container for the Optional techniques in the sidebar
            OptionalTechniquesContainerInSidebar();

            //Container for the Snapchart Versions in the sidebar
            SnapCharTVersionsContainerInSidebar();

            //Ajax async post call.               
            var userId = $('#hdnUserId').val();

            //Ajax post call for getting all the snapcaps titles for the event.
            function SnapcapsAllTitlesGet(userId, eventId) {

                var snapcapInfo = new snapcapDetails(parseInt(userId), eventId);
                var data = JSON.stringify(snapcapInfo);
                serviceurl = _baseURL + 'Services/SnapCharT.svc/SnapcapsTitlesByEventGet';
                PostAjax(serviceurl, data, eval(SnapcapsAllTitlesGetSuccess), eval(SnapcapsAllTitlesGetFail));

                function snapcapDetails(userId, eventId) {
                    this.userId = userId;
                    this.eventId = eventId;
                }

                function SnapcapsAllTitlesGetSuccess(result) {

                    if (result != null && result.d != null && result.d != undefined) {
                        _snapcapNames = JSON.parse(result.d);                        
                        //Clear the null values if exists in the _snapcapNames array.
                        _snapcapNames = _snapcapNames.filter(NullsClear);
                    }

                    function NullsClear(val) { return (val == undefined) ? 0 : 1; };
                }

                function SnapcapsAllTitlesGetFail(data) { }
            }

            //Snapchart customization second part.
            SnapchartSecondCustomize();

            //Toggle selection in the sidebar.
            SidebarSelectionToggle();

            //For converting the graph to an image.
            function ImageSave(graph, mxUtils, editor) {

                //Get all the snapcaps titles for the event.
                if (_snapcapNames.length == 0)
                    SnapcapsAllTitlesGet(userId, eventId);

                //Exact format to convert the graph as an image.
                var xmlDoc = mxUtils.createXmlDocument();
                var root = xmlDoc.createElement('output');
                xmlDoc.appendChild(root);

                var xmlCanvas = new mxXmlCanvas2D(root);
                var imgExport = new mxImageExport();
                imgExport.drawState(graph.getView().getState(graph.model.root), xmlCanvas);
                
                var bounds = graph.getGraphBounds();
                var w = Math.ceil(bounds.x + bounds.width);
                var h = Math.ceil(bounds.y + bounds.height);

                _natWidth = w;
                _natHeight = h;

                var xml = mxUtils.getXml(root);

                //Adds margin to the converted graph image.
                var convertedGrpahImageMargin = 40;
                //Image name for the converted graph image.
                var randNum = Math.floor((Math.random() * 100) + 1);
                _fileName = userId + '_' + randNum + '_convertedGraph.jpg';

                //Ajax call for convertion of graph to an image.
                var imageInfo = new ImageDetails('png', w, h, '#FFFFFF', xml, userId, _fileName, convertedGrpahImageMargin);
                var data = JSON.stringify(imageInfo);
                serviceurl = _baseURL + 'tap.snap.hand/SnapChartSave.ashx';
                PostAjax(serviceurl, data, eval(SnapChartimageSuccess), eval(SnapChartImageFail));

                function ImageDetails(format, w, h, bg, xml, userId, _fileName, convertedGrpahImageMargin) {
                    this.format = format;
                    this.w = w;
                    this.h = h;
                    this.bg = bg;
                    this.xml = xml;
                    this.userId = userId;
                    this.fileName = _fileName;
                    this.imagePad = convertedGrpahImageMargin;
                }
                
                function SnapChartimageSuccess(data) {
                   
                    //Hides the graph container.
                    $('.geDiagramContainer').hide();
                    $('.geToolbarContainer').hide();
                    $('.geMenubarContainer').hide();
                    $('.geHsplit').hide();
                    

                    //Show the footer .
                    $('.geFooterContainer').show();

                    //Removes the background div.
                    $('#bgGreyDiv').remove();
                     
                    var CAPTURE_IMAGE_PATH = '<%=ResolveUrl("~/temp") %>';
                    
                    var imageSource = CAPTURE_IMAGE_PATH + '/' + _fileName;
                    
                    var w = editui.container.clientWidth;                    
                    var h = GetIEVersion() > 0 ? document.documentElement.clientHeight : editui.container.clientHeight;                    
                    
                    var effHsplitPosition = Math.max(0, editui.hsplitPosition);

                    w = Math.max(0, w - effHsplitPosition) + 'px';
                    var diagramHeight = Math.max(0, h - editui.footerHeight - editui.menubarHeight - editui.toolbarHeight);
                    h = diagramHeight + 'px';

                    //Parse the values.
                    w = parseInt(w.substring(0, w.length - 2));
                    h = parseInt(h.substring(0, h.length - 2));

                    //Display the xml converted image in a dialog for capturing part of it.                    
                    var top = editui.diagramContainer.style.top;
                    top = parseInt(top.substring(0, top.length - 2)) + 'px';//Hard coded need to calculate.
                    var left = editui.diagramContainer.style.left;
                    left = parseInt(left.substring(0, left.length - 2));
                    left = left - 10 + 'px';//Hard coded need to calculate.
                    var bottom = editui.diagramContainer.style.bottom;

                    //Create background div.
                    var bgDiv = document.createElement('div');
                    bgDiv.setAttribute('id', 'bgGreyDiv');                   
                    bgDiv.style.width = _natWidth + 'px';
                    
                    if (_natWidth < editui.container.clientWidth) { //_natWidth < 1105
                        
                        bgDiv.style.width = '100%';
                        //bgDiv.style.width = editui.container.clientWidth + 'px';
                       
                        if (editui.container.clientHeight < _natHeight) {
                            
                            var ht = _natHeight + 100;
                            bgDiv.style.height = ht + 'px';
                        }

                        $('.geFooterContainer').hide();
                        
                    }
                    
                    //var bgWidth = _natWidth < 1105 && _natHeight < 460 ? editui.container.clientWidth : _natWidth;
                    //bgDiv.style.width = bgWidth + 'px';
                    
                    bgDiv.style.position = 'absolute';
                    bgDiv.style.background = '#DFDFDF';
                    bgDiv.style.left = '0px';
                    bgDiv.style.top = '0px';
                    bgDiv.style.bottom = '0px';
                    bgDiv.style.right = '0px';
                    mxUtils.setOpacity(bgDiv, 100);                    

                    /////////////////////////////////////////////////////////////////////////////////
                    //Exit from capture mode.
                    var closeDiv = document.createElement('div');
                    closeDiv.setAttribute('id', 'closeCaptureDiv');
                    closeDiv.setAttribute('style', 'height: 50px;');
                    //closeDiv.setAttribute('style', 'height: 50px;margin: 13px 5px 0px 0px;');                    



                    var exitButtonDiv = document.createElement('div');
                    exitButtonDiv.setAttribute('id', 'exitButtonDiv');
                    exitButtonDiv.setAttribute('style', 'width:200px;float: left;'); //width: 100px;
                    var closeBtn = document.createElement("BUTTON");
                    closeBtn.setAttribute('id', 'closeCaptureButton');
                    //closeBtn.setAttribute('style', 'display: block;width: 100px;border:2px solid green;float: left;');
                    //closeBtn.setAttribute('style', 'margin: 0px 0px 0px 202px;');
                    closeBtn.setAttribute('class', 'btn btn-primary btn-small');
                    closeBtn.setAttribute('onclick', 'CaptureModeExit()');
                    var closeText = document.createTextNode("Exit from Capture Mode");
                    closeBtn.appendChild(closeText);
                    exitButtonDiv.appendChild(closeBtn);
                    closeDiv.appendChild(exitButtonDiv);
                    //closeDiv.appendChild(closeBtn);

                    //Text for identification of snapcap mode .
                    var titleDiv = document.createElement('div');
                    titleDiv.setAttribute('id', 'snapcapTitleDiv');
                    titleDiv.setAttribute('style', 'margin:0px 0px 0px 200px;'); // text-align: center;
                    var snapcapTitle = document.createElement('p');
                    snapcapTitle.setAttribute('style', 'padding:15px 0px 0px 120px;font-weight: bold;font-size: 18px;position: relative;');
                    //snapcapTitle.setAttribute('style', 'padding: 0px 0px 0px 502px;font-weight: bold;font-size: 18px;position: relative;');
                    //snapcapTitle.setAttribute('style', 'text-align: center;font-weight: bold;font-size: 18px;position: relative;');
                    snapcapTitle.textContent = 'Click and drag to capture an image of your SnapCharT®';
                    titleDiv.appendChild(snapcapTitle);
                    closeDiv.appendChild(titleDiv);
                    //closeDiv.appendChild(snapcapTitle);

                    bgDiv.appendChild(closeDiv);


                    ///////////////////////////////////////////////////////////////////////////

                    ////create a div for displaying the image.
                    //var div = document.createElement('div');
                    //div.setAttribute('id', 'imagePlaceDiv');
                    //div.style.width = w + 'px';
                    //div.style.height = h + 'px';
                    //div.style.left = left;
                    //div.style.top = top;                    
                    //div.style.position = 'absolute';
                    //div.style.overflow = 'auto';

                    var elem = document.createElement('img');
                    elem.setAttribute('id', "cropbox");
                    elem.src = imageSource;
                    elem.setAttribute('style','overflow:auto');
                    //For large images
                    if (_natWidth > 1105 && _natHeight > 460) {                        
                        elem.style.width = _natWidth + 'px';
                        elem.style.height = _natHeight + 'px';
                        //Hide the footer
                        $('.geFooterContainer').hide();
                    }
                            

                    ////Insert an image into the div.
                    //var imageDiv = document.createElement('div');                    
                    //imageDiv.style.width = w + 'px';
                    //imageDiv.style.height = h + 'px';                    
                    //imageDiv.style.overflow = 'scroll';                    
                    //imageDiv.style.background = '#DFDFDF';

                    //imageDiv.appendChild(elem);
                    //div.appendChild(imageDiv);


                    /////////////////////////////////////////////////////////
                    var div1 = document.createElement('div');
                    div1.setAttribute('id', 'imageActualDiv');                    
                    div1.style.width = _natWidth + 'px';
                    div1.style.height = _natHeight + 'px';
                    ////////////////////////////////////////////////////////
                    div1.appendChild(elem);
                    bgDiv.appendChild(div1);
                    document.body.appendChild(bgDiv);
                    
                }

                function SnapChartImageFail() {
                }


                /*                  
                  trueSize: [_natWidth, _natHeight],                   
                 */
                var jcrop_api;
                //Cropping functionality
                $('#cropbox').Jcrop({
                    keySupport: false, //Fix : For cropping the exact selected area in chrome and MAC
                    boxWidth: _natWidth, //strict
                    boxHeight: _natHeight, //strict
                    onSelect: updateCoords
                }, function () {
                    jcrop_api = this;
                });
                
                ////set up size;
                //$("#imgPlaceDiv.img, #jcrop-holder img, #cropbox").prop("width", "3412px");
                //$("#imgPlaceDiv.img, #jcrop-holder img, #cropbox").prop("height", "1333px");
                //$("#imgPlaceDiv.img").prop("border", "solid 1px red;");

                //Ajax async post call for saving the cropped image.
                function updateCoords(c) {

                    //Removes the dynamically added div if exists.
                    if (document.getElementById("captureSelection") !== null) {

                        $("div").remove("#captureSelection");
                    }

                    //Create icons for capture and close the image.
                    CaptureCloseIconsCreate();

                    //close functionality
                    $('#closeImage').mouseup(function (e) {

                        jcrop_api.release();
                        return false;
                    });

                    //Image crop functionality.
                    $('#captureImage').mouseup(function (e) {
                        var snapcapInfo = new snapcapDetails(parseInt(userId), eventId);
                        var data = JSON.stringify(snapcapInfo);
                        var serviceURL = _baseURL + 'Services/SnapCharT.svc/SnapcapsCommonTypesGet';
                        function snapcapDetails(userId, eventId) {
                            this.userId = userId;
                            this.eventId = eventId;
                        }
                        AjaxPost(serviceURL, data, eval(SnapcapsCommonTypesGetSuccess), eval(SnapcapsCommonTypesGetFail));
                        function SnapcapsCommonTypesGetSuccess(result) {

                            if (result != null && result.d != null && result.d != undefined) {                                
                                _snapcapCTypes = JSON.parse(result.d);
                            }
                            
                            DpCTypes = "<select class='ContentInputTextBoxSnapCap Width200'  id=snapcapCommonTypes  disabled style='margin-top: 0px; margin-left: 25px; display: inline;'>";
                           
                            DpCTypes += "<option  style='display: none !important'>Select</option>";
                            for (var i = 1; i <= _snapcapCTypes.length; i++) {

                                var isAlreadyUsedStyle;
                                var val = _snapcapCTypes[i - 1].isAlreadyUsed;
                                if (val == 1)
                                    isAlreadyUsedStyle = "color: #BCC6CC";
                                else
                                    isAlreadyUsedStyle = "";
                                DpCTypes += "<option style='"+isAlreadyUsedStyle+"' value=" + _snapcapCTypes[i - 1].SnapCapCommonTypeId + " >" + _snapcapCTypes[i - 1].SnapCapCommonTypeName + "</option>";
                            }
                            DpCTypes += "</select>";

                            //TODO : Remove the predefined title related code .

                            //Filter the snapcaps to get exactly SnapCap space and a number.
                            var filteredSnapCaps = _snapcapNames.filter(isSnapCap);
                            function isSnapCap(element) {
                                return element.match(/^SnapCap [0-9]{0,9}$/);
                            }

                            SnapCapNumbersList = new Array;

                            //Extract the numbers from the snapcaps.
                            for (var i = 0; i < filteredSnapCaps.length; i++) {
                                SnapCapNumbersList.push(Number(filteredSnapCaps[i].substring(7)));
                            }

                            //Get the missing number in the sequence.
                            function SnapCapsSequenceMissingNumber(scaps) {
                                for (var i = 0; i < scaps.length; i++) {
                                    if (i + 1 !== scaps[i]) {
                                        return i + 1;
                                    }
                                }
                            }

                            //Find the missing number in the sequence.
                            _snapcapCount = SnapCapsSequenceMissingNumber(SnapCapNumbersList.sort(function (a, b) { return a - b }));

                            //Get the exact number to preset the snapcap title.
                            _snapcapCount = _snapcapCount == undefined && SnapCapNumbersList.length > 0 ? SnapCapNumbersList[SnapCapNumbersList.length - 1] + 1 : _snapcapCount;

                            _snapcapCount = _snapcapCount == undefined ? 1 : _snapcapCount;

                            
                            _predefinedTitle = 'SnapCap' + ' ' + _snapcapCount;
                            //Can use global variable instead of Array, _presetTitleList
                            _presetTitleList.push(_predefinedTitle);

                            //Display a dialog for saving snapcap.   
                            if ($('#snapcapTitle').length) {
                                $('#snapcapTitle').remove();
                                $('#snapcapdialog').remove();
                            }

                            ////added common type drop down by deepthi on 04 aug 2014
                        
                            //adding radio button for snapcap name
                            var Controls = "<div style='display: inline'> <input  type='radio' style='margin-right: 10px;' id='RdSnapCapName' name='RdSnapCaps' value='Snapcaps' checked='checked'></input></div>" +
                            
                            //Allow user to edit the snapcap name .
                            "<div style='display: inline' id='DivsnapcapTitle'><label id ='lblsnapcapTitle'>SnapCap name</label> <input class='ContentInputTextBox WidthP40 snapCapAlign' id='snapcapTitle' type='text' ></div>" +

                            //adding snapcap name and text feild
                            //"<div style='display: inline' id='DivsnapcapTitle'><label id ='lblsnapcapTitle'>SnapCap name</label> <input class='ContentInputTextBox WidthP40 snapCapAlign' id='snapcapTitle' type='text' value='" + _predefinedTitle + "' readonly></div>" +
                            "</br></br>"+
                            //adding radio button for common types
                            "<div style='display: inline'><input type='radio' id='RdSnapCapCtype' style='margin-right: 10px;' name='RdSnapCaps' value='CommonTypes'></input></div>" +
                            //adding drop down for common types
                            "<div style='display: inline' id = 'DivsnapcapCommonTypes'  ><label  style='color: #717171' id ='lblCommonTypes' >Common Type</label> " + DpCTypes + "</div>";

                            var addControls = "<div>" + Controls + "</div>";

                            $(document).ready(function () {

                                $('input[type=radio]').live('change', function (e) {


                                    if (this.value == 'Snapcaps') {                                        
                                        document.getElementById("snapcapCommonTypes").disabled = true;
                                        document.getElementById("snapcapTitle").disabled = false;
                                        document.getElementById("lblCommonTypes").style.color = "#717171";
                                        document.getElementById("lblsnapcapTitle").style.color = "Black";
                                        
                                        var e = document.getElementById("snapcapCommonTypes");
                                        e.selectedIndex = 0;
                                    }
                                    else if (this.value == 'CommonTypes') {
                                        
                                        document.getElementById("snapcapTitle").disabled = true;
                                        document.getElementById("snapcapCommonTypes").disabled = false;
                                        document.getElementById("lblsnapcapTitle").style.color = "#717171";
                                        document.getElementById("lblCommonTypes").style.color = "Black";
                                    }
                                });
                            });
                           
                            var SnapCapDialog = $("<div id='snapcapdialog'></div>").html(addControls);
                            SnapCapDialog.dialog({
                                title: "Save SnapCap",
                                modal: true,
                                width: 500,
                                dialogClass: 'dialogFonts',
                                buttons: [{ text: "Save", id:"btnSaveSnapcap", click: function () { SnapCapSave(c); } }, { text: "Cancel", id:"btnCancelSnapcap", click: function () { $(this).dialog("close"); } }]
                            });
                            // for adding default custom button style by deepthi
                            $("#btnSaveSnapcap, #btnCancelSnapcap").removeClass().addClass("btn btn-primary btn-small");
                            $("#btnSaveSnapcap, #btnCancelSnapcap").button().hover(function (e) {
                                $(this).removeClass("ui-state-hover");
                            });
                            //no hover effect in IE9 as its not proper
                            if (isIE9() != 9) {
                                $("#btnSaveSnapcap, #btnCancelSnapcap").hover(function () {
                                    $(this).css('background-color', '#0044cc');
                                });
                            }

                           
                        }
                        function isIE9() {
                            var myNav = navigator.userAgent.toLowerCase();
                            return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
                        }
                        function SnapcapsCommonTypesGetFail() {
                        }

                        //Save the snapcap in DB.
                        function SnapCapSave(c) {
                           
                            //added for common type id by deepthi on 04 aug 2014
                            var SnapCapCtypeId, snapcapName;
                            var isCommonType;
                            //if ($.trim($("#snapcapCommonTypes").val()) >= "1" &&  document.getElementById("DivsnapcapTitle").disabled == true) {
                            if ($.trim($("#snapcapCommonTypes").val()) >= "1" && document.getElementById("snapcapTitle").disabled == true) {
                                //if common type selected
                                SnapCapCtypeId = $.trim($("#snapcapCommonTypes").val());
                                snapcapName = $.trim($("#snapcapCommonTypes").find('option:selected').text());
                                isCommonType = true;
                            }
                            else {
                                //if common type not selected
                                snapcapName = $.trim($("#snapcapTitle").val());
                                isCommonType = false;

                            }

                            if ((snapcapName != null && snapcapName != '') || (SnapCapCtypeId != null && SnapCapCtypeId != '')) {
                                //Modify the code or show alert if _snapcapNames are available or not.
                                if (_snapcapNames != null) {
                                    
                                        _snapcapNames.push(snapcapName);

                                        //Logic for preset snapcap 
                                        if (_presetTitleList[_presetTitleList.length - 1] == snapcapName) {
                                            _snapcapCount++;
                                        }

                                        //Crops the image.
                                        //added new parameter for common type id by deepthi on 04 aug 2014
                                        ImageCrop(c, snapcapName, SnapCapCtypeId, isCommonType);
                                }
                            }
                            else {
                                alert('SnapCap name cannot be empty.');
                            }
                        }

                    });
                };

                //Create icons for capture and close the image.
                function CaptureCloseIconsCreate() {

                    //Path for the icons of cam and close.
                    //Image modified by deepthi on 29th july 2014
                    var closePath = _baseURL + "Images/safeguards/red_col_close.png";
                    var camPath = _baseURL + "Images/safeguards/camera.png";

                    //Creates a div for capturing the image.
                    var captureDiv = document.createElement('div');
                    captureDiv.setAttribute('id', 'captureSelection');
                    captureDiv.setAttribute('position', 'relative');

                    //create and place close icon.
                    var closeImg = document.createElement('img');
                    closeImg.setAttribute('id', 'closeImage');
                    closeImg.setAttribute('src', closePath);
                    closeImg.setAttribute('style', 'cursor:pointer;position:relative;float:right');
                    closeImg.setAttribute('title', 'Clear selection');

                    //create and place camera icon.
                    var captImage = document.createElement('img');
                    captImage.setAttribute('id', 'captureImage');
                    captImage.setAttribute('src', camPath);
                    captImage.setAttribute('style', 'cursor:pointer;position:relative;float:right;margin-right:10px');
                    captImage.setAttribute('title', 'Capture the image');

                    captureDiv.appendChild(closeImg);
                    captureDiv.appendChild(captImage);

                    var trackSelectionDiv = document.getElementById('topSelection');//id, topSelection added in the jcrop script file.                    
                    trackSelectionDiv.appendChild(captureDiv);
                }

                //Crops the provided image.
                function ImageCrop(c, snapcapName, SnapCapCtypeId, isCommonType) {

                    var croppedInfo = new CropImageDetails(c.x, c.y, c.w, c.h, userId, eventId, _fileName, snapcapName, SnapCapCtypeId, isCommonType);
                    //var croppedInfo = new CropImageDetails(c.x, c.y, c.w, c.h, userId, _fileName);
                    var data = JSON.stringify(croppedInfo);
                    serviceurl = _baseURL + 'tap.snap.hand/CropImage.ashx';
                    PostAjax(serviceurl, data, eval(CropImageSuccess), eval(CropImageFail));
                }

                function CropImageDetails(x, y, w, h, userId, eventId, _fileName, snapcapName, SnapCapCtypeId, isCommonType) {
                    this.x = ~~x;
                    this.y = ~~y;
                    this.w = ~~w;
                    this.h = ~~h;
                    this.userId = userId;
                    this.eventId = eventId;
                    this.fileName = _fileName;
                    this.snapcapName = snapcapName;
                    this.SnapCapCtypeId = SnapCapCtypeId;
                    this.isCommonType = isCommonType;
                }

                function CropImageSuccess() {

                    //Close the snapcap dialog.
                    $("#dialog:ui-dialog").dialog("destroy");
                    $("#snapcapTitle").empty();
                    $("#snapcapdialog").dialog("destroy");

                }

                function CropImageFail() {

                }
            }


            //get common type

            //get common type
            function GetCommonType() {
             
            }


            //Displays back the viewport for drawing the graph.
            function CaptureModeExit() {

                //Show back the hided graph container.
                $('.geDiagramContainer').show();

                $('.geToolbarContainer').show();
                $('.geMenubarContainer').show();
                $('.geHsplit').show();

                //Hides the capture screen.
                $('#bgGreyDiv').hide();

                //Ajax call for deleting the existing graph image.
                if (_fileName != '') {
                    var deleteImageInfo = new DeleteImageDetails(_fileName);
                    var data = JSON.stringify(deleteImageInfo);
                    serviceurl = _baseURL + 'tap.snap.hand/SnapChartDelete.ashx';
                    PostAjax(serviceurl, data, eval(SnapChartimageDeleteSuccess), eval(SnapChartImageDeleteFail));

                    function SnapChartimageDeleteSuccess() { }
                    function SnapChartImageDeleteFail() { }

                    function DeleteImageDetails(_fileName) {
                        this.fileName = _fileName;
                    }
                }
            }

            //Unload functionality
            $(window).unload(function () {
                CaptureModeExit();
            });

        </script>

    </form>
</body>
</html>







