﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tap.mst/Master.Master" AutoEventWireup="true" CodeBehind="SampleCropImage.aspx.cs" inherits="tap.ui.usr.SampleCropImage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <script src="<%=ResolveUrl("~/Scripts/jCrop/jquery.Jcrop.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jCrop/jquery.color.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Scripts/jCrop/jquery.Jcrop.css") %>" rel="stylesheet"
        type="text/css" />

    <script language="Javascript">
        $(function () {
            $('#cropbox').Jcrop({ aspectRatio: 1, onSelect: updateCoords });
        });
        function updateCoords(c) {
            $('#x').val(c.x); $('#y').val(c.y); $('#w').val(c.w); $('#h').val(c.h);
        };
        function checkCoords() {
            if (parseInt($('#w').val())) return true;
            alert('Select where you want to Crop.'); return false;
        };
        
        function saveCropImage() {
            var x = $('#x').val();
            var y = $('#y').val();
            var w = $('#w').val();
            var h = $('#h').val();
            var Details = new DetailData(x, y, w, h);
          
            $.ajax({
                type: "post",
                url: "SampleCropImage.aspx",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(Details),
                success: function (result) {
                    $('.imagem_artigo').attr('src', '/images/crop_dance.jpg');
                },
                error: function (xhr, status, error) {
                    
                }
            });
        }

        function DetailData(x, y, w, h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            //this.fileName = filename;
        }
    </script> 





    <div id="outer"> <div class="jcExample"> <div class="article"> 
        <h1>Crop jQuery</h1> <!--Image that we Will insert --> 
        <img class='imagem_artigo' src="/images/dance.jpg" id="cropbox" /> <!--Form to crop--> 
        <%--<form action="crop.php" method="post" onsubmit="return checkCoords();">--%>
            
             <input type="hidden" id="x" name="x" />
             <input type="hidden" id="y" name="y" />
             <input type="hidden" id="w" name="w" /> 
            <input type="hidden" id="h" name="h" /> 
            <input type="submit" value="Crop Image" onclick="saveCropImage()" />
       

       <%-- </form> --%>

                                             </div>

                     </div> 

    </div>



</asp:Content>

