﻿
#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom.usr;
using tap.dat;
#endregion

namespace tap.ui.usr
{
    public partial class SnapChart : System.Web.UI.Page
    {

        #region Global Variables

        int _eventId;
        public int? _season = 1;
        public bool _isWinterChart = false;
        public bool _isComplete = false;
        EFEntity _db = new EFEntity();
        #endregion

        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        #region Page_load

        /// <summary>
        ///  Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //on click of SnapChart popup link from TapRooT tab - check if session expires then close the popup window and redirec to Login Page.
            if ((Session.Count == 0) || (Convert.ToString(Session["CompanyId"]) == string.Empty))
            {
                Response.Write("<script>window.close();</" + "script>");

                //refresh parent page to get saved report
                Response.Write("<script>window.opener.location.reload(true);</" + "script>");
                Response.End();
              
            }

            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
            dom.gen.Security security = new dom.gen.Security();

            //track the company id, user id and division id.
            hdnCompanyId.Value = Convert.ToString(Session["CompanyId"]);
            hdnUserId.Value = Convert.ToString(Session["UserId"]);
            hdnDivisionId.Value = Convert.ToString(Session["DivisionId"]);
            hdnDateFormat.Value = Convert.ToString(Session["DateFormat"]);

            //int.TryParse(Page.RouteData.Values["EventId"].ToString(), out _eventId);
            string eventID = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            _eventId = int.Parse(eventID);

             #region "security check"
            int userId = Convert.ToInt32(hdnUserId.Value);

            //pass eventid,userid and cookies to check User have autorise to access this page
            if (userId!= 0)
                security.CheckEventAccess(_eventId, userId, Response);

            #endregion`u8`i90`ol0`r 042 

            //TapRootTab snap = new TapRootTab();            

            if (_eventId != 0)
            {
                var snapchartInfo = (from snapchart in _db.SnapCharts where snapchart.EventID == _eventId select snapchart).FirstOrDefault();
                //var snapchartInfo = (from snapchart in _db.SnapCharts where snapchart.EventID == _eventId select snapchart);
                if (snapchartInfo != null)
                {
                    _season = snapchartInfo.SeasonID;
                    _isComplete = snapchartInfo.IsComplete;
                    SnapData = SnapchartGraphDisplay(_season).ToString();
                    //SnapData = snapchartInfo.SnapChartData;
                }

                int winter = (int)tap.dom.hlp.Enumeration.Season.Winter;
                var winterInfo = (from snapchart in _db.SnapCharts where snapchart.EventID == _eventId && snapchart.SeasonID == winter select snapchart).FirstOrDefault();
                if (winterInfo != null)
                {
                    _isWinterChart = true;
                    int? seasonId = winterInfo.SeasonID;
                    WinterData = SnapchartGraphDisplay(seasonId).ToString();                    
                }

                //Page Title
                tap.dom.usr.Event _event = new dom.usr.Event();
                string EventName = _event.getEventName(Page.RouteData.Values["EventId"].ToString());
                Page.Title = "SnapCharT®" + ((EventName != "") ? " - " + EventName : "");
            }

            //Get the latest Snapchart data for this eventId.
           // var SnapData = SnapchartGraphDisplay(_season);
        }

        #endregion

        #region Get the snapchart graph data.

        public object SnapchartGraphDisplay(int? _season)
        {
            
            var snapData = (from a in _db.SnapCharts where a.EventID == _eventId && a.SeasonID == _season select a).FirstOrDefault();            
            if (snapData != null)
            {  
                //Escape single quotes if exists
                return snapData.SnapChartData.Replace("'", @"\'");

                //return snapData.SnapChartData;                
            }
            return null;
        }

        #endregion

        #region Properties

        public string SnapData { get; set; }
        public string WinterData { get; set; }

        #endregion

    }
}