﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExcelLibrary;
using ExcelLibrary.SpreadSheet;

    public partial class UploadUsers : System.Web.UI.Page
    {
        OleDbConnection oledbConn;
        tap.dat.Companies _company = new tap.dat.Companies();
        tap.dat.Users _user = new tap.dat.Users();
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region "Upload the file(s)"
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (file.PostedFile != null && file.PostedFile.FileName != "")
            {
                if (isValidExt(file.PostedFile.FileName))
                {
                    try
                    {
                        file.PostedFile.SaveAs(Server.MapPath("~/") + file.PostedFile.FileName);

                        string path = System.IO.Path.GetFullPath(Server.MapPath("~/" + file.PostedFile.FileName));

                        SaveUsers(path);

                        string strPhysicalFolder = Server.MapPath("~/" + file.PostedFile.FileName);

                        DeleteExcelFile(strPhysicalFolder);
                        CloseModalPopup();
                    }
                    catch (Exception ex)
                    {
                        tap.dom.hlp.ErrorLog.LogException(ex);
                    }
                }
                else
                {
                    lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    lblErrorMessage.Text = "Please select a valid file(.xls, .xlsx) to upload users.";
                }
            }
            else
            {
                lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                lblErrorMessage.Text = "Please select a file to upload users.";
            }
        }
        #endregion

        public void SaveFileInServer(FileUpload file)
        {
            file.PostedFile.SaveAs(Server.MapPath("~/") + file.PostedFile.FileName);

        }
        public void DeleteExcelFile(string file)
        {
            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
        }
        public void SaveUsers(string filePath)
        {
            try
            {
                if (Path.GetExtension(filePath) == ".xls")
                {
                    oledbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");
                }
                else if (Path.GetExtension(filePath) == ".xlsx")
                {
                    oledbConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';");
                }
                oledbConn.Open();
                DataTable Sheets = oledbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow drs in Sheets.Rows)
                {
                    string sheet = drs[2].ToString().Replace("'", "");
                    //OleDbDataAdapter dataAdapter = new OleDbDataAdapter("select * from [" + sht + "]", connection);
                    try
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        OleDbDataAdapter oleda = new OleDbDataAdapter();
                        DataSet ds = new DataSet();
                        cmd.Connection = oledbConn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "SELECT [CompanyID],[UserName],[Password],[PhoneNo],[isAdministrator], [FirstName],[LastName],[Email] FROM [" + sheet + "]";
                        oleda = new OleDbDataAdapter(cmd);
                        oleda.Fill(ds);
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string comId = dr["CompanyID"].ToString();
                            int companyId = 0;

                            string userName = dr["UserName"].ToString();
                            string password = dr["Password"].ToString();
                            string email = dr["Email"].ToString();
                            string lastName = dr["LastName"].ToString();
                            string isAdmin = dr["isAdministrator"].ToString();
                            string firstName = dr["FirstName"].ToString();
                            string PhoneNo = dr["PhoneNo"].ToString();

                            if (!string.IsNullOrEmpty(comId))
                            {
                                int.TryParse(comId, out companyId);

                                _company = _db.Companies.Where(a => a.CompanyID == companyId).FirstOrDefault();
                                if (_company != null && !string.IsNullOrEmpty(userName))
                                {
                                    _user = _db.Users.Where(a => a.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
                                    if (_user == null && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(email) && IsValidEmail(email))
                                    {
                                        tap.dat.Users user = new tap.dat.Users();
                                        user.FirstName = firstName;
                                        user.LastName = lastName;
                                        user.Email = email;
                                        user.CompanyID = Convert.ToInt32(comId);
                                        user.UserName = userName;
                                        user.Password = password;
                                        if(isAdmin.Equals("1") || isAdmin.Equals("0"))
                                            user.isAdministrator = (isAdmin.Equals("1"))?true:false;
                                        user.Active = true;
                                        user.PhoneNo = PhoneNo;
                                        _db.AddToUsers(user);
                                        _db.SaveChanges();
                                    }
                                }

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        tap.dom.hlp.ErrorLog.LogException(ex);
                    }
                }
                oledbConn.Close();
            }
            catch (Exception e)
            {
                tap.dom.hlp.ErrorLog.LogException(e);

            }
        }
        public bool isValidExt(string file)
        {
            FileInfo fi = new FileInfo(file);
            string ext = fi.Extension;

            return ext == ".xls"?true:
                ext == ".xlsx"?true:false;
        }

        #region "Close the modal popup page"
        protected void btnUploadUserClose_Click(object sender, EventArgs e)
        {
            CloseModalPopup();
        }
        #endregion

        #region "Close the modal pop up"
        private void CloseModalPopup()
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming", "closeUploadUserPopUp()", true);
        }
        #endregion


        //check email validation
        public bool IsValidEmail(string email)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                return false;
            }
        }

    }
