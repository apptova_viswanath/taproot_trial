﻿//---------------------------------------------------------------------------------------------
// File Name 	: DownloadAttachment.aspx.cs

// Date Created  : N/A

// Description   : Download Attachment Page Methods (.doc,.png,.img,.pdf etc files)

// Javascript File Used : js.atch.js
//----------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;
using System.Web.Script.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
#endregion

public partial class DownloadAttachment : System.Web.UI.Page
{
    #region "Properties"
    public int DocId
    {
        get
        {
            string id = Request.Params["id"];
            int outId = 0;
            int.TryParse(id, out  outId);
            return outId;
        }
        set { }
    }
    #endregion

    #region "Page Load Event"
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DocId > 0)
        {
            DownloadAttachmentById();
        }
    }
    #endregion

    #region "Download Attachments"
    private void DownloadAttachmentById()
    {
        try
        {

            tap.dat.Attachments attachments = tap.dom.adm.Attachment.GetAttachment(DocId);
            if (attachments == null)
                return;

            byte[] byteArray = attachments.AttachmentData.ToArray();
            MemoryStream memoryStream = new MemoryStream(byteArray);
            memoryStream.Flush();
            memoryStream.Close();

            //if (attachments.Extension == ".docx" || attachments.Extension == ".doc")
            //    Response.ContentType = "application/vnd.ms-word";
            //else if (attachments.Extension == ".xlsx")
            //    Response.ContentType = "application/vnd.xls";
            //else if (attachments.Extension == ".xls")
            //    Response.ContentType = "application/vnd.ms-excel";
            //else if (attachments.Extension == ".pdf")
            //    Response.ContentType = "application/pdf";

            Response.ContentType = ReturnExtension(attachments.Extension.ToLower());

            Response.AppendHeader("content-disposition", "attachment; filename=" + attachments.DocumentName.Replace("\"", "'"));

            Response.BinaryWrite(memoryStream.ToArray());

            Response.Flush();
            Response.End();

        }
        catch (Exception ex)
        {
            tap.dom.hlp.ErrorLog.LogException(ex);
        }
    }

    #endregion


    private string ReturnExtension(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".txt":
                return "text/plain";
            case ".pdf":
                return "application/pdf";
            case ".doc":
            case ".docx":
                return "application/ms-word";
            case ".xls":
                return "application/vnd.ms-excel";
            case ".xlsx":
                return "application/vnd.xls";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            //case ".wav":
            //    return "audio/wav";
            //case ".ppt":
            //    return "application/mspowerpoint";
            //case ".dwg":
            //    return "image/vnd.dwg";
            default:
                return "application/octet-stream";
        }
    }
}
