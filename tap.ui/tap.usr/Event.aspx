﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="tap.usr.evt.Event"
    MasterPageFile="~/tap.mst/Master.Master" %>

<%@ Register Src="~/tap.usr.ctrl/ucEventTabs.ascx" TagName="ucEvent" TagPrefix="uc" %>
<%@ Register Src="~/tap.usr.ctrl/ucCustomFields.ascx" TagName="custField" TagPrefix="UcCustField" %>
<%@ Register Src="~/tap.usr.ctrl/ucEventNextPrev.ascx" TagName="ucEventNextPrev" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!---Generalized  Function scripts -->
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/root.css") %>" rel="stylesheet"
        type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.0.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.min.js") %>"
        type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <!-- JStree plugin  ------------->
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.hotkeys.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.jstree.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jstree/!script.js") %>" type="text/javascript"></script>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.jstree/default/style.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/DialogStyle.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/jquery.ui.css/jquery.ui.validate.css") %>"
        rel="stylesheet" type="text/css" />

    <script type="text/javascript" lang="javascript">
        var _sessionLocationId = '';
        _sessionLocationId = '<%= Session["LocationId"] %>';    

    </script>

    <!-- Page Scripts -->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.event/js.evt.js") %>" type="text/javascript"> </script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/bootstrp.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet"
        type="text/css" />

    <!--Main Tabs and Sub Tabs -->
    <style>
        #btnEventSave, #btnCancel, #btnEventLocationCancel, #btnSelectEventLocation, #btnSelectEventClassification, #btnEventClassificationCancel {
            color: #ffffff;
        }

        .animate {
            position: absolute !important;
            /*left: -120px  !important;*/
            text-align: center;
        }

        .animateSubTab {
            position: absolute !important;
            padding: 0px 10px 0px 10px !important;
            margin-right: 10px !important;
            text-align: center;
        }

        #animateSubTabPosition {
            position: absolute !important;
        }
             #ui-datepicker-div {
            font-size: 12px;
        }
    </style>
    <input type="hidden" id="hdnLocationListId" value="0" runat="server" />
    <input type="hidden" id="hdnClassListId" value="0" runat="server" />


    <div class="siteContent">


        <asp:HiddenField ID="hdnDynamicFieldIds" runat="server" />
        <asp:HiddenField ID="hdnDynamicFieldValues" runat="server" />
        <asp:HiddenField ID="hdnDynamicControlNames" runat="server" />
        <asp:HiddenField ID="hdnIsDetailsTab" runat="server" Value="1" ClientIDMode="Static" />


        <div id="tabs">

            <uc:ucEvent ID="ucevent" runat="server" />

            <div id="divEventDetails" class="ContentWidth ClearAll">
                <div style="text-align: center">
                    <span onclick="javascript:EventUndo();" class="anchorStyle" style="display:none" id="ancUndo">Undo</span>&nbsp; <span class="anchorStyle" style="display:none" onclick="javascript:EventUndoAll();"
                        id="ancUndoAll">Undo All</span>
                </div>
                <div>
                    <label id="eventErrorMsg">
                    </label>
                </div>
                <div class="MarginTop10">
                    <label id="lblName" class="ContentUserLabel">
                        Event Name<span id="spnName" style="color: Red">*</span></label>
                </div>
                <div class="MarginTop10">
                    <input id="txtEventName" class="ContentInputTextBox WidthP40 trackChanges" type="text" runat="server"
                        data-mode="Update" data-type="Static" data-entity-type="tap.dat.Events,tap.dat" data-entity-set="Events"
                        data-entity-property="Name" data-entity-property-pk="EventID" data-entity-queryfield="EventID"
                        data-entity-datatype="String" data-entity-controltype="TextBox" data-entity-queryfield-value="0"
                        data-entity-sec-queryfield="EventID" autofocus="" />
                </div>
                <div class="MarginTop10">
                    <label id="lblDate" class="ContentUserLabel EventTypeDate" runat="server">
                        Incident Date<span id="spnIncDate" style="color: Red">*</span>
                    </label>
                </div>
                <div class="MarginTop10">
                    <input id="txtDate" readonly="readonly" class="DateInputTextBox trackChanges " type="text"
                        runat="server" data-mode="Update" data-type="Static" data-entity-type="tap.dat.Incidents,tap.dat"
                        data-entity-set="Incidents" data-entity-property="IncidentDatetime" data-entity-property-pk="IncidentID"
                        data-entity-queryfield="EventID" data-entity-datatype="datetime" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />


                </div>
                <div class="MarginTop10">
                    <label id="lblEventTime" class="ContentUserLabel EventTypeDate" runat="server">
                        Incident Time
                    </label>
                </div>
                <div class="MarginTop10">
                    <input type="text" id="txtTime" class="DateInputTextBox trackChanges" runat="server"
                        onblur="javascript:FormatMilitaryTime('body_txtTime')" data-mode="Update" data-type="Static" data-entity-type="tap.dat.Incidents,tap.dat"
                        data-entity-set="Incidents" data-entity-property="IncidentTime" data-entity-property-pk="IncidentID"
                        data-entity-queryfield="EventID" data-entity-datatype="time" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />
                </div>
                     <div class="MarginTop10">
                    <label id="lblinvDateTime" class=" ContentUserLabel" runat="server">
                        Investigation Date
                    </label>
                </div>
                <div class="MarginTop10">
                    <input id="txtInvDate" readonly="readonly" class="DateInputTextBox trackChanges" type="text" runat="server"
                        data-mode="Update" data-type="Static" data-entity-type="tap.dat.Investigations,tap.dat" data-entity-set="Investigations"
                        data-entity-property="InvestigationDatetime" data-entity-property-pk="InvestigationID"
                        data-entity-queryfield="EventID" data-entity-datatype="datetime" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />


                </div>
                <div class="MarginTop10">
                    <label id="lblInvestigationTIme" class="ContentUserLabel" runat="server">
                        Investigation Time
                    </label>
                </div>
                <div class="MarginTop10">
                    <input type="text" id="txtInvTime" class="DateInputTextBox trackChanges" runat="server"
                        onblur="javascript:FormatMilitaryTime('body_txtInvTime')" data-mode="Update" data-type="Static"
                        data-entity-type="tap.dat.Investigations,tap.dat" data-entity-set="Investigations"
                        data-entity-property="InvestigationTime" data-entity-property-pk="InvestigationID"
                        data-entity-queryfield="EventID" data-entity-datatype="time" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />
                </div>
                <div class="MarginTop10">
                    <label id="lblLocation" class="ContentUserLabel">
                        Location<span id="Span2" style="color: Red">*</span></label>
                </div>
                <div class="notranslate">
                    <label id="lblEventLocationData" runat="server" clientidmode="Static" class="EventImageLabel">
                    </label>
                </div>
                <div class="FloatLeft PaddingLeft20 Width120">
                    <img alt="OpenLink" class="HandCursor" id="lnkEventLocation" src="<%=ResolveUrl("../Images/OpenLink.png") %>" />
                </div>
                <div class="MarginTop10 ClearAll" style="width: 150px;">
                    <label id="lblClassification" class="ContentUserLabel">
                        Classification<span id="Span1" style="color: Red">*</span></label>
                </div>
                <div class="notranslate">
                    <label id="lblEventClassificationData" runat="server" clientidmode="Static" class="EventImageLabel">
                    </label>
                </div>
                <div class="FloatLeft PaddingLeft20">
                    <img alt="OpenLink" class="HandCursor" id="lnkEventClassification" src="<%=ResolveUrl("../Images/OpenLink.png") %>" />
                </div>
                <div class="MarginTop10 ClearAll" style="border: 1px solid transparent;">
                    <label id="lblStatus" class="ContentUserLabel MarginTop10">
                        Status</label>
                </div>
                <div class="MarginTop10">
                    <select id="ddlStatus" style="width: 40.5%" class="ContentInputTextBox WidthP40 trackChanges"
                        runat="server" data-mode="Update" data-type="Static" data-entity-type="tap.dat.Events,tap.dat" data-entity-set="Events"
                        data-entity-property="Status" data-entity-property-pk="EventID" data-entity-queryfield="EventID"
                        data-entity-datatype="Int" data-entity-controltype="Dropdown" data-entity-queryfield-value="0"
                        data-entity-sec-queryfield="EventID">

                        <option value="1">Active</option>
                        <option value="2">Archived</option>
                        <option value="3">Completed</option>
                    </select>
                </div>
                <div id="hideAdminBasis" class="hidden">
                    <div class="MarginTop10 ClearAll">
                        <label id="lblEventDescription" class="ContentUserLabel">
                            Description
                        </label>
                    </div>

                    <div class="MarginTop10">
                        <textarea id="txtEventDescription" class="ContentInputTextBox WidthP40 trackChanges" runat="server"
                            data-mode="Update" data-type="Static" data-entity-type="tap.dat.Events,tap.dat" data-entity-set="Events"
                            data-entity-property="Description" data-entity-property-pk="EventID" data-entity-queryfield="EventID"
                            data-entity-datatype="String" data-entity-controltype="ParagraphText" data-entity-queryfield-value="0"
                            data-entity-sec-queryfield="EventID"></textarea>
                    </div>
                    <div class="MarginTop10 ClearAll">
                        <label id="lblEventFileNumber" class="ContentUserLabel">
                            File Number
                        </label>
                    </div>
                    <div class="MarginTop10">
                        <input id="txtEventFileNumber" class="ContentInputTextBox WidthP40 validate trackChanges"
                            type="text" runat="server" data-mode="Update" data-type="Static" data-entity-type="tap.dat.Events,tap.dat"
                            data-entity-set="Events" data-entity-property="FileNumber" data-entity-property-pk="EventID"
                            data-entity-queryfield="EventID" data-entity-datatype="String" data-entity-controltype="TextBox"
                            data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />
                    </div>
                </div>
           
                <div class="MarginTop10">
                    <label id="lblCapDate" class=" ContentUserLabel" runat="server">
                        Cap Date
                    </label>
                </div>
                <div class="MarginTop10">
                    <input id="txtCapDate" class="DateInputTextBox trackChanges" type="text" runat="server"
                        data-mode="Update" data-type="Static" data-entity-type="tap.dat.CorrectiveActionPlans,tap.dat" data-entity-set="CorrectiveActionPlans"
                        data-entity-property="CapDatetime" data-entity-property-pk="CorrectiveActionPlanID"
                        data-entity-queryfield="EventID" data-entity-datatype="datetime" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" readonly="true" />


                </div>
                <div class="MarginTop10">
                    <label id="lblCapTime" class="ContentUserLabel" runat="server">
                      CAP Time
                    </label>
                </div>
                <div class="MarginTop10">
                    <input type="text" id="txtCapTime" class="DateInputTextBox trackChanges" runat="server"
                        onblur="javascript:FormatMilitaryTime('body_txtCapTime')" data-mode="Update" data-type="Static"
                        data-entity-type="tap.dat.CorrectiveActionPlans,tap.dat" data-entity-set="CorrectiveActionPlans"
                        data-entity-property="CapTime" data-entity-property-pk="CorrectiveActionPlanID"
                        data-entity-queryfield="EventID" data-entity-datatype="String" data-entity-controltype="TextBox"
                        data-entity-queryfield-value="0" data-entity-sec-queryfield="EventID" />
                </div>
                <div id="showBasedOnAdmin">
                    <UcCustField:custField ID="customField" runat="server" />
                </div>
                <div class="ContentMargin">

                    <input type="button" class="btn btn-primary btn-small" value="Create" id="btnEventSave" />
                    <input type="button" class="btn btn-primary btn-small" value="Cancel" id="btnCancel" />

                </div>
            </div>
            <div class="tab-next-prev">
                <uc:ucEventNextPrev ID="ucEventNextPrev" runat="server" />
            </div>
        </div>
        <asp:HiddenField ID="hdnLocationId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnClassificationId" ClientIDMode="Static" runat="server" />
        <div class="backgroundPopup">
        </div>
        <div class="demo">
            <div id="EventLocation" class="hidden" title="">
                <div>
                    <label id="ErrLocation">
                    </label>
                </div>
                <div class="CenterTree notranslate">
                    <div id="EventLocationTreeList" class="treeViewSection">
                    </div>
                </div>
                <div class="MarginTop10 PaddingRight100">
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Cancel" id="btnEventLocationCancel" />
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Select" id="btnSelectEventLocation" />

                </div>
            </div>
        </div>
        <!-- for Classification page popin -->
        <div class="demo hidden">
            <div id="divEventClassificationDialog" title="">
                <div>
                    <label id="ErrClassification">
                    </label>
                </div>
                <div class="CenterTree notranslate">
                    <div id="EventClassificationTreeList" class="treeViewSection">
                    </div>
                </div>
                <div class="MarginTop10 PaddingRight100">
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Cancel" id="btnEventClassificationCancel" />
                    <input type="button" class="btn btn-primary btn-small btn-float" value="Select" id="btnSelectEventClassification" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
