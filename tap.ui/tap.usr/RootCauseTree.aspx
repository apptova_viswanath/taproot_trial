﻿<!doctype html>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RootCauseTree.aspx.cs" Inherits="tap.ui.usr.RootCauseTree" %>
<%@ Register Src="~/tap.usr.ctrl/ucGenericCause.ascx" TagName="ucGenericCause" TagPrefix="uc" %>

<html>
<head id="correctiveActionHead" runat="server">
    <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE" />
    <title>Root Cause Tree® </title>


    <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RootCauseTree.js") %>" type="text/javascript"></script>      
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTEffects.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTGuidance.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTAnalysisComments.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/GenericCause.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/TapRootTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/root.css") %>" rel="stylesheet"
        type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>--%>
    <!-- Activity Logging --->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <!--  For notifying  --------------->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <!-- Notification plugin (modified plugin files) ------------->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/js.noty/jquery.notify.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/themes/default.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/layouts/center.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.mst.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.master/js.mst.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.tab.hover.js") %>" type="text/javascript"></script>
     <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';
       
        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

    </script>
    <style type="text/css">

        #tabs
        {
            /*margin-top: 5px !important;*/
        }
        .ui-widget-content a {
            /*color: #EDEDED;*/
        }

        .ui-widget-header {
            background: none !important;
            border: none !important;
            color: none !important;
            font-weight: normal !important;
        }

        .popupTitle {
            background: url('../../Scripts/jquery/images/ui-bg_highlight-soft_75_74a6e2_1x100.png') repeat-x scroll 50% 50% #74A6E2;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }

        .ui-tabs .ui-tabs-nav li {
            white-space: normal !important;
        }

        #content {
            overflow: hidden !important;
        }

        .siteContent {
            height: 100% !important;
            /*background: #f9f9f9 !important;*/
        }

        .ui-accordion .ui-accordion-header .ui-icon {
            left: 2em !important;
        }

        .selectPath {
            visibility: visible !important;
            display: block !important;
        }

        .unSelectPath {
            visibility: visible !important;
            display: block !important;
        }

        .USER_RESPONSE_NULL {
            visibility: visible !important;
            display: block !important;
        }

        #divLinkContent div {
            display: none;
        }

        #spnRestate {
            vertical-align: top;
            margin-left: 2px;
        }

        .restateImage {
            cursor: pointer;
            margin-left: 5px;
            margin-top: 5px;
        }

        #lblNone {
            display: none;
            margin-left: 2px;
        }

        .genericQuestion {
            margin-left: 100px;
        }

        .textArea {
            width: 455px;
            height: 170px;
            margin-left:0px !important;
        }

        .buttonGeneric {
            text-align: center;
        }

        #divGettingStartedHeader {
            float: left;
            clear: both;
        }

        #divLinkContent {
            clear: both;
        }

        #divAccordion {
            float: left;
        }

        #tabs {
            margin-top:40px;
            /*border: 1px solid #b1b1b1;*/
        }

        #divCausalFactor {
            /*font-family: Verdana;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-top: 5px;
            color: #0080C0;*/
             background-color: #8ab4e6;
            font-family: Verdana;
            margin-bottom: 10px;            
            /*margin-top: 5px;*/
            padding-bottom: 5px;
            padding-left: 5px;
            padding-top: 5px;
            position: fixed !important;
            top: 0;
            width: 100%;
            z-index: 999;
            box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for ie and other*/
            -moz-box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for mozilla*/
            -webkit-box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for chrome*/
        }

        #spnHeader {
            margin-left: 5px;
            font-weight: bold;
        }

        .analysisComments {
            display: none;
        }

        #lblQuestion2 {
            word-wrap: break-word;
        }

        .side {
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .top {
            border-top: 30px solid #000000;
            padding-top: 3px;
        }

        .bottomSection {
            border-bottom: 3px solid black !important;
        }

        .bottom {
            border-bottom: 0 solid #000000;
            padding-bottom: 2px;
        }


        .accordionHeader {
            margin-top: -1px !important;
        }

        .accordioncontent {
            margin-bottom: 1px !important;
        }

        .lblTextHeader {
            color: #FFFFFF;
            float: left;
            font-weight: bold;
            margin-left: 6px;
            margin-top: -25px;
            position: relative;
            display: none;
        }

        .container {
            border-bottom: 3px solid #000000;
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .basicCauseHeader {
            background-color: #000000;
            color: #FFFFFF;
            height: 28px;
            margin-left: -5px;
            margin-right: -5px;
        }

        #divGenericRestate {
            display: none;
            word-wrap: break-word !important;
            overflow: auto !important;
            height: auto !important;
        }


        .MsoNormal a {
            color: black !important;
        }

        .MsoNormal {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            font-size: 13px !important;
            line-height: 18px !important;
            margin-top: 10px !important;
        }

            .MsoNormal span p {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

            .MsoNormal span {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

        #divDic {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
        }

            #divDic a {
                color: black !important;
              text-decoration: underline;
              cursor:pointer !important;
            }
        
         #divDic p
        {
            margin-bottom: 10px !important;
        }
        /*Testing COMMIT 1*/

        /*body{ overflow-x:hidden;overflow-y:hidden; }*/

        .btn-small {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    padding-bottom: 2px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 2px;
}
.btn-primary {
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-color: #006dcc;
    background-image: linear-gradient(to bottom, #0088cc, #0044cc);
    background-repeat: repeat-x;
    border-bottom-color: rgba(0, 0, 0, 0.25);
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: rgba(0, 0, 0, 0.1);
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: rgba(0, 0, 0, 0.1);
    border-top-color: rgba(0, 0, 0, 0.1);
    color: #ffffff;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}
.btn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: #ccc;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: solid;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: 1px;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: #ccc;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: solid;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: 1px;
    border-top-style: solid;
    border-top-width: 1px;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
    display: inline-block;
    line-height: 18px;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
}
 input[type=text], select {
     background: #ffffff;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
    font: italic 13px Georgia, "Times New Roman", Times, serif;
    outline: none;
    padding:5px 0px 5px 5px;
    margin-left:20px;
    color:#355779;
    box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
    border:1px solid #717171;
        }
         form textarea   {
     background: #ffffff;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
    font: italic 13px Georgia, "Times New Roman", Times, serif;
    outline: none;
    padding:5px 0px 5px 5px;
    margin-left:20px;
    color:#355779;
    box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
    border:1px solid #717171;
        }

         .RCTDictionaryLink 
{
            
             
}
         .warningYellowImage
{
    float: left;
    background-image: url("../../Images/warning-yellow.png");
    background-repeat: no-repeat;
    padding-left: 20px;
    border: 0px solid red;
   color: red;
}
    </style>



</head>

<body style="background-color:#f9f9f9 !important">
  <form runat="server">  <asp:HiddenField ClientIDMode="Static" ID="hdnUserId" runat="server" /></form>
       <div id="divCausalFactor" >
        <h3 id="lblCausalFactor">Causal Factor: </h3></div>
    <div id="rctWindow" class="siteContent">
      
        <%--<div id ="divMainContent">--%>
            <div id="tabs">

            <span id="spnHeader" class="notranslate">Root Cause Tree®</span>

            <div style="float: right; margin-right: 20px">

                <span id="Span2"><span><b>Complete: </b>
                    <input type="checkbox" style="margin-top: 5px !important" id="chkComplete" title="Complete" /></span></span>
            </div>
            <br />
            <br />
            <div id="content">
                                               
                <div id="divLinkContent">

                    <div class="div-admin-tabs">
                        <a id="rc1" name="rc1-" class="focused">Getting Started</a>
                        <div class='arrow-down'></div>
                        <input type="hidden" value="Getting Started" />
                    </div>
                    <div class="div-admin-tabs">
                        <a id="rc3" name="rc2-">15 Questions</a>
                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnHumanPerformanceDifficulty" value="15 Questions"/>
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc151-" id="rc151">EQUIPMENT DIFFICULTY</a>
                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnEquipmentDifficulty" value="Equipment Difficulty" />
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc5-,rc6-,rc8-,rc18-" id="rc21">PROCEDURES</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnProcedures" value="Procedures" />
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc8-,rc9-,rc11-,rc14-" id="rc48">TRAINING</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnTraining"  value="Training"/>
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc19-" id="rc61">QUALITY CONTROL</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnQuality Control"  value="Quality Control" />
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc13-,rc14-,rc15-" id="rc70">COMMUNICATIONS</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnCommunication"  value="Communications"/>
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc16-,rc17-,rc18-" id="rc84">MANAGEMENT SYSTEM</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnManagement System"  value="Management System" />
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc4-,rc5-,rc7-,rc8-,rc10-,rc11-" id="rc107">HUMAN ENGINEERING</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnHuman Engineering"  value="Human Engineering"  />
                    </div>
                    <div class="div-admin-tabs headerTab">
                        <a name="rc4-,rc5-,rc9-,rc11-,rc18-,rc16-,rc14-" id="rc133">WORK DIRECTION</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="hdnWork Direction"  value="Work Direction"   />
                    </div>
                    <div id="divGenericCauseLink" class="div-admin-tabs">
                        <a id="lnkGenericCause">Causes</a>

                        <div class='arrow-down'></div>
                        <input type="hidden" id="Hidden1" value="Causes" />
                    </div>

                    <div class="largeSeparator"></div>
                    <br />
                </div>
                <br />
                  <div id="DivloadingImage"  >
                       <asp:Image ID="Image1" Height="40px" Width="40px" runat="server" ImageUrl="~/Images/ajax_loader_blue_512.gif" />
                  </div>
                <div id="questions">
                    <br />
                    <div id="divGettingStartedHeader">
                        <h3 id="lblHeader"></h3>
                        <div id="lblSubHeader"></div>
                    </div>
                   
                    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>

                    <script id="rctTemplate" type="text/x-jquery-tmpl">

                        <div id="dicToolTitles">
                            <label id="lblTextHeader" class="lblTextHeader"></label>

                            {{if GuidanceCount != 0}}
                                  <img id="imghelperFirst" class="guidance" title="Click to see Guidance" border='0' src="<%=ResolveUrl("~/Images/lightbulb.png")%>" onclick='DisplayGuidancePopUp(${RCID},"RCTDictionaryLink");' />
                            {{else}}     
                            <div id="divBlank" class="blank"></div>
                            {{/if}}
                    
                        <img id="imgAnalysisComments${RCID}" title="Analysis Comments" class="guidance analysisStatus" src="<%=ResolveUrl("~/Images/GreyStatus.png")%>" border='0' onclick='DisplayAnalysisPopUp(this);' />
                        <img id="imgErase" title="Erase" class="guidance erase" src="<%=ResolveUrl("~/Images/Eraser.png")%>" border='0' onclick='EraseStatus(this);' />
                        <img id="imgAllQuestionToNo" title="Set All Questions to NO" class="guidance allNo" src="<%=ResolveUrl("~/Images/Delete.png")%>" border='0' onclick='SetAllQuestionToNo(this);' />
                        <img id="imgSelectGreen" title="Select" class="guidance select" src="<%=ResolveUrl("~/Images/tick.png")%>" border='0' onclick='SelectNode(this);' />
                                       
                            <div id="divStatusImage" class="statusContent" name="${RCTValue}"></div>
                  
                            <div id="divParent" class="parent">
                                <ul id="divRCTTitle">
                                    <span class="title">${RCTTitle}</span>
                                    <input type="hidden" id="hdnrc${RCID}" />
                                </ul>

                                <div id="divRCTQuestions" style="clear: both;">
                                    {{if Questions.length > 0}}
                                        <div id="divRadioButtons">
                                            {{each Questions}}   
                                              
                                                              <div class="userSelect translate">
                                                                  <input type="radio" onchange="SetRCTNodeStatus(this,event);" id="rbYes" name="${QuestionID}" value="Yes" />Yes
                                                                  <input type="radio" onchange="SetRCTNodeStatus(this, event);" id="rbNo" name="${QuestionID}" value="No" />No
                                                                    <input type="hidden" id="hdnQuestionResponse" value="${Response}" />
                                                              </div>
                                            <div id="divTraslateQuestions" class="question">{{html  Question}}</div>
                                            <div id="divanalysisComments" class="analysisComments translate">


                                                <input type="hidden" id="hdnRCIDForAnalysisComments" value="${RCID}" />
                                                <input type="hidden" id="hdnAnalysisComments" value="${AnalysisComments}" />

                                                <span id="Span1">Enter Analysis Comments:</span>
                                                <br>
                                                <textarea rows="2" cols="100" class="textArea" id="txtAnalysisComments">${AnalysisComments}</textarea>

                                                <div class="buttonGeneric">
                                                    <br />

                                                    <input type="button" class="btn btn-primary btn-small" id="btnSaveAnalysisComments" value="Apply Changes" />
                                                    <input type="button" class="btn btn-primary btn-small" id="btnCancelAnalysisComments" value="Cancel" />
                                                </div>

                                            </div>


                                            <span class="separator"></span>

                                            {{/each}}
                                        </div>

                                    {{/if}}
                                <br />
                                    <div style="margin-left: 5px; margin-top: 10px; float: left; clear: both; width: 100%">
                                        {{if Children}}   
                                                  
                                        {{tmpl(Children) '#rctTemplate'}}
                                              
                                {{/if}}
                                    </div>
                                </div>



                            </div>
                        </div>
                    </script>

                    <ul id="divAccordion"></ul>

                </div>

                <uc:ucGenericCause ID="ucGenericCauses" runat="server" />

           <%--     <div id="divGenericCausesContent">
                    <br />
                    <b>Root Causes Identified</b>
                    <br />

                    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                    <script id="genericTemplate" type="text/x-jquery-tmpl">

                        <div id="divGenericCause" class="parent">

                            
                            <input type="hidden" id="hdnRCType"  value="${RootCauseType}"/>
                            <div class='gcLinks' style="float: right !important; text-align: right;">
                                <a id="lnkEditAnalysis" style="float: left !important; margin-right: 20px;" class="causeAnalysis" href="javascript:void(0)">Edit Generic Cause Analysis</a>
                                <a id="lnkPerformAnalysis" class="causeAnalysis" href="javascript:void(0)">Perform Generic Cause Analysis</a>
                            </div>
                            <input type="hidden" id="hdnRCTId" value="${RCTID}" />
                            <input type="hidden" id="hdnIsGeneric" value="${isGeneric}" />

                            <ul id="Ul1" >
                                <span class="title">${RCTTitle}</span>

                            </ul>
                            <div id="divGenericQuestions">

                                <div id="divGenericQuestionsContent">

                                    <div style="clear: both">What was the tool, equipment, process, procedure or item involved in <b><i>${RCTTitle}</i></b>?  (State this in the field below as concisely as possible.)</div>
                                    <input type="hidden" class="question1" id="Hidden2" value="${Question1Response}" />
                                    <input type="text" maxlength="256" style="clear: both; margin-bottom: 5px; margin-top: 10px; width: 50%;" value="${Question1Response}"
                                        onkeyup="SetQuestionText(this);return false;" class="textAreaQuestion1Response" id="txtQuestion1Response" />

                                    <div id="divQuestion2" style="display: none; clear: both; margin-top: 2px;">
                                        Do you have any more
                                       <label id="lblQuestion2">${Question1Response}</label>
                                        ? 
                                         <input type="hidden" class="question2" id="hdnQuestion2Response" value="${Question2Response}" />
                                        <input type="radio" name="${RCTID}Question2" id="rbQuestion2Yes" value="Yes" />Yes
                                       <input type="radio" name="${RCTID}Question2" id="rbQuestion2No" value="No" />No
                                    </div>

                                    <div id="divQuestion3" style="display: none; clear: both; margin-top: 2px;">
                                        Do a significant number of 
                                        <label id="lblQuestion3">${Question1Response}</label>
                                        have problems with <b><i>${RCTTitle}</i></b>?  
                                         <input type="hidden" class="question3" id="hdnQuestion3Response" value="${Question3Response}" />
                                        <input type="radio" name="${RCTID}Question3" id="rbQuestion3Yes" value="Yes" />Yes
                                       <input type="radio" name="${RCTID}Question3" id="rbQuestion3No" value="No" />No
                                 
                                    </div>

                                </div>


                                <span class="separator"></span>
                            </div>

                        </div>



                    </script>

                    <ul id="divGenericCauseAccordion"></ul>



                    <div id="divGenericRestate">

                        <input type="hidden" id="hdnRCTTitleGeneric" />
                        <input type="hidden" id="hdnRCTIDGeneric" />
                        <input type="hidden" id="hdnQuestion2" />
                        <input type="hidden" id="hdnQuestion3" />
                        <span>What in the system is allowing this to exist? (<span id="spnRestateCause"></span> & <b><i><span id="spnRCTTitle"></span></i></b>)</span><br />
                        <textarea rows="2" cols="100" class="textArea ContentInputTextBox" id="txtGenericCause"></textarea>

                        <div class="buttonGeneric">
                            <br />

                            <input type="button" class="btn btn-primary btn-small" id="btnSaveGenericCause" value="Create" />
                            <input type="button" class="btn btn-primary btn-small" id="btnGenericCauseClose" value="Close" />
                        </div>

                    </div>

                    <br />
                    <div id="div1">


                        <b>Generic Causes Identified</b>
                        <br />
                        <label id="lblNone">None</label><br />

                        <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                        <script id="GenericCauseStatementsTemplate" type="text/x-jquery-tmpl">

                            <div id="div4">

                                <span id="spnRestate">${Restate}</span>
                                <img title="Edit" src="<%=ResolveUrl("~/Images/Edit.png") %>" class="restateImage" id="btnEditGenericCause">
                                <img title="Delete" src="<%=ResolveUrl("~/Images/Delete.png") %>" class="restateImage" id="btnDeleteGenericCause">

                                <div id="divEditGenericCause" style="display: none;">
                                    <br />

                                    <input type="hidden" id="hdnEditGenericCauseID" value="${GenericCauseID}" />
                                    <textarea rows="2" cols="100" class="textArea ContentInputTextBox" id="txtEditGenericCause">${Restate}</textarea>

                                    <div class="buttonGeneric">
                                        <br />

                                        <input type="button" class="btn btn-primary btn-small" id="btnEditGenericCauseSave" value="Save" />
                                        <input type="button" class="btn btn-primary btn-small" id="btnEditGenericCauseClose" value="Cancel" />
                                    </div>

                                </div>

                            </div>




                        </script>
                        <ul id="divGenericCauseStatements"></ul>

                    </div>



                </div>--%>



            </div>

        </div>
        </div>
    <%--</div>--%>

</body>
</html>
