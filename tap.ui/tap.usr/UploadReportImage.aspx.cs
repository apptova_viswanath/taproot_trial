﻿//----------------------------------------------------------------------------------------------------
// File Name 	: UploadReportImage.aspx.cs

// Date Created  : N/A

// Description   : Upload Image Page Methods (.gif,.png,.jpg,.tif etc files and open popup page)

// Javascript File Used : js.cst.report.js
//-----------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using tap.dat;
using tap.dom.gen;
using tap.dom.hlp;
using tap.dom.adm;
using Microsoft.Win32;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Configuration;
#endregion

namespace tap.ui.usr
{   

    public partial class UploadReportImage : System.Web.UI.Page
    {     

        #region "Variables"
        EFEntity _db = new EFEntity();
        tap.dat.Report report;
        tap.dat.ReportElement reportElement = null;

        string msg;

        int standardWidth;
        int standardHeight;

        int width;
        int height;

        #endregion

        #region "Page Load Event"
        protected void Page_Load(object sender, EventArgs e)
        {
            btnImageAttach.Attributes.Add("onclick", "return ValidateImageTypes();");
           
        }
        #endregion
     
        #region "Upload the file(s)"
        protected void btnImageAttach_Click(object sender, EventArgs e)
        {
            //insert image file in Report Element table           
            string smallImage = "Image (Small)";
            string mediumImage = "Image (Medium)";


            if (fileImage.PostedFile != null && fileImage.PostedFile.FileName != "")
            {
                //take temp path from server
                string tempPath = Server.MapPath("~/temp/");

                //check directory exists or not -if not create it.
                if (Directory.Exists(tempPath) == false)
                {
                    Directory.CreateDirectory(tempPath);
                }

                string fileExtension = fileImage.PostedFile.FileName.Substring(fileImage.PostedFile.FileName.LastIndexOf("."));
              
                //create file name to save it in temp path
                string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + fileExtension;

                //save the file path 
                string orginalPath = tempPath + fileName;

                //save file path for orginal image
                fileImage.PostedFile.SaveAs(orginalPath);

                //create file name to save it in temp path for resize image
                string newFileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + "_Temp" + fileExtension;

                //save the new file path to resizing the image
                string newFilePath = tempPath + newFileName;

                string imageSize = hdnImageSize.Value.ToString();

                CustomReport.ImageSizes size = ((imageSize == smallImage) ? CustomReport.ImageSizes.size_300x300 : (imageSize == mediumImage) ? CustomReport.ImageSizes.size_410x410 : CustomReport.ImageSizes.size_510x510);

                //resize the image
                Resize(orginalPath, size, fileName, newFilePath, newFileName);                 
              
            }
           
        }
        
        #endregion

        #region "Close Jquery Madol Popup"
        protected void btnImageClosePopup_Click(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming", "SetReportId('" + hdnReportId.Value + "')", true);
        }
        #endregion

        #region "Enum Image Sizes"
        public enum ImageSizes
        {
            size_100x100,
            size_300x300,
            size_410x410,
            size_510x510
        }
        #endregion

        #region "Resize the image"
        public void Resize(string orginalPath, CustomReport.ImageSizes size, string fileName, string newFilePath, string newFileName)
        {
           
            int sortOrder = 0;
            int reportId = 0;
            bool flag = false;
            string elementTypeName = "Image";//remove hard code
            int.TryParse(Request.QueryString["SortOrder"].ToString(), out sortOrder);
            int.TryParse(Request.QueryString["ReportId"].ToString(), out reportId);         

            //call common Resize Image method
            tap.dom.gen.CustomReport c =new tap.dom.gen.CustomReport();
            c.ResizeImage(orginalPath, size, fileName, newFilePath);
         
            if (reportId == 0)
            {
                report = new tap.dat.Report();

                //Decrypt the company Id and pass
                int companyId = Convert.ToInt32(Session["CompanyId"]);

                //Decrypt the user Id and pass
                int userId = Convert.ToInt32(Session["UserId"]);

                //save Report table data 1st and take report id and save image data in Report Element table data
                report.ReportName = string.Empty;
                report.Description = string.Empty;
                report.CompanyID = companyId;
                report.CreatedBy = userId;
                report.CreatedDate = DateTime.UtcNow; //GMT;
                report.ReportTitle = string.Empty;
                report.IsCompanyLogoUsed = flag;
                report.isCompanyNameUsed = flag;
                report.isReportTitleUsed = flag;
                report.isCreatedDateUsed = flag;

                _db.AddToReport(report);
                _db.SaveChanges();

                reportId = report.ReportID;

            }
            reportElement = new tap.dat.ReportElement();

            reportElement.ReportID = reportId;
            hdnReportId.Value = reportId.ToString();

            //pass element name type and get type id
            var elementLabelType = _db.ReportElementType.FirstOrDefault(a => a.ElementType == elementTypeName);

            reportElement.ReportID = reportId;
            reportElement.ElementTypeID = elementLabelType.ElementTypeID;
            reportElement.ElementText = elementTypeName;
            reportElement.ElementStyle = string.Empty;
            reportElement.SortOrder = sortOrder;
            reportElement.TopPosition = string.Empty;
            reportElement.LeftPosition = string.Empty;
            reportElement.ReportImageData = null;
            reportElement.ImageSize = hdnImageSize.Value.ToString();

            _db.AddToReportElement(reportElement);
            _db.SaveChanges();

            reportElement = new tap.dat.ReportElement();

            reportElement.ReportID = reportId;

            reportElement.ElementTypeID = elementLabelType.ElementTypeID;

            reportElement.ElementText = null;
            reportElement.ElementStyle = string.Empty;
            reportElement.SortOrder = sortOrder;
            reportElement.TopPosition = string.Empty;
            reportElement.LeftPosition = string.Empty;

            reportElement.ReportImageData = File.ReadAllBytes(newFilePath);//pass the byte to dbase to save image
            reportElement.ImageSize = hdnImageSize.Value.ToString();

            _db.AddToReportElement(reportElement);
            _db.SaveChanges();

            //pass reportId to parent page
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Ming", "SetReportId('" + hdnReportId.Value + "' ,'" + reportElement.ElementID + "')", true);

            System.IO.File.Delete(orginalPath);
            System.IO.File.Delete(newFilePath);

        }

       
        #endregion


    }
}