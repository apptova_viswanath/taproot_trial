﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MasterRCT.aspx.cs" Inherits="tap.ui.usr.MasterRCT" %>

<!DOCTYPE html>

<html>
<head runat="server">

      <link rel="shortcut icon" href="<%=ResolveUrl("~/Images/TREEICON.ICO")%>"/>
       <title>Root Cause Tree® </title>
    
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"  type="text/css" />    
    <link href="<%=ResolveUrl("~/Scripts/js.switch/main.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Scripts/js.switch/jquery.switchButton.css") %>" rel="stylesheet" type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
     <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/MasterRCT.js") %>" type="text/javascript"></script>
       <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

    </script>
</head>

<body style="overflow:hidden" >
     <form runat="server">   <asp:HiddenField ClientIDMode="Static" ID="hdnUserId" runat="server" /></form>
    <div class="switch Blue-Bar" id="divSwitch" style="display:none">
    <%--<div class="switch" id="divSwitch">--%>
        <div class="switch-wrapper">
            <input type="checkbox" value="1" checked>
        </div>
    </div>
     <div class="rctContent" id="divRCTContent">
       <%--<iframe id="frameLoadPopup" scroll="visible" height="100%" width="100%" frameborder="0"></iframe>--%>
         <iframe id="frameLoadPopup" scrolling="auto" width="100%" marginheight="0" frameborder="0" height="1000px" ></iframe>
    </div>


    <script src="<%=ResolveUrl("~/Scripts/js.switch/jquery.switchButton.js") %>" type="text/javascript"></script>


    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        var _isLoad = false;

        var buffer = 20; //scroll bar buffer
        var iframe = document.getElementById('frameLoadPopup');


        // .onload doesn't work with IE8 and older.
        if (iframe.attachEvent) {
            iframe.attachEvent("onload", resizeIframe);
        } else {
            iframe.onload = resizeIframe;
        }

        window.onresize = resizeIframe;

    </script>

</body>
</html>
