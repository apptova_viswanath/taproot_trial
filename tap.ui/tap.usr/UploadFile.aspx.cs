﻿//----------------------------------------------------------------------------------------------------
// File Name 	: AdminAttach.aspx.cs

// Date Created  : N/A

// Description   : Upload Attachment Page Methods (.pdf,.png,.jpg etc files and open popup page)

// Javascript File Used : js.atch.js
//-----------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using tap.dat;
using Microsoft.Win32;
#endregion

public partial class UploadFile : System.Web.UI.Page
{
    #region "Variables"    
    EFEntity _db = new EFEntity();
    tap.dom.gen.Cryptography _cryptography = new tap.dom.gen.Cryptography();
    tap.dom.gen.Attachments _attachment = new tap.dom.gen.Attachments();

    #endregion

    #region "Page Load Event"
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    #endregion

    #region "Save the files"
    private void SaveFile()
    {
        int maxSize = 30000000;
        btnUpload.Enabled = false;
        int companyId = 0;
       
        if (Session["CompanyId"] != null)
        {
            companyId = Convert.ToInt32(Session["CompanyId"].ToString());
        }
       
      
        lblErrorMessage.Text = string.Empty;
        int eventId = 0;
        string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
        eventId = int.Parse(strEventId);

        int folderId = 0;
        int.TryParse(RouteData.Values["FolderId"].ToString(), out folderId);
        
        if (file.PostedFile != null && file.PostedFile.FileName != "")
        {
            if (isValidExt(file.PostedFile.FileName))
            {
                if (file.PostedFile.ContentLength < maxSize)
                {
                    HttpPostedFile myFile = file.PostedFile;
                    BinaryReader b = new BinaryReader(myFile.InputStream);
                    Attachments attachments = new Attachments();
                    string fileName = myFile.FileName;
                    if (fileName.IndexOf('\\') > -1)
                        fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);

                    attachments.EventID = eventId;

                    //Get the file extension and get the content type based on the extension
                    string ext = System.IO.Path.GetExtension(fileName).ToLower();

                    attachments.AttachmentFolderID = folderId;
                    attachments.DocumentName = fileName;
                    attachments.Extension = ext;
                    attachments.AttachmentData = b.ReadBytes(myFile.ContentLength);
                    attachments.CompanyId = companyId;

                    _db.AddToAttachments(attachments);
                    _db.SaveChanges();

                    CloseModalPopup();
                    btnUpload.Enabled = true;
                }
                else
                {
                    lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    lblErrorMessage.Text = "File size exceeds maximum limit 1 GB.";
                    btnUpload.Enabled = true;
                }
            }
            else
            {
                lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                lblErrorMessage.Text = "Please select a valid file to upload.";
                btnUpload.Enabled = true;
            }
        }
        else
        {
            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
            lblErrorMessage.Text = "Please attach a file to upload.";
            btnUpload.Enabled = true;
        }
    }
    #endregion

    #region "Upload the file(s)"
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        //insert file in Db
        SaveFile();
    }
    #endregion

    #region "Close the modal popup page"
    protected void btnAttachClosePopup_Click(object sender, EventArgs e)
    {
        CloseModalPopup();
    }
    #endregion

    #region "Close the modal pop up"
    private void CloseModalPopup()
    {
        Response.Redirect("~/tap.handler/index.html?action=close");
    }
    #endregion

    public bool isValidExt(string file)
    {
        FileInfo fi = new FileInfo(file);
        string ext = fi.Extension;

        return ext == ".exe" ? false :
            ext == ".dll" ? false :
            ext == ".msi" ? false :
            ext == ".bat" ? false : true;
    }
}