﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPerson.ascx.cs" Inherits="tap.ui.usr.ctrl.ucPerson" %>
<style>
    .ui-dialog-title, .dialogFonts {
        font-size: 12px;
    }

    .ui-widget {
        font-size: 12px;
    }

    #txtSearchTeamUser {
        margin-bottom: 5px;
        width: 300px;
    }
    .hidden{
        display:none;
    }
    .TeamUserListGrid{
        margin: 0px; margin-top: 20px;width:600px; 
    }
</style>
<div id="divTeamUserListGridPopUp" class="hidden" title="Add Team Member">

    <div class="TeamUserListGrid"  >
        <div style="display: none; width: 100%; margin-bottom: 20px;" id="divMsgForNoUsers">
            All system user has been added as team member for this event. You can also add custom users manually by type in the text box their name.
        </div>
        <input type="text" class="span2 bootstrp1" id="txtSearchTeamUser" placeholder="TYPE NAME" title="TYPE NAME" autofocus />

        <input type="button" class="btn btn-primary btn-small hidden" id="addTeamMemberManually" value="Add Team Member" />

        <div id="divPersonAddManually" class="hidden">
            <%--Would you like to add <span id="spnPersonName"></span> to display on the report? --%>
                         
                         <br />
            <br />
            Add Person: <span id="txtNewUserName" style="color: blue;"></span>

        </div>
        <div id="divTeamUserListGrid" class="hidden">
            <table id="TeamUserListGrid" class="scroll notranslate">
                <tr>
                    <td />
                </tr>
            </table>
            <div id="TeamUserListGridNavigation" class="scroll" style="text-align: center;">
            </div>
        </div>
        <div class="btnCloseTeamMemberpopup">
            <a id="ancAddTeamMemberManually" class="btn btn-primary btn-small " href="" style="float: right; color:white" alt="Add Team Member">
               <%-- <img id="imgAddTeamMemberManually" class="addImage" src="<%=ResolveUrl("~/images/add.png")%>" alt="Add Team Member">--%>
                Add
            </a>
            <%--<input type="button" class="btn btn-primary btn-small" id="closeTeamMemberpopup" value="Cancel" style="float: right;" />--%>
        </div>
    </div>
</div>
