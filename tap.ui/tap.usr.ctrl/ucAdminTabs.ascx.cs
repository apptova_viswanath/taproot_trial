﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;

namespace tap.usr.ctrl
{
    public partial class ucAdminTabs : System.Web.UI.UserControl
    {
        #region For Global variable

        tap.dom.tab.AdminTabs _adminTab = new dom.tab.AdminTabs();
        string _virtualDirectoryPath = System.Configuration.ConfigurationManager.AppSettings["VirtualLocation"];

        #endregion

        #region For Page load

        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int moduleId = 0;
            string url = ((System.Web.Routing.Route)(Page.RouteData.Route)).Url;
            string[] splitUrl = url.Split('/');
            string root = splitUrl[0].ToLower();
            bool hasHomeOrEvent = (root == "home" || root == "event");

            if (!hasHomeOrEvent)
            {
                if (Page.RouteData.Values["Module"] != null)
                {
                    string moduleName = Page.RouteData.Values["Module"].ToString();
                    moduleId = ((moduleName == tap.dom.hlp.Enumeration.Modules.Incident.ToString()) ? (int)tap.dom.hlp.Enumeration.Modules.Incident : (moduleName == tap.dom.hlp.Enumeration.Modules.Investigation.ToString()) ? (int)tap.dom.hlp.Enumeration.Modules.Investigation : (moduleName == tap.dom.hlp.Enumeration.Modules.Audit.ToString()) ? (int)tap.dom.hlp.Enumeration.Modules.Audit : (moduleName == tap.dom.hlp.Enumeration.Modules.ActionPlan.ToString()) ? (int)tap.dom.hlp.Enumeration.Modules.ActionPlan : 0);
                }
                else
                    moduleId = (int)tap.dom.hlp.Enumeration.Modules.Incident;
                LoadTab(moduleId);
            }
        }
        #endregion

        #region For Loading the Tabs and subtabs
        /// <summary>
        /// LoadTab
        /// </summary>
        /// <param name="modId"></param>
        public void LoadTab(int moduleId)
        {
            int companyId = Convert.ToInt32(Session["CompanyId"]);
            int divisionId = 0;
            if (Page.RouteData.Values["DivisionID"] != null)
                int.TryParse(Page.RouteData.Values["DivisionID"].ToString(), out divisionId);
            if (divisionId > 0)
                companyId = divisionId;

            string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();

            //if (Convert.ToString(Session["AdminMainTabsContent"]) == string.Empty)
            //{
                ulMainTabs.InnerHtml = _adminTab.BuildHTMlMainTabs(baseURL, divisionId);
                Session["AdminMainTabsContent"] = ulMainTabs.InnerHtml;
            //}
            //else            
                ulMainTabs.InnerHtml = Convert.ToString(Session["AdminMainTabsContent"]);

            ulSubTabs.InnerHtml = _adminTab.BuildHTMLSubTabs(baseURL, moduleId, companyId, divisionId);
            // for Building New Tab
            //subTabContent.Append("<div id='lnk_0' class='div-admin-tabs'>" + BuildAnchorTag(baseURL, 0, NEW_TAB, tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, true, divisionId) + "</div>");

            HiddenField hdf = this.Parent.Parent.FindControl("hdAddNewUrl") as HiddenField;
            if (hdf != null)
            {
                hdf.Value = _adminTab.GetURL(baseURL, 0, "New Tab", tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, true, divisionId);
            }
        }
        #endregion
    }
}