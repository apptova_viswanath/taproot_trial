﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRCTNode.ascx.cs" Inherits="tap.usr.ctrl.ucRCTNode" %>
  <%--    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RootCauseTree.js") %>" type="text/javascript"></script>      
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTEffects.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTGuidance.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTAnalysisComments.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/VisualRCT.js") %>" type="text/javascript"></script>      
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/Text.js") %>" type="text/javascript"></script>      
  
  <script>
      var _baseURL = '<%= ResolveUrl("~/") %>';
  </script>--%>

   <!---Generalized  Function scripts -->
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-1.8.3.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.9.2.custom.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
<script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/VisualRCT.js") %>" type="text/javascript"></script>      
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/VisualRCT/Text.js") %>" type="text/javascript"></script>  
   


    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RootCauseTree.js") %>" type="text/javascript"></script>      
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTEffects.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTGuidance.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/RCTAnalysisComments.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.rct/GenericCause.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/TapRootTab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/tab.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"
        type="text/css" />

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet"
        type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/root.css") %>" rel="stylesheet"
        type="text/css" />

    <script src="<%=ResolveUrl("~/Scripts/jstree/jquery.cookie.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.com.js") %>" type="text/javascript"></script>
    <!-- Activity Logging --->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.sec.audit.js") %>" type="text/javascript"></script>
    <!--  For notifying  --------------->
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <!-- Notification plugin (modified plugin files) ------------->
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/js.noty/jquery.notify.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/themes/default.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/layouts/center.js") %>" type="text/javascript"></script>

    <script src="<%=ResolveUrl("~/Scripts/tap.js.admin/js.adm.mst.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.master/js.mst.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.tab/js.tab.hover.js") %>" type="text/javascript"></script>
     <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

     <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/GoogleTranslate.css") %>" rel="stylesheet" />
    <script type="text/javascript">

        var _baseURL = '<%= ResolveUrl("~/") %>';

        //google translation
        function googleTranslateElementInit() {

            new google.translate.TranslateElement({
                pageLanguage: 'en'
            }, 'google_translate_element');
        }

    </script>
    <style type="text/css">
        .ui-widget-content a {
            /*color: #EDEDED;*/
        }

        .ui-widget-header {
            background: none !important;
            border: none !important;
            color: none !important;
            font-weight: normal !important;
        }

        .popupTitle {
            background: url('../../Scripts/jquery/images/ui-bg_highlight-soft_75_74a6e2_1x100.png') repeat-x scroll 50% 50% #74A6E2;
            border: 1px solid #AAAAAA;
            color: #222222;
            font-weight: bold;
        }

        .ui-tabs .ui-tabs-nav li {
            white-space: normal !important;
        }

        #content {
            overflow: hidden !important;
        }

        .siteContent {
            height: 100% !important;
            /*background: #f9f9f9 !important;*/
        }

        .ui-accordion .ui-accordion-header .ui-icon {
            left: 2em !important;
        }

        .selectPath {
            visibility: visible !important;
            display: block !important;
        }

        .unSelectPath {
            visibility: visible !important;
            display: block !important;
        }

        .USER_RESPONSE_NULL {
            visibility: visible !important;
            display: block !important;
        }

        #divLinkContent div {
            display: none;
        }

        #spnRestate {
            vertical-align: top;
            margin-left: 2px;
        }

        .restateImage {
            cursor: pointer;
            margin-left: 5px;
            margin-top: 5px;
        }

        #lblNone {
            display: none;
            margin-left: 2px;
        }

        .genericQuestion {
            margin-left: 100px;
        }

        .textArea {
            width: 455px;
            height: 170px;
            margin-left:0px !important;
        }

        .buttonGeneric {
            text-align: center;
        }

        #divGettingStartedHeader {
            float: left;
            clear: both;
        }

        #divLinkContent {
            clear: both;
        }

        #divAccordion {
            float: left;
        }
        
.div-admin-tabs {
    float: left !important;
   
    list-style: none !important;
    margin: 0 1px 10px 0 !important;
    padding: 0 0 !important;
    height:auto !important;
    width:auto;
}
#divLinkContent a
{
    display: block !important;
    font-size: 12px !important;
    line-height: 30px !important;
    height: 30px !important;
    color: black !important;
    text-decoration: none !important;
    padding: 0 10px !important;
    border:1px solid black;
    cursor:pointer !important;
    text-align:center !important;
    vertical-align:middle !important;
    width:100px !important;
    background-color:none;
}
        #tabs {
            /*margin-top:40px;*/
            /*border: 1px solid #b1b1b1;*/
        }

        #divCausalFactor {
            /*font-family: Verdana;
            margin-bottom: 10px;
            margin-left: 5px;
            margin-top: 5px;
            color: #0080C0;*/
             background-color: #8ab4e6;
            font-family: Verdana;
            margin-bottom: 10px;            
            /*margin-top: 5px;*/
            padding-bottom: 5px;
            padding-left: 5px;
            padding-top: 5px;
            position: fixed !important;
            top: 0;
            width: 100%;
            z-index: 999;
            box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for ie and other*/
            -moz-box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for mozilla*/
            -webkit-box-shadow: 0px 0px 10px 2px rgba(17, 50, 111, 0.75);/*for chrome*/
        }

        #spnHeader {
            margin-left: 5px;
            font-weight: bold;
        }

        .analysisComments {
            display: none;
        }

        #lblQuestion2 {
            word-wrap: break-word;
        }

        .side {
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .top {
            border-top: 30px solid #000000;
            padding-top: 3px;
        }

        .bottomSection {
            border-bottom: 3px solid black !important;
        }

        .bottom {
            border-bottom: 0 solid #000000;
            padding-bottom: 2px;
        }


        .accordionHeader {
            margin-top: -1px !important;
        }

        .accordioncontent {
            margin-bottom: 1px !important;
        }

        .lblTextHeader {
            color: #FFFFFF;
            float: left;
            font-weight: bold;
            margin-left: 6px;
            margin-top: -25px;
            position: relative;
            display: none;
        }

        .container {
            border-bottom: 3px solid #000000;
            border-left: 3px solid #000000;
            border-right: 3px solid #000000;
            padding-left: 2px;
            padding-right: 2px;
        }

        .basicCauseHeader {
            background-color: #000000;
            color: #FFFFFF;
            height: 28px;
            margin-left: -5px;
            margin-right: -5px;
        }

        #divGenericRestate {
            display: none;
            word-wrap: break-word !important;
            overflow: auto !important;
            height: auto !important;
        }


        .MsoNormal a {
            color: black !important;
        }

        .MsoNormal {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            font-size: 13px !important;
            line-height: 18px !important;
            margin-top: 10px !important;
        }

            .MsoNormal span p {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

            .MsoNormal span {
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
            }

        #divDic {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif !important;
        }

            #divDic a {
                color: black !important;
              text-decoration: underline;
              cursor:pointer !important;
            }
        
         #divDic p
        {
            margin-bottom: 10px !important;
        }
        /*Testing COMMIT 1*/

     
        .btn-small {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    padding-bottom: 2px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 2px;
}
.btn-primary {
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    background-color: #006dcc;
    background-image: linear-gradient(to bottom, #0088cc, #0044cc);
    background-repeat: repeat-x;
    border-bottom-color: rgba(0, 0, 0, 0.25);
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: rgba(0, 0, 0, 0.1);
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: rgba(0, 0, 0, 0.1);
    border-top-color: rgba(0, 0, 0, 0.1);
    color: #ffffff;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}
.btn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-image-outset: 0 0 0 0;
    border-image-repeat: stretch stretch;
    border-image-slice: 100% 100% 100% 100%;
    border-image-source: none;
    border-image-width: 1 1 1 1;
    border-left-color-ltr-source: physical;
    border-left-color-rtl-source: physical;
    border-left-color-value: #ccc;
    border-left-style-ltr-source: physical;
    border-left-style-rtl-source: physical;
    border-left-style-value: solid;
    border-left-width-ltr-source: physical;
    border-left-width-rtl-source: physical;
    border-left-width-value: 1px;
    border-right-color-ltr-source: physical;
    border-right-color-rtl-source: physical;
    border-right-color-value: #ccc;
    border-right-style-ltr-source: physical;
    border-right-style-rtl-source: physical;
    border-right-style-value: solid;
    border-right-width-ltr-source: physical;
    border-right-width-rtl-source: physical;
    border-right-width-value: 1px;
    border-top-style: solid;
    border-top-width: 1px;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
    display: inline-block;
    line-height: 18px;
    margin-bottom: 0;
    text-align: center;
    vertical-align: middle;
}
 input[type=text], select {
     background: #ffffff;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
    font: italic 13px Georgia, "Times New Roman", Times, serif;
    outline: none;
    padding:5px 0px 5px 5px;
    margin-left:20px;
    color:#355779;
    box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
    border:1px solid #717171;
        }
         form textarea   {
     background: #ffffff;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    -khtml-border-radius: 3px;
    border-radius: 3px;
    font: italic 13px Georgia, "Times New Roman", Times, serif;
    outline: none;
    padding:5px 0px 5px 5px;
    margin-left:20px;
    color:#355779;
    box-shadow: 0 0 20px 0 #e0e0e0 inset, 0 1px 0 white;
    border:1px solid #717171;
        }

         
         html{
             background-color:white !important;
             background-image:none !important;
             overflow:scroll !important;
         }

         #content{
            
         }

         #divLinkContent a
         {
             font-size:14px !important;
             font-weight:bold !important;
         }
       

                  html{
                 overflow:scroll !important;
             }
           
             .siteContent
             {
                   overflow:initial !important;
             }
             #tabs{
                   overflow:initial !important;
             }
             .warningYellowImage
{
    float: left;
    background-image: url("../../Images/warning-yellow.png");
    background-repeat: no-repeat;
    padding-left: 20px;
    border: 0px solid red;
   color: red;
}

    </style>




 <div id="divCausalFactor">
        <h3 id="lblCausalFactor">Causal Factor: </h3>
    </div>
<div id="rctWindow" class="siteContent" style="margin-top:42px;">
        
   
<%--      <span style="height: 20px;"><br /></span>--%>
    <div id="tabs" >
       <br />
        <div id="header" style="width:100%;"">
        <span id="spnHeader" style="width:33.33%;float:left;" class="notranslate">Root Cause Tree®</span>
        <div id="DivloadingImage" style="text-align: center;width:33.33%;float:left;">
            <asp:Image ID="Image1" Height="40px" Width="40px" runat="server" ImageUrl="~/Images/ajax_loader_blue_512.gif" />
        </div>
        <div style="float: right; margin-right: 20px;">
            <span id="Span2"><span><b>Complete: </b>
                <input type="checkbox" style="margin-top: 5px !important" id="chkComplete" title="Complete" /></span></span>
        </div>
       </div>
        
        <div id="divTreeNode" style="clear:both !important"></div>
         

              <%--  <div id="divLinkContent"  style="width:100% !important; ">  --%>

        <div id="content1" style="margin-top:-150px;">

            <div id="divLinkContent1">

                    <div class="div-admin-tabs"  style="
overflow: visible;white-space: nowrap;width:400PX !important; margin-bottom:60px!important; margin-right:40px!important;  " id="div151" >                        
             
                        
                          <%-- <a name="rc151-" id="rc151" style="-webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
box-shadow: 6px 5px 5px 0px rgba(0,0,0,0.75);color: black !important; background-color:white !important; width: 155px !important; cursor: default !important; ">Equipment Difficulty</a>
                        <input type="hidden" id="hdnEquipmentDifficulty" />
                           <input type="hidden" id="hdnNodeType151" value="2"/>--%>
                           
                    </div>

            </div>
           
        </div>
    

    <div id="content" style="margin-top:10px;">

              <div id="questions" style="display:none;">
                   
                    <div id="divGettingStartedHeader">
                        <h3 id="lblHeader"></h3>
                        <div id="lblSubHeader"></div>

                    </div>

                    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                    <script id="rctTemplate" type="text/x-jquery-tmpl">

                        <div>
                            <label id="lblTextHeader" class="lblTextHeader"></label>

                            {{if GuidanceCount != 0}}
                                  <img id="imghelperFirst" class="guidance" title="Click to see Guidance" border='0' src="<%=ResolveUrl("~/Images/lightbulb.png")%>" onclick='DisplayGuidancePopUp(${RCID},"RCTDictionaryLink");' />
                            {{else}}     
                            <div id="divBlank" class="blank"></div>
                            {{/if}}
                    
                        <img id="imgAnalysisComments${RCID}" title="Analysis Comments" class="guidance analysisStatus" src="<%=ResolveUrl("~/Images/GreyStatus.png")%>" border='0' onclick='DisplayAnalysisPopUp(this);' />
                        <img id="imgErase" title="Erase" class="guidance erase" src="<%=ResolveUrl("~/Images/Eraser.png")%>" border='0' onclick='EraseStatus(this);' />
                        <img id="imgAllQuestionToNo" title="Set All Questions to NO" class="guidance allNo" src="<%=ResolveUrl("~/Images/Delete.png")%>" border='0' onclick='SetAllQuestionToNo(this);' />
                        <img id="imgSelectGreen" title="Select" class="guidance select" src="<%=ResolveUrl("~/Images/tick.png")%>" border='0' onclick='SelectNode(this);' />

                            <div id="divStatusImage" class="statusContent" name="${RCTValue}"></div>
                            <div id="divParent" class="parent">
                                <ul id="divRCTTitle">
                                    <span class="title">${RCTTitle}</span>
                                    <input type="hidden" id="hdnrc${RCID}" />
                                </ul>

                                <div id="divRCTQuestions" style="clear: both;">
                                    {{if Questions.length > 0}}
                                        <div id="divRadioButtons">
                                            {{each Questions}}   
                                              
                                                                <div class="userSelect translate">
                                                                  <input type="radio" onchange="SetRCTNodeStatus(this,event);" id="rbYes" name="${QuestionID}" value="Yes" />Yes
                                                                  <input type="radio" onchange="SetRCTNodeStatus(this, event);" id="rbNo" name="${QuestionID}" value="No" />No
                                                                    <input type="hidden" id="hdnQuestionResponse" value="${Response}" />
                                                              </div>
                                            <div class="question">{{html  Question}}</div>
                                            <div id="divanalysisComments" class="analysisComments translate">


                                                <input type="hidden" id="hdnRCIDForAnalysisComments" value="${RCID}" />
                                                <input type="hidden" id="hdnAnalysisComments" value="${AnalysisComments}" />

                                                <span id="Span1">Enter Analysis Comments:</span>
                                                <br>
                                                <textarea rows="2" cols="100" class="textArea" id="txtAnalysisComments">${AnalysisComments}</textarea>

                                                <div class="buttonGeneric">
                                                    <br />

                                                    <input type="button" class="btn btn-primary btn-small" id="btnSaveAnalysisComments" value="Apply Changes" />
                                                    <input type="button" class="btn btn-primary btn-small" id="btnCancelAnalysisComments" value="Cancel" />
                                                </div>

                                            </div>


                                            <span class="separator"></span>

                                            {{/each}}
                                        </div>

                                    {{/if}}
                                <br />
                                    <div style="margin-left: 5px; margin-top: 10px; float: left; clear: both; width: 100%">
                                        {{if Children}}   
                                                  
                                        {{tmpl(Children) '#rctTemplate'}}
                                              
                                {{/if}}
                                    </div>
                                </div>



                            </div>
                        </div>
                    </script>

                    <ul id="divAccordion"></ul>

                </div>

        </div>

    </div>


</div>

  