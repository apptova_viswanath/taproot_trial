﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucGenericCause.ascx.cs" Inherits="tap.usr.ctrl.ucGenericCause" %>
       <div id="divGenericCausesContent">
                    <br />
                    <b>Root Causes Identified</b>
                    <br />

                    <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                    <script id="genericTemplate" type="text/x-jquery-tmpl">

                        <div id="divGenericCause" class="parent">

                            
                            <input type="hidden" id="hdnRCType"  value="${RootCauseType}"/>
                            <div class='gcLinks' style="float: right !important; text-align: right;">
                                <a id="lnkEditAnalysis" style="float: left !important; margin-right: 20px;" class="causeAnalysis" href="javascript:void(0)"  onclick="DisplayOrHideContent(this);return false;">Edit Generic Cause Analysis</a>
                                <a id="lnkPerformAnalysis" class="causeAnalysis" href="javascript:void(0)"  onclick="DisplayOrHideContent(this);return false;">Perform Generic Cause Analysis</a>


                            </div>
                            <input type="hidden" id="hdnRCTId" value="${RCTID}" />
                            <input type="hidden" id="hdnIsGeneric" value="${isGeneric}" />

                            <ul id="Ul1" >
                                <span class="title">${RCTTitle}</span>
                                 <input type="hidden" id="hdntitle" value="${RCTTitle}" />
                            </ul>
                            <div id="divGenericQuestions">

                                <div id="divGenericQuestionsContent">

                                    <div style="clear: both">What was the tool, equipment, process, procedure or item involved in <b><i>${RCTTitle}</i></b>?  (State this in the field below as concisely as possible.)</div>
                                    <input type="hidden" class="question1" id="Hidden2" value="${Question1Response}" />
                                    <input type="text" maxlength="256" style="clear: both; margin-bottom: 5px; margin-top: 10px; width: 50%;" value="${Question1Response}"
                                        onkeyup="SetQuestionText(this);return false;" class="textAreaQuestion1Response" id="txtQuestion1Response" />

                                    <div id="divQuestion2" style="display: none; clear: both; margin-top: 2px;">
                                        Do you have any more
                                       <label id="lblQuestion2">${Question1Response}</label>
                                        ? 
                                         <input type="hidden" class="question2" id="hdnQuestion2Response" value="${Question2Response}" />
                                        <input type="radio" name="${RCTID}Question2" id="rbQuestion2Yes" value="Yes" />Yes
                                       <input type="radio" name="${RCTID}Question2" id="rbQuestion2No" value="No" />No
                                    </div>

                                    <div id="divQuestion3" style="display: none; clear: both; margin-top: 2px;">
                                        Do a significant number of 
                                        <label id="lblQuestion3">${Question1Response}</label>
                                        have problems with <b><i>${RCTTitle}</i></b>?  
                                         <input type="hidden" class="question3" id="hdnQuestion3Response" value="${Question3Response}" />
                                        <input type="radio" name="${RCTID}Question3" id="rbQuestion3Yes" value="Yes" />Yes
                                       <input type="radio" name="${RCTID}Question3" id="rbQuestion3No" value="No" />No
                                 
                                    </div>

                                </div>


                                <span class="separator"></span>
                            </div>

                        </div>



                    </script>

                    <ul id="divGenericCauseAccordion"></ul>



                    <div id="divGenericRestate">

                        <input type="hidden" id="hdnRCTTitleGeneric" />
                        <input type="hidden" id="hdnRCTIDGeneric" />
                        <input type="hidden" id="hdnQuestion2" />
                        <input type="hidden" id="hdnQuestion3" />
                        <span>What in the system is allowing this to exist? (<span id="spnRestateCause"></span> & <b><i><span id="spnRCTTitle"></span>

                             <input type="hidden" id="hdnEditGenericRCTTitle" value="" />
                                                                                                                     </i></b>)</span><br />
                        <textarea rows="2" cols="100" class="textArea ContentInputTextBox" id="txtGenericCause"></textarea>

                        <div class="buttonGeneric">
                            <br />

                            <input type="button" class="btn btn-primary btn-small" id="btnSaveGenericCause" value="Create" />
                            <input type="button" class="btn btn-primary btn-small" id="btnGenericCauseClose" value="Close" />
                        </div>

                    </div>

                    <br />
                    <div id="div1">


                        <b>Generic Causes Identified</b>
                        <br />
                        <label id="lblNone">None</label><br />

                        <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                        <script id="GenericCauseStatementsTemplate" type="text/x-jquery-tmpl">

                            <div id="div4">

                                <span id="spnRestate">${Restate}</span>
                                     <input type="hidden" id="hdnEditRestate" value="${Restate}" />
                                <img title="Edit" src="<%=ResolveUrl("~/Images/Edit.png") %>" class="restateImage" id="btnEditGenericCause" onclick="DisplayEditStatement(this);return false;">
                                <img title="Delete" src="<%=ResolveUrl("~/Images/Delete.png") %>" class="restateImage" id="btnDeleteGenericCause" onclick="DeleteGenericCauseByID(this,event);return false;">

                                <div id="divEditGenericCause" style="display: none;">
                                    <br />

                                    <input type="hidden" id="hdnEditGenericCauseID" value="${GenericCauseID}" />
                                    <textarea rows="2" cols="100" class="textArea ContentInputTextBox" id="txtEditGenericCause">${Restate}</textarea>

                                    <div class="buttonGeneric">
                                        <br />

                                        <input type="button" class="btn btn-primary btn-small" id="btnEditGenericCauseSave" value="Apply Changes" />
                                        <input type="button" class="btn btn-primary btn-small" id="btnEditGenericCauseClose" value="Cancel" />
                                    </div>

                                </div>

                            </div>




                        </script>
                        <ul id="divGenericCauseStatements"></ul>

                    </div>



                </div>