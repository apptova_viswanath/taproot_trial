﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTask.ascx.cs" Inherits="tap.ui.usr.ctrl.ucTask" %>

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Master.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Content.css") %>" rel="stylesheet" type="text/css" />
 <link href="<%=ResolveUrl("~/Scripts/jquery/jquery-ui-1.8.23.custom.css") %>" rel="stylesheet"  type="text/css" />
 <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/Season.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/tap.ui.css/tap.stl.cus/CorrectiveActionTab.css") %>" rel="stylesheet" type="text/css" />

    <link href="<%=ResolveUrl("~/tap.ui.css/tap.js.noty/jNotify.jquery.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.notifi.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/js.noty/jNotify.jquery.js") %>" type="text/javascript"></script>   

           <script src="<%=ResolveUrl("~/Scripts/tap.js.common/js.person.js") %>" type="text/javascript"></script>
    <%--<script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.usr.tasks.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Scripts/tap.js.usr/js.tasks.popup.js") %>" type="text/javascript"></script>--%>
   <script type="text/javascript">
       var _baseURL = '<%= ResolveUrl("~/") %>';         
    </script>
<style>
        #ui-datepicker-div {
            font-size: 12px;
        }

</style>

  <div class="taskHeader" id="divTaskHeader">
            <a id="lnkCreateTask" href="#createTask">Add Task</a>
            <div class="statusPanel">
                <div class="greenSelectImage">
                    Completed
                </div>
                <div class=" warningYellowImage">
                    Not Completed
                </div>
                <div class=" warningRedImage">
                    Past Due
                </div>
            </div>
        </div>
  <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>

        <script id="taskTemplate" type="text/x-jquery-tmpl">
           
             {{if Tasks.length > 0}}           
                    
            <div class="causalFactorContentBorder">
                        
                        <div class="blueHeader">
                            Tasks
                        </div>

                        <div class="ContentWidth">
                           
                            {{each Tasks}}
                                    <div class="ClearAll"> 
                                        {{if TaskStatus == true}}
                                            <img id="imgCF" src="<%=ResolveUrl("~/Images/green-check.png") %>">
                                        {{else}}
                                            {{if TaskStatus == "PastDue" }}
                                                <img id="img1" src= "<%=ResolveUrl("~/Images/warning-red.png") %>" />
                                            {{else}}
                                                <img id="img2" src= "<%=ResolveUrl("~/Images/warning-yellow.png") %>">
                                            {{/if}}
                                
                                        {{/if}} 
                                        ${TaskType} (Due:  {{if TaskDueDate != null}} ${ConvertToDate(TaskDueDate)} {{else}} Not Set){{/if}}) 
                                        <input type="button" id="Button1" class="btn btn-primary btn-small" onclick='OpenTaskPopup("${TaskID}");' style="float: none; vertical-align: top; width: 50px" value="Edit" />
                                    </div>
                                    <b class="marginLeft50">Responsible Person: </b>${ResponsiblePerson}<br />
                                   
                            <table class="taskTable">
                                
                                <tr>
                                    <td><b >Description: </b></td>
                                    <td> <label class="bio">${Description}</label><br /></td>
                                </tr>

                            </table>                            
                                   
                                    {{if DueDateHistory.length >1}}
                            <div class="marginLeft50">

                                <b>Due Date History: </b>
                                {{each DueDateHistory}}
                                        <div class="marginLeft20">${DueDateType} :  {{if DueDate != null}} ${ConvertToDate(DueDate)} {{/if}}</div>
                                        {{/each}}
                            </div>
                                        
                                        
                                    {{else}}                        
                                    {{/if}}  
                            <br />                   
                           {{/each}}
                        </div>
                    </div>
            {{/if}}
        </script>       
        <ul id="taskTemplateResults"></ul>      
  <div id="divAddEditTaskDialog" title="Task" class="seasonContentHide">
            <div>
                <label id="lblMessage">
                </label>
            </div>
            <div id="divTask">
                <table>
                    <tr>
                        <td><span class="taskLabel" ><strong>Type:</strong><span id="spnIdentifierName" style="color: Red">*</span> </span></td>
                        <td><select id="ddlTaskType" class="ContentInputTextBox  WidthP40 taskText"></select>   </td>
                    </tr>
                    <tr>
                        <td><span class="taskLabel"><strong>Responsible Person:</strong><span id="Span2" style="color: Red">*</span> </span></td>
                        <td><div style="margin-left:20px;"><span id="spnTaskResponsiblePerson" ></span><img class='editImage1' onclick="OpenAddTeamUserPopup('TaskResponsiblePerson')" src="<%=ResolveUrl("../Images/edit.gif") %>" /></div></td>
                    </tr>
                    <tr>
                        <td><span class="taskLabel"><strong>Due Date:</strong><span id="Span3" style="color: Red">*</span> </span></td>
                        <td><input type="text" id="txtDueDate" readonly="readonly" tabindex="-1" class="DateInputTextBox" /></td>
                    </tr>
                </table>
               <%-- <div>
                    
                                      
                </div>
                <div>
                    
                  
                                   
                </div>--%>
                <%--<div>
                    
                    
                </div>--%>

                <div id="divIsCompleted">
                    <span id="Span1" class="taskLabel"><strong>Completed:</strong> </span>
                    <input class="MarginLeft20" id="chkIsCompleted" type="checkbox" />
                </div>

                <div>
                    <span><strong>Description</strong><span id="Span4" style="color: Red">*</span></span>
                    <br />
                    <textarea id="txtDescription" class="ContentInputTextBox WidthP40 popupTextArea"></textarea>
                </div>

                <div id="divNotes">
                    <span><strong>Notes</strong></span>
                    <br />
                    <textarea id="txtTaskNotes" class="ContentInputTextBox WidthP40 popupTextArea"></textarea>
                </div>

                <div id="divButton">
                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnCloseTask" value="Cancel" />
                    <input type="button" class="btn btn-primary btn-small btn-float" id="btnSaveTask" value="Create" />
<%--                    <a class="btn btn-primary btn-small btn-float" href="#" id="btnCloseTask">Cancel</a>
                    <a class="btn btn-primary btn-small btn-float" href="#" id="btnSaveTask">Save</a>--%>
                </div>
            </div>
        </div>
    
