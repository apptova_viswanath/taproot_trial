﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom.usr;
using tap.dom.hlp;
using System.Configuration;
#endregion

namespace tap.usr.ctrl
{
    public partial class UcNavBreadCrumb : System.Web.UI.UserControl
    {
        #region "Variables"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.adm.Divisions _division = new dom.adm.Divisions();
        public string _isSU = ConfigurationManager.AppSettings["IsSU"];
        tap.dom.adm.Company _company = new dom.adm.Company();
        #endregion

        #region "Page load Event"
        protected void Page_Load(object sender, EventArgs e)
        {
            navigationBreadCrumb.InnerHtml = BuildBreadcrumbs();
        }
        #endregion

        #region "Get Event name"
        private string GetEventName()
        {

            //does this page have an event id?
            if (Page.RouteData.Values["EventId"] == null)
            {
                Session["EventName"] = null;
                Session["EventId"] = null;
                return null;
            }
            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() !="0")?  _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()):"0";
            //int eventId = int.Parse(Page.RouteData.Values["EventId"].ToString());
            int eventId = int.Parse(strEventId);

            //is this an event page?

            //we need latest update event name in navigation path.
            //if (Session["EventId"] == null || Session["EventName"] == null || int.Parse(Session["EventId"].ToString()) != eventId)
            //{

            dat.Events events = _db.Events.Where(a => a.EventID == eventId).FirstOrDefault();
                if (events != null)
                {
                    Session["EventName"] = events.Name;
                    Session["EventId"] = eventId;
                    return Session["EventName"].ToString();
                }
                return string.Empty;
            //}
            
        }
        #endregion

        #region "Build Navigation Breadcrumb"
        private string BuildBreadcrumbs()
        {
            string url = ((System.Web.Routing.Route)(Page.RouteData.Route)).Url;
            string[] splitUrl = url.Split('/');

            string root = splitUrl[0].ToLower();

            if (root == "home")
                return "Home";

            if (root == "event")
                return BuildEventPageBreadCrumbs(splitUrl);

            //is this an event page?
            if (root == "admin")
                return BuildAdminBreadcrumbs(splitUrl);

            return string.Empty;
        }
        #endregion

        #region "Build Event page Navigation Breadcrumb"
        private string BuildEventPageBreadCrumbs(string[] urlArray)
        {
            string eventName = GetEventName();
            string moduleName = Page.RouteData.Values["Module"].ToString();
            string tabName = string.Empty;
            // string tabName = urlArray[2].ToLower().StartsWith("attachments") ? "Attachments" : urlArray[2].ToLower().StartsWith("7-steps") ? "7-Steps" : urlArray[2].ToLower().StartsWith("details") ? "Details" : Page.RouteData.Values["CustomSubTab"].ToString();

            string customTabName = urlArray[2].Contains("-") ? urlArray[2].Substring(0, urlArray[2].LastIndexOf("-")).ToLower() : urlArray[2].ToLower();
            switch (customTabName)
            {
                case "attachments":
                    tabName = "Attachments";
                    break;

                case "7-steps":
                    tabName = "7-Steps";
                    break;

                case "details":
                    tabName = "Details";
                    break;

                case "sharing":
                    tabName = "Sharing";
                    break;

                case "taproot":
                    tabName = "TapRooT";
                    break;

                case "fix":
                    tabName = "Fix";
                    break;

                case "report":
                    tabName="Report";
                    break;

                case "reference":
                    tabName = "Reference";
                    break;
                case "correctiveactions":
                    tabName = "Corrective Actions";
                    break;

                default:
                    tabName = Page.RouteData.Values["CustomSubTab"].ToString();
                    break;

            }

            string strEventId = (!string.IsNullOrEmpty(Page.RouteData.Values["EventId"].ToString()) && Page.RouteData.Values["EventId"].ToString() != "0") ? _cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()) : "0";
            int eventId = int.Parse(strEventId);
            //int eventId = int.Parse(Page.RouteData.Values["EventId"].ToString());

            //else part is for creating new events
            string breadCrumb = ((eventName != "") ? String.Format("{0} > {1} > {2}", moduleName, eventName, tabName) : String.Format("{0}s > Create {0}", moduleName, tabName));

            return breadCrumb;
        }
        #endregion

        #region "Build Admin pages Navigation Breadcrumb"
        private string BuildAdminBreadcrumbs(string[] urlArray)
        {
            string breadCrumbs = "Admin > ";

            // User details page
            if (urlArray[1].ToLower() == "users")
            {
                // Editing existing user details
                if( urlArray.Length > 3 )
                {
                    breadCrumbs = String.Format("{0} {1} > {2} ", breadCrumbs, "Users", "Edit");
                }
                else
                {
                    if (urlArray.Length == 2)
                    {
                        //breadCrumbs = String.Format("{0} {1}", breadCrumbs, "Create User");                    
                        //breadCrumbs = String.Format("{0} {1}", breadCrumbs, "Manage Users");
                        breadCrumbs = String.Format("{0} {1} > {2}", breadCrumbs, "Users","View User");
                    }
                    else
                    {
                        //breadCrumbs = String.Format("{0} {1} > {2} ", breadCrumbs, "Manage Users", "Create User");
                        breadCrumbs = String.Format("{0} {1} > {2} ", breadCrumbs, "Users", "Create User");
                    }
                }
                return breadCrumbs;
            }

            //for division users
            if (urlArray.Length > 2 && urlArray[2].ToLower() == "users")
            {
                if (urlArray.Length > 3)
                {
                    breadCrumbs = String.Format("{0} {1} > {2} > {3}", breadCrumbs,"Division", "Users", "Edit");
                }
                else
                {
                    if (urlArray.Length == 3)
                    {
                        breadCrumbs = String.Format("{0} {1} > {2} > {3}", breadCrumbs, "Division", "Users", "View User");
                    }
                    else
                    {
                        breadCrumbs = String.Format("{0} {1} > {2} > {3} ", breadCrumbs, "Division", "Users", "Create User");
                    }
                }
                return breadCrumbs;
            }
            if (urlArray[1].ToLower() == "division")
            {
                
                if (urlArray.Length > 3 && urlArray[3] == "{Module}")
                    breadCrumbs = String.Format("{0} {1} > {2} > {3}", breadCrumbs, "Division", Page.RouteData.Values["Module"].ToString(), Page.RouteData.Values["AdmCustomSubTab"].ToString());
                else
                    breadCrumbs = String.Format("{0} {1} > {2} ", breadCrumbs, "Division", urlArray[3]);

                return breadCrumbs;
            }
            if (urlArray[1].ToLower() == "configuration" && urlArray.Length > 2)
            {
                if (urlArray[2] == "{Module}")
                    breadCrumbs = String.Format("{0} {1} > {2} > {3}", breadCrumbs, "Configuration", Page.RouteData.Values["Module"].ToString(), Page.RouteData.Values["AdmCustomSubTab"].ToString());
                else
                    breadCrumbs = String.Format("{0} {1} > {2} ", breadCrumbs, "Configuration", "Settings");

                return breadCrumbs;
            }

            for (int i = 1; i < urlArray.Length; i++)
            {
                breadCrumbs += UrlRewrite(urlArray[i]);
            }

            breadCrumbs = ((breadCrumbs.EndsWith(">")) ? breadCrumbs.Remove(breadCrumbs.LastIndexOf("> "), 1) : breadCrumbs.Remove(breadCrumbs.LastIndexOf("> "), 2));
            return breadCrumbs;
        }
        #endregion

        #region "Url Rewrite with existing page url"
        private string UrlRewrite(string item)
        {
            //if block for custom tabs and else block for other subtabs
            if (item == "{AdmCustomSubTab}-{AdmCustomSubTabId}")
                return int.Parse(Page.RouteData.Values["AdmCustomSubTabId"].ToString()) == 0 ? "> New Tab" : " > " + Page.RouteData.Values["AdmCustomSubTab"].ToString().Replace("-", " ");
            else
                return item == "{Module}" ? Page.RouteData.Values["Module"].ToString() + " > " : item.Replace("-", " ") + " > ";
        }
        #endregion


        #region Navigation BreadCrumb for Taproot Tab
        /// <summary>
        /// Build the Navigation breadcrumb for Taproot System Tab
        /// </summary>
        /// <returns></returns>
        public string BuildTapRootTabBreadCrumb(string eventId)
        {
            tap.dom.usr.TapRootTab tapRootTab = new tap.dom.usr.TapRootTab();

            var seasonId = tapRootTab.SnapChartDataSelect(eventId);

            string seasonName = ((seasonId == (int)tap.dom.hlp.Enumeration.Season.Summer) ? tap.dom.hlp.Enumeration.Season.Summer.ToString()
                               : (seasonId == (int)tap.dom.hlp.Enumeration.Season.Autumn) ? tap.dom.hlp.Enumeration.Season.Autumn.ToString()
                                 : tap.dom.hlp.Enumeration.Season.Spring.ToString());
            return seasonName;
        }

        #endregion

    }
}
