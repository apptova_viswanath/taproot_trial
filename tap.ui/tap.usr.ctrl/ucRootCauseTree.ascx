﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucRootCauseTree.ascx.cs" Inherits="tap.usr.ctrl.ucRootCauseTree" %>
 <style type="text/css">

     .Section1 p
     {
         margin-top:10px !important;
     }


 </style>
<div class="ContentWidth" style="margin:15px 0px 0px 12px;overflow:auto;">
             
                 <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>"></script>
                 <script id="rctTemplate" type="text/x-jquery-tmpl">
                   
                         <div>

                         
                            {{if IsComplete == 1}}
                                <img id="imgCF" style="float:left" class="seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>">
                            {{else}}
                                <img id="imgCF1" style="float:left" class="seasonContentHide" src="<%=ResolveUrl("~/Images/warning-yellow.png") %>">
                            {{/if}}
                           
                           <div style="margin-left:20px;"> <span class="notranslate">CAUSAL FACTOR: ${Name}</span></div>
                        </div>                       

                    
                         <div id="divCFAnalyze" class="causalFactorAnalyze seasonContentHide">                              
                            <span >Analyze with: <a class="notranslate"  href="javascript:void(0)"  onclick="RedirectToRCT(${CausalFactorID});return false;" id="lnkRootCauseTree">Root Cause Tree®</a> 
                              
                                    </span>
                         </div>



                     <div id="divRCT" style="height:auto;" class="rctSection">
                         <strong style="margin-left:45px;">Root Causes Identified  </strong>
                         {{if RootCauses.length > 0}}
                         <br />
                         {{each RootCauses}}                           
                             
                         <div style="margin-left:48px;clear:both;" >

                                    <div id="divRCTStatus" class="rctStatus notranslate">
                                        {{if AddressedByCA == 0}}
                                        <img id="imgRCT" class="seasonContentHide" src="<%=ResolveUrl("~/Images/warning-yellow.png") %>">
                                        {{else}}
                                        <img id="imgRCT1" class="seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>">
                                        {{/if}}
                                                   
                                        <input style="margin-top:3px" type="checkbox" class="rctNode seasonContentHide" value='${RootCauseTreeID}' onclick='SaveRCTModifiedValue(this);' />
                                    </div>

                                    <span class="cfRootCuaseTitleText" style="margin-left:5px;" class="notranslate">${RootCause}</span>                                                    
                                    <a class="help seasonContentHide" id="lnkCAPDictionary">
                                        <img id="img1" title="Corrective Action Helper" border='0' src="<%=ResolveUrl("~/Images/lightbulb.png")%>" onclick='DisplayCAPGuidancePopUp("${RootCauseID}","CAPDictionaryLink", "${RootCause}");' />                                                                             
                                    </a>
                                 <br />
                          
                           
                                 </div>      
                         {{/each}}<br />

                         {{else}}
                         <br />
                          <span style="margin-left:45px;margin-top:1px;" >None</span> 
                         {{/if}}
                     </div>

                      
                       <div id="div1" class="rctSection"><br />
                         <strong style="margin-left:45px;margin-top:20px;">Generic Causes Identified  </strong>
                         {{if GenericCauses.length > 0}}
                         <br />
                         {{each GenericCauses}}                           
                                                <div style="margin-left:48px;clear:both;" >
                                                     <div id="div2" class="rctStatus notranslate">
                                                         {{if GenericCauseAddressedByCA == 0}}
                                                           <img id="imgGenericRCT" class="seasonContentHide" src="<%=ResolveUrl("~/Images/warning-yellow.png") %>">
                                                         {{else}}
                                                           <img id="imgGenericRCT1" class="seasonContentHide" src="<%=ResolveUrl("~/Images/green-check.png") %>">
                                                         {{/if}}
                                                   
                                                     <input style="margin-top:3px" type="checkbox" class="genericNode seasonContentHide" value='${GenericCauseID}' onclick='SaveGenericModifiedValue(this);' />
                                                     </div>
                        <span style="clear:both;margin-left:5px;">${Restate}</span>
                                                 

                           

                         <br />
</div>

                         {{/each}}<br />

                         {{else}}
                         <br />
                          <span style="margin-left:45px;">None</span> 
                         {{/if}}
                     </div>

                      <br />
                 </script>

    <ul><li id="rctTemplateResults"></li></ul>
    <br />
    <label id="lblNone" class="notranslate" style="display:none; margin-left: 2px;">There is no causal factor identified.</label>
     
</div>
