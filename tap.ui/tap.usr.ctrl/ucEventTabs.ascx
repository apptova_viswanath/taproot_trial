﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucEventTabs.ascx.cs"
    Inherits="tap.usr.ctrl.ucEventTabs" %>

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 
<style>   
     .ui-widget-header {
        border: 0px solid #aaaaaa;
    }
</style>

<div id="divTab"> 
<ul id="divMainTabs"  class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"   runat="server">
</ul>
<div id="ulSubTabs" ClientIDMode="Static" runat="server"  >
   
</div>
    </div>
