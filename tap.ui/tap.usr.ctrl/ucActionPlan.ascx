﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucActionPlan.ascx.cs" Inherits="tap.ui.usr.ctrl.ucActionPlan" %>
  <div id="divCausalFactorContent" class="ClearAll" >  
                      
                               <script src="<%=ResolveUrl("~/Scripts/jquery/jquery.tmpl.js") %>">
                               </script>
                               <script id="capTemplate" type="text/x-jquery-tmpl">                           
                                   
                                   
                                    <div class="correctiveActionMain"> CORRECTIVE ACTION:            
                                        <span class="notranslate">${Identifier}</span>
                                         <input type="hidden" id="hdnIsSmart"  value="${IsSmarter}" />
                        
                                        <input type="button" id='btnEdit'"${CorrectiveActionID}" onclick='OpenEditCorrectiveActionPopup("${CorrectiveActionID}", "${IsSmarter}", false);' class="editButton" value="Edit" />
                                     </div>

                                     {{if Tasks}}
                                        {{if Tasks.length > 0}}      
                                            <div class="marginLeft30"><div><strong>Tasks</strong></div>
                                                <div class="ClearAll notranslate">
                                                    {{each Tasks}}         
                                                       
                            {{if TaskStatus == "Completed"}}
                             <img id="imgCF" src="<%=ResolveUrl("~/Images/green-check.png") %>">

                            {{else}}
                                {{if TaskStatus == "PastDue"}}
                                <img id="img1" src= "<%=ResolveUrl("~/Images/warning-red.png") %>" />

                                {{else}}
                                <img id="img2" src= "<%=ResolveUrl("~/Images/warning-yellow.png") %>">

                                {{/if}}
                                
                            {{/if}} 
                                                    ${Type} (Due:  {{if DueDate != null}}  ${ConvertToDate(DueDate)}){{else}}Not Set)  {{/if}} <br />


                                                    {{/each}}
                                                </div>
                                            </div> 
                                        {{else}}      
                                               <div></div>
                                        {{/if}}                                                                
                                            <br />
                                     {{/if}}



                            </script>

                            <ul><li style="border:0px solid red" id="results"></li></ul>
                              
                     
                </div>