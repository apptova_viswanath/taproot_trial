﻿//---------------------------------------------------------------------------------------------
// File Name 	 : ucCustomFields.ascx.cs

// Date Created  : N/A

// Description   : Load the HTML controls
//---------------------------------------------------------------------------------------------

#region Namespace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using tap.dom;
#endregion

namespace tap.usr.ctrl
{
    public partial class ucCustomFields : System.Web.UI.UserControl
    {
        #region For Global variables  and property

        int _tabId;
       public string _eventId;
        string _tabName;
       public string testString = "test the string";
        #endregion

        #region For Pageload
        /// <summary>
        /// Populate the Controls and the Values based on eventid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            GetIdsFromUrl();
            DisplayTabName();
            bool hasEventId = ((string.IsNullOrEmpty(_eventId)) ? false : true);
            LoadCustomFieldValue(hasEventId);
        }
        #endregion

        #region To get The Eventid  and  Tabid From URl
        /// <summary>
        /// To get the Tabid and EventId from the URl
        /// </summary>
        /// <param name="tabid"></param>
        /// <param name="eventid"></param>
        public void GetIdsFromUrl()
        {
            int.TryParse(Page.RouteData.Values["CustomSubTabId"].ToString(), out _tabId);
            _eventId = Page.RouteData.Values["EventId"].ToString();
          
            if (Page.RouteData.Values["CustomSubTab"] != null)
            {
                _tabName = Page.RouteData.Values["CustomSubTab"].ToString();
                _tabName = (_tabName.Contains("-") ? _tabName.Replace("-", "  ") : _tabName);
            }

        }
        #endregion

        #region To load Field and  Fieldvalue
        /// <summary>
        /// To Load the HTMl controls and the values of the control
        /// </summary>
        /// <param name="eventid"></param>
        /// <param name="tabid"></param>
        public void LoadCustomFieldValue(bool includeFieldValue)
        {
            tap.dom.tab.CustomFields _customField = new dom.tab.CustomFields();
          
            string customFieldContent = _customField.BuildHTMLPage(_eventId, _tabId, includeFieldValue, tap.dom.adm.CommonOperation.GetSiteRoot(),
                Session["CompanyId"].ToString());
            divCustomfield.InnerHtml = customFieldContent;

        }
        #endregion

        #region Tabname
        /// <summary>
        /// Display the Tabname
        /// </summary>
        public void DisplayTabName()
        {
           lblHeader.InnerText = ((_tabName != "Details") ? _tabName : string.Empty);           
        }
        #endregion                

    }
}