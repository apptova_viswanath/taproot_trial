﻿//---------------------------------------------------------------------------------------------
// File Name 	: ucTopLevelNav.cs

// Date Created  : N/A

// Description   : Top level navigation bar used for Master page
//---------------------------------------------------------------------------------------------

#region "Namespaces"

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Hosting;
using tap.dom.adm;
using tap.dom.hlp;
using tap.dom.gen;

#endregion

namespace tap.ui.usr.ctrl
{
    public partial class ucTopLevelNav : System.Web.UI.UserControl
    {
        #region Fetch the contents of top level menu bar based on company id

        /// <summary>
        /// Fetch the contents of top level menu bar based on company id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["setup"] == "1")
                {
                    Response.RedirectToRoute("AdminSetting");
                }

                tap.dom.gen.TopNavigators navigators = new TopNavigators();
                string companyId = Convert.ToString(Session["CompanyId"]);
                string userId = Convert.ToString(Session["UserId"]);
                tap.dom.adm.CommonOperation commonOperation = new tap.dom.adm.CommonOperation();

                if (companyId == string.Empty)
                {
                    Exception ex = new Exception("Company id coming as empty from session");
                    ErrorLog.LogException(ex);
                    Response.Redirect("~/tap.usr/Login.aspx?message=1");
                }


                divTopNavigationBar.InnerHtml = navigators.GetTopNavData(companyId, userId, tap.dom.adm.CommonOperation.GetSiteRoot());

            }
            catch (Exception ex)
            {
                ErrorLog.LogException(ex);
            }
        }

        #endregion
    }
}