﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAdminTabs.ascx.cs" 
    Inherits="tap.usr.ctrl.ucAdminTabs" %>

     <%-- Customized theme from jquery theme roller --%>
    <link href="<%=ResolveUrl("~/tap.ui.css/custom-theme/jquery.ui.all.css") %>" rel="stylesheet"
        type="text/css" />   
     <%-- Customized theme ends  --%> 
<style>  
     /*override the border */
     .ui-widget-header {
        border: 0px solid #aaaaaa;
    }
</style>
<ul id="ulMainTabs" runat="server">
</ul>
<div id="ulSubTabs" ClientIDMode="Static" class="ClearAll " runat="server">
</div>
