﻿//---------------------------------------------------------------------------------------------
// File Name 	: ucEventTabs.ascx.cs

// Date Created  : N/A

// Description   : Load the Tabs and SubTabs in Public site
//---------------------------------------------------------------------------------------------

#region Namespace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using tap.dom;
using tap.dom.gen;
#endregion

namespace tap.usr.ctrl
{

    public partial class ucEventTabs : System.Web.UI.UserControl
    {
        #region For Global Variables
        string _virtualDirectoryPath = System.Configuration.ConfigurationManager.AppSettings["VirtualLocation"];
        tap.dom.tab.EventTabs _eventTab = new dom.tab.EventTabs();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        int _moduleId = 0;
        int _tabId;
        //int _eventId;
        string _eventId;
        string _moduleName = string.Empty;
        string _action = string.Empty;
        #endregion

        #region For Page load
        /// <summary>
        /// Load the Tabs on Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetUrlParameters();
                LoadTabs();

                //SessionService.ModuleId = _moduleId;

            }
        }
        #endregion

        /// <summary>
        /// Get UserId
        /// </summary>
        public int UserId
        {
            get
            {
                return Convert.ToInt32(Session["UserId"]);
            }
        }

        #region For Load Tabs
        /// <summary>
        /// To Load the main Tab and Sub Tabs
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="tabId"></param>
        /// <param name="eventId"></param>
        /// <param name="moduleName"></param>
        public void LoadTabs()
        {
            int companyId = Convert.ToInt32(Session["CompanyId"]);

            string baseURL = tap.dom.adm.CommonOperation.GetSiteRoot();
            string mainTabContent = _eventTab.BuildHTMlMainTabs(_tabId, _eventId, _moduleId, baseURL, companyId);
            divMainTabs.InnerHtml = mainTabContent;
                       
            string subTabContent = string.Empty;

            bool isNewEvent = (_eventId == "0" || _action == "New");

            if (isNewEvent)
            {
                subTabContent = _eventTab.BuildDetailsSubTab(_moduleId, _eventId, baseURL, companyId);
                ulSubTabs.InnerHtml = subTabContent;
                return;
            }

            subTabContent = _eventTab.BuildHTMlSubTabs(_tabId, _eventId, _moduleId, baseURL, companyId, UserId);
            ulSubTabs.InnerHtml = subTabContent;
        }
        #endregion

        #region  To get id  from Url
        /// <summary>
        ///To get ModuleId,Modulename,EventId
        /// </summary>
        public void GetUrlParameters()
        {
            _moduleName = Page.RouteData.Values["Module"].ToString();
            //int.TryParse(_cryptography.Decrypt(Page.RouteData.Values["EventId"].ToString()), out _eventId);
            _eventId = Page.RouteData.Values["EventId"].ToString();
            //int.TryParse(Page.RouteData.Values["CustomSubTabId"].ToString(), out _tabid);
            _moduleId = GetModuleIdFromName(_moduleName);
            _action = ((Page.RouteData.Values["Action"] != null) ? Page.RouteData.Values["Action"].ToString() : string.Empty);
        }
        #endregion

        #region To Get Moduleid  from name
        /// <summary>
        /// To get ModuleId from Modulename
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        public int GetModuleIdFromName(string moduleName)
        {
            int modId = ((moduleName == "Incident" || moduleName == "New-Incident") ?
                (int)tap.dom.hlp.Enumeration.Modules.Incident : (moduleName == "Investigation" || moduleName == "New-Investigation") ?
                (int)tap.dom.hlp.Enumeration.Modules.Investigation : (moduleName == "Audit" || moduleName == "New-Audit") ?
               //(int)tap.dom.hlp.Enumeration.Modules.Audit : (moduleName == "CorrectiveActionPlan" || moduleName == "New-CAP" || moduleName == "CAP" || moduleName == "Corrective-Action-Plan") ?
                (int)tap.dom.hlp.Enumeration.Modules.Audit : (moduleName == "CorrectiveActionPlan" || moduleName == "New-CAP" || moduleName == "CAP" || moduleName == "Action-Plan" || moduleName == "ActionPlan") ?
                (int)tap.dom.hlp.Enumeration.Modules.ActionPlan : 0);
            return modId;
        }
        #endregion
    }
}