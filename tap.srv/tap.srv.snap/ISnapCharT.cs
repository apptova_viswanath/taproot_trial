﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.snap
{
    
    [ServiceContract]
    public interface ISnapCharT
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SnapChartMostSeasonSave(string mode, string eventId, int seasonId, string snapChartXmlGragh, int[] causalfactorsId, string[] causalfactors, bool isComplete, bool isRevision, int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SnapChartSeasonComplete(string eventId, int seasonId, bool isSeasonCompleted, int userId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        int SnapchartIdSelect(string eventId, int seasonId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void CausalFactorUpdate(string eventId, int seasonId, int causalfactorId, string causalfactorName);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        object DefaultShapesByUserIdGet(int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string DefaultShapeSave(int userId, string shapeType, string key, string value);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string DefaultShapeSaveAll(int userId, string shapeType, string style);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string CausalFactorWithRootCauseTreeCheck(string eventID, int seasonId, int[] causalfactorsId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SnapcapsTitlesByEventGet(int userId, string eventId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object SnapcapsCommonTypesGet(int userId, string eventId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SnapChartXmlSave(string eventID, int seasonId, string snapChartData);
    }
}
