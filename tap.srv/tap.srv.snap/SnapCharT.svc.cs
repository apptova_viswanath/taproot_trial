﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.snap
{
    public class SnapCharT : ISnapCharT
    {
        tap.dom.snap.SnapChart snapchart = new tap.dom.snap.SnapChart();
        public string SnapChartMostSeasonSave(string mode, string eventId, int seasonId, string snapChartXmlGragh, int[] causalfactorsId, string[] causalfactors, bool isComplete, bool isRevision, int userId)
        {
            return snapchart.SaveSnapChart(mode, eventId, seasonId, isComplete, snapChartXmlGragh, isRevision, causalfactorsId, causalfactors, userId);
        }

        public void SnapChartSeasonComplete(string eventId, int seasonId, bool isSeasonCompleted, int userId)
        {
            bool isChanged = snapchart.SnapChartSeasonComplete(eventId, seasonId, isSeasonCompleted, userId);
        }

        public int SnapchartIdSelect(string eventId, int seasonId)
        {
            return snapchart.SnapchartIdSelect(eventId, seasonId);
        }

        public void CausalFactorUpdate(string eventId, int seasonId, int causalfactorId, string causalfactorName)
        {
            snapchart.CausalFactorUpdate(eventId, seasonId, causalfactorId, causalfactorName);
        }

        public object DefaultShapesByUserIdGet(int userId)
        {
            return snapchart.DefaultShapesByUserIdGet(userId);
        }

        public string DefaultShapeSave(int userId, string shapeType, string key, string value)
        {
            return snapchart.DefaultShapeSave(userId, shapeType, key, value);
        }

        public string DefaultShapeSaveAll(int userId, string shapeType, string style)
        {
            return snapchart.DefaultShapeSaveAll(userId, shapeType, style);
        }

        public string CausalFactorWithRootCauseTreeCheck(string eventID, int seasonId, int[] causalfactorsId)
        {
            return snapchart.CausalFactorWithRootCauseTreeCheck(eventID, seasonId, causalfactorsId);
        }

        public string SnapcapsTitlesByEventGet(int userId, string eventId)
        {
            return snapchart.SnapcapsTitlesByEventGet(userId, eventId);
        }

        public object SnapcapsCommonTypesGet(int userId, string eventId)
        {
            return snapchart.SnapcapsCommonTypesGet(userId, eventId);
        }

        public string SnapChartXmlSave(string eventID, int seasonId, string snapChartData)
        {
            return snapchart.SnapChartXmlSave(eventID, seasonId, snapChartData);
        }
    }
}
