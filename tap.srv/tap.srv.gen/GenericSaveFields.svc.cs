﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom;
#endregion 

namespace tap.srv.gen
{
    public class GenericSaveFields : IGenericSaveFields
    {
        tap.dom.gen.GenericSave _genericSave = new dom.gen.GenericSave();

        #region "Generic auto Save method"

        /// <summary>
        /// GenericSave
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="entityType"></param>
        /// <param name="entitySet"></param>
        /// <param name="fieldName"></param>
        /// <param name="fieldValue"></param>
        /// <param name="primaryKeyField"></param>
        /// <param name="eventId"></param>
        /// <param name="queryField"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public int GenericSave(string mode, bool isEventData, string entityType, string entitySet,
            string fieldName, string fieldValue, string primaryKeyField, string eventId, string queryField,
                                string dataType, int queryFieldValue, string secondQueryField, string companyID, int userId)
        {
            fieldValue = fieldValue == null ? fieldValue : fieldValue.Trim();
            return _genericSave.GenericSaveFields(mode, isEventData, entityType, entitySet, fieldName, fieldValue, primaryKeyField, eventId,
                                queryField, dataType, queryFieldValue, secondQueryField, companyID, userId);
        }

        #endregion
    }
}
