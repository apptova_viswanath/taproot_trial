﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
#endregion

namespace tap.srv.gen
{
    public class Attachments : IAttachments
    {
        tap.dom.gen.Attachments _attachments = new dom.gen.Attachments();

        #region "Attachment Move"

        /// <summary>
        /// AttachmentsMove
        /// </summary>
        /// <param name="attachmentNodeId"></param>
        /// <param name="attachmentParentNode"></param>
        /// <returns></returns>
        public bool AttachmentsMove(int attachmentNodeId, int attachmentParentNode)
        {
            return _attachments.AttachmentsMove(attachmentNodeId, attachmentParentNode);
        }

        #endregion
    }
}
