﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom;
using System.Data;
#endregion

namespace tap.srv.gen
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CustomReport : ICustomReport
    {
        tap.dom.gen.CustomReport _customReportTemplate = new dom.gen.CustomReport();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region "Generic method to get Custom Report"

        public object GetColumnNames(int companyId, int userId, string eventId, string reportType,int reportId)
        {
            return _customReportTemplate.GetColumnNames(companyId, userId, eventId, reportType, reportId);
        }

        #endregion

        #region "Generic method to Save Report Template"
        public string[] SaveReportTemplate(int reportId, string userID, string companyID, string reportName, string reportDescription, string[] arrLabelElements, string[] arrFieldElements,
                                  string reportTitle, bool isCompanyLogo, bool isCompanyName, bool isReportTitle, bool isDateCreated,
                                string eventID, bool isUserToSystemReport, bool isTempReport, string[] arrEditLabelElements, string reportTitleStyle, string reportType)
        {
            return _customReportTemplate.SaveReportTemplate(reportId,userID, companyID, reportName, reportDescription, arrLabelElements, arrFieldElements,reportTitle,
                                    isCompanyLogo, isCompanyName, isReportTitle, isDateCreated, eventID, isUserToSystemReport,
                                    isTempReport, arrEditLabelElements, reportTitleStyle, reportType);
        }
        #endregion

        #region "Generate Reports"
        public object GenerateReport(string eventID, int reportId)
        {
            return _customReportTemplate.GenerateReport(eventID, reportId);
        }
        #endregion

        #region "Get all Report Names depends on Company Id "
        public string GetReportTitleNames(string companyID, string userID, string eventId)
        {
            return _efSerializer.EFSerialize(_customReportTemplate.GetReportTitleNames(companyID, userID, eventId));
        }
        #endregion

        #region "Delete Image Data -on mousehover click cross/delete icon"
        public bool DeleteImageData(int elementId, int reportId)
        {
            return _customReportTemplate.DeleteImageData(elementId, reportId);
        }
        #endregion

        #region "Delete Report related data when user close the Preview browser"
        public bool DeletePreviewReportData(string eventID, int reportId)
        {
            return _customReportTemplate.DeletePreviewReportData(eventID, reportId);
          
        }
        #endregion

        #region "Fetch Report Data Types-Event's, User's and System"
        public string GetAllReportsData(string page, string rows, string sortIndex, string sortOrder, string companyID, string userID, string userSearch)
        {
            return _customReportTemplate.GetAllReportsData(page, rows,sortIndex, sortOrder, companyID, userID, userSearch);
        }
        #endregion

        #region "Fetch all label names for Edit Report"
        public string GetColumnLabelNames(string userID, string companyID, int reportId)
        {
            return _customReportTemplate.GetColumnLabelNames(userID, companyID, reportId);
           
        }
        #endregion

        #region "TapRooT Elements for Accordian"
        public object GetTapRooTElementsTest()
        {         
            return _customReportTemplate.GetTapRooTElementsTest();
        }
        #endregion


        #region "Get TapRooT Integrate values"
        public object GetTapRooTIntegrateValues(string eventID, int reportId,string integrateType)
        {
            return _customReportTemplate.GetTapRooTIntegrateValues(eventID, reportId, integrateType);
        }

        public object GetAllTapRooTIntegrateValues(string eventID, int reportId, string[] integrateType)
        {
            return _customReportTemplate.GetAllTapRooTIntegrateValues(eventID, reportId, integrateType);
        }
        #endregion

        #region "Get Winter SnapChart Update"
        //Check ALL RCT completed for snapchart or not
        public bool GetWinterSnapChartUpdate(string eventId)
        {
            return _customReportTemplate.GetWinterSnapChartStatus(eventId);
        }
        #endregion


        #region "Get user report templates based on User and company"
        public object GetUserReportTemplates(int companyId, int userId)
        {
            return _customReportTemplate.GetUserReportTemplates(companyId, userId);
           
        }
        #endregion

        #region "Create Event Report"
        public string CreateEventReport(string[] arrTemplateReportIds, int companyId, int userId, string eventID)
        {
            return _customReportTemplate.CreateEventReport(arrTemplateReportIds, companyId, userId, eventID);
           
        }
        #endregion
    }
}
