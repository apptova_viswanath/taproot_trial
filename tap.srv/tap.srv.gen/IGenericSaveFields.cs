﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.gen
{
    [ServiceContract]
    public interface IGenericSaveFields
    {
        #region "Generic auto Save method"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        int GenericSave(string mode, bool isEventData, string entityType, string entitySet, string fieldName, string fieldValue, string primaryKeyField, string eventId,
                        string queryField, string dataType, int queryFieldValue, string secondQueryField, string companyID, int userId);
        #endregion
    }
}
