﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.gen
{
    [ServiceContract]
    public interface IAttachments
    {
        #region "Attachments Move "
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool AttachmentsMove(int attachmentNodeId, int attachmentParentNode);
        #endregion
    }
}
