﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: RootCause.svc.cs
// Date Created  : N/A
// Description   : Service layer class used to call the rct related dom layer functions
//----------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.rct
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RootCause1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RootCause1.svc or RootCause1.svc.cs at the Solution Explorer and start debugging.
    public class RootCause : IRootCause
    {

        //The private variable for the entity data model
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        
        /// <summary>
        /// Get all rct questions based on parent rct id
        /// </summary>
        /// <param name="rootCauseID">based on this it will fetch all rct questions</param>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <returns></returns>
        public object GetRCTQuestions(int rootCauseID, int causalFactorID, int userId)
        {
            tap.dom.rct.RootCauseTreeCF rootCause = new tap.dom.rct.RootCauseTreeCF();
            return _efSerializer.EFSerialize(rootCause.GetRCTQuestions(rootCauseID, causalFactorID, userId));
        }

        public object SaveResponseAndGetQuestions(int rootCauseID, int causalFactorID, int userId, string title, bool? checkedValue, string questionResponse)
        {
            tap.dom.rct.VisualRCT rootCause = new tap.dom.rct.VisualRCT();
            return _efSerializer.EFSerialize(rootCause.SaveResponseAndGetQuestions(rootCauseID, causalFactorID, userId, title, checkedValue, questionResponse));
        }

        /// <summary>
        /// Save rct node status into database
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <param name="rootCauseID">root cause tree node id</param>
        /// <param name="checkedValue">checked value of node</param>
        /// <param name="questionResponse">user response</param>
        public object SaveRCTQuestions(int causalFactorID, int rootCauseID, bool? checkedValue, string questionResponse, bool isVisualRCT, bool allQuestionUnselected)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            return rootCauseTree.SaveRCTQuestions(causalFactorID, rootCauseID, checkedValue, questionResponse, isVisualRCT, allQuestionUnselected);
        }


        /// <summary>
        /// Save analysis added for RCT
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <param name="rootCauseID">root cause tree node id</param>
        /// <param name="analysisComments">analysis comments added for rct</param>
        public void SaveRCTAnalysis(int causalFactorID, int rootCauseID, string analysisComments)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            rootCauseTree.SaveRCTAnalysis(causalFactorID, rootCauseID, analysisComments);
        }


        /// <summary>
        /// Get the generic causes count
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <returns></returns>
        public object GetGenericCauseCount(int causalFactorID)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            return rootCauseTree.GetGenericCauseCount(causalFactorID);
        }

        /// <summary>
        /// Delete Tab related Data
        /// </summary>
        /// <param name="tabIds">Root cause tree node ids related to the tabs to be hidden</param>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        public void DeleteTabData(string tabIds,int causalFactorID)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            rootCauseTree.DeleteTabData(tabIds, causalFactorID);
        }


        /// <summary>
        /// Get all rct questions based on parent rct id
        /// </summary>
        /// <param name="rootCauseID">based on this it will fetch all rct questions</param>
        /// <param name="linkType">either cap link or rct link</param>
        /// <returns></returns>
        public object GetRCTGuidance(int rootCauseID, string linkType, int userId)
        {
            tap.dom.rct.RootCauseTreeCF rootCause = new tap.dom.rct.RootCauseTreeCF();
            return _efSerializer.EFSerialize(rootCause.GetRCTGuidance(rootCauseID, linkType, userId));
        }


        /// <summary>
        /// Get the causal factor for which rct will be designed and the status of user tab selections
        /// </summary>
        /// <param name="causalFactorId">causal factor id for the root cause tree node</param>
        /// <returns></returns>
        public object GetCausalFactorNameAndTabStatus(int causalFactorId, int userId)
        {
            tap.dom.action.CausalFactors causalFactor = new tap.dom.action.CausalFactors();
            return _efSerializer.EFSerialize(causalFactor.GetCausalFactorNameAndTabStatus(causalFactorId, userId));
        }


        /// <summary>
        /// Update status of RCT
        /// </summary>
        /// <param name="causalFactorId">causal factor id for the root cause tree node</param>
        /// <param name="status">status of the root cause tree node</param>
        /// <returns></returns>
        public void UpdateRCTStatus(int causalFactorId, string status)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            rootCauseTree.UpdateRCTStatus(causalFactorId, status);
        }

        //Check ALL RCT completed for snapchart or not
        public bool AllRCTComplete(string eventId)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            return rootCauseTree.AllRCTComplete(eventId);
        }

        //Check ALL RCT completed for snapchart or not
        public bool AnyRCTComplete(string eventId)
        {
            tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
            return rootCauseTree.AnyRCTComplete(eventId);
        }

        /// <summary>
        /// Get all rct questions based on parent rct id
        /// </summary>
        /// <param name="rootCauseID">based on this it will fetch all rct questions</param>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <returns></returns>
        public object GetVisualRCT(int rootCauseID, int causalFactorID,int userId)
        {
            tap.dom.rct.VisualRCT rootCause = new tap.dom.rct.VisualRCT();
            return _efSerializer.EFSerialize(rootCause.GetVisualRCT(rootCauseID, causalFactorID, userId));
        }

        public object GetTreeNodes(int causalFactorID, int userId)
        {
            tap.dom.rct.VisualRCT rootCause = new tap.dom.rct.VisualRCT();
            return _efSerializer.EFSerialize(rootCause.GetTreeNodes(causalFactorID, userId));
        }
       
         public object GetQuestionForRCTShapes(int rootCauseID, int causalFactorID, int userId)
        {
            tap.dom.rct.VisualRCT rootCause = new tap.dom.rct.VisualRCT();
            return _efSerializer.EFSerialize(rootCause.GetQuestionForRCTShapes(rootCauseID, causalFactorID, userId));
        }

         //Check ALL RCT completed for snapchart or not
         public bool CheckIfRCTClassicMode(string userId)
         {
             tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
             return rootCauseTree.CheckIfRCTClassicMode(userId);
         }

         //Check ALL RCT completed for snapchart or not
         public string GetUserLanguage(string userId)
         {
             tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
             return rootCauseTree.GetUserLanguage(userId);
         }


         public bool SaveRCTMode(string userId, bool isRCTClassicMode)
         {
             tap.dom.rct.RootCauseTreeCF rootCauseTree = new tap.dom.rct.RootCauseTreeCF();
             return rootCauseTree.SaveRCTMode(userId, isRCTClassicMode);
         }

        
    }
}
