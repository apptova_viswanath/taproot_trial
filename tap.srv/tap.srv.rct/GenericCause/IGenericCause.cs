﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: IRootCause.cs
// Date Created  : N/A
// Description   : Interface layer class used to call the root cause related Dom layer functions
//----------------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion


namespace tap.srv.rct
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRootCause1" in both code and config file together.
    [ServiceContract]
    [System.Web.Script.Services.ScriptService]
    public interface IGenericCause
    {
      
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveGenericCause(int rootCauseTreeId, bool? question2Response, bool? question3Response, bool? isGeneric, int causalFactorId, string question1Response);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveGenericCauseSatement(string statement, int rootCauseTreeId, int causalFactorId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveProcedure(int rctID, string question1Response);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetGenericCausesStatements(int causalFactorID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void UpdateGenericCause(int genericCauseId, string reState);
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetGenericCauseQuestions(int causalFactorID);

      
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void DeleteGenericCauseByID(int genericCauseID);
        
    }
}
