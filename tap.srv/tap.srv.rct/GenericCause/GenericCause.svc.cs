﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: RootCause.svc.cs
// Date Created  : N/A
// Description   : Service layer class used to call the rct related dom layer functions
//----------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.rct
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RootCause1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RootCause1.svc or RootCause1.svc.cs at the Solution Explorer and start debugging.
    public class GenericCause : IGenericCause
    {

        //The private variable for the entity data model
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        /// <summary>
        /// Save generic cause
       /// </summary>
       /// <param name="restate">re state of the cause</param>
       /// <param name="rootCauseTreeId">root cause tree id</param>
       /// <param name="question2Response">systematic problem response</param>
       /// <param name="question3Response">company problem response</param>
       /// <param name="isGeneric">is generic</param>
        public void SaveGenericCause(int rootCauseTreeId, bool? question2Response,
            bool? question3Response, bool? isGeneric, int causalFactorId, string question1Response)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            genericCause.SaveGenericCause(rootCauseTreeId, question2Response, question3Response, isGeneric, causalFactorId, question1Response);
        }

        /// <summary>
        /// Save generic cause
        /// </summary>
        /// <param name="restate">re state of the cause</param>
        /// <param name="rootCauseTreeId">root cause tree id</param>
        /// <param name="question2Response">systematic problem response</param>
        /// <param name="question3Response">company problem response</param>
        /// <param name="isGeneric">is generic</param>
        public void SaveGenericCauseSatement(string statement, int rootCauseTreeId, int causalFactorId)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            genericCause.SaveGenericCauseSatement(statement, rootCauseTreeId, causalFactorId);
        }

        public void SaveProcedure(int rctID, string question1Response)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            genericCause.SaveProcedure(rctID, question1Response);
        }

        /// <summary>
        /// Get all generic causes questions
        /// </summary>
        /// <param name="causalFactorID">causalFactorID</param>
        /// <returns></returns>
        public object GetGenericCauseQuestions( int causalFactorID)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            return _efSerializer.EFSerialize(genericCause.GetGenericCauseQuestions(causalFactorID));
        }


        /// <summary>
        /// Get all generic causes statements
        /// </summary>
        /// <param name="causalFactorID">causalFactorID</param>
        /// <returns></returns>
        public object GetGenericCausesStatements(int causalFactorID)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            return _efSerializer.EFSerialize(genericCause.GetGenericCausesStatements(causalFactorID));
        }


        /// <summary>
        /// Delete generic cause
        /// </summary>
        /// <param name="rootCauseTreeId">rootCauseTreeId</param>
        public void DeleteGenericCauseByID(int genericCauseID)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            genericCause.DeleteGenericCauseByID(genericCauseID);
        }

        /// <summary>
        /// Delete generic cause
        /// </summary>
        /// <param name="rootCauseTreeId">rootCauseTreeId</param>
        public void UpdateGenericCause(int genericCauseId, string reState)
        {
            tap.dom.rct.RCTGenericCause genericCause = new tap.dom.rct.RCTGenericCause();
            genericCause.UpdateGenericCause(genericCauseId, reState);
        }        

    }
}
