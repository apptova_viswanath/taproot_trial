﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.tab.evt
{
    [ServiceContract]
    public interface IEventTab
    {
        #region "Commented"
        //[OperationContract]
       // [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        // string GetEventTabs(int tabId, int eventId, int moduleId);
        #endregion
    }
}
