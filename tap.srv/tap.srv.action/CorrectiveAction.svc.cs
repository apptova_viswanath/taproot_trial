﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: CorrectiveAction.svc.cs
// Date Created  : N/A
// Description   : Service layer class used to call the corrective action related Dom layer functions
//----------------------------------------------------------------------------------------------------------

# region NameSpace

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.gen;
using tap.dom.usr;

#endregion


namespace tap.srv.action
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CorrectiveAction" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CorrectiveAction.svc or CorrectiveAction.svc.cs at the Solution Explorer and start debugging.
    public class CorrectiveAction : ICorrectiveAction
    {

        //The private variable for the entity data model
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        /// <summary>
        /// Save the corrective action
        /// </summary>
        /// <param name="correctiveActionID"> corrective action id </param>
        /// <param name="eventID"> event id </param>
        /// <param name="isSmarter"> is action is a smarter action plan </param>
        /// <param name="identifier"> identifier of action </param>
        /// <param name="description"> description of action plan </param>
        /// <param name="businessCase"> description of action plan </param>
        /// <param name="reviewNotes"> description of action plan </param>
        /// <param name="temporaryActions"> description of action plan </param>
        /// <param name="mResponsiblePersonID"> responsible person for measuring action plan </param>
        /// <param name="mInitialDueDate"> initial due date for measuring action plan </param>
        /// <param name="mVerificationPlan"> verification plan for measuring action plan </param>
        /// <param name="aResponsiblePersonID"> responsible person for accountable action plan </param>
        /// <param name="aResourceNeeded"> resource needed for accountable action plan </param>
        /// <param name="tInitialDueDate"> initial due date for timely action plan </param>
        /// <param name="eResponsiblePersonID"> responsible person for effectiveness of action plan </param>
        /// <param name="eInitialDueDate"> initial due date to measure effectiveness of action plan </param>
        /// <param name="eValidationPlan"> validation plan effectiveness of action plan </param>
        /// <param name="rootCausesSelected"> root causes selected for action plan </param>
        /// 
           DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            
        tap.dom.action.Tasks tasks = new tap.dom.action.Tasks();
        public string UpdateTasks(int taskID, int taskTypeID, string responsiblePersonID, string description,
                            string notes, int isCompleted, string dueDate, int companyId, int userId, bool isDirectTask)
        {
            return tasks.UpdateTasks(taskID, taskTypeID, responsiblePersonID,description,notes,isCompleted,dueDate, companyId,userId,isDirectTask);
        }
        public void SaveCorrectiveAction(int correctiveActionID, string eventID, Boolean isSmarter, string identifier, string description, string businessCase,
                                         string reviewNotes, string temporaryActions, int? mResponsiblePersonID, string mInitialDueDate,
                                         string mVerificationPlan, int? aResponsiblePersonID, string aResourceNeeded, string tInitialDueDate,
                                        int? eResponsiblePersonID, string eInitialDueDate, string eValidationPlan, string rootCausesSelected, string genericCausesSelected,           
                                         int tasksResponsiblePersonId, int companyId, int userId, string mResponsiblePersonName, string aResponsiblePersonName, 
                                        string eResponsiblePersonName, bool isManualMPersonResponsible, bool isManualEPersonResponsible, bool isManualAPersonResponsible)
        {

            tap.dom.action.CorrectiveActions correctiveAction = new tap.dom.action.CorrectiveActions();
            correctiveAction.SaveCorrectiveAction(correctiveActionID, eventID, isSmarter, identifier, description, businessCase, reviewNotes, temporaryActions, mResponsiblePersonID,
                                                              mInitialDueDate, mVerificationPlan, aResponsiblePersonID, aResourceNeeded, tInitialDueDate, eResponsiblePersonID,
                                                              eInitialDueDate, eValidationPlan, rootCausesSelected, genericCausesSelected,
                                                              tasksResponsiblePersonId, companyId, userId, mResponsiblePersonName, aResponsiblePersonName, eResponsiblePersonName, isManualMPersonResponsible,
                                                              isManualEPersonResponsible, isManualAPersonResponsible);
        }


        /// <summary>
        /// Get all Corrective Actions details by current event id
        /// </summary>
        /// <param name="eventID"> event id </param>
        public object GetCorectiveActionByEventId(string eventID)
        {
            tap.dom.action.CorrectiveActions correctiveAction = new tap.dom.action.CorrectiveActions();
            return _efSerializer.EFSerialize(correctiveAction.GetCAPDetails(eventID));
        }


        /// <summary>
        /// Get Corrective Action details by current event id
        /// </summary>
        /// <param name="correctiveActionID"> corrective action id </param>
        public object GetCorrectiveActionDetails(int correctiveActionID)
        {
            tap.dom.action.CorrectiveActions correctiveAction = new tap.dom.action.CorrectiveActions();
            return _efSerializer.EFSerialize(correctiveAction.GetCorrectiveActionDetailsByID(correctiveActionID));
        }

        
        /// <summary>
        /// Get Corrective Action details by current event id, COMMENTED FOR FURTUER USE
        /// </summary>
        /// <param name="correctiveActionID"> corrective action id </param>
        public object GetDictionaryDetails(int rootCauseID)
        {
            //tap.dom.action.RootCauseTree rootCauseTree = new tap.dom.action.RootCauseTree();
            //return _efSerializer.EFSerialize(rootCauseTree.GetDictionaryDetails(rootCauseID));
            return null;
        }


        /// <summary>
        /// Save the task for new task or edit task
        /// </summary>
        /// <param name="taskID"> TaskId for the task</param>
        /// <param name="correctiveActionID">For which ca the task is getting created/updated</param>
        /// <param name="taskTypeID">which type of task is getting created/updated</param>
        /// <param name="responsiblePersonID">who is responsible for the task getting created/updated</param>
        /// <param name="description">description of the task getting created/updated</param>
        /// <param name="notes">notes for task</param>
        /// <param name="isCompleted"> completed status of task</param>
        /// <param name="dueDate">dueDate set for the task getting created/updated</param>
        public void SaveTask(int taskID, int correctiveActionID, int taskTypeID, string description, string responsiblePersonID, string notes, int isCompleted,
                            string dueDate, int companyId, int userId, bool isDirectTask)
        {

            DateFormat dt = new DateFormat();
            dueDate = dt.fromDatefrmatToDate(dueDate, companyId).ToString();
            tap.dom.action.Tasks tasks = new tap.dom.action.Tasks();
            tasks.SaveTask(taskID, correctiveActionID, taskTypeID, responsiblePersonID, description, notes,
                isCompleted,Convert.ToDateTime( dueDate), companyId, userId, isDirectTask);

        }


        /// <summary>
        /// Get all the task types
        /// </summary>
        /// <returns>task types</returns>
        public object GetAllTaskTypes()
        {
            tap.dom.action.TaskType taskType = new tap.dom.action.TaskType();
            return _efSerializer.EFSerialize(taskType.GetAllTaskTypes());
        }


        /// <summary>
        /// Get task detail by task id
        /// </summary>
        /// <param name="taskID"> task id</param>
        /// <returns> task </returns>
        public object GetTaskDetailsByID(int taskID)
        {
            tap.dom.action.Tasks task = new tap.dom.action.Tasks();
            return _efSerializer.EFSerialize(task.GetTaskDetailsByID(taskID));
        }
        

        /// <summary>
        /// Get causal factor details by event id
        /// </summary>
        /// <param name="eventID"> event id</param>
        /// <returns>causal factor</returns>
        public object GetCausalFactorDetails(string eventID, int userId)
        {
            tap.dom.action.CausalFactors causalFactor = new tap.dom.action.CausalFactors();
            return _efSerializer.EFSerialize(causalFactor.GetCausalFactorDetails(eventID, userId));
        }



        public string GetCorrectiveActionInfo(int taskId)
        {
            tap.dom.action.CorrectiveActions correctiveAction = new tap.dom.action.CorrectiveActions();
            return correctiveAction.GetCorrectiveActionInfo(taskId);
        }
    }

}
