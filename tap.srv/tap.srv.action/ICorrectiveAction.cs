﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: ICorrectiveAction.cs
// Date Created  : N/A
// Description   : Interface layer class used to call the corrective action related Dom layer functions
//----------------------------------------------------------------------------------------------------------

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.action
{
    [ServiceContract]
    public interface ICorrectiveAction
    {

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string UpdateTasks(int taskID, int taskTypeID, string responsiblePersonID, string description,
                            string notes, int isCompleted, string dueDate, int companyId, int userId, bool isDirectTask);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveCorrectiveAction(int correctiveActionID, string eventID, bool isSmarter, string identifier, string description, 
                                  string businessCase, string reviewNotes, string temporaryActions, int? mResponsiblePersonID,
                                  string mInitialDueDate, 
                                  string mVerificationPlan, int? aResponsiblePersonID, string aResourceNeeded, 
                                  string tInitialDueDate, 
                                  int? eResponsiblePersonID, string eInitialDueDate,
                                  string eValidationPlan, string rootCausesSelected, string genericCausesSelected,
                                    int tasksResponsiblePersonId, int companyId, int userId,
                                    string mResponsiblePersonName, string aResponsiblePersonName, 
                                string eResponsiblePersonName, bool isManualMPersonResponsible, bool isManualEPersonResponsible, bool isManualAPersonResponsible);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveTask(int taskID, int correctiveActionID, int taskTypeID, string description, string responsiblePersonID, string notes, int isCompleted, string dueDate, int companyId, int userId, bool isDirectTask);
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetAllTaskTypes();
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetCorectiveActionByEventId(string eventID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetCorrectiveActionDetails(int correctiveActionID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetTaskDetailsByID(int taskID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetCausalFactorDetails(string eventID, int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]       
        object GetDictionaryDetails(int rootCauseID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]       
        string GetCorrectiveActionInfo(int taskId);
               
    }
}
