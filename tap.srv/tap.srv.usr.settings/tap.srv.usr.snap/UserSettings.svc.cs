﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.usr.snap
{
    
    public class UserSettings : IUserSettings
    {
        tap.dom.usr.settings.UserSettings usersettings = new dom.usr.settings.UserSettings();

        #region SnapCharT autosave duration Save

        /// <summary>
        /// Save the Snapchart auto save duration selected by the user.
        /// </summary>
        /// <param name="autosaveDuration"></param>
        public void SnapChartAutoSaveDurationSet(int userId, int autosaveDuration)
        {
            usersettings.SnapChartAutoSaveDurationSet(userId, autosaveDuration);
        }

        #endregion


        #region Get the latest SnapCharT autosave duration

        /// <summary>
        /// Get the latest SnapCharT autosave duration
        /// </summary>
        /// <returns></returns>
        public int? SnapChartAutoSaveDurationGet(int userId)
        {
            return usersettings.SnapChartAutoSaveDurationGet(userId);
        }

        #endregion

    }
}
