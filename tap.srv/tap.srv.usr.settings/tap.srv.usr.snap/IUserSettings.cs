﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.usr.snap
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserSettings" in both code and config file together.
    [ServiceContract]
    public interface IUserSettings
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SnapChartAutoSaveDurationSet(int userId, int autosaveDuration);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        int? SnapChartAutoSaveDurationGet(int userId);
    }
}
