﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;

namespace tap.srv.adm.grp
{
    [ServiceContract]
    public interface IManageUsers
    {
        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //string GetUsersDetails();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetUsersDetails(int page, int rows, string sortIndex, string sortOrder, int companyId, bool isGlobalAdmin, string searchFilter, bool isDivision);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetEventAccessUsers(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyId);
    }
}