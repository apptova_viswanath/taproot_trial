//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//using System.Data.Services;
//using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using tap.dom.adm;

namespace tap.srv.adm.grp
{
    public class UserDetails : IUserDetails
    {
        private tap.dom.adm.UserDetails userDetails = new dom.adm.UserDetails();

        public string AddUser(string userID, string userName, string password, string companyId, string firstName, string lastName, string phoneNumber, string emailId, bool active, bool admin, bool viewEvents, bool editEvents, string subscriptionStart, string subscriptionEnd, int LanguageID)
        {
            return userDetails.AddUser(userID, userName, password, companyId, firstName, lastName, phoneNumber, emailId, active, admin, viewEvents, editEvents, subscriptionStart, subscriptionEnd, LanguageID);
        }

        public bool IsUserExists(string userName)
        {
            return userDetails.IsUserExists(userName);
        }

        public string EnableOrDisableUser(string userID, bool isActivate)
        {
            return userDetails.EnableOrDisableUser(userID,  isActivate);
        }

        public string DeleteUser(string userID)
        {
            return userDetails.DeleteUser(userID);
        }

        public string AddEventAccessUsers(int companyId, string userIds, bool isEdit, bool isView)
        {
            return userDetails.AddEventAccessUsers(companyId, userIds, isEdit, isView);
        }
    }
}
