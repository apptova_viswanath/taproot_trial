﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.adm.grp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Password" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Password.svc or Password.svc.cs at the Solution Explorer and start debugging.
    public class Password : IPassword
    {
        tap.dom.usr.Password _passwords = new tap.dom.usr.Password();

        public string GetPasswordPolicyData(string compId)
        {
            return _passwords.PasswordPolicy(compId);
        }
        public string PasswordExpiresInDays(int userId)
        {
            return _passwords.PasswordExpiresInDays(userId);
        }

    }
}
