//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//using System.Data.Services;
//using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using tap.dom.adm;

namespace tap.srv.adm.grp
{
    public class ManageUsers : IManageUsers
    {
        private tap.dom.adm.ManageUsers manageUsers = new dom.adm.ManageUsers();

        public string GetUsersDetails(int page, int rows, string sortIndex, string sortOrder, int companyId, bool isGlobalAdmin,string searchFilter, bool isDivision)
        {
            return manageUsers.GetUsersDetails(page, rows, sortIndex, sortOrder, companyId, isGlobalAdmin, searchFilter, isDivision);
          
        }

        public string GetEventAccessUsers(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyId)
        {
            return manageUsers.GetEventAccessUsers(page, rows, SearchFilter, IgnoreCase, sortIndex, sortOrder, companyId);
        }
    }
}
