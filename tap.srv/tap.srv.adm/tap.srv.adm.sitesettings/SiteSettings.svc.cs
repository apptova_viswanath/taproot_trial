﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;
#endregion

namespace tap.srv.adm.sitesettings
{
    public class SiteSettings : ISiteSettings
    {
        tap.dom.adm.SiteSettings _siteSettings = new tap.dom.adm.SiteSettings();

        #region To Get time format

        /// <summary>
        /// GetTimeFormat()
        /// Description - invoke to get the time format
        /// </summary>
        /// <param name="iCompanyId"></param>
        /// <returns></returns>
        public bool GetTimeFormat()
        {
            return _siteSettings.GetTimeFormat();
        }

        #endregion

        #region to Get Date format

        /// <summary>
        /// GetDateFormat()
        /// Description-Get Date Format
        /// </summary>
        /// <param name="iCompanyId"></param>
        /// <returns></returns>
        public string GetDateFormat()
        {
            return _siteSettings.GetDateFormat();
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetSiteSettingData(int companyId)
        {

            return _siteSettings.GetSiteSettingData(companyId);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateFormat"></param>
        /// <param name="isTwelveHourFormat"></param>
        public void SaveSiteSettingData(string dateFormat,bool isTwelveHourFormat, string applicationTimeOut, int companyId)
        {
            _siteSettings.SaveSiteSettingData(dateFormat, isTwelveHourFormat, applicationTimeOut, companyId,0,0);
        }

    }
}
