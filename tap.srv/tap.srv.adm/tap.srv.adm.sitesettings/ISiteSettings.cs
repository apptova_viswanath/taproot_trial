﻿#region "Namespaces"
using System.ServiceModel;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.sitesettings
{
    [ServiceContract]
    public interface ISiteSettings
    {
        #region To Get time format
        //To get the time format
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        bool GetTimeFormat();
        #endregion

        #region to Get Date format
        //To get the Date format
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetDateFormat();
        #endregion
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetSiteSettingData(int companyId);


        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveSiteSettingData(string dateFormat, bool isTwelveHourFormat, string applicationTimeOut, int companyId);

    }
}
