//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
//using System.Data.Services;
//using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using tap.dom.adm;

namespace tap.srv.adm.sys
{
    public class SystemNotificaitons : ISystemNotifications
    {
        private tap.dom.adm.SystemNotifications sysNotify = new dom.adm.SystemNotifications();

        public bool AddSysNotification(string title, string fromDate, string toDate, string message, int sysId, bool active, int companyId)
        {
            return sysNotify.AddSysNotification(title, fromDate, toDate, message, sysId, active, companyId);
        }

        public bool DeleteSysNotification(int sysId, int companyId)
        {
            return sysNotify.DeleteSysNotification(sysId, companyId);
        }

        public string GetSystemNotifications(int page, int rows, string sortIndex, string sortOrder, string companyID)
        {
            return sysNotify.GetSystemNotifications(page, rows, sortIndex, sortOrder, companyID);
        }

        public string GetCurrentNotifications(int companyId)
        {
            return sysNotify.GetCurrentNotifications(companyId);
        }

        public string GetNotificationByID(int sysId, int companyId)
        {
            return sysNotify.GetNotificationByID(sysId, companyId);
        }
    }
}
