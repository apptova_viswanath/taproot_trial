﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.adm.grid
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomGrid" in both code and config file together.
    [ServiceContract]
    public interface ICustomGrid
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveGridColumnSortOrder(int userID, int gridID, string gridName, string pageName, string[] sortOrder);

 
    }
}
