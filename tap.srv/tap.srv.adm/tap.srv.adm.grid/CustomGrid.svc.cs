﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.adm;

namespace tap.srv.adm.grid
{
    public class CustomGrid : ICustomGrid
    {
        private tap.dom.gen.Common customGrid =new dom.gen.Common();

        public string SaveGridColumnSortOrder(int userID, int gridID, string gridName, string pageName, string[] sortOrder)
        {
            return customGrid.SaveGridColumnSortOrder(userID, gridID, gridName, pageName, sortOrder);
        }

       
    }
}
