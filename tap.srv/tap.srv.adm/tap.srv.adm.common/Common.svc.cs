﻿
#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
#endregion

namespace tap.srv.adm.common
{
    public class Common : ICommon
    {
        tap.dom.adm.CommonOperation _common = new dom.adm.CommonOperation();
        tap.dom.hlp.ActivityLog _activityLog = new dom.hlp.ActivityLog();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region "Get Field Details"

        /// <summary>
        /// GetFieldDetailsByTabID
        /// </summary>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public string GetFieldDetailsByTabID(int tabId)
        {
            return _efSerializer.EFSerialize(_common.GetFieldDetailsByTabID(tabId));
        }

        #endregion

        #region "Insert Activity Logging in "UserTransactionsDaily"

        /// <summary>
        /// SaveActivityLog
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <param name="categoryId"></param>
        /// <param name="logDetails"></param>
        /// <returns></returns>
        public bool SaveActivityLog(string companyID, int eventId, string userID, int categoryId, string logDetails)
        {
            return _activityLog.SaveActivityLog(companyID, eventId, userID, categoryId, logDetails);
        }

        #endregion
    }
}
