﻿
#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.common
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICommon" in both code and config file together.
    [ServiceContract]
    public interface ICommon
    {
        #region "Get Field Details"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetFieldDetailsByTabID(int _tabid);
        #endregion

        #region "Insert Activity Logging in "UserTransactionsDaily"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool SaveActivityLog(string companyID, int eventId, string userID, int categoryId, string logDetails);      
        #endregion
    }

   
}
