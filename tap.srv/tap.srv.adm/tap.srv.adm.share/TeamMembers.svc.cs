﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.adm.share
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TeamMembers" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TeamMembers.svc or TeamMembers.svc.cs at the Solution Explorer and start debugging.
    public class TeamMembers : ITeamMembers
    {
        tap.dom.adm.TeamMembers _tmList = new tap.dom.adm.TeamMembers();
        private tap.dom.adm.ManageUsers manageUsers = new dom.adm.ManageUsers();

        #region "To get Admin Share Lists"

        /// <summary>
        /// TeamMembersList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortOrder"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetTeamMemberList(string page, string rows, string sortIndex, string sortOrder, int companyId, string eventId)
        {
            return _tmList.GetTeamMemberList(page, rows, sortIndex, sortOrder, companyId, eventId);
        }

        #endregion

        public string DeleteTeamMember(int tmID)
        {
            return _tmList.DeleteTeamMember(tmID);
        }

        public string UpdateTeamMember(int tmID, bool value, string col)
        {
            return _tmList.UpdateTeamMember(tmID, value, col);
        }

        public string GetTeamUserList(string page, string rows, string sortIndex, string sortOrder, string basicSearch, int companyId)
        {
            return _tmList.GetTeamUserList(page, rows, sortIndex, sortOrder, basicSearch, companyId);
        }

        public string GetTeamUsersDetails(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID, string eventId, bool isShareTab)
        {
            return manageUsers.GetTeamUsersDetails(page, rows, SearchFilter, IgnoreCase, sortIndex, sortOrder, companyID, eventId, isShareTab);
        }

        public string AddTeamMember(int userID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID)
        {
            return _tmList.AddTeamMember(userID, isEdit, isView, isDisplayOnReport, eventID);
        }

        public string AddTeamMemberManually(string fullName, string eventId)
        {
            return _tmList.AddTeamMemberManually(fullName, eventId);
        }

        public string[] GetTeamMemberAccess(int userId, string eventId, int companyId)
        {
            return _tmList.GetTeamMemberAccess(userId, eventId, companyId);
        }
        public bool isUserExistTM(string name, string eventId, int companyId)
        {
            return _tmList.isUserExistTM(name, eventId, companyId);
        }

        public string SaveTeamMember(int userID, bool value, string col, string eventID)
        {
            return _tmList.SaveTeamMember(userID, value, col, eventID);
        }
        public string UpdateMember(int tmID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID)
        {
            return _tmList.UpdateTeamMember(tmID, isEdit, isView, isDisplayOnReport, eventID);
        }
    }
}
