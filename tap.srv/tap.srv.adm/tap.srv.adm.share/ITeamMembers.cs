﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.adm.share
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITeamMembers" in both code and config file together.
    [ServiceContract]
    public interface ITeamMembers
    {
        #region "To get Team Member Lists"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTeamMemberList(string page, string rows, string sortIndex, string sortOrder, int companyId, string eventId);
        #endregion

        #region "To Delete Event"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string DeleteTeamMember(int tmID);
        #endregion

        #region "To Update TeamMember"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string UpdateTeamMember(int tmID, bool value, string col);
        #endregion

        #region "To get Team User Lists"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTeamUserList(string page, string rows, string sortIndex, string sortOrder, string basicSearch, int companyId);
        #endregion

        #region "To get Team User Lists"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTeamUsersDetails(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID, string eventId, bool isShareTab);
        #endregion

        #region "To Add Team Member"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string AddTeamMember(int userID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID);
        #endregion

        #region "To Add Team Member manually"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string AddTeamMemberManually(string fullName, string eventId);
        #endregion

        #region "To Access Of Team Member"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string[] GetTeamMemberAccess(int userId, string eventId, int companyId);
        #endregion

        #region "To Check User"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool isUserExistTM(string name, string eventId, int companyId);
        #endregion

        #region "To Save/Update TeamMember"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveTeamMember(int userID, bool value, string col, string eventID);
        #endregion

        #region "To Update TeamMember"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string UpdateMember(int tmID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID);
        #endregion
    }
}
