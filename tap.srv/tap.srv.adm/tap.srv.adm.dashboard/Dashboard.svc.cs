﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom;

namespace tap.srv.adm.dashboard
{    
    public class Dashboard : IDashboard
    {
        tap.dom.usr.Dashboard dashboard = new dom.usr.Dashboard();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();
        public string EventBreakdown(int companyId, string fDate, string tDate)
        {
            return dashboard.EventBreakdown(companyId, fDate, tDate).ToString();
        }

        public string MostCommonRootCauses(int companyId, string fDate, string tDate)
        {
            return _efSerializer.EFSerialize(dashboard.MostCommonRootCauses(companyId, fDate, tDate));
        }

        public string BasicCauseCategoryDistribution(int companyId, string fDate, string tDate)
        {
            return _efSerializer.EFSerialize(dashboard.BasicCauseCategoryDistribution(companyId, fDate, tDate));
        }
    }
}
