﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;
#endregion

namespace tap.srv.adm.EmailSettings
{
    public class EmailSettings : IEmailSettings
    {

        tap.dom.adm.EmailSettings _emailSettings = new tap.dom.adm.EmailSettings();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

       
        #region For insert  tab in service Layer
        /// <summary>
        /// For insert  tab in service Layer
        /// </summary>
        /// <param name="_tabid"></param>
        /// <param name="_companyid"></param>
        /// <param name="_moduleid"></param>
        /// <param name="_tabname"></param>
        /// <param name="_isuniversal"></param>
        /// <param name="_issystem"></param>
        /// <param name="_sortorder"></param>
        /// <param name="_active"></param>
        public string SaveEmailSettings(string EmailFrom, string EmailPassword, string HostAddress, string PortNumber)
        {

            return _emailSettings.SaveEmailSettings( EmailFrom,  EmailPassword,  HostAddress,  PortNumber);
        }

        #endregion

        public string TestEmailSettings(string emailTo)
        {

            return _emailSettings.TestEmailSettings(emailTo);
        }


      
        #region To Get The Tab  By Tabid
        /// <summary>
        /// Invoked To Get The  Tab  bY Tabid
        /// </summary>
        /// <param name="_tabid"></param>
        /// <returns></returns>
        public object GetEmailSettings()
        {
            return _efSerializer.EFSerialize(_emailSettings.GetEmailSettings());
        }
        #endregion

    }
}
