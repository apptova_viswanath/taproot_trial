﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;
#endregion

namespace tap.srv.adm.mod
{
    public class Modules : IModules
    {
        tap.dom.adm.Module modules = new dom.adm.Module();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region "Get the Module List"

        /// <summary>
        /// Modulelist
        /// </summary>
        /// <returns></returns>
        public string Modulelist()
        {
            return _efSerializer.EFSerialize(modules.GetModuleList());
        }

        #endregion
    }
}
