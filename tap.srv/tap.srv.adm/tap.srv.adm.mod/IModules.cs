﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.mod
{
    [ServiceContract]
    public interface IModules
    {
        #region commented
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string Modulelist();
        #endregion
    }
}
