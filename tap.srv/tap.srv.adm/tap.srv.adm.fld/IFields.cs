﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion 

namespace tap.srv.adm.fld
{
    [ServiceContract]
    public interface IFields
    {
        # region "To get DataList"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string DataListSelect(int companyId);
        # endregion

        # region "To Save the Fields data"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool SaveField(int fieldId, int tabId, string displayName, int sortOrder, string dataType, string controlType, int maxLength, bool isRequired, int listId, bool isActive, string description, int userId);
        # endregion

        #region To Get Fieldlist By Tabid
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string FieldListByTabID(int tabId);
        #endregion

        #region To Get Field By ID
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetFieldByID(int fieldId);
        #endregion

        #region to Get the Fieldlist by Field Id
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetFieldByFieldID(int fieldId);
        #endregion

        #region to Get the Fieldlist by string
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetFieldListByTabID(int tabId);
        #endregion

        #region to Update the Field sort order
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool FieldSortOrderUpdate(string[] fieldId, string[] sortOrder);
        #endregion

        #region To check the Field can be  deleted
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        bool DeleteCustomField(int fieldId, int userId);
        #endregion
    }

}
