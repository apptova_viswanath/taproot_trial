﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
#endregion

namespace tap.srv.adm.fld
{
    public class FieldValues : IFieldValues
    {
        tap.dom.adm.FieldValues _fieldValue = new dom.adm.FieldValues();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region To Insert/Update FieldValues 
       
        /// <summary>
        /// SaveFieldValues
        /// </summary>
        /// <param name="fieldValueId"></param>
        /// <param name="eventId"></param>
        /// <param name="fieldId"></param>
        /// <param name="fieldValue"></param>
        /// <returns></returns>
        public bool SaveFieldValues(int fieldValueId,int eventId, int fieldId, string fieldValue)
        {
            return _fieldValue.SaveFieldValues(fieldValueId, eventId, fieldId, fieldValue);
        }

        #endregion

        #region "To get the Custom field values based on field id"

        /// <summary>
        /// CustomFieldValuesSelect
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public string CustomFieldValuesSelect(int fieldId, int eventId)
        {
            return _efSerializer.EFSerialize(_fieldValue.CustomFieldValuesSelect(fieldId, eventId));
        }

        #endregion
       
    }
}
