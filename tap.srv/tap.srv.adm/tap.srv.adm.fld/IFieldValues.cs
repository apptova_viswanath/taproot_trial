﻿#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.fld
{
    [ServiceContract]
    public interface IFieldValues
    {
        #region To Insert/Update FieldValues
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool SaveFieldValues(int fieldValueId, int eventId, int fieldId, string fieldValue);
        #endregion

        #region "To get the Custom field values based on field id"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string CustomFieldValuesSelect(int fieldId, int eventId);       

        #endregion
    }
}
