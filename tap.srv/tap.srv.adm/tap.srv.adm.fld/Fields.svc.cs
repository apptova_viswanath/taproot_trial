﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
#endregion

namespace tap.srv.adm.fld
{
    public class Fields : IFields
    {
        tap.dom.adm.Field _field = new dom.adm.Field();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        # region "To get DataList"

        /// <summary>
        /// DataListSelect
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string DataListSelect(int companyId)
        {
            return _efSerializer.EFSerialize(_field.DataListSelect(companyId));
        }

        # endregion

        # region "To Insert fields"

        /// <summary>
        /// SaveField
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="tabId"></param>
        /// <param name="displayName"></param>
        /// <param name="sortOrder"></param>
        /// <param name="dataType"></param>
        /// <param name="controlType"></param>
        /// <param name="maxLength"></param>
        /// <param name="isRequired"></param>
        /// <param name="listId"></param>
        /// <param name="isActive"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public bool SaveField(int fieldId, int tabId, string displayName, int sortOrder, string dataType, string controlType,
                              int maxLength, bool isRequired, int listId, bool isActive, string description, int userId)
        {
            return _field.SaveField(fieldId, tabId, displayName, sortOrder, dataType, controlType, maxLength, isRequired, listId, isActive, description, userId);
        }

        # endregion

        #region To Get Fieldlist By Tabid
        /// <summary>
        /// FieldListByTabID
        /// </summary>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public string FieldListByTabID(int tabId)
        {
            return _efSerializer.EFSerialize(_field.FieldListByTabID(tabId));
        }

        #endregion

        #region  to Get The Field By ID

        /// <summary>
        /// GetFieldByID
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public string GetFieldByID(int fieldId)
        {
            return _efSerializer.EFSerialize(_field.GetFieldByID(fieldId));
        }

        #endregion

        #region to Get the Field By FieldID 

        /// <summary>
        /// GetFieldByFieldID
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public string GetFieldByFieldID(int fieldId)
        {
            return _efSerializer.EFSerialize(_field.GetFieldByFieldID(fieldId));
        }

        #endregion

        #region to Get the Fieldlist by string

        /// <summary>
        /// GetFieldListByTabID
        /// </summary>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public string GetFieldListByTabID(int tabId)
        {
            return _efSerializer.EFSerialize(_field.GetFieldListByTabID(tabId));
        }

        #endregion

        #region to Update the Field sort order

        /// <summary>
        /// FieldSortOrderUpdate
        /// </summary>
        /// <param name="fieldId"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public bool FieldSortOrderUpdate(string[] fieldId, string[] sortOrder)
        {
            return _field.FieldSortOrderUpdate(fieldId, sortOrder);
        }

        #endregion

        #region For Checking deleting the field

        /// <summary>
        /// DeleteCustomField
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public bool DeleteCustomField(int fieldId, int userId)
        {
            return _field.DeleteCustomField(fieldId, userId);
        }

        #endregion
    }
}
