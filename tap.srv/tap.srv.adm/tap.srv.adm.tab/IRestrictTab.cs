﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.adm.tab
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRestrictTab" in both code and config file together.
    [ServiceContract]
    public interface IRestrictTab
    {
        #region "To get Restrict Users List"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string RestrictUsersList(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID, int tabId);
        #endregion

        #region "To save Restricted User"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string UpdateRestrictedUser(int userId, int tabId, bool isRestricted);
        #endregion
    }
}
