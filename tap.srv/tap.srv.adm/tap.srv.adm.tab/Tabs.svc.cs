﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;
#endregion

namespace tap.srv.adm.tab
{
    public class Tabs : ITabs
    {
        tap.dom.adm.Tab _tabDetails = new dom.adm.Tab();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region to Get The the Tablist in Json format in service
        public string GetTabList(int moduleId, int companyId)
        {
            return _efSerializer.EFSerialize(_tabDetails.GetTabList(moduleId, companyId));
        }
        #endregion

        #region For insert  tab in service Layer
        /// <summary>
        /// For insert  tab in service Layer
        /// </summary>
        /// <param name="_tabid"></param>
        /// <param name="_companyid"></param>
        /// <param name="_moduleid"></param>
        /// <param name="_tabname"></param>
        /// <param name="_isuniversal"></param>
        /// <param name="_issystem"></param>
        /// <param name="_sortorder"></param>
        /// <param name="_active"></param>
        public string[] SaveTab(int tabId, string companyID, string tabName, string currentModuleName, bool isUniversal, bool isSystem, int sortOrder,
                                bool active, string selectedModuleName, string baseURL, bool restrictedAccess, int userId)
        {

            return _tabDetails.SaveTab(tabId, companyID, tabName, currentModuleName, isUniversal, isSystem, sortOrder, active, selectedModuleName, baseURL, restrictedAccess, userId);
        }

        #endregion

        #region To Delete the Tab By Id
        /// <summary>
        /// Invoked To delete The Tab By TabId
        /// </summary>
        /// <param name="_tabid"></param>
        public void DeleteTabByID(int tabId, int userId)
        {
            _tabDetails.DeleteTabByID(tabId, userId);
        }
        #endregion

        #region To Get The Tab  By Tabid
        /// <summary>
        /// Invoked To Get The  Tab  bY Tabid
        /// </summary>
        /// <param name="_tabid"></param>
        /// <returns></returns>
        public string GetTabByID(int tabId)
        {
            return _efSerializer.EFSerialize(_tabDetails.GetTabInfoById(tabId));
        }
        /// <summary>
        /// Invoked To Get The  Tab  bY Tabid
        /// </summary>
        /// <param name="_tabid"></param>
        /// <returns></returns>
        public string GetTabDetailsByID(int tabId, int moduleId)
        {
            return _efSerializer.EFSerialize(_tabDetails.GetTabDetailsByID(tabId, moduleId));
        }
        #endregion

        #region To get the Tab By Tabname
        public string GetTabByName(string tabName)
        {
            return _efSerializer.EFSerialize(_tabDetails.GetTabByName(tabName));
        }
        #endregion

        # region "To update Subtabs sort order"
        public bool SubTabsSortUpdate(string[] tabId, string[] subTabOrder)
        {
            return _tabDetails.SubTabsSortUpdate(tabId, subTabOrder);
        }
        #endregion

        #region To Get CustomTablist
        public string GetCustomTabs(int companyId)
        {
            return _efSerializer.EFSerialize(_tabDetails.GetCustomTabs(companyId));
        }
        #endregion

        #region For insert tabs order in service Layer
        /// <summary>
        /// For insert  tabs order in service Layer
        /// </summary>
        /// <param name="_moduleid"></param>
        /// <param name="_tabsorder"></param>
        public void SaveTabsOrder(int moduleID, string tabsOrder)
        {
            if (moduleID > 0 && !string.IsNullOrEmpty(tabsOrder))
            {
                _tabDetails.SaveTabModuleOrder(moduleID, tabsOrder);

            }
        }

        #endregion
    }
}
