﻿#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.tab
{
    [ServiceContract]
    public interface ITabs
    {
        #region to Get The the Tablist in Json format in service
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTabList(int moduleId, int companyId);
        #endregion

        #region For insert  tab in service Layer
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string[] SaveTab(int tabId, string companyID, string tabName, string currentModuleName, bool isUniversal, bool isSystem, int sortOrder,
                                bool active, string selectedModuleName, string baseURL, bool restrictedAccess, int userId);
        #endregion

        #region For insert  tabs order in service Layer
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void SaveTabsOrder(int moduleID, string tabsOrder);
        #endregion

        #region To Delete the Tab By Id
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        void DeleteTabByID(int tabId, int userId);
        #endregion

        #region To Get The Tab  By Tabid
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetTabByID(int tabId);
        #endregion

        #region To Get The Tab Details By Tabid
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetTabDetailsByID(int tabId, int moduleId);
        #endregion

        #region To get the Tab By Tabname
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetTabByName(string tabName);
        #endregion

        #region "To update Subtabs sort order"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool SubTabsSortUpdate(string[] tabId, string[] subTabOrder);
        #endregion

        #region To Get CustomTablist
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetCustomTabs(int companyId);
        #endregion
    }
}
