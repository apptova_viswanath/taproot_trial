﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace tap.srv.adm.tab
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RestrictTab" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RestrictTab.svc or RestrictTab.svc.cs at the Solution Explorer and start debugging.
    public class RestrictTab : IRestrictTab
    {
        tap.dom.tab.RestrictTab _restrictTab = new tap.dom.tab.RestrictTab();

        public string RestrictUsersList(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID, int tabId)
        {
            return _restrictTab.RestrictUsersList(page, rows, SearchFilter, IgnoreCase, sortIndex, sortOrder, companyID, tabId);
        }

        public string UpdateRestrictedUser(int userId, int tabId, bool isRestricted)
        {
            return _restrictTab.UpdateRestrictedUser(userId, tabId, isRestricted);
        }
    }
}
