﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.adm;
#endregion 

namespace tap.srv.adm.atch
{
    public class AdminAttachments : IAdminAttachments
    {
        tap.dom.adm.Attachment _attachmentValues = new dom.adm.Attachment();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region Method To display the Jstreeview

        /// <summary>
        /// Invoked To Get Data For Jstreeview
        /// </summary>
        /// <param name="hirLevelID"></param>
        /// <returns></returns>
        public string BuildAttachmentJSTreeView(int hirLevelId, int companyID)
        {
            return _attachmentValues.BuildAttachmentJSTreeView(hirLevelId, companyID);
        }

        #endregion

        #region "To insert/update Items in tree view for Attachment setup folder"

        /// <summary>
        /// To insert/update Items in tree view for Attachment setup folder
        /// </summary>
        /// <returns></returns>
        public string SaveAttachmentFoldersValues(int attachmentFolders, int hirLevelId, int attachmentFolderParent, string hirLevelName, string brief, string description,
                                      bool active, string displayOrder, string companyId, int userId)
        {
            return _attachmentValues.SaveAttachmentFoldersValues(attachmentFolders, attachmentFolderParent, hirLevelName, active, displayOrder, companyId, userId);
        }

        #endregion

        #region "To delete multiple nodes in Attachment Setup Tree view"
        /// <summary>
        /// To Delete Items in tree view for Attachment setup folder
        /// </summary>
        /// <returns>bool</returns>

        public bool AttachmentFolderDelete(int nodeId, int userId)
        {
            return _attachmentValues.AttachmentFolderDelete(nodeId, userId);
        }

        #endregion

        #region "Update one record in Attachment Setupe JSTreeview (Inline Editing)"

        /// <summary>
        /// AttachmentFoldersValueUpdate
        /// </summary>
        /// <param name="hirLevelId"></param>
        /// <param name="attachmentFolder"></param>
        /// <param name="attachmentFolderParent"></param>
        /// <param name="attachmentFolderTitle"></param>
        /// <param name="parentNodeId"></param>
        /// <returns></returns>
        public string AttachmentFoldersValueUpdate(int hirLevelId, int attachmentFolders, int attachmentFolderParent, string attachmentFolderTitle, int parentNodeId, int companyId)
        {
            return _attachmentValues.AttachmentFoldersValueUpdate(hirLevelId, attachmentFolders, attachmentFolderParent, attachmentFolderTitle, parentNodeId, companyId);
        }

        #endregion

        #region "To update the folder structure (drag and drop of folders)"
        /// <summary>
        /// MoveFoldersUpdate
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="folderId"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public string MoveFoldersUpdate(int parent, int folderId, string folderName, int companyId, int userId)
        {
            return _attachmentValues.MoveFoldersUpdate(parent, folderId, folderName, companyId, userId);
        }


        public bool CheckIfAttachmentsFileExists(int folderId)
        {
            return _attachmentValues.CheckIfAttachmentsFileExists(folderId);
        }

        #endregion
    }
}
