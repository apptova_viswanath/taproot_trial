﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.atch
{
    [ServiceContract]
    public interface IAdminAttachments
    {
   
        #region "Get Attachment Setup Data For Jstreeview"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string BuildAttachmentJSTreeView(int hirLevelId, int companyID);
        #endregion

        #region "For Insert/Update operation of hirnode Table-for Attachment Setup items for Tree view"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveAttachmentFoldersValues(int hirNode, int hirLevelId, int hirNodeParent, string hirLevelName, string brief, string description,
            bool active, string displayOrder, string companyId, int userId);
        #endregion

        #region "Delete multiple nodes from Attachment Setup Tree view"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool AttachmentFolderDelete(int nodeId, int userId);
        #endregion

        #region "Update one record in Attachment Setupe JSTreeview (Inline Editing)"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string AttachmentFoldersValueUpdate(int hirLevelId, int hirNode, int hirNodeParent, string hirNodeTitle, int parentNodeId, int companyId);
        #endregion

        #region "To update the folder structure (drag and drop of folders)"
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string MoveFoldersUpdate(int parent, int folderId, string folderName, int companyId, int userId);
        #endregion

        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool CheckIfAttachmentsFileExists(int folderId);
       
    }
}
