﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.adm;
#endregion

namespace tap.srv.adm.lst
{
    public class ListValues : IListValues
    {
        tap.dom.adm.SystemList _systemList = new tap.dom.adm.SystemList();
        tap.dom.adm.ListValues _listValues = new dom.adm.ListValues();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();
        

        #region "Insert/Update System List"

        /// <summary>
        /// SaveSystemList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listName"></param>
        /// <param name="listValueId"></param>
        /// <param name="isSystemList"></param>
        /// <param name="isEditable"></param>
        /// <param name="companyId"></param>
        /// <param name="active"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public string SaveSystemList(int listId, string listName, int listValueId, bool isSystemList, bool isEditable, int companyId, bool active, string description, int userId)//change
        {
            //int listid = _listValues.SaveSystemList(listId, listName, listValueId, isSystemList, isEditable, companyId, active, description);
            //return listid;
            return _listValues.SaveSystemList(listId, listName, listValueId, isSystemList, isEditable, companyId, active, description, userId);
        }

        #endregion

        #region "Select System List ID"

        /// <summary>
        /// SysTemListByIDSelect
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string SystemListByIDSelect(int listId)
        {
            return _efSerializer.EFSerialize(_listValues.SystemListByIDSelect(listId));
        }

        #endregion

        #region "Select List Values by passing ID"

        /// <summary>
        /// ListValueByIDSelect
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>
        public string ListValueByIDSelect(int listValueId)
        {
            return _efSerializer.EFSerialize(_listValues.ListValueByIDSelect(listValueId));
        }

        #endregion

        #region "Insert/Update List Values"

        /// <summary>
        /// SaveListValues
        /// </summary>
        /// <param name="listValueId"></param>
        /// <param name="listId"></param>
        /// <param name="parent"></param>
        /// <param name="listValueName"></param>
        /// <param name="active"></param>
        /// <param name="isOptionList"></param>
        /// <returns></returns>
        public bool SaveListValues(int listValueId, int listId, int parent, string listValueName, bool active, bool isOptionList, int userId)//change
        {
            bool isListValueSuccess = _listValues.SaveListValues(listValueId, listId, parent, listValueName, active, userId);
            return isListValueSuccess;
        }

        #endregion

        #region "Delete List Values"

        /// <summary>
        /// ListValueByIDDelete
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>
        public string ListValueByIDDelete(int listValueId)
        {
            return _efSerializer.EFSerialize(_listValues.ListValueByIDDelete(listValueId));
        }

        #endregion

        #region "Get List Values Name"

        /// <summary>
        /// ListValueByNameSelect
        /// </summary>
        /// <param name="listValueName"></param>
        /// <returns></returns>
        public string ListValueByNameSelect(string listValueName)
        {
            return _efSerializer.EFSerialize(_listValues.ListValueByNameSelect(listValueName));
        }

        #endregion 

        #region "To display in tree view for List page"

        /// <summary>
        /// BuildTreeView
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string BuildTreeView(int listId)
        {
            return _listValues.BuildTreeView(listId);
        }

        #endregion

        #region method To display the Jstreeview
        /// <summary>
        /// Invoked To Get Data For Jstreeview
        /// </summary>
        /// <param name="_listid"></param>
        /// <returns></returns>
        public string BuildJSTreeView(int listId, string fullPath)
        {
            return _listValues.BuildJSTreeView(listId, fullPath);
        }

        #endregion

        #region "Method to display System List Items"

        /// <summary>
        /// BuildSystemJSTreeView
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public string BuildSystemJSTreeView(int listId, int companyId)
        {
            return _systemList.BuildSystemJSTreeView(listId, companyId);
        }

        #endregion

        #region To Get ListValueiD From listvaluename

        /// <summary>
        /// ListValueIDByNameSelect
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public string ListValueIDByNameSelect(string fullPath)
        {
            return _efSerializer.EFSerialize(_listValues.ListValueIDByNameSelect(fullPath));
        }

        #endregion

        #region method To display the systemlist either in ddl or treeview

        /// <summary>
        /// DisplaySystemlist
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string DisplaySystemlist(int listId)
        {
            return _efSerializer.EFSerialize(_listValues.DisplaySystemlist(listId));
        }

        #endregion

        #region  To getsystemlist values to Bind Dropdownlist 

        /// <summary>
        /// ListvaluesToBindDDl
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="displayName"></param>
        /// <returns></returns>
        public string ListvaluesToBindDDl(int listId, string displayName)
        {
            return _efSerializer.EFSerialize(_listValues.ListvaluesToBindDDl(listId, displayName));
        }

        #endregion

        #region To update ListValues
        public bool ListValuesUpdate(int listValueId, int parent, string listValueName, int userId)
        {
            return _listValues.ListValuesUpdate(listValueId, parent, listValueName, userId);
        }
        #endregion

        #region To delete multiple nodes

        /// <summary>
        /// ListValuesNodesDelete
        /// </summary>
        /// <param name="deleteListId"></param>
        /// <returns></returns>
        public string ListValuesNodesDelete(int listId, int userId)
        {
            return _listValues.ListValuesNodesDelete(listId, userId);
        }

        #endregion

        #region To build the User Attachment tree

        /// <summary>
        /// BuildAttachmentTree
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public string BuildAttachmentTree(string eventId, int companyId)
        {
            return _listValues.BuildAttachmentTree(eventId, companyId);
        }

        #endregion

        #region To delete multiple nodes from User Attachtree

        /// <summary>
        /// UserAttachTreeDelete
        /// </summary>
        /// <param name="UserAttachmentCheckedNodes"></param>
        /// <returns></returns>
        public bool UserAttachTreeDelete(int[] UserAttachmentCheckedNodes)
        {
            return _listValues.UserAttachTreeDelete(UserAttachmentCheckedNodes);
        }

        #endregion

        #region To Delete Systemlist by Field
        
        /// <summary>
        /// DeleteSystemListByField
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string DeleteSystemListByField(int listId, int userId)
        {
            string isDeleteSuccess = _listValues.DeleteSystemListByField(listId, userId);
            return isDeleteSuccess;
        }

        public string ActivateOrInactivateList(int listId, string listType, bool updateValue, int userId)
        {
            return _listValues.ActivateOrInactivateListFail(listId, listType, updateValue, userId);
        }

        #endregion

        #region "Select Lists(common method)"

        /// <summary>
        /// SelectLocation
        /// </summary>
        /// <param name="systemListIds"></param>
        /// <param name="eventId"></param>
        /// <param name="systemFieldId"></param>
        /// <returns></returns>
        public string SelectLocation(string systemListIds, int eventId, int systemFieldId)
        {
            return _efSerializer.EFSerialize(_listValues.SelectLocation(systemListIds, eventId, systemFieldId));
        }
        #endregion

        #region "To get Admin System Lists"

        /// <summary>
        /// SystemListSelect
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortOrder"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string SystemListSelect(string page, string rows, string sortIndex, string sortOrder,int companyId)
        {
            
            return _systemList.SystemListSelect(page, rows, sortIndex, sortOrder, companyId);
        }

        #endregion

        #region To Get the List value name based  on listid

        /// <summary>
        /// GetListValueName()
        /// Description- Get the List value name
        /// </summary>
        /// <param name="iListId">List id for the geting the List value name</param>
        /// <returns>List value name </returns>
        public string GetListValueName(int listId)
        {
            return _listValues.GetListValueName(listId);
        }

        #endregion

        #region "select location path"
        /// <summary>
        /// Gets the full path from the ListValues table.
        /// </summary>
        /// <param name="systemListIds"></param>
        /// <param name="eventId"></param>
        /// <param name="systemFieldId"></param>
        /// <returns></returns>
        public string SelectLocationPath(string systemListIds, int eventId, int systemFieldId)
        {
            return _efSerializer.EFSerialize(_listValues.SelectLocationPath(systemListIds, eventId, systemFieldId));
        }

        #endregion

        #region  to get systemlistcount
        /// <summary>
        /// SystemlistCount
        /// </summary>
        /// <returns></returns>
        public string SystemlistCount()
        {
            return _efSerializer.EFSerialize(_listValues.SystemlistCount());
        }

        #endregion
      
    }
}
