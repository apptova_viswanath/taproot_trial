﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
#endregion

namespace tap.srv.adm.lst
{
    [ServiceContract]
    public interface IListValues
    {
        #region "Insert/Update System List"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveSystemList(int listId, string listName, int listValueId, bool isSystemList, bool isEditable, int companyId, bool active, string description, int userId);
        #endregion

        #region "Select System List ID"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string SystemListByIDSelect(int listId);
        #endregion

        #region "Select List Values by passing ID"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string ListValueByIDSelect(int listValueId);
        #endregion

        #region "Insert/Update List Values"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool SaveListValues(int listValueId, int listId, int parent, string listValueName, bool active, bool isOptionList, int userId);
        #endregion

        #region "Delete List Values"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string ListValueByIDDelete(int listValueId);
        #endregion

        #region "To display in tree view for List page"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string BuildTreeView(int listId);
        #endregion

        #region method To display the Jstreeview
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string BuildJSTreeView(int listId, string fullPath);
        #endregion

        #region "Method to display System List Items"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string BuildSystemJSTreeView(int listId, int companyId);
        #endregion

        #region "Get List Values Name"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string ListValueByNameSelect(string listValueName);
        #endregion

        #region To Get ListValueiD From listvaluename
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string ListValueIDByNameSelect(string fullPath);
        #endregion

        #region method To display the systemlist either in ddl or treeview
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string DisplaySystemlist(int listId);
        #endregion

        #region  To getsystemlist values to Bind Dropdownlist
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string ListvaluesToBindDDl(int listId, string displayName);
        #endregion

        #region To update ListValues
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool ListValuesUpdate(int listValueId, int parent, string listValueName, int userId);
        #endregion

        #region To delete multiple nodes
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string ListValuesNodesDelete(int listId, int userId);
        #endregion

        #region To build the User Attachment tree
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string BuildAttachmentTree(string eventId, int companyId);
        #endregion

        #region To delete multiple nodes from User Attachtree
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool UserAttachTreeDelete(int[] UserAttachmentCheckedNodes);
        #endregion

        #region To Delete Systemlist by Field
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string DeleteSystemListByField(int listId, int userId);
        #endregion

        #region To de activate Systemlist 

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string ActivateOrInactivateList(int listId, string listType, bool updateValue, int userId);
        #endregion

        #region "Select Lists(common method)"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SelectLocation(string systemListIds, int eventId, int systemFieldId);
        #endregion

        #region "To get Admin System Lists"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SystemListSelect(string page, string rows, string sortIndex, string sortOrder, int companyId);
        #endregion

        #region To Get the List value name based  on listid
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetListValueName(int listId);
        #endregion

        #region "select location path"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SelectLocationPath(string systemListIds, int eventId, int systemFieldId);
        #endregion

        #region  to get systemlistcount
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string SystemlistCount();
        #endregion

    }
}
