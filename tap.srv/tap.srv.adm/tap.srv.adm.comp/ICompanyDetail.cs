﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.adm.comp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICompanyDetail" in both code and config file together.
    [ServiceContract]
    public interface ICompanyDetail
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveCompanyDetail(int companyId, string companyName, string phoneNumber, string website, string address, string email, bool adsecurity,
            string dateFormat, bool isTwelveHourFormat, string applicationTimeOut, string subscriptionStart, string subscriptionEnd, int passwordPolicy, int passwordExpiration);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool ParentCompany(int companyId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        int SubscriptionExpireDays(int companyID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string CheckSubscriptionTime(string userName);
    }
}
