﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.adm;

namespace tap.srv.adm.comp
{
   

    public class DivisionDetail : IDivisionDetail
    {
        tap.dom.adm.Divisions divisionDetails = new dom.adm.Divisions();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();



        public string DivisionDetails(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID)
        {
            return divisionDetails.DivisionDetails(page, rows, SearchFilter, IgnoreCase, sortIndex, sortOrder, companyID);
        }
        public string SaveCompanyWithAdmin(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website,
            string firstName, string lastName, string userEmail, string password, string userName, string userPhone, string subscriptionStart, string subscriptionEnd)
        {
            return divisionDetails.SaveCompanyWithAdmin(parentCompanyId, companyName, phoneNumber, address, companyEmail, website,
                firstName, lastName, userEmail, password, userName, userPhone, subscriptionStart, subscriptionEnd);
        }

        public string SystemListIdGet(int companyId)
        {
            return divisionDetails.SystemListIdGet(companyId);
        }
        //public string BuildJSCompanyTree(int parentCompany, string fullPath)
        //{
        //    return divisionDetails.BuildJSCompanyTree(parentCompany, fullPath);
        //}
        //public string SelectCompany(int companyId)
        //{
        //    return _efSerializer.EFSerialize(divisionDetails.SelectCompany(companyId));
        //}

        public string SaveCompanyWithActiveDirectoryAdmin(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website,
            string userName, //string firstName, string lastName, string userEmail, string password, string userName, string userPhone,
            string serverName, string domainName, string groupName, string adUserName, string adPassword)
        {
            return divisionDetails.SaveCompanyWithActiveDirectoryAdmin(parentCompanyId, companyName, phoneNumber, address, companyEmail, website,
                userName, //firstName, lastName, userEmail, password, userName, userPhone, 
                serverName, domainName, groupName, adUserName, adPassword);
        }
    }
}
