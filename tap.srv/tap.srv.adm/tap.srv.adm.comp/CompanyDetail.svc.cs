﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using tap.dom.adm;

namespace tap.srv.adm.comp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CompanyDetail" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CompanyDetail.svc or CompanyDetail.svc.cs at the Solution Explorer and start debugging.
    public class CompanyDetail : ICompanyDetail
    {
        private tap.dom.adm.Company companyDetails = new dom.adm.Company();

        public string SaveCompanyDetail(int companyId, string companyName, string phoneNumber, string website, string address,
            string email, bool adsecurity, string dateFormat, bool isTwelveHourFormat, string applicationTimeOut, string subscriptionStart, string subscriptionEnd,
            int passwordPolicy, int passwordExpiration)
        {
            return companyDetails.SaveCompanyDetails(companyId, companyName, phoneNumber, website, address, email, adsecurity, dateFormat, isTwelveHourFormat, applicationTimeOut,
                subscriptionStart, subscriptionEnd, passwordPolicy, passwordExpiration);
        }

        public bool ParentCompany(int companyId)
        {
            return companyDetails.isParentCompany(companyId);
        }

        public int SubscriptionExpireDays(int companyID)
        {
            return companyDetails.SubscriptionExpireDays(companyID);
        }

        public string CheckSubscriptionTime(string userName)
        {
            return companyDetails.CheckSubscriptionTime(userName);
        }
    }
}
