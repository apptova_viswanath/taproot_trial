﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace tap.srv.adm.comp
{
    [ServiceContract]
    public interface IDivisionDetail
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string DivisionDetails(int page, int rows, string SearchFilter, int IgnoreCase, string sortIndex, string sortOrder, int companyID);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveCompanyWithAdmin(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website,
            string firstName, string lastName, string userEmail, string password, string userName, string userPhone, string subscriptionStart, string subscriptionEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SystemListIdGet(int companyId);

        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //string BuildJSCompanyTree(int parentCompany, string fullPath);

        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //string SelectCompany(int companyId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveCompanyWithActiveDirectoryAdmin(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website,
            string userName,
            string serverName, string domainName, string groupName, string adUserName, string adPassword);
    }
}
