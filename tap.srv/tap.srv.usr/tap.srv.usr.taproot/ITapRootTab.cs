﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace tap.srv.usr.taproot
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITapRootTab" in both code and config file together.
    [ServiceContract]
    public interface ITapRootTab
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string SaveSnapChart(int snapChartId, string eventId, int seasonId, bool isComplete, string snapChartData, bool isReverse);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string SnapChartDataSelect(string eventId);

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string GetSnapChartDetails(string eventId);


        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string SeasonDataSelect();

        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string CompletedSnapChartSelect(string eventId);

    }
}
