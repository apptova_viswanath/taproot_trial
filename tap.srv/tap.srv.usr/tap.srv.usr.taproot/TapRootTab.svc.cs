﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace tap.srv.usr.taproot
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TapRootTab" in code, svc and config file together.
    public class TapRootTab : ITapRootTab
    {
        tap.dom.usr.TapRootTab _tapRootTab = new dom.usr.TapRootTab();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

        #region SaveSnapChart
        public string SaveSnapChart(int snapChartId, string eventId, int seasonId, bool isComplete, string snapChartData, bool isReverse)
        {
            return _tapRootTab.SaveSnapChart(snapChartId, eventId, seasonId, isComplete, snapChartData, isReverse);
        }
        #endregion

        #region SnapChartDataSelect
        public string SnapChartDataSelect(string eventId)
        {
            return _efSerializer.EFSerialize(_tapRootTab.SnapChartDataSelect(eventId));
        }
        #endregion

        #region GetSnapChartDetails
        //public object GetSnapChartDetails(string eventId)
        public string GetSnapChartDetails(string eventId)
        {
            return _tapRootTab.GetSnapChart(eventId);
            //return _efSerializer.EFSerialize(_tapRootTab.GetSnapChart(eventId));
        }
        #endregion

        #region SeasonDataSelect
        public string SeasonDataSelect()
        {
            return _efSerializer.EFSerialize(_tapRootTab.SeasonDataSelect());
        }
        #endregion

        #region CompletedSnapCharSelect
        public string CompletedSnapChartSelect(string eventId)
        {
            return _efSerializer.EFSerialize(_tapRootTab.CompletedSnapChartSelect(eventId));
        }
        #endregion

    }
}
