﻿#region "Namspaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
//using tap.dom.usr;
#endregion

namespace tap.srv.usr
{
    [ServiceContract]
    public interface IUsers
    {
        #region "Get Active Users"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object UsersMostActiveSelect(string userName, string password);
        #endregion
        
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetAllActiveUsers(int companyId);

        
        #region "Change Password"
        [OperationContract]
         [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string UserPasswordChange(string username, string oldPassword, string newPassword);
        #endregion

        #region "Get the User Details"
        [OperationContract]
         [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
         string GetLoginUserDetails(string userName, string password);
         #endregion

        #region "Get the User Authentication"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        [FaultContract(typeof(CustomFaultDetails))]
        string GetUsersAuthentication(string userName, string password);
        #endregion

        #region Get User Full Name
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object GetUserFullName(int userId);
        #endregion

        #region "Get the User admin details"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetUserandAdminDetails(string userName, string password);
        #endregion

        #region "Get the User Information"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        object CheckIfGlobalAdminProfile(int userId, int companyId, int selectedDivisionId);
        #endregion

        #region "User Forgot Password"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string forgotPassword(string emailId, string baseUrl);

        #endregion
        #region "User Reset Password"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool ResetPassword(string userName, string resetPassword);

        #endregion

        #region "User Language Settings"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string InsertLangaugeDetails(string langaugeCode, int companyId, int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetLangaugeDetails(int companyId, int userId, string langaugeCode);

        #endregion

        //#region "Encrypt All User Passwords"
        
        //[OperationContract]
        //[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //string EncryptDbPasswords();

        //#endregion

        #region "Active Directory"

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string ConnectActiveDirectory(string groupName, string domainName, string serverName, string adminUserName, string tapAdminUserName, string tapAdminPassword);

        #endregion
    }

    
}
