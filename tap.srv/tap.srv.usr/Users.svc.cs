﻿#region "Namesapces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
#endregion

namespace tap.srv.usr
{

    [DataContract]
    public class CustomFaultDetails
    {
        [DataMember]
        public string ErrorID { get; set; }

        [DataMember]
        public string ErrorDetails { get; set; }
    }

    //[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class Users : IUsers
    {
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();
        tap.dom.usr.Users _users = new tap.dom.usr.Users();
        tap.dom.usr.Authentication _authentication = new tap.dom.usr.Authentication();

        #region "Get Active Users"

        /// <summary>
        /// UsersMostActiveSelect
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public object UsersMostActiveSelect(string userName, string password)
        {
            return _users.MostActiveUsersSelect(userName, password);
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventID"></param>
        public object GetAllActiveUsers(int companyId)
        {
            tap.dom.usr.Users user = new tap.dom.usr.Users();
            return _efSerializer.EFSerialize(user.GetAllActiveUsers(companyId));
        }


        #region "Change Password"

        /// <summary>
        /// UserPasswordChange
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="oldpassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public string UserPasswordChange(string userName, string oldpassword, string newPassword)
        {
            return _users.UserPasswordChange(userName, oldpassword, newPassword);
        }

        #endregion

        #region "To get the User Login Details"

        /// <summary>
        /// GetLoginUserDetails
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetLoginUserDetails(string userName, string password)
        {
            return _efSerializer.EFSerialize(_users.GetLoginUserDetails(userName, password));
        }

        #endregion

        public string GetUserandAdminDetails(string userName, string password)
        {
            return _users.GetUserandAdminDetails(userName, password);
        }

        public string GetUsersAuthentication(string userName, string password)
        {
            string result = null;

            try
            {
                return _authentication.isAuthenticateUser(userName, password);
            }
            catch (Exception ex)
            {

                //CustomFaultDetails obj = new CustomFaultDetails();
                //obj.ErrorID = ex.Message;
                //obj.ErrorDetails = ex.ToString();
                //throw new FaultException(obj, "Reason: Testing…..");
                result = ex.ToString();
            }

            return result;
        }

        public object CheckIfGlobalAdminProfile(int userId, int companyId, int selectedDivisionId)
        {
            //return _users.CheckIfGlobalAdminProfile(userId, companyId, selectedDivisionId);
            tap.dom.usr.Users user = new tap.dom.usr.Users();
            return _efSerializer.EFSerialize(user.CheckIfGlobalAdminProfile(userId, companyId, selectedDivisionId));

        }
        public string forgotPassword(string emailId, string baseUrl)
        {
            return _users.ForgotPassword(emailId, baseUrl);
        }

        #region "User Setting Language"
        public string InsertLangaugeDetails(string langaugeCode, int companyId, int userId)
        {
            return _users.InsertLangaugeDetails(langaugeCode, companyId, userId);
        }


        public string GetLangaugeDetails(int companyId, int userId, string langaugeCode)
        {
            return _users.GetLangaugeDetails(companyId, userId, langaugeCode);
        }
        #endregion

        #region IUsers Members


        public bool ResetPassword(string userName, string resetPassword)
        {
            return _users.ResetPassword(userName, resetPassword);
        }

        #endregion

        //#region All User Password Encryption
        
        //public string EncryptDbPasswords()
        //{
        //    return _users.EncryptDBPasswords();
        //}

        //#endregion

        #region Active Directory Settings

        public string ConnectActiveDirectory(string groupName, string domainName, string serverName, string adminUserName, string tapAdminUserName, string tapAdminPassword)
        {
            return _users.ConnectActiveDirectory(groupName, domainName, serverName, adminUserName, tapAdminUserName, tapAdminPassword);
        }

        #endregion 

        #region Get User Full name based on UserID

        public object GetUserFullName(int userId)
        {
            return _users.GetUserFullName(userId);
        }

        #endregion
    }
}
