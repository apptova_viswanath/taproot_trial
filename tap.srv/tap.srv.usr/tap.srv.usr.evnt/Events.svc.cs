﻿#region "Namspaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
#endregion

namespace tap.srv.usr.evnt
{
    public class Events : IEvents
    {
        tap.dom.usr.Event _events = new dom.usr.Event();
        tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();
        tap.dom.gen.GenericSave _genericSave = new dom.gen.GenericSave();

        #region "To get the EventDetails"

        /// <summary>
        /// EventDetails
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="filter"></param>
        /// <param name="bssearch"></param>
        /// <param name="loginuser"></param>
        /// <param name="advsearch"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string EventDetails(int page, int rows, string sortIndex, string sortOrder, string filter, string basicSearch, string advanceSearch,
                                  string userID, string companyID, string baseURL)//Added bssearch on 10-May-2012/Advsearch on 14-May-2012
        {
            return _events.EventDetails(page, rows, sortIndex, sortOrder, filter, basicSearch, advanceSearch, userID, companyID, baseURL);
        }

        #endregion

        #region "To Get The Event count"

        /// <summary>
        /// Invoked To get The EventType count
        /// </summary>
        /// <param name="loginuser"></param>
        /// <returns></returns>
        public string EventCount(string userID)
        {
            return _events.EventCount(userID);
        }

        #endregion
        #region "To Get The Event count"

        public string TasksCount(string userID)
        {
            return _events.TasksCount(userID);
        }

        #endregion


        #region For  Saving the Events

        /// <summary>
        /// SaveEvent
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventName"></param>
        /// <param name="eventDate"></param>
        /// <param name="eventTime"></param>
        /// <param name="location"></param>
        /// <param name="classification"></param>
        /// <param name="status"></param>
        /// <param name="createdBy"></param>
        /// <param name="eventType"></param>
        /// <param name="strFieldvalues"></param>
        /// <param name="invDate"></param>
        /// <param name="invTime"></param>
        /// <param name="eventDescription"></param>
        /// <param name="eventFileNumber"></param>
        /// <param name="capDate"></param>
        /// <param name="capTime"></param>
        /// <returns></returns>
        public string[] SaveEvent(int eventId, string eventName, string eventDate, string eventTime, string location, string classification, int status,
                                string userID, int eventType, string investiagtionDate, string investiagtionTime, string eventDescription, string eventFileNumber,
                                   string capDate, string capTime, string companyID, string baseURL, string[] arrDynamicFieldIds, string[] arrDynamicFieldValues, string[] arrControlsNames)//new-change
        {
            return _events.SaveEvent(eventId, eventName, eventDate, eventTime, location, classification, status, userID, eventType, investiagtionDate, investiagtionTime,
                                    eventDescription, eventFileNumber, capDate, capTime, companyID, baseURL, arrDynamicFieldIds, arrDynamicFieldValues, arrControlsNames);
        }

        #endregion

        #region "Location Insert/Update"

        /// <summary>
        /// LocationSave
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="locationId"></param>
        public void LocationSave(string eventId, string locationId, int userId)
        {
            _genericSave.LocationSave(eventId, locationId, userId);
        }

        #endregion

        #region "Classification Insert/Update"

        /// <summary>
        /// ClassificationSave
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="classificationId"></param>
        public void ClassificationSave(string eventId, string classificationId, int userId)
        {
            _genericSave.ClassificationSave(eventId, classificationId, userId);
        }

        #endregion

        #region "Check Event Unique"

        /// <summary>
        /// EventUnique
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="eventName"></param>
        /// /// <param name="eventId"></param>
        public string CheckEventName(string eventId, string companyID, string eventName)
        {
            return _events.CheckEventName(eventId, companyID, eventName);
        }

        #endregion

        #region "Get 6 top pending tasks and action plans and 3 top event names"
        public string GetTopEventPendingTasks(int userId, int companyId)
        {
            return _events.GetTopEventPendingTasks(userId, companyId);
        }
        #endregion

        public string GetTasks(string page, string rows, string sortIndex, string sortOrder, int userId, int companyId, string status)
        {
            return _events.GetTasks(page, rows, sortIndex, sortOrder, userId, companyId, status);
        }

        public string GetTopEvents(int userId, int companyId, string baseURL)
        {
            return _events.GetTopEvents(userId, companyId, baseURL);
        }
    }
}
