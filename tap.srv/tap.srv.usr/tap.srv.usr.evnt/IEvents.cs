﻿#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.Web.Configuration;

#endregion

namespace tap.srv.usr.evnt
{
    [ServiceContract]
    public interface IEvents
    {
        #region "To get the EventDetails"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string EventDetails(int page, int rows, string sortIndex, string sortOrder, string filter, string basicSearch, string advanceSearch, string userID, string companyID, string baseURL);
        #endregion

        #region "To Get The Event count"
        //To get the Event count
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string EventCount(string userID);
        #endregion

        #region "To Get The Task count"
        //To get the Event count
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string TasksCount(string userID);
        #endregion

        #region For  Saving the Events
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string[] SaveEvent(int eventId, string eventName, string eventDate, string eventTime, string location, string classification, int status,
                                string userID, int eventType, string investiagtionDate, string investiagtionTime, string eventDescription, string eventFileNumber,
                                   string capDate, string capTime, string companyID, string baseURL, string[] arrDynamicFieldIds, string[] arrDynamicFieldValues, string[] arrControlsNames);//new-change
        #endregion

        #region "Location Insert/Update"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void LocationSave(string eventId, string locationId, int userId);
        #endregion

        #region "Classification Insert/Update"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void ClassificationSave(string eventId, string classificationId, int userId);
        #endregion

        #region "To check duplicate event"
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json)]
        string CheckEventName(string eventId, string companyID, string eventName);
        #endregion

        #region "Get 6 top pending tasks and action plans and 3 top event names"
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTopEventPendingTasks(int userId, int companyId);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTopEvents(int userId, int companyId, string baseURL);
        #endregion

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        string GetTasks(string page, string rows, string sortIndex, string sortOrder, int userId, int companyId, string status);
    }

}
