﻿//---------------------------------------------------------------------------------------------
// File Name 	: State.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to State entity
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;

namespace tap.dom.adm
{
    public class State
    {

        tap.dat.EFEntity _entityContext = new tap.dat.EFEntity();

        #region Get the state details by state id

        /// <summary>
        /// Get the state details by state id
        /// </summary>
        /// <param name="_stateid"></param>
        /// <returns></returns>
        //public tap.dat.States GetStateByID(int stateId)
        //{

        //    tap.dat.States state = new tap.dat.States();

        //    //Get the state detail by state id
        //    try
        //    {
        //        state = _entityContext.States.First(x => x.StateID == stateId);
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //    return state;

        //}

        //#endregion

        //#region Get the state By list

        ///// <summary>
        ///// Get all the states as a list
        ///// </summary>
        ///// <returns></returns>
        //public List<tap.dat.States> GetStateList()
        //{

        //    List<tap.dat.States> stateList = new List<tap.dat.States>();

        //    //Get all the statelist
        //    try
        //    {
        //        stateList = _entityContext.States.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //    return stateList;
        //}

        //#endregion

        //#region For Insert/Update State

        ///// <summary>
        ///// Save the state (Insert a new one or update an existing one)
        ///// </summary>
        ///// <param name="stateId">id of the state</param>
        ///// <param name="stateName">name of the state</param>
        ///// <param name="abbreviation">abbreviation used for the state</param>
        //public void SaveState(int stateId, string stateName, string abbreviation)
        //{

        //    tap.dat.States state = new tap.dat.States();

        //    try
        //    {

        //       //Check if the state to be inserted or updated
        //        if (stateId != Convert.ToInt32(Enumeration.Types.New))
        //        {
        //            //Get the state which will get updated
        //            state = GetStateByID(stateId);

        //            //Set current values, save changes and return
        //            SetValues(ref state, stateName, abbreviation);
        //            SaveChanges();

        //            return;
        //        }

        //        //Insert new state and save chnages
        //        SetValues(ref state, stateName, abbreviation);
                
        //        //Add and save changes
        //        _entityContext.AddToStates(state);
        //        SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //}

        ///// <summary>
        ///// Set the properties for the state object
        ///// </summary>
        ///// <param name="state"></param>
        ///// <param name="stateName"></param>
        ///// <param name="abbreviation"></param>
        //public void SetValues(ref tap.dat.States state, string stateName, string abbreviation)
        //{

        //    try
        //    {
        //        state.StateName = stateName;
        //        state.Abbrevation = abbreviation;
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //}

        ///// <summary>
        ///// Save changes
        ///// </summary>
        //public void SaveChanges()
        //{
        //    _entityContext.SaveChanges();
        //}
        //#endregion
                    
        //#region Delete a state By Id

        ///// <summary>
        ///// Delete a state By Id
        ///// </summary>
        ///// <param name="_stateid"></param>
        //public void DeleetStateByID(int stateId)
        //{

        //    tap.dat.States state = new tap.dat.States();

        //    try
        //    {
        //        //Get the state by state id
        //        state = GetStateByID(stateId);

        //        //Delete from database
        //        _entityContext.DeleteObject(state);

        //        SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //}

        #endregion

    }
}
