﻿//---------------------------------------------------------------------------------------------
// File Name 	: ListValues.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to SystemList and List values 
//---------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Data.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Web;
using tap.dom.gen;
using tap.dom.hlp;
#endregion

namespace tap.dom.adm
{
    public class ListValues
    {
        #region "Global variables"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        public int _newValue = Convert.ToInt32(tap.dom.hlp.Enumeration.Types.New);

        //for new insertion
        public int _newInsert = Convert.ToInt32(tap.dom.hlp.Enumeration.Types.NewInsert);
        List<int> listIds = new List<int>();
        List<tap.dat.ListValues> _currentListValues = new List<tap.dat.ListValues>();

        List<tap.dat.AttachmentFolders> _folders;
        List<tap.dom.hlp.AttachmentsInfo> _attachmentDetails;
        private string _message;
       
        #endregion

        #region "Insert/Update operation of Systemlist  Table"

        /// <summary>
        /// SaveSystemList
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listName"></param>
        /// <param name="listValueId"></param>
        /// <param name="isSystemList"></param>
        /// <param name="isEditable"></param>
        /// <param name="companyId"></param>
        /// <param name="active"></param>
        /// <param name="description"></param>
        /// <returns></returns>

        public string SaveSystemList(int listId, string listName, int listValueId, bool isSystemList, bool isEditable, int companyId, bool active, string description, int userId)//change
        {

            int currentListId = 0;
            string oldListName = string.Empty;
            listName = listName.Trim();

            try
            {
                var list = _db.ListValues.FirstOrDefault(a => a.ListID == listId && a.Parent == null);
                    
                tap.dat.SystemLists systemList = null;

               systemList = _db.SystemLists.Where(a => a.ListName == listName && a.CompanyID==companyId && a.ListID != listId).FirstOrDefault();

               if (systemList != null)
               {
                   return listName + " List already exists.";
               }

                systemList = listId == _newInsert ? new dat.SystemLists() : _db.SystemLists.First(a => a.ListID == listId);

                systemList.ListName = listName;
                systemList.IsSystemList = isSystemList;
                systemList.IsEditable = isEditable;
                systemList.CompanyID = companyId;
                if (listId == _newInsert)
                {
                    systemList.Active = active;
                }
                systemList.Description = description;

                if (list == null)
                    _db.AddToSystemLists(systemList);
                else
                    oldListName = _db.ListValues.FirstOrDefault(a => a.ListID == listId && a.Parent == null).ListValueName;

                _db.SaveChanges();

                //To insert the Data in ListValue Table with Parent as Null
                if (listId == _newInsert)
                {
                    SaveListValues(_newInsert, systemList.ListID, _newInsert, systemList.ListName, systemList.Active, userId);
                    _message = "System List '" + systemList.ListName + "' added.";
                }
                else
                {
                    FullPathUpdate(listId, listName, oldListName, listName);
                    _message = "System List '" + listName + "' modified to " + listName + ".";
                }

                tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Create, _message);       


                //Get the latest list id.
                systemList = _db.SystemLists.First(a => a.ListName == listName);
                currentListId = systemList.ListID;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            //return currentListId;
            return currentListId.ToString();
        }

        #endregion

        #region "Updating Fullpath"

        /// <summary>
        /// FullPathUpdate
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listName"></param>
        /// <param name="oldName"></param>
        /// <param name="newName"></param>

        public void FullPathUpdate(int listId, string listName, string oldName, string newName)
        {
            try
            {
                using (_db)
                {
                    List<tap.dat.ListValues> listValues = _db.ListValues.Where(a => a.ListID == listId).ToList();
                    if (oldName != newName)
                    {
                        foreach (var currentListValue in listValues)
                        {
                            if (currentListValue.Parent == null)
                                currentListValue.ListValueName = listName;

                            currentListValue.FullPath = currentListValue.FullPath.Replace("\\\\", "#").Replace(oldName, listName).Replace("#", "\\\\");

                            _db.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            };

            _db = new dat.EFEntity();
        }

        #endregion

        #region "Insert/Update the  Data  in  ListValues Table"

        /// <summary>
        /// SaveListValues-Insert List Values
        /// </summary>
        /// <param name="listValueId"></param>
        /// <param name="listId"></param>
        /// <param name="parent"></param>
        /// <param name="listValueName"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public bool SaveListValues(int listValueId, int listId, int parent, string listValueName, bool active, int userId)//change
        {
            bool isListValueExist = true;
            string fullPath = "";

            tap.dat.ListValues listValue = null;
            listValueName = listValueName.Trim();

            try
            {
                if (listValueId == _newInsert)
                {
                    listValue = new dat.ListValues();

                    //if (CheckListValueExists(listId, listValueName))
                    //{
                    //    isListValueExist = false;
                    //    return isListValueExist;
                    //}
                }
                else
                {
                    listValue = _db.ListValues.First(a => a.ListValueID == listValueId);

                    int newListValueId = ListValueByIDSelect(listId, listValueName);
                    if (newListValueId != 0)
                    {
                        if (newListValueId != listValueId)
                        {
                            isListValueExist = false;
                            return isListValueExist;
                        }
                    }
                }

                listValue.ListID = listId;

                listValue.Parent = parent == _newInsert ? (int?)null : parent;

                #region To Get the Fullpath

                fullPath = parent != _newInsert ? GetFullPath(parent) + listValueName + @"\" : "\\\\" + listValueName + @"\";

                #endregion

                listValue.ListValueName = listValueName;
                listValue.Active = active;
                listValue.FullPath = fullPath;

                if (listValueId == _newInsert)
                {
                    _db.AddToListValues(listValue);
                    if (parent != 0)
                    {
                        string parentListName = _db.ListValues.First(l => l.ListValueID == parent).ListValueName;
                        _message = "System List '" + listValueName + "' added to parent list '" + parentListName + "'";
                        transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Create, _message);
                    }
                }

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isListValueExist;
        }

        #endregion

        #region For checking duplicate Listvalues

        /// <summary>
        /// Invoked To Check the Duplicate ListValues
        /// </summary>
        /// <returns></returns>
        public bool CheckListValueExists(int listId, string listValueName)
        {
            bool isListValueExist = false;
            try
            {
                tap.dat.ListValues listValue = _db.ListValues.FirstOrDefault(a => (a.ListValueName.Trim().ToLower() == listValueName.Trim().ToLower()) && a.ListID == listId);

                if (listValue != null)
                    isListValueExist = true;

                return isListValueExist;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isListValueExist;
        }

        #endregion

        #region "Return the ListValueid By ListValuename"

        /// <summary>
        /// Overloade method  to Get the ListValueid By ListValuename
        /// </summary>
        /// <param name="_listid"></param>
        /// <param name="_listvaluename"></param>
        /// <returns></returns>
        public int ListValueByIDSelect(int listId, string listValueName)//chnage the method name
        {
            int listValueId = 0;
            try
            {
                tap.dat.ListValues listValue = _db.ListValues.FirstOrDefault(a => (a.ListValueName.Trim().ToLower() == listValueName.Trim().ToLower()) && a.ListID == listId);

                if (listValue != null)
                    listValueId = listValue.ListValueID;

                return listValueId;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return listValueId;
        }

        #endregion

        #region "Get The Systemlist Based on the ListId"

        /// <summary>
        /// SysTemListByIDSelect
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public tap.dat.SystemLists SystemListByIDSelect(int listId)//change the method name-deepa
        {
            try
            {
                var systemList = _db.SystemLists.First(a => a.ListID == listId);
                return systemList;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return null;
        }

        #endregion

        #region "Get the  Listvalue ByID"

        /// <summary>
        /// ListValueByIDSelect
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>
        public tap.dat.ListValues ListValueByIDSelect(int listValueId)//change the method name-deepa
        {
            try
            {
                var listValue = _db.ListValues.First(a => a.ListValueID == listValueId);
                return listValue;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return null;
        }

        #endregion

        #region "Delete the Listvalid By ID"

        /// <summary>
        /// ListValueByIDDelete
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>
        public bool ListValueByIDDelete(int listValueId)//change the method name-deepa
        {
            bool isDelete = false;
            try
            {
                var listValue = _db.ListValues.First(a => a.ListValueID == listValueId);

                if (!CheckChildNode(listValueId) && listValue.Parent != null)//Root Node should not Delete
                {
                    _db.DeleteObject(listValue);
                    _db.SaveChanges();
                    isDelete = true;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isDelete;
        }

        #endregion

        #region "Check the ChildNode For the ListValue"

        /// <summary>
        /// CheckChildNode
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>

        public bool CheckChildNode(int listValueId)
        {
            bool isParentNodeExists = false;
            try
            {
                var listValue = _db.ListValues.Where(a => a.Parent == listValueId).ToList();
                if (listValue.Count() > 0)
                    isParentNodeExists = true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isParentNodeExists;
        }

        #endregion

        #region To Get The list Value using LIST VALUE NAME

        /// <summary>
        /// ListValueByNameSelect
        /// </summary>
        /// <param name="listValueName"></param>
        /// <returns></returns>
        public bool ListValueByNameSelect(string listValueName)
        {
            bool isListNameExist = false;
            try
            {
                tap.dat.ListValues listValue = _db.ListValues.FirstOrDefault(a => a.ListValueName.ToLower() == listValueName.ToLower());
                if (listValue != null)
                    isListNameExist = true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isListNameExist;
        }

        #endregion

        #region "To display in tree view for List page"

        /// <summary>
        /// BuildTreeView
        /// </summary>
        /// <param name="_listid"></param>
        /// <returns></returns>
        public string BuildTreeView(int listId)
        {
            List<tap.dat.ListValues> securities = _db.ListValues.Where(a => a.ListID == listId).OrderBy(a => a.FullPath).ToList();

            //begin json
            StringBuilder jsonData = new StringBuilder("");
            try
            {
                var parents = from child in securities
                              where child.Parent == null
                              select child;

                if (parents.Count() != 0)
                {
                    jsonData.Append("[ ");
                    foreach (var parent in parents)
                    {
                        jsonData.Append(BuildChild(securities, parent.ListValueID));//build child nodes
                        jsonData.Append(" , ");
                    }
                    jsonData.Append(" ], ");
                    jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                    jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return jsonData.ToString();
        }

        private string BuildChild(List<tap.dat.ListValues> securities, int parentID)
        {
            StringBuilder childNodes = new StringBuilder();
            var currentNode = (from child in securities where child.ListValueID == parentID select child).First();

            var children = from child in securities
                           where child.Parent == parentID
                           select child;

            childNodes.Append("{ \"label\":\" " + currentNode.ListValueName + "\"" + "," + "\"id\":\" " + currentNode.ListValueID + "\" ");

            if (children.Count() == 0)
                childNodes.Append(" } ");
            else
            {
                childNodes.Append(", \"children\": [ ");//create child node
                foreach (var child in children)
                {
                    childNodes.Append(string.Format(" {0},", BuildChild(securities, child.ListValueID)));//recursive function call
                }
                childNodes.Remove(childNodes.ToString().LastIndexOf(","), 1);
                childNodes.Append(" ]} ");

            }

            return childNodes.ToString();
        }

        #endregion

        #region Build the JSTree for user side

        public string BuildJSTreeView(int listId, string fullPath)
        {
            List<tap.dat.ListValues> securities = new List<dat.ListValues>();
            List<tap.dat.AttachmentFolders> attachFoldersNode = new List<dat.AttachmentFolders>();
            List<dat.ListValues> security = new List<dat.ListValues>();
            StringBuilder jsonData = new StringBuilder("");

            if (fullPath != "0")
            {
                listId = ListValueIDByNameSelect(fullPath);
            }
            try
            {
                if (listId == _newValue)
                {
                    attachFoldersNode = _db.AttachmentFolders.OrderBy(a => a.FullPath).ToList();
                }
                else
                {
                    securities = _db.ListValues.Where(a => a.ListID == listId && a.Active == true).OrderBy(a => a.FullPath).ToList();
                    

                    foreach (var item in securities)
                    {
                        if (item.Parent == null)
                        {
                            security.Add(item);
                        }
                        else
                        {
                            var res = securities.Find(obj => obj.ListValueID == item.Parent);
                            if (res != null)
                                security.Add(item);
                        }
                    }
                }

                if (listId == _newValue)
                {
                    var attachmentFodlerParent = from child in attachFoldersNode
                                        where child.ParentId == null
                                        select child;
                    if (attachmentFodlerParent.Count() != 0)
                    {
                        jsonData.Append("[ ");
                        foreach (var parent in attachmentFodlerParent)
                        {
                            jsonData.Append(BuildJSTreeViewChildAttach(attachFoldersNode, parent.AttachementFolderId));//build child nodes
                            jsonData.Append(" , ");
                        }
                        jsonData.Append(" ], ");
                        jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                        jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                    }

                }
                else
                {
                    var parents = from child in security
                                  where child.Parent == null
                                  select child;
                    if (parents.Count() != 0)
                    {
                        jsonData.Append("[ ");

                        foreach (var parent in parents)
                        {
                            jsonData.Append(BuildJSTreeViewChild(security, parent.ListValueID));//build child nodes
                            jsonData.Append(",");
                        }

                        if (jsonData.ToString().LastIndexOf(",") == jsonData.Length - 1)
                        {
                            jsonData = jsonData.Remove(jsonData.Length - 2, 1);
                        }

                        jsonData.Append("]");

                        //jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                        //jsonData.Remove(jsonData.ToString().LastIndexOf(", "), 2);
                    }
                }
                
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            if (jsonData.ToString() != "")
            {
                return jsonData.ToString().Remove(jsonData.ToString().LastIndexOf(","), 1);
            }
            else
            {
                return null;
            }
            
        }

        #endregion

        #region "Child Build for Attachment"

        /// <summary>
        /// BuildJSTreeViewChildAttach
        /// </summary>
        /// <param name="atachmentFolders"></param>
        /// <param name="attachmentFolderParentID"></param>
        /// <returns></returns>
        public string BuildJSTreeViewChildAttach(List<tap.dat.AttachmentFolders> attachFoldersNode, int attachmentFolderParentID)
        {
            StringBuilder jsonData = new StringBuilder();
            try
            {
                var thisNode = (from child in attachFoldersNode where child.AttachementFolderId == attachmentFolderParentID select child).First();

                var children = from child in attachFoldersNode
                               where child.ParentId == attachmentFolderParentID
                               select child;

                jsonData.Append("{ \"data\":\" " + thisNode.FolderName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + thisNode.AttachementFolderId + "\" ");

                if (children.Count() == 0)
                    jsonData.Append(",\"rel\":\"folders\" }} ");
                else
                {
                    jsonData.Append(",\"rel\":\"files\" }, \"children\": [ ");//create child node

                    foreach (var child in children)
                    {
                        jsonData.Append(string.Format(" {0},", BuildJSTreeViewChildAttach(attachFoldersNode, child.AttachementFolderId)));
                    }

                    jsonData.Remove(jsonData.ToString().LastIndexOf(","), 1);
                    jsonData.Append(" ]} ");
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return jsonData.ToString();
        }

        #endregion

        #region "Format the fullpath"

        /// <summary>
        /// FormattedFullpath
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public string FormattedFullpath(string location)
        {
            string fullPath = "";
            try
            {
                fullPath = location.Remove(0, 2); // remove the text "//" from text like \\Classification List\c2\
                fullPath = fullPath.Remove(0, fullPath.IndexOf('\\') + 1); // remove the text related to topmost parent system list like '\\Classification List'
                
                if (fullPath != string.Empty)
                {
                    fullPath = fullPath.Remove(fullPath.Length - 1, 1); // remove the last text '\'
                    fullPath = fullPath.Replace("\\", " > "); // replace the remainiang \\ to >
                }

                return fullPath;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;

        }
        #endregion

        #region  Build ChildJsonData For JSTreeview

        /// <summary>
        /// Invoked To build the child Json Data For Jstree
        /// </summary>
        /// <param name="securities"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public string BuildJSTreeViewChild(List<tap.dat.ListValues> securities, int parentId)
        {
            StringBuilder jsonData = new StringBuilder();
            try
            {
                var currentNode = (from child in securities where child.ListValueID == parentId select child).First();


                var children = from child in securities
                               where child.Parent == parentId
                               select child;

                string fullPath = string.Empty;
                if (currentNode.Parent != null)
                {
                    // jsonData.Append("{ \"data\":\" " + currentNode.ListValueName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + currentNode.ListValueID + "\" ");

                    // Formatted the fullpath as need to be displayed at user end and to set that to the title of the li
                    //fullpath = currentNode.FullPath.Remove(0, 2); // remove the text "//" from text like \\Classification List\c2\
                    //fullpath = fullpath.Remove(0, fullpath.IndexOf('\\') + 1); // remove the text related to topmost parent system list like '\\Classification List'
                    //fullpath = fullpath.Remove(fullpath.Length - 1, 1); // remove the last text '\'
                    //fullpath = fullpath.Replace("\\", " > "); // replace the remainiang \\ to >

                    fullPath = FormattedFullpath(currentNode.FullPath);
                    jsonData.Append("{ \"data\":\" " + currentNode.ListValueName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + currentNode.ListValueID + "\"" + "," + "\"title\":\"" + fullPath + "\" ");

                }
                if (children.Count() == 0)
                    jsonData.Append(" }} ");
                else
                {
                    if (currentNode.Parent != null)
                        jsonData.Append("}, \"children\": [ ");//create child node

                    foreach (var child in children)
                    {
                        jsonData.Append(string.Format(" {0},", BuildJSTreeViewChild(securities, child.ListValueID)));
                    }

                    if (currentNode.Parent != null)
                    {
                        jsonData.Remove(jsonData.ToString().LastIndexOf(","), 1);
                        jsonData.Append(" ]} ");
                    }

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return jsonData.ToString();


        }

        #endregion

        #region To Get Fullapth of parentlist

        /// <summary>
        /// GetFullPath
        /// </summary>
        /// <param name="listValueId"></param>
        /// <returns></returns>
        public string GetFullPath(int listValueId)
        {
            try
            {
                var parentFullPath = _db.ListValues.FirstOrDefault(a => a.ListValueID == listValueId);
                return parentFullPath.FullPath.ToString();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return string.Empty;
        }

        #endregion

        #region To get the ListvalueId By listname

        /// <summary>
        /// ListValueIDByNameSelect
        /// </summary>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public int ListValueIDByNameSelect(string fullPath)//change the method name-Deepa
        {
            tap.dat.ListValues listValue = _db.ListValues.FirstOrDefault(a => a.FullPath.Contains(fullPath));
            return listValue.ListID;
        }

        #endregion

        #region To display the Systemlist by ddl-1 or Treeview-2

        /// <summary>
        /// DisplaySystemlist
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public int DisplaySystemlist(int listId)
        {
            int displayType = Convert.ToInt32(tap.dom.hlp.Enumeration.Display.DopdownDisplay);//display list as Dropdownlist

            try
            {
                var previousListId = _db.ListValues.FirstOrDefault(a => (a.ListID == listId && a.Parent == null)).ListValueID;
                var listValue = _db.ListValues.Where(a => a.ListID == listId);
                if (listValue != null)
                {
                    foreach (var currentList in listValue)
                    {
                        if (currentList.Parent != null && currentList.Parent != previousListId)
                        {
                            displayType = Convert.ToInt32(tap.dom.hlp.Enumeration.Display.TreeviewDisplay);//display list as Treeview
                            return displayType;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return displayType;
        }

        #endregion

        #region "Get Listvalues to populate in Dropdownlist"

        /// <summary>
        /// ListvaluesToBindDDl
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="displayName"></param>
        /// <returns></returns>
        public object ListvaluesToBindDDl(int listId, string displayName)//change the method name-Deepa
        {
            try
            {
                var listValue = (from a in _db.ListValues
                                 join b in _db.Fields on a.ListID equals b.ListID
                                 where (b.ListID == listId && b.DisplayName == displayName)
                                 select new { ListValueID = a.ListValueID, ListValueName = a.ListValueName, DisplayName = b.DisplayName }).Distinct();

                return listValue.ToList();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return null;

        }

        #endregion

        #region "Update the order of nodes"

        /// <summary>
        /// ListValuesUpdate
        /// </summary>
        /// <param name="listValueId"></param>
        /// <param name="parent"></param>
        /// <param name="listValueName"></param>
        /// <returns></returns>
        public bool ListValuesUpdate(int listValueId, int parent, string listValueName, int userId)
        {
            try
            {
                tap.dat.ListValues listValues = _db.ListValues.First(a => a.ListValueID == listValueId);

                var oldname = listValues.ListValueName.Trim();
                var listid = listValues.ListID;
                listValueName = listValueName.Trim();

                if (parent == _newValue)
                {
                    if (!CheckListValueExists(listid, listValueName))
                    {
                        listValues.ListValueName = listValueName;
                        _db.SaveChanges();

                        UpdateListValueFullPath(listid, listValueId, oldname, listValueName, userId);
                    }
                    else
                        return false;
                    
                }
                else
                {
                    listValues.FullPath = GetFullPath(parent) + listValueName + @"\";
                    listValues.Parent = parent;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }

        #endregion

        #region "Update Fullpath of ListValue"

        /// <summary>
        /// UpdateListValueFullPath
        /// </summary>
        /// <param name="listId"></param>
        /// <param name="listValueId"></param>
        /// <param name="listNameOld"></param>
        /// <param name="listNameNew"></param>
        public void UpdateListValueFullPath(int listId, int listValueId, string listNameOld, string listNameNew, int userId)//change parameter name listNameNew to newListName,listNameOld to oldListName
        {
            try
            {
                using (_db)
                {
                    _currentListValues = _db.ListValues.Where(a => a.ListID == listId).ToList();

                    ParentListId(listValueId);

                    List<tap.dat.ListValues> listValues = _currentListValues.Where(a => listIds.Contains(a.ListValueID)).Distinct().ToList();

                    if (listNameOld != listNameNew)
                    {
                        foreach (var currentList in listValues)
                        {
                            currentList.FullPath = ListValueFullPath(currentList.FullPath, listNameOld.Trim(), listNameNew.Trim());
                            _db.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
                            _message = "System List '" + listNameOld + "' modified to '" + listNameNew + "'.";
                            tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Create, _message);  
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            _db = new dat.EFEntity();

            listIds.Clear();
            _currentListValues.Clear();
        }

        #endregion

        #region "Modify the full path"

        /// <summary>
        /// ListValueFullPath
        /// </summary>
        /// <param name="fullPath"></param>
        /// <param name="listNameOld"></param>
        /// <param name="listNameNew"></param>
        /// <returns></returns>
        public string ListValueFullPath(string fullPath, string oldListName, string newListName)
        {
            try
            {
                string[] fullPathValue = fullPath.Replace("\\", "#").Split('#');

                StringBuilder currentFullPath = new StringBuilder();


                foreach (var currentPath in fullPathValue)
                {
                    if (currentPath == oldListName)
                        currentFullPath.Append(newListName);
                    else
                        currentFullPath.Append(currentPath);
                    currentFullPath.Append("\\");
                }

                return currentFullPath.ToString().Remove(currentFullPath.ToString().LastIndexOf("\\")).ToString();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return string.Empty;
        }

        #endregion

        #region "Delete ListValues"

        /// <summary>
        /// ListValuesNodesDelete
        /// </summary>
        /// <param name="deleteListId"></param>
        /// <returns></returns>
        public string ListValuesNodesDelete(int listId, int userId)
        {

            string message = string.Empty;
            try
            {
                var listValue = _db.ListValues.FirstOrDefault(a => a.ListValueID == listId);
                //for (int i = 0; i <= 1; i++)
                //{
                //    var parentid = _db.ListValues.FirstOrDefault(a => a.Parent == listId);
                //    if (parentid != null)
                //    {
                //        i++;
                //        message = GetMessageIfListIsInUse(parentid.ListValueID);
                //        if (message != string.Empty)
                //            break;

                //    }
                //    else
                //        message = GetMessageIfListIsInUse(listValue.ListValueID);
                //}
                 message = GetMessageIfListIsInUse(listValue.ListValueID);

                if (message == string.Empty)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        if (i == 0)
                        {
                            message = CheckChildNodeUsage(listValue.ListValueID);
                        }
                        if (message == string.Empty)
                        {
                            i++;
                            List<tap.dat.ListValues> childListValues = _db.ListValues.Where(a => a.Parent == listValue.ListValueID).ToList();
                            if(childListValues.Count > 0)
                            message= CheckChildNodeUsage(childListValues[0].ListValueID);
                        }
                        
                        

                    }
                    
                }


                if (message == string.Empty)
                {
                    _db.DeleteObject(listValue);
                    _db.SaveChanges();

                    transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete, "Tab '" + listValue.ListValueName + "' deleted.");

                    IterateChildNodesAndDelete(listId);

                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return message;
        }

        #endregion

        #region "Delete Attachments"

        /// <summary>
        /// UserAttachTreeDelete
        /// </summary>
        /// <param name="userAttachCheckedNodes"></param>
        /// <returns></returns>
        public bool UserAttachTreeDelete(int[] userAttachCheckedNodes)//Change the method name to UserAttachmentTreeviewDelete
        {
            bool isDeleted = false;
            try
            {
                foreach (var node in userAttachCheckedNodes)
                {
                    var attachment = _db.Attachments.First(a => a.AttachmentID == node);
                    var snapcap = _db.SnapCaps.FirstOrDefault(a=>a.EventID==attachment.EventID && a.SnapCapName==attachment.DocumentName);
                    if (attachment != null)
                    {
                        _db.DeleteObject(attachment);
                        _db.SaveChanges();

                        isDeleted = true;
                    }
                    if(snapcap!=null)
                    {
                        _db.DeleteObject(snapcap);
                        _db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isDeleted;
        }

        // Added by Rajkumar.G.
        public bool UserAttachmentTreeviewDelete(int[] userAttachCheckedNodes)//Change the method name to UserAttachmentTreeviewDelete
        {
            bool isDeleted = false;
            try
            {
                foreach (var node in userAttachCheckedNodes)
                {
                    var attachment = _db.Attachments.First(a => a.AttachmentID == node);

                    if (attachment != null)
                    {
                        _db.DeleteObject(attachment);
                        _db.SaveChanges();

                        isDeleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isDeleted;
        }

        #endregion

        #region Build User Attachment folder Treeview

        /// <summary>
        /// BuildAttachmentTree
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public string BuildAttachmentTree(string eventID, int companyId)
        {
            tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

            StringBuilder jsonData = new StringBuilder("");
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                jsonData.Append("[ ");

                jsonData.Append(BuildChildAttachTree(eventId, companyId));

                jsonData.Append(" ]");

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return jsonData.ToString();
        }

        #endregion

        #region "Build User Attachment tree Child part"

        /// <summary>
        /// BuildChildAttachTree
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>

        public string BuildChildAttachTree(int eventId, int companyId)
        {
            //TODO: Use Company ID - here it is hard coded
            // Attachments hard coded in query.
            try
            {
                _folders = (from attachmentNode in _db.AttachmentFolders
                            where attachmentNode.CompanyId == companyId && attachmentNode.FullPath.StartsWith("\\\\Attachments\\")
                            select attachmentNode).ToList();

                _attachmentDetails = (from attach in _db.Attachments
                                      where attach.EventID == eventId
                                      orderby attach.AttachmentFolderID
                                      select new tap.dom.hlp.AttachmentsInfo
                                      {
                                          attachId = attach.AttachmentID,
                                          attachFolderId = attach.AttachmentFolderID,
                                          documentName = attach.DocumentName,
                                          extension = attach.Extension
                                      }).ToList();

                tap.dat.AttachmentFolders folder = _folders.First();

                return BuildFolderNode(folder);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return string.Empty;
        }
        //public string BuildChildAttachTree(int eventId, int companyId) //10 feb 14
        //{
        //    tap.dom.tab.EventTabs events = new tab.EventTabs();
        //    string attachmentTree = "";

        //    StringBuilder jsonData = new StringBuilder();
        //    jsonData.Append("{ \"data\":\"" + "Attachments" + "\"" + "," + "\"attr\": {" + "\"id\":\"");

        //    try
        //    {
        //        _attachmentDetails = (from attach in _db.Attachments
        //                              where attach.EventID == eventId
        //                              orderby attach.AttachmentFolderID
        //                              select new tap.dom.hlp.AttachmentsInfo
        //                              {
        //                                  attachId = attach.AttachmentID,
        //                                  attachFolderId = attach.AttachmentFolderID,
        //                                  documentName = attach.DocumentName,
        //                                  extension = attach.Extension
        //                              }).ToList();

        //        List<int> parentsIds = events.GetCompanyParentIds(companyId);
        //        if (parentsIds != null && parentsIds.Count() > 0)
        //        {
        //            int i = 0;
        //            foreach (int item in parentsIds)
        //            {
        //                if (i == 0)
        //                {
        //                    jsonData.Append(item + "\" ,\"rel\":\"root\"} ");
        //                    jsonData.Append(", \"children\" : [ ");
        //                }

        //                _folders = (from attachmentNode in _db.AttachmentFolders
        //                            where attachmentNode.CompanyId == item && attachmentNode.FullPath.StartsWith("\\\\Attachments\\")
        //                            select attachmentNode).ToList();

        //                tap.dat.AttachmentFolders folder1 = _folders.First();
        //                attachmentTree = attachmentTree + BuildFolderNode(folder1);
        //                jsonData.Append(BuildFolderNode(folder1));
        //                if (i + 1 < parentsIds.Count())
        //                    jsonData.Append(",");
        //                i++;
        //            }
        //        }
        //        else
        //        {
        //            int attachmentId = _db.AttachmentFolders.Where(a => a.CompanyId == companyId && a.FullPath.StartsWith("\\\\Attachments\\")).FirstOrDefault().AttachementFolderId;
        //            jsonData.Append(attachmentId + "\" ,\"rel\":\"root\"} ");
        //            jsonData.Append(", \"children\" : [ ");
        //        }

        //        _folders = (from attachmentNode in _db.AttachmentFolders
        //                    where attachmentNode.CompanyId == companyId && attachmentNode.FullPath.StartsWith("\\\\Attachments\\")
        //                    select attachmentNode).ToList();

        //        tap.dat.AttachmentFolders folder2 = _folders.First();

        //        if (parentsIds != null && parentsIds.Count() > 0 && _db.AttachmentFolders.Where(a => a.CompanyId == companyId && a.FullPath.StartsWith("\\\\Attachments\\")).ToList().Count() > 1)
        //            jsonData.Append(",");

        //        jsonData.Append(BuildFolderNode(folder2));

        //        jsonData.Append("]}");

        //        return jsonData.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }
        //    return string.Empty;
        //}

        #endregion

        #region "Build User Attachment folder part"

        /// <summary>
        /// BuildFolderNode
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        private string BuildFolderNode(tap.dat.AttachmentFolders folder)
        {
            //Hardcoded Attachments
            try
            {
                StringBuilder jsonData = new StringBuilder();

                if (folder.FolderName != "Attachments")
                    jsonData.Append("{ \"data\":\"" + folder.FolderName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + folder.AttachementFolderId + "\",\"rel\":\"folder\"} ");
                else
                    jsonData.Append("{ \"data\":\"" + "Attachments" + "\"" + "," + "\"attr\": {" + "\"id\":\"" + folder.AttachementFolderId + "\" ,\"rel\":\"root\"} ");

                List<tap.dat.AttachmentFolders> subFolders = _folders.Where(f => f.ParentId == folder.AttachementFolderId).ToList();

                bool hasFolders = subFolders.Count > 0;

                List<tap.dom.hlp.AttachmentsInfo> attachments = _attachmentDetails.Where(a => a.attachFolderId == folder.AttachementFolderId).ToList();

                bool hasAttachment = attachments.Count > 0;

                bool hasChildren = hasFolders || hasAttachment;

                jsonData.Append(hasChildren ? ", \"children\" : [ " : "");

                int childCount = subFolders.Count + attachments.Count;

                for (int index = 0; index < subFolders.Count(); index++)
                {
                    jsonData.Append(BuildFolderNode(subFolders[index]));

                    if (index + 1 < childCount)
                        jsonData.Append(",");
                }

                for (int thisAttachment = 0; thisAttachment < attachments.Count(); thisAttachment++)
                {
                    jsonData.Append(BuildAttachmentNode(attachments[thisAttachment]));

                    if (thisAttachment + subFolders.Count + 1 < childCount)
                        jsonData.Append(",");
                }

                jsonData.Append(hasChildren ? "] " : "");

                jsonData.Append("} ");

                return jsonData.ToString();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }
        //private string BuildFolderNode(tap.dat.AttachmentFolders folder)10 feb 14
        //{
        //    //Hardcoded Attachments
        //    try
        //    {
        //        StringBuilder jsonData = new StringBuilder();

        //        if (folder.FolderName != "Attachments")
        //            jsonData.Append("{ \"data\":\"" + folder.FolderName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + folder.AttachementFolderId + "\",\"rel\":\"folder\"} ");
        //        //else
        //        //jsonData.Append("{ \"data\":\"" + "Attachments" + "\"" + "," + "\"attr\": {" + "\"id\":\"" + folder.AttachementFolderId + "\" ,\"rel\":\"root\"} ");

        //        List<tap.dat.AttachmentFolders> subFolders = _folders.Where(f => f.ParentId == folder.AttachementFolderId).ToList();

        //        bool hasFolders = subFolders.Count > 0;

        //        List<tap.dom.hlp.AttachmentsInfo> attachments = _attachmentDetails.Where(a => a.attachFolderId == folder.AttachementFolderId).ToList();

        //        bool hasAttachment = attachments.Count > 0;

        //        bool hasChildren = hasFolders || hasAttachment;

        //        if (folder.FolderName != "Attachments")
        //            jsonData.Append(hasChildren ? ", \"children\" : [ " : "");

        //        int childCount = subFolders.Count + attachments.Count;

        //        for (int index = 0; index < subFolders.Count(); index++)
        //        {
        //            jsonData.Append(BuildFolderNode(subFolders[index]));

        //            if (index + 1 < childCount)
        //                jsonData.Append(",");
        //        }

        //        for (int thisAttachment = 0; thisAttachment < attachments.Count(); thisAttachment++)
        //        {
        //            jsonData.Append(BuildAttachmentNode(attachments[thisAttachment]));

        //            if (thisAttachment + subFolders.Count + 1 < childCount)
        //                jsonData.Append(",");
        //        }

        //        if (folder.FolderName != "Attachments")
        //            jsonData.Append(hasChildren ? "] " : "");

        //        if (folder.FolderName != "Attachments")
        //            jsonData.Append("} ");

        //        return jsonData.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //    return string.Empty;
        //}

        #endregion

        #region "Build User Attachment documents"

        /// <summary>
        /// BuildAttachmentNode
        /// </summary>
        /// <param name="att"></param>
        /// <returns></returns>

        private string BuildAttachmentNode(tap.dom.hlp.AttachmentsInfo att)
        {
            try
            {
                StringBuilder jsonData = new StringBuilder();

                if (att.extension == ".txt" || att.extension == ".gif" || att.extension == ".png" || att.extension == ".jpg" || att.extension == ".doc" || att.extension == ".docx" || att.extension == ".xls" || att.extension == ".xlsx" || att.extension == ".pdf" || att.extension == ".ppt" || att.extension == ".pptx" || att.extension == ".xml" || att.extension == ".zip" || att.extension == ".bat")
                    jsonData.Append("{ \"data\":\" " + att.documentName + "\", \"attr\":{\"id\":\"" + att.attachId + "\" , \"rel\":\"document\", \"extn\":\"" + att.extension + "\" }  }");
                else
                    jsonData.Append("{ \"data\":\" " + att.documentName + "\", \"attr\":{\"id\":\"" + att.attachId + "\" , \"rel\":\"document\", \"extn\":\"other\" }  }");

                return jsonData.ToString();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        #endregion

        #region "Delete Systemlist Based on FieldID"


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        //public bool ActivateOrInactivateListFail(int listId, string listType, bool updateValue)
        //{
        //    bool isDeleted = true;

        //    try
        //    {
        //        List<tap.dat.ListValues> listValues = _db.ListValues.Where(a => a.ListValueID == listId).ToList();
               
        //        if (listValues.Count() > 0)
        //        {
        //            foreach (var listValue in listValues)
        //            {
        //                listValue.Active = updateValue;
        //                _db.SaveChanges();
        //                IterateChildNodesAndInActive(listId, updateValue);
        //            }
        //        }

        //        if (listType == "OriginParentList")
        //        {
        //             tap.dat.SystemLists systemList = _db.SystemLists.FirstOrDefault(a => a.ListID == listId);

        //            if (systemList != null)
        //            {
        //                systemList.Active = updateValue;
        //                _db.SaveChanges();
        //                _message = "System List '" + systemList.ListName + "' Inactivated";
        //            }

        //            transaction.Transaction.SaveTransaction(null, SessionService.UserId, null,
        //                Enumeration.TransactionCategory.Delete, _message);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //        isDeleted = false;
        //    }
        //    return isDeleted;
        //}

        public string ActivateOrInactivateListFail(int listId, string listType, bool updateValue, int userId)
        {
            string result = string.Empty;

            try
            {
                List<tap.dat.ListValues> listValues = _db.ListValues.Where(a => a.ListValueID == listId).ToList();

                if (listValues.Count() > 0)
                {
                    foreach (var listValue in listValues)
                    {
                        listValue.Active = updateValue;
                        _db.SaveChanges();
                        IterateChildNodesAndInActive(listId, updateValue);
                    }
                }

                if (listType == "OriginParentList")
                {
                    tap.dat.SystemLists systemList = _db.SystemLists.FirstOrDefault(a => a.ListID == listId);

                    if (systemList != null)
                    {
                        systemList.Active = updateValue;
                        _db.SaveChanges();

                    }

                    if (updateValue)
                        _message = systemList.ListName + " List activated";
                    else
                        _message = systemList.ListName + " List deactivated";

                    transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete, _message);
                }
                else
                {
                    string listName = _db.ListValues.FirstOrDefault(x => x.ListValueID == listId).ListValueName;
                    if (updateValue)
                        _message = listName + " List activated";
                    else
                        _message = listName + " List deactivated";
                }
                result = _message;
            }

            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        /// <summary>
        ///  DeleteSystemListByFieldID(int listid)
        /// </summary>
        /// <param name="listId"></param>
        public string DeleteSystemListByField(int listId, int userId)
        {
            
            string message = string.Empty;

            try
            {
                IList<tap.dat.Fields> fields = _db.Fields.Where(a => a.ListID == listId).ToList();

                if (fields.Count == 0)
                {
                    
                    List<tap.dat.ListValues> listValues = _db.ListValues.Where(a => a.ListID == listId).ToList();
                    tap.dat.SystemLists systemList = _db.SystemLists.FirstOrDefault(a => a.ListID == listId);

                    if (listValues.Count() > 0)
                    {
                        foreach (var listValue in listValues)
                        {
                            _db.DeleteObject(listValue);
                            _db.SaveChanges();
                            _message = "List Value '" + listValue.ListValueName + "' deleted";
                            IterateChildNodesAndDelete(listId);
                        }
                    }

                    if (systemList != null)
                    {
                        _db.DeleteObject(systemList);
                        _db.SaveChanges();
                        _message = "System List '" + systemList.ListName + "' deleted";
                    }
                    transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete, _message);  

                }
                else
                {
                    var tabNames = fields.Select(x => x.Tabs.TabName).Distinct().Select(x => new { TabName = x }).ToList();

                    message = "This list can not be deleted as it is being used by ";
                    foreach (var tab in tabNames)
                    {
                        message = message + tab.TabName;
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return message;
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listValueID"></param>
        /// <returns></returns>
        public string CheckChildNodeUsage(int listValueID)
        {
            List<tap.dat.ListValues> childListValues = _db.ListValues.Where(a => a.Parent == listValueID).ToList();
            string message = string.Empty;

            foreach (var childList in childListValues)
            {
                message = GetMessageIfListIsInUse(childList.ListValueID);
             
                if (message != string.Empty)
                {
                    return message;
                }

                CheckChildNodeUsage(childList.ListValueID);
            }

            return message;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string GetMessageIfListIsInUse(int listId)
        {
            string listItemId = Convert.ToString(listId);
            string message = string.Empty;

            //Check if used as localtion
            bool locationUsed = _db.Events.Where(a => a.Location == listItemId).ToList().Count > 0 ? true : false;
            //Check if used as classification
            bool classificationUsed = _db.FieldListValues.Where(a => a.ValueId == listId).ToList().Count > 0 ? true : false;
            //Check if used in any cutsom subtab
            bool customTabUsed = _db.FieldValues.Where(a => a.FieldValue == listItemId).ToList().Count > 0 ? true : false;

            bool isUsed = locationUsed || classificationUsed || customTabUsed;

            if (isUsed)
            {
                message = (locationUsed)
                    ? "This Location cannot be deleted. You can only inactivate this Location"
                    : (classificationUsed)
                        ? "This Classification cannot be deleted. You can only inactivate this Classification"
                        : (customTabUsed) ? "This is being used by Custom Tab '" + GetTabName(listItemId) + 
                                            "'  and cannot be deleted. Would you like to inactivate this item instead?"
                        : string.Empty;

                return message;

            }

            return message;
        }

        public string GetTabName(string listValueId)
        {
            
            return _db.FieldValues.Where(a => a.FieldValue == listValueId).FirstOrDefault().Fields.Tabs.TabName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listId"></param>
        public void IterateChildNodesAndDelete(int listId)
        {
            List<tap.dat.ListValues> childListValues = _db.ListValues.Where(a => a.Parent == listId).ToList();
            foreach (var childList in childListValues)
            {
                _db.DeleteObject(childList);
                _db.SaveChanges();
                IterateChildNodesAndDelete(childList.ListValueID);
            }
        }

        public void IterateChildNodesAndInActive(int listId, bool updateValue)
        {
            List<tap.dat.ListValues> childListValues = _db.ListValues.Where(a => a.Parent == listId).ToList();
            foreach (var childList in childListValues)
            {
                childList.Active = updateValue;
                _db.SaveChanges();
                IterateChildNodesAndInActive(childList.ListValueID, updateValue);
            }
        }

        
        #region "To Get the Fullpath based on ListValueId"

        /// <summary>
        /// SelectLocation
        /// </summary>
        /// <param name="systemListIds"></param>
        /// <param name="eventId"></param>
        /// <param name="systemFieldId"></param>
        /// <returns></returns>
        public object SelectLocation(string systemListIds, int eventId, int systemFieldId)
        {

            List<int> listIds = new List<int>();
            List<string> fullPathList = new List<string>();
            try
            {

                if (systemListIds == ((int)Enumeration.Types.NewInsert).ToString())//Location
                {
                    var systemListId = _db.FieldListValues.Where(a => (a.EventId == eventId && a.SystemFieldId == systemFieldId));

                    foreach (var Ids in systemListId)
                    {
                        listIds.Add(Convert.ToInt32(Ids.ValueId));
                    }

                }
                if (systemListIds == ((int)Enumeration.Types.New).ToString())//from home page grid and create investigation
                {
                    var systemListId = _db.Events.FirstOrDefault(a => a.EventID == eventId);

                    listIds.Add(Convert.ToInt32(systemListId.Location));

                }
                else if (systemListIds != "0")//from Incident page
                {
                    var splitListIds = systemListIds.Split(',');

                    for (int i = 0; i < splitListIds.Length; i++)
                    {
                        listIds.Add(Convert.ToInt32(splitListIds[i]));
                    }
                }
                var fullPath = from a in _db.ListValues
                               where listIds.Contains(a.ListValueID)
                               select new { FullPath = a.FullPath, ListValueID = a.ListValueID };

                foreach (var x in fullPath)
                {
                    fullPathList.Add(FormattedFullpath(x.FullPath));
                    fullPathList.Add(x.ListValueID.ToString());
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return fullPathList;
        }



        #endregion

        #region "Get the Location path list"

        /// <summary>
        /// SelectLocationPath()
        /// Description- Get the Location path list
        /// </summary>
        /// <param name="sSystemListIds"></param>
        /// <param name="iEventId"></param>
        /// <param name="sSystemFieldId"></param>
        /// <returns></returns>

        public List<string> SelectLocationPath(string systemListIds, int eventId, int systemFieldId)
        {
            List<int> newLists = new List<int>();
            List<string> fullPathLists = new List<string>();

            try
            {
                if (systemListIds == "0")//from home page grid
                {
                    var systemIds = _db.FieldListValues.Where(a => (a.EventId == eventId && a.SystemFieldId == systemFieldId));

                    foreach (var Ids in systemIds)
                    {
                        newLists.Add(Convert.ToInt32(Ids.ValueId));
                    }
                }
                else if (systemListIds != "0")//from Incident page
                {
                    var splitLocationIds = systemListIds.Split(',');


                    for (int i = CommonOperation.ZERO; i < splitLocationIds.Length; i++)
                    {
                        newLists.Add(Convert.ToInt32(splitLocationIds[i]));
                    }
                }

                var locationFullPath = from a in _db.ListValues
                                       where newLists.Contains(a.ListValueID)
                                       orderby a.FullPath
                                       select a.FullPath;

                foreach (var location in locationFullPath)
                {
                    fullPathLists.Add(FormattedFullpath(location));
                }


                //foreach (var location in locationFullPath)
                //{
                //    fullPathLists.Add(FormattedFullpath(location));
                //}

                //foreach (var location in locationFullPath)
                //{
                //    foreach (var listItem in fullPathLists)
                //    {
                //        if (listItem.Contains(FormattedFullpath(location)) && !listItem.Equals(FormattedFullpath(location)))
                //        {
                //            fullPathLists.Remove(FormattedFullpath(location));
                //            break;
                //        }
                //    }
                //}
            }


            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return fullPathLists;
        }

        #endregion

        #region To get the list value name by the list id
        /// <summary>
        /// GetListValueName()
        /// Description- Get the List value name
        /// </summary>
        /// <param name="listId">List id for geting the List value name</param>
        /// <returns>List value name from the list value table</returns>
        public string GetListValueName(int listId)
        {
            string listValueName = string.Empty;

            try
            {
                listValueName = _db.ListValues.Where(a => a.ListID == listId && a.Parent == null).First().ListValueName;
            }
            catch (Exception ex)
            {
                ErrorLog.LogException(ex);
            }

            return listValueName;
        }
        #endregion


        #region "Get the Systemlist count"

        /// <summary>
        /// SystemlistCount
        /// </summary>
        /// No arguements
        /// <returns></returns>
        public int SystemlistCount()//change method name to SystemListCount-Deepa
        {
            int systemListCount = 0;
            try
            {
                systemListCount = _db.SystemLists.Where(a => a.Active == true).Count();
                return systemListCount;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return systemListCount;
        }

        #endregion

        #region "Get Listid"

        /// <summary>
        ///  ParentListId
        /// </summary>
        /// <param name="listValueId"></param>
        public void ParentListId(int listValueId)
        {
            try
            {
                listIds.Add(listValueId);

                var listValueChild = _currentListValues.Where(a => a.Parent == listValueId);

                if (listValueChild != null && listValueChild.Count() > 0)
                {
                    foreach (var thisListValueChild in listValueChild)
                    {
                        ParentListId(thisListValueChild.ListValueID);
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        #endregion
    
    }
}


