﻿#region Header Information
    /********************************************** 
    * CLASS NAME 	- SiteSettings
    * Project Name  - TapRoot
    * Module Name 	- 
    * Purpose 	    - This class is used to get the sitetting
    * Created By 	- Mindfire
    * Created On 	- 24/09/2012
    * Modified On 	- 
    * Modified By 	- 
    **********************************************/
#endregion
using System;
using System.Linq;
using tap.dom.hlp;
using tap.dom.gen;
using System.Text.RegularExpressions;
using System.Globalization;

namespace tap.dom.adm
{
    public class SiteSettings
    {
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer(); 

        #region "Get the time format"
        /// <summary>
        /// GetTimeFormat()
        /// Description - Get the Time format.
        /// </summary>
        /// <param name="iCompanyId">Time format according to company id</param>
        /// <returns>boolean</returns>
        public bool GetTimeFormat(int? companyId = 0)
        {
            bool timeFormat = false;
            //if (SessionService.CompanyId != 0)
            //{
            //Decrypt the value and pass                
            //int _companyId = Convert.ToInt32(companyId) == 0 ? Convert.ToInt32(tap.dom.gen.SessionService.CompanyId) : Convert.ToInt32(companyId);

            try
            {
                timeFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId).IsTwelveHourFormat;
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            //}
            return timeFormat;
        }
        #endregion

        #region To Get DateFormat
        /// <summary>
        /// GetDateFormat()
        /// Description-Get Date Format
        /// </summary>
        /// <param name="iCompanyId"></param>
        /// <returns></returns>
        public string GetDateFormat()
        {
            //string dateFormat = "";
            //if (SessionService.CompanyId != 0)
            //{
            //    //Decrypt the value and pass
            //    int companyId = Convert.ToInt32(tap.dom.gen.SessionService.CompanyId);
              
            //    try
            //    {
            //        //dateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId).DateFormat; //nw1
            //        //dateFormat = SessionService.CurrentCulture.DateTimeFormat.ShortDatePattern;
            //        //if (String.IsNullOrEmpty(dateFormat))
            //            dateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId).DateFormat;
            //    }
            //    catch (Exception ex)
            //    {
            //        dateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern;
            //        throw ex;
            //    }
            //}
            return CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern;
        }


        public int GetApplicationTimeOut(int companyId)
        {
            //tap.dat.SiteSettings siteSettings = new tap.dat.SiteSettings();
            //siteSettings = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId);

            tap.dat.SiteSettings organizationSiteSetting = new tap.dat.SiteSettings();

            tap.dom.adm.Company companyData = new tap.dom.adm.Company();
            int organizationId = companyData.OrganizationId();

            organizationSiteSetting = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == organizationId);

            int applicationTimeOut = 0;

            if (organizationSiteSetting != null)
            {
                if (organizationSiteSetting.ApplicationTimeOut != null)
                {
                    applicationTimeOut = Convert.ToInt32(organizationSiteSetting.ApplicationTimeOut);
                }
            }
            return applicationTimeOut;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public string GetSiteSettingData(int companyId)
        {

            try
            {

                if(companyId>0)//if (SessionService.CompanyId != 0)
                {
                   // int companyId = Convert.ToInt32(SessionService.CompanyId);
                    var systemListsDetails = from siteSetting in _db.SiteSettings
                        where siteSetting.CompanyID == companyId
                        select new
                        {
                            SiteSettingID = siteSetting.SiteSettingID,
                            DateFormat = siteSetting.DateFormat,
                            TimeFormat = siteSetting.IsTwelveHourFormat,
                            ApplicationTimeOut = siteSetting.ApplicationTimeOut,
                            PasswordPolicyType = siteSetting.PasswordPolicyId,
                            PasswordValidity = siteSetting.PasswordExpiration
                        };


                    return _efSerializer.EFSerialize(systemListsDetails);
                }

                return string.Empty;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }
        
        #endregion


        public void SaveSiteSettingData(string dateFormat, bool isTwelveHourFormat, string applicationTimeOut, int companyId, int passwordPolicy, int passwordExpiration)
        {
            if(companyId>0)
            {
                tap.dat.SiteSettings siteSettings = new tap.dat.SiteSettings();
                siteSettings = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId);
                siteSettings.DateFormat = dateFormat;
                siteSettings.IsTwelveHourFormat = isTwelveHourFormat;
                siteSettings.ApplicationTimeOut = (applicationTimeOut.Trim() == string.Empty) ? 0: Convert.ToInt32(applicationTimeOut);

                siteSettings.PasswordPolicyId = (passwordPolicy > 0) ? passwordPolicy : 0;
                siteSettings.PasswordExpiration = passwordExpiration;

                _db.SaveChanges();
            }
        }

        //Get the date format of the company
        public string DateFormatGet(int companyId)
        {
           string dateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == companyId).DateFormat;
           return dateFormat;

        }
    }
}
