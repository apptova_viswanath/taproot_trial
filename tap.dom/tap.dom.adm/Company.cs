﻿/*Company page methods*/

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using tap.dom.hlp;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing;
using tap.dom.usr;
using tap.dom.gen;

#endregion

namespace tap.dom.adm
{
    public class Company
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        int _companyBannerLogo = 1;
        int _companyReportLogo = 3;
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        string fileName = string.Empty;

        public string _isSU = ConfigurationManager.AppSettings["IsSU"];

        #region For  Insert/Update company Information
        public void SaveCompany(int companyId, string companyName, string phoneNumber, string address, string email, string webSite, bool active, byte[] logo)
        {
            //logo-pending
            tap.dat.Companies company = null;
            try
            {
                if (companyId == Convert.ToInt32(Enumeration.Types.New))
                    company = new tap.dat.Companies();
                else
                    company = _db.Companies.First(x => x.CompanyID == companyId);

                company.CompanyName = companyName;
                company.PhoneNo = phoneNumber;
                company.Address = address;
                company.Email = email;
                company.Website = webSite;
                company.Active = active;
                //company.Logo = logo;

                if (companyId == Convert.ToInt32(Enumeration.Types.New))
                    _db.AddToCompanies(company);

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region For Deleteing Company Information
        public void DeleteCompanyID(int companyId)
        {
            try
            {
                var company = _db.Companies.First(x => x.CompanyID == companyId);
                _db.DeleteObject(company);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region To Get CompanyDetail By companyID
        public tap.dat.Companies GetCompanyByID(int companyId)
        {
            var company = _db.Companies.First(x => x.CompanyID == companyId);
            return company;
        }

        #endregion

        #region To Get the company as List
        //Currently not using may use in future
        public List<tap.dat.Companies> GetCompanyList()
        {
            var companyList = _db.Companies.Where(x => x.Active == true);
            return companyList.ToList();
        }

        #endregion

        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats 
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec 
            for (int i = 0; i < codecs.Length; i++)
                if (codecs[i].MimeType == mimeType)
                    return codecs[i];
            return null;
        }
        // resize image with max height
        public Bitmap ProportionallyResizeBitmapByHeight(Bitmap imgToResize, int height)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float scale = 0;

            scale = (height / (float)sourceHeight);

            int destWidth = (int)(sourceWidth * scale);
            int destHeight = (int)(sourceHeight * scale);

            Bitmap result = new Bitmap(destWidth, destHeight);
            result.SetResolution(imgToResize.HorizontalResolution, imgToResize.VerticalResolution);
            Graphics g = Graphics.FromImage(result);
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return result;
        }
        /// <summary>
        /// Save company banner and report logo
        /// </summary>
        /// <param name="headerBannerLogo"> banner logo of company uploaded</param>
        /// <param name="reportLogo">report logo of company uploaded</param>
        public void SaveLogo(int companyId, HttpPostedFile headerBannerLogo, HttpPostedFile reportLogo)//, HttpPostedFile companyLogo
        {

            try
            {
                //Get company by companyid
                dat.Companies company = new tap.dat.Companies();
                company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

                //Set column values and save to database
                if (headerBannerLogo != null && headerBannerLogo.ContentLength > 0)
                {
                    Bitmap img = new Bitmap(headerBannerLogo.InputStream);
                    //resizing image maintainign ascept raio
                   
                                       
                    int quality = 100;
                    string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                    string newFilePath = tempPath + "BannerLogo.jpg";

                    //save the new image with the proper format
                    string saveAs = string.Format("{0}", newFilePath);

                    EncoderParameter qualityParam =
                        new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
                    // Jpeg image codec 
                    ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

                    EncoderParameters encoderParams = new EncoderParameters(1);
                    encoderParams.Param[0] = qualityParam;
                    //if image height more that 125px
                    if (img.Height > 125)
                    {
                        Bitmap result = ProportionallyResizeBitmapByHeight(img, 125);
                        result.Save(newFilePath, jpegCodec, encoderParams);
                    }
                    else
                        img.Save(newFilePath, jpegCodec, encoderParams);

                    company.BannerLogo = File.ReadAllBytes(newFilePath);

                    System.IO.File.Delete(newFilePath);
                }

               
                //resize the image if it is moe than 100/100------------------------------move this code to common file and reuse it everywhere
                if(reportLogo != null && reportLogo.ContentLength > 0 && reportLogo.FileName!="")
                {

                    //take temp path from server
                    string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                    //check directory exists or not -if not create it.
                    if (Directory.Exists(tempPath) == false)
                    {
                        Directory.CreateDirectory(tempPath);
                    }

                    string fileExtension = reportLogo.FileName.Substring(reportLogo.FileName.LastIndexOf("."));                  

                    //create file name to save it in temp path
                    string fileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + fileExtension;

                    //save the file path 
                    string orginalPath = tempPath + fileName;

                    //save file path for orginal image
                    reportLogo.SaveAs(orginalPath);
                 
                    System.Drawing.Image img = System.Drawing.Image.FromFile(orginalPath);
                   

                    if (img.Width > 100 || img.Height > 100)
                    {
                        //create file name to save it in temp path for resize image
                        string newFileName = DateTime.Now.ToString("MMddyyyy_HHmmss") + "_Temp" + fileExtension;

                        //save the new file path to resizing the image
                        string newFilePath = tempPath + newFileName;

                         tap.dom.gen.CustomReport resize = new tap.dom.gen.CustomReport();

                         resize.ResizeImage(orginalPath, tap.dom.gen.CustomReport.ImageSizes.size_100x100, fileName, newFilePath);                    

                        img.Dispose();
                        company.ReportLogo = File.ReadAllBytes(newFilePath);

                        System.IO.File.Delete(orginalPath);
                        System.IO.File.Delete(newFilePath);
                    }
                    else
                    {
                        company.ReportLogo = gen.Common.GetBinaryData(reportLogo);
                        img.Dispose();
                    }

                }   
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete logo from comapny table
        /// </summary>
        /// <param name="companyId">company id</param>
        /// <param name="logoType">logo type(banner logo, company logo, report logo) </param>
        public void DeleteLogo(int companyId, int logoType)
        {

            try
            {
                //Get company by companyid
                dat.Companies company = new tap.dat.Companies();
                company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

                if (logoType.Equals(_companyBannerLogo))
                {
                    company.BannerLogo = null;
                }
                else if (logoType.Equals(_companyReportLogo))
                {
                    company.ReportLogo = null;
                }

                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get banner logo for the current company
        /// </summary>
        /// <param name="companyId">id of company</param>
        /// <returns></returns>
        public byte[] GetBannerLogo(int companyId)
        {

            try
            {

                //Get the company by company id
                tap.dat.Companies company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

                if (company != null)
                {
                    return company.BannerLogo;
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        ///// <summary>
        ///// Get company logo for the current company
        ///// </summary>
        ///// <param name="companyId">id of company</param>
        ///// <returns></returns>
        //public byte[] GetCompanyLogo(int companyId)
        //{
        //    try
        //    {
        //        //Get the company by company id
        //        tap.dat.Companies company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

        //        if (company != null)
        //        {
        //            return company.Logo;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //    return null;
        //}

        /// <summary>
        /// Get report logo for the current company
        /// </summary>
        /// <param name="companyId">id of company</param>
        /// <returns></returns>
        public byte[] GetReportLogo(int companyId)
        {
            try
            {
                //Get the company by company id
                tap.dat.Companies company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

                if (company != null)
                {
                    return company.ReportLogo;
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        //save company details.
        public string SaveCompanyDetails(int companyId, string companyName, string phoneNumber, string website, string address, string email, bool adsecurity,
            string dateFormat, bool isTwelveHourFormat, string applicationTimeOut, string subscriptionStart, string subscriptionEnd, int passwordPolicy, int passwordExpiration)
        {
            string result = string.Empty;
            string message = string.Empty;
            try
            {
                ////Check for email availability.
                //if (email !="" && EmailAvailableCheck(companyId, email))
                //{
                //    message = email + " already exists";
                //    return message;
                //}

                //Get company by companyid
                dat.Companies company = new tap.dat.Companies();
                company = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault();

                if (phoneNumber != null && phoneNumber.Length > 0)
                    company.PhoneNo = phoneNumber;
                if (website != null && website.Length > 0)
                    company.Website = website;
                if (address != null && address.Length > 0)
                    company.Address = address;
                if (companyName != null && companyName.Length > 0)
                    company.CompanyName = companyName;
                if (email != null && email.Length > 0 && IsValidEmail(email))
                    company.Email = email;

                company.CheckADSecurity = adsecurity;
                DateFormat dt = new DateFormat();
                if (subscriptionStart != null && subscriptionStart.Length > 0 && subscriptionStart != "undefined aN NaN" && subscriptionStart != "aN-aN-aN" && subscriptionStart != "aN/aN/aN")
                {



                    company.SubscriptionStart = dt.fromDatefrmatToDate(subscriptionStart, companyId);
                   
                }

                if (subscriptionEnd != null && subscriptionEnd.Length > 0 && subscriptionEnd != "undefined aN NaN" && subscriptionEnd != "aN-aN-aN" && subscriptionEnd != "aN/aN/aN")
                {




                    company.SubscriptionEnd = dt.fromDatefrmatToDate(subscriptionEnd, companyId);
                 
                }

                result = "Saved Successfully";
                _db.SaveChanges();

                tap.dom.adm.SiteSettings setting = new tap.dom.adm.SiteSettings();

                setting.SaveSiteSettingData(dateFormat, isTwelveHourFormat, applicationTimeOut, companyId, passwordPolicy, passwordExpiration);
            }
            catch (Exception ex)
            {
                result = "Data updation failed.";
                tap.dom.hlp.ErrorLog.LogException(ex);
                return result;
            }
            return result;
        }

        public string GetCompanyDetails(int companyID)
        {
            var company = from a in _db.Companies
                          where a.CompanyID == companyID
                          select new
                          {
                              CompanyID = a.CompanyID,
                              CompanyName = a.CompanyName,
                              PhoneNumber = a.PhoneNo,
                              Website = a.Website,
                              Address = a.Address,
                              Email = a.Email,
                              ReportLogo = a.ReportLogo,
                              BannerLogo = a.BannerLogo,
                              Active = a.Active,
                              ParentId = a.ParentId,
                              CheckADSecurity = a.CheckADSecurity,
                              SubscriptionStart = a.SubscriptionStart,
                              SubscriptionEnd = a.SubscriptionEnd
                          };
            return _efSerializer.EFSerialize(company);
        }
        public tap.dat.Companies GetCompanyData(int companyID)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();

            tap.dat.Companies companyDetails = _db.Companies.FirstOrDefault(a => a.CompanyID == companyID);

            return companyDetails;
        }

        /// <summary>
        /// Check for Email availability.
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailAvailableCheck(int companyId, string email)
        {
            tap.dat.Users emailUser = null;
            tap.dat.Companies emailComapany = null;
            try
            {
                emailUser = _db.Users.FirstOrDefault(a=>a.Email.ToLower() == email.ToLower());
                emailComapany = _db.Companies.FirstOrDefault(a => a.CompanyID != companyId && a.Email.ToLower() == email.ToLower());    
            }
            catch(Exception ex){
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            if (emailUser == null && emailComapany == null)
                return false;

            return true;
        }

        //check email validation
        public bool IsValidEmail(string email)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                return false;
            }
        }
        public bool isCompanySetUp()
        {
            IList<dat.Companies> companies = new List<dat.Companies>();
            companies = _db.Companies.ToList();
            return (companies.Count > 0);
        }

        public bool isParentCompany(int companyId)
        {
            IList<dat.Companies> companies = new List<dat.Companies>();
            companies = _db.Companies.Where(x => x.ParentId == companyId).ToList();
            return (companies.Count > 0);
        }

        public int OrganizationId()
        {
            return _db.Companies.FirstOrDefault(x => x.ParentId == null && x.Active == true).CompanyID;
        }

        public bool isSUCompany(int companyID)
        {

            bool result = false;

            try
            {               
                dat.Companies company = new tap.dat.Companies();
                company = _db.Companies.Where(x => x.CompanyID == companyID).FirstOrDefault();
                if (company != null && company.ParentId == null && _isSU != null && _isSU.Equals("1"))
                    result = true;
            }
            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
        public bool isSUDivision(int companyID)
        {
            bool result = false;

            try
            {

            dat.Companies company = new tap.dat.Companies();
            company = _db.Companies.Where(x => x.CompanyID == companyID).FirstOrDefault();
            //if (company != null && company.isSU == true && company.ParentId != null)
            //    result = true;

            if (company != null && company.ParentId != null && company.ParentId != 0 && _isSU != null && _isSU.Equals("1"))
                result = true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        // Company subscription expire days
        public int SubscriptionExpireDays(int companyID)
        {
            int days = 0;
            try
            {
                if (!isSUDivision(companyID))
                    return days;

                if (companyID.Equals(0))
                    return days;

                tap.dat.Companies company = new dat.Companies();

                company = _db.Companies.Where(a => a.CompanyID == companyID && (a.ParentId != null || a.ParentId != 0)).FirstOrDefault();
                if (company == null)
                    return days;

                DateTime today = DateTime.Now;

                if (company.SubscriptionEnd != null)
                {
                    days = (Convert.ToDateTime(company.SubscriptionEnd).Date - today.Date).Days;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return days;
        }

        //Check Company subscription time
        public string CheckSubscriptionTime(string userName)
        {
            int days = 0;
            int companyId = 0;
            string result = null;
            try
            {
                if (userName == null || userName.Length == 0)
                    return null;

                var userDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);
                if (userDetails == null)
                    return null;

                int.TryParse(userDetails.CompanyID.ToString(), out companyId);

                if (isSUDivision(companyId))
                {

                    tap.dat.Companies company = new dat.Companies();

                    company = _db.Companies.Where(a => a.CompanyID == companyId && (a.ParentId != null || a.ParentId != 0)).FirstOrDefault();
                    if (company == null)
                        return null;

                    DateTime today = DateTime.Now;

                    if (company.SubscriptionEnd != null)
                    {
                        days = (Convert.ToDateTime(company.SubscriptionEnd).Date - today.Date).Days;
                    }

                    if (days < 0)
                        result = "Subscription to the TapRooT® software has expired. Click <u><a href='http://www.taproot.com/store/taproot-single-user-licence-renewal.html'>here</a></u> to renew your subscription.";
                        //result = "Your subscription to the TapRooT® software has expired. Please contact <a href='mailto:support@taproot.com'>support@taproot.com</a>";

                    days = 0;
                    days = (Convert.ToDateTime(company.SubscriptionStart).Date - today.Date).Days;

                    if (days > 0)
                        result = "Subscription to the TapRooT® software will be activated on " + Convert.ToDateTime(company.SubscriptionStart).ToString("dd MMM yyyy") + ".";

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Company.cs", "CheckSubscriptionTime");
            }
            return result;
        }

    }
}
