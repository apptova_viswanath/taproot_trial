﻿//---------------------------------------------------------------------------------------------
// File Name 	: Module.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Module entity
//---------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace tap.dom.adm
{
    public class Module
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        #region "To get all the list of Active modules."

        /// <summary>
        /// GetModuleList
        /// Get all modules
        /// </summary>
        /// <returns></returns>
        public List<tap.dat.Modules> GetModuleList()
        {
            List<tap.dat.Modules> moduleList = null;

            try {

                 moduleList = (from x in _db.Modules
                               where x.Active == true
                               select x).ToList();

            }
            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            
            return moduleList;
        }

        #endregion
    }
}
