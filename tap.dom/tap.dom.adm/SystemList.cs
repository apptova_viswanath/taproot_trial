﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.adm
{
    public  class SystemList
    {
        #region "Global variables"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer(); 
        #endregion

        # region "To get The System List"

        /// <summary>
        /// SystemListSelect
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string SystemListSelect(string page, string rows, string sidx, string sord, int companyId)
        {
            try
            {

                var systemListsDetails = _db.SystemLists.Where(d => d.CompanyID == companyId).OrderBy(d => d.ListName);

                //for sorting
                string sortByColumnName = sidx ?? "ListName";
                string sortDirection = sord ?? "desc";

                //Sort by ascending or descending for ListName or Description.
                systemListsDetails = sortByColumnName == "ListName" ? sortDirection.Equals("desc") ? systemListsDetails.OrderByDescending(e => e.ListName)
                    : systemListsDetails.OrderBy(e => e.ListName) : sortDirection.Equals("desc") ? systemListsDetails.OrderByDescending(e => e.Description)
                        : systemListsDetails.OrderBy(e => e.Description);

                var data = systemListsDetails.ToList();

                //pagination
                int totalRecords = 0;

                var currentPazeSize = rows ?? "15";
                int pageSize = int.Parse(currentPazeSize);

                if (data.Count != 0)
                    totalRecords = data.Count();

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var thisPage = page ?? "1";
                int currentPage = int.Parse(thisPage);

                if (currentPage > 1 && data.Count != 0)
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                var jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = (
                        from s in data
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = s.ListID.ToString(),
                            cell = new string[] {                            
                            s.ListName, 
                            s.Description,
                            s.IsSystemList.ToString(),
                            s.Active.ToString()
                                    
                        }
                        }).ToArray()
                };

                return _efSerializer.EFSerialize(jsonData);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }

        # endregion

        #region "Method to display System List Items"

        /// <summary>
        /// BuildSystemJSTreeView
        /// </summary>
        /// <param name="listId"></param>
        /// <returns></returns>
        public string BuildSystemJSTreeView(int listId, int companyId)
        {
            List<tap.dat.ListValues> securities = new List<dat.ListValues>();
            StringBuilder jsonSystemListData = new StringBuilder("");
            try
            {
                securities = _db.ListValues.Where(a => a.ListID == listId).OrderBy(a => a.FullPath).ToList();
                //securities = _db.ListValues.Where(a => a.ListID == listId).OrderBy(a => a.FullPath).ToList();

                var parents = from child in securities
                              where child.Parent == null
                              select child;
                if (parents.Count() != 0)
                {
                    jsonSystemListData.Append("[ ");
                    foreach (var parent in parents)
                    {
                        jsonSystemListData.Append(BuildSystemJSTreeViewChild(securities, parent.ListValueID));//build child nodes
                        jsonSystemListData.Append(" , ");
                    }
                    jsonSystemListData.Append(" ], ");
                    jsonSystemListData.Remove(jsonSystemListData.ToString().LastIndexOf(", "), 2);
                    jsonSystemListData.Remove(jsonSystemListData.ToString().LastIndexOf(", "), 2);
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return jsonSystemListData.ToString();
        }

        #endregion

        #region "Method to display System List Child Items"

        /// <summary>
        /// BuildSystemJSTreeViewChild
        /// </summary>
        /// <param name="securities"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        public string BuildSystemJSTreeViewChild(List<tap.dat.ListValues> securities, int parentID)
        {
            StringBuilder jsonSystemListChildData = new StringBuilder();
            try
            {
                var thisNode = (from child in securities where child.ListValueID == parentID select child).First();

                var children = from child in securities
                               where child.Parent == parentID
                               select child;

                jsonSystemListChildData.Append("{ \"data\":\" " + thisNode.ListValueName + "\"" + "," +
                    "\"attr\": {" + "\"id\":\"" + thisNode.ListValueID + "\" " + "," + "\"active\":\"" + thisNode.Active + "\" ");

                if (children.Count() == 0)
                {
                    jsonSystemListChildData.Append(" }} ");
                }
                else
                {
                    jsonSystemListChildData.Append("}, \"children\": [ ");//create child node
                    foreach (var child in children)
                    {
                        jsonSystemListChildData.Append(string.Format(" {0},", BuildSystemJSTreeViewChild(securities, child.ListValueID)));
                    }
                    jsonSystemListChildData.Remove(jsonSystemListChildData.ToString().LastIndexOf(","), 1);
                    jsonSystemListChildData.Append(" ]} ");

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return jsonSystemListChildData.ToString();

        }

        #endregion

    }
}
