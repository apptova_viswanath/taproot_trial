﻿/*Generic methods*/

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
using System.Web;
using tap.dom.gen;
#endregion

namespace tap.dom.adm
{
    public class CommonOperation
    {
        #region "Variables"
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.adm.SiteSettings siteSetting = new tap.dom.adm.SiteSettings();
        #endregion

        #region Constants

        public const string RADIOBUTTON = "Radio Button";
        public const string YES = "Yes";
        public const string NO = "No";        
        public const int ZERO = 0;
        public const int ONE = 1;
        public const string Location = "Location";

        #endregion       

        #region "Select Data List"
        public List<tap.dat.SystemLists> DataListSelect(int companyId)
        {
            List<tap.dat.SystemLists> dataListsActive = null;
            try
            {
                dataListsActive = _db.SystemLists.Where(d => d.CompanyID == companyId
                   && d.Active == true).OrderBy(d => d.ListName).ToList();

                //tap.dom.tab.EventTabs eventTabs = new tap.dom.tab.EventTabs();
                //List<int> parents = eventTabs.GetCompanyParentIds(companyId);

                // if (parents != null && parents.Count() > 0)
                // {
                //     foreach (int id in parents)
                //     {
                //         List<tap.dat.SystemLists> dataListsActiveLocal = null;

                //         dataListsActiveLocal = _db.SystemLists.Where(d => d.CompanyID == id && d.ListName.ToLower() != Location.ToLower()//&& d.IsSystemList == false
                //                    && d.Active == true).OrderBy(d => d.ListName).ToList();

                //         if (dataListsActive != null)
                //             dataListsActive = dataListsActive.Union(dataListsActiveLocal).ToList();
                //         else
                //             dataListsActive = dataListsActiveLocal.ToList();
                //     }
                // }

                // List<tap.dat.SystemLists> currentdataLists = null;
                // currentdataLists = _db.SystemLists.Where(d => d.CompanyID == companyId
                // && d.Active == true).OrderBy(d => d.ListName).ToList();

                 //if (dataListsActive != null)
                 //    dataListsActive = dataListsActive.Union(currentdataLists).ToList();
                 //else
                 //    dataListsActive = currentdataLists.ToList();
            }
            catch(Exception ex){             
                tap.dom.hlp.ErrorLog.LogException(ex); 
            }

            return dataListsActive;           
            
        }
        #endregion

        #region To Get FieldDetails By TabID
        /// <summary>
        /// Invoked to Get the Fielddetails By Tabid
        /// </summary>
        /// <param name="_tabid"></param>
        /// <returns></returns>
        public object GetFieldDetailsByTabID(int tabId)
        {
            try
            {
                var fieldDetails = from a in _db.Fields
                                 join b in _db.FieldValues on a.FieldID equals b.FieldID
                                 where a.TabID == tabId
                                 select new { TabID = a.TabID, FieldID = a.FieldID, ControlType = a.ControlType, FieldValueID = b.FieldValueID, FieldValue = b.FieldValue, DisplayName = a.DisplayName };
                
                return fieldDetails.ToList();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex); 
            }
            return null;          
        }

        #endregion

        #region For SiteSetting DateFomat
        /// <summary>
        /// Description-SiteSettingDateFormat
        /// </summary>
        public string SiteSettingDateFormat(string companyID, string eventDate)
        {
            int compId = Convert.ToInt32(companyID);
            string dateFormat = _db.SiteSettings.Where(a => a.CompanyID == compId).Select(a => a.DateFormat).FirstOrDefault();
            string formattedDate = "";

            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            if (eventDate != "")
            {
                dateInfo.ShortDatePattern = ((dateFormat == "MM/dd/yyyy" || dateFormat == "MMM dd yyyy") ? "MM/dd/yyyy" : (dateFormat == "dd/MM/yyyy" || dateFormat == "dd-MM-yyyy") ? "dd/MM/yyyy" : "MM/dd/yyyy");
                DateTime evDate = Convert.ToDateTime(eventDate, dateInfo);
                formattedDate = evDate.ToString("MM/dd/yyyy").Replace("-", "/");
            }

            return formattedDate;
        }

        #endregion


        public static int GetModuleIdByName(string moduleName)
        {

            int moduleId = (moduleName.Contains("Incident")) ? Convert.ToInt32(Enumeration.Modules.Incident)
                                                              : (moduleName.Contains("Investigation")) ? Convert.ToInt32(Enumeration.Modules.Investigation)
                                                              : (moduleName.Contains("Audit")) ? Convert.ToInt32(Enumeration.Modules.Audit)
                                                              : (moduleName.Contains("CAP")) ? Convert.ToInt32(Enumeration.Modules.ActionPlan)
                                                              : (moduleName.Contains("ActionPlan")) ? Convert.ToInt32(Enumeration.Modules.ActionPlan) 
                                                              : Convert.ToInt32(tap.dom.hlp.Enumeration.Modules.Incident);
            return moduleId;
        }

        /// <summary>
        /// Get Company Id
        /// </summary>
        /// <returns></returns>
        public int GetCompanyId()
        {
            tap.dat.Companies company = _db.Companies.FirstOrDefault();
            int companyId = 0;

            if (company != null)
            {
                companyId = company.CompanyID;
            }
            return companyId;
        }

        public static string GetModuleNameByModuleId(int moduleId)
        {
            string moduleName = string.Empty;
            moduleName = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Incident) 
                                        ? tap.dom.hlp.Enumeration.Modules.Incident.ToString() : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Investigation) 
                                        ? tap.dom.hlp.Enumeration.Modules.Investigation.ToString() : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Audit)
                                        ? tap.dom.hlp.Enumeration.Modules.Audit.ToString() : tap.dom.hlp.Enumeration.Modules.ActionPlan.ToString();
            return moduleName;
        }

        public  static string GetSiteRoot()
        {
            string port = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port + System.Web.HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
        
        /// <summary>
        /// Check for the email availability.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool EmailAvailabilityCheck(string email){
            tap.dat.Users emailUser = null;
            tap.dat.Companies emailCompany = null;
            try
            {
                emailUser = _db.Users.FirstOrDefault(a => a.Email.ToLower() == email.ToLower());
                emailCompany = _db.Companies.FirstOrDefault(a => a.Email.ToLower() == email.ToLower());
            }
            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            if (emailUser == null && emailCompany == null)
                return false;

            return true;
        }
    }
}
