﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.adm
{
    public class MemberDetails
    {
        private string _fullName;
        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        private int _userid;
        public int UserId
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private int _companyid;
        public int CompanyID
        {
            get { return _companyid; }
            set { _companyid = value; }
        }

        private int _teammemberid;
        public int TeamMemberID
        {
            get { return _teammemberid; }
            set { _teammemberid = value; }
        }

        private string _hasedit;
        public string HasEdit
        {
            get { return _hasedit; }
            set { _hasedit = value; }
        }

        private string _hasview;
        public string HasView
        {
            get { return _hasview; }
            set { _hasview = value; }
        }

        private string _displayonreport;
        public string DisplayOnReport
        {
            get { return _displayonreport; }
            set { _displayonreport = value; }
        }

        private string _manualadd;
        public string isManualAdd
        {
            get { return _manualadd; }
            set { _manualadd = value; }
        }
    }
}
