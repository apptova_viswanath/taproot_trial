﻿/*Custom Control functionality on public side*/

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
#endregion

namespace tap.dom.adm
{
    public class FieldValues
    {
        tap.dat.EFEntity _db = new dat.EFEntity();

        #region To insert Field Values
        public bool SaveFieldValues(int fieldValueId, int eventId, int fieldId, string fieldValueName)
        {
            bool fieldValueExist = true;
            tap.dat.FieldValues fieldValue = null;
            try
            {
                if (fieldValueId == Convert.ToInt32(Enumeration.Types.New))               
                    fieldValue = new tap.dat.FieldValues();  
                else
                    fieldValue = _db.FieldValues.FirstOrDefault(a => a.EventID == eventId && a.FieldID == fieldId);
             
                fieldValue.EventID = eventId;
                fieldValue.FieldID = fieldId;
                fieldValue.FieldValue = fieldValueName;
                fieldValue.isDataList = false;

                if (fieldValueId == Convert.ToInt32(Enumeration.Types.New))   
                   _db.AddToFieldValues(fieldValue);
            
                _db.SaveChanges();                                           

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return fieldValueExist;
        }
        #endregion
      
        #region To Get The FieldValues By Fieldid
        public List<tap.dat.FieldValues> FieldValueListByID(int fieldId)
        {
            List<tap.dat.FieldValues> fieldValueList = new List<dat.FieldValues>();
            fieldValueList = (_db.FieldValues.Where(a => a.FieldID == fieldId)).ToList();
            return fieldValueList;
        }
        #endregion

        #region "To get the Custom field values based on field id"
        public List<tap.dat.FieldValues> CustomFieldValuesSelect(int fieldId, int eventId)
        {
            List<tap.dat.FieldValues> fieldValueList = new List<dat.FieldValues>();
            fieldValueList = (_db.FieldValues.Where(a => a.FieldID == fieldId && a.EventID == eventId)).ToList();
            return fieldValueList;
        }
        #endregion

        #region To Update FieldValues By Fieldvalueid
        public tap.dat.FieldValues FieldvaluesByFieldValueid(int fieldValueId)
        {
            tap.dat.FieldValues fieldValue = new dat.FieldValues();
            fieldValue = _db.FieldValues.First(a => a.FieldValueID == fieldValueId);
            return fieldValue;
        }

        #endregion

    }
}
