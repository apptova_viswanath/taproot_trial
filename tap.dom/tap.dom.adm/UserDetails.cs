﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
using tap.dom.usr;
using tap.dom.adm;
using tap.dom.gen;

namespace tap.dom.adm
{
    public class UserDetails
    {
        private tap.dat.EFEntity db = new tap.dat.EFEntity();
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        tap.dom.gen.Cryptography _crypt = new gen.Cryptography();
        string _message = string.Empty;


        /// <summary>
        /// Add new user and update user details
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="companyId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="emailId"></param>
        /// <param name="group"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public string AddUser(string userID, string userName, string password, string companyId, string firstName, string lastName, string phoneNumber, string emailId, bool active, bool admin, bool viewEvents, bool editEvents, string subscriptionStart, string subscriptionEnd, int LanguageID)
        {
            _message = string.Empty;

            int userid = 0;
            int.TryParse(userID, out userid);

            //Check if the save is for new task or edit task
            bool isNewInsert = (userid == Convert.ToInt32(tap.dom.hlp.Enumeration.Types.NewInsert)) ? true : false;

            if (isNewInsert)
            {
                if (IsUserExists(userName))
                {
                    _message = "User name " + userName + " already exists";
                    return _message;
                }
            }
            else
            {
                if (CheckUser(userid, userName))
                {
                    _message = "User name " + userName + " already exists";
                    return _message;
                }

                //Check for email availablity
                if (emailId !="" && EmailAvailableCheck(userid, emailId))
                {
                    _message = emailId + " already exists";
                    return _message;
                }
            }
            //Get the details of task if is called for editing;
            tap.dat.Users userDetails = isNewInsert ? new tap.dat.Users() : GetUserDetailsByID(userid);

            tap.dat.Companies companydetails = GetCompanyDetailsByID(Convert.ToInt32( companyId));
            //companydetails = _db.Companies.FirstOrDefault(a => a.CompanyID == Convert.ToInt32(companyId));

            //Set all properties for the task to be saved
            SetProperties(ref userDetails, userid, userName, password, companyId, firstName, lastName, phoneNumber, emailId,
                         active, admin, viewEvents, editEvents, ref companydetails, subscriptionStart, subscriptionEnd, LanguageID);

            //Save the changes to db
            SaveChanges(isNewInsert, userDetails, companydetails);

            if (isNewInsert)
            {
                tap.dom.usr.settings.UserSettings userSetting = new tap.dom.usr.settings.UserSettings();
                userSetting.SnapChartAutoSaveDurationSet(userDetails.UserID, 1);
            }

            return _message;

            
        }

        /// <summary>
        /// Get User Details by userID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public tap.dat.Users GetUserDetailsByID(int userID)
        {
            tap.dat.Users userDetails = new tap.dat.Users();

            try
            {
                userDetails = _db.Users.FirstOrDefault(a => a.UserID == userID);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return userDetails;

        }
        public tap.dat.Companies GetCompanyDetailsByID(int CompanyId)
        {
            tap.dat.Companies CompanyDetails = new tap.dat.Companies();

            try
            {
                CompanyDetails = _db.Companies.FirstOrDefault(a => a.CompanyID == CompanyId);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return CompanyDetails;

        }

        /// <summary>
        /// Setting user details 
        /// </summary>
        /// <param name="userDetails"></param>
        /// <param name="userID"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="companyId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="emailId"></param>
        /// <param name="group"></param>
        /// <param name="active"></param>
        public void SetProperties(ref tap.dat.Users userDetails, int userID, string userName, string password, string companyId, string firstName, string lastName,
                            string phoneNumber, string emailId, bool active, bool admin, bool viewEvents, bool editEvents, ref tap.dat.Companies companydetails, string SubscriptionStart, string SubscriptionEnd, int LanguageID)
        {
            try
            {
                if (userID > 0)
                {
                    if (userDetails.Password != _crypt.Encrypt(password))
                        userDetails.LastPasswordChange = DateTime.Now;
                }
                else
                    userDetails.LastPasswordChange = DateTime.Now;

                userDetails.UserName = userName;
                userDetails.Password = _crypt.Encrypt(password);
                userDetails.CompanyID = companyId == "" ? 0 : Int32.Parse(companyId);
                userDetails.FirstName = firstName;
                userDetails.LastName = lastName;
                userDetails.PhoneNo = phoneNumber;
                userDetails.Email = emailId;
                userDetails.Active = active;
                userDetails.isAdministrator = admin;
                userDetails.ViewEvents = viewEvents;
                userDetails.EditEvents = editEvents;
                if (LanguageID != 0)
                    userDetails.LanguageId = LanguageID;
                DateFormat dt= new DateFormat();
                companydetails.SubscriptionStart = dt.fromDatefrmatToDate(SubscriptionStart, Int32.Parse(companyId));
                companydetails.SubscriptionEnd = dt.fromDatefrmatToDate(SubscriptionEnd, Int32.Parse(companyId));
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        /// <summary>
        /// Save changes for new entry and update
        /// </summary>
        /// <param name="isNewInsert"></param>
        /// <param name="userDetails"></param>
        public void SaveChanges(bool isNewInsert, tap.dat.Users userDetails, tap.dat.Companies companydetails)
        {

            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();
            try
            {
                //If new user is getting created then add to the users table
                if (isNewInsert)
                {
                    _db.AddToUsers(userDetails);
                    //_message = "User account created successfully for '" + userDetails.FirstName + "'.";
                    transactionCategory = Enumeration.TransactionCategory.Create;
                }
                else
                {
                    //_message = "User account updated successfully for '" + userDetails.UserName + "'.";
                    transactionCategory = Enumeration.TransactionCategory.Update;
                }

                //Save the changes
                _db.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        public bool IsUserExists(string userName)
        {
            string user = string.Empty;
            var UsersList = _db.Users.ToList();
            try
            {
                user = _db.Users.FirstOrDefault(x => x.UserName == userName).UserName;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            if (user == string.Empty)
            {
                return false;
            }
            return true;
        }
        public bool CheckUser(int userID, string userName)
        {
            string user = string.Empty;
            var UsersList = _db.Users.ToList();
            try
            {
                user = _db.Users.FirstOrDefault(x => x.UserName == userName && x.UserID != userID).UserName;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            if (user == string.Empty)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check for email available or not
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        
        public bool EmailAvailableCheck(int userID, string email)
        {
            tap.dat.Users emailUser = null;
            tap.dat.Companies emailCompany = null;

            try { 
            emailUser = _db.Users.FirstOrDefault(a => a.UserID != userID && a.Email.ToLower() == email.ToLower());
            emailCompany = _db.Companies.FirstOrDefault(a => a.Email.ToLower() == email.ToLower());
            }
            catch(Exception ex){
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            if (emailUser == null && emailCompany == null)
                return false;

            return true;
        }

        /// <summary>
        /// Check user having events or not
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public bool IsUserHasEvent(string userID)
        {
            bool result = false;
            int userid = 0;
            int.TryParse(userID, out userid);

            if (userid > 0)
            {
                var eventCount = _db.Events.Where(a => a.CreatedBy == userid).Count();
                result = eventCount > 0 ? true : false;
            }
            return result;
        }

        /// <summary>
        /// Enable and disable user
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="isActivate"></param>
        /// <returns></returns>
        public string EnableOrDisableUser(string userID, bool isActivate)
        {
            _message = string.Empty;
            tap.dat.Users userDetails = new tap.dat.Users();
            try
            {
                int userid = 0;
                int.TryParse(userID, out userid);

                if (userid > 0)
                {
                    userDetails = _db.Users.FirstOrDefault(a => a.UserID == userid);

                    if (isActivate)
                    {
                        userDetails.Active = true;
                        _message = "User activated successfully.";
                    }
                    else
                    {
                        userDetails.Active = false;
                        _message = "User de-activated successfully.";
                    }
                }

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return _message;
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string DeleteUser(string userID)
        {
            _message = string.Empty;

            tap.dat.Users userDetails = new tap.dat.Users();
            try
            {
                int userid = 0;
                int.TryParse(userID, out userid);

                if (userid > 0)
                {
                    userDetails = _db.Users.FirstOrDefault(a => a.UserID == userid);

                    _db.DeleteObject(userDetails);
                    _message = "User deleted successfully.";
                }
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return _message;
        }

        //Add Event Access to Users
        public string AddEventAccessUsers(int companyId, string userIds, bool isEdit, bool isView)
        {
            string result = string.Empty;
            try
            {
                if (companyId.Equals(0))
                    return result;

                if (userIds.Contains(','))
                    {
                        string[] arrayUser = userIds.Split(',');
                        if (arrayUser != null)
                        {
                            foreach (string item in arrayUser)
                            {
                                int UserId = Convert.ToInt32(item);

                                tap.dat.Users user = new dat.Users();
                                user = _db.Users.Where(a => a.UserID == UserId && a.CompanyID == companyId).FirstOrDefault();

                                if (user != null)
                                {
                                    user.EditEvents = isEdit;
                                    user.ViewEvents = isView;

                                    _db.SaveChanges();
                                }
                            }
                        }
                    }
                    else if(userIds != null && userIds.Length>0)
                    {
                        int UserId = Convert.ToInt32(userIds);
                        tap.dat.Users user = new dat.Users();
                        user = _db.Users.Where(a => a.UserID == UserId && a.CompanyID == companyId).FirstOrDefault();

                        if (user != null)
                        {
                            user.EditEvents = isEdit;
                            user.ViewEvents = isView;

                            _db.SaveChanges();
                        }
                    }

                result = "Event Access saved successfully.";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
    }
}