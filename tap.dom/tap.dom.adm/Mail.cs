﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace tap.dom.adm
{
    public class Mail
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        public string SendEmail(string toAddress, string subject, string body)
        {
            tap.dat.EmailSettings settings = new dat.EmailSettings();
            settings = _db.EmailSettings.FirstOrDefault();
            if(settings == null)
                return "error: No Configuration settings found.";

            string result = "Message Sent Successfully.";
            string senderID = settings.EmailFrom;
            string senderPassword = settings.EmailPassword; 
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                    Host = settings.HostAddress,
                    Port = Convert.ToInt32(settings.PortNumber),
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new System.Net.NetworkCredential(senderID, senderPassword),
                    Timeout = 30000,
                };
                MailMessage message = new MailMessage(senderID, toAddress, subject, body);
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                result = "error: Sending Failed.";
            }
            return result;
        }
    }
}
