﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using System.Data.Objects.SqlClient;
using System.Globalization;

namespace tap.dom.adm
{
    public class SystemNotifications
    {
        private tap.dat.EFEntity db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        tap.dom.adm.SiteSettings _siteSetting = new SiteSettings();

        #region function to add system notification

        public bool AddSysNotification(string title, string fromDate, string toDate, string message, int sysId, bool active, int companyId)
        {
            bool result = true;

            tap.dat.SystemNotifications notification;

            if (sysId == 0)
            {
                notification = new tap.dat.SystemNotifications();
            }
            else
            {
                notification = db.SystemNotifications.Where(a => a.SysID == sysId && a.CompanyId == companyId).FirstOrDefault() ;
            }
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd-MM-yyyy";
            dtfi.DateSeparator = "-";
            // Add the values
            notification.Title = title;
            if (fromDate != null && fromDate.Length > 0)
            {
               
                if (fromDate.Contains('/'))
                {
                    string[] dateParams = fromDate.Split('/');
                    if (dateParams[2].Length == 2)
                        dateParams[2] = "20" + dateParams[2];
                    DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[0]), Int32.Parse(dateParams[1]));
                    notification.FromDate = date;
                }
                else
                {
                    notification.FromDate = Convert.ToDateTime(fromDate,dtfi);
                }
            }
            else
            {
                notification.FromDate = null;
            }
            if (toDate != null && toDate.Length > 0)
            {
                if (toDate.Contains('/'))
                {
                    string[] dateParams = toDate.Split('/');
                    if (dateParams[2].Length == 2)
                        dateParams[2] = "20" + dateParams[2];
                    DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[0]), Int32.Parse(dateParams[1]));
                    notification.ToDate = date;
                }
                else
                {
                notification.ToDate = Convert.ToDateTime(toDate, dtfi);
                }
            }
            else
            {
                notification.ToDate = null;
            }
            notification.Message = message;
            notification.SysID = sysId;
            notification.Active = active;
            notification.CompanyId = companyId;

            if (sysId == 0)
            {
                db.AddToSystemNotifications(notification);
            }
            db.SaveChanges();

            return result;
        }

        #endregion

        # region Delete system notification

        public bool DeleteSysNotification(int sysId, int companyId)
        {
            bool result = true;
            var sysNotification = db.SystemNotifications.Where( a => a.SysID == sysId && a.CompanyId==companyId).FirstOrDefault();
            db.DeleteObject(sysNotification);
            db.SaveChanges();
            return result;
        }

        #endregion

        #region to add get the system notifications

        public string GetNotificationByID(int sysId, int companyId)
        {
            var SysNotification = from a in db.SystemNotifications
                                  where a.CompanyId == companyId && a.SysID == sysId
                                  select new { SysID = a.SysID, Title = a.Title, FromDate = a.FromDate, ToDate = a.ToDate, Message = a.Message, Active = a.Active };

            return _efSerializer.EFSerialize(SysNotification);
        }

        public string GetSystemNotifications(int page, int rows, string sortIndex, string sortOrder, string companyID)
        {
            if (sortIndex == null)
                sortIndex = "ToDate";

            if (sortOrder == null)
                sortOrder = "desc";

            int skipRowsCount = 0;
            if (page != 1)
                skipRowsCount = (page - 1) * rows;

            int companyId = 0;
            if (!string.IsNullOrEmpty(companyID))
                companyId = Convert.ToInt32(companyID);

            //DateTime date = DateTime.Now.Date;
            IEnumerable<dat.SystemNotifications> SysNotificationsList = null;


            //Sorting logic
            switch (sortIndex)
            {
                case "Title":
                    SysNotificationsList = ((sortOrder.Equals("desc")) ? db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderByDescending(e => e.Title).ToList() : db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderBy(e => e.Title).ToList());
                    break;

                case "FromDate":
                    SysNotificationsList = ((sortOrder.Equals("desc")) ? db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderByDescending(e => e.FromDate).ToList() : db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderBy(e => e.FromDate).ToList());
                    break;

                case "ToDate":
                    SysNotificationsList = ((sortOrder.Equals("desc")) ? db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderByDescending(e => e.ToDate).ToList() : db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderBy(e => e.ToDate).ToList());
                    break;

                case "Message":
                    SysNotificationsList = ((sortOrder.Equals("desc")) ? db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderByDescending(e => e.Message).ToList() : db.SystemNotifications.Where(x => x.CompanyId == companyId).OrderBy(e => e.Message).ToList());
                    break;
            }

            if (SysNotificationsList == null || SysNotificationsList.Count() == 0)
                return String.Empty;


            #region To Add filter opeartion

            int totalRecords;
            var pageSize1 = rows.ToString() ?? "15";
            int pageSize = int.Parse(pageSize1);

            totalRecords = (SysNotificationsList != null && SysNotificationsList.Count() > 0)? SysNotificationsList.Count():'0';

            if (SysNotificationsList != null && SysNotificationsList.Count() > 0)
                SysNotificationsList = SysNotificationsList.Skip(skipRowsCount).Take(rows)
                                    .AsEnumerable().DefaultIfEmpty();
                


            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var currentPage1 = page.ToString() ?? "1";
            if (totalRecords <= 50)
                currentPage1 = "1";
            int currentPageNumber = int.Parse(currentPage1);

            #endregion

            var jsonData = new tap.dom.hlp.jqGridJson()
            {
                total = totalPages,
                page = currentPageNumber,
                records = totalRecords,
                rows = (
                    from notification in SysNotificationsList
                    let Title = notification.Title
                    let FromDate = ((notification.FromDate != null)?((DateTime)notification.FromDate).Date.ToString():"")
                    let ToDate = ((notification.ToDate != null)?((DateTime)notification.ToDate).Date.ToString():"")
                    let Message = notification.Message
                    let SysID = notification.SysID
                    let Active = notification.Active
                    select new tap.dom.hlp.jqGridRowJson
                    {
                        id = notification.SysID.ToString(),
                        cell = new string[] {                            
                            notification.Title, 
                            notification.Message,
                            ((FromDate != null && FromDate.Length>0)?(GetDateFormat(Convert.ToDateTime(FromDate), companyID)):""),
                            ((ToDate != null && ToDate.Length>0)?(GetDateFormat(Convert.ToDateTime(ToDate), companyID)):""),
                            //ConvertDateFormat(FromDate),
                            //ConvertDateFormat(ToDate),
                            notification.SysID.ToString(),
                            notification.Active.ToString()
                            }
                    }).ToArray()
            };

            return _efSerializer.EFSerialize(jsonData);
        }
        #endregion

        # region Function to get current notifications to be displayed based on the current date

        public string GetCurrentNotifications(int companyId)
        {
            IEnumerable<dat.SystemNotifications> SysNotificationsList = null;
            DateTime date = DateTime.Now.Date;
            //SysNotificationsList = db.SystemNotifications.OrderBy(a => a.ToDate).Where(a => a.FromDate <= date && date <= a.ToDate).ToList();
            try
            {
                //SysNotificationsList = ((from a in db.SystemNotifications
                //                         where a.CompanyId==companyId && a.Active == true && a.FromDate == null && a.ToDate == null
                //                         select a)
                //                            .Union(from a in db.SystemNotifications where a.CompanyId == companyId && a.Active == true && a.FromDate <= date && date <= a.ToDate select a)
                //                            .Union(from a in db.SystemNotifications where a.CompanyId == companyId && a.Active == true && a.FromDate <= date && a.ToDate == null select a)
                //                            .Union(from a in db.SystemNotifications where a.CompanyId == companyId && a.Active == true && a.FromDate == null && date <= a.ToDate select a)
                //                        ).OrderBy(a=>a.Active).OrderByDescending(a=>a.ToDate).OrderByDescending(a=>a.FromDate).ToList();
                                
                //Get the organization company id.
                int? orgCompany = 0;
                var org = db.Companies.FirstOrDefault(a=>a.ParentId == null);
                if (org != null)
                {
                    orgCompany = org.CompanyID;
                }

                SysNotificationsList = (from a in db.SystemNotifications
                                        where
                                          a.Active == true &&
                                          (a.CompanyId == companyId ||
                                          a.CompanyId == orgCompany) &&
                                          ((a.FromDate <= SqlFunctions.GetDate() &&
                                          a.ToDate >= SqlFunctions.GetDate()) ||
                                          (a.FromDate == null &&
                                          a.ToDate >= SqlFunctions.GetDate()) ||
                                          (a.FromDate <= SqlFunctions.GetDate() &&
                                          a.ToDate == null) ||
                                          (a.FromDate == null &&
                                          a.ToDate == null))
                                        orderby
                                          a.ToDate descending,
                                          a.FromDate descending select a).ToList();                                      

            }
            catch (Exception ex) { 
                
                tap.dom.hlp.ErrorLog.LogException(ex); 
            
            }

            if (SysNotificationsList == null || SysNotificationsList.Count() == 0)
                return string.Empty;

            var jsonData = new tap.dom.hlp.jqGridJson()
            {
                rows = (
                    from notification in SysNotificationsList
                    let Title = notification.Title
                    let Message = notification.Message
                    select new tap.dom.hlp.jqGridRowJson
                    {
                        id = notification.SysID.ToString(),
                        cell = new string[] {                            
                            notification.Title, 
                            notification.Message,
                            }
                    }).ToArray()
            };

            return _efSerializer.EFSerialize(jsonData);
        }        

        # endregion

        # region Function to convert date to string
        //public string ConvertDateFormat(DateTime date)
        public string ConvertDateFormat(string date)
        {
            DateTime dt;
            string dateString = "";
            if (date != null && date.Length > 0)
            {
                dt = Convert.ToDateTime(date);
                DateTime dateOnly = dt.Date;
                dateString = dateOnly.ToString("d");
            }
            return dateString;
        }
        # endregion 

        public string GetDateFormat(DateTime? eventDate, string companyID)
        {
            DateTime evDate = Convert.ToDateTime(eventDate);
            int compId = Convert.ToInt32(companyID);

            string datePattern = db.SiteSettings.FirstOrDefault(a => a.CompanyID == compId).DateFormat;
            //datePattern = (datePattern == string.Empty) ? SessionService.CurrentCulture.DateTimeFormat.ShortDatePattern : datePattern;
            return evDate.ToString(datePattern);
        }

    }
}