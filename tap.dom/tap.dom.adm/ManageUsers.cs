﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dat;
using tap.dom.gen;

namespace tap.dom.adm
{
    public class ManageUsers
    {
        private tap.dat.EFEntity db = new tap.dat.EFEntity();
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        public string GetUsersDetails(int page, int rows, string sortIndex, string sortOrder, int companyId, bool isGlobalAdmin, string searchFilter, bool isDivision)
        {
            if (searchFilter != null && searchFilter.Trim().Length == 0)
                searchFilter = null;
            isGlobalAdmin = isDivision ? false : true;
            if (sortIndex == null)
                sortIndex = "FirstName";

            if (sortOrder == null)
                sortOrder = "desc";

            IEnumerable<tap.dat.Users> users = from u in _db.Users
                                               where (u.CompanyID == companyId || isGlobalAdmin)
                                               && ((searchFilter == null || u.UserName.Contains(searchFilter))
                                               || (searchFilter == null || u.FirstName.Contains(searchFilter))
                                               || (searchFilter == null || u.LastName.Contains(searchFilter))
                                              || (searchFilter == null || u.Email.Contains(searchFilter)))
                                               select u; 


            switch (sortIndex)
            {
                case "FirstName":
                    if (sortOrder.Equals("desc"))
                    {
                        users = users.OrderByDescending(e => e.FirstName).ToList();
                    }
                    else
                    {
                        users = users.OrderBy(e => e.FirstName).ToList();
                    }
                    break;

                case "LastName":
                    if (sortOrder.Equals("desc"))
                    {
                        users = users.OrderByDescending(e => e.LastName).ToList();
                    }
                    else
                    {
                        users = users.OrderBy(e => e.LastName).ToList();
                    }
                    break;

                case "UserName":
                    if (sortOrder.Equals("desc"))
                    {
                        users = users.OrderByDescending(e => e.UserName).ToList();
                    }
                    else
                    {
                        users = users.OrderBy(e => e.UserName).ToList();
                    }
                    break;

                case "Email":
                    if (sortOrder.Equals("desc"))
                    {
                        users = users.OrderByDescending(e => e.Email).ToList();
                    }
                    else
                    {
                        users = users.OrderBy(e => e.Email).ToList();
                    }
                    break;
            }

            if (users == null)
                return string.Empty;

            return BuildJsonData(page, rows, users);
        }

        

        public string BuildJsonData(int page, int rows, IEnumerable<Users> UsersList)
        {
            int skipRowsCount = 0;
            if (page != 1)
                skipRowsCount = (page - 1) * rows;

            #region To Add filter opeartion

            int totalRecords = 0;
            var pageSize1 = rows.ToString() ?? "15";
            int pageSize = int.Parse(pageSize1);

            totalRecords = (UsersList != null && UsersList.Count() > 0) ? UsersList.Count() : 0;

            if (UsersList != null && UsersList.Count() > 0)
                UsersList = UsersList.OrderBy(a => a.Active).Skip(skipRowsCount).Take(rows)
                                    .AsEnumerable().DefaultIfEmpty().ToList();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var currentPage1 = page.ToString() ?? "1";
            if (totalRecords <= 50)
                currentPage1 = "1";
            int currentPageNumber = int.Parse(currentPage1);

            #endregion
            var jsonData = new tap.dom.hlp.jqGridJson();

            try
            {
                jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPageNumber,
                    records = totalRecords,
                    rows = (
                        from user in UsersList
                        let FirstName = user.FirstName
                        let LastName = user.LastName
                        let Email = user.Email
                        let UserName = user.UserName
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = user.UserID.ToString(),
                            cell = new string[] {                            
                            user.FirstName, 
                            user.LastName,
                            user.Email,
                            user.UserName
                            }
                        }).ToArray()
                };
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return _efSerializer.EFSerialize(jsonData);
        }

    
        /// <summary>
        /// Get Team Users list for Add Team member
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="filter"></param>
        /// <param name="ignoreCase"></param>
        /// <param name="sortIndex"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        public string GetTeamUsersDetails(int page, int rows, string filter, int ignoreCase, string sortIndex, string sortOrder, int companyID, string eventID,bool isShareTab)
        {

            IList<UserResult> details = new List<UserResult>();
  
            if (sortIndex == null)
                sortIndex = "FullName";

            if (sortOrder == null)
                sortOrder = "desc";
            try
            {
                if (filter != "")
                {
                   
                    if (isShareTab)
                    {
                        int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                        IQueryable<UserResult> teamMemberDetails = (from u in _db.Users
                                   join tm in _db.TeamMembers on u.UserID equals tm.UserID //&&  u.CompanyID equals tm.companyID
                                   into tempTM

                                   from TTM in tempTM.Where(a => a.EventID == eventId).DefaultIfEmpty()
                                   where !(from tt in _db.TeamMembers
                                           join us in _db.Users on tt.UserID equals us.UserID
                                           select tt.UserID).Contains(u.UserID)
                                        //&& u.CompanyID == companyID
                                                                select new UserResult
                                   {
                                       UserId = u.UserID,
                                       UserName = u.UserName,
                                       FullName = u.FirstName + " " + u.LastName,
                                       Email = u.Email,
                                       hasView = TTM.hasView != null ? TTM.hasView : false,
                                       hasEdit = TTM.hasEdit != null ? TTM.hasEdit : false,
                                       isDisplayOnReport = TTM.isDisplayOnReport != null ? TTM.isDisplayOnReport : false,
                                       CompanyID = u.CompanyID
                                   });

                        details = teamMemberDetails.Where(i => i.CompanyID == companyID).ToList();
                        
                    }
                    else {
                        IQueryable<UserResult> personDetails = (from u in _db.Users
                                                                                                                         
                                                            where u.CompanyID==companyID && u.Active==true

                                                            select new UserResult
                                                            {
                                                                UserId = u.UserID,
                                                                UserName = u.UserName,
                                                                FullName = u.FirstName + " " + u.LastName,
                                                                Email = u.Email,
                                                                hasView = false,
                                                                hasEdit = false,
                                                                isDisplayOnReport = false,
                                                                CompanyID = companyID
                                                            });

                        //IQueryable<object> personDetails = (from u in _db.Users
                        //           join
                        //               tm in _db.TasksResponsiblePersons on u.UserID equals tm.UserId
                        //                   into tempTM
                        //           from TTM in tempTM.DefaultIfEmpty()
                        //           where !(from tt in _db.TeamMembers
                        //                   join us in _db.Users on tt.UserID equals us.UserID
                        //                   select tt.UserID).Contains(u.UserID)
                        //           select new
                        //           {
                        //               UserId = u.UserID,
                        //               UserName = u.UserName,
                        //               FullName = u.FirstName + " " + u.LastName,
                        //               hasView = false,
                        //               hasEdit = false,
                        //               isDisplayOnReport = false
                        //           });

                        details = personDetails.ToList();
                    }

                    
                    //TODO: Here code is almost repeated , Optimize the code .
                    if (filter != "" && filter != null && filter.Length > 0)
                    {
                        details = (details.Where(a => a.Email.ToLower().Contains(filter.ToLower()) || a.FullName.ToLower().Contains(filter.ToLower()))).ToList();
                        //Sorting logic
                        switch (sortIndex)
                        {
                            case "FullName":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.FullName) : details.OrderBy(e => e.FullName)).ToList();
                                //details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.FullName).Where(a => a.FullName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.FullName).Where(a => a.FullName.ToLower().Contains(filter.ToLower()))).ToList();
                                break;
                            case "Email":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.Email) : details.OrderBy(e => e.Email)).ToList();
                                //details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.Email).Where(a => a.FullName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.Email).Where(a => a.FullName.ToLower().Contains(filter.ToLower()))).ToList();
                                break;
                            case "View":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasView == true ? "1" : "0") : details.OrderBy(e => e.hasView == true ? "1" : "0")).ToList();
                            //details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasView == true ? "1" : "0").Where(a => a.FullName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.hasView == true ? "1" : "0").Where(a => a.FullName.ToLower().Contains(filter.ToLower()))).ToList();
                                break;
                            case "Edit":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasEdit == true ? "1" : "0") : details.OrderBy(e => e.hasEdit == true ? "1" : "0")).ToList();
                                break;
                            case "isDisplayOnReport":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.isDisplayOnReport == true ? "1" : "0") : details.OrderBy(e => e.isDisplayOnReport == true ? "1" : "0")).ToList();
                                break;                           
                        }
                    }
                    else
                    {
                        switch (sortIndex)
                        {
                            case "FullName":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.FullName) : details.OrderBy(e => e.FullName)).ToList();
                                break;
                            case "Email":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.Email) : details.OrderBy(e => e.Email)).ToList();
                                break;
                            case "View":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasView == true ? "1" : "0") : details.OrderBy(e => e.hasView == true ? "1" : "0")).ToList();
                                break;
                            case "Edit":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasEdit == true ? "1" : "0") : details.OrderBy(e => e.hasEdit == true ? "1" : "0")).ToList();
                                break;
                            case "isDisplayOnReport":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.isDisplayOnReport == true ? "1" : "0") : details.OrderBy(e => e.isDisplayOnReport == true ? "1" : "0")).ToList();
                                break;                           

                        }
                    }

                    int skipRowsCount = 0;
                    if (page != 1)
                        skipRowsCount = (page - 1) * rows;

                    #region To Add filter opeartion

                    int totalRecords = 0;
                    var currentPazeSize = rows.ToString() ?? "15";
                    int pageSize = int.Parse(currentPazeSize);

                    var data = details.ToList();

                    totalRecords = (data != null && data.Count() > 0) ? data.Count() : 0;

                    int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                    var currentPage1 = page.ToString() ?? "1";

                    int currentPageNumber = int.Parse(currentPage1);

                    if (currentPageNumber > 1 && data != null && data.Count() > 0)
                        data = data.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();

                    #endregion
                    var jsonData = new tap.dom.hlp.jqGridJson();

                    jsonData = new tap.dom.hlp.jqGridJson()
                    {
                        total = totalPages,
                        page = currentPageNumber,
                        records = totalRecords,
                        rows = (
                            from user in data
                            let UserId = user.UserId.ToString()
                            let UserName = user.UserName
                            let FullName = user.FullName
                            select new tap.dom.hlp.jqGridRowJson
                            {
                                id = user.UserId.ToString(),
                                cell = new string[] {                            
                            user.UserId.ToString(),
                            user.UserName,
                            user.FullName,
                            user.Email,
                            user.hasView.ToString(),
                            user.hasEdit.ToString(),
                            user.isDisplayOnReport.ToString()
                            }
                            }).ToArray()
                    };

                    return _efSerializer.EFSerialize(jsonData);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }

        //making json for team members
        public string BuildTeamUserJsonData(int page, int rows, IEnumerable<Users> UsersList)
        {
            int skipRowsCount = 0;
            if (page != 1)
                skipRowsCount = (page - 1) * rows;

            #region To Add filter opeartion

            int totalRecords = 0;
            var currentPazeSize = rows.ToString() ?? "15";
            int pageSize = int.Parse(currentPazeSize);


            totalRecords = (UsersList != null && UsersList.Count() > 0) ? UsersList.Count() : 0;

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var currentPage1 = page.ToString() ?? "1";

            int currentPageNumber = int.Parse(currentPage1);

            if (currentPageNumber > 1 && UsersList != null && UsersList.Count() > 0)
                UsersList = UsersList.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();

            #endregion
            var jsonData = new tap.dom.hlp.jqGridJson();

            try
            {
                jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPageNumber,
                    records = totalRecords,
                    rows = (
                        from user in UsersList
                        let UserId = user.UserID.ToString()
                        let UserName = user.UserName
                        let FullName = user.FirstName + " " + user.LastName
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = user.UserID.ToString(),
                            cell = new string[] {                            
                            user.UserID.ToString(),
                            user.UserName,
                            user.FirstName + " " + user.LastName
                            }
                        }).ToArray()
                };
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return _efSerializer.EFSerialize(jsonData);
        }

        public string GetEventAccessUsers(int page, int rows, string filter, int ignoreCase, string sortIndex, string sortOrder, int companyId)
        {
            IEnumerable<tap.dat.Users> UsersList = null;

            if (sortIndex == null)
                sortIndex = "FirstName";

            if (sortOrder == null)
                sortOrder = "desc";

            UsersList = _db.Users.Where(x => x.CompanyID == companyId).ToList();
            //Sorting logic
            if (filter != null && filter.Length > 0)
            {
                switch (sortIndex)
                {
                    case "FirstName":
                        if (sortOrder.Equals("desc"))
                        {
                            UsersList = UsersList.OrderByDescending(e => e.FirstName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        else
                        {
                            UsersList = UsersList.OrderBy(e => e.FirstName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        break;

                    case "LastName":
                        if (sortOrder.Equals("desc"))
                        {
                            UsersList = UsersList.OrderByDescending(e => e.LastName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        else
                        {
                            UsersList = UsersList.OrderBy(e => e.LastName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        break;

                    case "UserName":
                        if (sortOrder.Equals("desc"))
                        {
                            UsersList = UsersList.OrderByDescending(e => e.UserName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        else
                        {
                            UsersList = UsersList.OrderBy(e => e.UserName).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        break;

                    case "Email":
                        if (sortOrder.Equals("desc"))
                        {
                            UsersList = UsersList.OrderByDescending(e => e.Email).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        else
                        {
                            UsersList = UsersList.OrderBy(e => e.Email).ToList().Where(a => a.FirstName.ToLower().Contains(filter.ToLower()) || a.UserName.ToLower().Contains(filter.ToLower()) || a.LastName.ToLower().Contains(filter.ToLower()));
                        }
                        break;
                }
            }
            else
            {
                switch (sortIndex)
                {
                    case "FirstName":
                        UsersList = (sortOrder.Equals("desc")) ? UsersList.OrderByDescending(e => e.FirstName).ToList() : UsersList.OrderBy(e => e.FirstName).ToList();
                        break;

                    case "LastName":
                        UsersList = (sortOrder.Equals("desc")) ? UsersList.OrderByDescending(e => e.LastName).ToList() : UsersList.OrderBy(e => e.LastName).ToList();
                        break;

                    case "UserName":
                        UsersList = (sortOrder.Equals("desc")) ? UsersList.OrderByDescending(e => e.UserName).ToList() : UsersList.OrderBy(e => e.UserName).ToList();
                        break;

                    case "Email":
                        UsersList = (sortOrder.Equals("desc")) ? UsersList.OrderByDescending(e => e.Email).ToList() : UsersList.OrderBy(e => e.Email).ToList();
                        break;
                }

            }
            int skipRowsCount = 0;
            if (page != 1)
                skipRowsCount = (page - 1) * rows;

            #region To Add filter opeartion

            int totalRecords = 0;
            var currentPazeSize = rows.ToString() ?? "15";
            int pageSize = int.Parse(currentPazeSize);

            var data = UsersList.ToList();

            totalRecords = (data != null && data.Count() > 0) ? data.Count() : 0;

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
            var currentPage1 = page.ToString() ?? "1";

            int currentPageNumber = int.Parse(currentPage1);

            if (currentPageNumber > 1 && data != null && data.Count() > 0)
                data = data.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();

            #endregion
            var jsonData = new tap.dom.hlp.jqGridJson();

            jsonData = new tap.dom.hlp.jqGridJson()
            {
                total = totalPages,
                page = currentPageNumber,
                records = totalRecords,
                rows = (
                    from user in data
                    let UserId = user.UserID.ToString()
                    let UserName = user.UserName
                    let FirstName = user.FirstName
                    select new tap.dom.hlp.jqGridRowJson
                    {
                        id = user.UserID.ToString(),
                        cell = new string[] {                            
                            user.FirstName,
                            user.LastName,
                            user.Email,
                            user.UserName,
                            user.ViewEvents.ToString(),
                            user.EditEvents.ToString()
                            }
                    }).ToArray()
            };

            return _efSerializer.EFSerialize(jsonData);

        }

    }

    public class UserResult
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool? hasView { get; set; }
        public bool? hasEdit { get; set; }
        public bool? isDisplayOnReport { get; set; }
        public int? CompanyID { get; set; }
    }
}