﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using tap.dom.hlp;
using tap.dat;

namespace tap.dom.adm
{
    /// <summary>
    /// For Admin Attachment and Attachment Folder Setup functions
    /// Created By: Deepa RJ
    /// Created Date: 30-05-2012
    /// </summary>

    public class Attachment
    {
        tap.dat.EFEntity db = new tap.dat.EFEntity();
        Enumeration.Types newtype = Enumeration.Types.New;

        List<string> lstHireNodeName = new List<string>();//nw1
        List<tap.dat.AttachmentFolders> lstAttachFoldersNode = new List<dat.AttachmentFolders>();//nw1 
        private string _message = string.Empty;
        const string ATTACHMENT_PATH = "\\" + "Attachments" + "\"" + "Attachments" + "\"";

        #region To Get The attachment folder id by  fullpath
        public int GetRootAttachmentFolderId()
        {
            int attachmentFolderId = 0;
            AttachmentFolders rootAttachmentNode = db.AttachmentFolders.FirstOrDefault(a => a.FullPath.ToLower() == ATTACHMENT_PATH.ToLower());

            if (rootAttachmentNode != null)
            {
                attachmentFolderId = rootAttachmentNode.AttachementFolderId;
            }

            return attachmentFolderId;
        }
        #endregion

        #region "Save AttachmentFolders values"
        public string SaveAttachmentFoldersValues(int AttachmentFolders, int attachmentFolderParent, string AttachmentFolderName, 
            bool Active, string DisplayOrder, string companyID, int userId)
        {
            bool _attachmentfoldervalueFlag = true;
            //Decrypt the value and pass
            int companyId = Convert.ToInt32(companyID);
            string message = String.Empty;
            tap.dat.AttachmentFolders attachFoldersNode = null;
            string _strfullpath = "";
            string _fullpath = "";

            try
            {
                if (AttachmentFolders == Convert.ToInt32(newtype))
                {
                    attachFoldersNode = new dat.AttachmentFolders();

                    if (attachmentFolderParent != -1)
                    {
                        _fullpath = GetFullPath(attachmentFolderParent);
                        _strfullpath = _fullpath + AttachmentFolderName + @"\";
                    }
                    else
                    {
                        _strfullpath = "\\\\" + AttachmentFolderName + @"\";
                    }

                    _attachmentfoldervalueFlag = AttachmentFolderExists(_strfullpath, companyId);
                    if (_attachmentfoldervalueFlag == true)
                    {
                        bool _attachmentfolderexist = true;

                        return AttachmentFolderValidationMsg(companyId, AttachmentFolderName, _strfullpath);
                    }

                }
                else
                {
                    //attachFoldersNode = db.AttachmentFolders.First();
                    attachFoldersNode = db.AttachmentFolders.Where(x=>x.CompanyId == companyId).First();
                }

                attachFoldersNode.FolderName = AttachmentFolderName;
                attachFoldersNode.Active = Active;
                attachFoldersNode.CompanyId = companyId;

                if (attachmentFolderParent == -1)
                    attachFoldersNode.ParentId = null;
                else
                    attachFoldersNode.ParentId = attachmentFolderParent;

                attachFoldersNode.FullPath = _strfullpath;

                if (AttachmentFolders == Convert.ToInt32(newtype))
                {
                    db.AddToAttachmentFolders(attachFoldersNode);
                }
                db.SaveChanges();


                _message = (attachmentFolderParent == 0) ?"Attachment Folder '" + AttachmentFolderName + "' Created."
                    : "Attachment Folder '" + AttachmentFolderName + "' added to parent folder '"
                    + db.AttachmentFolders.FirstOrDefault(a => a.AttachementFolderId == attachmentFolderParent).FolderName + "'" ;

                transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Create, _message);  
               
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            //return _attachmentfoldervalueFlag;
            return message;

        }
        #endregion

        #region For checking duplicate attachmentFolders values
        public bool AttachmentFolderExists(string FullPath, int companyId)
        {
            lstAttachFoldersNode = db.AttachmentFolders.ToList();

            bool attachmentFolderExists = false;
            //var Node = lstAttachFoldersNode.FirstOrDefault(a => a.FullPath.ToLower() == FullPath.ToLower());
            var Node = lstAttachFoldersNode.FirstOrDefault(a => a.FullPath.ToLower() == FullPath.ToLower() && a.CompanyId == companyId);
            if (Node != null)
                attachmentFolderExists = true;

            return attachmentFolderExists;
        }
        #endregion

        #region  Added for Taking  all parentNode
        public void HireNodeParentListName(int hireNodeParent)
        {
            var hrNode = lstAttachFoldersNode.FirstOrDefault(a => a.AttachementFolderId == hireNodeParent);
            var rootNode = lstAttachFoldersNode.FirstOrDefault(a => a.FolderName == "Attachments").AttachementFolderId;
            if (hrNode.AttachementFolderId != rootNode)
            {
                lstHireNodeName.Add(hrNode.FolderName);
                HireNodeParentListName(Convert.ToInt32(hrNode.ParentId));
            }

        }
        #endregion

        #region To Return the Hir Node Id By Title
        /// <summary>
        /// Overload method  to Get the AttachmentFolders By AttachmentFolders
        /// </summary>
        /// <param name="_listid"></param>
        /// <param name="_listvaluename"></param>
        /// <returns></returns>
        public int AttachmentFolderByIDSelect(string attachmentFolderName)
        {
            int attachementFolderID = 0;
            tap.dat.AttachmentFolders attachFoldersNode = db.AttachmentFolders.FirstOrDefault(a => (a.FolderName.Trim().ToLower() == attachmentFolderName.Trim().ToLower()));
            if (attachFoldersNode != null)
                attachementFolderID = attachFoldersNode.AttachementFolderId;
            return attachementFolderID;
        }
        #endregion

        #region To Get Fullapth of parentlist
        public string GetFullPath(int attachementFolderParentId)
        {
            var parentfullpath = db.AttachmentFolders.FirstOrDefault(a => a.AttachementFolderId == attachementFolderParentId);
            return parentfullpath.FullPath.ToString();
        }
        #endregion

        #region Added on 01-May-2012 To Build JsonData For JSTreeview
        /// <summary>
        /// Invoked To Build JsonData For JSTreeview for Attachment Folder
        /// </summary>
        /// <param name="_listid"></param>
        /// <returns></returns>
        public string BuildAttachmentJSTreeView(int attachmentFolderID, int companyID)
        {
            List<tap.dat.AttachmentFolders> attachFoldersNode = new List<dat.AttachmentFolders>();

            //Decrypt the value and pass
            //int companyId = Convert.ToInt32(EncryptDecrypt.Decrypt(companyID));

            StringBuilder sbJson = new StringBuilder("");
            try
            {
                //attachFoldersNode = db.AttachmentFolders.ToList();

                attachFoldersNode = db.AttachmentFolders.Where(x=>x.CompanyId== companyID).ToList();

                var attachmentNodeParent = from child in attachFoldersNode
                                    where child.ParentId == null
                                    select child;
                if (attachmentNodeParent.Count() != 0)
                {
                    sbJson.Append("[ ");
                    foreach (var parent in attachmentNodeParent)
                    {
                        sbJson.Append(BuildAttachmentJSTreeViewChild(attachFoldersNode, parent.AttachementFolderId));//build child nodes
                        sbJson.Append(" , ");
                    }
                    sbJson.Append(" ], ");
                    sbJson.Remove(sbJson.ToString().LastIndexOf(", "), 2);
                    sbJson.Remove(sbJson.ToString().LastIndexOf(", "), 2);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return sbJson.ToString();
        }
        #endregion

        #region "Child Build for Attachment"
        public string BuildAttachmentJSTreeViewChild(List<tap.dat.AttachmentFolders> attachFoldersNode, int attachementFolderParentId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var thisNode = (from child in attachFoldersNode where child.AttachementFolderId == attachementFolderParentId select child).First();

                var children = from child in attachFoldersNode
                               where child.ParentId == attachementFolderParentId
                               select child;

                sb.Append("{ \"data\":\" " + thisNode.FolderName + "\"" + "," + "\"attr\": {" + "\"id\":\"" + thisNode.AttachementFolderId + "\" ");

                if (children.Count() == 0)
                {
                    sb.Append(",\"rel\":\"folders\" }} ");
                }
                else
                {
                    sb.Append(",\"rel\":\"files\" }, \"children\": [ ");//create child node
                    foreach (var child in children)
                    {
                        sb.Append(string.Format(" {0},", BuildAttachmentJSTreeViewChild(attachFoldersNode, child.AttachementFolderId)));
                    }
                    sb.Remove(sb.ToString().LastIndexOf(","), 1);
                    sb.Append(" ]} ");

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return sb.ToString();
        }
        #endregion

        #region To delete multiple nodes

        public bool AttachmentFolderDelete(int nodeID, int userId)
        {
            bool boolRes = false;
            var _attachementFolder = db.AttachmentFolders.First(a => a.AttachementFolderId == nodeID);

            try
            {
                var _attachmentFoldervalueddrow = db.AttachmentFolders.FirstOrDefault(a => a.ParentId == null);
                var _attachmentFolderValue = _attachmentFoldervalueddrow.AttachementFolderId;

                if (_attachmentFolderValue != nodeID)
                {

                    //List<tap.dat.AttachmentFolders> hirenodeChild = (from hnode in db.AttachmentFolders
                    //                                                 where hnode.ParentId == nodeID
                    //                                                 select hnode).ToList();
                    //// Delete the subfolders
                    //foreach (var hnodes in hirenodeChild)
                    //{
                    //    db.DeleteObject(hnodes);
                    //}

                    // Delete the folder
                    db.DeleteObject(_attachementFolder);
                    MoveFilesToTheParentFolder(nodeID);

                    try
                    {
                        db.SaveChanges(); // Save the database
                        _message = "Attachment Folder '" + _attachementFolder.FolderName + "' Deleted.";
                        transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete, _message);  
               
                    }
                    catch (System.Data.OptimisticConcurrencyException ex)
                    {
                        ErrorLog.LogException(ex);
                        // Optimistic concurrency exception has occured, update the records and then save...
                        db.Refresh(System.Data.Objects.RefreshMode.ClientWins, _attachementFolder);
                        db.SaveChanges();
                    }
                    boolRes = true;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return boolRes;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        public void MoveFilesToTheParentFolder(int nodeID)
        {
            var parentFolderId = Convert.ToInt32( db.AttachmentFolders.First(a => a.AttachementFolderId == nodeID).ParentId);
            
            List<tap.dat.Attachments> attachments = (from attach in db.Attachments
                                                                where attach.AttachmentFolderID == nodeID
                                                                select attach).ToList();
            // Delete the subfolders
            foreach (tap.dat.Attachments attach in attachments)
            {
               attach.AttachmentFolderID = parentFolderId;
               db.SaveChanges();
            }
        }

        #region "To Check File Attachment exist or not"
        public bool CheckAttachmentFileExist(int AttachmentFolders)
        {
            bool _filexist = false;
            tap.dat.Attachments attachment = db.Attachments.FirstOrDefault(a => (a.AttachmentFolderID == AttachmentFolders));
            if (attachment != null)
                _filexist = true;
            return _filexist;
        }
        #endregion

        #region To update the order of nodes
        public string AttachmentFoldersValueUpdate(int HirLevelId, int AttachmentFolders, int AttachmentFolderParent, string AttachmentFolderTitle, int parentnodeId, int companyId)
        {
            bool attachmentFolderExist = false;
            string fullPath = GetFullPath(AttachmentFolderParent);
            string strFullPath = fullPath + AttachmentFolderTitle + @"\";
            tap.dat.AttachmentFolders attachFoldersNode = new AttachmentFolders();
            string message = string.Empty;

            try
            {

                tap.dat.AttachmentFolders attachmentFolderRecord = db.AttachmentFolders.First(a => a.AttachementFolderId == AttachmentFolders);
                if (AttachmentFolderParent == GetRootAttachmentFolderId())
                {
                    attachmentFolderExist = AttachmentFolderExists(strFullPath, companyId);//check Item exist or not
                    if (!attachmentFolderExist)
                    {
                        attachmentFolderRecord.FolderName = AttachmentFolderTitle;
                        attachmentFolderRecord.FullPath = strFullPath;
                        db.SaveChanges();
                    }
                    else
                    {
                        //return attachmentFolderExist;
                        return AttachmentFolderValidationMsg(companyId, AttachmentFolderTitle, strFullPath);
                    }
                }
                else
                {
                    if(AttachmentFolderExists(strFullPath, companyId))
                        return AttachmentFolderValidationMsg(companyId, AttachmentFolderTitle, strFullPath);

                    attachmentFolderRecord.FolderName = AttachmentFolderTitle;
                    attachmentFolderRecord.FullPath = strFullPath;
                    attachmentFolderRecord.ParentId = AttachmentFolderParent;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            //return attachmentFolderExist;
            return message;

        }
        #endregion

        #region to get the attachment by Id
        public static tap.dat.Attachments GetAttachment(int atchDocId)
        {
            tap.dat.EFEntity db = new tap.dat.EFEntity();

            tap.dat.Attachments att = (from a in db.Attachments
                                       where a.AttachmentID == atchDocId
                                       select a).FirstOrDefault();
            return att;
        }

        #endregion

        #region "To update the folder structure (drag and drop of folders)"

        public string MoveFoldersUpdate(int parent, int folderid, string foldername, int companyId, int userId)
        {
            bool isBool = false;
            string result = string.Empty;

            //string folder = foldername.Trim();
            //int nWhiteSpace = folder.IndexOf(" ");
            //if(nWhiteSpace > 0)
            //{
            //    foldername = folder.Substring(0, nWhiteSpace).Trim();
            //}
            try
            {
                tap.dat.AttachmentFolders folderrecord = db.AttachmentFolders.First(a => a.AttachementFolderId == folderid);

                string fullPath = GetFullPath(parent);
                string strFullPath = fullPath + foldername.Trim() + @"\";
                if (AttachmentFolderExists(strFullPath, companyId) == false)
                {
                    folderrecord.FullPath = strFullPath;
                    folderrecord.ParentId = parent;
                    db.SaveChanges();
                    isBool = true;
                }
                else
                {
                    return AttachmentFolderValidationMsg(companyId, foldername, strFullPath);
                }

                string parentFolder = db.AttachmentFolders.First(a => a.AttachementFolderId == parent).FolderName;
                _message = "'" + foldername + "' moved to '" + parentFolder + "'";  
                transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete, _message);  
               
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
//            return isBool;
            return result;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public bool CheckIfAttachmentsFileExists(int folderId)
        {
           var rowCount =  db.Attachments.Count(a => a.AttachmentFolderID == folderId);
           var isRecordsExist = (rowCount > 0)?  true : false;
           return isRecordsExist;
        }

        //Attachment folders validation messages
        public string AttachmentFolderValidationMsg(int companyId, string folderName, string fullPath)
        {
            string message = string.Empty;

            //int tempCompanyId = db.AttachmentFolders.Where(a => a.FullPath.ToLower() == fullPath).FirstOrDefault().CompanyId;
            //tap.dom.tab.EventTabs events = new tab.EventTabs();
            //List<int> parentsIds = events.GetCompanyParentIds(companyId);

            //if(companyId.Equals(tempCompanyId))
            //    return folderName + " Folder already exists.";

            //if (parentsIds.Contains(tempCompanyId))
            //    message = folderName + " Folder can't be added, as this has been added by one of the parent level company.";
            //else
            //    message = folderName + " already added by Company: " + db.Companies.Where(a => a.CompanyID == tempCompanyId).FirstOrDefault().CompanyName.ToString() + ".";

            var Node = lstAttachFoldersNode.FirstOrDefault(a => a.FullPath.ToLower() == fullPath.ToLower() && a.CompanyId == companyId);
            if (Node != null)
                message = folderName + " Folder already exists.";

            return message;
        }
    }
}
