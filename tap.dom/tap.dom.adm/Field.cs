﻿/*Dynamic Fields page functionality*/

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using tap.dom.hlp;
#endregion

namespace tap.dom.adm
{
    public class Field
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        private string _message;

        #region to insert/Update For Fields
        public bool SaveField(int fieldId, int tabId, string displayName, int sortOrder, string dataType, string controlType, int maxLength, bool isRequired, int listId, bool isActive, string description, int userId)
        {
            bool fieldExist = true;
            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();

            tap.dat.Fields field = null;
            try
            {
                if (fieldId == Convert.ToInt32(Enumeration.Types.New))
                {
                    field = new tap.dat.Fields();
                    bool checkFieldExist = CheckFieldExist(displayName, tabId);//Check for the Duplicate fieldname insert operation
                    if (checkFieldExist)
                    {
                        fieldExist = false;
                        return fieldExist;
                    }
                }
                else
                {
                    field = _db.Fields.FirstOrDefault(x => x.FieldID == fieldId);//Check for the Duplicate fieldname update operation
                    int customFieldId = GetFieldId(displayName, tabId);
                    if (customFieldId != 0)
                    {
                        if (customFieldId != fieldId)
                        {
                            fieldExist = false;
                            return fieldExist;
                        }
                    }
                }

                //only set fieldId if it is an update. For new ones, don't set.
                if (fieldId > 0)
                    field.FieldID = fieldId; 

                field.TabID = tabId;
                field.DisplayName = displayName;
                field.SortOrder = sortOrder;
                field.Datatype = dataType;
                field.ControlType = controlType;

                if (maxLength == 0)
                    field.MaxLength = null;
                else
                    field.MaxLength = maxLength;

                field.IsRequired = isRequired;
                field.IsActive = isActive;

                //if this is a datalist, store list Id. Else, leave as null.
                //This is a fix necessary due to adding foreign key restraints.
                if (listId > 0)
                    field.ListID = listId;

                //check the empty 
                if (description == string.Empty)
                    field.Description = null;
                else
                    field.Description = description;

                if (fieldId == Convert.ToInt32(Enumeration.Types.New))
                {
                    _db.AddToFields(field);
                    transactionCategory = Enumeration.TransactionCategory.Create;
                    _message = "Field '" + field.DisplayName + "' added to " + field.Tabs.TabName;
                   
                }
                {
                    transactionCategory = Enumeration.TransactionCategory.Update;
                    _message = "Field '" + field.DisplayName + "' of tab '" + field.Tabs.TabName + "' updated";
                   
                }

                _db.SaveChanges();
                transaction.Transaction.SaveTransaction(null, userId, null, transactionCategory, _message);  

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return fieldExist;
        }

        #endregion

        #region To check the Field alredy exist
        /// <summary>
        /// To check the Field alredy exist and return using DisplayName
        /// </summary>
        /// <param name="_displayname"></param>
        /// <returns></returns>
        public bool CheckFieldExist(string displayName, int tabId)
        {
            bool fieldExist = false;
            tap.dat.Fields field = _db.Fields.FirstOrDefault(a => (a.DisplayName.Trim().ToLower() == displayName.Trim().ToLower()) && a.TabID == tabId);
            if (field != null)
                fieldExist = true;

            return fieldExist;
        }
        #endregion

        #region To Get FieldId by tabid and fieldname
        /// <summary>
        /// return the fieldid by tabid and fieldname
        /// </summary>
        /// <param name="_displayname"></param>
        /// <returns></returns>
        public int GetFieldId(string displayName, int tabId)
        {
            int fieldId = 0;
            try
            {
                tap.dat.Fields field = _db.Fields.FirstOrDefault(a => (a.DisplayName.Trim().ToLower() == displayName.Trim().ToLower()) && a.TabID == tabId);
                if (field != null)
                    fieldId = field.FieldID;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return fieldId;
        }

        #endregion

        #region  To Delete Field BY ID
        public bool DeleteFieldByID(int fieldId, int userId)
        {
            try
            {
                tap.dat.Fields field = _db.Fields.First(x => x.FieldID == fieldId);                
                     
                    var RECS = _db.ReportElementCustomSource.FirstOrDefault(e=>e.CustomFieldID == fieldId);
                    if (RECS != null)
                    {
                        return false;
                    }
                    _db.DeleteObject(field);
                    _db.SaveChanges();

                    tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete,
                    "Field '" + field.DisplayName + "' deleted for tab '" + field.Tabs.TabName + "'"); 
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }
        #endregion

        #region To Get Field By ID
        public tap.dat.Fields GetFieldByID(int fieldId)
        {
            tap.dat.Fields field = _db.Fields.First(x => x.FieldID == fieldId);
            return field;
        }
        #endregion

        #region To Get Field By FieldID
        public object GetFieldByFieldID(int fieldId)
        {
            var fieldList = from a in _db.Fields
                            join b in _db.SystemLists
                            on a.ListID equals b.ListID
                            into temp
                            from t in temp.DefaultIfEmpty()
                            where a.FieldID == fieldId
                            select new { FieldID = a.FieldID, TabID = a.TabID, DisplayName = a.DisplayName, SortOrder = a.SortOrder, Datatype = a.Datatype, ControlType = a.ControlType, MaxLength = a.MaxLength, IsRequired = a.IsRequired, ListID = a.ListID, ListName = t.ListName, Description = a.Description, IsActive = a.IsActive };

            return fieldList.ToList();
        }
        #endregion

        #region To Get Field By list
        public List<tap.dat.Fields> GetFieldlist()
        {
            List<tap.dat.Fields> fieldList = _db.Fields.ToList();
            return fieldList;
        }
        #endregion

        # region "To get The DataList"
        public List<tap.dat.SystemLists> DataListSelect(int companyId)
        {
            tap.dom.adm.CommonOperation dataListSelect = new tap.dom.adm.CommonOperation();
            return dataListSelect.DataListSelect(companyId);
        }
        # endregion

        #region To Get Field By TabID
        public tap.dat.Fields FieldByTabIDSelect(int tabId)
        {
            tap.dat.Fields field = _db.Fields.First(x => x.TabID == tabId);
            return field;
        }
        #endregion

        #region To Get The Field By Tabid
        public List<tap.dat.Fields> FieldListByTabID(int tabId)
        {
            List<tap.dat.Fields> fieldList = _db.Fields.Where(a => a.TabID == tabId).ToList();
            return fieldList;
        }
        #endregion

        #region To Get the Fieldlist in object form
        /// <summary>
        /// Invoked To Get the fieldlist By Tabid
        /// </summary>
        /// <param name="_tabId"></param>
        /// <returns></returns>
        ///        

        public object GetFieldListByTabID(int tabId)
        {
            var fieldList = from a in _db.Fields
                            join b in _db.SystemLists
                            on a.ListID equals b.ListID
                            into temp
                            from t in temp.DefaultIfEmpty()
                            where a.TabID == tabId
                            orderby a.SortOrder
                            let disptype = //GetDisplayTypes(a)//(a.ControlType == "Selectmultiplefromlist") ? 2 : 1
                            (from listvalues in
                                 (from listvalues in _db.ListValues
                                  where
                                      (from listvalues0 in _db.ListValues
                                       where
                                           (from listvalues01 in _db.ListValues
                                            where listvalues01.ListID == a.ListID
                                            select new
                                            {
                                                listvalues01.ListValueID
                                            }).Contains(new { ListValueID = (Int32)listvalues0.Parent })
                                       select new
                                       {
                                           listvalues0.ListValueID
                                       }).Contains(new { ListValueID = (Int32)listvalues.Parent })
                                  select new
                                  {
                                      Dummy = "x"
                                  })
                             group listvalues by new { listvalues.Dummy } into g
                             select new { RowCount = (int)g.Count() }).FirstOrDefault().RowCount

                            select new
                            {
                                FieldID = a.FieldID,
                                TabID = a.TabID,
                                DisplayName = a.DisplayName,
                                SortOrder = a.SortOrder,
                                Datatype = a.Datatype,
                                ControlType = a.ControlType,
                                MaxLength = a.MaxLength,
                                IsRequired = a.IsRequired,
                                ListID = a.ListID,
                                ListName = t.ListName,
                                Disptype = a.ControlType == "Select multiple from list" ? 2 : (disptype > 0) ? 2 : 1,
                                IsActive = a.IsActive,
                                Description = a.Description

                            };


            return fieldList.ToList();

        }

        #endregion
            

        #region Updating SortOrder of the fields
        public bool FieldSortOrderUpdate(string[] fieldId, string[] sortOrder)
        {
            bool isSortUpdate = false;
            var i = 0;
            try
            {
                foreach (var fid in fieldId)
                {
                    var fieldIdList = Convert.ToInt32(fid);
                    tap.dat.Fields field = _db.Fields.FirstOrDefault(a => a.FieldID == fieldIdList);
                    if (field != null)
                    {
                        field.SortOrder = Convert.ToInt32(sortOrder[i]);
                        _db.SaveChanges();
                        isSortUpdate = true;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return isSortUpdate;
        }
        #endregion

        #region For conditional Delete of fields
        /// <summary>
        /// DeleteCustomField
        /// </summary>
        /// <param name="fieldId"></param>
        /// <returns></returns>
        public bool DeleteCustomField(int fieldId, int userId)
        {
            bool DeleteExist = true;
            var fieldValue = _db.FieldValues.FirstOrDefault(a => a.FieldID == fieldId);
            if (fieldValue != null)
                DeleteExist = false;
            else
            {
                DeleteExist = DeleteFieldByID(fieldId, userId);
                
            }

            return DeleteExist;
        }
        #endregion
    }
}
