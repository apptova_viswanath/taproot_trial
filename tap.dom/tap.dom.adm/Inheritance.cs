﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.adm
{
    public class Inheritance
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        //Custom Tab inheritance
        public void CustomTabs(int companyId, int parentCompanyId)
        {
            try
            {
                List<tap.dat.Tabs> tabs = new List<dat.Tabs>();

                if (companyId > 0 && parentCompanyId > 0)
                {
                    tabs = _db.Tabs.Where(a => a.CompanyID == parentCompanyId && a.Active == true && a.IsSystem == false && a.IsUniversal == false).ToList();

                    if (tabs == null || tabs.Count == 0)
                        return;

                    SaveCustomTabs(tabs, parentCompanyId, companyId);

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        //Site Settings
        public void SiteSettings(int companyId, int parentCompanyId)
        {
            try
            {
                tap.dat.SiteSettings parentCompSiteSetting = new dat.SiteSettings();
                tap.dat.SiteSettings currentCompSiteSetting = new dat.SiteSettings();

                if (companyId > 0 && parentCompanyId > 0)
                {

                    parentCompSiteSetting = _db.SiteSettings.Where(a => a.CompanyID == parentCompanyId).ToList().FirstOrDefault();
                    currentCompSiteSetting = _db.SiteSettings.Where(a => a.CompanyID == companyId).ToList().FirstOrDefault();

                    bool newAdd = (currentCompSiteSetting == null);
                    if (newAdd)
                    {
                        currentCompSiteSetting = new dat.SiteSettings();
                        currentCompSiteSetting.CompanyID = companyId;
                    }

                    currentCompSiteSetting.IsTwelveHourFormat = parentCompSiteSetting.IsTwelveHourFormat;
                    currentCompSiteSetting.DateFormat = parentCompSiteSetting.DateFormat;
                    currentCompSiteSetting.ApplicationTimeOut = parentCompSiteSetting.ApplicationTimeOut;
                    currentCompSiteSetting.PasswordPolicyId = parentCompSiteSetting.PasswordPolicyId;
                    currentCompSiteSetting.PasswordExpiration = parentCompSiteSetting.PasswordExpiration;

                    if (newAdd)
                    {
                        _db.AddToSiteSettings(currentCompSiteSetting);
                    }
                    _db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        //Comapny Logos
        public void CompanySettings(int companyId, int parentCompanyId)
        {
            try
            {
                tap.dat.Companies parentCompany = new dat.Companies();
                tap.dat.Companies currentCompany = new dat.Companies();

                if (companyId > 0 && parentCompanyId > 0)
                {

                    parentCompany = _db.Companies.Where(a => a.CompanyID == parentCompanyId).ToList().FirstOrDefault();
                    currentCompany = _db.Companies.Where(a => a.CompanyID == companyId).ToList().FirstOrDefault();

                    currentCompany.BannerLogo = parentCompany.BannerLogo;
                    currentCompany.ReportLogo = parentCompany.ReportLogo;
                    currentCompany.CheckADSecurity = parentCompany.CheckADSecurity;

                    _db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
              


        #region Save Custom Tabs
        //Save Custom Tabs
        public void SaveCustomTabs(List<tap.dat.Tabs> tabs, int parentCompanyId, int companyId)
        {
            foreach (tap.dat.Tabs item in tabs)
            {
                tap.dat.Tabs newTab = new dat.Tabs();

                newTab.CompanyID = companyId;
                newTab.TabName = item.TabName;
                newTab.SortOrder = item.SortOrder;
                newTab.Active = item.Active;
                _db.AddToTabs(newTab);
                _db.SaveChanges();

                List<tap.dat.TabModules> tabmodules = new List<dat.TabModules>();

                tabmodules = _db.TabModules.Where(a => a.TabID == item.TabID).ToList();

                if (tabmodules != null && tabmodules.Count > 0)
                {
                    SaveCustomTabModules(tabmodules, newTab.TabID, parentCompanyId, companyId);
                }

                // to copy fields of custom tab
                CustomTabFields(item.TabID, newTab.TabID);

                tap.dat.Inheritance inheritance = new dat.Inheritance();
                inheritance.CompanyId = companyId;
                inheritance.ParentCompanyId = parentCompanyId;
                inheritance.ParentTabId = item.TabID;
                inheritance.TabId = newTab.TabID;
                _db.AddToInheritance(inheritance);
                _db.SaveChanges();

            }

        }
        #endregion

        //Save Custom Tab Modules
        public void SaveCustomTabModules(List<tap.dat.TabModules> tabmodules, int tabId,  int parentCompanyId, int companyId)
        {
            foreach (tap.dat.TabModules subitem in tabmodules)
            {
                tap.dat.TabModules newTabModule = new dat.TabModules();
                newTabModule.TabID = tabId;
                newTabModule.ModuleID = subitem.ModuleID;
                newTabModule.SortOrder = subitem.SortOrder;
                _db.AddToTabModules(newTabModule);
                _db.SaveChanges();
            }
        }

        //To Custom tab fields copy from Parent Company
        public void CustomTabFields(int parentTabId, int tabId)
        {
            try
            {
                if (parentTabId == 0 || parentTabId == null)
                    return;

                List<tap.dat.Fields> fieldList = new List<dat.Fields>();
                fieldList = _db.Fields.Where(a => a.TabID == parentTabId).ToList();

                if (fieldList == null || fieldList.Count == 0)
                    return;

                foreach (tap.dat.Fields field in fieldList)
                {
                    tap.dat.Fields newField = new dat.Fields();
                    newField.TabID = tabId;
                    newField.SortOrder = field.SortOrder;
                    newField.DisplayName = field.DisplayName;
                    newField.Datatype = field.Datatype;
                    newField.MaxLength = field.MaxLength;
                    newField.ControlType = field.ControlType;
                    newField.IsRequired = field.IsRequired;
                    newField.IsActive = field.IsActive;
                    newField.ListID = field.ListID == 0 ? null : field.ListID;
                    newField.Description = field.Description;
                    _db.AddToFields(newField);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        //Attachment Folders inherit
        public void AttachmnetFolders(int companyId, int parentCompanyId)
        {
            try
            {
                tap.dat.AttachmentFolders folders = new dat.AttachmentFolders();

                if (parentCompanyId <= 0)
                    return;
                  folders = _db.AttachmentFolders.Where(a => a.CompanyId == parentCompanyId && a.Active == true && a.ParentId == null).FirstOrDefault();
                  if (folders == null)
                      return;

                  tap.dat.AttachmentFolders newFolder = new dat.AttachmentFolders();

                  newFolder.CompanyId = companyId;
                  newFolder.FolderName = folders.FolderName;
                  newFolder.FullPath = folders.FullPath;
                  newFolder.SortOrder = folders.SortOrder;
                  newFolder.Active = folders.Active;
                  newFolder.ParentId = null;

                  _db.AddToAttachmentFolders(newFolder);
                  _db.SaveChanges();

                  GetChildAttachmentFolder(companyId, folders.AttachementFolderId, newFolder.AttachementFolderId);

                  tap.dat.Inheritance inheritance = new dat.Inheritance();
                  inheritance.CompanyId = companyId;
                  inheritance.ParentCompanyId = parentCompanyId;
                  inheritance.ParentAttachmentFolderId = folders.AttachementFolderId;
                  inheritance.AttachmentFolderId = newFolder.AttachementFolderId;
                  _db.AddToInheritance(inheritance);
                  _db.SaveChanges();
                
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        //Child Attachment Folders inherit
        public void GetChildAttachmentFolder(int companyId, int parentFolderId, int newParentFolderId)
        {
            try
            {
                List<tap.dat.AttachmentFolders> folders = new List<dat.AttachmentFolders>();

                if (parentFolderId <= 0)
                    return;
                folders = _db.AttachmentFolders.Where(a => a.ParentId == parentFolderId && a.Active == true).ToList();
                if (folders == null || folders.Count == 0)
                    return;

                foreach (tap.dat.AttachmentFolders folder in folders)
                {
                    tap.dat.AttachmentFolders newFolder = new dat.AttachmentFolders();

                    newFolder.CompanyId = companyId;
                    newFolder.FolderName = folder.FolderName;
                    newFolder.FullPath = folder.FullPath;
                    newFolder.SortOrder = folder.SortOrder;
                    newFolder.Active = folder.Active;
                    newFolder.ParentId = newParentFolderId;

                    _db.AddToAttachmentFolders(newFolder);
                    _db.SaveChanges();

                    List<tap.dat.AttachmentFolders> folderList = new List<dat.AttachmentFolders>();
                    folderList = _db.AttachmentFolders.Where(a => a.ParentId == folder.AttachementFolderId && a.Active == true).ToList();
                    if(folderList.Count >0 )
                        GetChildAttachmentFolder(companyId, folder.AttachementFolderId, newFolder.AttachementFolderId);
                }


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        //System List
        public void SystemLists(int companyId, int parentCompanyId)
        {
            try
            {
                List<tap.dat.SystemLists> lists = new List<dat.SystemLists>();

                if (parentCompanyId > 0)
                {
                    lists = _db.SystemLists.Where(a => a.CompanyID == parentCompanyId && a.Active == true && a.IsLocation == null).OrderBy(x=>x.ListID).ToList();

                    if (lists == null)
                        return;

                    SaveList(lists, parentCompanyId, companyId);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
        //Save System List
        public void SaveList(List<tap.dat.SystemLists> list, int parentCompanyId, int companyId)
        {
            foreach (tap.dat.SystemLists item in list)
            {
                tap.dat.SystemLists newList = new dat.SystemLists();
                newList.CompanyID = companyId;
                newList.ListName = item.ListName;
                newList.IsSystemList = item.IsSystemList;
                newList.Active = item.Active;
                newList.IsEditable = item.IsEditable;
                newList.Description = item.Description;

                if (item.IsClassification == true)
                    newList.IsClassification = true;

                _db.AddToSystemLists(newList);
                _db.SaveChanges();

                List<tap.dat.ListValues> listItem = _db.ListValues.Where(a => a.ListID == item.ListID && a.Parent == null).ToList();
                if (listItem != null)
                    SaveListValues(listItem, newList.ListID, parentCompanyId, companyId);

                tap.dat.Inheritance inheritance = new dat.Inheritance();
                inheritance.CompanyId = companyId;
                inheritance.ParentCompanyId = parentCompanyId;
                inheritance.ParentListId = item.ListID;
                inheritance.ListId = newList.ListID;
                _db.AddToInheritance(inheritance);
                _db.SaveChanges();

            }

        }

        //Save List Values
        public void SaveListValues(List<tap.dat.ListValues> list, int listId,  int parentCompanyId, int companyId)
        {
            foreach (tap.dat.ListValues subItem in list)
            {
                tap.dat.ListValues newSubList = new dat.ListValues();
                newSubList.ListID = listId;
                newSubList.ListValueName = subItem.ListValueName;
                newSubList.FullPath = subItem.FullPath;
                newSubList.Active = subItem.Active;
                newSubList.Parent = null;
                _db.AddToListValues(newSubList);
                _db.SaveChanges();

                CopyListItems(listId, subItem.ListValueID, newSubList.ListValueID);
            }
        }

        public void CopyListItems(int listId, int listValueId, int currentParentId)
        {
            tap.dat.ListValues listDetails = new tap.dat.ListValues();
            string Id = null;
            List<tap.dat.ListValues> list = new List<dat.ListValues>();

            list = _db.ListValues.Where(x => x.Parent == listValueId).ToList();

            if (list.Count > 0)
            {
                foreach (tap.dat.ListValues item in list)
                {
                    Id = item.ListValueID.ToString();
                    tap.dat.ListValues newList = new tap.dat.ListValues();

                    newList.ListValueName = item.ListValueName;
                    newList.ListID = listId;
                    newList.Parent = currentParentId;
                    newList.Active = true;
                    newList.FullPath = item.FullPath;
                    newList.Value = item.Value;
                    _db.AddToListValues(newList);
                    _db.SaveChanges();

                    List<tap.dat.ListValues> list1 = new List<dat.ListValues>();
                    list1 = _db.ListValues.Where(x => x.Parent == item.ListValueID).ToList();
                    if (list1 != null && list1.Count > 0)
                        CopyListItems(listId, item.ListValueID, newList.ListValueID);
                }
            }
        }

        //Save copy template for report
        public void SystemReports(int companyId, int parentCompanyId, int userId)
        {
            try
            {
                int CUSTOM_FIELD = _db.ReportElementType.Where(a => a.ElementType.ToLower() == "custom field").FirstOrDefault().ElementTypeID;
                int DATA_FIELD = _db.ReportElementType.Where(a => a.ElementType.ToLower() == "data field").FirstOrDefault().ElementTypeID;


                List<tap.dat.Report> reportList = new List<dat.Report>();

                if (parentCompanyId <= 0)
                    return;

                reportList = _db.Report.Where(a => a.CompanyID == parentCompanyId && a.EventID == 0).ToList();


                if (reportList == null)
                    return;

                foreach (tap.dat.Report report in reportList)
                {
                    tap.dat.Report newReport = new dat.Report();

                    newReport.ReportName = report.ReportName;
                    newReport.Description = report.Description;
                    newReport.CompanyID = companyId;
                    newReport.CreatedBy = userId;
                    newReport.CreatedDate = DateTime.UtcNow;
                    newReport.ReportTitle = report.ReportTitle;
                    newReport.IsCompanyLogoUsed = report.IsCompanyLogoUsed;
                    newReport.isCompanyNameUsed = report.isCompanyNameUsed;
                    newReport.isReportTitleUsed = report.isReportTitleUsed;
                    newReport.isCreatedDateUsed = report.isCreatedDateUsed;
                    newReport.ReportTypeId = report.ReportTypeId;
                    newReport.isTemplate = report.isTemplate;                
                    newReport.isTemporaryReport = report.isTemporaryReport;
                    newReport.ReportTitleStyle = report.ReportTitleStyle;
                    newReport.EventID = report.EventID;
                    _db.AddToReport(newReport);
                    _db.SaveChanges();

                    List<tap.dat.ReportElement> reportElementList = new List<dat.ReportElement>();

                    reportElementList = _db.ReportElement.Where(a => a.ReportID == report.ReportID).ToList();
                    if (reportElementList == null)
                        return;

                    foreach (tap.dat.ReportElement reportElement in reportElementList)
                    {
                        tap.dat.ReportElement newReportElement = new tap.dat.ReportElement();
                        newReportElement.ReportID = newReport.ReportID;
                        newReportElement.ElementTypeID = reportElement.ElementTypeID;

                        newReportElement.ElementText = reportElement.ElementText;
                        newReportElement.ElementStyle = reportElement.ElementStyle;
                        newReportElement.SortOrder = reportElement.SortOrder;
                        newReportElement.TopPosition = reportElement.TopPosition;
                        newReportElement.LeftPosition = reportElement.LeftPosition;
                        newReportElement.ReportImageData = reportElement.ReportImageData;
                        newReportElement.IntegrateType = reportElement.IntegrateType;

                        _db.AddToReportElement(newReportElement);
                        _db.SaveChanges();

                        if (reportElement.ElementTypeID == CUSTOM_FIELD)
                        {
                            SaveReportElementCustomSourceDetails(reportElement.ElementID, newReportElement.ElementID);
                        }

                        if (reportElement.ElementTypeID == DATA_FIELD)
                        {
                            SaveReportElementDataSourceDetails(reportElement.ElementID, newReportElement.ElementID);
                        }

                    }

                    tap.dat.Inheritance inheritance = new dat.Inheritance();
                    inheritance.CompanyId = companyId;
                    inheritance.ParentCompanyId = parentCompanyId;
                    inheritance.ParentReportId = report.ReportID;
                    inheritance.ReportId = newReport.ReportID;
                    _db.AddToInheritance(inheritance);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        public void SaveReportElementDataSourceDetails(int elementId, int newElementId)
        {
            tap.dat.ReportElementDataSource reportElementDS = _db.ReportElementDataSource.Where(a => a.ReportElementID == elementId).FirstOrDefault();
            if (reportElementDS == null)
                return;

            tap.dat.ReportElementDataSource newReportElementDS = new dat.ReportElementDataSource();
            newReportElementDS.ReportElementID = newElementId;

            newReportElementDS.DataSourceColumnName = reportElementDS.DataSourceColumnName;

            newReportElementDS.DataSourceTableName = reportElementDS.DataSourceTableName;

            _db.AddToReportElementDataSource(newReportElementDS);
            _db.SaveChanges();
        }

        public void SaveReportElementCustomSourceDetails(int elementId, int newElementId)
        {

            try
            {
                tap.dat.ReportElementCustomSource reportElementCSDetails = _db.ReportElementCustomSource.Where(a => a.ReportElementID == elementId).FirstOrDefault();
                if (reportElementCSDetails == null)
                    return;

                tap.dat.ReportElementCustomSource reportElementCS = new tap.dat.ReportElementCustomSource();
                reportElementCS.ReportElementID = newElementId;
                reportElementCS.CustomFieldID = reportElementCSDetails.CustomFieldID == 0 ? null : reportElementCSDetails.CustomFieldID;                
                _db.AddToReportElementCustomSource(reportElementCS);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

            }
        }

        //To Details tab fields copy from Parent Company
        public void DetailTabFields(int companyId, int parentCompanyId)
        {
            try
            {
                int parentDetailTabId = _db.Tabs.Where(a => a.CompanyID == parentCompanyId && a.TabName.ToLower() == "details").FirstOrDefault().TabID;
                if (parentDetailTabId == 0 || parentDetailTabId ==null)
                    return;

                int TabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.TabName.ToLower() == "details").FirstOrDefault().TabID;

                List<tap.dat.Fields> fieldList = new List<dat.Fields>();
                fieldList = _db.Fields.Where(a => a.TabID == parentDetailTabId).ToList();

                if (fieldList == null || fieldList.Count == 0)
                    return;

                foreach (tap.dat.Fields field in fieldList)
                {
                    tap.dat.Fields newField = new dat.Fields();
                    newField.TabID = TabId;
                    newField.SortOrder = field.SortOrder;
                    newField.DisplayName = field.DisplayName;
                    newField.Datatype = field.Datatype;
                    newField.MaxLength = field.MaxLength;
                    newField.ControlType = field.ControlType;
                    newField.IsRequired = field.IsRequired;
                    newField.IsActive = field.IsActive;
                    newField.ListID = field.ListID == 0 ? null : field.ListID;
                    newField.Description = field.Description;
                    _db.AddToFields(newField);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
    }
}
