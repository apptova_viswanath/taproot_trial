﻿
//---------------------------------------------------------------------------------------------
// File Name 	: Tab.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Tabs
//---------------------------------------------------------------------------------------------

#region "Namespaces"

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using tap.dom.hlp;

#endregion

namespace tap.dom.adm
{
    public class Tab
    {
        tap.dat.EFEntity _db = new dat.EFEntity();


        #region "To get The Tablist based on moduleid"

        /// <summary>
        /// Get Tabs by considering Module,company,Active and Universal
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<tap.dat.Tabs> GetTabList(int moduleId, int companyId)
        {
            List<tap.dat.Tabs> tabList = null;
            try
            {
                tabList = ((from a in _db.Tabs
                            join b in _db.TabModules
                                on a.TabID equals b.TabID
                            where a.Active == true && b.ModuleID == moduleId && a.CompanyID == companyId
                            select a)
                             .Union(from a in _db.Tabs where a.Active == true && a.IsUniversal == true select a)).ToList();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return tabList;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabs"></param>
        /// <param name="currentModuleName"></param>
        /// <param name="selectedModuleName"></param>
        /// <param name="virtualDirectoryPath"></param>
        /// <returns></returns>
        public string[] SaveTabDetails(tap.dat.Tabs tabs, string currentModuleName, string selectedModuleName, string baseURL, string previousTabName, int companyId, int userId)
        {
            string[] messageArray = new string[4];

            string existModuleName = string.Empty;
            bool isExist = false;

            isExist = CheckTabExist(tabs.TabName, selectedModuleName, companyId, out existModuleName); //Check for the Duplicate Tab Name insert operation

            if (tabs.TabID > 0 && tabs.TabName == previousTabName)
                isExist = false;

            if (isExist)
            {
                messageArray = GetResultArray(currentModuleName, tabs.TabID, "Tab already exists for " + existModuleName, baseURL, companyId, tabs.TabName);
                return messageArray;
            }

            if (tabs.TabID > 0)
            {
                SaveChanges(); //Update Tab details
                tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Update,
                "Tab '" + previousTabName + "' updated.");
            }
            else
            {


                InsertTab(ref tabs, userId); //Insert Tab details
            }

            if (!Convert.ToBoolean(tabs.IsUniversal))
            {
                SaveTabModuleDetails(tabs.TabID, selectedModuleName);

            }

            messageArray = GetResultArray(currentModuleName, tabs.TabID, string.Empty, baseURL, companyId, tabs.TabName);

            return messageArray;
        }

        #region "For Insert/Update Of Tab"

        /// <summary>
        /// Invoked to Save the tabDetails
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="companyId"></param>
        /// <param name="moduleId">id that selected from the module dropdown</param>
        /// <param name="tabName"></param>
        /// <param name="isUniversal"></param>
        /// <param name="isSystem"></param>
        /// <param name="sortOrder"></param>
        /// <param name="active"></param>
        /// <param name="moduleName">The current displaying module</param>
        public string[] SaveTab(int tabId, string companyID, string tabName, string currentModuleName, bool isUniversal, bool isSystem, int sortOrder,
                                bool active, string selectedModuleName, string baseURL, bool restrictedAccess, int userId)
        {

            // bool isExist = false;
            string[] messageArray = new string[4];
            tap.dat.Tabs tabs = new tap.dat.Tabs();

            try
            {
                //Get tab by tabid
                if (tabId > (int)tap.dom.hlp.Enumeration.Types.NewInsert)
                {
                    tabs = GetTabInfoById(tabId);
                }

                string previousTabName = tabs.TabName;
                SetValues(ref tabs, companyID, tabName, isUniversal, isSystem, sortOrder, active, restrictedAccess);

                return SaveTabDetails(tabs, currentModuleName, selectedModuleName, baseURL, previousTabName, Convert.ToInt32(companyID), userId);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                messageArray = GetResultArray(currentModuleName, tabs.TabID, string.Empty, baseURL, Convert.ToInt32(companyID), tabName);
            }

            return messageArray;
        }

        /// <summary>
        /// Svae tab module details
        /// </summary>
        /// <param name="tabID"></param>
        /// <param name="selectedModuleName"></param>
        public void SaveTabModuleDetails(int tabID, string selectedModuleName)
        {
            TabModules tabModules = new TabModules();
            string[] moduleDetails = selectedModuleName.Split(',');
            string moduleName = string.Empty;

            int index = 0;

            for (index = 0; index <= moduleDetails.Length - 1; index++)
            {
                moduleName = moduleDetails[index].Split(':')[0];
                bool isModuleChecked = (moduleDetails[index].Split(':')[1]) == "checked" ? true : false;
                int moduleId = (moduleName.Contains("Incident")) ? Convert.ToInt32(Enumeration.Modules.Incident)
                                                                : (moduleName.Contains("Investigation")) ? Convert.ToInt32(Enumeration.Modules.Investigation)
                                                                : (moduleName.Contains("Audit")) ? Convert.ToInt32(Enumeration.Modules.Audit)
                                                                : (moduleName.Contains("CAP")) ? Convert.ToInt32(Enumeration.Modules.ActionPlan)
                                                                : (moduleName.Contains("Action Plan")) ? Convert.ToInt32(Enumeration.Modules.ActionPlan) : 0;
                if (isModuleChecked)
                {
                    tabModules.SaveTabModules(tabID, moduleId);
                }
                else
                {
                    tabModules.DeleteTabModules(tabID, moduleId);
                }

            }
        }

        /// <summary>
        /// Svae tab module order details
        /// </summary>
        /// <param name="moduleID"></param>
        /// <param name="tabsOrder"></param>
        public void SaveTabModuleOrder(int moduleID, string tabsOrder)
        {

            if (moduleID > 0 && !string.IsNullOrEmpty(tabsOrder))
            {
                try
                {
                    string[] tabsOrderArray = tabsOrder.Split(',');
                    if (tabsOrderArray != null)
                    {
                        foreach (string item in tabsOrderArray)
                        {
                            string[] itemArray = item.Split('-');
                            if (itemArray != null)
                            {
                                int tabID = 0;
                                int order = 0;
                                int.TryParse(itemArray[1].ToString(), out tabID);
                                int.TryParse(itemArray[0].ToString(), out order);
                                if (tabID > 0)
                                {
                                    tap.dat.TabModules tabs = _db.TabModules.FirstOrDefault(a => a.TabID == tabID && a.ModuleID == moduleID);
                                    if (tabs != null)
                                    {
                                        tabs.SortOrder = order;
                                        SaveChanges();
                                    }
                                    //tabModules.SaveTabModulesOrder(tabID, moduleID, order);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    tap.dom.hlp.ErrorLog.LogException(ex);
                }
            }
        }

        /// <summary>
        /// Insert new tab entry into table Tabs
        /// </summary>
        /// <param name="tabs"></param>
        public void InsertTab(ref tap.dat.Tabs tabs, int userId)
        {
            _db.AddToTabs(tabs);
            SaveChanges();
            tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Create,
                "New tab '" + tabs.TabName + "' created.");

        }


        /// <summary>
        /// Get result array to pass to the ajax calling method
        /// </summary>
        /// <param name="moduleName"></param>
        /// <param name="tabID"></param>
        /// <param name="isInsert"></param>
        /// <returns></returns>
        public string[] GetResultArray(string moduleName, int tabId, string displayMessage, string baseURL, int companyId, string tabName)
        {

            string message = (displayMessage == string.Empty) ? tabName + " saved successfully" : displayMessage;

            string[] resultArray = new string[4];
            tap.dom.tab.AdminTabs adminTab = new dom.tab.AdminTabs();

            string mainTab = adminTab.BuildHTMlMainTabs(baseURL, 0);

            int moduleId = CommonOperation.GetModuleIdByName(moduleName);

            string subTab = adminTab.BuildHTMLSubTabs(baseURL, moduleId, companyId, 0);

            resultArray[0] = message.ToString();
            resultArray[1] = mainTab;
            resultArray[2] = subTab;
            resultArray[3] = tabId.ToString();

            return resultArray;
        }


        /// <summary>
        ///  Set the property values for the tab object with form values to insert/update the enity
        /// </summary>
        /// <param name="tabs"></param>
        /// <param name="companyId"></param>
        /// <param name="moduleId"></param>
        /// <param name="tabName"></param>
        /// <param name="isUniversal"></param>
        /// <param name="isSystem"></param>
        /// <param name="sortOrder"></param>
        /// <param name="active"></param>
        public void SetValues(ref tap.dat.Tabs tabs, string companyID, string tabName, bool isUniversal, bool isSystem,
                              int sortOrder, bool active, bool restrictedAccess)
        {
            try
            {
                if (tabs == null)
                    tabs = new tap.dat.Tabs();

                //Decrypt the value and pass
                int companyId = Convert.ToInt32(companyID);

                //Set the properties
                tabs.CompanyID = companyId;
                tabs.TabName = tabName;
                tabs.IsUniversal = isUniversal;
                tabs.IsSystem = isSystem;
                tabs.SortOrder = sortOrder;
                tabs.Active = active;
                tabs.isRestricted = restrictedAccess;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
        #endregion

        #region "To check Tab Name exists or not"

        /// <summary>
        /// Check if the tab exists or not by tabname and moduleid
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public bool CheckTabExist(string tabName, string selectedModuleName, int companyId, out string existModuleName)
        {
            bool isTabExist = false;
            int index = 0;
            int moduleId = 0;
            existModuleName = string.Empty;
            string moduleName = string.Empty;
            string[] selectedModuleDetails = selectedModuleName.Split(',');
            bool isModuleChecked = false;

            try
            {
                //All modules sleected               
                var tabData = _db.Tabs.FirstOrDefault(a => (a.TabName.Trim() == tabName && a.CompanyID == companyId));

                if (tabData != null && (Convert.ToBoolean(tabData.IsUniversal)))
                {
                    isTabExist = true;
                    existModuleName += "all Modules";
                    return isTabExist;
                }

                // some of teh moduels selected
                for (index = 0; index <= selectedModuleDetails.Length - 1; index++)
                {

                    moduleName = selectedModuleDetails[index].Split(':')[0];
                    isModuleChecked = (selectedModuleDetails[index].Split(':')[1]) == "checked" ? true : false;
                    moduleId = CommonOperation.GetModuleIdByName(moduleName);

                    if (isModuleChecked)
                    {
                        var tabDetails = (from tabModules in _db.TabModules
                                          join tabs in _db.Tabs
                                          on tabModules.TabID equals tabs.TabID
                                          where tabs.CompanyID == companyId
                                          select new { tabModules, tabs });


                        var tabModuleDetails = tabDetails.FirstOrDefault(a => (a.tabs.TabName.Trim() == tabName));

                        if (tabModuleDetails != null)
                        {
                            var getmoduleName = (from tab in _db.Tabs
                                                 join tabModule in _db.TabModules on tab.TabID equals tabModule.TabID
                                                 join module in _db.Modules on tabModule.ModuleID equals module.ModuleID
                                                 where tab.TabName == tabName && tab.CompanyID == companyId
                                                 select new
                                                 {
                                                     ModuleName = module.ModuleName
                                                 });

                            isTabExist = true;
                            if (getmoduleName.Count() != 0)
                            {
                                existModuleName = string.Empty;
                                foreach (var x in getmoduleName)
                                {
                                    existModuleName += (existModuleName.Length > 0) ? ", " + x.ModuleName : x.ModuleName;

                                }
                            }


                        }
                    }
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isTabExist;
        }

        #endregion

        #region "To Delete The Tab By ID"

        /// <summary>
        /// Invoked to Delete the Tab 
        /// </summary>
        /// <param name="tabId"></param>
        public void DeleteTabByID(int tabId, int userId)
        {
            try
            {
                //Get the tab by id
                tap.dat.Tabs tabs = GetTabInfoById(tabId);
                tabs.IsDeleted = true;
                SaveChanges();

                TabModules tabModules = new TabModules();
                tabModules.DeleteTabModulesByTabId(tabId);

                tap.dom.transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Delete,
                "Tab '" + tabs.TabName + "' deleted.");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #endregion

        #region "To Get the Tab By ID"

        public tap.dat.Tabs GetTabInfoById(int tabId)
        {
            tap.dat.Tabs tabs = new tap.dat.Tabs();
            tabs = _db.Tabs.FirstOrDefault(a => a.TabID == tabId);

            return tabs;
        }

        /// <summary>
        /// Invoked to Get the Tab By ID
        /// </summary>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public object GetTabDetailsByID(int tabId, int moduleId)
        {
            tap.dat.Tabs tabs = new tap.dat.Tabs();
            tap.dat.TabModules mod = new tap.dat.TabModules();

            try
            {
                tabs = _db.Tabs.FirstOrDefault(a => a.TabID == tabId);

                if (!Convert.ToBoolean(tabs.IsUniversal))
                {
                    var tabModuleDetails = (from tabModules in _db.TabModules
                                            join modules in _db.Modules
                                            on tabModules.ModuleID equals modules.ModuleID
                                            where tabModules.TabID == tabId
                                            select new { tabModules, modules }).ToList();

                    string selectedModuleName = string.Empty;

                    foreach (var tabModule in tabModuleDetails)
                    {
                        selectedModuleName += tabModule.modules.ModuleName + ",";
                    }

                    //selectedModuleName = selectedModuleName.Replace("Corrective Action Plan", "CAP");
                    selectedModuleName = selectedModuleName.Replace("Corrective Action Plan", "ActionPlan");

                    var tabDetails = (from a in tabModuleDetails
                                      join b in _db.Tabs
                                      on a.tabModules.TabID equals b.TabID
                                      where a.tabModules.ModuleID == moduleId
                                      select new
                                      {
                                          TabID = b.TabID,
                                          CompanyID = b.CompanyID,
                                          TabName = b.TabName,
                                          IsUniversal = b.IsUniversal,
                                          IsSystem = b.IsSystem,
                                          SortOrder = b.SortOrder,
                                          Active = b.Active,
                                          IsDeleted = b.IsDeleted,
                                          SelectedModuleName = selectedModuleName,
                                          IsRestricted = b.isRestricted
                                      }).FirstOrDefault();

                    return tabDetails;
                }

                return tabs;

            }

            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        #endregion

        #region "To Get the Tab By Tabname"

        /// <summary>
        /// Get Tab By tab Name
        /// </summary>
        /// <param name="tabName"></param>
        /// <returns></returns>
        public tap.dat.Tabs GetTabByName(string tabName)
        {
            tap.dat.Tabs tabs = new tap.dat.Tabs();

            try
            {
                tabs = _db.Tabs.First(a => a.TabName == tabName);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return tabs;
        }

        #endregion

        # region "To update Subtabs sort order"

        /// <summary>
        /// To update Subtabs sort order
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="subTabOrder"></param>
        /// <returns></returns>
        public bool SubTabsSortUpdate(string[] tabId, string[] subTabOrder)
        {
            bool isUpdate = false;
            int index = 0;

            try
            {
                foreach (string currentTabId in tabId)
                {
                    tap.dat.Tabs tabs = _db.Tabs.FirstOrDefault(a => a.TabID == Convert.ToInt32(currentTabId));

                    if (tabs != null)
                    {
                        tabs.SortOrder = Convert.ToInt32(subTabOrder[index]);
                        SaveChanges();
                        isUpdate = true;
                    }

                    index++;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isUpdate;
        }


        /// <summary>
        /// Commit changes to database
        /// </summary>
        public void SaveChanges()
        {
            _db.SaveChanges();
        }

        #endregion

        #region To Get CustomTab list

        /// <summary>
        /// Get Custom Tabs by company id
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<tap.dat.Tabs> GetCustomTabs(int companyId)
        {
            List<tap.dat.Tabs> tabList = null;

            try
            {
                tabList = (from a in _db.Tabs
                           where a.CompanyID == companyId && a.Active == true && a.IsUniversal == false
                           select a).ToList();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return tabList;
        }

        #endregion
    }
}


