﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.adm
{
   public  class Licensing
    {
       tap.dat.EFEntity _db = new tap.dat.EFEntity();

        #region For Insert/Update operation of Licensing Table
       /// <summary>
       /// Decsription-Save licensing Data
       /// </summary>
       /// <param name="licenseId"></param>
       /// <param name="companyId"></param>
       /// <param name="licenseCount"></param>
       /// <param name="enrollmentDate"></param>
       /// <param name="disEnrollmentDate"></param>
        public void SaveLicensing(int licenseId, int companyId, int licenseCount, DateTime enrollmentDate, DateTime disEnrollmentDate)
        {
            try
            {
                tap.dat.Licensing licensing = null;
               licensing = ((licenseId == -1) ? new tap.dat.Licensing() : _db.Licensing.First(x => x.LicenseID == licenseId));

               licensing.CompanyID = companyId;
               licensing.NoOfLicense = licenseCount;
               licensing.EnrollmentDate = enrollmentDate;
               licensing.DisenrollmentDate = disEnrollmentDate;

               if (licenseId == -1)
                    _db.AddToLicensing(licensing);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region To Delete the Licensing By ID
       /// <summary>
       /// Description-Delete Licensing Based on id
       /// </summary>
       /// <param name="licenseId"></param>
        public void DeleteLicenseByID(int licenseId)
        {
            try
            {
                tap.dat.Licensing licensing = _db.Licensing.First(x => x.LicenseID == licenseId);
                _db.DeleteObject(licensing);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region To Get the License By ID
       /// <summary>
       /// Description-Get Licensing by primary Key
       /// </summary>
       /// <param name="licenseId"></param>
       /// <returns></returns>
        public tap.dat.Licensing GetLicensingByID(int licenseId)
        {
            tap.dat.Licensing licensing = _db.Licensing.First(x => x.LicenseID == licenseId);
            return licensing;
        }
        #endregion

        #region To Get The License By List
        public List<tap.dat.Licensing> GetLicensingByList()
        {
            List<tap.dat.Licensing> licenseList = new List<tap.dat.Licensing>();
            licenseList = _db.Licensing.ToList();
            return licenseList;
        }
        #endregion
    }
}
