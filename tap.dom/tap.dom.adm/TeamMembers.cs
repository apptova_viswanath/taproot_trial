﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;

namespace tap.dom.adm
{
    public class TeamMembers
    {
        #region "Global variables"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        public const string EDIT = "Edit";
        public const string VIEW = "View";
        public const string DISPLAY_ON_REPORT = "DisplayOnReport";
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        #endregion

        # region "To get The Team Member List"
        /// <summary>
        /// GetTeamMemberList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetTeamMemberList(string page, string rows, string sidx, string sord, int companyId, string eventID)
        {
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                int eventUserId = 0;
                tap.dat.Events events = new dat.Events();
                if (eventId > 0)
                    events = _db.Events.FirstOrDefault(a => a.EventID == eventId);
                if (events != null)
                    eventUserId = Convert.ToInt32(events.CreatedBy);

                var shareListsDetails =  (from tm in _db.TeamMembers.AsEnumerable()
                                               where tm.EventID == eventId 
                                             select new
                                              {
                                                    TeamMemberID= tm.TeamMemberID,
                                                    UserID = tm.UserID,    
                                                    //FullName=tm.FullName,
                                                    FullName = UserFullName(tm.TeamMemberID, companyId),
                                                    hasView = tm.hasView,
                                                    hasEdit=tm.hasEdit,
                                                    isDisplayOnReport= tm.isDisplayOnReport,
                                                    isManualAdd=tm.isManualAdd,
                                                    isOwner = (tm.UserID == eventUserId)?true:false
                                               }
                                         );

                switch (sidx)
                {
                    case "FullName":
                        shareListsDetails = ((sord.Equals("desc")) ? shareListsDetails.OrderByDescending(e => e.FullName) : shareListsDetails.OrderBy(e => e.FullName));
                        break;

                    case "hasView":
                        shareListsDetails = ((sord.Equals("desc")) ? shareListsDetails.OrderByDescending(e => e.hasView == true ? "1" : "0") : shareListsDetails.OrderBy(e => e.hasView == true ? "1" : "0"));
                        break;
                    case "hasEdit":
                        shareListsDetails = ((sord.Equals("desc")) ? shareListsDetails.OrderByDescending(e => e.hasEdit == true ? "1" : "0") : shareListsDetails.OrderBy(e => e.hasEdit == true ? "1" : "0"));
                        break;
                    case "isDisplayOnReport":
                        shareListsDetails = ((sord.Equals("desc")) ? shareListsDetails.OrderByDescending(e => e.isDisplayOnReport == true ? "1" : "0") : shareListsDetails.OrderBy(e => e.isDisplayOnReport == true ? "1" : "0"));
                        break;
                }
                //for sorting
                string sortByColumnName = sidx ?? "FullName";
                string sortDirection = sord ?? "desc";

                var data = shareListsDetails.ToList();

                //pagination
                int totalRecords = 0;

                var currentPazeSize = rows ?? "15";
                int pageSize = int.Parse(currentPazeSize);

                if (data.Count != 0)
                    totalRecords = data.Count();

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var thisPage = page ?? "1";
                int currentPage = int.Parse(thisPage);

                if (currentPage > 1 && data.Count != 0)
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                var jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = (
                        from s in data
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = s.TeamMemberID.ToString(),
                            cell = new string[] { 
                            s.UserID.ToString(),
                            s.FullName, 
                            s.hasView.ToString(),
                            s.hasEdit.ToString(),
                            s.isDisplayOnReport.ToString(),
                            "",
                            s.isManualAdd.ToString(),
                            s.isOwner.ToString()
                        }
                        }).ToArray()
                };

                return _efSerializer.EFSerialize(jsonData);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }

        # endregion

        //Delete Team member
        public string DeleteTeamMember(int tmID)
        {
            string result = "";
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            try
            {
                if (tmID > 0)
                {
                    details = _db.TeamMembers.Where(a => a.TeamMemberID == tmID).FirstOrDefault();
                    _db.DeleteObject(details);
                    _db.SaveChanges();
                    result =  details.FullName + " team member removed successfully.";
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        //Get User Full Name
        public string UserFullName(int tmID, int companyId)
        {
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            details = _db.TeamMembers.Where(a=>a.TeamMemberID == tmID).FirstOrDefault();

            return (!details.UserID.Equals(0) && details.UserID != null) ? _db.Users.Where(a => a.UserID == details.UserID && a.CompanyID == companyId).FirstOrDefault().FirstName + " " +
                                                    _db.Users.Where(a => a.UserID == details.UserID && a.CompanyID == companyId).FirstOrDefault().LastName : details.FullName;
        }

        //Update Team member
        public string UpdateTeamMember(int tmID, bool value, string col)
        {
            string result = "";
            string errorResult = "Can not remove this setting. Team memeber should have atleast one privillege.";

            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            try
            {

                if (tmID > 0)
                {
                    details = _db.TeamMembers.Where(a => a.TeamMemberID == tmID).FirstOrDefault();

                    if (!value)
                    {
                        if (col.Equals(EDIT))
                        {
                            if (details.hasView == false && details.isDisplayOnReport == false)
                                return errorResult;
                        }
                        if (col.Equals(VIEW))
                        {
                            if (details.hasEdit == false && details.isDisplayOnReport == false)
                                return errorResult;
                        }
                        if (col.Equals(DISPLAY_ON_REPORT))
                        {
                            if (details.hasView == false && details.hasEdit == false)
                                return errorResult;
                        }
                    }
                    if (col.Equals(EDIT))
                        details.hasEdit = value;

                    if (col.Equals(VIEW))
                        details.hasView = value;

                    if (col.Equals(DISPLAY_ON_REPORT))
                        details.isDisplayOnReport = value;

                    _db.SaveChanges();


                    result = "Team Member updated.";
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        public string UpdateTeamMember(int tmID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID)
        {
            string result = "";
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                if (tmID > 0)
                {
                    details = _db.TeamMembers.Where(a => a.TeamMemberID == tmID && a.EventID == eventId).FirstOrDefault();
                    if (details != null)
                    {
                        details.hasEdit = isEdit;
                        details.hasView = isView;
                        details.isDisplayOnReport = isDisplayOnReport;
                        result = details.FullName +  " team member updated successfully.";
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
        /// <summary>
        /// Add Team Member
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="companyID"></param>
        /// <param name="fullName"></param>
        /// <param name="isEdit"></param>
        /// <param name="isView"></param>
        /// <param name="isDisplayOnReport"></param>
        /// <returns></returns>
        public string AddTeamMember(int userID, bool isEdit, bool isView, bool isDisplayOnReport, string eventID)
        {
            string result = "";
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            tap.dat.TeamMembers newDetails = new tap.dat.TeamMembers();
            tap.dat.Users user = new tap.dat.Users();
            System.Int32 companyID = 0;
            string fullName = "";
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                if (userID > 0)
                {
                    user = _db.Users.FirstOrDefault(a => a.UserID == userID);
                    if (user != null)
                        fullName = user.FirstName + " " + user.LastName; companyID = Convert.ToInt32(user.CompanyID);

                    details = _db.TeamMembers.Where(a => a.UserID == userID && a.EventID == eventId).FirstOrDefault();
                    if (details == null)
                    {
                        newDetails.UserID = userID;
                        newDetails.CompanyID = companyID;
                        newDetails.FullName = fullName;
                        newDetails.hasEdit = isEdit;
                        newDetails.hasView = isView;
                        newDetails.isDisplayOnReport = isDisplayOnReport;
                        newDetails.isManualAdd = false;
                        newDetails.EventID = eventId;

                        _db.AddToTeamMembers(newDetails);
                        result = fullName + " team member added successfully.";
                    }
                    else
                    {
                        details.hasEdit = isEdit;
                        details.hasView = isView;
                        details.isDisplayOnReport = isDisplayOnReport;
                        result = fullName + " team member updated successfully.";
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        public string SaveTeamMember(int userID, bool value, string col, string eventID)
        {
            string result = "";
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            tap.dat.TeamMembers newDetails = new tap.dat.TeamMembers();
            tap.dat.Users user = new tap.dat.Users();
            System.Int32 companyID = 0;
            string fullName = "";
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                if(userID>0)
                {
                     user = _db.Users.FirstOrDefault(a => a.UserID == userID);
                    if (user != null)
                        fullName = user.FirstName + " " + user.LastName; companyID = Convert.ToInt32(user.CompanyID);

                    details = _db.TeamMembers.Where(a => a.UserID == userID && a.EventID == eventId).FirstOrDefault();
                    if (details == null)
                    {
                        newDetails.UserID = userID;
                        newDetails.CompanyID = companyID;
                        newDetails.FullName = fullName;
                        newDetails.hasEdit = col.Equals(EDIT) ? value : false;
                        newDetails.hasView = col.Equals(VIEW) ? value : false;
                        newDetails.isDisplayOnReport = col.Equals(DISPLAY_ON_REPORT) ? value : false;
                        newDetails.isManualAdd = false;
                        newDetails.EventID = eventId;

                        _db.AddToTeamMembers(newDetails);
                        result = fullName + " team member added successfully.";
                    }
                    else
                    {
                        details.hasEdit = col.Equals(EDIT) ? value : details.hasEdit;
                        details.hasView = col.Equals(VIEW) ? value : details.hasView;
                        details.isDisplayOnReport = col.Equals(DISPLAY_ON_REPORT) ? value : details.isDisplayOnReport;
                        result = fullName + " team member updated successfully.";
                    }
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
        # region "To get The Team User List"
        /// <summary>
        /// GetTeamUserList
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetTeamUserList(string page, string rows, string sidx, string sord, string basicSearch, int companyId)
        {
            try
            {

                var userLists = _db.Users.Where(d => d.CompanyID == companyId).OrderBy(d => d.FirstName);

                // if (basicSearch != null && basicSearch.Length > 0)
                //userLists = userLists.Where(a => a.FirstName.Contains(basicSearch));
                //for sorting
                string sortByColumnName = sidx ?? "FirstName";
                string sortDirection = sord ?? "desc";

                var data = userLists.ToList();

                //pagination
                int totalRecords = 0;

                var currentPazeSize = rows ?? "15";
                int pageSize = int.Parse(currentPazeSize);

                if (data.Count != 0)
                    totalRecords = data.Count();

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var thisPage = page ?? "1";
                int currentPage = int.Parse(thisPage);

                if (currentPage > 1 && data.Count != 0)
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                var jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = (
                        from s in data
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = s.UserID.ToString(),
                            cell = new string[] { 
                            s.UserID.ToString(),
                            s.UserName,
                            s.FirstName + " "+ s.LastName, 
                        }
                        }).ToArray()
                };

                return _efSerializer.EFSerialize(jsonData);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }

        # endregion

        /// <summary>
        /// Add team member manually
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public string AddTeamMemberManually(string fullName, string eventID)
        {
            string result = "";
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                if (!string.IsNullOrEmpty(fullName))
                {
                    details.FullName = fullName;
                    details.hasEdit = false;
                    details.hasView = false;
                    details.isDisplayOnReport = true;
                    details.isManualAdd = true;
                    details.EventID = eventId;

                    _db.AddToTeamMembers(details);
                    _db.SaveChanges();
                    result = fullName + " team member manually added successfully.";
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        public string[] GetTeamMemberAccess(int userId, string eventID, int companyId)
        {
            string[] resultArray = new string[3];
            tap.dat.Users user = new tap.dat.Users();
            tap.dat.TeamMembers details = new tap.dat.TeamMembers();
            
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                string isAdmin = null;
                user = _db.Users.FirstOrDefault(a => a.UserID == userId);
                if (user != null)
                    isAdmin = user.isAdministrator.ToString();

                if (isAdmin != null && isAdmin == "True")
                {
                    resultArray[0] = "True";
                    resultArray[1] = "True";
                    resultArray[2] = "True";
                    return resultArray;
                }
                details = _db.TeamMembers.FirstOrDefault(a => a.UserID == userId && a.EventID == eventId && a.CompanyID == companyId);
                if (details == null)
                    return resultArray;

                resultArray[0] = details.hasView.ToString();
                resultArray[1] = details.hasEdit.ToString();
                resultArray[2] = details.isDisplayOnReport.ToString();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return resultArray;
        }

        public bool isUserExistTM(string name, string eventID, int companyId)
        {
            bool result = false;
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            var details = _db.TeamMembers.FirstOrDefault(user => user.EventID == eventId && user.CompanyID == companyId && user.FullName.ToLower() == name.ToLower());
            if (details != null)
                return true;
            else
            {
                details = _db.TeamMembers.FirstOrDefault(user => user.EventID == eventId &&  user.FullName.ToLower() == name.ToLower());
                if (details != null)
                    return true;
            }
            return result;
        }
    }
}
