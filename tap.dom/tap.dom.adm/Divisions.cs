﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using tap.dom.hlp;

namespace tap.dom.adm
{
    public class Divisions
    {
        public string _isSU = ConfigurationManager.AppSettings["IsSU"];
        public const string LOCATION = "Location";
        public const string CLASSIFICATION = "Classification";
        public const string ENVIRONMENTAL = "Environmental";
        public const string QUALITY = "Quality";
        public const string SAFETY = "Safety";
        public const string ATTACHMENTS = "Attachments";
        public const int GENERAL_LANGUAGE_ID = 16; // For English
        public const string SERVER_PROTOCOL = "LDAP://";

        private tap.dat.EFEntity db = new tap.dat.EFEntity();
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();

        //To get divisions for grid
        public string DivisionDetails(int page, int rows, string filter, int ignoreCase, string sortIndex, string sortOrder, int companyID)
        {
            if (sortIndex == null)
                sortIndex = "Name";

            if (sortOrder == null)
                sortOrder = "desc";
            try
            {
                var CompanyId = from company in _db.Companies
                                where company.ParentId == companyID && company.Active == true
                                select company.CompanyID;
                if (CompanyId != null)
                {
                    var details = (from comp in _db.Companies
                                   join user in _db.Users
                                   on comp.CompanyID equals user.CompanyID
                                   into temp
                                   from t in temp.DefaultIfEmpty()
                                   where CompanyId.Contains(comp.CompanyID) && comp.Active == true
                                   && t.Active == true && t.isAdministrator == true
                                   orderby comp.CompanyName, t.dateCreated
                                   select new
                                   {
                                       DivisionId = comp.CompanyID,
                                       Name = _db.Companies.FirstOrDefault(x => x.CompanyID == comp.ParentId).CompanyName + " > " + comp.CompanyName,
                                       primaryUser = t.UserName,
                                       userEmail = t.Email
                                   });

                    #region To Add filter opeartion
                    if (filter != null && filter.Length > 0)
                    {
                        switch (sortIndex)
                        {
                            case "Name":
                                details = ((sortOrder.Equals("desc")) ?
                                    details.OrderByDescending(e => e.Name).Where(a => a.Name.ToLower().Contains(filter.ToLower())
                                        || a.primaryUser.ToLower().Contains(filter.ToLower()) || a.userEmail.ToLower().Contains(filter.ToLower()))
                                    : details.OrderBy(e => e.Name).Where(a => a.Name.ToLower().Contains(filter.ToLower())
                                        || a.primaryUser.ToLower().Contains(filter.ToLower()) || a.userEmail.ToLower().Contains(filter.ToLower())));
                                break;
                            case "primaryUser":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.primaryUser).Where(a => a.primaryUser.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.primaryUser).Where(a => a.primaryUser.ToLower().Contains(filter.ToLower())));
                                break;
                        }
                    }
                    else
                    {
                        switch (sortIndex)
                        {
                            case "Name":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.Name) : details.OrderBy(e => e.Name));
                                break;
                            case "primaryUser":
                                details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.primaryUser) : details.OrderBy(e => e.primaryUser));
                                break;
                        }
                    }
                    #endregion
                    int skipRowsCount = 0;
                    if (page != 1)
                        skipRowsCount = (page - 1) * rows;



                    int totalRecords = 0;
                    var currentPazeSize = rows.ToString() ?? "15";
                    int pageSize = int.Parse(currentPazeSize);

                    var data = details.ToList();

                    totalRecords = (data != null && data.Count() > 0) ? data.Count() : 0;

                    int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                    var currentPage1 = page.ToString() ?? "1";

                    int currentPageNumber = int.Parse(currentPage1);

                    if (currentPageNumber > 1 && data != null && data.Count() > 0)
                        data = data.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();


                    var jsonData = new tap.dom.hlp.jqGridJson();

                    jsonData = new tap.dom.hlp.jqGridJson()
                    {
                        total = totalPages,
                        page = currentPageNumber,
                        records = totalRecords,
                        rows = (
                            from company in data
                            let DivisionId = company.DivisionId.ToString()
                            let Name = company.Name
                            let primaryUser = company.primaryUser
                            select new tap.dom.hlp.jqGridRowJson
                            {
                                id = company.DivisionId.ToString(),
                                cell = new string[] {                            
                                company.DivisionId.ToString(),
                                company.Name,
                                company.primaryUser                                
                                }
                            }).ToArray()
                    };

                    return _efSerializer.EFSerialize(jsonData);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }

        //to save company with admin user at the set up time
        public string SaveCompanyWithAdmin(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website,
            string firstName, string lastName, string userEmail, string password, string userName, string userPhone, string subscriptionStart, string subscriptionEnd)
        {

            int UserId = 0;
            //tap.dat.Companies company = null;
            tap.dom.adm.UserDetails userDetails = new tap.dom.adm.UserDetails();
            tap.dom.adm.CommonOperation co = new tap.dom.adm.CommonOperation();
            int companyId = 0;
            try
            {
                //company = new tap.dat.Companies();
                //company = _db.Companies.Where(x => x.ParentId == parentCompanyId && x.CompanyName.ToLower() == companyName.ToLower()).FirstOrDefault();

                //if (company != null)
                //    return "Can not create same division name in one organization";

                if (userDetails.IsUserExists(userName))
                    return "User name " + userName + " already exists";

                //Check for email availability in total database.
                //if (companyEmail != "" && co.EmailAvailabilityCheck(companyEmail))
                //    return companyEmail + " already exists";

                if (co.EmailAvailabilityCheck(userEmail))
                    return "User " + userEmail + " already exists";

                //save company
                companyId = SaveCompany(parentCompanyId, companyName, phoneNumber, address, companyEmail, website, subscriptionStart, subscriptionEnd);

                if (companyId > 0)
                {
                    //save user
                    UserId = SaveUser(firstName, lastName, userEmail, userName, password, userPhone, companyId);

                    List<string> systemlist = new List<string>();

                    if (parentCompanyId.Equals(0))
                        systemlist.Add(CLASSIFICATION);

                    systemlist.Add(LOCATION);

                    CompanyDefaultDataDB(companyId);

                    if (!parentCompanyId.Equals(0))
                    {
                        InheritDatas(parentCompanyId, companyId, UserId);
                    }

                    // To set up default system list and list values
                    SaveDefaultSystemList(companyId, systemlist);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            if (parentCompanyId > 0)
                return "CompanyId:" + companyId.ToString();

            return companyId.ToString() + ":" + UserId.ToString();
        }

        public void InheritDatas(int parentCompanyId, int companyId, int userId)
        {
            tap.dom.adm.Inheritance inherit = new Inheritance();

            inherit.SiteSettings(companyId, parentCompanyId);
            inherit.CustomTabs(companyId, parentCompanyId);
            inherit.AttachmnetFolders(companyId, parentCompanyId);
            inherit.SystemReports(companyId, parentCompanyId, userId);
            inherit.SystemLists(companyId, parentCompanyId);
            inherit.DetailTabFields(companyId, parentCompanyId);
            inherit.CompanySettings(companyId, parentCompanyId);

        }

        //Save User 
        public int SaveUser(string firstName, string lastName, string Email, string userName, string password, string userPhone, int companyId)
        {
            try
            {
                tap.dom.gen.Cryptography _crypt = new gen.Cryptography();
                tap.dat.Users user = new tap.dat.Users();

                user.FirstName = firstName;
                user.LastName = lastName;
                user.Email = Email;
                user.UserName = userName;
                user.Password = (password.Length == 0 ? null : _crypt.Encrypt(password));
                user.PhoneNo = userPhone;
                user.CompanyID = companyId;
                user.isAdministrator = true;
                user.Active = true;
                user.ViewEvents = false;
                user.EditEvents = false;
                user.LastPasswordChange = DateTime.Now;
                user.LanguageId = GENERAL_LANGUAGE_ID;
                _db.AddToUsers(user);
                _db.SaveChanges();

                return user.UserID;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return 0;
        }

        //Save Company
        public int SaveCompany(int parentCompanyId, string companyName, string phoneNumber, string address, string companyEmail, string website, string subscriptionStart, string subscriptionEnd, bool IsADSecurity = false)
        {
            try
            {
                tap.dat.Companies company = new tap.dat.Companies();

                company.CompanyName = companyName;
                company.PhoneNo = phoneNumber;
                company.Address = address;
                company.Active = true;
                company.Email = companyEmail;
                company.Website = website;
                company.CheckADSecurity = IsADSecurity;

                if (parentCompanyId > 0)
                    company.ParentId = parentCompanyId;
                else
                    company.ParentId = null;

                if (subscriptionStart != null && subscriptionStart.Length > 0)
                    company.SubscriptionStart = DateTime.Parse(subscriptionStart);
                if (subscriptionEnd != null && subscriptionEnd.Length > 0)
                    company.SubscriptionEnd = DateTime.Parse(subscriptionEnd);

                _db.AddToCompanies(company);
                _db.SaveChanges();

                return company.CompanyID;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return 0;
        }
        //Insert default company datas thorugh SP
        public static void CompanyDefaultDataDB(int companyId)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString);
            try
            {
                SqlCommand myCommand = new SqlCommand("DefaultCompanyData", con);

                string isSU = ConfigurationManager.AppSettings["IsSU"];
                int isSUApp = 0;
                if (isSU != null && isSU.Equals("1"))
                    int.TryParse(isSU, out isSUApp);

                con.Open();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.Add("@CompanyId", SqlDbType.Int, 0).Value = companyId;
                myCommand.Parameters.Add("@isSU", SqlDbType.Int, 0).Value = isSUApp;
                int rows = myCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            finally
            {
                con.Close();
            }

        }

        //To save default System List for new setup Organization/Division
        public string SaveDefaultSystemList(int companyId, List<string> list)
        {
            string result = "";
            try
            {
                int locationListId = 0;
                int classificationListId = 0;

                List<string> classificationList = new List<string>();

                classificationList.Add(ENVIRONMENTAL);
                classificationList.Add(QUALITY);
                classificationList.Add(SAFETY);

                string compName = _db.Companies.Where(a => a.CompanyID == companyId).FirstOrDefault().CompanyName;

                List<string> locationList = new List<string>();
                locationList.Add(compName);

                tap.dat.SystemLists systemList = new dat.SystemLists();
                if (companyId.Equals(0))
                    return "";

                if (list != null && list.Count > 0)
                    SaveSystemList(companyId, list, locationList, classificationList);

                result = "CompanyId:" + companyId.ToString() + ":" + classificationListId.ToString() + ":" + locationListId.ToString();
            }
            catch (Exception ex) { tap.dom.hlp.ErrorLog.LogException(ex); }
            return result;
        }
        public void SaveSystemList(int companyId, List<string> list, List<string> locList, List<string> clsList)
        {
            tap.dat.SystemLists systemList = new dat.SystemLists();

            foreach (string item in list)
            {
                systemList.ListName = item;
                systemList.IsSystemList = true;
                systemList.IsEditable = true;
                systemList.CompanyID = companyId;
                systemList.Active = true;
                systemList.Description = item;

                if (item.Equals(CLASSIFICATION))
                    systemList.IsClassification = true;
                else if (item.Equals(LOCATION))
                    systemList.IsLocation = true;

                _db.AddToSystemLists(systemList);
                _db.SaveChanges();

                if (item.Equals(LOCATION))
                    SaveSystemListLoc(item, systemList.ListID, locList);

                if (item.Equals(CLASSIFICATION))
                    SaveSystemListCls(item, systemList.ListID, clsList);

                systemList = new dat.SystemLists();
            }

        }
        public void SaveSystemListLoc(string name, int listId, List<string> location)
        {
            string[] arr = new string[4];
            int locationListId = 0;
            int parentListId = 0;

            arr[0] = listId.ToString();
            arr[1] = null;
            arr[2] = name;
            arr[3] = "\\\\" + LOCATION + "\\";
            parentListId = SaveDefaultListValue(listId, arr);
            locationListId = listId;

            foreach (string listItem in location)
            {
                arr[0] = listId.ToString();
                arr[1] = parentListId.ToString();
                arr[2] = listItem;
                arr[3] = "\\\\" + LOCATION + "\\" + listItem + "\\";

                SaveDefaultListValue(listId, arr);
            }
        }

        public void SaveSystemListCls(string name, int listId, List<string> clsList)
        {
            string[] arr = new string[4];
            int classificationListId = 0;
            int parentListId = 0;

            arr[0] = listId.ToString();
            arr[1] = null;
            arr[2] = name;
            arr[3] = "\\\\" + CLASSIFICATION + "\\";
            parentListId = SaveDefaultListValue(listId, arr);
            classificationListId = listId;

            foreach (string listItem in clsList)
            {
                arr[0] = listId.ToString();
                arr[1] = parentListId.ToString();
                arr[2] = listItem;
                if (listItem.Equals(ENVIRONMENTAL))
                    arr[3] = "\\\\" + CLASSIFICATION + "\\" + ENVIRONMENTAL + "\\";
                if (listItem.Equals(QUALITY))
                    arr[3] = "\\\\" + CLASSIFICATION + "\\" + QUALITY + "\\";
                if (listItem.Equals(SAFETY))
                    arr[3] = "\\\\" + CLASSIFICATION + "\\" + SAFETY + "\\";
                SaveDefaultListValue(listId, arr);
            }
        }
        //To save default List Values for new setup Organization/Division
        public int SaveDefaultListValue(int listId, string[] details)
        {
            int parentId = 0;
            try
            {

                tap.dat.ListValues listValue = new dat.ListValues();

                if (listId > 0)
                {
                    listValue.ListID = listId;
                    if (details[1] != null)
                        listValue.Parent = Convert.ToInt32(details[1]);
                    else
                        listValue.Parent = null;
                    listValue.ListValueName = details[2];
                    listValue.Active = true;
                    listValue.FullPath = details[3];
                    _db.AddToListValues(listValue);
                    _db.SaveChanges();

                    parentId = listValue.ListValueID;
                }
            }
            catch (Exception ex) { tap.dom.hlp.ErrorLog.LogException(ex); }
            return parentId;
        }

        //To save default attachment folder
        public void SaveDefaultAttachmentFolder(int companyId)
        {
            if (companyId > 0)
            {
                tap.dat.AttachmentFolders newattachment = new dat.AttachmentFolders();
                newattachment.CompanyId = companyId;
                newattachment.FolderName = ATTACHMENTS;
                newattachment.ParentId = null;
                newattachment.FullPath = "\\\\" + ATTACHMENTS + "\\";
                newattachment.Active = true;
                newattachment.SortOrder = 0;
                _db.AddToAttachmentFolders(newattachment);
                _db.SaveChanges();
            }
        }

        //To save default site setting
        public void SaveDefaultSiteSetting(int companyId)
        {
            if (companyId > 0)
            {
                tap.dat.SiteSettings newsetting = new dat.SiteSettings();
                newsetting.CompanyID = companyId;
                newsetting.IsTwelveHourFormat = false;
                newsetting.DateFormat = "MMM dd yyyy";
                newsetting.ApplicationTimeOut = 240;
                _db.AddToSiteSettings(newsetting);
                _db.SaveChanges();
            }
        }

        //Get List Ids for location and classification
        public string SystemListIdGet(int companyId)
        {
            int locationListId = 0;
            int classListId = 0;
            tap.dat.SystemLists list = new dat.SystemLists();
            try
            {
                locationListId = _db.SystemLists.Where(x => x.IsLocation == true && x.CompanyID == companyId).FirstOrDefault().ListID;

                list = _db.SystemLists.Where(x => x.IsClassification == true && x.CompanyID == companyId).FirstOrDefault();

                if (list == null)
                    classListId = _db.SystemLists.Where(x => x.IsClassification == true).FirstOrDefault().ListID;
                else
                    classListId = list.ListID;

            }
            catch (Exception ex) { tap.dom.hlp.ErrorLog.LogException(ex); }
            return classListId.ToString() + ":" + locationListId.ToString();
        }

        public string CompanyAdminSetUpDetails()
        {
            string result = string.Empty;
            tap.dat.Companies company = new dat.Companies();
            tap.dat.Users user = new dat.Users();
            try
            {
                company = _db.Companies.Where(x => x.Active == true).FirstOrDefault();
                user = _db.Users.Where(x => x.isAdministrator == true && x.CompanyID == company.CompanyID).FirstOrDefault();
                if (company != null && user != null)
                    result = company.CompanyID.ToString() + ":" + user.UserID.ToString() + ":" + user.UserName + ":" + user.FirstName;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        public string CompanyNameOnId(int companyId)
        {
            if (companyId > 0)
                return _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault().CompanyName;
            else
                return "";
        }


        //to save company with admin user at the set up time with Active Directory security settings
        public string SaveCompanyWithActiveDirectoryAdmin(int parentCompanyId, string companyName, string phoneNumber, string address,
                string companyEmail, string website, string adminUserName,
                string serverName, string domainName, string groupName, string adUserName, string adPassword)
        {
            int UserId = 0;
            //tap.dat.Companies company = null;
            tap.dom.adm.UserDetails userDetails = new tap.dom.adm.UserDetails();
            tap.dom.adm.CommonOperation co = new tap.dom.adm.CommonOperation();
            int companyId = 0;

            try{

                //if (userDetails.IsUserExists(userName))
                //    return "User name " + userName + " already exists";

                //if (co.EmailAvailabilityCheck(userEmail))
                //    return "User " + userEmail + " already exists";

                // Check if company exists with ADSecurity = true
                companyId = (from c in _db.Companies
                                  where c.CompanyName == companyName && c.CheckADSecurity == true
                                  select c.CompanyID).FirstOrDefault();
                
                // If company doesnot exists, then save company and user
                if (companyId == 0)
                {
                    //save company
                    companyId = SaveCompany(parentCompanyId, companyName, phoneNumber, address, companyEmail, website, null, null, true);

                    if (companyId > 0)
                    {
                        //save user
                        UserId = SaveUserFromActiveDirectory(companyId, adminUserName, serverName, domainName, groupName, adUserName, adPassword);
                        List<string> systemlist = new List<string>();

                        if (parentCompanyId.Equals(0))
                            systemlist.Add(CLASSIFICATION);

                        systemlist.Add(LOCATION);
                        CompanyDefaultDataDB(companyId);

                        if (!parentCompanyId.Equals(0))
                        {
                            InheritDatas(parentCompanyId, companyId, UserId);
                        }

                        // To set up default system list and list values
                        SaveDefaultSystemList(companyId, systemlist);
                    }
                }
                // If company exists, then just save the user and create InheritDatas and default values
                else
                {
                    //save user
                    UserId = SaveUserFromActiveDirectory(companyId, adminUserName, serverName, domainName, groupName, adUserName, adPassword);
                    List<string> systemlist = new List<string>();

                    if (parentCompanyId.Equals(0))
                        systemlist.Add(CLASSIFICATION);

                    systemlist.Add(LOCATION);
                    CompanyDefaultDataDB(companyId);

                    if (!parentCompanyId.Equals(0))
                    {
                        InheritDatas(parentCompanyId, companyId, UserId);
                    }

                    // To set up default system list and list values
                    SaveDefaultSystemList(companyId, systemlist);
                }


                // Check if Active Directory Settings exists or not
                tap.dom.gen.Cryptography _crypt = new gen.Cryptography();
               
                // Add Active Directory Settings to DB
                tap.dat.ActiveDirectorySettings adSettings = new tap.dat.ActiveDirectorySettings();


                int rowADSettings = (from d in _db.ActiveDirectorySettings
                                    where d.ServerName == serverName && d.AdminUserName == adUserName
                                    select d).Count();
                if (rowADSettings == 0)
                {
                    adSettings.ServerName = serverName;
                    adSettings.DomainName = domainName;
                    adSettings.GroupName = groupName;
                    adSettings.ServerProtocol = SERVER_PROTOCOL;
                    adSettings.AdminUserName = adUserName;
                    adSettings.AdminPassword = _crypt.Encrypt(adPassword);

                    _db.AddToActiveDirectorySettings(adSettings);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return companyId.ToString() + ":" + UserId.ToString();
        }

        public int SaveUserFromActiveDirectory(int companyId, string adminUserName, string serverName, string domainName, string groupName, string adUserName, string adPassword)
        {
            //string ldapSearchFilter2 = string.Empty;
            //if (groupName.Split(',').Length > 0)
            //{
            //    for (int i = 0; i < groupName.Split(',').Length; i++)
            //    {
            //        ldapSearchFilter2 += "OU=" + groupName.Split(',')[i];
            //    }
            //}

            //string ldapQueryOptions = "CN=" + adminUserName + "," + ldapSearchFilter2 + "DC=i3siitest,DC=com";
            string ldapQueryOptions = "OU=" + groupName; // +"," + "DC=i3siitest,DC=com";

            // For Specific User
            string ldapSearchFilter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + adminUserName + "))";

            try
            {
                var credential = new NetworkCredential(adUserName, adPassword, domainName);

                // Create the new LDAP connection
                var ldapConnection = new LdapConnection(serverName);
                ldapConnection.Credential = credential;

                SearchRequest searchRequest = new SearchRequest
                                    (ldapQueryOptions, ldapSearchFilter, System.DirectoryServices.Protocols.SearchScope.Subtree, null);

                // cast the returned directory response as a SearchResponse object
                SearchResponse searchResponse =
                            (SearchResponse)ldapConnection.SendRequest(searchRequest);

                //Console.WriteLine("\r\nSearch Response Entries:{0}",
                //            searchResponse.Entries.Count);

                if (searchResponse.Entries.Count == 1)
                {
                    // First Name
                    string firstName = "";
                    if (searchResponse.Entries[0].Attributes["givenName"] != null)
                    {
                        firstName = Convert.ToString(searchResponse.Entries[0].Attributes["givenName"][0]);
                    }
                    
                    // Last Name
                    string lastName = "";
                    if (searchResponse.Entries[0].Attributes["sn"] != null)
                    {
                        lastName = Convert.ToString(searchResponse.Entries[0].Attributes["sn"][0]);
                    }

                    // Email
                    string email = "";
                    if (searchResponse.Entries[0].Attributes["mail"] != null)
                    {
                        email = Convert.ToString(searchResponse.Entries[0].Attributes["mail"][0]);
                    }

                    //Phone No
                    string phoneNo = "";
                    if (searchResponse.Entries[0].Attributes["telephoneNumber"] != null)
                    {
                        phoneNo = Convert.ToString(searchResponse.Entries[0].Attributes["telephoneNumber"][0]);
                    }

                    // User account active/enabled
                    int isEnabled = Convert.ToInt32(searchResponse.Entries[0].Attributes["userAccountControl"][0]);
                    bool isUserEnabled = !Convert.ToBoolean(isEnabled & 0x0002);

                    return SaveUser(firstName, lastName, email, adminUserName, "", phoneNo, companyId);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "DirectoryEntry");
            }

            return 0;
        }
    
    }
}
