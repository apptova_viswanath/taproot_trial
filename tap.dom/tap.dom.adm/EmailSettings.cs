﻿
//---------------------------------------------------------------------------------------------
// File Name 	: Tab.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Tabs
//---------------------------------------------------------------------------------------------

#region "Namespaces"

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using tap.dom.hlp;

#endregion

namespace tap.dom.adm
{
    public class EmailSettings
    {
        tap.dat.EFEntity _db = new dat.EFEntity();


     

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabs"></param>
        /// <param name="currentModuleName"></param>
        /// <param name="selectedModuleName"></param>
        /// <param name="virtualDirectoryPath"></param>
        /// <returns></returns>
        public string SaveEmailSettings(string emailFrom, string emailPassword, string hostAddress, string portNumber)
        {

            try { 

                tap.dat.EmailSettings emailSettings = new tap.dat.EmailSettings();
                emailSettings = _db.EmailSettings.FirstOrDefault();
                var isAdd = (emailSettings == null);
            
                if (isAdd)
                {
                    emailSettings = new tap.dat.EmailSettings();
                }

                emailSettings.EmailFrom = emailFrom;
                emailSettings.EmailPassword = emailPassword;
                emailSettings.HostAddress = hostAddress;
                emailSettings.PortNumber = Convert.ToInt32(portNumber);

                if (isAdd)
                {
                    _db.AddToEmailSettings(emailSettings);
                }


                _db.SaveChanges();
                return "Success";

            }

            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                return "Fail";
            }
        }

        public string TestEmailSettings(string emailTo)
        {

            try {

                tap.dom.adm.Mail email = new tap.dom.adm.Mail();
                return email.SendEmail(emailTo, "This is a test email", "This is a test email");

            }

            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                return "error: Sending Failed.";
            }
        }

 

        #region "To Get the Tab By ID"

        public tap.dat.EmailSettings GetEmailSettings()
        {
            tap.dat.EmailSettings emailSettings = new tap.dat.EmailSettings();
            emailSettings = _db.EmailSettings.FirstOrDefault();

            return emailSettings;
        }

        /// <summary>
        /// Invoked to Get the Tab By ID
        /// </summary>
        /// <param name="tabId"></param>
        /// <returns></returns>
        public object GetTabDetailsByID(int tabId, int moduleId)
        {
            tap.dat.Tabs tabs = new tap.dat.Tabs();
            tap.dat.TabModules mod = new tap.dat.TabModules();

            try
            {
                tabs = _db.Tabs.FirstOrDefault(a => a.TabID == tabId);

                if (!Convert.ToBoolean(tabs.IsUniversal))
                {
                    var tabModuleDetails = (from tabModules in _db.TabModules
                                            join modules in _db.Modules
                                            on tabModules.ModuleID equals modules.ModuleID
                                            where tabModules.TabID == tabId
                                            select new { tabModules, modules }).ToList();

                    string selectedModuleName = string.Empty;

                    foreach (var tabModule in tabModuleDetails)
                    {
                        selectedModuleName += tabModule.modules.ModuleName + ",";
                    }

                    //selectedModuleName = selectedModuleName.Replace("Corrective Action Plan", "CAP");
                    selectedModuleName = selectedModuleName.Replace("Corrective Action Plan", "ActionPlan");

                    var tabDetails = (from a in tabModuleDetails
                                      join b in _db.Tabs
                                      on a.tabModules.TabID equals b.TabID
                                      where a.tabModules.ModuleID == moduleId
                                      select new
                                      {
                                          TabID = b.TabID,
                                          CompanyID = b.CompanyID,
                                          TabName = b.TabName,
                                          IsUniversal = b.IsUniversal,
                                          IsSystem = b.IsSystem,
                                          SortOrder = b.SortOrder,
                                          Active = b.Active,
                                          IsDeleted = b.IsDeleted,
                                          SelectedModuleName = selectedModuleName,
                                          IsRestricted = b.isRestricted
                                      }).FirstOrDefault();

                    return tabDetails;
                }

                return tabs;

            }

            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        #endregion


    }
}


