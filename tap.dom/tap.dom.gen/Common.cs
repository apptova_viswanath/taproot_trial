﻿/********************************************************************************
 *  File Name   : Common.cs
 *  Description : A common file to do the common operations that can be used by any class
 *  Created on  : N/A
 *******************************************************************************/

#region "Namespace"
using System;
using System.IO;
using System.Web;
using System.Linq;
#endregion

namespace tap.dom.gen
{

    //A common file to do the common operations that can be used by any class
    public class Common
    {

        tap.dat.EFEntity _db = new tap.dat.EFEntity();


        /// <summary>
        /// Get binary data from the image file uploaded
        /// </summary>
        /// <param name="file">file that contain data to upload</param>
        /// <returns></returns>
        public static byte[] GetBinaryData(HttpPostedFile file)
        {

            try
            {

                BinaryReader binaryData = new BinaryReader(file.InputStream);
                return binaryData.ReadBytes(file.ContentLength);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }


        //save custom grid column sorting details.
        public string SaveGridColumnSortOrder(int userID, int gridID, string gridName, string pageName, string[] sortOrder)
        {
            string result = null;
            try
            {

                tap.dat.UserCustomGrid customgrid = new tap.dat.UserCustomGrid();
                customgrid = _db.UserCustomGrid.FirstOrDefault(a => a.UserID == userID && a.GridName == gridName);

                dat.UserCustomGrid newCustomGrid = new tap.dat.UserCustomGrid();

                if (customgrid != null)
                {
                    newCustomGrid = _db.UserCustomGrid.FirstOrDefault(a => a.UserID == userID && a.GridName == gridName);

                    string columnOrder = "";
                    for (int i = 0; i < sortOrder.Length; i++)
                    {
                        columnOrder += (columnOrder != null && columnOrder.Length > 0) ? "," : "";
                        columnOrder = columnOrder + sortOrder[i];
                    }
                    newCustomGrid.SortOrder = columnOrder;
                    result = "Saved Successfully";
                    _db.SaveChanges();
                }
                else
                {
                    newCustomGrid.UserID = userID;
                    string columnOrder = "";
                    for (int i = 0; i < sortOrder.Length; i++)
                    {
                        columnOrder += (columnOrder != null && columnOrder.Length > 0) ? "," : "";
                        columnOrder = columnOrder + sortOrder[i];
                    }

                    newCustomGrid.PageName = pageName;
                    newCustomGrid.GridName = gridName;
                    newCustomGrid.SortOrder = columnOrder;
                    newCustomGrid.Active = true;
                    result = "Saved Successfully";

                    _db.AddToUserCustomGrid(newCustomGrid);
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

       
    }
}
