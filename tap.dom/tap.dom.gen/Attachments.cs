﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.gen
{
    public class Attachments
    {
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        #region "Attachments Move"
        public bool AttachmentsMove(int atchNodeId, int atchParentNode)
        {
            bool boolinfo = false;
            tap.dat.Attachments attachments = _db.Attachments.FirstOrDefault(a => a.AttachmentID == atchNodeId);
            if (attachments != null)
            {
                attachments.AttachmentFolderID = atchParentNode;
                _db.SaveChanges();
                boolinfo = true;
            }
            return boolinfo;
        }
        #endregion 

        #region "Save SnapCapImages to SnapCap folder"
        public void SnapCapAttachment(string eventID,int companyId,int userId)
        {
            int eventId = 0;
          
            if (eventID != "0" && eventID != "")
                eventId = Convert.ToInt32(eventID);

            int snapCapFolderId = _db.AttachmentFolders.Where(a =>a.CompanyId==companyId && a.SortOrder==0 && a.FolderName=="SnapCaps").FirstOrDefault().AttachementFolderId;

            var getSnapCapImageData = (from snapcap in _db.SnapCaps
                                       where snapcap.UserID == userId && snapcap.EventID == eventId
                                       select new
                                       {
                                           SnapCapImageData = snapcap.SnapCapData,
                                           SnapcapColumnName = snapcap.SnapCapName,
                                           SnapCapId = snapcap.SnapCapID
                                       });


            if (getSnapCapImageData.ToList().Count > 0)
            {
                tap.dat.Attachments attachments = null;

                var attachmentSnapCapDetails = from attachment in _db.Attachments.AsEnumerable()
                                               join sci in getSnapCapImageData.AsEnumerable()  on attachment.DocumentName equals sci.SnapcapColumnName
                                               where attachment.EventID == eventId && attachment.CompanyId == companyId
                                               select attachment;
                //delete the existing records and re insert - if snapcap images overidden with other image with same name it is not updating
               if (attachmentSnapCapDetails != null)
                {
                    foreach (var data in attachmentSnapCapDetails)
                    {
                        _db.DeleteObject(data);
                    }
                    _db.SaveChanges();
                }


               foreach (var data in getSnapCapImageData)
               {
                   attachments = new tap.dat.Attachments();
                   attachments.EventID = eventId;
                   attachments.AttachmentFolderID = snapCapFolderId;
                   attachments.DocumentName = data.SnapcapColumnName.ToString();
                   attachments.Extension = ".jpg";
                   attachments.AttachmentData = data.SnapCapImageData;
                   attachments.CompanyId = companyId;

                   _db.AddToAttachments(attachments);

               }
                _db.SaveChanges();
            }
        }
        #endregion
    }
}
