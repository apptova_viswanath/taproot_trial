﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.gen
{
    public class RootCausesNodes
    {

        public const int RCID_HUMAN_PERFORMANCE_DIFFICULTY = 2;
        public const int RCID_EQUIPMENT_DIFFICULTY = 151;
        public const int RCID_NATURAL_SABOTAGE = 187;
        public const int RCID_OTHER = 188;

    }
}
