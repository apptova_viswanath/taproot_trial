﻿//--------------------------------------------------------------------------------------------------------
// File Name 	: Security.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Security Access.
//                 Independent of any class or property

//--------------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

#endregion

namespace tap.dom.gen
{
    public class Security
    {   
        //security access denied url
        string securityUrl = String.Format(tap.dom.adm.CommonOperation.GetSiteRoot() + "/Security/Access-Denied/");

        #region "Get Event Ids from Events table"
        public static string GetEventIds(int userId)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();

            bool isUserView = Convert.ToBoolean(_db.Users.Where(a => a.UserID == userId).FirstOrDefault().ViewEvents.Value);
            int companyId = _db.Users.Where(a => a.UserID == userId).FirstOrDefault().CompanyID.Value;


            var eventIds = (from events in _db.Events.AsEnumerable()
                            where (events.CompanyID == companyId && isUserView) ||
                                (events.CreatedBy == userId && !isUserView)
                            select events.EventID);

            //add share tab access too
            return _efSerializer.EFSerialize(eventIds);

        }
        #endregion

        #region "Pass UserId to get Admin Details"
        public bool isAdminActive(int userId)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            bool isAdmin = _db.Users.Where(a => a.UserID == userId).FirstOrDefault().isAdministrator.Value;

            return isAdmin;
        }
        #endregion

        #region "Check if passed userid is isAdmin or not"
        public void ChckIfAdmin(System.Web.HttpRequest request, System.Web.HttpResponse response, string url)
        {

            if (!Convert.ToBoolean(request.Cookies["isAdmin"].Value))
                response.Redirect(url + "0");
        }
        #endregion

        #region "Check the Event Access Security"
        //user side pages
        /// <summary>
        /// CheckEventAccess()
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <param name="response"></param>
        /// <param name="url"></param>
        public void CheckEventAccess(int eventId, int userId, System.Web.HttpResponse response)
        {
             tap.dat.EFEntity _db = new tap.dat.EFEntity();

             var hasAccess = _db.Events.Where(a => a.CreatedBy == userId && a.EventID == eventId).Count() != 0 ? true :
                             _db.TeamMembers.Where(a => a.UserID == userId && a.EventID == eventId).Count() != 0 ? true : false;
             if (!hasAccess)
                 response.Redirect(securityUrl + eventId);
                      
        }

        //admin side pages
        /// <summary>
        /// IsUserAutherised
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="response"></param>
        /// <param name="url"></param>
        public void IsUserAutherised(int userId, System.Web.HttpResponse response)
       {
           tap.dat.EFEntity _db = new tap.dat.EFEntity();

           //Check if user is belongs to Admin or not 
           bool isAdmin = _db.Users.Where(a => a.UserID == userId && a.isAdministrator == true).ToList().Count > 0 ? true : false;

           if (!isAdmin)
               response.Redirect(securityUrl + 0);
       }

        public void CheckReportAccess(int reportId, int userId, int companyId, int reportType, int eventId, System.Web.HttpResponse response)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            tap.dom.adm.Company _company = new tap.dom.adm.Company();
            bool hasAccess =false;
            bool isAdmin =_company.isSUCompany(companyId);

            if(isAdmin)
                 hasAccess = _db.Report.Where(a =>  a.ReportID == reportId).Count() != 0 ? true : false;
            else if(reportType==3)
                hasAccess = _db.Report.Where(a => a.CompanyID == companyId && a.ReportID == reportId).Count() != 0 ? true : false;
            else
                 hasAccess = _db.Report.Where(a => a.CreatedBy == userId && a.ReportID == reportId).Count() != 0 ? true : false;

            if (!hasAccess)
                response.Redirect(securityUrl + eventId);
        }
        #endregion

        #region "Get Security Access Address"
        public string GetCompanyAddress(int companyId, int userId,int eventId)
        {
            tap.dat.EFEntity db = new tap.dat.EFEntity();

            var addressDetails = (from events in db.Events
                                  join users in db.Users on events.CreatedBy equals users.UserID
                                  where events.EventID == eventId
                                  select new
                                  {
                                      FirstName = users.FirstName,
                                      LastName = users.LastName,
                                      Email = users.Email,
                                      PhoneNumber = users.PhoneNo
                                  });
                      

            StringBuilder sb = new StringBuilder();

            sb.Append("<div  id=mainDiv>");
            foreach (var address in addressDetails)
            {
                sb.Append("<div>" + address.FirstName + " " + address.LastName + "</div>");
                sb.Append("<div>" + address.Email + "</div>");
                sb.Append("<div>" + address.PhoneNumber + "</div>");

            }
            sb.Append("</div");
            return sb.ToString();

        }
        #endregion

    }
}
