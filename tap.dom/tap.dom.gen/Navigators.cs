﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;

namespace tap.dom.gen
{
    public class TopNavigators
    {
        tap.dat.EFEntity db = new tap.dat.EFEntity();

        public string GetTopNavData(string companyID, string userID, string sVirtualPath)
        {
            System.Text.StringBuilder sbTopNav = new System.Text.StringBuilder();
            tap.dat.Users userDetails = new tap.dat.Users();

            //Decrypt the value and pass
            int companyId = Convert.ToInt32(companyID);
            int userId = 0;
            int.TryParse(userID, out userId);
            bool isAdministrator = false;

            try
            {
                if (userId > 0)
                    userDetails = db.Users.FirstOrDefault(a => a.UserID == userId && a.CompanyID == companyId);
                if (userDetails != null && userDetails.isAdministrator.ToString() != null && userDetails.isAdministrator.ToString().Length>0)
                    isAdministrator = Convert.ToBoolean(userDetails.isAdministrator.ToString());
                //Top nave data

                var TopNave = db.TopNav.Where( a => a.Active == true).ToList();

                int detailTabId = 1;

                if (!string.IsNullOrEmpty(companyID))
                    detailTabId = db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;


                //check the data exist or not
                if (TopNave.Count() != 0)
                {
                    sbTopNav.Append("<ul id=\"tab\">");

                    //create LI for the top nave
                    foreach (var parent in (from x in TopNave where x.ParentID == null orderby x.SortOrder select x).ToList())
                    {
                        //create LI for the top nave
                        //sbTopNav.Append(CreateLI(parent.DisplayName, parent.LinkURL, sVirtualPath));
                        if(parent.DisplayName.Equals("Admin") && isAdministrator)
                            sbTopNav.Append(CreateLI(parent.DisplayName, parent.LinkURL, sVirtualPath));
                        else if(!parent.DisplayName.Equals("Admin") )
                            sbTopNav.Append(CreateLI(parent.DisplayName, parent.LinkURL, sVirtualPath));
                        

                        var Level1 = (from x in TopNave
                                      where x.ParentID == parent.TopNavID
                                      orderby x.SortOrder
                                      select x).ToList();

                        //check first level menu
                        if (Level1.Count() != 0)
                        {
                            //Create Ul
                            sbTopNav.Append(CreateUL());

                            foreach (var child in Level1)
                            {
                                string linkUrl = child.LinkURL;

                                if (child.LinkURL.Contains("Details-1"))
                                    linkUrl = linkUrl.Replace("Details-1", "Details-" + detailTabId);
                                //create LI for the first level
                                sbTopNav.Append(CreateLI(child.DisplayName, linkUrl, sVirtualPath));

                                var Level2 = (from x in TopNave
                                              where x.ParentID == child.TopNavID
                                              orderby x.SortOrder
                                              select x).ToList();

                                //check 2nd level menu
                                if (Level2.Count() != 0)
                                {
                                    //create UL
                                    sbTopNav.Append(CreateUL());

                                    foreach (var child2 in Level2)
                                    {
                                        //create LI for the 2nd level menu
                                        sbTopNav.Append(CreateLI(child2.DisplayName,  child2.LinkURL, sVirtualPath));

                                        var Level3 = (from x in TopNave
                                                      where x.ParentID == child2.TopNavID
                                                      orderby x.SortOrder
                                                      select x).ToList();

                                        //check the 3rd level menu 
                                        if (Level3.Count() != 0)
                                        {
                                            //create UL
                                            sbTopNav.Append(CreateUL());

                                            foreach (var child3 in Level3)
                                            {
                                                //create LI for the 3rd level menu
                                                sbTopNav.Append(CreateLI(child3.DisplayName, child3.LinkURL, sVirtualPath));
                                                sbTopNav.Append(CloseLI());
                                            }

                                            sbTopNav.Append(CloseUL());
                                        }

                                        sbTopNav.Append(CloseLI());
                                    }

                                    sbTopNav.Append(CloseUL());
                                }

                                sbTopNav.Append(CloseLI());
                            }

                            sbTopNav.Append(CloseUL());
                        }

                        sbTopNav.Append(CloseLI());
                    }

                    sbTopNav.Append(CloseUL());
                }

            }
            catch (Exception Ex)
            {
                throw Ex;

            }

            return sbTopNav.ToString();
        }

        private string CreateUL()
        {
            return "<ul class=\"SubTabs\">";
        }

        /// <summary>
        /// CloseUL() 
        /// Decription - Close the Ul
        /// </summary>
        /// <returns>string</returns>
        private string CloseUL()
        {
            return "</ul>";
        }

        /// <summary>
        /// CloseLI()
        /// Description - Close the LI
        /// </summary>
        /// <returns>string</returns>
        private string CloseLI()
        {
            return "</li>";
        }

        private string CreateLI(string sTitle, string sURLLink, string sVirtualPath)
        {

            System.Text.StringBuilder sbTopNav = new System.Text.StringBuilder();

            try
            {
                sbTopNav.Append("<li id=\"" + sTitle.Replace(" ", "") + "Tabs\">");

                //check the null link
                if (sURLLink != null)
                {
//sbTopNav.Append("<a title=\"" + sTitle + "\" href='.." + sURLLink + "' id=\"anch" + sTitle.Replace(" ", "") + "\"><nobr>" + sTitle + "</nobr></a>");
                    sbTopNav.Append("<a title=\"" + sTitle + "\" href=\"" + sVirtualPath + sURLLink + "\" id=\"anch" + sTitle.Replace(" ", "") + "\"><nobr>" + sTitle + "</nobr></a>");
                }
                else
                {
                    sbTopNav.Append("<a title=\"" + sTitle + "\"  id=\"anch" + sTitle.Replace(" ", "") + "\"><nobr>" + sTitle + "</nobr></a>");
                }

            }
            catch (Exception Ex)
            {
                throw Ex;
            }

            return sbTopNav.ToString();
        }

    }
}
