﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.gen
{
    public class ForeignLanguage
    {
        public const string English_Language = "en";
        public const string Spanish_Language = "es";
        public const string Russian_Language = "ru";
        public const string French_Language = "fr";
        public const string German_Language = "de";
        public const string Portuguese_Language = "pt";
    }
}
