﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace tap.dom.gen
{
    public class Cryptography
    {
        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="stringToEncrypt"></param>
        /// <returns></returns>
        public string Encrypt(string stringToEncrypt)
        {
            byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes("A0D1nX0Q");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                //return HttpUtility.UrlEncode(Convert.ToBase64String(ms.ToArray()).Replace("/", "_").Replace("+","-").Replace("=","."));
                return HttpUtility.UrlEncode(Convert.ToBase64String(ms.ToArray()).Replace("/", "_").Replace("+", "-"));
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }



        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="EncryptedText"></param>
        /// <returns></returns>
        public string Decrypt(string EncryptedText)
        {
            
            //if (EncryptedText.Contains('%'))
            EncryptedText = HttpUtility.UrlDecode(EncryptedText);

            EncryptedText = EncryptedText.Replace("_", "/").Replace("-", "+");
            //EncryptedText = HttpUtility.UrlDecode(EncryptedText);
            byte[] inputByteArray = new byte[EncryptedText.Length + 1];
            byte[] rgbIV = { 0x21, 0x43, 0x56, 0x87, 0x10, 0xfd, 0xea, 0x1c };
            byte[] key = { };
            EncryptedText = EncryptedText.Replace("_", "/");
            try
            {
                key = System.Text.Encoding.UTF8.GetBytes("A0D1nX0Q");
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(EncryptedText);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, rgbIV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

    }
}
