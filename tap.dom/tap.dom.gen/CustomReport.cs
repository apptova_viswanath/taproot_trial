﻿//--------------------------------------------------------------------------------------------------------
// File Name 	: CustomReport.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Custom Report all the columns in a generic way.
//                 Independent of any class or property

//--------------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Objects;
using System.Data.Metadata.Edm;
using System.Data.Objects.ELinq;
using System.Data.Entity;
using System.Reflection;
using tap.dom.hlp;
using System.Data.Common;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Collections;

#endregion

namespace tap.dom.gen
{

    public class CustomReport
    {
        #region "Variable Declarations"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        List<string> columnNames = new List<string>();

        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        public const string TAB_NAME = "Events";
        public const string MODULE_NAME = "Event Details";
        public const string DATA_FIELD = "Data Field";
        public const string CUSTOM_FIELD = "Custom Field";
        public const string CLASSIFICATION_COLUMN = "Classification";
        public const string LOCATION_COLUMN = "Location";
        public const string FIELDLISTVALUES_TABLE = "FieldListValues";
        public const string VALUEID_COLUMN = "ValueId";
        public const string SELECTONEFROMLIST_DATATYPE = "Select one from list";
        public const string SELECTMULTIPLEFROMLIST_DATATYPE = "Select multiple from list";
        public const string CUSTOM_ELEMENTS = "Visual Elements";
        public const string CUSTOM_MODULE = "Fields";
        public const string CUSTOM_SMALLIMAGE = "Image (Small)";
        public const string CUSTOM_MEDIUMIMAGE = "Image (Medium)";
        public const string CUSTOM_LARGEIMAGE = "Image (Large)";
        public const string CUSTOM_SPACER = "Line Break";
        public const string CUSTOM_PAGE_BREAK = "Page Break";
        public const string SNAPCAP_IMAGE = "Image";
       
        public const string EVENT_DATETIME = "Event Date/Time";
        public const string EVENTDATE = "EventDate";

        /// <summary>
        /// add Values to show Header Details
        /// </summary>
        public const string SHOWCOMPANY_LOGO = "Show Company Logo";
        public const string SHOWCOMPANY_NAME = "Show Company Name";
        public const string SHOWREPORT_TITLE = "Show Report Title";
        public const string SHOWCREATED_DATE = "Show Date Created";
        public const string HEADER_ELEMENTS = "Header Elements";
        public const string HEADER_MODULE = "Details";

        string EVENT_REPORT = "Event";
        string USER_REPORT = "User";
        string SYSTEM_REPORT = "System";

        // causal Factor,RCT,CAP & Tasks
        public const string TAPROOT_MODULE = "TapRooT®";
        public const string TAPROOT_NAME = "Items";
        const string AUTUMN_SEASON = "Autumn";
        public const string TAPROOT_CF = "Causal Factor";
        public const string TAPROOT_CAP = "Action Plan";
        public const string TAPROOT_RCT = "Root Cause";

        SqlQueryInfo _sqlquery = new SqlQueryInfo();

        int standardWidth;
        int standardHeight;

        int width;
        int height;
        //insert image file in Report Element table           
        string smallImage = "Small";
        string mediumImage = "Medium";

        #endregion    

        #region "Get Column Names"
        public object GetColumnNames(int companyId, int userId, string eventId, string reportType, int reportId)
        {
            int reportEventId= 0;

            if (eventId != "0" && eventId != "")
                reportEventId = Convert.ToInt32(_cryptography.Decrypt(eventId));

            string reportLogoName = GetReportLogoImageData(companyId);
            var customColumns = (from t in _db.Tabs
                                 join tm in _db.TabModules on t.TabID equals tm.TabID into temptabs
                                 from ttm in temptabs.DefaultIfEmpty()
                                 join m in _db.Modules on ttm.ModuleID equals m.ModuleID into tempmodules
                                 from tmm in tempmodules.DefaultIfEmpty()
                                 join f in _db.Fields on t.TabID equals f.TabID into tempfields
                                 from tf in tempfields.DefaultIfEmpty()
                                 where tf.DisplayName != null && tf.IsActive==true && t.Active == true && t.CompanyID == companyId && t.TabName != "Details"
                                 select new
                                 {
                                     TabId = t.TabID,
                                     ModuleName = tmm.ModuleName,
                                     TabName = t.TabName,
                                     ColumnName = tf.DisplayName,
                                     ReportLogoName = reportLogoName,
                                     isStaticColumn=true,
                                     SnapCapImageId = 0,
                                     CustomTab=true
                                 }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();


            DataTable dt = _sqlquery.ExecuteQuery(string.Format(@"SELECT c.name AS ColumnName FROM sys.tables AS t INNER JOIN sys.columns c ON t.OBJECT_ID = c.OBJECT_ID INNER JOIN sys.types tp ON c.system_type_id = tp.system_type_id where t.name = 'Events' and tp.name <> 'int' "));
            dt.Rows.Add(CLASSIFICATION_COLUMN);
            dt.Rows.Add(EVENT_DATETIME);
            //Event columns
            List<DataRow> columnNames = dt.AsEnumerable().ToList();
            var eventColumns = (from dr in columnNames
                                where dr["ColumnName"].ToString() != "FileNumber" && dr["ColumnName"].ToString() != "Description" && dr["ColumnName"].ToString() != "DateModified" && dr["ColumnName"].ToString() != "DateCreated" && dr["ColumnName"].ToString() != "EventTime" && dr["ColumnName"].ToString() != EVENTDATE  //time being hide it
                                select new
                                {
                                    TabId = 0,
                                    ModuleName = MODULE_NAME,
                                    TabName = TAB_NAME,
                                    ColumnName = dr["ColumnName"].ToString(),
                                    ReportLogoName = reportLogoName,
                                    isStaticColumn = false,
                                    SnapCapImageId = 0,
                                    CustomTab = false
                                }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();

              //event page Custom fields
            var eventCustomFields = (from t in _db.Tabs
                                     join tm in _db.TabModules on t.TabID equals tm.TabID into temptabs
                                     from ttm in temptabs.DefaultIfEmpty()
                                     join m in _db.Modules on ttm.ModuleID equals m.ModuleID into tempmodules
                                     from tmm in tempmodules.DefaultIfEmpty()
                                     join f in _db.Fields on t.TabID equals f.TabID into tempfields
                                     from tf in tempfields.DefaultIfEmpty()
                                     where tf.DisplayName != null && t.Active == true && t.CompanyID == companyId && t.TabName == "Details"
                                     select new
                                     {
                                         TabId = 0,
                                         ModuleName = MODULE_NAME,
                                         TabName = TAB_NAME,
                                         ColumnName = tf.DisplayName,
                                         ReportLogoName = reportLogoName,
                                         isStaticColumn = true,
                                         SnapCapImageId = 0,
                                         CustomTab = false
                                     }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();

            //Custom Fields
            DataTable dtCustomElements = GetCustomElements();
            List<DataRow> customElementNames = dtCustomElements.AsEnumerable().ToList();

            var customElements = (from t in customElementNames
                                  select new
                                  {
                                      TabId = -1,
                                      ModuleName = CUSTOM_ELEMENTS,
                                      TabName = CUSTOM_MODULE,
                                      ColumnName = t["ColumnName"].ToString(),
                                      ReportLogoName = reportLogoName,
                                      isStaticColumn = false,
                                      SnapCapImageId = 0,
                                      CustomTab = false
                                  }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();


            //add Check boxes Values-Company Logo,Name and Report Title ,Date
            DataTable dtHeaderElements = GetCompanyReportElements();
            List<DataRow> headerElementsNames = dtHeaderElements.AsEnumerable().ToList();

            var headerElements = (from t in headerElementsNames
                                  select new
                                  {
                                      TabId = -2,
                                      ModuleName = HEADER_ELEMENTS,
                                      TabName = HEADER_MODULE,
                                      ColumnName = t["ColumnName"].ToString(),
                                      ReportLogoName = reportLogoName,
                                      isStaticColumn = false,
                                      SnapCapImageId = 0,
                                      CustomTab = false
                                  }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();


            //TapRooT elements
            DataTable dtTapRooTElements = GetTapRooTElements();
            List<DataRow> tapRooTElementsNames = dtTapRooTElements.AsEnumerable().ToList();

            var tapRooTElements = (from t in tapRooTElementsNames
                                   select new
                                   {
                                       TabId = -3,
                                       ModuleName = TAPROOT_MODULE,
                                       TabName = TAPROOT_NAME,
                                       ColumnName = t["ColumnName"].ToString(),
                                       ReportLogoName = reportLogoName,
                                       isStaticColumn = false,
                                       SnapCapImageId = 0,
                                       CustomTab = false
                                   }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.ModuleName)).ToList();

            if (reportType == "1")// show snapCap images only for Report builder
            {
                //Snap Caps
                List<string> snapCapFileNames = new List<string>();
                List<string> snapCapColumnNames = new List<string>();
               

                if(reportId!=0)
                    reportEventId = _db.Report.FirstOrDefault(a => a.ReportID == reportId).EventID.Value;

                var getSnapCapImageData = (from snapcap in _db.SnapCaps
                                           where snapcap.UserID == userId && snapcap.EventID == reportEventId
                                           orderby snapcap.SnapCapID
                                           select new
                                           {
                                               SnapCapImageData = snapcap.SnapCapData,
                                               SnapcapColumnName = snapcap.SnapCapName,
                                               SnapCapId = snapcap.SnapCapID
                                           });

                if (getSnapCapImageData.ToList().Count > 0)
                {
                    foreach (var data in getSnapCapImageData)
                    {
                        string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                        //check directory exists or not -if not create it.
                        if (Directory.Exists(tempPath) == false)
                        {
                            Directory.CreateDirectory(tempPath);
                        }

                        //create image  and save it in temp folder
                        if (!System.IO.File.Exists(tempPath + data.SnapCapId + ".jpg"))
                        {
                            string fileName = WriteImage(data.SnapCapImageData, data.SnapCapId);

                            snapCapFileNames.Add(fileName);

                            //set standard size
                            standardHeight = 600;
                            standardWidth = 500;

                            string tempFile = tempPath + fileName;

                            
                            System.Drawing.Image img = System.Drawing.Image.FromFile(tempPath + fileName);

                            //if image is more than 500/600 then resize to large image(500/600)
                            if (img.Width > 500 || img.Height > 600)
                            {
                                img.Dispose();
                                ResizeImage(tempFile, ImageSizes.size_510x510, fileName, tempFile); 
                            }
                            img.Dispose();
                        }
                        snapCapColumnNames.Add(data.SnapcapColumnName);
                    }
                }

                var snapCapElements = (from s in getSnapCapImageData
                                       select new
                                       {
                                           TabId = -4,
                                           ModuleName = "SnapCap",
                                           TabName = "Images",
                                           ColumnName = s.SnapcapColumnName,
                                           ReportLogoName = reportLogoName,
                                           isStaticColumn = false,
                                           SnapCapImageId = s.SnapCapId,
                                           CustomTab = false
                                       }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.SnapCapImageId)).ToList();

                var concatHeaderCustomElements = headerElements.Concat(customElements);
                var concatTapRooTElements = concatHeaderCustomElements.Concat(tapRooTElements);
                var concatSnapCapElements = concatTapRooTElements.Concat(snapCapElements);
                var concatEvent = concatSnapCapElements.Concat(eventColumns);
                var concatEventCustomElements = concatEvent.Concat(eventCustomFields);
                var allColumnElements = concatEventCustomElements.Concat(customColumns);

                return _efSerializer.EFSerialize(allColumnElements);
            }
            else
            {
                //Snap Caps
                List<string> snapCapFileNames = new List<string>();
                List<string> snapCapColumnNames = new List<string>();


                if (reportId != 0)
                    reportEventId = _db.Report.FirstOrDefault(a => a.ReportID == reportId).EventID.Value;

                var getSnapCapImageData = (from snapcap in _db.SnapCaps
                                           where snapcap.UserID == userId && snapcap.EventID == reportEventId && snapcap.SnapCapCommonTypeId > 0 
                                           orderby snapcap.SnapCapID
                                           select new
                                           {
                                               SnapCapImageData = snapcap.SnapCapData,
                                               SnapcapColumnName = snapcap.SnapCapName,
                                               SnapCapId = snapcap.SnapCapID
                                           });

                if (getSnapCapImageData.ToList().Count > 0)
                {
                    foreach (var data in getSnapCapImageData)
                    {
                        string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                        //check directory exists or not -if not create it.
                        if (Directory.Exists(tempPath) == false)
                        {
                            Directory.CreateDirectory(tempPath);
                        }

                        //create image  and save it in temp folder
                        if (!System.IO.File.Exists(tempPath + data.SnapCapId + ".jpg"))
                        {
                            string fileName = WriteImage(data.SnapCapImageData, data.SnapCapId);

                            snapCapFileNames.Add(fileName);

                            //set standard size
                            standardHeight = 600;
                            standardWidth = 500;

                            string tempFile = tempPath + fileName;


                            System.Drawing.Image img = System.Drawing.Image.FromFile(tempPath + fileName);

                            //if image is more than 500/600 then resize to large image(500/600)
                            if (img.Width > 500 || img.Height > 600)
                            {
                                img.Dispose();
                                ResizeImage(tempFile, ImageSizes.size_510x510, fileName, tempFile);
                            }
                            img.Dispose();
                        }
                        snapCapColumnNames.Add(data.SnapcapColumnName);
                    }
                }
                var snapCapElements = (from s in getSnapCapImageData
                                       select new
                                       {
                                           TabId = -4,
                                           ModuleName = "SnapCap",
                                           TabName = "Images",
                                           ColumnName = s.SnapcapColumnName,
                                           ReportLogoName = reportLogoName,
                                           isStaticColumn = false,
                                           SnapCapImageId = s.SnapCapId,
                                           CustomTab = false
                                       }).GroupBy(a => new { TabId = a.TabId, a.TabName, a.ColumnName }).OrderBy(b => b.Max(c => c.SnapCapImageId)).ToList();

                var concatHeaderCustomElements = headerElements.Concat(customElements);
                var concatTapRooTElements = concatHeaderCustomElements.Concat(tapRooTElements);
                var concatSnapCapElements = concatTapRooTElements.Concat(snapCapElements);
                var concatEvent = concatSnapCapElements.Concat(eventColumns);
                var concatEventCustomElements = concatEvent.Concat(eventCustomFields);
                var allColumnElements = concatEventCustomElements.Concat(customColumns);

                return _efSerializer.EFSerialize(allColumnElements);
            }    
           
        }

        //discuss the TabId--1 and -2 is ok??
        public DataTable GetCustomElements()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TabId", typeof(int));
            dt.Columns.Add("ModuleName", typeof(string));
            dt.Columns.Add("TabName", typeof(string));
            dt.Columns.Add("ColumnName", typeof(string));

            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, tap.dom.hlp.Enumeration.CustomElementTypes.Line);
            dt.Rows.Add(-1, CUSTOM_MODULE, "CUSTOM_ELEMENTS Elements", tap.dom.hlp.Enumeration.CustomElementTypes.Label);//deleted string format      
            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, CUSTOM_SPACER);
            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, CUSTOM_PAGE_BREAK);
            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, CUSTOM_SMALLIMAGE);
            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, CUSTOM_MEDIUMIMAGE);
            dt.Rows.Add(-1, CUSTOM_MODULE, CUSTOM_ELEMENTS, CUSTOM_LARGEIMAGE);

            return dt;
        }

        public DataTable GetCompanyReportElements()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TabId", typeof(int));
            dt.Columns.Add("ModuleName", typeof(string));
            dt.Columns.Add("TabName", typeof(string));
            dt.Columns.Add("ColumnName", typeof(string));

            dt.Rows.Add(-2, HEADER_MODULE, HEADER_ELEMENTS, SHOWCOMPANY_LOGO);
            dt.Rows.Add(-2, HEADER_MODULE, HEADER_ELEMENTS, SHOWCOMPANY_NAME);
            dt.Rows.Add(-2, HEADER_MODULE, HEADER_ELEMENTS, SHOWREPORT_TITLE);
            dt.Rows.Add(-2, HEADER_MODULE, HEADER_ELEMENTS, SHOWCREATED_DATE);

            return dt;
        }

        public DataTable GetTapRooTElements()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("TabId", typeof(int));
            dt.Columns.Add("ModuleName", typeof(string));
            dt.Columns.Add("TabName", typeof(string));
            dt.Columns.Add("ColumnName", typeof(string));

            dt.Rows.Add(-3, TAPROOT_NAME, TAPROOT_MODULE, "Causal Factors");
            dt.Rows.Add(-3, TAPROOT_NAME, TAPROOT_MODULE, "Root Causes");
            dt.Rows.Add(-3, TAPROOT_NAME, TAPROOT_MODULE, "Action Plans");
            dt.Rows.Add(-3, TAPROOT_NAME, TAPROOT_MODULE, "Tasks");

            return dt;

        }
        #endregion

        #region "Get user report templates based on User and company"
        public object GetUserReportTemplates(int companyId, int userId)
        {
            //get all User Types Report-event name should be null and based on User Id
            var getUserReports = (from r in _db.Report
                                   join ur in _db.UserReports on r.ReportID equals ur.ReportID
                                   join user in _db.Users on r.CreatedBy equals user.UserID
                                   join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                   where r.CreatedBy == userId && r.ReportName != "" && r.CompanyID==companyId
                                   select new
                                   {
                                       ReportName = r.ReportName,
                                       ReportId = r.ReportID,
                                       reportType=2,
                                       isAdmin=user.isAdministrator
                                   
                                   }).Distinct().AsEnumerable();

            var systemReports = (from company in _db.Companies
                                 join report in _db.Report on company.CompanyID equals report.CompanyID
                                 join user in _db.Users on report.CreatedBy equals user.UserID
                                 join rt in _db.ReportTypes on report.ReportTypeId equals rt.ReportTypeId
                                 where company.CompanyID == companyId && rt.ReportType == SYSTEM_REPORT && report.ReportName != ""
                                 select new
                                 {
                                   
                                     ReportName = report.ReportName,
                                     ReportId = report.ReportID,
                                      reportType=3,                                     
                                      isAdmin=user.isAdministrator
                                 });

            tap.dom.tab.EventTabs eventTabs = new tap.dom.tab.EventTabs();

            //get the parent id's and pass to Report table to get System Reports 
            List<int> parents = eventTabs.GetCompanyParentIds(companyId);

            var systemReportsParentNames = (from report in _db.Report
                                            join user in _db.Users on report.CreatedBy equals user.UserID
                                            join rt in _db.ReportTypes on report.ReportTypeId equals rt.ReportTypeId
                                            where rt.ReportType == SYSTEM_REPORT && report.ReportName != "" && parents.Contains(report.CompanyID)
                                            select new
                                            {
                                               
                                                ReportName = report.ReportName,
                                                ReportId = report.ReportID,
                                                reportType=3,
                                                isAdmin = user.isAdministrator
                                            });


            var getAllSystemReports = systemReports.Union(systemReportsParentNames);

            var concatAllReportNames = getUserReports.Concat(getAllSystemReports);

            var getReportTemplateNames = (from r in concatAllReportNames
                                          select new
                                          {
                                              ReportName=r.ReportName,
                                              ReportId = r.ReportId,
                                              ReportType = r.reportType,
                                              isAdmin=r.isAdmin

                                          }).Distinct().AsEnumerable();
         
            return _efSerializer.EFSerialize(getReportTemplateNames);
        }
        #endregion

        #region "Bind Causal Foctor,RCT,CAP and Tasks-static content"
        public object GetTapRooTElementsTest()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("HeaderName", typeof(string));
            dt.Columns.Add("SubHeader", typeof(string));
            dt.Columns.Add("SubData", typeof(string));

            dt.Rows.Add(TAPROOT_NAME, "Display CF", "");
            dt.Rows.Add(TAPROOT_NAME, "Display RCT", "Integrate within CF");
            dt.Rows.Add(TAPROOT_NAME, "Display CAP", "Integrate within RCT");
            dt.Rows.Add(TAPROOT_NAME, "Display Tasks", "Integrate within CAP");

            List<DataRow> TapRooTElements = dt.AsEnumerable().ToList();

            var elements = (from a in TapRooTElements
                            select new
                            {
                                HeaderName = a["HeaderName"],
                                SubHeader = a["SubHeader"],
                                SubData = a["SubData"]

                            }).ToList();


            return _efSerializer.EFSerialize(elements);

        }
        #endregion

        #region "Generic method to Save Report Template"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="comapnyID"></param>
        /// <param name="reportName"></param>
        /// <param name="reportDescription"></param>
        /// <param name="arrReportElements"></param>
        /// <returns></returns>
        public string[] SaveReportTemplate(int reportId, string userID, string companyID, string reportName, string reportDescription, string[] arrLabelElements, string[] arrFieldElements,
                                string reportTitle, bool isCompanyLogo, bool isCompanyName, bool isReportTitle, bool isDateCreated,
                                string eventID, bool isUserToSystemReport, bool isTempReport, string[] arrEditLabelElements, string reportTitleStyle, string reportType)
        {
          
            int reportID = 0;
            string eventType = string.Empty;
            bool isTemporaryReport = false;
            tap.dat.Report reportValues;
            
            //Decrypt the user Id and pass
            int userId = Convert.ToInt32(userID);

            //Decrypt the company Id and pass
            int companyId = Convert.ToInt32(companyID);
            try
            {
                int eventId=0;

                if (eventID == null || eventID == "0")
                {
                    var reportData = _db.Report.FirstOrDefault(r => r.ReportID == reportId);

                    if (reportData != null)
                        eventID = _cryptography.Encrypt(Convert.ToString(reportData.EventID));
                }

                if (eventID != "0" && eventID != null)
                   eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                //save Report Table elements
                reportValues = SaveReportDetails(reportId, reportName, reportDescription, companyId, userId, reportTitle, isCompanyLogo, isCompanyName,
                                                isReportTitle, isDateCreated, eventId,  isUserToSystemReport, isTempReport, reportTitleStyle,reportType);

                reportID = reportValues.ReportID;
                isTemporaryReport = Convert.ToBoolean(reportValues.isTemporaryReport);
                eventType = reportType;

                //if it is UserType Report-save ReportId and UserId in UserReports table.
                if (reportType==USER_REPORT)
                    SaveUserReportsDetails(reportID, userId);

                //save data in ReportElement table.
                SaveReportElementDetails(reportID, arrLabelElements, arrFieldElements, arrEditLabelElements,companyId);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            string[] reportArray = new string[3];

            reportArray[0] = reportID.ToString();
            reportArray[1] = eventType;
            reportArray[2] = isTemporaryReport.ToString();

            return reportArray;

        }
        #endregion

        #region "Save User Reports table"
        public void SaveUserReportsDetails(int reportID, int userId)
        {
            tap.dat.UserReports userReport = new tap.dat.UserReports();
            try
            {
                userReport.ReportID = reportID;
                userReport.CreatedBy = userId;

                _db.AddToUserReports(userReport);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region "Save Report Table Details"
        public tap.dat.Report SaveReportDetails(int reportId, string reportName, string reportDescription, int companyId, int userId, string reportTitle, bool isCompanyLogo, bool isCompanyName,
                                    bool isReportTitle, bool isDateCreated, int eventId, bool isUserToSystemReport, bool isTempReport, string reportTitleStyle,string reportType)
        {
            tap.dat.Report report = null;
            try
            {
                bool isNewReport = true;

                report = _db.Report.FirstOrDefault(a => a.ReportID == reportId);

                if (report != null)
                    isNewReport = false;

                if (isNewReport)
                    report = new tap.dat.Report();
                else
                {
                    report.ReportID = reportId;
                    //delete from UserDetails table
                    if (isUserToSystemReport)
                    {
                        var userDetails = _db.UserReports.FirstOrDefault(a => a.ReportID == reportId);

                        _db.DeleteObject(userDetails);
                        _db.SaveChanges();
                    }
                }


                var reportTypeDetails = _db.ReportTypes.FirstOrDefault(a => a.ReportType == reportType);

                report.ReportName = reportName;
                report.Description = reportDescription;
                report.CompanyID = companyId;
                report.CreatedBy = userId;
                report.CreatedDate = DateTime.UtcNow; //GMT;
                report.ReportTitle = reportTitle;
                report.IsCompanyLogoUsed = isCompanyLogo;
                report.isCompanyNameUsed = isCompanyName;
                report.isReportTitleUsed = isReportTitle;
                report.isCreatedDateUsed = isDateCreated;
                report.ReportTypeId = reportTypeDetails.ReportTypeId;
                report.isTemplate = (reportType == SYSTEM_REPORT || reportType == USER_REPORT ? true : false);
               // report.ReportType = reportType;

                //if user type report - store eventid as 0
                if(reportType==EVENT_REPORT)
                    report.EventID = eventId;
                else
                    report.EventID = 0;

                report.isTemporaryReport = isTempReport;
                report.ReportTitleStyle = reportTitleStyle;
                
                if (isNewReport)
                    _db.AddToReport(report);

                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            transaction.Transaction.SaveTransaction(null, userId, null, Enumeration.TransactionCategory.Update, "Custom Report " + reportName + " added");

            return report;
        }
        #endregion

        #region "Save Report Element Table Details"
        public void SaveReportElementDetails(int reportId, string[] arrLabelElements, string[] arrFieldElements, string[] arrEditLabelElements,int companyId)
        {
            string arrLabelValues = string.Empty;
            string elementTypeId = string.Empty;
            string elementText = string.Empty;
            int elementSortOrder = 0;
            string elementTabName = string.Empty;
            string elementStyle = string.Empty;
            string integrateType = string.Empty;
            int snapCapImageId = 0;
            tap.dat.ReportElement reportElement = null;


            if (arrLabelElements != null)
            {
                for (int i = 0; i < arrLabelElements.Length; i++)
                {
                    arrLabelValues = arrLabelElements[i];
                    elementTypeId = arrLabelValues.Split(',')[0].ToString();
                    elementText = arrLabelValues.Split(',')[1].ToString();
                    elementSortOrder = Convert.ToInt32(arrLabelValues.Split(',')[2]);
                    elementTabName = arrLabelValues.Split(',')[3].ToString();
                    elementStyle = arrLabelValues.Split(',')[4].ToString();
                    integrateType = arrLabelValues.Split(',')[5].ToString();
                    snapCapImageId = Convert.ToInt32(arrLabelValues.Split(',')[6]);

                    if (elementText != CUSTOM_SMALLIMAGE && elementText !=CUSTOM_MEDIUMIMAGE && elementText !=CUSTOM_LARGEIMAGE)
                    {
                       

                        if (elementStyle != "")
                            elementStyle = elementStyle.Replace('!', ',').ToString();

                        reportElement = new tap.dat.ReportElement();

                        reportElement.ReportID = reportId;
                        //pass element name type and get type id
                        var elementLabelType = _db.ReportElementType.FirstOrDefault(a => a.ElementType == elementTypeId);


                        reportElement.ElementTypeID = elementLabelType.ElementTypeID;

                        if (snapCapImageId != 0)
                        {
                            reportElement.ElementText = SNAPCAP_IMAGE;
                        }
                        else
                            reportElement.ElementText = elementText;

                        reportElement.ElementStyle = elementStyle;
                        reportElement.SortOrder = elementSortOrder;
                        reportElement.TopPosition = string.Empty;
                        reportElement.LeftPosition = string.Empty;
                        reportElement.ReportImageData = null;
                        reportElement.IntegrateType = integrateType;

                        _db.AddToReportElement(reportElement);
                        _db.SaveChanges();

                        reportElement = new tap.dat.ReportElement();

                        string arrFieldValues = arrFieldElements[i];
                        reportElement.ReportID = reportId;

                        //get Custom/Data Field element Type
                        string elementFieldTypeId = arrFieldValues.Split(',')[0].ToString();

                        var elementFieldType = _db.ReportElementType.FirstOrDefault(a => a.ElementType == elementFieldTypeId);
                        reportElement.ElementTypeID = elementFieldType.ElementTypeID;
                        string elementDataText = arrFieldValues.Split(',')[1].ToString();

                        if (elementDataText == " ")//changed on 30th April
                            elementDataText = null;


                        reportElement.ElementText = elementDataText;
                        reportElement.ElementStyle = elementStyle;
                        reportElement.SortOrder = Convert.ToInt32(arrFieldValues.Split(',')[2]);
                        reportElement.TopPosition = string.Empty;
                        reportElement.LeftPosition = string.Empty;

                        //image data for snapCap
                        byte[] snapCapImageByte;
                        if (snapCapImageId != 0)
                        {
                           
                            var getSnapCapImageData = from snapcap in _db.SnapCaps
                                                      where snapcap.SnapCapID == snapCapImageId
                                                      select snapcap.SnapCapData;

                            foreach (var data in getSnapCapImageData)
                            {
                                snapCapImageByte = data.ToArray();

                                // 

                                //convert to image and resize to Large size(600/500)
                                string fileName = WriteImage(snapCapImageByte, 99);
                                string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                                //check directory exists or not -if not create it.
                                if (Directory.Exists(tempPath) == false)
                                {
                                    Directory.CreateDirectory(tempPath);
                                }

                                string tempFile = tempPath + fileName;
                                string newFileName = fileName;
                                string newFilePath = tempPath + 2;                              

                                System.Drawing.Image img = System.Drawing.Image.FromFile(tempPath + fileName);

                                if (img.Width > 500 || img.Height > 600)
                                {
                                    ResizeImage(tempFile, ImageSizes.size_510x510, fileName, newFilePath);
                                   
                                    img.Dispose();
                                    reportElement.ReportImageData = File.ReadAllBytes(newFilePath);//pass the byte to dbase to save image;
                                   
                                    //delete created image files after save into dbase
                                    System.IO.File.Delete(tempFile);
                                    System.IO.File.Delete(newFilePath);

                                }
                                else
                                {                                  
                                    reportElement.ReportImageData = snapCapImageByte;
                                    //close the original image
                                    img.Dispose();                                    
                                    System.IO.File.Delete(tempFile);
                                }
                            }                         
                            
                        }
                        else
                            reportElement.ReportImageData = null;
                      
                          

                        reportElement.IntegrateType = integrateType;

                        _db.AddToReportElement(reportElement);
                        _db.SaveChanges();



                        int elementId = reportElement.ElementID;
                        if (elementFieldTypeId == DATA_FIELD)
                        {
                            //pass table name and column Name and save in ReportElementDataSource
                            SaveReportElementDataSourceDetails(elementId, elementText, elementTabName);
                        }
                        else if (elementFieldTypeId == CUSTOM_FIELD)
                        {
                            //pass tab name and column name to Fields table to get Field id
                            SaveReportElementCustomSourceDetails(elementId, elementText, elementTabName, companyId);
                        }
                    }
                }
            }
            if (arrEditLabelElements != null && arrEditLabelElements.Length > 0)//edit
            {

                string arrEditLabelValues = string.Empty;
                for (int i = 0; i < arrEditLabelElements.Length; i++)
                {

                    arrEditLabelValues = arrEditLabelElements[i];
                    string editReportId = arrEditLabelValues.Split(',')[0].ToString();
                    int editElementId = Convert.ToInt32(arrEditLabelValues.Split(',')[1].ToString());
                    string editElementStyle = arrEditLabelValues.Split(',')[2].ToString();
                    int editSortOrder = Convert.ToInt32(arrEditLabelValues.Split(',')[3].ToString());
                    string integrateValue = arrEditLabelValues.Split(',')[4].ToString();
                    int editSnapCapImageId = 0;

                    if (editElementStyle != "")
                        editElementStyle = editElementStyle.Replace('!', ',').ToString();

                    reportElement = _db.ReportElement.FirstOrDefault(a => a.ElementID == editElementId);
                    reportElement.ElementStyle = editElementStyle;
                    reportElement.SortOrder = editSortOrder;
                    //reportElement.IntegrateType = integrateValue;
                  
                    _db.SaveChanges();

                    editElementId = editElementId - 1;
                    reportElement = _db.ReportElement.FirstOrDefault(a => a.ElementID == editElementId);
                    reportElement.ElementStyle = editElementStyle;
                    reportElement.SortOrder = editSortOrder;
                   

                    _db.SaveChanges();

                }
            }

        }

     
        #endregion

        #region "Save ReportElementDataSource table"
        public void SaveReportElementDataSourceDetails(int elementId, string columnName, string elementTableName)
        {
            tap.dat.ReportElementDataSource reportElementDS = new tap.dat.ReportElementDataSource();
            reportElementDS.ReportElementID = elementId;

            reportElementDS.DataSourceColumnName = ((columnName == CLASSIFICATION_COLUMN) ? VALUEID_COLUMN : columnName);

            reportElementDS.DataSourceTableName = ((columnName == CLASSIFICATION_COLUMN) ? FIELDLISTVALUES_TABLE : elementTableName);

            _db.AddToReportElementDataSource(reportElementDS);
            _db.SaveChanges();
        }
        #endregion

        #region "Save ReportElementCustomDource table"
        public void SaveReportElementCustomSourceDetails(int elementId, string fieldName, string tabName, int companyId)
        {
            var getElementFieldType = (from t in _db.Tabs
                                       join f in _db.Fields on t.TabID equals f.TabID //into tempfields
                                       //from ft in tempfields.DefaultIfEmpty()
                                       where t.TabName == tabName && f.DisplayName == fieldName && t.CompanyID==companyId
                                       select f.FieldID).FirstOrDefault();


            //pass Field id and save in ReportElementCustomSource
            tap.dat.ReportElementCustomSource reportElementCS = new tap.dat.ReportElementCustomSource();
            reportElementCS.ReportElementID = elementId;
            reportElementCS.CustomFieldID = getElementFieldType;
            _db.AddToReportElementCustomSource(reportElementCS);
            _db.SaveChanges();
        }
        #endregion

        #region "Generate Report both In Grid and Report Template page"
        public object GenerateReport(string eventID, int reportId)
        {

            List<string> imageFileNames = new List<string>();

           string reportLogoName = GetReportLogoImageData();

            imageFileNames = GetImageData(reportId);

            int eventId = 0;

            if (eventID == null || eventID == "null")
                eventID = _cryptography.Encrypt(Convert.ToString(_db.Report.FirstOrDefault(r => r.ReportID == reportId).EventID));

            if (eventID != "0")
                eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

            //User/System types
            if (eventId == 0)
            {
                //get all column values and Label names by sort order-deepa
                var reportResults = (from rpt in _db.ReportElement.AsEnumerable()
                                     join r in _db.Report on rpt.ReportID equals r.ReportID
                                     join usr in _db.Users on r.CreatedBy equals usr.UserID
                                     join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                     where rpt.ElementText != null && r.ReportID == reportId
                                     orderby rpt.SortOrder

                                     select new
                                     {
                                         ElementId = rpt.ElementID,
                                         ReportName = r.ReportName,
                                         CreatedBy = usr.UserName,
                                         ElementText = rpt.ElementText,
                                         ElementValue = 0,
                                         ElementStyle = rpt.ElementStyle,
                                         TopPosition = rpt.TopPosition,
                                         LeftPosition = rpt.LeftPosition,
                                         FileName = imageFileNames,
                                         IntegrateType=rpt.IntegrateType,
                                         ReportType=rt.ReportType,
                                         isAdminReport=true,
                                         ReportLogoName = reportLogoName,
                                         ReportTitle=r.ReportTitle
                                     }).AsEnumerable();

                return _efSerializer.EFSerialize(reportResults);
            }
            else//event and normal reports types
            {
                //get all column values and Label names by sort order-optimise the query
                var reportResults = (from rpt in _db.ReportElement.AsEnumerable()
                                     join reds in _db.ReportElementDataSource.AsEnumerable() on rpt.ElementID equals reds.ReportElementID into tempreds
                                     from eds in tempreds.DefaultIfEmpty()
                                     join cust in _db.ReportElementCustomSource.AsEnumerable() on rpt.ElementID equals cust.ReportElementID into temprecs
                                     from ecs in temprecs.DefaultIfEmpty()
                                     join r in _db.Report on rpt.ReportID equals r.ReportID
                                     join usr in _db.Users on r.CreatedBy equals usr.UserID
                                     join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                     where r.ReportID == reportId
                                     orderby rpt.SortOrder
                                     let ColumnValue = ((rpt.ElementTypeID == 4 || rpt.ElementTypeID == 5 || rpt.ElementTypeID==6) ?
                                     GetColumnValue(rpt.ElementTypeID, eds, ecs, eventId, rpt.ElementID) : string.Empty)

                                     select new
                                     {
                                         ElementId = rpt.ElementID,
                                         ReportName = r.ReportName,
                                         CreatedUser = usr.UserName,
                                         ElementStyle = rpt.ElementStyle,
                                         ElementText = rpt.ElementText,
                                         ColumnValue = ColumnValue,
                                         Id = rpt.ElementID,
                                         SortOrder = rpt.SortOrder,
                                         TopPosition = rpt.TopPosition,
                                         LeftPosition = rpt.LeftPosition,
                                         IntegrateType = rpt.IntegrateType,
                                         ReportType = rt.ReportType,
                                         isAdminReport = false,
                                         ReportLogoName = reportLogoName,
                                         ReportTitle = r.ReportTitle
                                     }).AsEnumerable();

                //pass resultset which is not null-Label Names
                var labelItems = reportResults.ToList().Where(a => a.ElementText != null).Select(a => a);
               
                //get Label Name and Value 
                var textValues = from labelItem in labelItems
                                 select new
                                 {
                                     ElementId = labelItem.ElementId,
                                     ReportName = labelItem.ReportName,
                                     CreatedBy = labelItem.CreatedUser,
                                     ElementText = labelItem.ElementText,
                                     ElementValue = (from result in reportResults
                                                     where result.SortOrder == labelItem.SortOrder && result.ElementText == null
                                                     select (labelItem.ElementText == "Event Date/Time" ? result.ColumnValue.ToString().Substring(0, 10) : result.ColumnValue)).FirstOrDefault(),// need to format the date in other way 
                                     ElementStyle = labelItem.ElementStyle,
                                     TopPosition = labelItem.TopPosition,
                                     LeftPosition = labelItem.LeftPosition,
                                     FileName = imageFileNames,
                                     IntegrateType = labelItem.IntegrateType,
                                     ReportType = labelItem.ReportType,
                                     AdminReport = labelItem.isAdminReport,
                                     ReportLogoName = labelItem.ReportLogoName,
                                     ReportTitle = labelItem.ReportTitle
                                 };


                return _efSerializer.EFSerialize(textValues);
            }

        }

        #region "Get Image Data and save in temp folder"
        public List<string> GetImageData(int reportId)
        {
            List<string> fileNames = new List<string>();           

            var getImageData = from rpt in _db.ReportElement.AsEnumerable()
                               where rpt.ReportID == reportId && rpt.ReportImageData != null
                               select new
                               {
                                   ReportImageData = rpt.ReportImageData,
                                   ElementId = rpt.ElementID,
                                   ImageSize=rpt.ImageSize
                               };
           

            if (getImageData.ToList().Count > 0)
            {
                foreach (var data in getImageData)
                {
                    string fileName = WriteImage(data.ReportImageData, data.ElementId);

                    fileNames.Add(fileName);
                }
            }
            return fileNames;
        }

        //get Report Logo name
        public string GetReportLogoImageData(int? companyId = null)
        {
            tap.dat.Companies getReportLogoData = new tap.dat.Companies();

            getReportLogoData = GetCompanyDetails(companyId);

            string logoFileName = string.Empty;

            if (getReportLogoData != null)
            {
                logoFileName = WriteImage(getReportLogoData.ReportLogo, getReportLogoData.CompanyID, getReportLogoData.CompanyName);
                return logoFileName;

            }
            return string.Empty;
        }
        #endregion

        #region "WriteImage and save in temp folder"
       
        public string WriteImage(byte[] reportData, int elementId, string elementName = null)
        {

            if (reportData != null && elementId != 0)
            {
                string FilePath = string.Empty;
                string tempPath = System.Web.Hosting.HostingEnvironment.MapPath("~/temp/");

                //check directory exists or not -if not create it.
                if (Directory.Exists(tempPath) == false)
                {
                    Directory.CreateDirectory(tempPath);
                }

                List<string> fileNames = new List<string>();

                string fileName = string.Empty;

                byte[] arrayFormat = reportData;

                MemoryStream ms = new MemoryStream((byte[])arrayFormat);

                System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);

                if (elementName == null)
                    fileName = "Img_" + elementId + ".jpg";
                else
                    fileName = elementName + "_" + elementId + ".jpg";

                FilePath = tempPath + fileName;

                // Just resample the Original Image into a new Bitmap
                var ResizedBitmap = new System.Drawing.Bitmap(returnImage);

                ImageFormat Format = ImageFormat.Jpeg;              

                System.Drawing.Imaging.Encoder quality = System.Drawing.Imaging.Encoder.Quality;
                var ratio = new EncoderParameter(quality, 100L);
                var codecParams = new EncoderParameters(1);
                codecParams.Param[0] = ratio;

                ResizedBitmap.Save(FilePath, GetEncoder(ImageFormat.Jpeg), codecParams);
                return fileName;
            }
            return null;
        }
        #endregion

        #region "encode the image"
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
                if (codec.FormatID == format.Guid)
                    return codec;
            return null;
        }
        #endregion

        #endregion

        #region "Get TapRooT Integrate Values"
        public object GetTapRooTIntegrateValues(string eventID, int reportId, string integrateType)
        {

            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

            var autumnSeasonID = (from season in _db.Seasons where season.SeasonName == AUTUMN_SEASON select season).FirstOrDefault().SeasonID;

            //(HARD CODED FOR NOW ONLY, as no causal factor screen is present in the application)
            var snapChartDetails = (from snapCharts in _db.SnapCharts
                                    where snapCharts.EventID == eventId &&
                                        snapCharts.SeasonID == autumnSeasonID
                                    select snapCharts).FirstOrDefault();


            int rootCauseCategoryID = Convert.ToInt32((from rootCauseType in _db.RootCauseCategories 
                                          where rootCauseType.Category == TAPROOT_RCT 
                                          select rootCauseType.RootCauseCategoryID).FirstOrDefault());

            if (snapChartDetails != null)
            {
                int snapChartID = snapChartDetails.SnapChartID;

                if (integrateType == TAPROOT_CF)
                {
                    //Get causal factor details
                    return GetCausalFactorDetails(snapChartID, eventId, rootCauseCategoryID);                   
                  
                }
                else if (integrateType == TAPROOT_RCT)
                { 
                    //get root cause details
                    return GetRootCauseDetails(snapChartID, eventId, rootCauseCategoryID);
                }
                else if (integrateType == TAPROOT_CAP)
                {
                    //Get Corrective Action Plan details
                    return GetActionPlanDetails(snapChartID, eventId, rootCauseCategoryID);
                }
               
            }


            return null;        
        }

        public object[] GetAllTapRooTIntegrateValues(string eventID, int reportId, string[] integrateType)
        {

            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            object[] resultObject =  new object[3];

            var autumnSeasonID = (from season in _db.Seasons where season.SeasonName == AUTUMN_SEASON select season).FirstOrDefault().SeasonID;

            //(HARD CODED FOR NOW ONLY, as no causal factor screen is present in the application)
            var snapChartDetails = (from snapCharts in _db.SnapCharts
                                    where snapCharts.EventID == eventId &&
                                        snapCharts.SeasonID == autumnSeasonID
                                    select snapCharts).FirstOrDefault();


            int rootCauseCategoryID = Convert.ToInt32((from rootCauseType in _db.RootCauseCategories
                                                       where rootCauseType.Category == TAPROOT_RCT
                                                       select rootCauseType.RootCauseCategoryID).FirstOrDefault());

            if (snapChartDetails != null)
            {
                int snapChartID = snapChartDetails.SnapChartID;

                for (int i = 0; i < integrateType.Length; i++)
                {
                    if (integrateType[i] == TAPROOT_CF)
                    {
                        //Get causal factor details
                        //var resCF = GetCausalFactorDetails(snapChartID, eventId, rootCauseCategoryID);
                        var resCF = GetCausalFactorDetailsTable(snapChartID, eventId, rootCauseCategoryID);
                        resultObject[i] = resCF;

                    }
                    else if (integrateType[i] == TAPROOT_RCT)
                    {
                        //get root cause details
                        //var resRCT = GetRootCauseDetails(snapChartID, eventId, rootCauseCategoryID);
                        var resRCT = GetRootCauseDetailsTable(snapChartID, eventId, rootCauseCategoryID);
                        resultObject[i] = resRCT;
                    }
                    else if (integrateType[i] == TAPROOT_CAP)
                    {
                        //Get Corrective Action Plan details
                        //var resCAP = GetActionPlanDetails(snapChartID, eventId, rootCauseCategoryID);
                        var resCAP = GetActionPlanDetailsTable(snapChartID, eventId, rootCauseCategoryID);
                        resultObject[i] = resCAP;
                    }
                }

                return resultObject;
            }
            else
                return null;
        }

      
        #region "Get causal factor details"
        /// <summary>
        /// GetCausalFactorDetails
        /// </summary>
        /// <param name="snapChartID"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public object GetCausalFactorDetails(int snapChartID, int eventId, int rootCauseCategoryID)
        {
            //added by deepa- depends on event id get Cap values
            var getCAPValues = (from carct in _db.CorrectiveActionRCT
                                join caps in _db.CorrectiveActions.AsEnumerable() on carct.CorrectiveActionID equals caps.CorrectiveActionID
                                where caps.EventID == eventId
                                select new
                                {
                                    ActionPlans = ((caps.Identifier == "" || caps.Identifier == null) ? caps.Description : (caps.Description == "" || caps.Description == null ? caps.Identifier : caps.Identifier + " - " + caps.Description)),
                                    CorrectiveActionID = caps.CorrectiveActionID,
                                    RootCauseTreeID = carct.RootCauseTreeID
                                }).Distinct();


            var causalFactorDetail = (from causalFactor in _db.CausalFactors.AsEnumerable()
                                      where causalFactor.SnapChartID == snapChartID
                                      select new
                                      {
                                          ResultType = "Causal Factor",
                                          CausalFactorName = causalFactor.Name,
                                          RootCauses = (from rootCauseTree in _db.RootCauseTree.AsEnumerable()
                                                        join rootCauses in _db.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                                        equals rootCauses.RootCauseID
                                                        join rootCauseCategory in _db.RootCauseCategories.AsEnumerable() on rootCauses.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                                                        where rootCauseTree.CausalFactorID == causalFactor.CausalFactorID && rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == rootCauseCategoryID
                                                                                                              
                                                        select new
                                                        {
                                                            RootCauses = rootCauses.Title,
                                                            ActionPlans = (from carct in getCAPValues
                                                                           where carct.RootCauseTreeID == rootCauseTree.RootCauseTreeID
                                                                           select new
                                                                           {                                                                              
                                                                               ActionPlans = carct.ActionPlans,
                                                                               Tasks = (from t in _db.Tasks.AsEnumerable()
                                                                                        join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                                                                        join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                                                                        join caps in _db.CorrectiveActions on t.CorrectiveActionID equals caps.CorrectiveActionID
                                                                                        where t.CorrectiveActionID == carct.CorrectiveActionID
                                                                                        orderby t.DueDate
                                                                                        select new
                                                                                        {
                                                                                            Tasks = tt.TaskTypeName + "," + trp.DisplayName
                                                                                        }).AsEnumerable().Distinct(),

                                                                           }).AsEnumerable()


                                                        }).AsEnumerable()
                                      }).AsEnumerable();

            return _efSerializer.EFSerialize(causalFactorDetail);
        }
        #endregion 

        #region "Get Root Cause Details"
        public object GetRootCauseDetails(int snapChartID, int eventId, int rootCauseCategoryID)
        {
         
            // Get the causal factors for the events
            var cfIDs =  (from causalFactor in _db.CausalFactors.AsEnumerable()
                                   where causalFactor.SnapChartID == snapChartID select causalFactor.CausalFactorID);

            // Get distinct root causes found for the causal facotrs related to the event
             var distinctRootCauses = (from rootCauseTree in _db.RootCauseTree.AsEnumerable()
                                        join rootCauses in _db.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                        equals rootCauses.RootCauseID
                                        join causalFactor in _db.CausalFactors.AsEnumerable() on rootCauseTree.CausalFactorID equals causalFactor.CausalFactorID                                        
                                        where rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == 3
                                        && cfIDs.Contains(rootCauseTree.CausalFactorID)
                                       select rootCauses).ToList().Distinct();

            // a hardcoded cf, for using the existing template used for 'By causal factor'
            IList<string> cfs = new List<string>();
            cfs.Add("My Causal Factor");


            //for those distinct root causes find the caps and task added
            var result = (from causalFactor in cfs
                         select new
                         {
                            ResultType = "Root Cause",
                            CausalFactorName = causalFactor.FirstOrDefault(),
                            RootCauses =  (from rootCauses in distinctRootCauses
                                            select new
                                            {
                                                
                                                RootCauses = rootCauses.Title,
                                                RootCauseId = rootCauses.RootCauseID,
                                                RCTIDs = from rctt in _db.RootCauseTree.AsEnumerable()
                                                         where rctt.RootCauseID == rootCauses.RootCauseID && cfIDs.Contains(rctt.CausalFactorID)
                                                         select rctt.RootCauseTreeID,
                                                ActionPlans = (from carct in _db.CorrectiveActionRCT.AsEnumerable()
                                                                join cap in _db.CorrectiveActions.AsEnumerable() on carct.CorrectiveActionID equals cap.CorrectiveActionID
                                                                where (from rctt in _db.RootCauseTree.AsEnumerable()
                                                                        where rctt.RootCauseID == rootCauses.RootCauseID && cfIDs.Contains(rctt.CausalFactorID)
                                                                        select rctt.RootCauseTreeID).Contains(carct.RootCauseTreeID)
                                                                select new
                                                                {
                                                                    ActionPlans = ((cap.Identifier=="" || cap.Identifier==null) ? cap.Description : (cap.Description=="" || cap.Description==null? cap.Identifier :cap.Identifier + " - " + cap.Description)),
                                                                    Tasks = (from t in _db.Tasks.AsEnumerable()
                                                                             join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                                                             join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                                                             where t.CorrectiveActionID == cap.CorrectiveActionID
                                                                             orderby t.DueDate
                                                                             select new
                                                                             {
                                                                                 Tasks = tt.TaskTypeName + "," + trp.DisplayName
                                                                             }
                                                                    ).AsEnumerable().Distinct(),
                                                            }).Distinct(),
                                            }),
                    });

            return _efSerializer.EFSerialize(result);


        }
        #endregion

        #region "Get Corrective Action Plan details"
        /// <summary>
        /// Get Corrective Action Plan details
        /// </summary>
        /// <param name="snapChartID"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public object GetActionPlanDetails(int snapChartID, int eventId, int rootCauseCategoryID)
        {
            var actionPlanDetail = (from caps in _db.CorrectiveActions.AsEnumerable()
                                    where caps.EventID == eventId
                                    select new
                                    {
                                        ResultType = "Action Plan",
                                        ActionPlans = ((caps.Identifier=="" || caps.Identifier==null) ? caps.Description : (caps.Description=="" || caps.Description==null? caps.Identifier :caps.Identifier + " - " + caps.Description)),
                                      
                                        Tasks=(from t in _db.Tasks.AsEnumerable()
                                                   join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                                   join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                                   where t.CorrectiveActionID==caps.CorrectiveActionID orderby t.DueDate
                                                   select new
                                                   {
                                                        Tasks = tt.TaskTypeName+ "," + trp.DisplayName
                                                   }
                                                   ).AsEnumerable().Distinct(),
                                                RootCauses = (from carct in _db.CorrectiveActionRCT.AsEnumerable()
                                                      join
                                                          rct in _db.RootCauseTree.AsEnumerable() on carct.RootCauseTreeID
                                                          equals rct.RootCauseTreeID
                                                      join rc in _db.RootCauses.AsEnumerable() on rct.RootCauseID
                                                     equals rc.RootCauseID

                                                      join rootCauseCategory in _db.RootCauseCategories.AsEnumerable() on rc.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                                                      where rct.IsSelected == true && carct.CorrectiveActionID == caps.CorrectiveActionID && rc.RootCauseCategoryID == rootCauseCategoryID
                                                      select new
                                                      {
                                                          RootCauses = ((rc.Title == "" || rc.Title == null) ? "No Root Causes" : rc.Title),
                                                          CausalFactorName = (from causalFactor in _db.CausalFactors.AsEnumerable()
                                                                              where causalFactor.SnapChartID == snapChartID && rct.CausalFactorID == causalFactor.CausalFactorID
                                                                              select new
                                                                              {
                                                                                  CausalFactorName = causalFactor.Name
                                                                              }).AsEnumerable().Distinct(),
                                                      }).AsEnumerable(),


                                    }).AsEnumerable();    
            return _efSerializer.EFSerialize(actionPlanDetail);
        
        }
        #endregion

        #region Get Report data using sprocs

        public object GetCausalFactorDetailsTable(int snapChartID, int eventId, int rootCauseCategoryID)
        {
            var result = (from CARCT in _db.CorrectiveActionRCT
                          join CA in _db.CorrectiveActions on CARCT.CorrectiveActionID equals CA.CorrectiveActionID
                          join RCT in _db.RootCauseTree on CARCT.RootCauseTreeID equals RCT.RootCauseTreeID
                          join RC in _db.RootCauses on RCT.RootCauseID equals RC.RootCauseID
                          join RCC in _db.RootCauseCategories on RC.RootCauseCategoryID equals RCC.RootCauseCategoryID
                          join CF in _db.CausalFactors on RCT.CausalFactorID equals CF.CausalFactorID
                          where RCC.RootCauseCategoryID == 3 && RCT.IsSelected == true && CA.EventID == eventId
                          orderby CF.CausalFactorID
                          select new
                          {
                              ResultType = "Causal Factor",
                              Identifier = CA.Identifier,
                              Name = CF.Name,
                              CausalFactorID = CF.CausalFactorID,
                              Title = RC.Title,
                              Tasks = (from T in _db.Tasks
                                       join TT in _db.TaskType on T.TaskTypeID equals TT.TaskTypeID
                                       join TRP in _db.TasksResponsiblePersons on T.TaskID equals TRP.TaskId
                                       where T.CorrectiveActionID == CA.CorrectiveActionID
                                       select new { 
                                           TaskTypeName = TT.TaskTypeName,
                                           TaskResponsiblePerson = TRP.DisplayName
                                       })
                          }).AsEnumerable();

            return _efSerializer.EFSerialize(result);
        }

        public object GetRootCauseDetailsTable(int snapChartID, int eventId, int rootCauseCategoryID)
        {
            var result = (from CARCT in _db.CorrectiveActionRCT
                          join CA in _db.CorrectiveActions on CARCT.CorrectiveActionID equals CA.CorrectiveActionID
                          join RCT in _db.RootCauseTree on CARCT.RootCauseTreeID equals RCT.RootCauseTreeID
                          join RC in _db.RootCauses on RCT.RootCauseID equals RC.RootCauseID
                          join RCC in _db.RootCauseCategories on RC.RootCauseCategoryID equals RCC.RootCauseCategoryID
                          where RCC.RootCauseCategoryID == 3 && RCT.IsSelected == true && CA.EventID == eventId
                          select new
                          {
                              ResultType = "Root Cause",
                              CausalFactorID = RCT.CausalFactorID,
                              Identifier = CA.Identifier,
                              Title = RC.Title,
                              Tasks = (from T in _db.Tasks
                                       join TT in _db.TaskType on T.TaskTypeID equals TT.TaskTypeID
                                       join TRP in _db.TasksResponsiblePersons on T.TaskID equals TRP.TaskId
                                       where T.CorrectiveActionID == CA.CorrectiveActionID
                                       select new
                                       {
                                           TaskTypeName = TT.TaskTypeName,
                                           TaskResponsiblePerson = TRP.DisplayName
                                       })
                          }).AsEnumerable();

            return _efSerializer.EFSerialize(result);
        }

        public object GetActionPlanDetailsTable(int snapChartID, int eventId, int rootCauseCategoryID)
        {
            var result = (from CARCT in _db.CorrectiveActionRCT
                          join CA in _db.CorrectiveActions on CARCT.CorrectiveActionID equals CA.CorrectiveActionID
                          join RCT in _db.RootCauseTree on CARCT.RootCauseTreeID equals RCT.RootCauseTreeID
                          join RC in _db.RootCauses on RCT.RootCauseID equals RC.RootCauseID
                          join RCC in _db.RootCauseCategories on RC.RootCauseCategoryID equals RCC.RootCauseCategoryID
                          join CF in _db.CausalFactors on RCT.CausalFactorID equals CF.CausalFactorID
                          where  CA.EventID == eventId && RCC.RootCauseCategoryID == 3
                          orderby CA.CorrectiveActionID
                          select new
                          {
                              ResultType = "Action Plan",
                              CorrectiveActionID = CA.CorrectiveActionID,
                              IsSelected = RCT.IsSelected,
                              Name = CF.Name,
                              Identifier = CA.Identifier + " - " + CA.Description,
                              Title = RC.Title,
                              Tasks = (from T in _db.Tasks
                                       join TT in _db.TaskType on T.TaskTypeID equals TT.TaskTypeID
                                       join TRP in _db.TasksResponsiblePersons on T.TaskID equals TRP.TaskId
                                       where T.CorrectiveActionID == CA.CorrectiveActionID
                                       select new
                                       {
                                           TaskTypeName = TT.TaskTypeName,
                                           TaskResponsiblePerson = TRP.DisplayName
                                       })
                          }).AsEnumerable();

            return _efSerializer.EFSerialize(result);
        }

        #endregion


        #endregion

        #region "Print - Integrate Values"
        public object GetPrintIntegrateValues(string eventID, string integrateType)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();

            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

            var autumnSeasonID = (from season in _db.Seasons where season.SeasonName == AUTUMN_SEASON select season).FirstOrDefault().SeasonID;

            var snapChartDetails = (from snapCharts in _db.SnapCharts
                                    where snapCharts.EventID == eventId &&
                                        snapCharts.SeasonID == autumnSeasonID
                                    select snapCharts).FirstOrDefault();


            var rcType = (from rootCauseType in _db.RootCauseCategories where rootCauseType.Category == TAPROOT_RCT select rootCauseType).FirstOrDefault();

            int rootCauseCategoryID = Convert.ToInt32(rcType.RootCauseCategoryID);

            StringBuilder sb = new StringBuilder();
            if (integrateType == TAPROOT_CF)
            {
                if (snapChartDetails != null)
                    return GetCausalFactorPrint(eventId, snapChartDetails.SnapChartID, rootCauseCategoryID);
                else
                {

                    sb.AppendFormat("<tr border='2' style='color:#aaaaaa'> ");
                    sb.AppendFormat("<td colspan='3'>{0}</td>", "No Causal Factor.");
                    sb.AppendFormat("</tr><br>/n");
                    return sb.ToString();
                   
                }
            }
            else if (integrateType == TAPROOT_RCT)
            {
                if (snapChartDetails != null)
                     return GetRootCausesPrint(eventId, snapChartDetails.SnapChartID, rootCauseCategoryID);
                else
                {
                    sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
                    sb.AppendFormat("<td colspan='3'>{0}</td>", "No Root Causes.");
                    sb.AppendFormat("</tr><br>/n");
                    return sb.ToString();
                }
            }
            else if (integrateType == TAPROOT_CAP)
            {
                if (snapChartDetails != null)
                    return GetActionPlansPrint(eventId, snapChartDetails.SnapChartID, rootCauseCategoryID);
                else
                {
                    sb.AppendFormat("<tr border='1' style='color:#aaaaaa'>");
                    sb.AppendFormat("<td colspan='3'>{0}</td>", "No Action Plans.");
                    sb.AppendFormat("</tr><br>/n");
                    return sb.ToString();
                }
            }
            return string.Empty;
        }


        #region "Print - By Causal Factor"
        public object GetCausalFactorPrint(int eventId, int snapChartID, int rootCauseCategoryID)
        {
            int iCounter = 0;

            //create string builder
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<td colspan='3'><table>");
            var getCausalFactor = (from causalFactor in _db.CausalFactors.AsEnumerable()
                                   where causalFactor.SnapChartID == snapChartID
                                   select new
                                   {
                                       causalFactorId = causalFactor.CausalFactorID,
                                       CausalFactorName = causalFactor.Name
                                   });

            //  pass causal factor id and get RCT,CAP and Tasks
            foreach (var cfId in getCausalFactor)
            {
                // create Causal Factor and string builder
                sb.AppendFormat("<tr border='1' cellpadding='5' style='text-align:center;color:#ffffff;margin-bottom:5px;' bgcolor='#000000'>");
                sb.AppendFormat("<td colspan='3' class='divHeaderValue'>{0}</td></tr>","Causal Factor: " + cfId.CausalFactorName);
              
                var rct = (from rootCauseTree in _db.RootCauseTree.AsEnumerable()
                               join rootCauses in _db.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                               equals rootCauses.RootCauseID
                               join rootCauseCategory in _db.RootCauseCategories.AsEnumerable() on rootCauses.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                               where rootCauseTree.CausalFactorID == cfId.causalFactorId && rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == rootCauseCategoryID
                               select new
                               {
                                   RootCauses = rootCauses.Title,
                                   RootCauseTreeId = rootCauseTree.RootCauseTreeID
                               });
                if (rct.Count() > 0)
                {
                    sb.AppendFormat("<tr border='1' cellpadding='5' bgcolor='#AAAAAA' style='text-align:center;'>");
                    sb.AppendFormat("<td>{0}</td>", "Root Cause");
                    sb.AppendFormat("<td>{0}</td>", "Corrective Action");
                    sb.AppendFormat("<td>{0}</td>", "Tasks");
                    sb.AppendFormat("</tr>");

                    foreach (var rctID in rct)
                    {
                        //  if(rctID.RootCauseTreeId)
                        var id = rctID.RootCauseTreeId;
                        iCounter = 0;

                        var getCAPValues = (from carct in _db.CorrectiveActionRCT.AsEnumerable()
                                            join caps in _db.CorrectiveActions.AsEnumerable()
                                                on carct.CorrectiveActionID equals caps.CorrectiveActionID
                                            where caps.EventID == eventId && carct.RootCauseTreeID == id
                                            select new
                                            {
                                                ActionPlans = ((caps.Identifier == "" || caps.Identifier == null) ? caps.Description : (caps.Description == "" || caps.Description == null ? caps.Identifier : caps.Identifier + " - " + caps.Description)),
                                                CorrectiveActionID = caps.CorrectiveActionID,
                                                RootCauseTreeID = carct.RootCauseTreeID
                                            }).Distinct();

                        //if (getCAPValues.Count() == 0)
                        //{
                        //    sb.AppendFormat("<tr border='1' style='color:#AAAAAA'> ");
                        //    sb.AppendFormat("<td colspan='3'>{0}</td>", rctID.RootCauses);
                        //}
                        //else
                        //{
                            sb.AppendFormat("<tr border='1' style='color:#AAAAAA'> ");
                            sb.AppendFormat("<td >{0}</td>", rctID.RootCauses);
                        //}

                        if (getCAPValues.Count() > 0)
                        {
                            foreach (var cap in getCAPValues)
                            {
                                var tasks = (from t in _db.Tasks.AsEnumerable()
                                             join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                             join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                             where t.CorrectiveActionID == cap.CorrectiveActionID
                                             orderby t.DueDate
                                             select new
                                             {
                                                 Task = tt.TaskTypeName + ", " + trp.DisplayName + " " + "&nbsp;&nbsp;<br/>"
                                             }).AsEnumerable().Distinct();

                                if (tasks.Count() == 0)
                                {
                                    sb.AppendFormat("<td colspan='2'>{0}</td>", cap.ActionPlans);
                                }
                                else
                                    sb.AppendFormat("<td>{0}</td>", cap.ActionPlans);

                                if (tasks.Count() > 0)
                                {
                                    sb.AppendFormat("<td>");
                                    foreach (var tsk in tasks)
                                    {
                                        sb.AppendFormat(tsk.Task);
                                    }
                                    sb.AppendFormat("</td>");
                                }

                                iCounter++;
                                if (getCAPValues.Count() > 1 && iCounter < getCAPValues.Count())
                                {
                                    sb.AppendFormat("</tr><tr border='1' style='color:#aaaaaa'><td></td>");
                                }
                            }
                        }
                        else
                        {
                            //sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
                            sb.AppendFormat("<td colspan='2'>{0}</td>", "No Action Plans.</tr>");
                        }

                        sb.AppendFormat("</tr>");
                    }
                }
                else
                {
                    sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
                    sb.AppendFormat("<td colspan='3'>{0}</td>", "No Root Causes.</tr>");
                }
                //sb.AppendFormat("</tr>");
            }
            if (sb.ToString() == "")
            {
                sb.AppendFormat("<tr border='1' cellpadding='5' style='color:#aaaaaa'> ");
                sb.AppendFormat("<td colspan='3'>{0}</td>", "No Causal Factor.</tr>");
            }

            sb.AppendFormat("</table></td>");
            return sb.ToString();

        }
        #endregion

        #region "Print - By Root Cause"
        public object GetRootCausesPrint(int eventId, int snapChartID, int rootCauseCategoryID)
        {
            int iCounter = 0;

            //create string builder
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<td colspan='3'><table>");
            var getCausalFactor = (from causalFactor in _db.CausalFactors.AsEnumerable()
                                   where causalFactor.SnapChartID == snapChartID
                                   select new
                                   {
                                       causalFactorId = causalFactor.CausalFactorID,
                                       CausalFactorName = causalFactor.Name
                                   });

            //  pass causal factor id and get RCT,CAP and Tasks
            foreach (var cfId in getCausalFactor)
            {
              var rct = (from rootCauseTree in _db.RootCauseTree.AsEnumerable()
                               join rootCauses in _db.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                               equals rootCauses.RootCauseID
                               join rootCauseCategory in _db.RootCauseCategories.AsEnumerable() on rootCauses.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                               where rootCauseTree.CausalFactorID == cfId.causalFactorId && rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == rootCauseCategoryID
                               select new
                               {
                                   RootCauses = rootCauses.Title,
                                   RootCauseTreeId = rootCauseTree.RootCauseTreeID
                               });

              if (rct.Count() > 0)
              {
                  sb.AppendFormat("<tr border='1' cellpadding='5' bgcolor='#AAAAAA' style='text-align:center;'>");
                  sb.AppendFormat("<td>{0}</td>", "Root Cause");
                  sb.AppendFormat("<td>{0}</td>", "Corrective Action");
                  sb.AppendFormat("<td>{0}</td>", "Task");
                  sb.AppendFormat("</tr>");

                  foreach (var rctID in rct)
                  {
                      var id = rctID.RootCauseTreeId;
                      iCounter = 0;

                      var getCAPValues = (from carct in _db.CorrectiveActionRCT.AsEnumerable()
                                          join caps in _db.CorrectiveActions.AsEnumerable()
                                              on carct.CorrectiveActionID equals caps.CorrectiveActionID
                                          where caps.EventID == eventId && carct.RootCauseTreeID == id
                                          select new
                                          {
                                              ActionPlans = ((caps.Identifier == "" || caps.Identifier == null) ? caps.Description : (caps.Description == "" || caps.Description == null ? caps.Identifier : caps.Identifier + " - " + caps.Description)),
                                              CorrectiveActionID = caps.CorrectiveActionID,
                                              RootCauseTreeID = carct.RootCauseTreeID
                                          }).Distinct();

                      //if (getCAPValues.Count() == 0)
                      //{
                      //    sb.AppendFormat("<tr border='1' style='color:#AAAAAA'> ");
                      //    sb.AppendFormat("<td colspan='3'>{0}</td>", rctID.RootCauses);
                      //}
                      //else
                      //{
                          sb.AppendFormat("<tr border='1' style='color:#AAAAAA'> ");
                          sb.AppendFormat("<td >{0}</td>", rctID.RootCauses);
                      //}

                      if (getCAPValues.Count() > 0)
                      {                         
                          foreach (var cap in getCAPValues)
                          {
                              var tasks = (from t in _db.Tasks.AsEnumerable()
                                           join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                           join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                           join caps in _db.CorrectiveActions on t.CorrectiveActionID equals cap.CorrectiveActionID
                                           where t.CorrectiveActionID == cap.CorrectiveActionID
                                           orderby t.DueDate
                                           select new
                                           {
                                               Task = tt.TaskTypeName + ", " + trp.DisplayName + " " + "&nbsp;&nbsp;<br/>"
                                           }).AsEnumerable().Distinct();

                              if (tasks.Count() == 0)
                              {
                                  sb.AppendFormat("<td colspan='2'>{0}</td>", cap.ActionPlans);
                              }
                              else
                                  sb.AppendFormat("<td>{0}</td>", cap.ActionPlans);

                              

                              if (tasks.Count() > 0)
                              {
                                  sb.AppendFormat("<td>");
                                  foreach (var tsk in tasks)
                                  {
                                      sb.AppendFormat(tsk.Task);
                                  }
                                  sb.AppendFormat("</td>");
                              }

                              iCounter++;
                              if(getCAPValues.Count() > 1 && iCounter < getCAPValues.Count())
                              {
                                  sb.AppendFormat("</tr><tr border='1' style='color:#aaaaaa'><td></td>");
                              }
                          }
                      }
                      else
                      {
                          sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
                          sb.AppendFormat("<td colspan='3'>{0}</td>", "No Action Plans.");
                          sb.AppendFormat("</tr>");
                      }
                      sb.AppendFormat("</tr>");
                  }
              }
              //else
              //{
              //    sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
              //    sb.AppendFormat("<td colspan='3'>{0}</td>", "No Root Causes.");
              //    sb.AppendFormat("</tr>");
              //}
              if (sb.ToString() == "")
              {
                  sb.AppendFormat("<tr border='1' style='color:#aaaaaa'> ");
                  sb.AppendFormat("<td colspan='3'>{0}</td>", "No Root Causes.");
                  sb.AppendFormat("</tr>");
                  return sb.ToString();
              }
                
            }
            sb.AppendFormat("</table></td>");
            return sb.ToString();


        }
        #endregion

        #region "Print - By Action Plan"
        public object GetActionPlansPrint(int eventId, int snapChartID, int rootCauseCategoryID)
        {
            //create string builder
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<td colspan='3'><table>");
            var getCAPValues = (from caps in _db.CorrectiveActions.AsEnumerable()
                                where caps.EventID == eventId
                                select new {
                                    ActionPlans = "Action Plan: "+((caps.Identifier == "" || caps.Identifier == null) ? caps.Description : (caps.Description == "" || caps.Description == null ? caps.Identifier : caps.Identifier + " - " + caps.Description)),
                                    CorrectiveActionID = caps.CorrectiveActionID,
                                  
                                });
            if (getCAPValues.Count() > 0)
            {
                foreach (var cap in getCAPValues)
                {
                    sb.AppendFormat("<tr border='1'> ");
                    sb.AppendFormat("<td colspan='3' bgColor='#000000' style='text-align:center;color:#ffffff'>{0}</td></tr>", cap.ActionPlans);
                    var tasks = (from t in _db.Tasks.AsEnumerable()
                                 join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID
                                 join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId
                                 join caps in _db.CorrectiveActions on t.CorrectiveActionID equals cap.CorrectiveActionID
                                 where t.CorrectiveActionID == cap.CorrectiveActionID
                                 orderby t.DueDate
                                 select new
                                 {
                                     taskName = tt.TaskTypeName + ", " + trp.DisplayName + " " + "&nbsp;&nbsp;<br/>"
                                 }).AsEnumerable().Distinct();

                    if (tasks.Count() > 0)
                    {
                        //for tasks
                        sb.AppendFormat("<tr border='1'> ");
                        sb.AppendFormat("<td colspan='3'>{0}</td>", "Tasks");
                        sb.AppendFormat("</tr>");
                        sb.AppendFormat("<tr border='1'> ");

                        sb.AppendFormat("<td colspan='3' style='color:#AAAAAA'>");

                        foreach (var tsk in tasks)
                        {
                            sb.AppendFormat(tsk.taskName.ToString());
                        }
                        sb.AppendFormat("</td></tr>");
                    }
                    else
                    {
                        sb.AppendFormat("<tr><td colspan='3' style='color:#AAAAAA'> No Tasks </td></tr>");
                    }
                    var rootCauses = (from carct in _db.CorrectiveActionRCT.AsEnumerable()
                                      join rct in _db.RootCauseTree.AsEnumerable() on carct.RootCauseTreeID equals rct.RootCauseTreeID
                                      join rc in _db.RootCauses.AsEnumerable() on rct.RootCauseID equals rc.RootCauseID
                                      join rootCauseCategory in _db.RootCauseCategories.AsEnumerable() on rc.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                                      join cf in _db.CausalFactors on rct.CausalFactorID equals cf.CausalFactorID
                                      where rct.IsSelected == true && carct.CorrectiveActionID == cap.CorrectiveActionID
                                      && rc.RootCauseCategoryID == rootCauseCategoryID //&& carct.CorrectiveActionID != null
                                      select new
                                      {
                                          RootCauses = ((rc.Title == "" || rc.Title == null) ? "No Root Causes" : rc.Title),
                                          CausalFactorID = rct.CausalFactorID,
                                          RootCauseID = rct.RootCauseID,
                                          Name = cf.Name
                                      }).Distinct();


                    if (rootCauses.Count() > 0)
                    {
                        //for RootCauses and CF's
                        sb.AppendFormat("<tr border='1' style='text-align:center;'> ");
                        sb.AppendFormat("<td colspan='2' >{0}</td>", "Root Causes Fixed");
                        sb.AppendFormat("<td >{0}</td>", "Causal Factor");
                        sb.AppendFormat("</tr>");

                        

                        foreach (var rctID in rootCauses)
                        {
                            sb.AppendFormat("<tr border='1'> ");
                            sb.AppendFormat("<td colspan='2' style='color:#AAAAAA'>{0}</td>", rctID.RootCauses);
                            sb.AppendFormat("<td style='color:#AAAAAA'>{0}</td>", rctID.Name);
                            sb.AppendFormat("</tr>");
                        }

                        //sb.AppendFormat("</tr>");
                    }
                    else
                    {
                        sb.AppendFormat("<tr border='1'> ");
                        sb.AppendFormat("<td colspan='3' style='color:#AAAAAA'>{0}</td>", "No Root Causes and Causal Factors");
                        sb.AppendFormat("</tr>");
                    }

                    //sb.AppendFormat("</tr>");
                    //sb.AppendFormat("</tr>");

                }
            }
            else
            {
                sb.AppendFormat("<tr border='1'> <td colspan='3' style='color:#AAAAAA'>{0}</td></tr>", "No Action Plans.");
            }

            if (sb.ToString() == "")
            {
                sb.AppendFormat("<tr border='1'><td colspan='3'>{0}</td></tr>", "No Action Plans.");
            }
            sb.AppendFormat("</table></td>");
            return sb.ToString();

        }
        #endregion

        #endregion

        #region "Generic Method to fetch Column values"
        /// <summary>
        /// GetColumnValue()
        /// </summary>
        /// <param name="elementTypeID"></param>
        /// <param name="red"></param>
        /// <param name="recd"></param>
        /// <returns></returns>
        public object GetColumnValue(int elementTypeID, tap.dat.ReportElementDataSource red, tap.dat.ReportElementCustomSource recd, int eventId,int elementID)
        {

            string value = string.Empty;

            if (elementTypeID == Convert.ToInt32(tap.dom.hlp.Enumeration.ReportFields.DataField))//data field
            {
                if (red != null)
                {
                    DataTable dataTable;

                    //call DataUtil class method to get values from command query (tap.dat)
                    if (red.DataSourceColumnName == EVENT_DATETIME)//remove time display only date
                    {
                       // red.DataSourceColumnName = EVENTDATE +" + EventTime as EventDate";
                        red.DataSourceColumnName = EVENTDATE + " + ISNULL(EventTime,'00:00:00.000') as EventDate";                        

                        dataTable = _sqlquery.ExecuteQuery(string.Format("SELECT {0} FROM {1} WHERE {2} = {3} ", red.DataSourceColumnName, red.DataSourceTableName, "EventID", eventId));//event id-pass from Jquery method once toolbox funtionality completes

                        red.DataSourceColumnName = EVENTDATE;

                        TimeSpan time = ((System.DateTime)(dataTable.Rows[0][red.DataSourceColumnName])).TimeOfDay;
                        string nullTime = "00:00:00";
                        TimeSpan timeNull = TimeSpan.Parse(nullTime);

                        if (time == timeNull)
                        {
                            var dt = ((System.DateTime)(dataTable.Rows[0][red.DataSourceColumnName])).Date.Month + "/" + ((System.DateTime)(dataTable.Rows[0][red.DataSourceColumnName])).Date.Day + "/" + ((System.DateTime)(dataTable.Rows[0][red.DataSourceColumnName])).Date.Year;
                            return Convert.ToString(dt);
                        }   
                      
                    }
                    else
                        dataTable = _sqlquery.ExecuteQuery(string.Format("SELECT {0} FROM {1} WHERE {2} = {3} ", red.DataSourceColumnName, red.DataSourceTableName, "EventID", eventId));//event id-pass from Jquery method once toolbox funtionality completes

                    //if column name is Location -pass listValueId to ListValues table and get the value
                    if ((red.DataSourceColumnName == LOCATION_COLUMN) || (red.DataSourceColumnName == VALUEID_COLUMN))
                    {
                        return GetFullPathValues(dataTable, red.DataSourceColumnName);
                    }                  

                    return Convert.ToString(dataTable.Rows[0][red.DataSourceColumnName]);
                }
            }
            else if (elementTypeID == Convert.ToInt32(tap.dom.hlp.Enumeration.ReportFields.CustomField))//custom field
            {
                if (recd != null)
                {
                    var getControlType = from f in _db.Fields
                                         where f.FieldID == recd.CustomFieldID
                                         select f;


                    //get values from FieldValues table
                    var customRecords = (from fd in _db.FieldValues
                                         join cst in _db.ReportElementCustomSource on fd.FieldID equals cst.CustomFieldID
                                         where cst.CustomFieldID == recd.CustomFieldID && fd.EventID == eventId
                                         select fd);

                    //if no record found it was throwing null reference exception
                    if (customRecords.ToList().Count > 0)
                    {
                        value = customRecords.FirstOrDefault().FieldValue;
                        string controlType = getControlType.FirstOrDefault().ControlType;

                        // if control type is Tree view then pass value id to FieldListValues table to get id's
                        if (controlType == SELECTONEFROMLIST_DATATYPE || controlType == SELECTMULTIPLEFROMLIST_DATATYPE)
                        {
                            //values are not storing in FieldListValues table so am directly taking id's FieldValues table
                            //create data table and pass to common method to get full path-check with Matt
                            DataTable dt = new DataTable();
                            string[] header = value.Split(',');
                            dt.Columns.Add(VALUEID_COLUMN);

                            foreach (string head in header)
                            {
                                dt.Rows.Add(head);
                            }

                            //pass listid's and valueid column name to get full path values
                            return GetFullPathValues(dt, VALUEID_COLUMN);
                        }
                    }
                }
            }
            else if (elementTypeID == Convert.ToInt32(tap.dom.hlp.Enumeration.ReportFields.StandAlone))
            { 
               int elementId=elementID-1;
               var getSingleTypeName = (from re in _db.ReportElement
                                        where re.ElementID == elementId
                                        select new { 
                                            SingleElementTName=re.ElementText
                                        }).FirstOrDefault();

            
                   return GetSingleTapRooTValues(getSingleTypeName.SingleElementTName,eventId);
            
            }

            return value;
        }

        #endregion

        #region " get Single Item TapRooT values"
        public object GetSingleTapRooTValues(string SingleTypeName,int eventId)
        {

            //will remove hard code
            if (SingleTypeName == "Causal Factors")
            {
                List<string> cfValues = new List<string>();
                var getCausalFactorValues = (from cf in _db.CausalFactors
                                             join s in _db.SnapCharts
                                             on cf.SnapChartID equals s.SnapChartID
                                             where s.EventID == eventId
                                             select new {
                                                 CausalFactorName =  cf.Name + "<br/>"
                                             });
                if (getCausalFactorValues.Count() != 0)
                {
                    foreach (var x in getCausalFactorValues)
                    {
                        cfValues.Add(x.CausalFactorName);
                    }
                    return cfValues;
                }
                else
                    return "No Causal Factor";
               
            }
            else if (SingleTypeName == "Root Causes")
            {
                List<string> rctValues = new List<string>();
                 var autumnSeasonID = (from season in _db.Seasons where season.SeasonName == AUTUMN_SEASON select season).FirstOrDefault().SeasonID;

                var rcType = (from rootCauseType in _db.RootCauseCategories where rootCauseType.Category == TAPROOT_RCT select rootCauseType).FirstOrDefault();

                int rootCauseCategoryID = Convert.ToInt32(rcType.RootCauseCategoryID);

                var getCausalFactorId = (from cf in _db.CausalFactors
                                        join s in _db.SnapCharts
                                        on cf.SnapChartID equals s.SnapChartID
                                        where s.EventID == eventId
                                        select new
                                                  {
                                                      CausalFactorID = cf.CausalFactorID
                                                  });


                if (getCausalFactorId.Count() != 0)
                {
                    foreach (var y in getCausalFactorId)
                    {
                        var getRootCauseValues = (from rootCauseTree in _db.RootCauseTree
                                                  join rootCauses in _db.RootCauses on rootCauseTree.RootCauseID equals rootCauses.RootCauseID
                                                  where rootCauseTree.IsSelected == true && rootCauseTree.CausalFactorID == y.CausalFactorID
                                                  && rootCauses.RootCauseCategoryID == rootCauseCategoryID
                                                  orderby rootCauseTree.RootCauseTreeID
                                                  select new
                                                  {
                                                      RootCauses = rootCauses.Title
                                                  });

                        if (getRootCauseValues.Count() != 0)
                        {
                            foreach (var x in getRootCauseValues)
                            {
                                if (x.RootCauses != "")
                                    rctValues.Add( x.RootCauses.ToString() + "<br/>");
                            }                            
                        }                        

                    }

                    if(rctValues.Count == 0)
                         return "No Root Cause";

                    return rctValues;
                }
            }
            else if (SingleTypeName == "Action Plans")
            {
                List<string> capValues = new List<string>();
                var getActionPlanValues = (from caps in _db.CorrectiveActions.AsEnumerable()
                                           where caps.EventID == eventId
                                           select new
                                           {
                                               Identifier = caps.Identifier,
                                               Description = caps.Description

                                           });
                string cssMargin = "clear:both;margin-left:90px;";
                if (getActionPlanValues.Count() != 0)
                {
                    bool flag = false;
                    foreach (var x in getActionPlanValues)
                    {
                        //capValues.Add(x.ActionPlans != "" ? x.ActionPlans + "<br/>" : x.Identifier + "<br/>");

                        if (x.Description != null)
                        {
                            if (!flag)
                            {
                                capValues.Add("<span style='clear:both' >" + x.Identifier + (Convert.ToString(x.Description).Length > 0 ? " - " + x.Description : "") + "</span><br/>");
                                flag = true;
                            }
                            else
                                capValues.Add("<span style='" + cssMargin + "'>" + x.Identifier + (Convert.ToString(x.Description).Length > 0 ? " - " + x.Description : "") + "</span><br/>");
                        }
                        else
                        {
                            if (!flag)
                            {
                                capValues.Add("<span style='clear:both' >" + x.Identifier + "</span><br/>");
                                flag = true;
                            }
                            else
                                capValues.Add("<span style='" + cssMargin + "'>" + x.Identifier + "</span><br/>");
                        }
                    }
                    return capValues;
                }
                else
                    return "No Action Plans";
            }
            else if (SingleTypeName == "Tasks")
            {
                List<string> taskValues = new List<string>();             

                 var getCAPValues = (from caps in _db.CorrectiveActions.AsEnumerable()
                                where caps.EventID == eventId
                                select new {
                                    ActionPlans =caps.Identifier,// "Action Plan: "+((caps.Identifier == "" || caps.Identifier == null) ? caps.Description : (caps.Description == "" || caps.Description == null ? caps.Identifier : caps.Identifier + " - " + caps.Description)),
                                    Description=caps.Description,
                                    CorrectiveActionID = caps.CorrectiveActionID,
                                  
                                });

                 var getTaskValues = (from carct in getCAPValues
                                      select new
                                      {
                                          CorrectiveActionName = carct.ActionPlans,
                                          Tasks = (from t in _db.Tasks.AsEnumerable()

                                                   join tt in _db.TaskType on t.TaskTypeID equals tt.TaskTypeID

                                                   join trp in _db.TasksResponsiblePersons on t.TaskID equals trp.TaskId

                                                   join caps in _db.CorrectiveActions on t.CorrectiveActionID equals caps.CorrectiveActionID

                                                   where t.CorrectiveActionID == carct.CorrectiveActionID

                                                   orderby t.DueDate
                                                   select new
                                                   {
                                                       TaskType="<b>Task Type: </b>"+tt.TaskTypeName + "<br/>",
                                                       ResponsiblePersonName = "<b>Responsible Person:</b>" + trp.DisplayName + "<br/>",
                                                       DueDate = "<b>Due Date: </b>" + Convert.ToDateTime(t.DueDate).ToString("dd/MM/yyyy") + "<br/>",
                                                       Status = "<b>Status: </b>" + (t.IsCompleted.Value ? "Completed" : "pending") + "<br/>",
                                                       Desciption = "<b>Description: </b>" + t.Description + "<br/>",
                                                       

                                                   }).AsEnumerable()    //.Distinct(),
                                      });



         
                if (getTaskValues.Count() != 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("<table style='border-top:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;' width='98%'>");
                    sb.AppendFormat("<tr border='1' width='100%' style='text-align:center;font-weight:bold;'>");
                    sb.AppendFormat("<td width='100%' style='border-bottom:1px solid #000000;padding-left:20px;' colspan='4'>");                    
                    sb.AppendFormat("Tasks");
                    sb.AppendFormat("</td>");
                    sb.AppendFormat("</tr><br/>");

                   
                    foreach (var t in getTaskValues)
                    {
                        sb.AppendFormat("<tr border='1' width='100%' style='text-align:left;'>");
                       // sb.AppendFormat("<td width='100%' style='border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;' colspan='4'>");
                        sb.AppendFormat("<td style='clear:both;border-bottom:1px solid #000000;padding-left:20px;' colspan='4' width='100%' >");
                        sb.AppendFormat("<b>Corrective Action: </b>" + t.CorrectiveActionName);
                        sb.AppendFormat("</td>");
                        sb.AppendFormat("</tr>");

                        foreach (var tr in t.Tasks.ToList())
                        {
                            sb.AppendFormat("<tr border='1'  width='100%' style='text-align:left;'>");
                            sb.AppendFormat("<td style='clear:both;border-bottom:1px solid #000000;padding-left:20px;' colspan='4' width='100%'>");
                            sb.AppendFormat(tr.TaskType);                              
                            sb.AppendFormat(tr.ResponsiblePersonName);
                            sb.AppendFormat(tr.DueDate); 
                            sb.AppendFormat(tr.Status);
                            sb.AppendFormat(tr.Desciption);
                            sb.AppendFormat("<br/>");
                            sb.AppendFormat("</td>");
                           
                            sb.AppendFormat("</tr>");
                        }
                      
                    }
                    sb.AppendFormat("</table>");

                    //sb.AppendFormat("<div style='border:1px solid #000000;text-align:center;font-weight:bold;width:51%'>");
                    //sb.AppendFormat("Tasks");
                    //sb.AppendFormat("</div>");

                    //foreach (var t in getTaskValues)
                    //{
                    //    sb.AppendFormat("<div style='border-bottom:1px solid #000000;border-left:1px solid #000000;border-right:1px solid #000000;text-align:left;padding-left:10px;width:50%'>");
                    //    sb.AppendFormat("<b>Corrective Action: </b>"+t.CorrectiveActionName +"<br/>");

                    //   foreach (var tr in t.Tasks.ToList())
                    //   {
                    //       sb.AppendFormat("<div style='text-align:left;'>");
                    //       sb.AppendFormat(tr.TaskType);
                    //       sb.AppendFormat(tr.ResponsiblePersonName);
                    //       sb.AppendFormat(tr.DueDate);
                    //       sb.AppendFormat(tr.Status);
                    //       sb.AppendFormat(tr.Desciption);
                    //       sb.AppendFormat("</div><br/>");
                    //   }
                     
                    //   sb.AppendFormat("</div>");
                    //}
                  
                    return sb.ToString();
                }
                else
                    return "No Tasks";
            } 

            return null;
        }
        #endregion

        #region "Get FullPath and Values"
        /// <summary>
        /// Common method used for both Custom and Event Page
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="columName"></param>
        /// <returns></returns>
        public List<string> GetFullPathValues(DataTable dataTable, string columName)
        {
            tap.dom.adm.ListValues listValues = new tap.dom.adm.ListValues();

            List<string> fullPathLists = new List<string>();

            List<int> listIds = new List<int>();

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                listIds.Add(Convert.ToInt32(dataTable.Rows[i][columName]));
            }

            //pass list value id's to get Full Path
            var fullPath = from lv in _db.ListValues
                           where listIds.Contains(lv.ListValueID)
                           orderby lv.FullPath
                           select lv.FullPath;

            //take full path of Location values and pass to FormattedFullpath method to get(formated) > values
            foreach (var x in fullPath)
            {
                fullPathLists.Add(listValues.FormattedFullpath(x));
            }

            return fullPathLists;
        }

        #endregion

        #region "Get all Report Names depends on Company Id"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyID"></param>
        /// <param name="userID"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public object GetReportTitleNames(string companyID, string userID, string eventID)
        {
            try
            {

                //Decrypt the company Id and UserId
                int companyId = Convert.ToInt32(companyID);
                int userId = Convert.ToInt32(userID);
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                //get all Event Types Report
                var eventReportNames = (from r in _db.Report
                                        join e in _db.Events on r.EventID equals e.EventID
                                        join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                        where !(from u in _db.UserReports
                                                select u.ReportID).Contains(r.ReportID)

                                       && e.EventID == eventId && rt.ReportType != SYSTEM_REPORT && r.ReportName != ""

                                        select new
                                        {
                                            EventName = e.Name,
                                            ReportName = r.ReportName,
                                            ReportId = r.ReportID,
                                            ReportType = rt.ReportType
                                        });

                return eventReportNames;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region "Get Company Details-Logo,Name"
        public static tap.dat.Companies GetCompanyDetails(int? companyID = null)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            //if (companyID == null)
            //    companyID = SessionService.CompanyId;

            tap.dat.Companies companyDetails = _db.Companies.FirstOrDefault(a => a.CompanyID == companyID);

            return companyDetails;
        }
        #endregion

        #region "GetReportDetails"
        public static tap.dat.Report GetReportDetails(int reportId)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            tap.dat.Report reportDetails = _db.Report.FirstOrDefault(a => a.ReportID == reportId);
            return reportDetails;

        }
        #endregion

        #region "Delete Image Data -on mousehover click cross/delete icon"
        public bool DeleteImageData(int elementId, int reportId)
        {
            try
            {
                int labelElement = elementId;
                labelElement = labelElement - 1;

                var dataElements = _db.ReportElementCustomSource.FirstOrDefault(x => x.ReportElementID == elementId);
                var customElements = _db.ReportElementDataSource.FirstOrDefault(x => x.ReportElementID == elementId);

                var imageDataElements = _db.ReportElement.FirstOrDefault(x => x.ElementID == elementId);


                if (dataElements != null)
                {
                    _db.DeleteObject(dataElements);
                    _db.SaveChanges();
                }

                if (customElements != null)
                {
                    _db.DeleteObject(customElements);
                    _db.SaveChanges();
                }

                if (imageDataElements != null)
                {
                    _db.DeleteObject(imageDataElements);
                    _db.SaveChanges();
                }

                //remove this lines of code for optimisation
                var imageElements = _db.ReportElement.First(x => x.ElementID == labelElement);

                _db.DeleteObject(imageElements);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }
        #endregion

        #region "Delete Report related data when user close the Preview browser"
        public bool DeletePreviewReportData(string eventID, int reportId)
        {
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                //get the ReportElement Data
                var reportElementData = from re in _db.ReportElement
                                        where re.ReportID == reportId
                                        select re;

                var getUserReportData = from us in _db.UserReports
                                        where us.ReportID == reportId
                                        select us;

                //get the Custom Source Data(fields and fields Values) Data
                var getCustomSourceData = from cs in _db.ReportElementCustomSource
                                          join re in reportElementData
                                          on cs.ReportElementID equals re.ElementID
                                          select cs;

                //get the Data source (events values) Data
                var getDataSourceData = from ds in _db.ReportElementDataSource
                                        join re in reportElementData
                                        on ds.ReportElementID equals re.ElementID
                                        select ds;
                var getReportData = _db.Report.First(x => x.ReportID == reportId);


                //delete ReportElementCustomSource table data
                foreach (tap.dat.ReportElementCustomSource recs in getCustomSourceData)
                {
                    _db.DeleteObject(recs);
                }
                _db.SaveChanges();

                //delete ReportElementDataSource table data
                foreach (tap.dat.ReportElementDataSource reds in getDataSourceData)
                {
                    _db.DeleteObject(reds);
                }
                _db.SaveChanges();

                if (getUserReportData!=null)
                {
                    //delete UserReports table data
                    foreach (tap.dat.UserReports ur in getUserReportData)
                    {
                        _db.DeleteObject(ur);
                    }
                    _db.SaveChanges();
                }

                //delete ReportElement table data
                foreach (tap.dat.ReportElement re in reportElementData)
                {
                    _db.DeleteObject(re);
                }
                _db.SaveChanges();

                //delete Report table Data
                _db.DeleteObject(getReportData);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }
        #endregion

        #region "Fetch Report Data Types -Event's, User's and System and auto search functionality"
        public string GetAllReportsData(string page, string rows, string sortIndex, string sortOrder, string companyID, string userID, string userSearch)
        {


            //convert to int user id and pass
            int userId = Convert.ToInt32(userID);

            //convert to int company id and pass
            int companyId = Convert.ToInt32(companyID);

            var getAllReportsData = (from r in _db.Report
                                     join u in _db.Users on r.CreatedBy equals u.UserID
                                     join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                     where r.CompanyID == companyId && r.ReportName != "" && rt.ReportType!=EVENT_REPORT

                                     let EventType = rt.ReportType //Types of Report
                                     select new
                                     {
                                         ReportId = r.ReportID,
                                         ReportName = r.ReportName,
                                         CreatedBy = u.UserName,
                                         EventType=EventType,
                                         EventId = r.EventID,
                                         CreatedDate = r.CreatedDate
                                     }).OrderByDescending(e => e.CreatedDate);

            //auto search query
            if (userSearch != "false")
            {
                getAllReportsData = (from r in _db.Report
                                     join u in _db.Users on r.CreatedBy equals u.UserID
                                     join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                     where r.CompanyID == companyId && r.ReportName.Contains(userSearch) && rt.ReportType!=EVENT_REPORT
                                     orderby r.CreatedDate descending
                                     let EventType = rt.ReportType //Types of Report
                                     select new
                                     {
                                         ReportId = r.ReportID,
                                         ReportName = r.ReportName,
                                         CreatedBy = u.UserName,
                                         EventType = EventType,
                                         EventId = r.EventID,
                                         CreatedDate = r.CreatedDate
                                     }).OrderByDescending(e => e.CreatedDate);
            }



            #region Sorting the Reports

            //Sorting Filter
            switch (sortIndex)
            {
                case "ReportName":
                    getAllReportsData = ((sortOrder.Equals("desc")) ? getAllReportsData.OrderByDescending(e => e.ReportName) : getAllReportsData.OrderBy(e => e.ReportName));
                    break;

                case "ReportType":
                    getAllReportsData = ((sortOrder.Equals("desc")) ? getAllReportsData.OrderByDescending(e => e.EventType).ThenByDescending(e => e.EventType) : getAllReportsData.OrderBy(e => e.EventType).ThenByDescending(e => e.EventType));
                    break;

                case "CreatedBy":
                    getAllReportsData = ((sortOrder.Equals("desc")) ? getAllReportsData.OrderByDescending(e => e.CreatedBy).ThenByDescending(e => e.CreatedBy) : getAllReportsData.OrderBy(e => e.CreatedBy).ThenByDescending(e => e.CreatedBy));
                    break;


            }

            #endregion

            var data = getAllReportsData.ToList();

            //PAGINATION
            int totalRecords = 0;

            var currentPazeSize = rows ?? "15";
            int pageSize = int.Parse(currentPazeSize);

            if (data.Count != 0)
                totalRecords = data.Count();

            int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

            var thisPage = page ?? "1";
            int currentPage = int.Parse(thisPage);

            if (currentPage > 1 && data.Count != 0)
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

            var jsonData = new tap.dom.hlp.jqGridJson()
            {
                total = totalPages,
                page = currentPage,
                records = totalRecords,
                rows = (
                    from s in data
                    select new tap.dom.hlp.jqGridRowJson
                    {
                        id = s.ReportId.ToString(),
                        cell = new string[] {  
                            _cryptography.Encrypt(s.EventId.ToString()),
                            s.ReportName, 
                            s.CreatedBy.ToString(),
                           s.EventType                           
                                    
                        }
                    }).ToArray()
            };

            return _efSerializer.EFSerialize(jsonData);

        }
        #endregion

        #region "Fetch all label names for Edit Report"
        public string GetColumnLabelNames(string userID, string companyID, int reportId)
        {
            //Decrypt the user Id and pass
            int userId = Convert.ToInt32(userID);

            //Decrypt the company Id and pass
            int companyId = Convert.ToInt32(companyID);

            var reportAllNames = (from rpt in _db.ReportElement.AsEnumerable()
                                  join r in _db.Report on rpt.ReportID equals r.ReportID
                                  join usr in _db.Users on r.CreatedBy equals usr.UserID
                                  join rt in _db.ReportTypes on r.ReportTypeId equals rt.ReportTypeId
                                  where r.ReportID == reportId && r.CompanyID == companyId
                             
                                  orderby rpt.SortOrder

                                  select new
                                  {
                                      ReportId = r.ReportID,
                                      ElementId = rpt.ElementID,
                                      ReportName = r.ReportName,
                                      CreatedUser = usr.UserName,
                                      ElementStyle = rpt.ElementStyle,
                                      ElementText = rpt.ElementText,
                                      Id = rpt.ElementID,
                                      SortOrder = rpt.SortOrder,
                                      TopPosition = rpt.TopPosition,
                                      LeftPosition = rpt.LeftPosition,
                                      ElementType = rpt.ElementTypeID,
                                      ReportTitle = r.ReportTitle,
                                      EventId = r.EventID,
                                      Description = r.Description,
                                      isTemporaryReport = r.isTemporaryReport,
                                      ReportTitleStyle = r.ReportTitleStyle,
                                      isCompanyLogoUsed = r.IsCompanyLogoUsed,
                                      isCompanyNameUsed = r.isCompanyNameUsed,
                                      isCreatedDateUsed = r.isCreatedDateUsed,
                                      isReportTitleUsed = r.isReportTitleUsed,
                                      IntegrateType=rpt.IntegrateType,
                                      ReportType=rt.ReportType
                                  }).AsEnumerable();

            var filterLabelNames = reportAllNames.ToList().Where(a => a.ElementText != null).Select(a => a);

            var labelNamesWithElementTypes = (from lbl in filterLabelNames
                                              select new
                                              {
                                                  ReportId = lbl.ReportId,
                                                  ElementId = lbl.ElementId,
                                                  ReportName = lbl.ReportName,
                                                  CreatedUser = lbl.CreatedUser,
                                                  ElementStyle = lbl.ElementStyle,
                                                  ElementText = lbl.ElementText,
                                                  Id = lbl.ElementId,
                                                  SortOrder = lbl.SortOrder,
                                                  TopPosition = lbl.TopPosition,
                                                  LeftPosition = lbl.LeftPosition,
                                                  ElementType = (from e in reportAllNames
                                                                 where e.ElementText == null
                                                                 select e.ElementType),
                                                  ReportTitle = lbl.ReportTitle,
                                                  EventId = _cryptography.Encrypt(lbl.EventId.ToString()),
                                                  Description = lbl.Description,
                                                  isTemporaryReport = lbl.isTemporaryReport,
                                                  ReportTitleStyle = lbl.ReportTitleStyle,
                                                  isCompanyLogoUsed = lbl.isCompanyLogoUsed,
                                                  isCompanyNameUsed = lbl.isCompanyNameUsed,
                                                  isCreatedDateUsed = lbl.isCreatedDateUsed,
                                                  isReportTitleUsed = lbl.isReportTitleUsed,
                                                  IntegrateType = lbl.IntegrateType,
                                                  ReportType = lbl.ReportType
                                              });

            return _efSerializer.EFSerialize(labelNamesWithElementTypes);
        }
        #endregion

        #region "Snapchart"
        public bool GetWinterSnapChartStatus(string eventIdStr)
        {
            try
            {
                tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventIdStr));
                if (eventId.Equals(0))
                    return false;

                int snapChartId = _db.SnapCharts.Where(x => x.EventID == eventId).FirstOrDefault().SnapChartID;
                int winter = Convert.ToInt32(tap.dom.hlp.Enumeration.Season.Winter);
                List<tap.dat.SnapCharts> snapchart = _db.SnapCharts.Where(x => x.EventID == eventId && x.SeasonID == winter
                                                            && x.IsActive == true).ToList();

                return (snapchart.Count > 0);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return false;
        }
        #endregion

        #region "Create Event Report"
        public string CreateEventReport(string[] arrTemplateReportIds, int companyId, int userId, string eventID)
        {
            tap.dat.ReportElement reportElement = null;
            tap.dat.Report reportData = null;
            int eventId = 0;

            if (eventID != "0")
                eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            if (arrTemplateReportIds != null)
            {
                for (int i = 0; i < arrTemplateReportIds.Length; i++)
                {

                    int reportId = Convert.ToInt32(arrTemplateReportIds[i]);
                    reportData = _db.Report.FirstOrDefault(a => a.ReportID == reportId);
                    var reportType = _db.ReportTypes.FirstOrDefault(a => a.ReportType == EVENT_REPORT);

                    var newEventReport = (from r in _db.Report
                                          where r.ReportID == reportId
                                          select r).FirstOrDefault();


                    reportData = new tap.dat.Report();


                    reportData.ReportName = newEventReport.ReportName;
                    reportData.Description = newEventReport.Description;
                    reportData.CompanyID = companyId;
                    reportData.CreatedBy = userId;
                    reportData.CreatedDate = DateTime.UtcNow; //GMT;
                    reportData.ReportTitle = newEventReport.ReportTitle;
                    reportData.IsCompanyLogoUsed = newEventReport.IsCompanyLogoUsed;
                    reportData.isCompanyNameUsed = newEventReport.isCompanyNameUsed;
                    reportData.isReportTitleUsed = newEventReport.isReportTitleUsed;
                    reportData.isCreatedDateUsed = newEventReport.isCreatedDateUsed;
                    reportData.ReportTypeId = reportType.ReportTypeId;
                    reportData.isTemplate=false;
                  //  reportData.ReportType = EVENT_REPORT;

                    reportData.EventID = eventId;
                    reportData.isTemporaryReport = newEventReport.isTemporaryReport;
                    reportData.ReportTitleStyle = newEventReport.ReportTitleStyle;

                    _db.AddToReport(reportData);
                    _db.SaveChanges();

                    //save Report Element table data
                    SaveReportElementEventReport(reportData.ReportID, reportId);


                }
            }
            return string.Empty;

        }


        public void SaveReportElementEventReport(int reportId,int oldReportId)
        {
            //get the ReportElement Data and re insert to create Event Report.(from user/system templates)
            var newEventReportElement= (from re in _db.ReportElement
                                        where re.ReportID == oldReportId
                                         select re).ToList();
                   
            foreach (var re in newEventReportElement)
            {                
                    tap.dat.ReportElement reportElement = new tap.dat.ReportElement();
                    reportElement.ReportID = reportId;
                    reportElement.ElementTypeID = re.ElementTypeID;
                    reportElement.ElementText = re.ElementText;
                    reportElement.ElementStyle = re.ElementStyle;
                    reportElement.SortOrder = re.SortOrder;
                    reportElement.TopPosition = string.Empty;
                    reportElement.LeftPosition = string.Empty;
                    reportElement.ReportImageData = re.ReportImageData;
                    reportElement.IntegrateType = re.IntegrateType;

                    _db.AddToReportElement(reportElement);
                    _db.SaveChanges();

                    //if element Text is null then pass the element id and save it in ReportElementCustomSource and ReportElementDataSource tables
                    if (re.ElementText == null)
                    {
                        int oldElementId = re.ElementID;
                        if (re.ElementTypeID == 4)//data fields- event table
                        {
                            var getEventDetails = (from reds in _db.ReportElementDataSource
                                                   where reds.ReportElementID == oldElementId
                                                   select reds).FirstOrDefault();

                            if (getEventDetails != null)
                            {
                                tap.dat.ReportElementDataSource reportElementDS = new tap.dat.ReportElementDataSource();
                                reportElementDS.ReportElementID = reportElement.ElementID;
                                reportElementDS.DataSourceColumnName = getEventDetails.DataSourceColumnName;
                                reportElementDS.DataSourceTableName = getEventDetails.DataSourceTableName;
                                _db.AddToReportElementDataSource(reportElementDS);
                                _db.SaveChanges();
                            }

                        }
                        else if (re.ElementTypeID == 5)//custom fields- fields table
                        {
                            var getCustomDetails = (from recs in _db.ReportElementCustomSource
                                                    where recs.ReportElementID == recs.ReportElementID
                                                    select recs).FirstOrDefault();
                            if (getCustomDetails != null)
                            {
                                //pass Field id and save in ReportElementCustomSource
                                tap.dat.ReportElementCustomSource reportElementCS = new tap.dat.ReportElementCustomSource();
                                reportElementCS.ReportElementID = reportElement.ElementID;
                                reportElementCS.CustomFieldID = getCustomDetails.CustomFieldID;
                                _db.AddToReportElementCustomSource(reportElementCS);
                                _db.SaveChanges();
                            }
                        }
                    }
            }

            _db.SaveChanges();          
        }
        #endregion

        #region "Resize the Image"
        public void ResizeImage(string orginalPath, ImageSizes size, string fileName, string newFilePath)
        { 
            //set standard size
            SetStandardSizes(size);

            // Image img = Image.FromFile(fi.FullName);
            System.Drawing.Image img = System.Drawing.Image.FromFile(orginalPath);

            SetParams(img);

            // create a new empty bitmpat with the specified size
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(width, height);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage((System.Drawing.Image)bmp);

            //copy the original image on the canvas, and thus on the new bitmap, with the new size
            g.DrawImage(img, 0, 0, width, height);

            //close the original image
            img.Dispose();

            //save the new image with the proper format
            string saveAs = string.Format("{0}", newFilePath);

            bmp.Save(saveAs, System.Drawing.Imaging.ImageFormat.Jpeg);
            bmp.Dispose();

        }

        public void SetStandardSizes(ImageSizes size)
        {
            switch (size)
            {
                case ImageSizes.size_100x100:
                    standardHeight = 100;
                    standardWidth = 100;
                    break;
                case ImageSizes.size_300x300:
                    standardHeight = 300;
                    standardWidth = 300;
                    break;
                case ImageSizes.size_410x410:
                    standardHeight = 410;
                    standardWidth = 410;
                    break;
                case ImageSizes.size_510x510:
                    standardHeight = 600;
                    standardWidth = 500;
                    break;
               

            }
        }
        #region "Enum Image Sizes"
        public enum ImageSizes
        {
            size_100x100,
            size_300x300,
            size_410x410,
            size_510x510
        }
        #endregion


        /// <summary>
        /// Sets the height and width to properly scale and fit inside a maximum images size of 150x113.
        /// </summary>
        /// <param name="img">The image to scale</param>
        private void SetParams(System.Drawing.Image img)
        {
            width = img.Width;
            height = img.Height;

            bool isLandscape = img.Width > img.Height;
            double ratio;
            //double ratio = double.Parse(img.Width.ToString()) / double.Parse(img.Height.ToString());

            if (isLandscape && img.Width > standardWidth)
            {
                ratio = double.Parse(img.Width.ToString()) / double.Parse(standardWidth.ToString());

                width = standardWidth;
                height = int.Parse(Math.Round(img.Height / ratio, 0).ToString());

                if (height > standardHeight)
                {
                    double reducer = double.Parse(standardHeight.ToString()) / double.Parse(height.ToString());
                    height = standardHeight;
                    width = int.Parse(Math.Round(double.Parse(width.ToString()) * reducer, 0).ToString());
                }
            }
            else if (img.Height > standardHeight)
            {
                ratio = double.Parse(img.Height.ToString()) / double.Parse(standardHeight.ToString());
                height = standardHeight;
                width = int.Parse(Math.Round(img.Width / ratio, 0).ToString());

                if (width > standardWidth)
                {
                    double reducer = double.Parse(standardWidth.ToString()) / double.Parse(width.ToString());
                    width = standardWidth;
                    height = int.Parse(Math.Round(double.Parse(height.ToString()) * reducer, 0).ToString());
                }
            }
        }
        #endregion

        public string GetEventID(int reportId)
        {
            tap.dat.EFEntity _db = new tap.dat.EFEntity();
            var reportDetails = Convert.ToString(_db.Report.FirstOrDefault(a => a.ReportID == reportId).EventID);
            return reportDetails;
        }
    }
}
