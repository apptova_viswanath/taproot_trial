﻿
//--------------------------------------------------------------------------------------------------------
// File Name 	: GenericSave.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Auto save all the fields in a generic way.
//                 Independent of any class or property

//--------------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dat;
using System.Data.Objects;
using System.Data.Metadata.Edm;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using tap.dom.adm;
using tap.dom.hlp;
using tap.dom.usr;

#endregion

namespace tap.dom.gen
{
    public class GenericSave
    {
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        #region "Variables"
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dat.Events _currentEvent = null;
        tap.dom.adm.CommonOperation _commonOperation = new adm.CommonOperation();
        #endregion

        #region "Auto Save Input Fields"

        /// <summary>
        /// GenericSaveFields
        /// </summary>
        /// <param name="mode">Insertion or update</param>
        /// <param name="entityType">Entity type</param>
        /// <param name="entitySet">Table name</param>
        /// <param name="fieldName">Column name</param>
        /// <param name="fieldValue"></param>
        /// <param name="primaryKeyField">Primary key column name</param>
        /// <param name="eventId"></param>
        /// <param name="queryField">Column name required for query</param>
        /// <param name="dataType"></param>
        /// <returns>currently no use of returning, simply integer one returned</returns>
        public int GenericSaveFields(string mode, bool isEventData, string entityType, string entitySet, string fieldName, dynamic fieldValue, string primaryKeyField, string eventIdStr,
                                    string queryField, string dataType, int queryFieldValue, string secondQueryField, string companyID, int userId)
        {
            try
            {

                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventIdStr));
                int moduleId = entityType.Contains("Incident") ? 1: (entityType.Contains("Investigation") ? 2: (entityType.Contains("Audit")? 3 : 4));
                bool isInsertion = false;

                #region For Audits

                if (isEventData) //parameter getting passed from the js file, for event related pages it 'IsEventData' will be true and for non-event related pages it will be false
                {

                    //Audits,AuditID,AuditDatetime are Audit table column names in DB.
                    //if (SessionService.ModuleId == (int)tap.dom.hlp.Enumeration.Modules.Audit && fieldName == "IncidentDatetime") // Is Audit ?
                    if (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Audit && fieldName == "IncidentDatetime") // Is Audit ?
                    {
                        entityType = "tap.dat.Audits,tap.dat";
                        entitySet = "Audits";
                        primaryKeyField = "AuditID";
                        fieldName = "AuditDatetime";
                    }

                }
                #endregion

                #region Create ObjectContext

                //Get connection string,create the ObjectContext instance             

                string connectionString = ConfigurationManager.ConnectionStrings["EFEntity"].ToString();
                ObjectContext context = new ObjectContext(connectionString);
                context.DefaultContainerName = "EFEntity";//check how to remove hard coded

                #endregion

                #region Instance of the entity

                //Create Instance for the entityset

                var getEntityType = Type.GetType(entityType);
                var setEntityInstance = Activator.CreateInstance(getEntityType);

                #endregion

                #region For Custom radio buttons mode change

                //For second insert of radio button, change the mode to update.
                if (mode == "Insert" && entitySet == "FieldValues")
                {
                    var thisEntity = _db.ExecuteStoreQuery<int>(string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}' and {4} = '{5}' ", primaryKeyField, entitySet, queryField, queryFieldValue, secondQueryField, eventId)).FirstOrDefault();
                    if (thisEntity != 0)
                        mode = "Update";
                }

                #endregion

                #region Check for Update

                if (mode == "Update")
                {
                    int selectedEntity = 1;

                    if (entitySet == "Events")
                        selectedEntity = eventId;

                    if (entitySet == "FieldValues")
                    {
                        selectedEntity = _db.ExecuteStoreQuery<int>(string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}' and {4} = '{5}' ", primaryKeyField, entitySet, queryField, queryFieldValue, secondQueryField, eventId)).FirstOrDefault();
                        if (selectedEntity == 0)
                        {
                            mode = "Insert";
                            isInsertion = true;
                        }
                    }
                    else
                    {
                        selectedEntity = _db.ExecuteStoreQuery<int>(string.Format("SELECT {0} FROM {1} WHERE {2} = '{3}' ", primaryKeyField, entitySet, queryField, eventId)).Single();
                    }

                    //To update the fields
                    if (!isInsertion) //Skip if the operation is insertion.
                    {
                        setEntityInstance.GetType().GetProperty(primaryKeyField).SetValue(setEntityInstance, selectedEntity, null);

                        _db.AttachTo(entitySet, setEntityInstance);
                    }
                }

                #endregion

                #region Conversion to the respective types

                //Parsed the fieldValue to integer to save the value in DB.
                if (dataType == "Int")
                {
                    var propertyName = setEntityInstance.GetType().GetProperty(fieldName).PropertyType.Name;

                    if (propertyName == "Nullable`1")
                    {
                        int fieldValues;
                        bool result = Int32.TryParse(fieldValue, out fieldValues);
                        fieldValue = fieldValues;
                    }
                    
                    //int fieldValues;
                    //bool result = Int32.TryParse(fieldValue, out fieldValues);
                    //fieldValue = fieldValues;
                }
                else if (dataType == "Boolean")
                {
                    bool fieldValues = (fieldValue == "0") ? false : true;
                    fieldValue = fieldValues;
                }
                else if (dataType == "datetime")
                {
                    //Changed the fieldValue to the DateTime format to save the value in DB.
                    //fieldValue = _commonOperation.SiteSettingDateFormat(companyID, fieldValue);
                    //DateTime dateValue = DateTime.ParseExact(fieldValue, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                   // DateTime? dateValue = string.IsNullOrEmpty(fieldValue) ? (DateTime?)null : DateTime.ParseExact(fieldValue, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateFormat dt = new DateFormat();
                    DateTime? dateValue = dt.fromDatefrmatToDate(fieldValue, Convert.ToInt32(companyID));
                    fieldValue = dateValue;
                }
                else if (dataType == "time")
                {
                    if (!string.IsNullOrEmpty(fieldValue))
                    {
                        DateTime dt = Convert.ToDateTime(fieldValue);
                        TimeSpan ts = dt.TimeOfDay;
                        fieldValue = ts;
                    }
                    else
                    {
                        fieldValue = null;
                    }
                }
                //if (string.IsNullOrEmpty(fieldValue))
                //    fieldValue = null;
                setEntityInstance.GetType().GetProperty(fieldName).SetValue(setEntityInstance, fieldValue, null);

                #endregion

                #region For insertion of field values

                if (mode == "Insert")
                {
                    //Currently hard coded the properties EventID and FieldID.
                    setEntityInstance.GetType().GetProperty(secondQueryField).SetValue(setEntityInstance, eventId, null);

                    //int fieldValues;
                    //bool result = Int32.TryParse(queryFieldValue, out fieldValues);
                    setEntityInstance.GetType().GetProperty(queryField).SetValue(setEntityInstance, queryFieldValue, null);

                    //Need to remove this line of code after changing the notNull constraint in DB.                     
                    setEntityInstance.GetType().GetProperty("isDataList").SetValue(setEntityInstance, false, null);

                    _db.AddObject(entitySet, setEntityInstance);
                }

                #endregion

                #region Event type, DateModified on changes

                //Update the DateModified value in DB on any changes.

                if (isEventData)
                {
                    var eventData = _db.Events.First(a => a.EventID == eventId);
                    eventData.DateModified = DateTime.UtcNow;
                    
                    if (dataType == "datetime" && (entitySet == "Incidents" || entitySet == "Audits"))
                    {
                        eventData.EventDate = fieldValue;
                    }
                }
                #endregion

                #region Delete the object

                //While inserting, on undo the custom fields, Delete the object
                if (mode == "Update" && entitySet == "FieldValues" && fieldValue == "")
                    _db.DeleteObject(setEntityInstance);

                #endregion

                _db.SaveChanges();

                //string message = isEventData ? CommonOperation.GetModuleNameByModuleId(SessionService.ModuleId) : entitySet;
                string message = isEventData ? CommonOperation.GetModuleNameByModuleId(moduleId) : entitySet;
                message = message + " updated. '" + fieldName + "' changed to '" + fieldValue + "'.";

                transaction.Transaction.SaveTransaction(eventId, userId, null, Enumeration.TransactionCategory.Update, message);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            #region "Do not Delete"
            //To Save the fields

            //entityset.GetType().GetProperty(fieldname).SetValue(entityset, fieldvalue, null);
            //_db.AddObject(entityset,entitysetIns);
            // _db.SaveChanges();
            #endregion

            //Simply returning one
            //In future we might return any ID or Success status
            return 1;//remove hard code return type 1
        }


        #endregion

        #region "Insert  Location Field"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="locationId"></param>
        public void LocationSave(string eventID, string locationId, int userId)
        {
            //Get the prperties for the entitySet, Events
            try
            {
                int eventId = Convert.ToInt32((!string.IsNullOrEmpty(eventID) && eventID != "0") ? _cryptography.Decrypt(eventID) : "0");
                //Got the Event Properties based on Event id
                //Changed the location and saved.
                _currentEvent = _db.Events.FirstOrDefault(a => a.EventID == eventId);
                _db.Attach(_currentEvent);
                _currentEvent.Location = locationId;
                _db.SaveChanges();

                tap.dom.transaction.Transaction.SaveTransaction(eventId, userId, null, Enumeration.TransactionCategory.Update,
                "Location Updated");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #endregion

        #region "Insert Classification"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="classification"></param>
        public void ClassificationSave(string eventID, string classification, int userId)
        {


            int classificationId;
            bool isDeleteListValue = false;
            try
            {
                int eventId = Convert.ToInt32((!string.IsNullOrEmpty(eventID) && eventID != "0") ? _cryptography.Decrypt(eventID) : "0");

                //Get the properties for the entitySet, Events based on EventId
                _currentEvent = _db.Events.FirstOrDefault(a => a.EventID == eventId);


                //Deleted the related event ids for the classification and saved the new classification.
                if (classification != null && classification != "")
                {
                    string[] arrayListValues = classification.Split(',');

                    for (int i = 0; i < arrayListValues.Length; i++)
                    {

                        classificationId = Convert.ToInt32(arrayListValues[i].Split('#')[0]);

                        if (eventId != -1 && !isDeleteListValue)
                        {
                            DeleteFieldListValues(Convert.ToInt32(tap.dom.hlp.Enumeration.SystemFields.Events_Classification), _currentEvent.EventID, classificationId);
                            isDeleteListValue = true;
                        }

                        SaveFieldListValues(Convert.ToInt32(tap.dom.hlp.Enumeration.SystemFields.Events_Classification), _currentEvent.EventID, classificationId, eventId);
                    }


                    tap.dom.transaction.Transaction.SaveTransaction(eventId, userId, null, Enumeration.TransactionCategory.Update, "Classification Updated");
                }
            }
            catch (Exception ex)
            {

                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #endregion

        #region "Delete FieldList Values"

        /// <summary>
        /// DeleteFieldListValues
        /// </summary>
        /// <param name="systemFieldId"></param>
        /// <param name="eventId"></param>
        /// <param name="valueId"></param>
        public void DeleteFieldListValues(int systemFieldId, int eventId, int valueId)
        {
            //Deleted the object in the DB based on the Event id.
            try
            {
                int listId = 0;
                string fullPath = "\\Classification";//remove hard code
                tap.dom.adm.ListValues listValues = new adm.ListValues();

                listId = listValues.ListValueIDByNameSelect(fullPath);//get the list id

                List<tap.dat.FieldListValues> fieldListValues = _db.FieldListValues.Where(a => (a.EventId == eventId)).ToList();

                for (int i = 0; i < fieldListValues.Count(); i++)
                {
                    _db.DeleteObject(fieldListValues[i]);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #endregion

        #region To insert Data in FieldListValues
        public void SaveFieldListValues(int systemFieldId, int eventId, int valueId, int updateEventId)
        {
            //Saved the classification details in DB.
            bool isNewFieldValue = false;
            try
            {
                int listId = 0;
                string fullPath = "\\Classification";
                tap.dom.adm.ListValues listValue = new adm.ListValues();
                listId = listValue.ListValueIDByNameSelect(fullPath);//get the list id

                tap.dat.FieldListValues fieldListValue = null;
                fieldListValue = _db.FieldListValues.FirstOrDefault(a => a.EventId == eventId);
                fieldListValue = new dat.FieldListValues();
                isNewFieldValue = true;

                fieldListValue.FieldValueId = listId;
                fieldListValue.SystemFieldId = systemFieldId;
                fieldListValue.EventId = eventId;
                fieldListValue.ValueId = valueId;
                if (isNewFieldValue)
                    _db.AddToFieldListValues(fieldListValue);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

    }
}
