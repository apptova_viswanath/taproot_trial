﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using tap.dom.adm;

namespace tap.dom.usr
{
    public class Password
    {
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();

        //Get Password Policy for Regular Expression
        public string PasswordPolicy(string compId)
        {
            try
            {
                int? paswordpolicyId = 0;
                if (compId != "0")
                {
                    Company company = new Company();
                    int companyId = Convert.ToInt32(compId);
                    tap.dat.SiteSettings settings = new dat.SiteSettings();
                    settings = _db.SiteSettings.FirstOrDefault(x => x.CompanyID == companyId);
                    paswordpolicyId = settings.PasswordPolicyId;
                    if (settings == null || settings.PasswordPolicyId == 0)
                        return string.Empty;

                }
                else
                    paswordpolicyId = 1;



                var passwordPolicyDetails = from passwordPolicy in _db.PasswordPolicy
                                            where passwordPolicy.PasswordPolicyId == paswordpolicyId
                                            select new
                                            {
                                                PasswordPolicyID = passwordPolicy.PasswordPolicyId,
                                                Name = passwordPolicy.Name,
                                                MinimumCharacters = passwordPolicy.MinimumCharacters,
                                                MaximumCharacters = passwordPolicy.MaximumCharacters,
                                                UpperCaseLetter = passwordPolicy.UpperCaseLetter,
                                                LowerCaseLetter = passwordPolicy.LowerCaseLetter,
                                                Number = passwordPolicy.Number,
                                                SpecialCharacter = passwordPolicy.SpecialCharacter
                                            };


                return _efSerializer.EFSerialize(passwordPolicyDetails);


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return string.Empty;
        }





        //Get Password Regular Expression according to Password Policy
        public string PaswordRegularExpression()
        {
            string RegularExpression = string.Empty;
            try
            {
                Company company = new Company();
                int companyId = company.OrganizationId();

                tap.dat.SiteSettings settings = new dat.SiteSettings();
                settings = _db.SiteSettings.FirstOrDefault(x => x.CompanyID == companyId);

                if (settings == null || settings.PasswordPolicyId == 0)
                    return string.Empty;
                tap.dat.PasswordPolicy policy = new dat.PasswordPolicy();
                policy = _db.PasswordPolicy.FirstOrDefault(x => x.PasswordPolicyId == settings.PasswordPolicyId);

                if (policy == null)
                    return null;

                RegularExpression = "^";

                if (policy.Number != null)
                    RegularExpression += "(?=(.*[0-9]){" + policy.Number + ",})";
                if (policy.UpperCaseLetter != null)
                    RegularExpression += "(?=(.*[A-Z]){" + policy.UpperCaseLetter + ",})";
                if (policy.LowerCaseLetter != null)
                    RegularExpression += "(?=(.*[a-z]){" + policy.UpperCaseLetter + ",})";
                if (policy.SpecialCharacter != null)
                    RegularExpression += "(?=(.*[#@!^%$*=+_]){" + policy.UpperCaseLetter + ",})";
                if (policy.MinimumCharacters != null)
                    RegularExpression += ".{" + policy.MinimumCharacters;
                if (policy.MaximumCharacters != null)
                    RegularExpression += "," + policy.MaximumCharacters + "}";
                else
                    RegularExpression += ",}";

                RegularExpression += "$";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return RegularExpression;
        }

        //Match password with password policy
        public bool MatchPasswordPolicy(int userId)
        {
            bool result = false;

            try
            {
                string password = string.Empty;

                password = _db.Users.FirstOrDefault(x => x.UserID == userId).Password;

                if (password == null)
                    return true;

                if (PaswordRegularExpression() == null)
                    return true;

                Match match = Regex.Match(password, PaswordRegularExpression(), RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }

        //Check password expiration time with user
        public bool PasswordExpired(int userId)
        {
            try
            {
                DateTime today = DateTime.Now;
                DateTime lastPasswordChange = DateTime.Now;
                int expireDays = 0;
                tap.dat.Users user = new dat.Users();

                Company company = new Company();
                int companyId = company.OrganizationId();

                tap.dat.SiteSettings setting = new dat.SiteSettings();
                setting = _db.SiteSettings.FirstOrDefault(x => x.CompanyID == companyId);
                if (setting == null)
                    return false;

                user = _db.Users.FirstOrDefault(x => x.UserID == userId && x.Active == true);
                if (user == null)
                    return false;


                if (setting.PasswordPolicyId == 0 || user.LastPasswordChange == null)
                    return false;
                else
                {
                    if(setting.PasswordExpiration != null && setting.PasswordExpiration > 0)
                        expireDays = Convert.ToInt32(setting.PasswordExpiration);
                    if (expireDays == 0)
                        return false;

                    lastPasswordChange = Convert.ToDateTime(user.LastPasswordChange).AddDays(expireDays);

                    if (today > lastPasswordChange)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return false;

        }
        public string PasswordExpiresInDays(int userId)
        {
            try
            {
                DateTime today = DateTime.Now;
                DateTime lastPasswordChange = DateTime.Now;
                int expireDays = 0;
                tap.dat.Users user = new dat.Users();
                               

                user = _db.Users.FirstOrDefault(x => x.UserID == userId && x.Active == true);
                if (user == null)
                    return "0";


                Company company = new Company();
                int companyId = Convert.ToInt32(user.CompanyID); //company.OrganizationId();

                tap.dat.SiteSettings setting = new dat.SiteSettings();
                setting = _db.SiteSettings.FirstOrDefault(x => x.CompanyID == companyId);
                if (setting == null)
                    return "0";


                if (setting.PasswordPolicyId == 0 || user.LastPasswordChange == null)
                    return "0";
                else
                {
                    if (setting.PasswordExpiration != null && setting.PasswordExpiration > 0)
                        expireDays = Convert.ToInt32(setting.PasswordExpiration);
                    if (expireDays == 0)
                        return "0";

                    lastPasswordChange = Convert.ToDateTime(user.LastPasswordChange).AddDays(expireDays);
                    return Convert.ToString((Convert.ToDateTime(lastPasswordChange.Date) - Convert.ToDateTime(DateTime.Now.Date)).TotalDays);

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return "0";

        }

    }
}
