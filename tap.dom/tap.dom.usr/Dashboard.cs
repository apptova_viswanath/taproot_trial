﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using tap.dat;

namespace tap.dom.usr
{
    /// <summary>
    /// For Admin Dashboard operations.
    /// </summary>
    /// 

    public class Dashboard
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        #region Event Breakdown

        /// <summary>
        /// Gets the Event Breakdown
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns>Concatinated event breakdown value</returns>
        public StringBuilder EventBreakdown(int companyId, string fDate, string tDate)
        {
            DateTime fromDate = Convert.ToDateTime(fDate, DatePatternGet());
            DateTime toDate = Convert.ToDateTime(tDate, DatePatternGet());

            int? totalEvents = (from evt in _db.Events
                                where (evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Incident ||
                                    evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Investigation ||
                                    evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Audit)
                                && evt.CompanyID == companyId
                                && evt.EventDate > fromDate
                                && evt.EventDate < toDate
                                select evt).Count();

            var totalInc = (from inc in _db.Incidents
                                   join evt in _db.Events
                                   on inc.EventID equals evt.EventID
                                   where evt.CompanyID == companyId && evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Incident
                                   && evt.EventDate > fromDate
                                   && evt.EventDate < toDate
                                   select inc);
            int? totalIncidents = (from inc in totalInc
                                    where !(from inv in _db.Investigations
                                             select inv.EventID).Contains(inc.EventID)
                                    select inc).Count();

            int? actualTotalInc = (from inc in _db.Incidents
                                   join evt in _db.Events
                                   on inc.EventID equals evt.EventID
                                   where evt.CompanyID == companyId && evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Incident
                                   && evt.EventDate > fromDate
                                   && evt.EventDate < toDate
                                   select inc).Count();

            int? totalInvestigations = (from inv in _db.Investigations
                                        join evt in _db.Events
                                        on inv.EventID equals evt.EventID
                                        where evt.CompanyID == companyId // && evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Investigation
                                        && evt.EventDate > fromDate
                                        && evt.EventDate < toDate
                                        select inv).Count();

            int? totalAudits = (from aud in _db.Audits
                                join evt in _db.Events on aud.EventID equals evt.EventID
                                where evt.CompanyID == companyId && evt.ModuleID == (int)tap.dom.hlp.Enumeration.Modules.Audit
                                && evt.EventDate > fromDate
                                && evt.EventDate < toDate
                                select aud).Count();

            StringBuilder evtBreakdownValues = new StringBuilder("");
            evtBreakdownValues.Append(string.Format("ActiveEventsCount={0}#ActiveIncidents={1}#ActiveInvestigations={2}#ActiveAudits={3}", totalEvents, totalIncidents, totalInvestigations, totalAudits));

            return evtBreakdownValues;
        }

        #endregion

        #region Most Common Root Causes

        /// <summary>
        /// Top 10 Root Causes with fullpath
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public object MostCommonRootCauses(int companyId, string fDate, string tDate)
        {
            DateTime fromDate = Convert.ToDateTime(fDate, DatePatternGet());
            DateTime toDate = Convert.ToDateTime(tDate, DatePatternGet());

            var rootcauses = (from ev in _db.Events
                              join inv in _db.Investigations on ev.EventID equals inv.EventID
                              join sc in _db.SnapCharts on ev.EventID equals sc.EventID
                              join cf in _db.CausalFactors on sc.SnapChartID equals cf.SnapChartID
                              join rct in _db.RootCauseTree on cf.CausalFactorID equals rct.CausalFactorID
                              join rc in _db.RootCauses on rct.RootCauseID equals rc.RootCauseID
                              where rc.RootCauseCategoryID == (int) tap.dom.hlp.Enumeration.RootCauseTreeType.RootCause
                                    && ev.EventDate > fromDate
                                    && ev.EventDate < toDate
                                    && ev.CompanyID == companyId
                              group rct by new { rct.RootCauseID, rc.FullPath } into temp
                              let number = temp.Count()
                              orderby number descending
                              select new
                              {
                                  num = number,
                                  rootcause = temp.Key
                              }).Take(10);

            return rootcauses.ToList();
        }


        #endregion

        #region Basic Cause Category

        public object BasicCauseCategoryDistribution(int companyId, string fDate, string tDate)
        {
            
            DateTime fromDate = Convert.ToDateTime(fDate, DatePatternGet());
            DateTime toDate = Convert.ToDateTime(tDate, DatePatternGet());

            //DateTime fromDate = new DateTime(2014, 1, 1); 
            //toDate = new DateTime(2014, 12, 31, 23, 59, 59);            

            var basicCauses = (from ev in _db.Events
                               join inv in _db.Investigations on ev.EventID equals inv.EventID
                               join sc in _db.SnapCharts on ev.EventID equals sc.EventID
                               join cf in _db.CausalFactors on sc.SnapChartID equals cf.SnapChartID
                               join rct in _db.RootCauseTree on cf.CausalFactorID equals rct.CausalFactorID
                               join rc in _db.RootCauses on rct.RootCauseID equals rc.RootCauseID
                               where
                                    rc.RootCauseCategoryID == (int)tap.dom.hlp.Enumeration.RootCauseTreeType.Basic
                                    && ev.EventDate > fromDate
                                    && ev.EventDate < toDate
                                    && ev.CompanyID == companyId
                               group rc by new { rc.Title, rc.RootCauseCategoryID } into temp
                               let number = temp.Count()
                               orderby number descending
                               select new
                                {
                                    num = number,
                                    basicCause = temp.Key
                                }).Take(6);

            return basicCauses.ToList();
        }

        #endregion

        #region Set the Date pattern
        public DateTimeFormatInfo DatePatternGet()
        {
            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd-MM-yyyy";
            dtfi.DateSeparator = "-";
            return dtfi;
        }

#endregion

        /* Select rc.Title, Count(rc.RootCauseCategoryID) From Events e 
Inner Join Investigations I on I.EventId = e.EventId 
Inner Join SnapCharts sc on e.EventId = sc.EventId 
Inner Join CausalFactors cf on cf.SnapChartID = sc.SnapChartID  --230
Inner Join RootCauseTree rct on rct.CausalFactorID= cf.CausalFactorID 
Inner Join RootCauses rc on rc.RootCauseID = rct.RootCauseID 
WHERE rc.RootCauseCategoryID = 2
AND e.EventDate BETWEEN '1/1/2014' AND '1/1/2015'
Group By rc.Title, rc.RootCauseCategoryID
order by count(rc.RootCauseCategoryID) DESC */        

    }
}
