﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using tap.dom.adm;
using tap.dom.gen;

namespace tap.dom.usr
{
    public class DateFormat
    {
        public DateTime convertAsAdminDate(string SourseDate)
        {

            DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd-MM-yyyy";
            dtfi.DateSeparator = "-";

            if (SourseDate.Contains('/'))
            {

                string[] dateParams = SourseDate.Split('/');
                //if (dateParams[2].Length == 2)
                //{                   
                int fullyear = convertTwoDigitYearToFour(Convert.ToInt16(dateParams[2]));
                //}
                DateTime date = new DateTime(fullyear, Int32.Parse(dateParams[0]), Int32.Parse(dateParams[1]));
                return date;
            }
            else
            {

                return Convert.ToDateTime(SourseDate, dtfi);
            }
        }
        private int convertTwoDigitYearToFour(int yearTwoOrFour)
        {
            try
            {
                if (yearTwoOrFour.ToString().Length <= 2)
                {
                    DateTime yearOnly = DateTime.ParseExact(yearTwoOrFour.ToString("D2"), "yy", null);
                    if (yearOnly.Year.ToString().Contains("19"))
                    {
                        return Convert.ToInt32("20" + yearTwoOrFour.ToString());
                    }
                    return yearOnly.Year;
                }
            }
            catch
            {
            }

            return yearTwoOrFour;
        }
        public DateTime fromDatefrmatToDate(string dateValue, int? companyId = 0)
        {
            var dateDisplayFormat = string.Empty;

            if (companyId > 0)
            {
                tap.dat.EFEntity _entityContext = new dat.EFEntity();
                dateDisplayFormat = _entityContext.SiteSettings.Where(a => a.CompanyID == companyId).Select(a => a.DateFormat).FirstOrDefault();
            }
            else
                dateDisplayFormat = "MMM dd yyyy";

            switch (dateDisplayFormat)
            {

                case "dd/MM/yyyy":
                    {
                        var dateSplit = dateValue.Split('/');
                      
                        if(!(dateSplit.Length > 1))
                        {
                            goto default;
                        }
                        return new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
                    }


                case "MM/dd/yyyy":
                    {
                        var dateSplit = dateValue.Split('/');
                        if (!(dateSplit.Length > 1))
                        {
                            goto default;
                        }
                        return new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]));
                    }


                case "MMM dd yyyy":
                    {
                        var dateSplit = dateValue.Split(' ');
                        if (!(dateSplit.Length > 1))
                        {
                            goto default;
                        }
                        return Convert.ToDateTime(dateValue);
                    }
               
                default:
                    return Convert.ToDateTime(dateValue);
            }
        }
    }
}
