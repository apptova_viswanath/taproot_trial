﻿/********************************************************************************
 *  File Name   : CompanyProfile.aspx.cs
 *  Description : Class that contain functions to insert 100s of records to test load balancing
 *  Created on  : N/A
 *******************************************************************************/

# region NameSpace
using System;
#endregion


namespace tap.dom.usr
{

    //Class that contain functions to insert 100s of records to test load balancing
    public class LoadTesting
    {

        tap.dat.EFEntity _entityContext = new dat.EFEntity();


        /// <summary>
        /// Insert data into admin custom tabs and fields
        /// </summary>
        public void InsertDataForTab()
        {

            try
            {
                
                int tabCount = 50;
                int startTabCount = 1;

                int fieldCount = 100;
                int startFieldCout = 1;

                
                for (startTabCount = 1; startTabCount <= tabCount; startTabCount++)
                {
                    
                    //Assign tab data
                    tap.dat.Tabs tab = new tap.dat.Tabs();
                    tab.TabName = "Tab-" + startTabCount;
                    tab.IsUniversal = false;
                    tab.IsSystem = false;
                    tab.CompanyID = 1;
                    tab.Active = true;
                    tab.IsDeleted = false;
                     
                    //Save tab
                    _entityContext.AddToTabs(tab);
                    _entityContext.SaveChanges();

                    //Save tab modules
                    tap.dom.adm.TabModules tabModules = new tap.dom.adm.TabModules();
                    tabModules.SaveTabModules(tab.TabID, Convert.ToInt32(tap.dom.hlp.Enumeration.Modules.Incident));
                    tabModules.SaveTabModules(tab.TabID, Convert.ToInt32(tap.dom.hlp.Enumeration.Modules.Investigation));
                    tabModules.SaveTabModules(tab.TabID, Convert.ToInt32(tap.dom.hlp.Enumeration.Modules.Audit));
                    tabModules.SaveTabModules(tab.TabID, Convert.ToInt32(tap.dom.hlp.Enumeration.Modules.ActionPlan));

                    //Save fields
                    for (startFieldCout = 1; startFieldCout <= fieldCount; startFieldCout++)
                    {

                        //Assign fields data
                        tap.dat.Fields fields = new tap.dat.Fields();
                        fields.DisplayName = "Field-" + startTabCount;
                        fields.TabID = tab.TabID;
                        fields.Datatype = "String";
                        fields.ControlType = "ParagraphText";
                        fields.IsActive = true;

                        //Save fields
                        _entityContext.AddToFields(fields);
                        _entityContext.SaveChanges();

                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Insert data into company, user, events and investigations
        /// </summary>
        public void InsertData()
        {

            try
            {
                int companyCount = 50;
                int startCompanyCount = 1;

                int startUserCount = 1;
                int userCount = 0;

                int startEventCount = 1;
                int eventCount = 100;

                #region Insert User Data

                //Iterate and insert 50 companies
                for (startCompanyCount = 1; startCompanyCount <= companyCount; startCompanyCount++)
                {

                    //Assign company data
                    tap.dat.Companies company = new tap.dat.Companies();
                    company.CompanyName = "Company-" + startCompanyCount;
                    company.Active = true;

                    //Save company
                    _entityContext.AddToCompanies(company);
                    _entityContext.SaveChanges();

                    //For each company add 100s of users
                    userCount = startCompanyCount*100;

                    #region Insert User Data

                    for (startUserCount = 1; startUserCount <= userCount; startUserCount++)
                    {

                        //Assign user data
                        tap.dat.Users user = new tap.dat.Users();
                        user.UserName = user.FirstName = user.Password = "User-" + startUserCount;
                        user.Active = true;
                        user.GroupId = 1;
                        user.CompanyID = company.CompanyID;

                        //Save user data
                        _entityContext.AddToUsers(user);
                        _entityContext.SaveChanges();

                        #region Insert Event Data

                        //For each user add 100s of events and investigations
                        for (startEventCount = 1; startEventCount <= eventCount; startEventCount++)
                        {

                            //Assign event data
                            tap.dat.Events events = new tap.dat.Events();
                            events.Name = "Event-" + startEventCount;
                            events.Status = 1;
                            events.ModuleID = 2;
                            events.CompanyID = company.CompanyID;
                            events.CreatedBy = user.UserID;
                            events.DateCreated = DateTime.Now;

                            //Save event data
                            _entityContext.AddToEvents(events);
                            _entityContext.SaveChanges();

                            //Assign investigation data
                            tap.dat.Investigations investigation = new tap.dat.Investigations();
                            investigation.EventID = events.EventID;
                            investigation.CreatedBy = user.UserID;
                            investigation.DateCreated = DateTime.Now;

                            //Save investigation data
                            _entityContext.AddToInvestigations(investigation);
                            _entityContext.SaveChanges();

                        }

                        #endregion

                    }

                    #endregion

                }

                #endregion
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
