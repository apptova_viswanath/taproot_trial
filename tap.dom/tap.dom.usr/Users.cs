﻿/*Login Details Methods*/
#region "Namespace" 
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using tap.dom.gen;
#endregion

namespace tap.dom.usr
{
    public class Users
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        tap.dom.adm.CommonOperation _commonOperation = new adm.CommonOperation();
        tap.dom.gen.Cryptography _crypt = new dom.gen.Cryptography();

        #region "Select Active Users"
        public object GetAllActiveUsers(int companyId)
        {
            var activeUsers = (from u in _db.Users
                               where u.Active == true && u.CompanyID == companyId
                               select u).ToList();
            return activeUsers;
        }
        #endregion


        #region "Login authentication"
        public object MostActiveUsersSelect(string userName, string password)
        {
            bool isValidUser = false;
            try
            {
                string passwordEncrypted = _crypt.Encrypt(password);

                // Check if normal User or AD User
                var userType = (from u in _db.Users
                                join c in _db.Companies on u.CompanyID equals c.CompanyID
                                where u.UserName == userName
                                select c.CheckADSecurity).FirstOrDefault();

                if (Convert.ToBoolean(userType))
                {
                    tap.dat.Users loginDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);
                    return true;
                }
                else
                {
                    tap.dat.Users loginDetails = _db.Users.FirstOrDefault(a => a.UserName == userName &&
                                 (a.Password == passwordEncrypted 
                                 //|| a.Password == password
                                 ));
                    if (loginDetails != null)
                    {
                        if (!loginDetails.Active)
                        {
                            return "InActive";
                        }

                        var loginPassword = loginDetails.Password;
                        bool checkLoginPassword = password.Equals(password, StringComparison.Ordinal);

                        if (checkLoginPassword)
                            isValidUser = true;
                        else
                            isValidUser = false;
                    }
                    else
                        isValidUser = false;
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Users.cs", "MostActiveUsersSelect");
            }

            return isValidUser;
        }
        #endregion

        #region "Change Password"
        public string UserPasswordChange(string userName, string oldPassword, string newPassword)
        {
            string passwordSatus = string.Empty;
            try
            {
                tap.dat.Users userDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);
                if (userDetails != null)
                {
                    userDetails.Password = _crypt.Encrypt(newPassword);
                    userDetails.LastPasswordChange = DateTime.Now;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return passwordSatus;
        }
        #endregion

        #region "Get the User Details"
        public tap.dat.Users GetLoginUserDetails(string userName, string password)
        {
            string encryptedPassword = _crypt.Encrypt(password);

            var UserDetails = _db.Users.FirstOrDefault(a => a.UserName == userName
                         && (a.Password == encryptedPassword 
                         //|| a.Password == password
                         ) && a.Active == true);
            return UserDetails;
        }
        #endregion


        #region "Select  Users adm dateformat details"
        public string GetUserandAdminDetails(string userName, string password)
        {
            string encryptedPassword = _crypt.Encrypt(password);

            var userPwd = (from u in _db.Users
                           where u.UserName == userName
                           select u.Password).FirstOrDefault();

            // If user password is NULL, then AD user
            if (userPwd == null || userPwd == "")
            {
                var UsersAndAdmin = (from u in _db.Users
                                     join s in _db.SiteSettings on u.CompanyID equals s.CompanyID
                                     join c in _db.Companies on u.CompanyID equals c.CompanyID
                                     join la in _db.Languages on u.LanguageId equals la.LanguageID
                                     where u.Active == true && u.UserName == userName
                                     select new
                                     {
                                         companyId = c.CompanyID,
                                         companyName = c.CompanyName,
                                         userId = u.UserID,
                                         userName = u.UserName,
                                         firstName = u.FirstName,
                                         dateFormat = s.DateFormat,
                                         languageId = u.LanguageId,
                                         IsGlobal = c.ParentId,
                                         languageName = la.LangaugeCode,
                                         languageDescription = la.Description
                                     }).ToList();
                return _efSerializer.EFSerialize(UsersAndAdmin);
            }
            else
            {
                var UsersAndAdmin = (from u in _db.Users
                                     join s in _db.SiteSettings on u.CompanyID equals s.CompanyID
                                     join c in _db.Companies on u.CompanyID equals c.CompanyID
                                     join la in _db.Languages on u.LanguageId equals la.LanguageID
                                     where u.Active == true && u.UserName == userName
                                         && (u.Password == encryptedPassword 
                                         //|| u.Password == password
                                         )
                                     select new
                                     {
                                         companyId = c.CompanyID,
                                         companyName = c.CompanyName,
                                         userId = u.UserID,
                                         userName = u.UserName,
                                         firstName = u.FirstName,
                                         dateFormat = s.DateFormat,
                                         languageId = u.LanguageId,
                                         IsGlobal = c.ParentId,
                                         languageName = la.LangaugeCode,
                                         languageDescription = la.Description
                                     }).ToList();
                return _efSerializer.EFSerialize(UsersAndAdmin);
            }
        }
        #endregion

        public void SaveUserDetails(string userName, string password, string companyId, string firstName, string lastName,
                             string phoneNumber, string emailId, string group, int active)
        {
            tap.dat.Users newUser = new tap.dat.Users(); // Create new user object

            // Add the values
            newUser.UserName = userName;
            newUser.Password = _crypt.Encrypt(password);
            newUser.CompanyID = companyId == "" ? 0 : Int32.Parse(companyId);
            newUser.FirstName = firstName;
            newUser.LastName = lastName;
            newUser.PhoneNo = phoneNumber;
            newUser.Email = emailId;
            //newUser.SecurityGroupAuthorization = group;
            if (group.ToLower() == "admin")
                newUser.GroupId = 1;
            if (group.ToLower() == "operator")
                newUser.GroupId = 2;
            if (group.ToLower() == "investigator")
                newUser.GroupId = 3;
            if (group.ToLower() == "auditor")
                newUser.GroupId = 4;
            newUser.Active = active == 1 ? true : false;

            _db.AddToUsers(newUser);
            _db.SaveChanges();
        }

        public string GetUserDetails(int userID)
        {
            var user = from a in _db.Users
                       join c in _db.Companies on a.CompanyID equals c.CompanyID
                       where a.UserID == userID
                       select new
                       {
                           userID = a.UserID,
                           FirstName = a.FirstName,
                           LastName = a.LastName,
                           UserName = a.UserName,
                           Password = a.Password,
                           Email = a.Email,
                           PhoneNumber = a.PhoneNo,
                           CompanyID = a.CompanyID,
                           GroupID = a.GroupId,
                           Active = a.Active,
                           Admin = a.isAdministrator,
                           ViewEvents = a.ViewEvents,
                           EditEvents = a.EditEvents,
                           SubscriptionStart = c.SubscriptionStart,
                           SubscriptionEnd = c.SubscriptionEnd
                       };//new-change

            return _efSerializer.EFSerialize(user);
        }

        public List<tap.dat.Companies> GetUserData(int userID)
        {
            List<tap.dat.Companies> company = (from a in _db.Users
                                               join c in _db.Companies on a.CompanyID equals c.CompanyID
                                               where a.UserID == userID
                                               select c).ToList();//new-change

            return company;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="companyId"></param>
        /// <param name="selectedDivisionId"></param>
        /// <returns></returns>
        public object CheckIfGlobalAdminProfile(int userId, int companyId, int selectedDivisionId)
        {


            bool isGlobalAdminPage = false;
            bool hideSubscriptionPanel = false;

            tap.dat.Users loggedInUserDetails = new tap.dat.Users();
            tap.dat.Companies loggedInCompanyDetails = new tap.dat.Companies();

            if (companyId > 0)
            {
                loggedInCompanyDetails = _db.Companies.FirstOrDefault(x => x.CompanyID == companyId);
            }

            if (userId > 0)
            {
                loggedInUserDetails = _db.Users.FirstOrDefault(a => a.UserID == userId && a.CompanyID == companyId);
            }



            if (loggedInCompanyDetails.ParentId != null)
            {
                hideSubscriptionPanel = true;
                isGlobalAdminPage = false;
            }
            else if (loggedInUserDetails != null && loggedInCompanyDetails != null &&
                Convert.ToBoolean(loggedInUserDetails.isAdministrator) && loggedInCompanyDetails.ParentId == null)
            {
                if (selectedDivisionId == companyId && selectedDivisionId > 0)
                {
                    hideSubscriptionPanel = true;
                }

                isGlobalAdminPage = true;
            }


            var result = new
            {
                isGlobalAdminPage = isGlobalAdminPage,
                hideSubscriptionPanel = hideSubscriptionPanel

            };

            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetCompanyIdOnUserId(int userId)
        {
            int companyId = 0;
            try
            {
                if (userId > 0)
                    companyId = Convert.ToInt32(_db.Users.Where(x => x.UserID == userId).FirstOrDefault().CompanyID);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return companyId;
        }


        public string ForgotPassword(string emailId, string baseUrl)//,string password,string confirmPassword
        {
            string result = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(emailId))
                    return null;

                tap.dat.Users user = new dat.Users();
                user = _db.Users.Where(a => a.Email.ToLower() == emailId.ToLower()).FirstOrDefault();

                if (user == null)
                    //return "error:" + emailId + " User Name does not exist.";
                    return "Cannot find " + emailId + " email address in this system.";
                if (user.Active == true)
                {
                    tap.dom.gen.Cryptography _cryptography = new Cryptography();

                    //  string password = user.Password;
                    string email = user.Email;
                    StringBuilder sb = new StringBuilder();

                    string resetPasswordUrl = String.Format(baseUrl + "tap.usr/ChangePassword.aspx?" + _cryptography.Encrypt(user.UserName.ToString() + "," + DateTime.Now.AddHours(24).ToString() + "," + user.CompanyID.ToString()));
                    sb.Append("We heard that you are having trouble logging in. Sorry about that!\n\n");
                    sb.Append("Your username is " + user.UserName + "\n\n");
                    sb.Append("If you lost your password, you can use the following link to reset your password:");
                    sb.Append("\n\n" + resetPasswordUrl);

                    sb.Append("\n\n This link is good for 24 hours.");
                    sb.Append("\n\n Thanks, \n Your friends at TapRooT(R) Software.");
                    string subject = "TapRooT Software Password Reminder";
                    tap.dom.adm.Mail mail = new adm.Mail();
                    result = mail.SendEmail(email, subject, sb.ToString());
                }
                else
                    return "error:" + emailId + " User Name is deactivated.";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
        public bool ResetPassword(string userName, string resetPassword)
        {
            tap.dat.Users user = null;
            user = _db.Users.FirstOrDefault(a => a.UserName == userName);
            if (user != null)
            {
                user.Password = _crypt.Encrypt(resetPassword);
                _db.SaveChanges();
                return true;
            }
            return false;

        }

        #region "User Language Settings"
        public string InsertLangaugeDetails(string langaugeCode, int companyId, int userId)
        {
            tap.dat.Languages languages = null;
            tap.dat.Users users = new tap.dat.Users();
            try
            {
                int languageId = _db.Languages.Where(a => a.LangaugeCode == langaugeCode).FirstOrDefault().LanguageID;
                users = _db.Users.FirstOrDefault(a => a.UserID == userId);

                if (users != null)
                {
                    users.UserID = userId;
                    users.LanguageId = languageId;

                    _db.SaveChanges();
                    return users.LanguageId.ToString();
                }

                return users.LanguageId.ToString();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return null;
        }

        public string GetLangaugeDetails(int companyId, int userId, string langaugeCode)
        {
            var languageDetails = (from language in _db.Languages
                                   where language.LangaugeCode == langaugeCode
                                   select language.Description);
            //var LanguageDetails = from users in _db.Users
            //                      join languages in _db.Languages on users.LanguageId equals languages.LanguageID
            //                      where users.UserID == userId && users.CompanyID == companyId
            //                      select languages.Description;

            return _efSerializer.EFSerialize(languageDetails);
        }
        #endregion

        // Connect to Active Direcotry to check for the setup
        public string ConnectActiveDirectory(string groupName, string domainName, string serverName, string adminUserName, string tapAdminUserName, string tapAdminPassword)
        {
            tap.dom.usr.Authentication _db = new tap.dom.usr.Authentication();
            return _db.ConnectAcitveDirectory(groupName, domainName, serverName, adminUserName, tapAdminUserName, tapAdminPassword);
        }

        //#region All User Password Encryption

        //public string EncryptDBPasswords()
        //{
        //    bool hasChanges = false;
        //    try
        //    {
        //        // Get all the existing users into a list
        //        IList<tap.dat.Users> userDetails = _db.Users.ToList();

        //        // Loop through the users list and Update the password and save the changes.
        //        foreach (tap.dat.Users usr in userDetails)
        //        {
        //            string test = _crypt.Decrypt(usr.Password);

        //            if (test.Contains("Invalid") || test.Contains("invalid") || test.Contains("valid"))
        //            {
        //                usr.Password = _crypt.Encrypt(usr.Password);
        //                hasChanges = true;
        //            }
        //        }

        //        if (hasChanges)
        //        {
        //            _db.SaveChanges();
        //            return hasChanges.ToString();
        //        }
        //        else
        //            return "done";               
        //    }
        //    catch (Exception)
        //    {
        //        return false.ToString();
        //    }           
        //}

        //#endregion


        #region Get User Full name based on UserID

        public string GetUserFullName(int userId)
        {
            var resultSet = _db.Users.Where(a => a.UserID == userId).FirstOrDefault();
            if (resultSet != null)
                return resultSet.FirstName + " " + resultSet.LastName + " (" + resultSet.Email + ")";
            else
                return string.Empty;
        }

        #endregion
    }
}
