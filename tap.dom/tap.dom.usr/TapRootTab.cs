﻿//---------------------------------------------------------------------------------------------
// File Name 	: TapRootTab.cs

// Date Created  : N/A

// Description   : Class is used for snapchart related Data
//---------------------------------------------------------------------------------------------


#region Namespace
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
#endregion

namespace tap.dom.usr
{
    public class TapRootTab
    {
        #region Global Variables
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        #endregion

        #region SaveSnapChart
        /// <summary>
        /// To save the investigation season(spring,Summer,Autumn or winter)
        /// </summary>
        /// <param name="snapChartId"></param>
        /// <param name="eventId"></param>
        /// <param name="seasonId"></param>
        /// <param name="isComplete"></param>
        /// <param name="snapChartData"></param>
        /// <returns></returns>
        public string SaveSnapChart(int snapChartId, string eventID, int seasonId, bool isComplete, string snapChartData, bool isReverse)
        {
            string message = string.Empty;
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                bool isNewSnapChart = true;
                tap.dat.SnapCharts snapChart = null;
                UpdateIsCurrentStatus(eventId);
                snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);

                if (snapChart != null)
                    isNewSnapChart = false;

                if (isNewSnapChart)
                {
                    snapChart = new tap.dat.SnapCharts();
                    message = "Inserted";
                }
                else
                {
                    //for deleting and recreating the snapchart
                    DeleteSnapCharts(eventId, seasonId);
                    message = "Updated";
                }

                snapChart.EventID = eventId;
                snapChart.SeasonID = seasonId;

                snapChart.IsComplete = isComplete;
                // snapChart.SnapChartData = snapChartData;

                snapChart.IsCurrent = (isReverse ? true : false);

                if (isNewSnapChart)
                    _db.AddToSnapCharts(snapChart);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return message;
        }
        #endregion

        #region SnapChartDataSelect
        /// <summary>
        /// get the snapchart data 
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public int SnapChartDataSelect(string eventID)
        {
            int seasonId = 0;

            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                tap.dat.SnapCharts snapChart = new dat.SnapCharts();
                var IsSnapChartExist = (_db.SnapCharts.FirstOrDefault(a => a.EventID == eventId) != null);

                if (IsSnapChartExist)
                {

                    seasonId = (int)_db.SnapCharts.FirstOrDefault(a => a.EventID == eventId).SeasonID;
                    //var snapChartData = (from a in _db.SnapCharts
                    //                     where a.EventID == eventId && a.SeasonID ==
                    //                     (
                    //                         from b in _db.SnapCharts
                    //                         where a.EventID == b.EventID && b.IsComplete == true
                    //                         select b.SeasonID).Max()
                    //                     select a).ToList();

                    //if (snapChartData != null && snapChartData.Count() > 0)
                    //{
                    //    seasonId = Convert.ToInt32(snapChartData.FirstOrDefault().SeasonID);

                    //    if (seasonId <= (int)(Enumeration.Season.Autumn))
                    //        seasonId++;
                    //}
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return seasonId;
        }
        #endregion

        #region SeasonDataSelect
        /// <summary>
        /// get the  seasons for investigation and audit
        /// </summary>
        /// <returns></returns>
        public List<tap.dat.Seasons> SeasonDataSelect()
        {
            List<tap.dat.Seasons> seasonList = _db.Seasons.ToList();
            return seasonList;
        }

        #endregion


        //public object GetSnapChart(string eventID)
        public string GetSnapChart(string eventID)
        {
            //string seasonData = "";
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                tap.dat.SnapCharts snapChart = new dat.SnapCharts();

                //Check for snapchart exists by this event id.
                var schart = (_db.SnapCharts.FirstOrDefault(a => a.EventID == eventId));
                
                if (schart != null)
                {
                    int sid = schart.SnapChartID;
                    //Check for causal factors exists for this snapchart id.
                    var isSC = _db.CausalFactors.FirstOrDefault(a => a.SnapChartID == sid);

                    //Get the causalfactors count which are not completed.
                    int? cfPendingCount = isSC != null ? PendingCausalFactorsGet(sid) : 1;

                    //return seasonData = schart.SeasonID.ToString() + "," + cfPendingCount;
                    return schart.SeasonID.ToString() + "," + cfPendingCount;

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return null;
        }

        //int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
        //tap.dat.SnapCharts snapChart = new dat.SnapCharts();
        //var IsSnapChartExist = (_db.SnapCharts.FirstOrDefault(a => a.EventID == eventId) != null);
        //if (IsSnapChartExist)
        //{

        //    //var rctCompleteCount =  _db.CausalFactors.Where(x => x.SnapChartID == snapChartId && (x.IsComplete == null || x.IsComplete == false)).ToList();
        //    var snapChartData = (from a in _db.SnapCharts
        //                         where a.EventID == eventId
        //                         select new
        //                         {
        //                             seasonId = a.SeasonID,
        //                             allRCTCompleted = 
        //                             (from x in _db.CausalFactors
        //                             where x.SnapChartID == a.SnapChartID && (x.IsComplete == null || x.IsComplete == false) select x).AsEnumerable().Count()
        //                         });

        //    return snapChartData;
        //}

        //    return null;
        //}

        #region Get the count of causalfactors which are not completed.

        /// <summary>
        /// Get the count of causalfactors which are not completed.
        /// </summary>
        /// <param name="sid">snapchart id</param>
        /// <returns></returns>
        /// 
        public int? PendingCausalFactorsGet(int sid)
        {
            return (from x in _db.CausalFactors where x.SnapChartID == sid && (x.IsComplete == null || x.IsComplete == false) select x).AsEnumerable().Count();
        }

        #endregion

        #region CompletedSnapCharSelect
        /// <summary>
        /// To get the snapchart records which are completed
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public List<tap.dat.SnapCharts> CompletedSnapChartSelect(string eventID)
        {
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            var snapCharts = (from a in _db.SnapCharts
                              where a.EventID == eventId && a.IsComplete == true
                              select a).ToList();

            return snapCharts;
        }

        #endregion

        #region DeleteSnapCharts
        /// <summary>
        /// Deleting and recreating the snapchart
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="seasonId"></param>
        public void DeleteSnapCharts(int eventId, int seasonId)
        {

            bool isSummer = false;
            bool isAutumn = false;

            switch (seasonId)
            {
                case (int)tap.dom.hlp.Enumeration.Season.Spring:
                    isSummer = true;
                    isAutumn = true;
                    break;

                case (int)tap.dom.hlp.Enumeration.Season.Summer:
                    isAutumn = true;
                    break;
            }

            if (isSummer)
            {
                tap.dat.SnapCharts snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == 2);
                if (snapChart != null)
                    DeleteSeason(snapChart);
            }

            if (isAutumn)
            {
                tap.dat.SnapCharts snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == 3);
                if (snapChart != null)
                    DeleteSeason(snapChart);
            }

        }
        #endregion

        #region UpdateIsCurrentStatus
        /// <summary>
        /// UpdateIsCurrentStatus
        /// </summary>
        public void UpdateIsCurrentStatus(int eventId)
        {
            var snapChartSpring = _db.SnapCharts.FirstOrDefault(a => (a.EventID == eventId && a.SeasonID == 1 && a.IsCurrent == true));

            var snapChartSummer = _db.SnapCharts.FirstOrDefault(a => (a.EventID == eventId && a.SeasonID == 2 && a.IsCurrent == true));

            var snapChartAutumn = _db.SnapCharts.FirstOrDefault(a => (a.EventID == eventId && a.SeasonID == 3 && a.IsCurrent == true));

            if (snapChartSpring != null)
                CurrentStatus(snapChartSpring);

            if (snapChartSummer != null)
                CurrentStatus(snapChartSummer);

            if (snapChartAutumn != null)
                CurrentStatus(snapChartAutumn);

        }
        #endregion

        #region UpdateStatus
        /// <summary>
        /// CurrentStatus
        /// </summary>
        /// <param name="snapChartData"></param>
        public void CurrentStatus(tap.dat.SnapCharts snapChartData)
        {
            snapChartData.IsCurrent = false;
            _db.SaveChanges();
        }
        #endregion

        #region DeleteSeason
        /// <summary>
        /// Delete  the SnapchartRecord
        /// </summary>
        /// <param name="snapChartData"></param>
        public void DeleteSeason(tap.dat.SnapCharts snapChartData)
        {
            _db.DeleteObject(snapChartData);
            _db.SaveChanges();
        }

        #endregion

    }
}
