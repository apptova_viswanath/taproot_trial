﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.DirectoryServices.Protocols;

namespace tap.dom.usr
{
    public enum UserInfo
    {
        Disabled = 0,
        Enabled = 1,
        NoUser = 2,
        EnabledWrongPassword = 3,
        DisabledWrongPassword = 4
    }

    public class Authentication
    {

        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _crypt = new gen.Cryptography();

        public const string TAPROOT = "Taproot";
        public const string CHECK_SETTINGS = "Could not communicate with Active Directory with the settings provided. Please check the settings~1";
        public const string CHECK_USERNAME_PASSWORD = "Could not communicate with Active Directory with the credentials provided. Please check the username and password.~1";
        public const string USER_ENABLED = "Active Directory is all set up! User was authenticated~0";
        public const string USER_DISABLED = "Active Directory is all set up! User was denied authentication~1";
        public const string LOGIN_FAILURE_BAD_CREDENTIALS = "Logon failure: unknown user name or bad password.";

        public const int GENERAL_LANGUAGE_ID = 16; // For English
        public string _domainName = string.Empty;
        public string _adminUser = string.Empty;
        public string _adminPassword = string.Empty;
        public string _serverName = string.Empty;
        public string _serverProtocol = string.Empty;
        public string _groupName = string.Empty;
        
        List<string> _listActiveUser = new List<string>();
        List<string> _listDisabledUser = new List<string>();

        public Authentication()
        {
            tap.dat.ActiveDirectorySettings adSettings = _db.ActiveDirectorySettings.FirstOrDefault();
            if (adSettings == null)
                return;

            _domainName = adSettings.DomainName;
            _adminUser = adSettings.AdminUserName;
            _adminPassword = adSettings.AdminPassword;
            _serverName = adSettings.ServerName;
            _serverProtocol = adSettings.ServerProtocol;
            _groupName = adSettings.GroupName;
        }

        /// <summary>
        /// Get the connection with AD
        /// </summary>
        public DirectoryEntry DirADConnection
        {
            get
            {
                DirectoryEntry connectionAD = new DirectoryEntry(_serverProtocol + _serverName, _domainName + "\\" + _adminUser, _crypt.Decrypt(_adminPassword));
                return connectionAD;
            }
        }

        public string isAuthenticateUser(string userName, string Password)
        {
            try
            {
                return CheckUserInADGroup(userName, Password);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }            
        }

        public string CheckUserInADGroup(string userName, string password)
        {
            try
            {
                //To check application contains custom security 'or' Active Directory security.
                bool isADAuthentication = GetCompanyActiveDirectorySecutiry(userName);
                if (!isADAuthentication)
                {
                    return "true";
                }

                //get user
                string userResult = CheckUserInActiveDirectory(userName, password);

                if (userResult != null)
                {
                    if (userResult == UserInfo.Enabled.ToString())
                    {
                        // Add user into Database and if exists, do nothing
                        ActiveUserInAD(userName, password);
    
                        // Update user if 
                        //UpdateUserActiveStatus(userName, true);
                        UpdateADUserDetailsInDatabase(userName, true);
                        return userResult;
                    }
                    else if (userResult == UserInfo.Disabled.ToString())
                    {
                        //UpdateUserActiveStatus(userName, false);
                        UpdateADUserDetailsInDatabase(userName, false);
                        return userResult;
                    }
                    else
                        return userResult;
                }
                else
                    return "false";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "UserInADGroup");
                return "false";
            }
        }

        //Check User exist in AD or not
        public string CheckUserInActiveDirectory(string userName, string password)
        {
            UserPrincipal foundUser = null;

            try
            {
                // Check with generic query user's credentials
                var domainContext = new PrincipalContext(ContextType.Domain, _serverName, _domainName + "\\" + _adminUser, _crypt.Decrypt(_adminPassword));
                foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName);
                GroupPrincipal group = GroupPrincipal.FindByIdentity(domainContext, "TaprooT");

                if (!foundUser.IsMemberOf(group))
                {
                    return null;
                }
                else{
                    if (!Convert.ToBoolean(foundUser.Enabled))
                        return UserInfo.Disabled.ToString();
                }

                // Check with user's credentials for password matching
                domainContext = new PrincipalContext(ContextType.Domain, _serverName, _domainName + "\\" + userName, password);
                foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName);

                if (Convert.ToBoolean(foundUser.Enabled))
                    return UserInfo.Enabled.ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("bad password") && Convert.ToBoolean(foundUser.Enabled))
                    return UserInfo.EnabledWrongPassword.ToString();
                else if (ex.Message.Contains("bad password") && !Convert.ToBoolean(foundUser.Enabled))
                    return UserInfo.DisabledWrongPassword.ToString();
                else
                    return null;
            }
            return null;
        }

        //Steps in DB
        public void ActiveUserInAD(string userName, string password)
        {
            try
            {
                var userDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);

                //if user is not in 'users' table then add in table
                if (userDetails == null)
                    ADUserNotInDB(userName, password);
                //else
                //{

                //    //If user active in AD group but not active in DB
                //    if (!userDetails.Active)
                //        UpdateUserInDB(userName, true);

                //    ////if user is admin then update AD list
                //    //if (userDetails.isAdministrator != null && userDetails.isAdministrator.Equals(true))
                //    //{
                //    //    ListUserInAD(DirADConnection);

                //    //    AddUsersAgainstAD(_listActiveUser, Convert.ToInt32(userDetails.CompanyID));

                //    //    DisabledUserAgainstAD(_listDisabledUser, Convert.ToInt32(userDetails.CompanyID));
                //    //}
                //}
            }

            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "ActiveUserInAD");
            }

        }

        public void ADUserNotInDB(string userName, string password)
        {
            int CompanyID = 0;
            //Get company Id based AD group to add user in DB
            //CompanyID = UserCompanyAD(userName);

            CompanyID = (from c in _db.Companies
                         where c.ParentId == null
                         select c.CompanyID).FirstOrDefault();

            //Add user in DB
            if (CompanyID > 0)
                AddUserInDB(userName, password, CompanyID);
        }
          
        //Add new user in DB with refrence of AD group
        public void AddUserInDB(string userName, string password, int companyID)
        {
            try
            {
                DirectorySearcher dssearch = new DirectorySearcher(DirADConnection);
                dssearch.Filter = "(SAMAccountName=" + userName + ")";
                SearchResult sresult = dssearch.FindOne();
                DirectoryEntry dsresult = sresult.GetDirectoryEntry();

                tap.dat.Users newUser = new tap.dat.Users();

                newUser.UserName = userName;
                //newUser.Password = password;
                newUser.CompanyID = companyID;
                newUser.Active = true;
                newUser.LastPasswordChange = DateTime.Now;
                newUser.LanguageId = GENERAL_LANGUAGE_ID;
                _db.AddToUsers(newUser);

                if (dsresult.Properties["givenName"] != null)
                newUser.FirstName = Convert.ToString(dsresult.Properties["givenName"][0]);
                if (dsresult.Properties["sn"] != null)
                    newUser.LastName = Convert.ToString(dsresult.Properties["sn"][0]);

                int isEnabled = Convert.ToInt32(dsresult.Properties["userAccountControl"][0]);
                newUser.Active = !Convert.ToBoolean(isEnabled & 0x0002);
                if(dsresult.Properties["mail"] != null)
                    newUser.Email = dsresult.Properties["mail"][0].ToString();
                if(dsresult.Properties["mobile"] != null)
                    newUser.PhoneNo = dsresult.Properties["mobile"][0].ToString();                
            }
            catch (Exception ex)
            {
            }
            _db.SaveChanges();
        }

        //List of active users in AD Group
        public void ActiveUsersInADGroup(DirectoryEntry de, string OrganizationalUnit)
        {
            DirectorySearcher search = new DirectorySearcher(de);
            search.PageSize = 1001;
            //search.Filter = "(&(objectClass=user)(!userAccountControl:1.2.840.113556.1.4.803:=2))";
            search.Filter = "(objectClass=user)";
            SearchResultCollection result = search.FindAll();
            if (result.Count > 0)
            {
                foreach (SearchResult item in result)
                {
                    if (item.Properties["SAMAccountName"].Count > 0
                            && Convert.ToString(item.Properties["distinguishedName"][0]).Contains("OU=" + OrganizationalUnit))
                        _listActiveUser.Add(item.Properties["sAMAccountName"][0].ToString());
                }
            }
        }

        //List of disabled users in AD Group
        public void DisabledUsersInADGroup(DirectoryEntry de, string OrganizationalUnit)
        {
            DirectorySearcher search = new DirectorySearcher(de);
            search.PageSize = 1001;
            search.Filter = "(&(objectClass=user)(userAccountControl:1.2.840.113556.1.4.803:=2))";
            SearchResultCollection result = search.FindAll();
            if (result.Count > 0)
            {
                foreach (SearchResult item in result)
                {
                    if (item.Properties["SamAccountName"].Count > 0
                        && Convert.ToString(item.Properties["distinguishedName"][0]).Contains("OU=" + OrganizationalUnit))
                        _listDisabledUser.Add(item.Properties["sAMAccountName"][0].ToString());
                }
            }
        }

        //Get company security type
        public bool GetCompanyActiveDirectorySecutiry(string userName)
        {
            bool isSecurityAD = false;
            try
            {
                var details = _db.Users.FirstOrDefault(a => a.UserName == userName);

                if (details != null)
                {
                    if (details.Password != null)
                    {
                        return isSecurityAD;
                    }

                    var companyDetails = _db.Companies.FirstOrDefault(a => a.CompanyID == details.CompanyID);
                    if (companyDetails != null && companyDetails.CheckADSecurity != null && companyDetails.CheckADSecurity == true)
                        isSecurityAD = (bool)companyDetails.CheckADSecurity;
                }
                else
                {
                    var companyDetails = _db.Companies.FirstOrDefault(a => a.ParentId == null);
                    if (companyDetails != null && companyDetails.CheckADSecurity != null && companyDetails.CheckADSecurity == true)
                        isSecurityAD = (bool)companyDetails.CheckADSecurity;
                }
            }
            catch (Exception ex) {

                tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "isSiteSecurityAD");
            }
            return isSecurityAD;
        }

        //Find out companyID through AD group
        public int UserCompanyAD(string userName)
        {
            int CompanyID = 0;
            
            try
            {
                DirectorySearcher dssearch = new DirectorySearcher(DirADConnection);
                dssearch.Filter = "(SAMAccountName=" + userName + ")";
                SearchResult sresult = dssearch.FindOne();
                DirectoryEntry dsresult = sresult.GetDirectoryEntry();
                string comp = dsresult.Properties["company"][0].ToString();
                if(comp != null && comp.Length>0)
                    CompanyID = GetCompanyID(comp);
            }
            catch (Exception)
            {
            }
            return CompanyID;
        }

        //Get companyID from DB
        public int GetCompanyID(string companyName)
        {
            int CompanyID = 0;

            var _companies = _db.Companies.FirstOrDefault(a => a.CompanyName == companyName && a.Active == true);
            if (_companies != null)
                CompanyID = _companies.CompanyID;

            return CompanyID;
        }

        public void AddUsersAgainstAD(List<string> users, int companyId)
        {
            var lists = (from u in _db.Users
                         where u.CompanyID == companyId
                         select u.UserName).ToList();

            bool isExist = false;
            if (lists == null)
                return;

            foreach(string item in users)
            {
                isExist = lists.Contains(item);
                 if (!isExist)
                     AddUserInDB(item, null, companyId);
            }
        }

        public void DisabledUserAgainstAD(List<string> tapRootDisabledUsers, int companyId)
        {
            var lists = (from u in _db.Users
                         where u.Active == true && u.CompanyID == companyId
                         select u).ToList();
            if (lists == null)
                return;
            bool isExist = false;
            foreach (var item in lists)
            {
                isExist = tapRootDisabledUsers.Contains(item.UserName);
                if (isExist)
                {
                    //DeactivateUserInDB(item.UserName);
                    UpdateUserActiveStatus(item.UserName, false);
                }
            }
        }

        /// <summary>
        /// Update User status in 'Users' custom table
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="status"></param>
        public void UpdateUserActiveStatus(string userName, bool status)
        {
            var userDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);
            if (userDetails != null)
            {
                userDetails.Active = status;
                _db.SaveChanges();
            }
        }

        public void UpdateADUserDetailsInDatabase(string userName, bool  isActive)
        {
            try
            {
                DirectorySearcher dssearch = new DirectorySearcher(DirADConnection);
                dssearch.Filter = "(SAMAccountName=" + userName + ")";
                SearchResult sresult = dssearch.FindOne();
                DirectoryEntry dsresult = sresult.GetDirectoryEntry();

                var userDetails = _db.Users.FirstOrDefault(a => a.UserName == userName);

                userDetails.Active = isActive;
                //if (dsresult.Properties["givenName"].Value != null)
                    userDetails.FirstName = Convert.ToString(dsresult.Properties["givenName"].Value);
                //if (dsresult.Properties["sn"].Value != null)
                    userDetails.LastName = Convert.ToString(dsresult.Properties["sn"].Value);
                //if (dsresult.Properties["mail"].Value != null)
                    userDetails.Email = Convert.ToString(dsresult.Properties["mail"].Value);
                //if (dsresult.Properties["mobile"].Value != null)
                    userDetails.PhoneNo = Convert.ToString(dsresult.Properties["mobile"].Value);   

                if(userDetails != null)
                {
                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                _db.SaveChanges();
            }
        }

        /// <summary>
        /// Check if provided settings and details are able to connect to Active Directory server
        /// </summary>
        /// <param name="groupName">Provide OUs with comma seperated values</param>
        /// <param name="domainName">domain of the computer</param>
        /// <param name="serverName">server address eg: google.com</param>
        /// <param name="adminUserName">Admin user to be created in the database</param>
        /// <param name="queryUserName">LDAP Query Username</param>
        /// <param name="queryPassword">LDAP Query Password</param>
        /// <returns>String with </returns>
        public string ConnectAcitveDirectory(string groupName, string domainName, string serverName, string adminUserName, string queryUserName, string queryPassword)
        {
            try
            {
                using (PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, serverName, queryUserName, queryPassword))
                {
                    using (UserPrincipal foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, adminUserName))
                    {
                        if (foundUser != null && foundUser.Enabled == true)
                            return USER_ENABLED;
                        else if (foundUser != null && foundUser.Enabled == false)
                            return USER_DISABLED;
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "ConnectAcitveDirectory");
                if (ex.Message.Contains(LOGIN_FAILURE_BAD_CREDENTIALS))
                {
                    return CHECK_USERNAME_PASSWORD;
                }
                return CHECK_SETTINGS;

            }
            return CHECK_SETTINGS;            
        }


        /// <summary>
        /// Method to query through AD server using ldap querying
        /// </summary>
        /// <param name="server"></param>
        /// <param name="AdminUser"></param>
        /// <param name="adminUserName"></param>
        /// <param name="adminPassword"></param>
        /// <param name="domain"></param>
        /// <param name="groupName"></param>
        protected UserInfo CheckUserInActiveDirectory_New(string adminUserName, string adminPassword)
        {   
            string ldapSearchFilter2 = string.Empty;

            if(_groupName.Split(',').Length > 0)
            {
                for (int i = 0; i < _groupName.Split(',').Length; i++)
                {
                    ldapSearchFilter2 += "OU=" + _groupName.Split(',')[i] + ",";
                }
            }

            string ldapQueryOptions = "CN=Users," + ldapSearchFilter2 + "DC=i3siitest,DC=com";
            ldapQueryOptions = ldapSearchFilter2 + "DC=i3siitest,DC=com";

            // For Specific User
            string ldapSearchFilter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + adminUserName + "))";
            // For all User
            //string ldapSearchFilter = "(&(objectCategory=Person)(objectClass=User))";
            // For Gropus
            //string ldapSearchFilter = "(&((&(objectCategory=group))))";
            // For OU's
            //string ldapSearchFilter = "(&((&(objectCategory=organizationalUnit))))";

            try
            {   
                var credential = new NetworkCredential(adminUserName, adminPassword, _domainName);
                
                // Create the new LDAP connection
                var ldapConnection = new LdapConnection(_serverName);
                ldapConnection.Credential = credential;

                SearchRequest searchRequest = new SearchRequest(ldapQueryOptions, ldapSearchFilter, System.DirectoryServices.Protocols.SearchScope.Subtree, null);

                // cast the returned directory response as a SearchResponse object
                SearchResponse searchResponse = (SearchResponse)ldapConnection.SendRequest(searchRequest);

                if (searchResponse.Entries.Count > 0)
                {
                    // enumerate the entries in the search response
                    foreach (SearchResultEntry entry in searchResponse.Entries)
                    {
                        int userEnableFlag = Convert.ToInt32(entry.Attributes["userAccountControl"]);
                        bool isUserEnabled = !Convert.ToBoolean(userEnableFlag & 0x0002);

                        if (isUserEnabled)
                            return UserInfo.Enabled;
                        else if (!isUserEnabled)
                            return UserInfo.Disabled;
                    }
                }

                return UserInfo.EnabledWrongPassword;
            }
            catch (Exception ex)
            {
                //tap.dom.hlp.ErrorLog.LogException(ex, "Authentication.cs", "DirectoryEntry");
                if (ex.Message.Contains("invalid"))
                    return UserInfo.DisabledWrongPassword;

                return UserInfo.NoUser;
            }
        }

    }
}
