﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.usr
{
    class EventDetail
    {
        private int _priority;
        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }


        private int _eventID;
        public int EventID
        {
            get { return _eventID; }
            set { _eventID = value; }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        
        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }


        
        private string _eventType;
        public string EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        private int? _moduleID;
        public int? ModuleID
        {
            get { return _moduleID; }
            set { _moduleID = value; }
        }

        private string _eventURL;
        public string EventURL
        {
            get { return _eventURL; }
            set { _eventURL = value; }
        }

         private string _createdBy;
        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private DateTime? _eventDate;
        public DateTime? EventDate
        {
            get { return _eventDate; }
            set { _eventDate = value; }
        }

        private DateTime? _dateCreated;
        public DateTime? DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }


         private Nullable<TimeSpan> _eventTime;
         public Nullable<TimeSpan> EventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        
         private DateTime? _dateModified;
        public DateTime? DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        

    }
}
