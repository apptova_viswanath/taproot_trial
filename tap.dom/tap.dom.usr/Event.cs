﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using tap.dom.adm;
using tap.dom.hlp;
using System.Configuration;
using tap.dom.gen;

namespace tap.dom.usr
{
    public class Event
    {
        DateFormat dt = new DateFormat();
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        tap.dom.tab.EventTabs _eventTab = new tab.EventTabs();
        tap.dom.adm.SiteSettings _siteSetting = new SiteSettings();
        tap.dom.gen.Cryptography _cryptography = new Cryptography();

        tap.dom.hlp.Enumeration.Modules _mainModuleId;

        CultureInfo _provider = CultureInfo.InvariantCulture;
        string _dateFormat = "d";
        string _virtualDirectory = ConfigurationManager.AppSettings["VirtualLocation"];
        DateTime _eventDateTime = new DateTime();
        string _isNewEvent = string.Empty;
        int _companyId = 0;
        int _userId = 0;

        public const string TAPROOT_TAB_NAME = "TapRooT®";
        public const string CAP_TAB_NAME = "Corrective Actions";
        public const string FIX_TAB_NAME = "Fix";
        public const string FIX_TAB_DISPLAY_NAME = "Fix";
        public const string DETAILS_TAB_NAME = "Details";
        public const string TAPROOT_TAB_DISPLAY_NAME = "TapRooT";
        public const string CAP_TAB_DISPLAY_NAME = "CorrectiveActions";


        #region Home grid operations

        /// <summary>
        /// EventDetails for home page grid operations
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sidx"></param>
        /// <param name="sord"></param>
        /// <param name="filter"></param>
        /// <param name="basicSearch"></param>
        /// <param name="userID"></param>
        /// <param name="advanceSearch"></param>
        /// <param name="companyID"></param>
        /// <returns>json string</returns>

        public string EventDetails(int page, int rows, string sidx, string sord, string filter, string basicSearch, string advanceSearch,
            string userID, string companyID, string baseURL)//Added bssearch/loginuser/advsearch
        {

            string userName = string.Empty;
            string eventsList = string.Empty;
            string eveTime = string.Empty;

          
            //Decrypt the user Id and pass
            int loginUser = Convert.ToInt32(userID);

            if (loginUser != 0 && loginUser != -1)
                userName = _db.Users.FirstOrDefault(x => x.UserID == loginUser).UserName;

            int _compId = Convert.ToInt32(companyID);
            string strDateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == _compId).DateFormat;

            string fullName = _db.Users.Where(a => a.UserID == loginUser && a.CompanyID == _compId).FirstOrDefault().FirstName + " " +
                                                  _db.Users.Where(a => a.UserID == loginUser && a.CompanyID == _compId).FirstOrDefault().LastName;


            IEnumerable<EventDetail> evntDetails = null;

            int skipRowsCount = 0;
            if (page != 1)
                skipRowsCount = (page - 1) * rows;

            var eventDetails = (from evt in _db.Events where (evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true)))) select evt).AsEnumerable();
            if (eventDetails.Count() > 0)
            {


                #region Linq query to fetch the records from DB

                if (advanceSearch == "-1" && filter == "-1" && basicSearch == "-1")
                {
                    //Get all active events based on login user id.
                    evntDetails = (from evt in _db.Events
                                   join inc in _db.Incidents on evt.EventID equals inc.EventID into temIncs
                                   from incs in temIncs.DefaultIfEmpty()
                                   join inv in _db.Investigations on evt.EventID equals inv.EventID into temInvs
                                   from invs in temInvs.DefaultIfEmpty()
                                   join aud in _db.Audits on evt.EventID equals aud.EventID into temAuds
                                   from auds in temAuds.DefaultIfEmpty()
                                   join cap in _db.CorrectiveActionPlans on evt.EventID equals cap.EventID into temCaps
                                   from caps in temCaps.DefaultIfEmpty()
                                   join mod in _db.Modules on evt.ModuleID equals mod.ModuleID

                                   where (evt.CreatedBy == loginUser ||
                                       //(_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit ==true || f.hasView==true)))
                                   (from tm in _db.TeamMembers
                                    where
                                        tm.UserID == loginUser && tm.EventID == evt.EventID &&
                                        tm.hasEdit == true ||
                                        tm.hasView == true
                                    select new
                                    {
                                        tm.EventID
                                    }).Contains(new { EventID = (Int32?)evt.EventID })

                                   )

                                   &&
                                   evt.Status == (int)tap.dom.hlp.Enumeration.EventStatus.Active


                                   select new EventDetail
                                   {
                                       EventID = evt.EventID,
                                       Name = evt.Name,
                                       EventType = mod.ModuleName,
                                       CreatedBy = fullName,//_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                       EventDate = (evt.ModuleID == 1) ? incs.IncidentDatetime : (evt.ModuleID == 2) ? (incs.IncidentDatetime == null) ? invs.InvestigationDatetime : incs.IncidentDatetime : (evt.ModuleID == 3) ? auds.AuditDatetime : (incs.IncidentDatetime == null) ? auds.AuditDatetime : incs.IncidentDatetime,
                                       DateCreated = evt.DateCreated,
                                       EventTime = (evt.ModuleID == 1) ? (TimeSpan)incs.IncidentTime : (evt.ModuleID == 2) ? (TimeSpan)invs.InvestigationTime : (evt.ModuleID == 3) ? (TimeSpan)auds.AuditTime : (TimeSpan)caps.CapTime,
                                       DateModified = evt.DateModified,
                                       Status = (evt.Status == 1) ? "Active" : (evt.Status == 2) ? "Archived" : (evt.Status == 3) ? "Completed" : string.Empty,
                                       ModuleID = evt.ModuleID,
                                   }).OrderByDescending(e => e.DateCreated).Skip(skipRowsCount).Take(rows)
                                  .AsEnumerable().DefaultIfEmpty()
                                    .Select(p => new EventDetail
                                    {
                                        EventID = p.EventID,
                                        Name = p.Name,
                                        EventType = p.EventType,
                                        CreatedBy = p.CreatedBy,
                                        EventDate = p.EventDate,
                                        DateCreated = GetEventCreatedDate(p.DateCreated),
                                        EventTime = p.EventTime,
                                        DateModified = p.DateModified,
                                        Status = p.Status,
                                        ModuleID = p.ModuleID,
                                    }).DefaultIfEmpty();
                }
                else if (advanceSearch != "-1" || basicSearch != "-1")
                {

                    evntDetails = (from evt in _db.Events
                                   join incident in _db.Incidents on evt.EventID equals incident.EventID into temIncs
                                   from incident in temIncs.DefaultIfEmpty()
                                   join investigation in _db.Investigations on evt.EventID equals investigation.EventID into temInvs
                                   from investigation in temInvs.DefaultIfEmpty()
                                   join aud in _db.Audits on evt.EventID equals aud.EventID into temAuds
                                   from auds in temAuds.DefaultIfEmpty()
                                   join cap in _db.CorrectiveActionPlans on evt.EventID equals cap.EventID into temCaps
                                   from caps in temCaps.DefaultIfEmpty()
                                   join mod in _db.Modules on evt.ModuleID equals mod.ModuleID

                                   where (evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true))))
                                   select new EventDetail
                                   {
                                       EventID = evt.EventID,
                                       Name = evt.Name,
                                       EventType = mod.ModuleName,
                                       CreatedBy = fullName, //_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                       EventDate = (evt.ModuleID == 1) ? incident.IncidentDatetime : (evt.ModuleID == 2) ? (investigation.InvestigationDatetime != null) ? investigation.InvestigationDatetime : incident.IncidentDatetime : (evt.ModuleID == 3) ? auds.AuditDatetime : (incident.IncidentDatetime == null) ? auds.AuditDatetime : incident.IncidentDatetime,
                                       DateCreated = evt.DateCreated,
                                       EventTime = (evt.ModuleID == 1) ? (TimeSpan)incident.IncidentTime : (evt.ModuleID == 2) ? (TimeSpan)investigation.InvestigationTime : (evt.ModuleID == 3) ? (TimeSpan)auds.AuditTime : (TimeSpan)caps.CapTime,
                                       DateModified = evt.DateModified,
                                       Status = (evt.Status == 1) ? "Active" : (evt.Status == 2) ? "Archived" : (evt.Status == 3) ? "Completed" : string.Empty,
                                       ModuleID = evt.ModuleID,
                                   }).DefaultIfEmpty().AsEnumerable().Select(p => new EventDetail
                                   {
                                       EventID = p.EventID,
                                       Name = p.Name,
                                       EventType = p.EventType,
                                       CreatedBy = p.CreatedBy,
                                       EventDate = p.EventDate,
                                       DateCreated = GetEventCreatedDate(p.DateCreated),
                                       EventTime = p.EventTime,
                                       DateModified = p.DateModified,
                                       Status = p.Status,
                                       ModuleID = p.ModuleID,
                                   });
                }

                #endregion


                #region Event types filter

                if (filter != "-1")
                {

                    if (filter.Trim() == "UninvestigatedIncidents")// For only Incidents not the Investigations
                    {
                        // Get the events created by this current user and ModuleID == 1
                        var Events = _db.Events.Where(a => (a.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == a.EventID && (f.hasEdit == true || f.hasView == true)))) && a.ModuleID == 1).ToList();
                        int incidentsCount = 0;
                        int userscount = 0;
                        // iterate through all the events
                        foreach (tap.dat.Events evt in Events)
                        {
                            // find the number of incidents with eventId matching
                            incidentsCount = incidentsCount + _db.Incidents.Where(a => a.EventID == evt.EventID).ToList().Count();
                            if (_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName != null)
                            {
                                userscount++;
                            }
                            if (incidentsCount != 0 && userscount != 0)
                            {
                                break;
                            }
                        }
                        // If the number of incidents is zero or users are empty, then return
                        if (incidentsCount == 0 || userscount == 0)
                        {
                            return eventsList;
                        }

                        evntDetails = (from evt in _db.Events
                                       join incident in _db.Incidents on evt.EventID equals incident.EventID
                                       where ((evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true)))) && evt.ModuleID == 1)
                                       select new EventDetail
                                       {
                                           EventID = evt.EventID,
                                           Name = evt.Name,
                                           EventType = "Incident",
                                           CreatedBy = fullName, //_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                           EventDate = incident.IncidentDatetime,
                                           DateCreated = evt.DateCreated,
                                           EventTime = (TimeSpan)incident.IncidentTime,
                                           DateModified = evt.DateModified,
                                           ModuleID = evt.ModuleID,
                                       }).DefaultIfEmpty()
                                       .OrderByDescending(e => e.DateCreated).Skip(skipRowsCount).Take(rows).DefaultIfEmpty()
                                       .AsEnumerable().Select(p => new EventDetail
                                       {
                                           EventID = p.EventID,
                                           Name = p.Name,
                                           EventType = p.EventType,
                                           CreatedBy = p.CreatedBy,
                                           EventDate = p.EventDate,
                                           DateCreated = GetEventCreatedDate(p.DateCreated),
                                           EventTime = p.EventTime,
                                           DateModified = p.DateModified,
                                           ModuleID = p.ModuleID,

                                       });
                    }

                    if (filter.Trim() == "Investigations") //For Investigations and CAP
                    {
                        // If there are no investigations, need to return
                        // Find Events created by this user
                        var Events = _db.Events.Where(a => (a.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == a.EventID && (f.hasEdit == true || f.hasView == true))))).ToList();
                        int incidentsCount = 0;
                        int investigationsCount = 0;
                        int usersCount = 0;
                        // iterate through all the events
                        foreach (tap.dat.Events evt in Events)
                        {
                            // Find all the incidents and investigation matching EventID
                            incidentsCount = incidentsCount + _db.Incidents.Where(a => a.EventID == evt.EventID).ToList().Count();
                            investigationsCount = investigationsCount + _db.Investigations.Where(a => a.EventID == evt.EventID).ToList().Count();
                            if (_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName != null)
                            {
                                usersCount++;
                            }
                            if (incidentsCount != 0 && investigationsCount != 0 && usersCount != 0)
                            {
                                break; // atleast one record exists, so exit the loop
                            }

                        }
                        // No records, so return empty string
                        if (incidentsCount == 0 || investigationsCount == 0 || usersCount == 0)
                        {
                            return eventsList;
                        }

                        evntDetails = (from evt in _db.Events
                                       join inv in _db.Investigations on evt.EventID equals inv.EventID
                                       join inc in _db.Incidents on inv.EventID equals inc.EventID
                                       where (evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true))))
                                       select new EventDetail
                                       {
                                           EventID = evt.EventID,
                                           Name = evt.Name,
                                           EventType = evt.ModuleID == 2 ? "Investigation" : "Action Plan",
                                           CreatedBy = fullName, //_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                           EventDate = inc.IncidentDatetime,
                                           DateCreated = evt.DateCreated,
                                           EventTime = (TimeSpan)inc.IncidentTime,
                                           DateModified = evt.DateModified,
                                           ModuleID = evt.ModuleID,

                                       }).OrderByDescending(e => e.DateCreated).Skip(skipRowsCount).Take(rows)
                                           .DefaultIfEmpty().AsEnumerable().Select(inv => new EventDetail
                                           {
                                               EventID = inv.EventID,
                                               Name = inv.Name,
                                               EventType = inv.EventType,
                                               CreatedBy = inv.CreatedBy,
                                               EventDate = inv.EventDate,
                                               DateCreated = GetEventCreatedDate(inv.DateCreated),
                                               EventTime = inv.EventTime,
                                               DateModified = inv.DateModified,
                                               ModuleID = inv.ModuleID,
                                           });
                    }

                    if (filter.Trim() == "Audits") //For Audits and CAP
                    {
                        // If there are no Audits, need to return
                        int eventsCount = _db.Audits.Where(a => a.CreatedBy == loginUser).ToList().Count();
                        var Events = _db.Events.Where(a => (a.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == a.EventID && (f.hasEdit == true || f.hasView == true))))).ToList();
                        int usersCount = 0;
                        // Find users exist or not
                        foreach (tap.dat.Events evt in Events)
                        {
                            if (_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName != null)
                            {
                                usersCount++;
                            }
                            if (usersCount != 0)
                            {
                                break;
                            }
                        }
                        if (eventsCount == 0 || usersCount == 0)
                        {
                            return eventsList;
                        }

                        evntDetails = (from evt in _db.Events
                                       join audit in _db.Audits on evt.EventID equals audit.EventID
                                       where (evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true))))
                                       select new EventDetail
                                       {
                                           EventID = evt.EventID,
                                           Name = evt.Name,
                                           EventType = (evt.ModuleID == 4) ? "Action Plan" : "Audit",
                                           CreatedBy = fullName, //_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                           EventDate = audit.AuditDatetime,
                                           DateCreated = evt.DateCreated,
                                           EventTime = (TimeSpan)audit.AuditTime,
                                           DateModified = evt.DateModified,
                                           ModuleID = evt.ModuleID,

                                       }).OrderByDescending(e => e.DateCreated).Skip(skipRowsCount).Take(rows)
                                           .DefaultIfEmpty().AsEnumerable().Select(aud => new EventDetail
                                           {
                                               EventID = aud.EventID,
                                               Name = aud.Name,
                                               EventType = aud.EventType,
                                               CreatedBy = aud.CreatedBy,
                                               EventDate = aud.EventDate,
                                               DateCreated = GetEventCreatedDate(aud.DateCreated),
                                               EventTime = aud.EventTime,
                                               DateModified = aud.DateModified,
                                               ModuleID = aud.ModuleID,
                                           });
                    }

                    if (filter.Trim() == "ActionPlans")  //For Corrective Action Plans
                    {
                        // Find all the events created by this user
                        var Events = _db.Events.Where(a => (a.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == a.EventID && (f.hasEdit == true || f.hasView == true))))).ToList();
                        int capCount = 0;
                        int incidentsCount = 0;
                        int auditCount = 0;
                        int usersCount = 0;
                        // iterate through all the events
                        foreach (tap.dat.Events evt in Events)
                        {
                            // Find all the CAPs, Incidents and Audits matching EventID
                            capCount = capCount + _db.CorrectiveActionPlans.Where(a => a.EventID == evt.EventID).ToList().Count();
                            incidentsCount = incidentsCount + _db.Incidents.Where(a => a.EventID == evt.EventID).ToList().Count();
                            auditCount = auditCount + _db.Audits.Where(a => a.EventID == evt.EventID).ToList().Count();
                            if (_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName != null)
                            {
                                usersCount++;
                            }
                            if (capCount != 0 && incidentsCount != 0 && auditCount != 0 && usersCount != 0)
                            {
                                break; // atleast one record exists, so exit the loop
                            }

                        }
                        // No records, so return empty string
                        if (capCount == 0 || incidentsCount == 0 || auditCount == 0 || usersCount == 0)
                        {
                            return eventsList;
                        }

                        // If there are no CorrectiveActionPlans, need to return
                        int eventsCount = _db.CorrectiveActionPlans.Where(a => a.CreatedBy == loginUser).ToList().Count();
                        if (eventsCount == 0)
                        {
                            return eventsList;
                        }
                        evntDetails = (from evt in _db.Events
                                       join caplan in _db.CorrectiveActionPlans on evt.EventID equals caplan.EventID
                                       join inc in _db.Incidents on caplan.EventID equals inc.EventID into tempIncs
                                       from incs in tempIncs.DefaultIfEmpty()
                                       join aud in _db.Audits on caplan.EventID equals aud.EventID into tempsAuds
                                       from auds in tempsAuds.DefaultIfEmpty()
                                       where (evt.CreatedBy == loginUser || (_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == evt.EventID && (f.hasEdit == true || f.hasView == true))))
                                       select new EventDetail
                                       {
                                           EventID = evt.EventID,
                                           Name = evt.Name,
                                           EventType = "Action Plan",
                                           CreatedBy = fullName, //_db.Users.FirstOrDefault(x => x.UserID == evt.CreatedBy).UserName,
                                           EventDate = (incs.IncidentDatetime == null) ? auds.AuditDatetime : incs.IncidentDatetime,
                                           DateCreated = evt.DateCreated,
                                           EventTime = (TimeSpan)caplan.CapTime,
                                           DateModified = evt.DateModified,
                                           ModuleID = evt.ModuleID,

                                       }).OrderByDescending(e => e.DateCreated).Skip(skipRowsCount).Take(rows)
                                           .DefaultIfEmpty().AsEnumerable().Select(cap => new EventDetail
                                           {
                                               EventID = cap.EventID,
                                               Name = cap.Name,
                                               EventType = cap.EventType,
                                               CreatedBy = cap.CreatedBy,
                                               EventDate = cap.EventDate,
                                               DateCreated = GetEventCreatedDate(cap.DateCreated),
                                               EventTime = cap.EventTime,
                                               DateModified = cap.DateModified,
                                               ModuleID = cap.ModuleID,
                                           });
                    }

                }

                #endregion

                #region  Advance search,Basic search and Filters

                if (advanceSearch != "-1" || basicSearch != "-1") //remove filter condition
                {

                    //Basic Search
                    if (basicSearch != "-1")//Search in only active events.
                        evntDetails = evntDetails.Where(a => a.Name.IndexOf(basicSearch, StringComparison.OrdinalIgnoreCase) != -1 && a.Status == "Active");

                    //Advance search
                    if (advanceSearch != "-1")
                    {
                        //search scope filter
                        List<string> statusList = new List<string>();
                        List<string> eventTypeList = new List<string>();
                        List<string> eventDateList = new List<string>();

                        var inc = false;
                        var inv = false;
                        var audit = false;
                        var action = false;

                        var incDtPeriod = string.Empty;

                        DateTime fromDate = DateTime.Parse("01/01/1900");
                        DateTime toDate = DateTime.Parse("01/01/2100");


                        string strAdvearch = advanceSearch;
                        string[] fldArray = strAdvearch.Split(',');

                        for (int i = 0; i < fldArray.Length; i++)
                        {
                            string[] valArray = fldArray[i].Split('#');

                            if ((valArray[0] == "chkinc" || valArray[0] == "chkinv" || valArray[0] == "chkaudit" || valArray[0] == "chkaction") && (valArray[1] == "1")) //chkinv
                            {
                                if (valArray[0] == "chkinc")
                                {
                                    inc = true;
                                }
                                if (valArray[0] == "chkinv")
                                {
                                    inv = true;
                                }
                                if (valArray[0] == "chkaudit")
                                    audit = true;
                                if (valArray[0] == "chkaction")
                                    action = true;
                            }

                            else if ((valArray[0] == "isIncdt30" && valArray[1] == "1") || (valArray[0] == "chkincdt60" && valArray[1] == "1") || (valArray[0] == "chkIncDateBetween" && valArray[1] == "1"))
                            {
                                incDtPeriod = ((valArray[0] == "isIncdt30") ? "30" : (valArray[0] == "chkincdt60") ? "60" : "BetweenDate");
                            }
                            else if (valArray[0] == "fromdate" && valArray[1] != "-1")
                            {
                                fromDate = Convert.ToDateTime(valArray[1]);
                            }
                            else if (valArray[0] == "todate" && valArray[1] != "-1")
                            {
                                toDate = Convert.ToDateTime(valArray[1]);
                            }

                            else if (valArray[0] == "status" && valArray[1] != "-1")
                            {
                                if (valArray[1] == "Active")
                                    statusList.Add(tap.dom.hlp.Enumeration.EventStatus.Active.ToString());

                                if (valArray[1] == "Completed")
                                    statusList.Add(tap.dom.hlp.Enumeration.EventStatus.Completed.ToString());

                                if (valArray[1] == "Archived")
                                    statusList.Add(tap.dom.hlp.Enumeration.EventStatus.Archived.ToString());
                            }
                        }
                        //date filter
                        if (inc)
                        {
                            eventTypeList.Add(tap.dom.hlp.Enumeration.Modules.Incident.ToString());
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Incident.ToString());
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Investigation.ToString());
                            eventDateList.Add("Action Plan");
                            eventTypeList.Add("Action Plan");

                            if (!audit && !inv)
                                evntDetails = evntDetails.Where(a => _db.Incidents.Any(f => f.EventID == a.EventID));
                            else if (!audit)
                                evntDetails = evntDetails.Where(a => !_db.Audits.Any(f => f.EventID == a.EventID));
                        }
                        if (inv)
                        {
                            eventTypeList.Add(tap.dom.hlp.Enumeration.Modules.Investigation.ToString());
                            //Hard coded here , get it from enumeration                            
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Investigation.ToString());
                            eventDateList.Add("Action Plan");
                            eventTypeList.Add("Action Plan");

                            if (!audit && !inc)
                                evntDetails = evntDetails.Where(a => _db.Investigations.Any(f => f.EventID == a.EventID));
                            else if (!audit)
                                evntDetails = evntDetails.Where(a => !_db.Audits.Any(f => f.EventID == a.EventID));
                        }
                        if (audit)
                        {
                            eventTypeList.Add(tap.dom.hlp.Enumeration.Modules.Audit.ToString());
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Audit.ToString());
                            //Hard coded here , get it from enumeration.                            
                            eventDateList.Add("Action Plan");
                            eventTypeList.Add("Action Plan");

                            if (!inv && !action)
                                evntDetails = evntDetails.Where(a => _db.Audits.Any(f => f.EventID == a.EventID));

                        }
                        if (action)
                        {
                            eventDateList.Add("Action Plan");
                            eventTypeList.Add("Action Plan");
                        }

                        if ((!inc && !audit) && (incDtPeriod != string.Empty))
                        {
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Incident.ToString());
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Investigation.ToString());
                            eventDateList.Add(tap.dom.hlp.Enumeration.Modules.Audit.ToString());
                        }


                        if (statusList.Count() != 0)
                            evntDetails = evntDetails.Where(a => statusList.Contains(a.Status));

                        if (eventTypeList.Count() != 0)
                        {
                            evntDetails = evntDetails.Where(a => eventTypeList.Contains(a.EventType));
                        }
                        if (eventDateList.Count() != 0 && incDtPeriod != string.Empty)
                        {
                            if (incDtPeriod == "BetweenDate")
                            {
                                evntDetails = evntDetails.Where(a => eventDateList.Contains(a.EventType) && (a.EventDate >= fromDate && a.EventDate <= toDate));
                            }
                            else
                            {
                                toDate = DateTime.ParseExact(DateTime.Now.Date.ToString("MM/dd/yyyy").Replace("-", "/"), _dateFormat, _provider).AddDays(1);
                                fromDate = ((incDtPeriod == "30") ? DateTime.ParseExact(DateTime.Now.Date.ToString("MM/dd/yyyy").Replace("-", "/"), _dateFormat, _provider).AddDays(-30) : DateTime.ParseExact(DateTime.Now.Date.ToString("MM/dd/yyyy").Replace("-", "/"), _dateFormat, _provider).AddDays(-60));
                                evntDetails = evntDetails.Where(a => eventDateList.Contains(a.EventType) && (a.EventDate >= fromDate && a.EventDate < toDate));
                            }
                        }

                    }

                }

                #endregion

                #region Sorting the events

                //Sorting Filter
                switch (sidx)
                {
                    case "Name":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.Name) : evntDetails.OrderBy(e => e.Name));
                        break;

                    case "EventType":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.EventType).ThenByDescending(e => e.DateModified) : evntDetails.OrderBy(e => e.EventType).ThenByDescending(e => e.DateModified));
                        break;

                    case "CreatedBy":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.CreatedBy).ThenByDescending(e => e.DateModified) : evntDetails.OrderBy(e => e.CreatedBy).ThenByDescending(e => e.DateModified));
                        break;

                    case "EventDate":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.EventDate).ThenByDescending(e => e.DateModified) : evntDetails.OrderBy(e => e.EventDate).ThenByDescending(e => e.DateModified));//show latest created event (DateCreated-today's Date)
                        break;

                    case "Status":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.Status) : evntDetails.OrderBy(e => e.Status));
                        break;

                    case "DateModified":
                        evntDetails = ((sord.Equals("desc")) ? evntDetails.OrderByDescending(e => e.DateModified) : evntDetails.OrderBy(e => e.DateModified));
                        break;
                }

                #endregion

                #region To Add filter opeartion

                int totalRecords;
                var pageSize1 = rows.ToString() ?? "15";
                int pageSize = int.Parse(pageSize1);

                if (advanceSearch != "-1" || basicSearch != "-1" || filter != "-1")
                    totalRecords = evntDetails.Count();
                else

                    totalRecords = _db.Events.Where(a => (a.CreatedBy == loginUser ||
                        //(_db.TeamMembers.Any(f => f.UserID == loginUser && f.EventID == a.EventID && (f.hasEdit == true || f.hasView == true)))
                          (from tm in _db.TeamMembers
                           where
                               tm.UserID == loginUser && tm.EventID == a.EventID &&
                               tm.hasEdit == true ||
                               tm.hasView == true
                           select new
                           {
                               tm.EventID
                           }).Contains(new { EventID = (Int32?)a.EventID })


                        )

                        && a.Status == (int)tap.dom.hlp.Enumeration.EventStatus.Active).Count();

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var currentPage1 = page.ToString() ?? "1";
                if (totalRecords <= 50)
                    currentPage1 = "1";
                int currentPageNumber = int.Parse(currentPage1);

                #endregion

                if (currentPageNumber > 1 && (advanceSearch != "-1" || basicSearch != "-1" || filter != "-1"))//IS any search other than first page            
                    evntDetails = evntDetails.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();

                #region Json data to display in home page grid

                //int detailTabId = 1;
                int companyId = 0;
                int.TryParse(companyID, out companyId);

                //if(!string.IsNullOrEmpty(companyID))
                //    detailTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;


                var evDetails = evntDetails.AsEnumerable().Select(x => new
                {
                    EventID = x.EventID,
                    EventName = x.Name,
                    EventType = x.EventType,
                    EventURL = GetEventURL(x.Name, x.ModuleID, x.EventID, companyId, baseURL),
                    EventDate = GetEventDate(x.EventDate, companyID),
                    DateModified = LastModifiedDaysCalculate(x.DateModified, strDateFormat),
                    CreatedBy = x.CreatedBy
                });

                var jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPageNumber,
                    records = totalRecords,
                    rows = (
                        from ev in evDetails
                        //let EventDate = GetEventDate(ev.EventDate,companyID) 
                        //let DateModified = LastModifiedDaysCalculate(ev.DateModified, strDateFormat)
                        //let EventURL = GetEventURL(ev.Name, ev.ModuleID, ev.EventID, companyId, baseURL)

                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = ev.EventID.ToString() + "," + ev.EventType.ToString(),
                            cell = new string[] {                            
                            "<a href='" + ev.EventURL + "'>" + ev.EventName + "</a>",
                            ev.EventType,                       
                            ev.CreatedBy.ToString(),
                            ev.EventDate.ToString(),
                            ev.DateModified 
                            }
                        }).ToArray()
                };

                #endregion

                return _efSerializer.EFSerialize(jsonData);

            }
            else
            {
                return null;
            }
        }
        #endregion

        #region Convertion from UTC to local time

        /// <summary>
        /// Get the local tim ezone info
        /// </summary>
        /// <param name="createdDate"></param>
        /// <returns></returns>
        public DateTime GetEventCreatedDate(object createdDate)
        {
            DateTime evtDate = Convert.ToDateTime(createdDate);
            return evtDate;
                //TimeZoneInfo.ConvertTimeFromUtc(evtDate, TimeZoneInfo.Local);
        }

        #endregion

        #region To Get Event Count Added on 21-may-2012
        /// <summary>
        /// Invoked To count the number of incident,Investigation,Audit and Cap
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string EventCount(string userID)
        {
            int userId = Convert.ToInt32(userID);

            var evntDetails = (from a in _db.Events
                               join b in _db.Incidents
                               on a.EventID equals b.EventID
                               select new
                               {
                                   EventID = a.EventID,
                                   Name = a.Name,
                                   EventType = "Incident",

                                   CreatedBy = a.CreatedBy,
                                   EventDate = a.EventDate,

                                   DateModified = a.DateModified,
                                   Status = (a.Status == 1) ? "Active" : (a.Status == 2) ? "Archived" : (a.Status == 3) ? "Completed" : string.Empty
                               })
                              .Union(
                                 from a in _db.Events
                                 join b in _db.Investigations on a.EventID equals b.EventID
                                 select new
                                 {
                                     EventID = a.EventID,
                                     Name = a.Name,

                                     EventType = "Investigation",

                                     CreatedBy = a.CreatedBy,

                                     EventDate = a.EventDate,

                                     DateModified = a.DateModified,
                                     Status = (a.Status == 1) ? "Active" : (a.Status == 2) ? "Archived" : (a.Status == 3) ? "Completed" : string.Empty
                                 })
                                 .Union(

                                    from a in _db.Events
                                    join b in _db.Audits on a.EventID equals b.EventID
                                    select new
                                    {
                                        EventID = a.EventID,
                                        Name = a.Name,

                                        EventType = "Audit",

                                        CreatedBy = a.CreatedBy,

                                        EventDate = a.EventDate,

                                        DateModified = a.DateModified,
                                        Status = (a.Status == 1) ? "Active" : (a.Status == 2) ? "Archived" : (a.Status == 3) ? "Completed" : string.Empty

                                    })
                                  .Union(
                                      from a in _db.Events
                                      join b in _db.CorrectiveActionPlans on a.EventID equals b.EventID
                                      select new
                                      {
                                          EventID = a.EventID,
                                          Name = a.Name,

                                          EventType = "Corrective Action Plan",

                                          CreatedBy = a.CreatedBy,

                                          EventDate = a.EventDate,

                                          DateModified = a.DateModified,
                                          Status = (a.Status == 1) ? "Active" : (a.Status == 2) ? "Archived" : (a.Status == 3) ? "Completed" : string.Empty
                                      });

            if (userId != 0 && userId != 0 - 10)
            {
                evntDetails = evntDetails.Where(a => a.CreatedBy == userId);
            }
            var total = evntDetails.Count();
            var incCount = evntDetails.Where(a => a.EventType == "Incident").Count();
            var invCount = evntDetails.Where(a => a.EventType == "Investigation").Count();
            var auditCount = evntDetails.Where(a => a.EventType == "Audit").Count();
            var capCount = evntDetails.Where(a => a.EventType == "Corrective Action Plan").Count();
            var openCAPCount = evntDetails.Where(a => a.EventType == "Corrective Action Plan" && a.Status == "Active").Count();
            List<int> eventList = new List<int> { total, incCount, invCount, auditCount, capCount, openCAPCount };
            return _efSerializer.EFSerialize(eventList);
        }
        #endregion

        public string TasksCount(string userID)
        {
            int userId = Convert.ToInt32(userID);
            var getTaskDetails = (from tk in _db.Tasks
                                  join taskrp in _db.TasksResponsiblePersons on tk.TaskID equals taskrp.TaskId
                                  where taskrp.UserId == userId
                                  orderby tk.DueDate
                                  select new
                                  {
                                      TaskTypeName = tk.TaskType.TaskTypeName,
                                      ResponsiblePerson = taskrp.Users.UserName,
                                      DueDate = tk.DueDate,
                                      // TaskId = tk.TaskID,
                                      Status = tk.IsCompleted,
                                      Description = tk.Description
                                  });
            string TasksCount = Convert.ToString(getTaskDetails.Count());
            return TasksCount;
        }

        public string GetEventURL(string name, int? moduleId, int eventId, int companyId, string baseURL)
        {

            string eventURL = string.Empty;
            string linkURL = string.Empty;

            string moduleName = ((hlp.Enumeration.Modules)moduleId).ToString();


            var investigation = _db.Investigations.FirstOrDefault(a => a.EventID == eventId);
            bool hasInvestigation = (investigation != null);

            bool navigateToTapRoot = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Investigation);
            bool navigatToFixTab = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan && hasInvestigation);
            bool navigateToCAP = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan);

            bool navigateToDetail = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Audit) || (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Incident);

            
          //  string navigateTabName = navigateToTapRoot ? TAPROOT_TAB_NAME : navigatToFixTab ? FIX_TAB_NAME : navigateToCAP ? CAP_TAB_NAME : DETAILS_TAB_NAME;
            string navigateTabName = navigateToTapRoot ? TAPROOT_TAB_NAME : navigatToFixTab ? TAPROOT_TAB_NAME : navigateToCAP ? CAP_TAB_NAME : DETAILS_TAB_NAME;

            int tabId = _db.Tabs.Where(x => x.CompanyID == companyId && x.TabName == navigateTabName).FirstOrDefault().TabID;


           // string displayName = navigateToTapRoot ? TAPROOT_TAB_DISPLAY_NAME : navigatToFixTab ? FIX_TAB_DISPLAY_NAME : navigateToCAP ? CAP_TAB_DISPLAY_NAME : DETAILS_TAB_NAME;
            string displayName = navigateToTapRoot ? TAPROOT_TAB_DISPLAY_NAME : navigatToFixTab ? TAPROOT_TAB_DISPLAY_NAME : navigateToCAP ? CAP_TAB_DISPLAY_NAME : DETAILS_TAB_NAME;

            string eventID = _cryptography.Encrypt(eventId.ToString());

            eventURL = navigatToFixTab ? String.Format(baseURL + "Event/{0}/{1}/{2}", "Investigation", displayName + "-" + tabId, eventID)
                : (navigateToTapRoot || navigateToCAP) ? String.Format(baseURL + "Event/{0}/{1}/{2}", moduleName, displayName + "-" + tabId, eventID)
                   : String.Format(baseURL + "Event/{0}/{1}/{2}/{3}", moduleName, displayName + "-" + tabId, eventID, "Edit");         



            return eventURL;


        }
        public void SetEventDateAndTime(string eventDate, string eventTime, string nfEventTime, int userId, ref tap.dat.Events events)
        {
            int? companyId = _db.Users.Where(a => a.UserID == userId).Select(a => a.CompanyID).FirstOrDefault();

            if (eventDate != "")
            {
                if (eventTime != "")
                {
                    events.EventTime = GetTimeSpan(nfEventTime); 
                    eventTime = GetTime(eventTime);
                    eventDate = eventDate + " " + eventTime;
                }
                else
                {
                    eventTime = null;
                    events.EventTime = null;
                    eventDate = eventDate + " " + "00:00";
                }

                //_eventDateTime = DateTime.ParseExact(eventDate, "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                _eventDateTime = dt.fromDatefrmatToDate(eventDate, companyId);
            }
            else
            {
                _eventDateTime = Convert.ToDateTime("01/01/1900");
            }

            events.EventDate = _eventDateTime;
        }

        public void SetPropertiesForEvent(ref tap.dat.Events events, int userId, int eventId, string eventName, string eventDate, string eventTime, string location,
                                          string classification, int status, string userID, int eventType, string invDate, string invTime, string eventDescription,
                                          string eventFileNumber, string capDate, string capTime, string companyID, string nfEventTime)
        {

            events.Name = eventName;
            events.CreatedBy = userId;
            SetEventDateAndTime(eventDate, eventTime, nfEventTime, userId, ref events);
            if (eventId == -1)
            {
                events.DateCreated = DateTime.UtcNow; //GMT;
                events.ModuleID = eventType;
            }

            events.DateModified = DateTime.UtcNow;
            events.Status = status;
            events.Location = location;
            events.Description = eventDescription;
            events.FileNumber = eventFileNumber;
            events.ModuleID = eventType;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="classification"></param>
        /// <param name="events"></param>
        public void SaveClassification(string classification, int eventId, tap.dat.Events events)
        {
            int classificationId = 0;
            bool isDeleteListValue = false;

            if (classification != null && classification != string.Empty)
            {
                string[] _listArrval = classification.Split(',');

                for (int i = 0; i < _listArrval.Length; i++)
                {

                    classificationId = Convert.ToInt32(_listArrval[i].Split('#')[0]);
                    if (events.EventID != -1)
                    {
                        if (isDeleteListValue == false)
                            DeleteFieldListValues(Convert.ToInt32(tap.dom.hlp.Enumeration.SystemFields.Events_Classification), events.EventID, classificationId);

                        SaveFieldListValues(Convert.ToInt32(tap.dom.hlp.Enumeration.SystemFields.Events_Classification), events.EventID, classificationId, eventId);
                        isDeleteListValue = true;
                    }
                    else
                        SaveFieldListValues(Convert.ToInt32(tap.dom.hlp.Enumeration.SystemFields.Events_Classification), events.EventID, classificationId, eventId);
                }
            }
        }


        public void CreateOrUpdateIncident(int eventId, tap.dat.Events events, string nfEventTime)
        {
            tap.dat.Incidents incident = null;
            incident = (eventId == -1) ? new tap.dat.Incidents() : _db.Incidents.FirstOrDefault(a => a.EventID == eventId);

            if (incident != null)
            {
                if (eventId == -1)
                    incident.EventID = events.EventID;
                incident.IncidentDatetime = _eventDateTime;
                incident.DateCreated = DateTime.Now;

                if (nfEventTime != "")
                {

                    incident.IncidentTime = GetTimeSpan(nfEventTime);
                }
                else
                    incident.IncidentTime = null;


                if (eventId == -1)
                {
                    _db.AddToIncidents(incident);
                    _isNewEvent = tap.dom.hlp.Enumeration.ModuleNew.NewInvestigation.ToString();
                    _mainModuleId = tap.dom.hlp.Enumeration.Modules.Investigation;
                    //tap.dom.transaction.Transaction.SaveTransaction(incident.EventID, _userId, _companyId,
                    //                                                Enumeration.TransactionCategory.Create, "Incident created");
                }
                _db.SaveChanges();

            }
        }

        public void CreateOrUpdateInvestigation(int eventId, string invDate, string invTime, int id, string nfInvTime, int userId)
        {
            bool isNewInv = false;
            tap.dat.Investigations investigation = null;
            investigation = _db.Investigations.FirstOrDefault(a => a.EventID == eventId);
            int? companyId = _db.Users.Where(a => a.UserID == userId).Select(a => a.CompanyID).FirstOrDefault();

            if (investigation == null)
            {
                isNewInv = true;
                investigation = new tap.dat.Investigations();

                _isNewEvent = tap.dom.hlp.Enumeration.ModuleNew.NewCap.ToString();
                _mainModuleId = tap.dom.hlp.Enumeration.Modules.Investigation;
            }

            if (invDate != "")
            {
                if (invTime != "")
                {
                    investigation.InvestigationTime = GetTimeSpan(nfInvTime);
                    invTime = GetTime(invTime);
                    invDate = invDate + " " + invTime;
                }
                else
                {
                    investigation.InvestigationTime = null;
                    invDate = invDate + " " + "00:00";
                }
               // _eventDateTime = DateTime.ParseExact(invDate, "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                _eventDateTime = dt.fromDatefrmatToDate(invDate, companyId);
                investigation.InvestigationDatetime = _eventDateTime;

            }
            else
            {
                investigation.InvestigationDatetime = null;
            }

            if (eventId != -1)
                investigation.EventID = eventId;
            else
                investigation.EventID = id;

            investigation.CreatedBy = userId;
            investigation.DateCreated = DateTime.Now;

            if (isNewInv)
            {
                _db.AddToInvestigations(investigation);
                //tap.dom.transaction.Transaction.SaveTransaction(investigation.EventID, _userId, _companyId,
                //                                                Enumeration.TransactionCategory.Create, "Investigation Created");
            }

            _db.SaveChanges();

        }

        public TimeSpan GetTimeSpan(string time)
        {

            DateTime dt = Convert.ToDateTime(time);
            TimeSpan ts = dt.TimeOfDay;

            return ts;
        }

        public void CreateOrUpdateAudit(int eventId, int id, string nfEventTime, int userId)
        {
            tap.dat.Audits audit = null;
            bool newAudit = false;
            audit = _db.Audits.FirstOrDefault(a => a.EventID == eventId);

            if (audit == null)
            {
                audit = new tap.dat.Audits();
                newAudit = true;
                _isNewEvent = tap.dom.hlp.Enumeration.ModuleNew.NewAudit.ToString();
                _mainModuleId = tap.dom.hlp.Enumeration.Modules.Audit;
            }

            if (nfEventTime != "")
            {
                audit.AuditTime = GetTimeSpan(nfEventTime);
            }
            else
            {
                audit.AuditTime = null;
            }

            audit.EventID = (eventId != -1) ? eventId : id;

            audit.AuditDatetime = _eventDateTime;
            audit.CreatedBy = userId;
            audit.DateCreated = DateTime.Now;

            if (newAudit)
            {
                _db.AddToAudits(audit);
                //tap.dom.transaction.Transaction.SaveTransaction(audit.EventID, _userId, _companyId,
                //                                                Enumeration.TransactionCategory.Create, "Audit created");
            }

            _db.SaveChanges();

        }

        public void CreateOrUpdateCAP(int eventId, string capDate, string capTime, int id, string nfCapTime, int userId)
        {
            tap.dat.CorrectiveActionPlans cap = null;
            bool newCap = false;
            cap = _db.CorrectiveActionPlans.FirstOrDefault(a => a.EventID == eventId);
            int? companyId = _db.Users.Where(a => a.UserID == userId).Select(a => a.CompanyID).FirstOrDefault();

            if (cap == null)
            {
                cap = new dat.CorrectiveActionPlans();
                newCap = true;

                _isNewEvent = tap.dom.hlp.Enumeration.ModuleNew.NewCap.ToString();
                _mainModuleId = tap.dom.hlp.Enumeration.Modules.ActionPlan;
            }

            if (capDate != "")
            {
                if (capTime != "")
                {
                    cap.CapTime = GetTimeSpan(nfCapTime);
                    capTime = GetTime(capTime);
                    capDate = capDate + " " + capTime;
                }
                else
                {
                    capTime = null;
                    capDate = capDate + " " + "00:00";
                }
                //_eventDateTime = DateTime.ParseExact(capDate, "MM/dd/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                _eventDateTime = dt.fromDatefrmatToDate(capDate, companyId);
                cap.CapDatetime = _eventDateTime;
            }
            else
            {
                cap.CapDatetime = null;
            }

            if (eventId != -1)
                cap.EventID = eventId;
            else
                cap.EventID = id;

            cap.CreatedBy = userId;
            cap.DateCreated = DateTime.Now;

            if (newCap)
            {
                _db.AddToCorrectiveActionPlans(cap);
                //tap.dom.transaction.Transaction.SaveTransaction(cap.EventID, _userId, _companyId, Enumeration.TransactionCategory.Create,
                //                                                "Corrective action created");
            }

            _db.SaveChanges();

        }

        public string GetModuleName(int? moduleId)
        {
            return (moduleId == 1) ? "Incident"
                                  : (moduleId == 2) ? "Investigation"
                                  : (moduleId == 3) ? "Audit" : "Action Plan";

        }

        public bool NewmoduleNotExist(int newModule, int eventId)
        {
            bool moduleExist = false;
            dynamic moduleDetails = null;
            switch (newModule)
            {

                case 1:
                    moduleDetails = _db.Incidents.FirstOrDefault(a => a.EventID == eventId);
                    break;
                case 2:
                    moduleDetails = _db.Investigations.FirstOrDefault(a => a.EventID == eventId);
                    break;
                case 3:
                    moduleDetails = _db.Audits.FirstOrDefault(a => a.EventID == eventId);
                    break;
                case 4:
                    moduleDetails = _db.CorrectiveActionPlans.FirstOrDefault(a => a.EventID == eventId);
                    break;
            }


            if (moduleDetails != null)
            {
                moduleExist = true;
            }
            return moduleExist;
        }

        public void SaveEventDetails(ref tap.dat.Events events, int userId, int eventId, string eventName, string eventDate, string eventTime,
                                                string location, string classification, int status, string userID, int eventType, string invDate, string invTime,
                                                string eventDescription, string eventFileNumber, string capDate, string capTime, string companyID, string nfEventTime, string[] arrDynamicFieldIds, string[] arrDynamicFieldValues, string[] arrControlsNames)
        {

            string transactionMessage = string.Empty;
            events = ((eventId == -1) ? new tap.dat.Events() : _db.Events.FirstOrDefault(a => a.EventID == eventId));

            if (events.EventID > 0)
            {
                if (!NewmoduleNotExist(eventType, events.EventID))
                {
                    string prevModule = GetModuleName(events.ModuleID);
                    string newModule = GetModuleName(eventType);

                    transactionMessage = newModule + " added to " + prevModule;
                }
            }

            int eventCompanyID = 0;
            int.TryParse(companyID, out eventCompanyID);

            events.Name = eventName;
            events.CreatedBy = userId;
            SetEventDateAndTime(eventDate, eventTime, nfEventTime, userId, ref events);
            events.DateCreated = (eventId == -1) ? DateTime.UtcNow : events.DateCreated;
            events.DateModified = DateTime.UtcNow;
            events.Status = status;
            events.Location = location;
            events.Description = eventDescription;
            events.FileNumber = eventFileNumber;
            events.ModuleID = eventType;
            events.CompanyID = eventCompanyID;

            if (eventId == -1)
            {
                _db.AddToEvents(events);

            }
            _db.SaveChanges();

            //Save Classification valueid
            SaveClassification(classification, eventId, events);

            // to save dynamic field values in FieldValues table
            if (arrDynamicFieldIds != null && arrDynamicFieldIds.Count() != 0)
                SaveDynamicFields(events.EventID, arrDynamicFieldIds, arrDynamicFieldValues, arrControlsNames, false);

            if (eventId == -1)
            {
                //Save Team Member
                SaveTeamMemberWithEvent(userId, eventCompanyID, events.EventID);
            }

            if (transactionMessage != string.Empty)
            {
                //tap.dom.transaction.Transaction.SaveTransaction(events.EventID, _userId, _companyId,
                //                                                Enumeration.TransactionCategory.Create, transactionMessage);
            }

        }

        #region "Save Dynamic Fields for Details Tab"
        public string SaveDynamicFields(int eventId, string[] arrDynamicFieldIds, string[] arrDynamicFieldValues, string[] arrControlsNames, bool isDataList)
        {
            tap.dat.FieldValues fieldValues = null;
            for (int i = 0; i < arrDynamicFieldIds.Count(); i++)
            {
                fieldValues = new tap.dat.FieldValues();
                fieldValues.EventID = eventId;
                fieldValues.FieldID = Convert.ToInt32(arrDynamicFieldIds[i]);
                if (arrControlsNames[i] == "Radio Button")
                    fieldValues.FieldValue = (arrDynamicFieldValues[i] == "1" ? "Yes" : "No");
                else
                    fieldValues.FieldValue = arrDynamicFieldValues[i];
                fieldValues.isDataList = isDataList;

                _db.AddToFieldValues(fieldValues);
                _db.SaveChanges();

            }
            return string.Empty;
        }
        #endregion


        #region Common Method  For Saving Eventtype(Incident,Investigation)
        /// <summary>
        /// Decsription-Save the Event Data
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventName"></param>
        /// <param name="eventDate"></param>
        /// <param name="eventTime"></param>
        /// <param name="location"></param>
        /// <param name="classification"></param>
        /// <param name="status"></param>
        /// <param name="createdBy"></param>
        /// <param name="eventType"></param>
        /// <param name="strFieldvalues"></param>
        public string[] SaveEvent(int eventId, string eventName, string eventDate, string eventTime, string location, string classification, int status, string userID,
            int eventType, string invDate, string invTime, string eventDescription, string eventFileNumber, string capDate, string capTime,
            string companyID, string baseURL, string[] arrDynamicFieldIds, string[] arrDynamicFieldValues, string[] arrControlsNames)
        {

            #region "Varibles"

            //encrypt userID and pass
            int userId = Convert.ToInt32(userID);
            _userId = userId;
            _companyId = Convert.ToInt32(companyID);

            tap.dat.Events events = null;
            int id = 0;

            //Added  For Time Field in Event Tables
            string nfEventTime = string.Empty;
            string nfInvTime = string.Empty;
            string nfCapTime = string.Empty;
            nfEventTime = eventTime;
            nfInvTime = invTime;
            nfCapTime = capTime;

            string transactionMessage = string.Empty;

            #endregion

            #region Added for formatted Date
            if (eventDate != "")
                eventDate = SiteSettingDateFormat(companyID, eventDate);
            if (invDate != "")
                invDate = SiteSettingDateFormat(companyID, invDate);
            if (capDate != "")
                capDate = SiteSettingDateFormat(companyID, capDate);
            #endregion

            try
            {
                //change the hard coded values with enum values created
                if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Incident || eventType == (int)tap.dom.hlp.Enumeration.Modules.Investigation || eventType == (int)tap.dom.hlp.Enumeration.Modules.Audit || eventType == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan)
                {

                    SaveEventDetails(ref events, userId, eventId, eventName, eventDate, eventTime, location, classification, status, userID,
                                                eventType, invDate, invTime, eventDescription, eventFileNumber, capDate, capTime, companyID, nfEventTime, arrDynamicFieldIds, arrDynamicFieldValues, arrControlsNames);

                    if (eventType != (int)tap.dom.hlp.Enumeration.Modules.Audit)//nw
                    {
                        CreateOrUpdateIncident(eventId, events, nfEventTime);
                    }

                    id = events.EventID;

                }
                if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Investigation) //if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Investigation || eventType == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan)
                {
                    CreateOrUpdateInvestigation(eventId, invDate, invTime, id, nfInvTime, userId);
                }
                if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Audit)
                {
                    CreateOrUpdateAudit(eventId, id, nfEventTime, userId);
                }
                if (eventType == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan)
                {
                    CreateOrUpdateCAP(eventId, capDate, capTime, id, nfCapTime, userId);
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return GetResultDetails(id, baseURL, Convert.ToInt32(companyID));
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public string[] GetResultDetails(int id, string baseURL, int companyId)
        {
            string[] resultArray = new string[6];
            string strMainTab = string.Empty;
            string strSubTab = string.Empty;
            int detailsTabId = 0;
            int decryptEvent = id;
            string eventID = (!string.IsNullOrEmpty(id.ToString()) && id.ToString() != "0") ? _cryptography.Encrypt(id.ToString()) : "0";
            string configsetting = ConfigurationManager.AppSettings["IsSU"];
            string appType = (configsetting == "0") ? "EntEventIds" : "SUEventIds";

            if (_isNewEvent != string.Empty)
            {
                strMainTab = _eventTab.BuildHTMlMainTabs(0, eventID, (int)_mainModuleId, baseURL, companyId);
                strSubTab = _eventTab.BuildHTMlSubTabs(0, eventID, (int)_mainModuleId, baseURL, companyId, 0);
                if (companyId > 0)
                    detailsTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;
            }

            resultArray[0] = eventID;
            resultArray[1] = strMainTab;
            resultArray[2] = strSubTab;
            resultArray[3] = detailsTabId.ToString();
            resultArray[4] = decryptEvent.ToString();
            resultArray[5] = appType.ToString();

            return resultArray;

        }
        #region "Delete FieldList Values"
        /// <summary>
        /// Description-Delete The data from Fieldvalue Table
        /// </summary>
        /// <param name="systemFieldId"></param>
        /// <param name="eventId"></param>
        /// <param name="valueId"></param>
        public void DeleteFieldListValues(int systemFieldId, int eventId, int valueId)
        {
            try
            {
                int listId = 0;
                string fullPath = "\\Classification";
                tap.dom.adm.ListValues listValue = new adm.ListValues();
                listId = listValue.ListValueIDByNameSelect(fullPath);//get the list id
                List<tap.dat.FieldListValues> fieldListValues = _db.FieldListValues.Where(a => (a.EventId == eventId)).ToList();
                for (int i = 0; i < fieldListValues.Count(); i++)
                {
                    _db.DeleteObject(fieldListValues[i]);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        #region To insert Data in FieldListValues
        /// <summary>
        /// Description-save the Data  to FiledValue Table
        /// </summary>
        /// <param name="systemFieldId"></param>
        /// <param name="eventId"></param>
        /// <param name="valueId"></param>
        /// <param name="updateEventId"></param>
        public void SaveFieldListValues(int systemFieldId, int eventId, int valueId, int updateEventId)
        {
            bool isNewFieldVal = false;
            try
            {
                int listId = 0;
                string fullPath = "\\Classification";
                tap.dom.adm.ListValues listValue = new adm.ListValues();
                listId = listValue.ListValueIDByNameSelect(fullPath);//get the list id

                tap.dat.FieldListValues fieldListValue = null;
                fieldListValue = _db.FieldListValues.FirstOrDefault(a => a.EventId == eventId);
                fieldListValue = new dat.FieldListValues();
                isNewFieldVal = true;

                fieldListValue.FieldValueId = listId;
                fieldListValue.SystemFieldId = systemFieldId;
                fieldListValue.EventId = eventId;
                fieldListValue.ValueId = valueId;
                if (isNewFieldVal)
                    _db.AddToFieldListValues(fieldListValue);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public bool HasInvestigation(int eventId)
        {
            var investigationCount = _db.Investigations.Where(a => a.EventID == eventId).Count();
            bool hasInvestigation = investigationCount > 0 ? true : false;
            return hasInvestigation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public bool HasCorrectiveAction(int eventId)
        {
            var capCount = _db.CorrectiveActionPlans.Where(a => a.EventID == eventId).Count();
            bool hasCAP = capCount > 0 ? true : false;
            return hasCAP;
        }


        #region For Common Method To Get EventRecord
        /// <summary>
        /// Decsription-To get the Event Records
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public string GetEventDataByEventType(int eventId, int eventType)
        {
            try
            {

                if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Incident)
                {
                    var incident = from a in _db.Events
                                   join b in _db.Incidents
                                   on a.EventID equals b.EventID
                                   where a.EventID == eventId
                                   select new { EventID = a.EventID, Name = a.Name, EventDateTime = b.IncidentDatetime, Status = a.Status, InvestigationDateTime = string.Empty, Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = b.IncidentTime, InvestigationTime = string.Empty, CapDatetime = string.Empty, capTime = string.Empty };//new-change


                    return _efSerializer.EFSerialize(incident);
                }
                else if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Investigation)
                {

                    var investigation = from a in _db.Events
                                        join b in _db.Incidents
                                            on a.EventID equals b.EventID into inctemp
                                        where a.EventID == eventId
                                        from t in inctemp
                                        join c in _db.Investigations
                                        on t.EventID equals c.EventID into invtemp
                                        from t1 in invtemp.DefaultIfEmpty()
                                        select new { EventID = a.EventID, Name = a.Name, EventDateTime = t.IncidentDatetime, Status = a.Status, InvestigationDateTime = t1.InvestigationDatetime, Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = t.IncidentTime, InvestigationTime = t1.InvestigationTime, CapDatetime = string.Empty, capTime = string.Empty };

                    return _efSerializer.EFSerialize(investigation);

                }
                else if (eventType == (int)tap.dom.hlp.Enumeration.Modules.Audit)
                {

                    var audit = from a in _db.Events
                                join b in _db.Audits
                                on a.EventID equals b.EventID
                                where a.EventID == eventId
                                select new { EventID = a.EventID, Name = a.Name, EventDateTime = b.AuditDatetime, Status = a.Status, InvestigationDateTime = string.Empty, Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = b.AuditTime, InvestigationTime = string.Empty, CapDatetime = string.Empty, capTime = string.Empty };

                    return _efSerializer.EFSerialize(audit);
                }
                else if (eventType == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan)
                {
                    var hasInvestigation = _db.Investigations.FirstOrDefault(a => a.EventID == eventId);

                    if (hasInvestigation != null)
                    {
                        var cap = from a in _db.Events
                                  join b in _db.Incidents
                                      on a.EventID equals b.EventID
                                  join c in _db.Investigations on a.EventID equals c.EventID
                                  into inctemp
                                  where a.EventID == eventId
                                  from t in inctemp
                                  join d in _db.CorrectiveActionPlans
                                  on t.EventID equals d.EventID into invtemp
                                  from t1 in invtemp.DefaultIfEmpty()
                                  select new { EventID = a.EventID, Name = a.Name, EventDateTime = b.IncidentDatetime, Status = a.Status, InvestigationDateTime = t.InvestigationDatetime, Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = b.IncidentTime, InvestigationTime = t.InvestigationTime, CapDatetime = t1.CapDatetime, capTime = t1.CapTime };


                        var capNew = from a in _db.Events
                                     join b in _db.Audits
                                     on a.EventID equals b.EventID into audittemp
                                     where a.EventID == eventId
                                     from t in audittemp
                                     join d in _db.CorrectiveActionPlans
                                     on t.EventID equals d.EventID into temp
                                     from t1 in temp.DefaultIfEmpty()
                                     select new { EventID = a.EventID, Name = a.Name, EventDateTime = t.AuditDatetime, Status = a.Status, InvestigationDateTime = "", Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = t.AuditTime, InvestigationTime = "", CapDatetime = t1.CapDatetime, capTime = t1.CapTime };

                        if (cap.Count() != 0)
                            return _efSerializer.EFSerialize(cap);
                        else if (capNew.Count() != 0)
                            return _efSerializer.EFSerialize(capNew);

                        return _efSerializer.EFSerialize(cap);
                    }
                    else
                    {
                        var cap = from a in _db.Events
                                  join b in _db.Incidents
                                      on a.EventID equals b.EventID into inctemp
                                  where a.EventID == eventId
                                  from t in inctemp
                                  join c in _db.CorrectiveActionPlans
                                  on t.EventID equals c.EventID into invtemp
                                  from t1 in invtemp.DefaultIfEmpty()
                                  select new { EventID = a.EventID, Name = a.Name, EventDateTime = t.IncidentDatetime, Status = a.Status, InvestigationDateTime = "", Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = t.IncidentTime, InvestigationTime = "", CapDatetime = t1.CapDatetime, capTime = t1.CapTime };


                        var capNew = from a in _db.Events
                                     join b in _db.Audits
                                     on a.EventID equals b.EventID into audittemp
                                     where a.EventID == eventId
                                     from t in audittemp
                                     join d in _db.CorrectiveActionPlans
                                     on t.EventID equals d.EventID into temp
                                     from t1 in temp.DefaultIfEmpty()
                                     select new { EventID = a.EventID, Name = a.Name, EventDateTime = t.AuditDatetime, Status = a.Status, InvestigationDateTime = "", Location = a.Location, Description = a.Description, FileNumber = a.FileNumber, IncidentTime = t.AuditTime, InvestigationTime = "", CapDatetime = t1.CapDatetime, capTime = t1.CapTime };

                        if (cap.Count() != 0)
                            return _efSerializer.EFSerialize(cap);
                        else if (capNew.Count() != 0)
                            return _efSerializer.EFSerialize(capNew);

                        return _efSerializer.EFSerialize(cap);
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);

            }
            return string.Empty;
        }

        #endregion

        #region To get time in HH:MM Format
        /// <summary>
        /// To get the time in 12 or 24 hr format
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public string GetTime(string time)
        {
            string strTime = string.Empty;
            string hh = string.Empty;
            string mm = string.Empty;

            try
            {

                if (time.IndexOf(":") != -1)
                {
                    strTime = DateTime.Parse(time).ToString("HH:mm");
                }
                else
                {
                    if (time.ToLower().ToString().Contains("am"))
                        time = time.Substring(0, time.ToLower().ToString().LastIndexOf("am")).Trim();

                    else if (time.ToLower().ToString().Contains("pm"))
                        time = time.Substring(0, time.ToLower().ToString().LastIndexOf("pm")).Trim();

                    if (time.Length == 3)
                    {
                        hh = time.Substring(0, 1);
                        mm = time.Substring(1, 2);
                        strTime = DateTime.Parse(hh + ":" + mm).ToString("HH:mm");
                    }
                    else if (time.Length == 4)
                    {
                        hh = time.Substring(0, 2);
                        mm = time.Substring(2, 2);
                        strTime = DateTime.Parse(hh + ":" + mm).ToString("HH:mm");
                    }

                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return strTime;
        }
        #endregion

        #region Date time format
        /// <summary>
        /// DateFormat()
        /// Description- Its used to format the time according to site settings
        /// </summary>
        /// <param name="dtDate">to be formated</param>
        /// <param name="iCompanyId">To get the site settings according to the company id</param>
        /// <param name="sTime">To get the site settings according to the time</param>
        /// <returns>formatted date according to the site settings</returns>
        private string DateFormat(DateTime? dtDate, string sTime, string companyID)
        {
            string sDateTime = string.Empty;

            try
            {
                //check the not null for the date
                if (dtDate != null)
                {

                    //make not nullabe
                    // var vCompanyId = companyID;
                    DateTime dtDateTime = dtDate ?? DateTime.Now;

                    //get the time format from the site settings
                    bool blIsTwelveHrFormat = Convert.ToBoolean(_siteSetting.GetTimeFormat());

                    if (!string.IsNullOrEmpty(sTime))
                    {
                        //check the 12 hour format
                        if (blIsTwelveHrFormat)
                            sDateTime = dtDateTime.ToString("MM/dd/yyyy hh:mm tt");
                        else//for the 24 hour format
                            sDateTime = dtDateTime.ToString("MM/dd/yyyy HH:mm");
                    }
                    else
                    {
                        sDateTime = dtDateTime.ToString("MM/dd/yyyy");
                    }
                }
            }
            catch (Exception ex)
            {
                //Error log
                ErrorLog.LogException(ex);
            }

            return sDateTime;
        }

        #endregion

        #region For SiteSetting DateFomat
        /// <summary>
        /// Description-SiteSettingDateFormat
        /// </summary>
        public string SiteSettingDateFormat(string companyID, string eventDate)
        {
            string strDateFormat = string.Empty;
            string strFormattedDate = string.Empty;
            int compId = Convert.ToInt32(companyID);

            try
            {
                strDateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == compId).DateFormat;
                DateFormat dt = new DateFormat();
                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                if (eventDate != "")
                {
                    //dateInfo.ShortDatePattern = ((strDateFormat == "MM/dd/yyyy" || strDateFormat == "MMM dd yyyy") ? "MM/dd/yyyy" : (strDateFormat == "dd/MM/yyyy" || strDateFormat == "dd-MM-yyyy") ? "dd/MM/yyyy" : "MM/dd/yyyy");
                    //DateTime evDate = Convert.ToDateTime(DateTime.Parse(eventDate), dateInfo);
                    //strFormattedDate = evDate.ToString("MM/dd/yyyy").Replace("-", "/");
                    DateTime evDate = dt.fromDatefrmatToDate(eventDate, compId);
                    strFormattedDate = evDate.ToString("MMMM dd,yyyy");
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return strFormattedDate;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventDate"></param>
        /// <returns></returns>
        public string GetEventDate(DateTime? eventDate, string companyID)
        {
            if (eventDate == null) { return ""; }

            try
            {
                DateTime evDate = Convert.ToDateTime(eventDate);
                int compId = Convert.ToInt32(companyID);
                string datePattern = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == compId).DateFormat;

                if (string.IsNullOrEmpty(datePattern))
                    datePattern = "MM/dd/yyyy";
                return evDate.ToString(datePattern, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                return "";
            }
        }

        #region To Calculate the duration from the last modified Date

        /// <summary>
        /// LastModifiedDaysCalculate
        /// </summary>
        /// <param name="lastDateModified"></param>
        /// <param name="strDateFormat"></param>
        /// <returns>Formatted duration of days</returns>
        public string LastModifiedDaysCalculate(DateTime? lastDateModified, string strDateFormat)
        {
            string totalDays = string.Empty;
            DateTime currentValue = DateTime.Now;
            DateTime lastModifiedDate = lastDateModified ?? currentValue;
            TimeSpan totalDuration = currentValue - lastModifiedDate;

            if (totalDuration.Days > 13)
                totalDays = ((lastDateModified == null) ? null : (strDateFormat == "dd/MM/yyyy" || strDateFormat == "MM/dd/yyyy") ? lastDateModified.Value.ToString(strDateFormat) : lastDateModified.Value.ToString(strDateFormat, CultureInfo.InvariantCulture));
            else
            {
                totalDays = totalDuration.Days == 0 ? " Today "
                    : (totalDuration.Hours > 0 || totalDuration.Minutes > 0 || totalDuration.Seconds > 0) ? (totalDuration.Days + 1).ToString() + " Days"
                    : totalDuration.Days.ToString() + " Days";
            }

            return totalDays;

        }

        #endregion


        /// <summary>
        /// Update date modified for event
        /// </summary>
        /// <param name="eventId"> event id</param>
        public static void UpdateDateModified(int eventId)
        {

            try
            {

                tap.dat.EFEntity entityContext = new tap.dat.EFEntity();
                tap.dat.Events events = entityContext.Events.Where(a => a.EventID == eventId).FirstOrDefault();
                events.DateModified = DateTime.Now;
                entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        public string CheckEventName(string eventID, string companyID, string eventName)
        {
            string _dbEventID = null;
            tap.dat.EFEntity entityContext = new tap.dat.EFEntity();
            int eventId = 0;
            if (!String.IsNullOrEmpty(eventID) && !eventID.Equals("0"))
                eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

            try
            {
                int eventCompanyID = 0;
                int.TryParse(companyID, out eventCompanyID);
                if (eventName != null && eventName.Length > 0 && eventCompanyID > 0)
                {
                    tap.dat.Events events = entityContext.Events.Where(a => a.Name == eventName && a.CompanyID == eventCompanyID).FirstOrDefault();
                    if (events != null)
                        _dbEventID = events.EventID.ToString();

                }

                if (_dbEventID != null && eventId > 0 && eventId.Equals(Convert.ToInt32(_dbEventID)))
                {
                    _dbEventID = null;
                    return _efSerializer.EFSerialize(null);
                }

            }
            catch (Exception ex) { tap.dom.hlp.ErrorLog.LogException(ex); }

            return _efSerializer.EFSerialize(_dbEventID);
        }

        public static void SaveTeamMemberWithEvent(int userId, int companyId, int eventId)
        {
            try
            {
                tap.dat.Users user = new dat.Users();
                tap.dat.EFEntity _db = new tap.dat.EFEntity();
                tap.dat.TeamMembers details = new tap.dat.TeamMembers();

                user = _db.Users.FirstOrDefault(a => a.UserID == userId && a.CompanyID == companyId);

                if (user == null)
                    return;

                string fullName = user.FirstName + " " + user.LastName;

                details.UserID = userId;
                details.CompanyID = companyId;
                details.FullName = fullName;
                details.hasEdit = true;
                details.hasView = false;
                details.isDisplayOnReport = true;
                details.isManualAdd = false;
                details.EventID = eventId;

                _db.AddToTeamMembers(details);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #region "Get 6 top pending tasks and action plans and 3 top event names"
        public string GetTopEventPendingTasks(int userId, int companyId)
        {
            #region commented tasks

            //var subTabId = GetSubTabId(companyId);

            //var getEventPendingTasks = (from tk in _db.Tasks
            //                            join ca in _db.CorrectiveActions on tk.CorrectiveActionID equals ca.CorrectiveActionID
            //                            join ev in _db.Events on ca.EventID equals ev.EventID
            //                            where tk.DueDate != null &&
            //                              tk.IsCompleted == false &&
            //                              (ev.CreatedBy == userId ||

            //                            (from tm in _db.TeamMembers
            //                             where
            //                               tm.UserID == userId &&
            //                              tm.hasEdit == true ||
            //                              tm.hasView == true
            //                             select new
            //                             {
            //                                 tm.EventID
            //                             }).Contains(new { EventID = (Int32?)ev.EventID }))
            //                            orderby tk.DueDate
            //                            select new
            //                            {
            //                                TaskTypeName = tk.TaskType.TaskTypeName,
            //                                ActionName = ca.Identifier,
            //                                DueDate = tk.DueDate,
            //                                EventName = ev.Name,
            //                                TaskId = tk.TaskID,
            //                                ActionPlanID = (int?)tk.CorrectiveActionID,
            //                                EventId = ev.EventID,
            //                                IsCompleted = tk.IsCompleted
            //                            }).Take(6).AsEnumerable().Select(x => new
            //                            {
            //                                TaskTypeName = x.TaskTypeName,
            //                                ActionName = x.ActionName,
            //                                DueDate = x.DueDate,
            //                                EventName = x.EventName,
            //                                EventId = GetEncryptedEventId(x.EventId),
            //                                SubTabId = subTabId,
            //                                TaskId = x.TaskId,
            //                                ActionPlanID = x.ActionPlanID,
            //                                IsCompleted = x.IsCompleted
            //                            });

            //return _efSerializer.EFSerialize(getEventPendingTasks);           
            
            #endregion

            var tasks = _db.GetDependencyTasks(userId);
            var tskList = from tsk in tasks.ToList() select tsk;
            var undefinedDueDateCount = (from c in tskList where c.DueDate == null select c).Count();
            var withUndefinedDueDate = tskList.Take(undefinedDueDateCount).ToList();
            var withoutUndefinedDueDate = tskList.Skip(undefinedDueDateCount).ToList();

            var resultSet = withoutUndefinedDueDate.Union(withUndefinedDueDate).Take(6);


            var dependencyTasks = (from t in resultSet.ToList()
                       select new
                       {
                           eventName = t.Name,
                           eventId = _cryptography.Encrypt(t.EventID.ToString()),
                           correctiveActionId = t.CorrectiveActionID,
                           correctiveActionName = t.Identifier,
                           taskDescription = t.Description,
                           taskDueDate = t.DueDate,
                           taskId = t.TaskID,
                           taskTypeName = t.TaskTypeName
                       }).ToList();

            return _efSerializer.EFSerialize(dependencyTasks);
        }

        public string GetTaskStatus(tap.dat.Tasks tsk)
        {
            string taskStatus = string.Empty;

            try
            {
                 
                var IsCompleted = _db.Tasks.Where(a => a.TaskID == tsk.TaskID - 1 && a.CorrectiveActionID== tsk.CorrectiveActionID).FirstOrDefault().IsCompleted;
                if (IsCompleted == true)
                {
                    taskStatus = "Ready";

                }
                else
                    taskStatus = "Pending";
               // taskStatus = (Convert.ToBoolean(tsk.IsCompleted)) ? "Completed" : tsk.DueDate < DateTime.Now.Date ? "Over Due" : "Pending";
            }
            catch (Exception ex)
            {
                taskStatus = "Ready";
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return taskStatus;

        }


        public string GetResponsiblePerson(tap.dat.TasksResponsiblePersons taskrp)
        {

            string name = (taskrp.UserId != 0) ? _db.Users.Where(a => a.UserID == taskrp.UserId).FirstOrDefault().UserName : taskrp.DisplayName;
            return name;

        }

        public string GetEventsWithCap(tap.dat.Events evt, tap.dat.CorrectiveActions cap)
        {
            string Createdby = _db.Events.Where(a => a.EventID == cap.EventID).FirstOrDefault().CreatedBy.ToString();
            return Createdby;
        }

        public string GetTasks(string page, string rows, string sidx, string sord, int userId, int companyId, string status)
        {
            try
            {
               // var DueDt= changeCompleteStatus()




                var getTaskDetails = (from tk in _db.Tasks.AsEnumerable()
                                      join taskrp in _db.TasksResponsiblePersons.AsEnumerable() on tk.TaskID equals taskrp.TaskId
                                      join cap in _db.CorrectiveActions.AsEnumerable() on tk.CorrectiveActionID equals cap.CorrectiveActionID
                                      join evt in _db.Events.AsEnumerable() on cap.EventID equals evt.EventID
                                      where tk.IsCompleted == false &&
                                      (taskrp.UserId== userId || 
                                      taskrp.UserId==0 && userId == evt.CreatedBy )
                                      orderby tk.CorrectiveActionID, tk.TaskID, tk.TaskTypeID
                                      //orderby tk.DueDate
                                      let TaskStatus = GetTaskStatus(tk)
                                      let PersonName = GetResponsiblePerson(taskrp)
                                      let DueDate = GetEventDate(tk.DueDate, companyId.ToString())
                                      select new
                                      {
                                          EventId = evt.EventID,
                                          EventName = evt.Name,
                                          CorrectiveAction = cap.Identifier,
                                          TaskTypeName = tk.TaskType.TaskTypeName,
                                          ResponsiblePerson = PersonName,
                                          DueDate = DueDate,
                                          TaskId = tk.TaskID,
                                          Status = TaskStatus,
                                          Description = tk.Description,


                                      }).AsEnumerable();
                var TasksDetails= getTaskDetails.OrderBy(a=> a.TaskTypeName);

                if(status == "Only")
                {
                  var TasksDetailsReady = getTaskDetails.Where(d => d.Status == "Ready");
                    TasksDetails= TasksDetailsReady.OrderBy(a=> a.TaskTypeName);
                }

                #region Sorting the tasks

                //Sorting Filter
                switch (sidx)
                {
                    case "EventName":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.EventName) : TasksDetails.OrderBy(e => e.EventName));
                        break;

                    case "CorrectiveAction":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.CorrectiveAction) : TasksDetails.OrderBy(e => e.CorrectiveAction));
                        break;

                    case "TaskTypeName":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.TaskTypeName) : TasksDetails.OrderBy(e => e.TaskTypeName));
                        break;

                    case "ResponsiblePerson":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.ResponsiblePerson) : TasksDetails.OrderBy(e => e.ResponsiblePerson));
                        break;

                    case "DueDate":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.DueDate) : TasksDetails.OrderBy(e => e.DueDate));
                        break;

                    case "Status":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.Status) : TasksDetails.OrderBy(e => e.Status));
                        break;

                    case "Description":
                        TasksDetails = ((sord.Equals("desc")) ? TasksDetails.OrderByDescending(e => e.Description) : TasksDetails.OrderBy(e => e.Description));
                        break;

                        
                }

                #endregion

                //for sorting
                //string sortByColumnName = sidx ?? "DueDate";
                //string sortDirection = sord ?? "asc";

                ////Sort by ascending or descending for ListName or Description.
                //TasksDetails = sortByColumnName == "DueDate" ? sortDirection.Equals("asc") ? TasksDetails.OrderByDescending(e => e.DueDate)
                //    : TasksDetails.OrderBy(e => e.DueDate) : sortDirection.Equals("asc") ? TasksDetails.OrderByDescending(e => e.Description)
                //        : TasksDetails.OrderBy(e => e.Description);

                var data = TasksDetails.ToList();

                //pagination
                int totalRecords = 0;

                var currentPazeSize = rows ?? "15";
                int pageSize = int.Parse(currentPazeSize);

                if (data.Count != 0)
                    totalRecords = data.Count();

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);

                var thisPage = page ?? "1";
                int currentPage = int.Parse(thisPage);

                if (currentPage > 1 && data.Count != 0)
                    data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                var jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPage,
                    records = totalRecords,
                    rows = (
                        from s in data
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = s.TaskId.ToString(),
                            
                            cell = new string[] {  
                                _cryptography.Encrypt(s.EventId.ToString()),
                                s.EventName,
                                s.CorrectiveAction,
                            s.TaskTypeName, 
                            s.ResponsiblePerson,
                            s.DueDate.ToString(),
                            s.Status.ToString(),
                            s.Description
                            //s.eventId.ToString()
                                              
                                    
                        }
                        }).ToArray()
                };

                return _efSerializer.EFSerialize(jsonData);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;

        }



        public string GetEncryptedEventId(int eventId)
        {
            return _cryptography.Encrypt(eventId.ToString());
        }

        public int GetSubTabId(int companyId)
        {
            int subTabId = _db.Tabs.FirstOrDefault(a => a.TabName == CAP_TAB_NAME && a.CompanyID == companyId).TabID;
            return subTabId;
        }

        public string GetTopEvents(int userId, int companyId, string baseURL)
        {



            var getTopEvents = (from ev in _db.Events
                                where
                                  ev.CreatedBy == userId ||

                                (from tm in _db.TeamMembers
                                 where
                                  tm.UserID == userId &&
                                  tm.hasEdit == true ||
                                  tm.hasView == true
                                 select new
                                 {
                                     tm.EventID
                                 }).Contains(new { EventID = (Int32?)ev.EventID })
                                orderby
                                ev.DateModified descending
                                select new

                                {

                                    EventID = ev.EventID,
                                    EventName = ev.Name,
                                    ModuleID = ev.ModuleID

                                }).Take(3).AsEnumerable().Select(x => new
                                {
                                    EventID = x.EventID,
                                    EventName = x.EventName,
                                    RecentEventURL = GetEventURL(x.EventName, x.ModuleID, x.EventID, companyId, baseURL)
                                });


            return _efSerializer.EFSerialize(getTopEvents);
        }
        #endregion

        //To get event name based on encrypted event id
        public string getEventName(string eventID)
        {
            string name = string.Empty;
            try
            {
                int eventId = 0;
                if (!String.IsNullOrEmpty(eventID) && !eventID.Equals("0"))
                    eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                if (eventId == 0)
                    return "";

                name = _db.Events.Where(a => a.EventID == eventId).FirstOrDefault().Name;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return name;
        }
    }
}
