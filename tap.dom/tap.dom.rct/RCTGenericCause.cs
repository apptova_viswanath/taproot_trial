﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.rct
{
    public class RCTGenericCause
    {

        public const string TAPROOT_RCT = "Root Cause";

        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        public void UpdateGenericCause(int genericCauseId, string reState)
        {

            try
            {

                //Get the details of task if is called for editing
                tap.dat.GenericCause genericCause = _entityContext.GenericCause.FirstOrDefault(a => a.GenericCauseID == genericCauseId);
                genericCause.Restate = reState;
                _entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        /// <summary>
        /// Delete generic cause 
        /// </summary>
        /// <param name="rootCauseTreeId"></param>
        public void DeleteGenericCauseByID(int genericCauseId)
        {

            try
            {

                //Get the details of task if is called for editing
                tap.dat.GenericCause genericCause = _entityContext.GenericCause.FirstOrDefault(a => a.GenericCauseID == genericCauseId);
                _entityContext.DeleteObject(genericCause);
                _entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        ///// <summary>
        ///// Delete generic cause 
        ///// </summary>
        ///// <param name="rootCauseTreeId"></param>
        //public void DeleteGenericCauseByRCTID(int rootCauseTreeID, string questionType)
        //{

        //    try
        //    {

        //        //Get the details of task if is called for editing
        //        tap.dat.GenericCause genericCause = _entityContext.GenericCause.FirstOrDefault(a => a.RootCauseTreeID == rootCauseTreeID 
        //                                                                                        && a.QuestionType == questionType);
        //        _entityContext.DeleteObject(genericCause);
        //        _entityContext.SaveChanges();

        //    }
        //    catch (Exception ex)
        //    {
        //        tap.dom.hlp.ErrorLog.LogException(ex);
        //    }

        //}

        /// <summary>
        /// Get all generic causes questions
        /// </summary>
        /// <param name="causalFactorId"></param>
        /// <returns></returns>
        public object GetGenericCauseQuestions(int causalFactorId)
        {

            try
            {

                var rcType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == TAPROOT_RCT select rootCauseType).FirstOrDefault();
                var nearRCType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == "Near" select rootCauseType).FirstOrDefault();

                int rootCauseCategoryID = Convert.ToInt32(rcType.RootCauseCategoryID);
                int nearRootCauseCategoryID = Convert.ToInt32(nearRCType.RootCauseCategoryID);

                var genericQuestions = (from rootCauseTree in _entityContext.RootCauseTree.AsEnumerable()
                                        join rootCauses in _entityContext.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                        equals rootCauses.RootCauseID
                                        //where rootCauseTree.IsSelected == true && rootCauseTree.CausalFactorID == causalFactorId
                                        //&& rootCauses.RootCauseCategoryID == 3
                                        where (rootCauseTree.CausalFactorID == causalFactorId && rootCauseTree.IsSelected == true
                                            && rootCauses.RootCauseCategoryID == rootCauseCategoryID) ||

                                           ((rootCauseTree.CausalFactorID == causalFactorId
                                            && rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == nearRootCauseCategoryID)
                                            && ((from rootCauses1 in _entityContext.RootCauses
                                                 where rootCauses1.ParentID == rootCauses.RootCauseID && rootCauses1.RootCauseCategoryID == rootCauseCategoryID
                                                 select rootCauses1).Count() ==
                                                (from rootCauseTree1 in _entityContext.RootCauseTree
                                                 where rootCauseTree1.CausalFactorID == causalFactorId && rootCauseTree1.IsSelected == false &&
                                                 rootCauseTree1.RootCauses.ParentID == rootCauses.RootCauseID
                                                 select rootCauseTree1).Count()))
                                        orderby rootCauseTree.RootCauseTreeID
                                        select new
                                        {

                                            RCTID = rootCauseTree.RootCauseTreeID,
                                            RCID = rootCauseTree.RootCauseID,
                                            RCTTitle = rootCauses.Title,
                                            isGeneric = rootCauseTree.isGeneric,
                                            Question2Response = rootCauseTree.Question2Response,
                                            Question3Response = rootCauseTree.Question3Response,
                                            Question1Response = rootCauseTree.Question1Response,
                                            RootCauseType = rootCauses.RootCauseCategoryID
                                        });

                return genericQuestions;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        /// <summary>
        /// Get all generic causes questions
        /// </summary>
        /// <param name="causalFactorId"></param>
        /// <returns></returns>
        public object GetGenericCause(int causalFactorId, int eventID)
        {

            try
            {

                var rcType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == TAPROOT_RCT select rootCauseType).FirstOrDefault();
                var corrcetiveActions = (from ca in _entityContext.CorrectiveActions where ca.EventID == eventID select ca.CorrectiveActionID).AsEnumerable();


                var genericQuestions = (from rootCauseTree in _entityContext.RootCauseTree.AsEnumerable()
                                        join rootCauses in _entityContext.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                        equals rootCauses.RootCauseID
                                        where rootCauseTree.IsSelected == true && rootCauseTree.CausalFactorID == causalFactorId
                                        && rootCauses.RootCauseCategoryID == 3 && rootCauseTree.isGeneric == true
                                        orderby rootCauseTree.RootCauseTreeID
                                        select new
                                        {

                                            RCTID = rootCauseTree.RootCauseTreeID,
                                            RCID = rootCauseTree.RootCauseID,
                                            RCTTitle = rootCauses.Title,
                                            GenericCauseAddressedByCA = (from carct in _entityContext.CorrectiveActionRCT
                                                             where
                                                                 corrcetiveActions.Contains(carct.CorrectiveActionID)
                                                                 && carct.RootCauseTreeID == rootCauseTree.RootCauseTreeID
                                                             select carct).Count(),

                                        });

                return genericQuestions;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }




        /// <summary>
        /// Get generic cause statements
        /// </summary>
        /// <param name="causalFactorId"></param>
        /// <returns></returns>
        public object GetGenericCausesStatements(int causalFactorId)
        {

            try
            {
                var genericCauseStatements = (from genericCauses in _entityContext.GenericCause.AsEnumerable()
                                              where genericCauses.CausalFactorID == causalFactorId
                                              select new
                                              {
                                                  RCTID = genericCauses.RootCauseTreeID,
                                                  Restate = genericCauses.Restate,
                                                  QuestionType = genericCauses.QuestionType,
                                                  GenericCauseID = genericCauses.GenericCauseID,
                                              });

                return genericCauseStatements;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

        /// <summary>
        /// Save generic cause
        /// </summary>
        /// <param name="restate"></param>
        /// <param name="rootCauseTreeId"></param>
        /// <param name="question2Response"></param>
        /// <param name="question3Response"></param>
        /// <param name="isGeneric"></param>
        public void SaveGenericCause(int rootCauseTreeId, bool? question2Response,
                                    bool? question3Response, bool? isGeneric, int causalFactorId, string question1Response)
        {

            try
            {

                //Get the details of task if is called for editing
                tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseTreeID == rootCauseTreeId);
                rootCauseTree.Question2Response = question2Response;
                rootCauseTree.Question3Response = question3Response;
                rootCauseTree.isGeneric = isGeneric;
                rootCauseTree.Question1Response = question1Response;

                _entityContext.SaveChanges();

                //if (restate != string.Empty && restate != null)
                //{

                //    tap.dat.GenericCause genericCause = new tap.dat.GenericCause();
                //    genericCause.Restate = restate;
                //    genericCause.RootCauseTreeID = rootCauseTreeId;
                //    genericCause.CausalFactorID = causalFactorId;
                //    _entityContext.AddToGenericCause(genericCause);
                //    _entityContext.SaveChanges();

                //}

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        
             public void SaveProcedure(int rctID, string question1Response)
        {

            try
            {

                //Get the details of task if is called for editing
                tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseTreeID == rctID);
                rootCauseTree.Question1Response = question1Response;

                if (question1Response == string.Empty)
                {
                    rootCauseTree.Question2Response = null;
                    rootCauseTree.Question3Response = null;
                    rootCauseTree.isGeneric = false;
                }

                _entityContext.SaveChanges();

            }
                  catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

             }


        public void SaveGenericCauseSatement(string statement, int rootCauseTreeId, int causalFactorId)
        {

            try
            {
                if (statement != string.Empty && statement != null)
                {
                   
                    tap.dat.GenericCause genericCause = new tap.dat.GenericCause();
                    genericCause.Restate = statement;
                    genericCause.RootCauseTreeID = rootCauseTreeId;
                    genericCause.CausalFactorID = causalFactorId;
                    _entityContext.AddToGenericCause(genericCause);
                    _entityContext.SaveChanges();

                }


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

     
    }
}
