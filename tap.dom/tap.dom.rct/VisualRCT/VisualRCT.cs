﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;

namespace tap.dom.rct
{
    public class VisualRCT
    {

        const int HPD_NODE_ID = 3;
        const string AUTUMN_SEASON = "Autumn";
        const string HPD = "Human Performance Difficulty";
        const string EPD = "Equipment Difficulty";


        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();


        
       /// <summary>
       /// Get all rct questions based on parent rct id, need optimization
       /// </summary>
       /// <param name="rootCauseID"></param>
       /// <param name="causalFactorID"></param>
       /// <returns></returns>
        public object SaveResponseAndGetQuestions(int rootCauseID, int causalFactorID, int userId, string title, bool? checkedValue, string questionResponse)
        {


            object[] resultArray = new object[4];
            bool isHPDNodeClicked = (title == HPD);
            bool isEquipmentDifficultyNodeClicked = (title == EPD);
            bool fetchQuestion = (((title == HPD) || (title == EPD)) && (checkedValue == true));

            #region Save Questions
            RootCauseTreeCF rctCF = new RootCauseTreeCF();
            tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseID == rootCauseID && a.CausalFactorID == causalFactorID);
            bool isNewInsert = (rootCauseTree == null) ? true : false;

            rootCauseTree = isNewInsert ? new tap.dat.RootCauseTree() : rootCauseTree;
            rootCauseTree.CausalFactorID = causalFactorID;
            rootCauseTree.RootCauseID = Convert.ToInt32(rootCauseID);
            rootCauseTree.IsSelected = checkedValue;

            if (isNewInsert)
            {
                _entityContext.AddToRootCauseTree(rootCauseTree);
            }
            _entityContext.SaveChanges(true);
            #endregion

            rootCauseID = (isHPDNodeClicked) ? HPD_NODE_ID : rootCauseID;

            resultArray[0] = isHPDNodeClicked;
            resultArray[1] = isEquipmentDifficultyNodeClicked;
            resultArray[2] = null;
            resultArray[3] = checkedValue;

            if(fetchQuestion)
            {
                resultArray[2] = (isHPDNodeClicked) ? rctCF.GetRCTQuestions(rootCauseID, causalFactorID, userId) : GetVisualRCT(rootCauseID, causalFactorID, userId);
            }
            else
            {
                if (isEquipmentDifficultyNodeClicked)
                {
                    tap.dom.rct.RootCauseTreeCF rct = new tap.dom.rct.RootCauseTreeCF();
                    rct.DeleteRecursivelyAllChild(rootCauseID, causalFactorID);
                }
            }

            if (questionResponse != string.Empty)
            {
                //Get root cause tree by causal factor id and rct id
                //tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseID == rootCauseID && a.CausalFactorID == causalFactorID);
                tap.dom.rct.RootCauseTreeCF rct = new tap.dom.rct.RootCauseTreeCF();
                rct.SaveRCTQuestionResponse(questionResponse, rootCauseTree.RootCauseTreeID);
            }

            return resultArray;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootCauseID"></param>
        /// <param name="causalFactorID"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public object GetQuestionForRCTShapes(int rootCauseID, int causalFactorID, int userId)
        {

            //string spanishLanguage = "es";
            //string englishLanguage = "en";
            //string frenchlanguage = "fr";
            var users = (from user in _entityContext.Users
                     join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                     where user.UserID == userId
                     select new
                     {
                         languageId = user.LanguageId,
                         LangguageName = language.LangaugeCode
                     }).FirstOrDefault();

            int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == ForeignLanguage.English_Language).LanguageID;
            
            int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));

            string languageName = ((users == null) ? ForeignLanguage.English_Language : users.LangguageName);

            var isTitleAvaliable = (from title in _entityContext.RootCausesLanguages where title.RootCauseId == rootCauseID && title.LanguageId == languageId select title).FirstOrDefault();

            if(isTitleAvaliable == null)
            {
                languageId = englishLangugeId;
                languageName = ForeignLanguage.English_Language;
            }

            try
            {

                //Fetch the rct title,rct id and all dictionary questions related to all the child nodes of the provided rct id
                var rctQuestions = (
                from parentRCT in _entityContext.RootCauses
                join parentRCTLanguages in _entityContext.RootCausesLanguages
                on parentRCT.RootCauseID equals parentRCTLanguages.RootCauseId
                where parentRCT.RootCauseID == rootCauseID && parentRCTLanguages.LanguageId == languageId
                orderby parentRCT.RootCauseID
                select new
                {
                    EnglishTitle = from a in _entityContext.RootCausesLanguages where a.RootCauseId == parentRCT.RootCauseID && a.LanguageId == languageId select a.Title,
                    RCType = parentRCT,
                    RCID = parentRCT.RootCauseID,
                    RCTTitle = parentRCTLanguages.Title,
                    RCTLanguageName = languageName,
                    RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == parentRCT.RootCauseID
                                select rootCauseTree).FirstOrDefault().IsSelected,
                    Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                where rctQuestion.RootCauseId == parentRCT.RootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                select new
                                {
                                    QuestionID = rctQuestion.RCTDictionaryQuestionID,
                                    Question = rctQuestionLanguages.HTMLDescription,
                                    QuestionResponse = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                rctt1.CausalFactorID == causalFactorID
                                                select response).FirstOrDefault().IsSelected
                                }
                });

                return rctQuestions;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootCauseID"></param>
        /// <param name="causalFactorID"></param>
        /// <returns></returns>
        public object GetVisualRCT(int rootCauseID, int causalFactorID, int userId)
        {


            //string spanishLanguage = "es";
            string englishLanguage = ForeignLanguage.English_Language.ToString();
            //string frenchlanguage = "fr";
            var users = (from user in _entityContext.Users
                         join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                         where user.UserID == userId
                         select new
                         {
                             languageId = user.LanguageId,
                             LangguageName = language.LangaugeCode
                         }).FirstOrDefault();

            int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
            int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));

            string languageName = ((users == null) ? ForeignLanguage.English_Language : users.LangguageName);

            var isTitleAvaliable = (from title in _entityContext.RootCausesLanguages where title.RootCauseId == rootCauseID && title.LanguageId == languageId select title).FirstOrDefault();

            if (isTitleAvaliable == null)
            {
                languageId = englishLangugeId;
                languageName = ForeignLanguage.English_Language;
            }

            var parentNodeId = _entityContext.RootCauses.FirstOrDefault(a => a.Title == "RootCauseTree").RootCauseID;

            try
            {


                string humanPerformanceTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_HUMAN_PERFORMANCE_DIFFICULTY && a.LanguageId == languageId).Title;
                string equipmentTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_EQUIPMENT_DIFFICULTY && a.LanguageId == languageId).Title;
                string naturalSabotageTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_NATURAL_SABOTAGE && a.LanguageId == languageId).Title;
                string otherTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_OTHER && a.LanguageId == languageId).Title;
                
                //Fetch the rct title,rct id and all dictionary questions related to all the child nodes of the provided rct id
                var rctQuestions = (
                from parentRCT in _entityContext.RootCauses
                join parentRCTLanguages in _entityContext.RootCausesLanguages
                on parentRCT.RootCauseID equals parentRCTLanguages.RootCauseId
                where parentRCT.RootCauseID == rootCauseID && parentRCTLanguages.LanguageId == languageId
                orderby parentRCT.RootCauseID
                select new
                {
                    EnglishTitle = from a in _entityContext.RootCausesLanguages where a.RootCauseId == parentRCT.RootCauseID && a.LanguageId == languageId select a.Title,                    
                    RCType = parentRCT.RootCauseCategoryID,
                    RCID = parentRCT.RootCauseID,
                    RCTTitle = parentRCTLanguages.Title,
                    RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == parentRCT.RootCauseID
                                select rootCauseTree).FirstOrDefault().IsSelected,

                    AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                        where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == parentRCT.RootCauseID
                                        select rootCauseTree).FirstOrDefault().AnalysisComments,
                    GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                     where rctQuestion.RootCauseId == parentRCT.RootCauseID && rctQuestion.IsQuestion == false
                                     select rctQuestion).Count(),

                    Children = from rct in _entityContext.RootCauses
                               join rctLanguages in _entityContext.RootCausesLanguages
                               on rct.RootCauseID equals rctLanguages.RootCauseId
                               where rct.ParentID == rootCauseID && rctLanguages.LanguageId == languageId
                               orderby rct.RootCauseID
                               select new
                               {
                                   RCType = rct.RootCauseCategoryID,
                                   RCID = rct.RootCauseID,
                                   RCTTitle = rctLanguages.Title,
                                   RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                               where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == rct.RootCauseID
                                               select rootCauseTree).FirstOrDefault().IsSelected,
                                   AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                       where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == rct.RootCauseID
                                                       select rootCauseTree).FirstOrDefault().AnalysisComments,

                                   GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                    where rctQuestion.RootCauseId == rct.RootCauseID && rctQuestion.IsQuestion == false
                                                    select rctQuestion).Count(),

                                   Children = from child in _entityContext.RootCauses
                                              join childRctLanguages in _entityContext.RootCausesLanguages on child.RootCauseID equals childRctLanguages.RootCauseId
                                              //join title in _entityContext.RCTLanguagesTitles on childRctLanguages.RootCausesLanguageId equals title.RootCausesLanguageId  

                                              where child.ParentID == rct.RootCauseID

                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DIFICULTAD DE DESEMPEÑO HUMANO" : (languageName == frenchlanguage) ? "Difficulté de la performance humaine" : "Human Performance Difficulty")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DIFICULTAD CON EL EQUIPO" : (languageName == frenchlanguage) ? "Équipement Difficulté" : "Equipment Difficulty")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DESASTRE NATURAL/SABOTAJE" : (languageName == frenchlanguage) ? "Catastrophe naturelle / Sabotage" : "Natural Disaster / Sabotage")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "OTRO(ESPECIFICAR)" : (languageName == frenchlanguage) ? "Autre (précisez )" : "Other (Specify)")                                                  
                                              && rctLanguages.Title != humanPerformanceTitleText
                                              && rctLanguages.Title != equipmentTitleText
                                              && rctLanguages.Title != naturalSabotageTitleText
                                              && rctLanguages.Title != otherTitleText
                                              && childRctLanguages.LanguageId == languageId
                                              orderby child.RootCauseID
                                              select new
                                              {
                                                  RCType = child.RootCauseCategoryID,
                                                  RCID = child.RootCauseID,
                                                  RCTTitle = childRctLanguages.Title,
                                                  RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                              where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child.RootCauseID
                                                              select rootCauseTree).FirstOrDefault().IsSelected,
                                                  AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                      where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child.RootCauseID
                                                                      select rootCauseTree).FirstOrDefault().AnalysisComments,


                                                  GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                   where rctQuestion.RootCauseId == child.RootCauseID && rctQuestion.IsQuestion == false
                                                                   select rctQuestion).Count(),

                                                
                                                  Children = from child2 in _entityContext.RootCauses
                                                             join childRctLanguages2 in _entityContext.RootCausesLanguages
                                                            on child2.RootCauseID equals childRctLanguages2.RootCauseId
                                                             where child2.ParentID == child.RootCauseID

                                                             && childRctLanguages2.LanguageId == languageId
                                                             orderby child.RootCauseID
                                                             select new
                                                             {
                                                                 RCType = child2.RootCauseCategoryID,
                                                                 RCID = child2.RootCauseID,
                                                                 RCTTitle = childRctLanguages2.Title,
                                                                 RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                                             where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child2.RootCauseID
                                                                             select rootCauseTree).FirstOrDefault().IsSelected,

                                                                 AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                             where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child2.RootCauseID
                                                                                     select rootCauseTree).FirstOrDefault().AnalysisComments,


                                                                 GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                                  where rctQuestion.RootCauseId == child2.RootCauseID
                                                                                  && rctQuestion.IsQuestion == false
                                                                                  select rctQuestion).Count(),

                                                                 Children = from child3 in _entityContext.RootCauses
                                                                            join childRctLanguages3 in _entityContext.RootCausesLanguages
                                                                            on child3.RootCauseID equals childRctLanguages3.RootCauseId
                                                                            where child3.ParentID == child2.RootCauseID
                                                                            && childRctLanguages3.LanguageId == languageId
                                                                            orderby child3.RootCauseID
                                                                            select new
                                                                            {
                                                                                RCType = child3.RootCauseCategoryID,
                                                                                RCID = child3.RootCauseID,
                                                                                RCTTitle = childRctLanguages3.Title,
                                                                                RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                                                            where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child3.RootCauseID
                                                                                            select rootCauseTree).FirstOrDefault().IsSelected,
                                                                           
                                                                                AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                                                    where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child3.RootCauseID
                                                                                                    select rootCauseTree).FirstOrDefault().AnalysisComments,

                                                                                GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                                                 where rctQuestion.RootCauseId == child3.RootCauseID && rctQuestion.IsQuestion == false
                                                                                                 select rctQuestion).Count()

                                                                            }
                                                             }

                                              }
                               },
                });

                return rctQuestions;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="causalFactorID"></param>
        /// <returns></returns>
        public object GetTreeNodes(int causalFactorID, int userId)
        {
            try
            {


                //string spanishLanguage = "es";
                string englishLanguage = ForeignLanguage.English_Language.ToString();
                //string frenchlanguage = "fr";
                var users = (from user in _entityContext.Users
                             join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                             where user.UserID == userId
                             select new
                             {
                                 languageId = user.LanguageId,
                                 LangguageName = language.LangaugeCode
                             }).FirstOrDefault();

                int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
                int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));

                string languageName = ((users == null) ? ForeignLanguage.English_Language : users.LangguageName);

                var isTitleAvaliable = (from title in _entityContext.RootCausesLanguages 
                                        where  title.LanguageId == languageId select title).FirstOrDefault();

                if (isTitleAvaliable == null)
                {
                    languageId = englishLangugeId;
                    languageName = ForeignLanguage.English_Language;
                }

                var parentNodeId = _entityContext.RootCauses.FirstOrDefault(a => a.Title == "RootCauseTree").RootCauseID;

                //Fetch the rct title,rct id and all dictionary questions related to all the child nodes of the provided rct id
                var rctQuestions = (
                from rct in _entityContext.RootCauses
                join parentRCTLanguages in _entityContext.RootCausesLanguages
                on rct.RootCauseID equals parentRCTLanguages.RootCauseId
                where parentRCTLanguages.LanguageId == languageId && 
                 rct.ParentID == parentNodeId && rct.RootCauseCategoryID == 5
                orderby rct.RootCauseID
                select new
                {
                    EnglishTitle = rct.Title,
                    RCType = rct.RootCauseCategoryID,
                    RCID = rct.RootCauseID,
                    RCTTitle = parentRCTLanguages.Title,
                    IsSelected = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseID == rct.RootCauseID && a.CausalFactorID == causalFactorID).IsSelected,
                    AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                        where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == rct.RootCauseID
                                        select rootCauseTree).FirstOrDefault().AnalysisComments,
                    GuidanceCount = (from rctQuestion in _entityContext.RCTDictionaryQuestions
                                        where rctQuestion.RootCauseId == rct.RootCauseID && rctQuestion.IsQuestion == false
                                        select rctQuestion).Count(),
                    UserLanguageName = languageName

                });

                return rctQuestions;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }


    }
}
