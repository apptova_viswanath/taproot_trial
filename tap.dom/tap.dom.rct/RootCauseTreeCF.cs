﻿/********************************************************************************
 *  File Name   : RootCauseTreeCF.cs
 *  Description : This file contains all the database related code for the RootCauseTree table
 *  Created on  : N/A
 *  Extra Info : Foreign Language supported by TapRoot: English, Spanish, Russian, French, German, Portuguese
 *  For CA Help : French, German and Portuguese > English
 *******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;
using tap.dom.hlp;

namespace tap.dom.rct
{
    public class RootCauseTreeCF
    {

        const string AUTUMN_SEASON = "Autumn";


        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();

        
        /// <summary>
        /// Get all rct questions based on parent rct id
        /// </summary>
        /// <returns></returns>
        public object GetRCTGuidance(int rootCauseID, string linkType, int userId)
        {
          
            try
            {

                string englishLanguage = "en";

                var users = (from user in _entityContext.Users
                             join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                             where user.UserID == userId
                             select new
                             {
                                 languageId = user.LanguageId,
                                 LangguageName = language.LangaugeCode
                             }).FirstOrDefault();

                int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
               
                
                int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));
                int selecteLanguageId = Convert.ToInt32(users.languageId);
                string languageName = ((users == null) ? englishLanguage : users.LangguageName);
                string selectedLanguageName = users.LangguageName;
              var languageQuesionsCount=  (from rowcount
                          in _entityContext.RCTDictionaryQuestionLanguages
                                           where rowcount.LanguageId == languageId
                                           select rowcount).Count();

                if(languageQuesionsCount <= 0 )
                {
                    languageId = englishLangugeId;
                    languageName = englishLanguage;
                }

                if (linkType == "RCTDictionaryLink")
                {
                    var guidance = from rootCuases in _entityContext.RootCauses
                                   join rcttitles in _entityContext.RootCausesLanguages
                                              on rootCuases.RootCauseID equals rcttitles.RootCauseId

                                   where rootCuases.RootCauseID == rootCauseID && rcttitles.LanguageId == languageId
                               select new
                               {
                                   Title = rcttitles.Title,
                                   Guidance = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                              join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages  
                                              on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                              
                                              where rctQuestion.RootCauseId == rootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                              && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                              select rctQuestionLanguages.HTMLDescription,
                                             
                                              LinkType = linkType,
                                   RCTLanguageName = selectedLanguageName
                                              
                               };
                    
                     return guidance;
                }
                else if (linkType == "RCTQuestionLink")
                {
                    var guidance = from rootCuases in _entityContext.RootCauses
                                   join rcttitles in _entityContext.RootCausesLanguages
                                              on rootCuases.RootCauseID equals rcttitles.RootCauseId

                                   where rootCuases.RootCauseID == rootCauseID && rcttitles.LanguageId == languageId
                                   select new
                                   {
                                       Title = rcttitles.Title,
                                       Guidance = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                  join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                  on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                                  where rctQuestion.RootCauseId == rootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                                  && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                                  select rctQuestionLanguages.HTMLDescription,

                                       LinkType = linkType,
                                       RCTLanguageName = selectedLanguageName

                                   };

                    return guidance;
                }
                else
                {

                    int correctiveType = Convert.ToInt32(tap.dom.hlp.Enumeration.DictionaryTypes.CorrectiveAction);
                    var CAHCount = (from rowcount
                       in _entityContext.CAHeplerLanguages
                                    where rowcount.LanguageId == selecteLanguageId
                                    select rowcount).Count();


                    if (CAHCount <= 0)
                    {
                        languageId = englishLangugeId;
                        languageName = englishLanguage;
                    }
                    else
                    {
                        languageId = selecteLanguageId;
                        languageName = selectedLanguageName;
                    }


                    var guidance =  from rootCuases in _entityContext.RootCauses
                               where rootCuases.RootCauseID == rootCauseID
                               select new
                               {
                                   Title = rootCuases.Title,                        
                                   //Guidance = (from dictionary in _entityContext.DictionaryDefinition
                                   //             where dictionary.RootCauseID == rootCauseID &&
                                   //                 dictionary.DictionaryTypeID == correctiveType
                                   //            select dictionary.HTMLDescription).FirstOrDefault(),
                                   Guidance = (from CAH in _entityContext.CAHeplerLanguages
                                               where CAH.RootCauseID == rootCauseID &&
                                                   CAH.LanguageId == languageId
                                               select CAH.HTMLDescription).FirstOrDefault(),
                                   LinkType = linkType,
                                   RCTLanguageName = selectedLanguageName
                               };

                    return guidance;
                }   

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }
     

       /// <summary>
        /// Get all rct questions based on parent rct id, need optimization
       /// </summary>
       /// <param name="rootCauseID"></param>
       /// <param name="causalFactorID"></param>
       /// <returns></returns>
        public object GetRCTQuestions(int rootCauseID, int causalFactorID, int userId)
        {
       
            //string spanishLanguage = "es";
            string englishLanguage = ForeignLanguage.English_Language.ToString();
            //string frenchlanguage = "fr";
            //string russianlanguage = "ru";
            var users = (from user in _entityContext.Users
                     join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                     where user.UserID == userId
                     select new
                     {
                         languageId = user.LanguageId,
                         LangguageName = language.LangaugeCode
                     }).FirstOrDefault();

            int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
            
            //int languageId = ((users == null) ? englishLangugeId : ((users.LangguageName != englishLanguage) && (users.LangguageName != spanishLanguage)) ? englishLangugeId :
            //    Convert.ToInt32(users.languageId));

            //string languageName = ((users == null) ? englishLanguage :((users.LangguageName != englishLanguage) && (users.LangguageName != spanishLanguage))? englishLanguage: users.LangguageName);
            int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));

            string languageName = ((users == null) ? ForeignLanguage.English_Language : users.LangguageName);

            var isTitleAvaliable = (from title in _entityContext.RootCausesLanguages where title.RootCauseId == rootCauseID && title.LanguageId == languageId select title).FirstOrDefault();

            if(isTitleAvaliable == null)
            {
                languageId = englishLangugeId;
                languageName = ForeignLanguage.English_Language;
            }


            try
            {

                string humanPerformanceTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_HUMAN_PERFORMANCE_DIFFICULTY && a.LanguageId == languageId).Title;
                string equipmentTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_EQUIPMENT_DIFFICULTY && a.LanguageId == languageId).Title;
                string naturalSabotageTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_NATURAL_SABOTAGE && a.LanguageId == languageId).Title;
                string otherTitleText = _entityContext.RootCausesLanguages.FirstOrDefault(a => a.RootCauseId == RootCausesNodes.RCID_OTHER && a.LanguageId == languageId).Title;

                //Fetch the rct title,rct id and all dictionary questions related to all the child nodes of the provided rct id
                var rctQuestions = (
                from parentRCT in _entityContext.RootCauses
                join parentRCTLanguages in _entityContext.RootCausesLanguages
                on parentRCT.RootCauseID equals parentRCTLanguages.RootCauseId
                where parentRCT.RootCauseID == rootCauseID && parentRCTLanguages.LanguageId == languageId
                orderby parentRCT.RootCauseID
                select new
                {

                    EnglishTitle = from a in _entityContext.RootCausesLanguages where a.RootCauseId == parentRCT.RootCauseID && a.LanguageId == languageId select a.Title,
                    RCType = parentRCT,
                    RCID = parentRCT.RootCauseID,
                    RCTTitle = parentRCTLanguages.Title,
                    RCTLanguageName = languageName,
                    RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == parentRCT.RootCauseID
                                select rootCauseTree).FirstOrDefault().IsSelected,
                    Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                where rctQuestion.RootCauseId == parentRCT.RootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                select new
                                {
                                    QuestionID = rctQuestion.RCTDictionaryQuestionID,
                                    Question = rctQuestionLanguages.HTMLDescription,
                                    Response = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                rctt1.CausalFactorID == causalFactorID
                                                select response).FirstOrDefault().IsSelected
                                },
                    AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                        where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == parentRCT.RootCauseID
                                        select rootCauseTree).FirstOrDefault().AnalysisComments,

                    GuidanceCount = (from rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                     join rctQuestion in _entityContext.RCTDictionaryQuestions
                                     on rctQuestionLanguages.RCTDictionaryQuestionId equals rctQuestion.RCTDictionaryQuestionID
                                     where rctQuestion.RootCauseId == parentRCT.RootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                     && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                     select rctQuestion).Count(),

                    Children = from rct in _entityContext.RootCauses
                               join rctLanguages in _entityContext.RootCausesLanguages
                               on rct.RootCauseID equals rctLanguages.RootCauseId
                               where rct.ParentID == rootCauseID && rctLanguages.LanguageId == languageId
                               orderby rct.RootCauseID
                               select new
                               {
                                   RCID = rct.RootCauseID,
                                   RCTTitle = rctLanguages.Title,
                                   RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                               where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == rct.RootCauseID
                                               select rootCauseTree).FirstOrDefault().IsSelected,
                                   Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                               join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                               on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                               where rctQuestion.RootCauseId == rct.RootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                               select new
                                               {
                                                   QuestionID = rctQuestion.RCTDictionaryQuestionID,
                                                   Question = rctQuestionLanguages.HTMLDescription,
                                                   Response = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                               join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                               where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                               rctt1.CausalFactorID == causalFactorID
                                                               select response).FirstOrDefault().IsSelected
                                               },

                                   AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                       where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == rct.RootCauseID
                                                       select rootCauseTree).FirstOrDefault().AnalysisComments,
                                   GuidanceCount = (from rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                    join rctQuestion in _entityContext.RCTDictionaryQuestions
                                                    on rctQuestionLanguages.RCTDictionaryQuestionId equals rctQuestion.RCTDictionaryQuestionID
                                                    where rctQuestion.RootCauseId == rct.RootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                                    && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                                    select rctQuestion).Count(),
    
                                   Children = from child in _entityContext.RootCauses
                                              join childRctLanguages in _entityContext.RootCausesLanguages on child.RootCauseID equals childRctLanguages.RootCauseId
                                              
                                              where child.ParentID == rct.RootCauseID

                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DIFICULTAD DE DESEMPEÑO HUMANO" : (languageName == frenchlanguage) ? "Difficulté de la performance humaine" : "Human Performance Difficulty")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DIFICULTAD CON EL EQUIPO" : (languageName == frenchlanguage) ? "Équipement Difficulté" : "Equipment Difficulty")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "DESASTRE NATURAL/SABOTAJE" : (languageName == frenchlanguage) ? "Catastrophe naturelle / Sabotage" : "Natural Disaster / Sabotage")
                                              //&& rctLanguages.Title != ((languageName == spanishLanguage) ? "OTRO(ESPECIFICAR)" : (languageName == frenchlanguage) ? "Autre (précisez )" : "Other (Specify)")                                           
                                              && rctLanguages.Title != humanPerformanceTitleText
                                              && rctLanguages.Title != equipmentTitleText
                                              && rctLanguages.Title != naturalSabotageTitleText
                                              && rctLanguages.Title != otherTitleText
                                              && childRctLanguages.LanguageId == languageId
                                              orderby child.RootCauseID
                                              select new
                                              {
                                                  RCID = child.RootCauseID,
                                                  RCTTitle = childRctLanguages.Title,
                                                  RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                              where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child.RootCauseID
                                                              select rootCauseTree).FirstOrDefault().IsSelected,
                                                  Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                              join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                               on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                                              where rctQuestion.RootCauseId == childRctLanguages.RootCauseId// child.RootCauseID 
                                                              && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                                              select new
                                                              {

                                                                  QuestionID = rctQuestion.RCTDictionaryQuestionID,
                                                                  Question = rctQuestionLanguages.HTMLDescription,
                                                                  Response = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                                              join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                                              where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                                              rctt1.CausalFactorID == causalFactorID
                                                                              select response).FirstOrDefault().IsSelected
                                                              },

                                                  AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                      where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child.RootCauseID
                                                                      select rootCauseTree).FirstOrDefault().AnalysisComments,
                                                  GuidanceCount = (from rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                                   join rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                   on rctQuestionLanguages.RCTDictionaryQuestionId equals rctQuestion.RCTDictionaryQuestionID
                                                                   where rctQuestion.RootCauseId == child.RootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                                                   && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                                                   select rctQuestion).Count(),
                                                 
                                                  Children = from child2 in _entityContext.RootCauses
                                                             join childRctLanguages2 in _entityContext.RootCausesLanguages
                                                              on child2.RootCauseID equals childRctLanguages2.RootCauseId
                                                             where child2.ParentID == child.RootCauseID
                    
                                                             && childRctLanguages2.LanguageId == languageId
                                                             orderby child.RootCauseID
                                                             select new
                                                             {
                                                                 RCID = child2.RootCauseID,
                                                                 RCTTitle = childRctLanguages2.Title,
                                                                 RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                                             where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child2.RootCauseID
                                                                             select rootCauseTree).FirstOrDefault().IsSelected,

                                                                 Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                             join rctQuestionLanguages1 in _entityContext.RCTDictionaryQuestionLanguages
                                                                              on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages1.RCTDictionaryQuestionId
                                                                             where rctQuestion.RootCauseId == child2.RootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages1.LanguageId == languageId
                                                                             select new
                                                                             {

                                                                                 QuestionID = rctQuestion.RCTDictionaryQuestionID,
                                                                                 Question = rctQuestionLanguages1.HTMLDescription,
                                                                                 Response = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                                                             join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                                                             where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                                                             rctt1.CausalFactorID == causalFactorID
                                                                                             select response).FirstOrDefault().IsSelected
                                                                             },

                                                                 AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                                     where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child2.RootCauseID
                                                                                     select rootCauseTree).FirstOrDefault().AnalysisComments,
                                                                 GuidanceCount = (from rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                                                  join rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                                  on rctQuestionLanguages.RCTDictionaryQuestionId equals rctQuestion.RCTDictionaryQuestionID
                                                                                  where rctQuestion.RootCauseId == child2.RootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                                                                  && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                                                                  select rctQuestion).Count(),

                                                                 Children = from child3 in _entityContext.RootCauses
                                                                            join childRctLanguages3 in _entityContext.RootCausesLanguages
                                                                           on child3.RootCauseID equals childRctLanguages3.RootCauseId
                                                                            where child3.ParentID == child2.RootCauseID
                                                                          
                                                                              && childRctLanguages3.LanguageId == languageId
                                                                            orderby child3.RootCauseID
                                                                            select new
                                                                            {
                                                                                RCID = child3.RootCauseID,
                                                                                RCTTitle = childRctLanguages3.Title,
                                                                                RCTValue = (from rootCauseTree in _entityContext.RootCauseTree
                                                                                            where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child3.RootCauseID
                                                                                            select rootCauseTree).FirstOrDefault().IsSelected,
                                                                                Questions = from rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                                            join rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                                                            on rctQuestion.RCTDictionaryQuestionID equals rctQuestionLanguages.RCTDictionaryQuestionId
                                                                                            where rctQuestion.RootCauseId == child3.RootCauseID && rctQuestion.IsQuestion == true && rctQuestionLanguages.LanguageId == languageId
                                                                                            select new
                                                                                            {
                                                                                                Question = rctQuestionLanguages.HTMLDescription,
                                                                                                QuestionID = rctQuestion.RCTDictionaryQuestionID,

                                                                                                Response = (from response in _entityContext.RCTDictionaryQuestionResponse
                                                                                                            join rctt1 in _entityContext.RootCauseTree on response.RootCauseTreeID equals rctt1.RootCauseTreeID
                                                                                                            where response.RCTDictionaryQuestionID == rctQuestion.RCTDictionaryQuestionID &&
                                                                                                            rctt1.CausalFactorID == causalFactorID
                                                                                                            select response).FirstOrDefault().IsSelected
                                                                                            },
                                                                                AnalysisComments = (from rootCauseTree in _entityContext.RootCauseTree
                                                                                                    where rootCauseTree.CausalFactorID == causalFactorID && rootCauseTree.RootCauseID == child3.RootCauseID
                                                                                                    select rootCauseTree).FirstOrDefault().AnalysisComments,
                                                                                GuidanceCount = (from rctQuestionLanguages in _entityContext.RCTDictionaryQuestionLanguages
                                                                                                 join rctQuestion in _entityContext.RCTDictionaryQuestions
                                                                                                 on rctQuestionLanguages.RCTDictionaryQuestionId equals rctQuestion.RCTDictionaryQuestionID
                                                                                                 where rctQuestion.RootCauseId == child3.RootCauseID && rctQuestion.IsQuestion == false && rctQuestionLanguages.LanguageId == languageId
                                                                                                 && !rctQuestionLanguages.HTMLDescription.Contains("missing") && !rctQuestionLanguages.HTMLDescription.Contains("googlefrench") 
                                                                                                 select rctQuestion).Count(),

                                                                            }
                                                             }

                                              }
                               },
                });

                return rctQuestions;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;

        }


        /// <summary>
        /// Get the generic causes count
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <returns></returns>
        public object GetGenericCauseCount(int causalFactorId)
        {

            var rcType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == "Root Cause" select rootCauseType).FirstOrDefault();
            var nearRCType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == "Near" select rootCauseType).FirstOrDefault();

            int rootCauseCategoryID = Convert.ToInt32(rcType.RootCauseCategoryID);
            int nearRootCauseCategoryID = Convert.ToInt32(nearRCType.RootCauseCategoryID);


            var genericCausesCount = (from rootCauseTree in _entityContext.RootCauseTree.AsEnumerable()
                                      join rootCauses in _entityContext.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                        equals rootCauses.RootCauseID
                                      //where genericRCT.CausalFactorID == causalFactorId && genericRCT.IsSelected == true
                                      //  && rootCauses.RootCauseCategoryID == 3
                                      //select genericRCT).ToList().Count();        
                                     where  (rootCauseTree.CausalFactorID == causalFactorId && rootCauseTree.IsSelected == true 
                                              && rootCauses.RootCauseCategoryID == rootCauseCategoryID) ||

                                             ((rootCauseTree.CausalFactorID == causalFactorId
                                              && rootCauseTree.IsSelected == true && rootCauses.RootCauseCategoryID == nearRootCauseCategoryID)
                                              && ((from rootCauses1 in _entityContext.RootCauses 
                                                   where rootCauses1.ParentID == rootCauses.RootCauseID && rootCauses1.RootCauseCategoryID == rootCauseCategoryID 
                                                   select rootCauses1).Count() ==                                  
                                                  (from rootCauseTree1 in _entityContext.RootCauseTree
                                                   where rootCauseTree1.CausalFactorID == causalFactorId && rootCauseTree1.IsSelected == false &&
                                                   rootCauseTree1.RootCauses.ParentID == rootCauses.RootCauseID
                                                   select rootCauseTree1).Count()))
                                   select rootCauseTree).ToList().Count();  
         
            return genericCausesCount;
                                  
        }


        /// <summary>
        /// Save rct analysis data
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <param name="rootCauseID">root cause tree node id</param>
        /// <param name="analysisComments">analysis comments added</param>
        /// <returns></returns>
        public void SaveRCTAnalysis(int causalFactorID, int rootCauseID, string analysisComments)
        {

            //Get root cause tree by causal factor id and rct id
            tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseID == rootCauseID && a.CausalFactorID == causalFactorID);

            if (rootCauseTree == null)
            {

                rootCauseTree = new tap.dat.RootCauseTree();
                rootCauseTree.CausalFactorID = causalFactorID;
                rootCauseTree.RootCauseID = Convert.ToInt32(rootCauseID);
                rootCauseTree.IsSelected = null;
                rootCauseTree.AnalysisComments = analysisComments;
                _entityContext.AddToRootCauseTree(rootCauseTree);

            }
            else
            {
                rootCauseTree.AnalysisComments = analysisComments;
            }

            _entityContext.SaveChanges(true);

        }


        /// <summary>
        /// Save rct node status into database
        /// </summary>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        /// <param name="rootCauseID">root cause tree node id</param>
        /// <param name="checkedValue">checked value of node</param>
        /// <param name="questionResponse">user response</param>
        public object SaveRCTQuestions(int causalFactorID, int rootCauseID, bool? checkedValue, string questionResponse, bool isVisualRCT, bool allQuestionUnselected)
        {

            var genericCausesCount = 0;

            try
            {
                //Get root cause tree by causal factor id and rct id
                tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault(a => a.RootCauseID == rootCauseID && a.CausalFactorID == causalFactorID);

                //If question answered by user are all blank, nothing has been selected as true/false
                if (checkedValue == null && ((!questionResponse.Contains("true")) && (!questionResponse.Contains("false"))))
                {
                    if (questionResponse != string.Empty)
                    {

                        SaveRCTQuestionResponse(questionResponse, rootCauseTree.RootCauseTreeID);
                    }
                    else
                    {

                        //Get the root causes
                        IList<tap.dat.RCTDictionaryQuestionResponse> rctDictionaryQuestionResponses =
                            _entityContext.RCTDictionaryQuestionResponse.Where(a => a.RootCauseTreeID == rootCauseTree.RootCauseTreeID).ToList();

                        foreach (var rctDictionaryQuestionResponse in rctDictionaryQuestionResponses)
                        {
                            _entityContext.DeleteObject(rctDictionaryQuestionResponse);
                        }


                        DeleteRootCauseTreeNode(rootCauseTree);
                    }
                }

                //if null create new or else save rct
                bool isNewInsert = (rootCauseTree == null) ? true : false;
                rootCauseTree = isNewInsert ? new tap.dat.RootCauseTree() : rootCauseTree;

                rootCauseTree.CausalFactorID = causalFactorID;
                rootCauseTree.RootCauseID = Convert.ToInt32(rootCauseID);
                rootCauseTree.IsSelected = checkedValue;

                if (isNewInsert)
                {
                    _entityContext.AddToRootCauseTree(rootCauseTree);
                }

              
                //save changes
                _entityContext.SaveChanges(true);

                if (checkedValue != false)
                {
                    SaveRCTQuestionResponse(questionResponse, rootCauseTree.RootCauseTreeID);                   
                }

                if (checkedValue == false || checkedValue == null)
                {
                    checkedValue = ((rootCauseID == RootCausesNodes.RCID_EQUIPMENT_DIFFICULTY) || (rootCauseID == RootCausesNodes.RCID_HUMAN_PERFORMANCE_DIFFICULTY)) ? null : checkedValue;
                    EraseOrUnSelectAllChildNodes(rootCauseID, causalFactorID, checkedValue);

                    if(checkedValue == false)
                    { 
                        //Get the root causes
                        IList<tap.dat.RCTDictionaryQuestionResponse> rctDictionaryQuestionResponses =
                            _entityContext.RCTDictionaryQuestionResponse.Where(a => a.RootCauseTreeID == rootCauseTree.RootCauseTreeID).ToList();

                        foreach (var rctDictionaryQuestionResponse in rctDictionaryQuestionResponses)
                        {
                            _entityContext.DeleteObject(rctDictionaryQuestionResponse);                        
                        }
                        _entityContext.SaveChanges(true);
                    }
                }

                //VISUALRCT BLOCKS
                if (isVisualRCT) {

                    if (checkedValue == false || checkedValue == null)
                    {
                        checkedValue = ((rootCauseID == RootCausesNodes.RCID_EQUIPMENT_DIFFICULTY) || (rootCauseID == RootCausesNodes.RCID_HUMAN_PERFORMANCE_DIFFICULTY)) ? null : checkedValue;
                        EraseOrUnSelectAllChildNodes(rootCauseID, causalFactorID, checkedValue);

                        if(checkedValue == false)
                        {

                            tap.dat.RootCauses rc = _entityContext.RootCauses.Where(a => a.RootCauseID == rootCauseID).FirstOrDefault();
                            if ((rc.RootCauseCategoryID == 3 || rc.RootCauseCategoryID == 1) && allQuestionUnselected)
                            {
                                SaveRCTNodeStatus(causalFactorID, Convert.ToInt32(rc.ParentID), true);
                            }

                        }
                    }
                    else if(checkedValue == true)
                    {
                        SelectAllParentNodes(causalFactorID, rootCauseID);
                    }

                }
                //Save the transaction
                var causalFactorName = from cf in _entityContext.CausalFactors where cf.CausalFactorID == causalFactorID select cf.Name;
                string message = " Root causes updated for causal factor " + causalFactorName;
                //transaction.Transaction.SaveTransaction(null, SessionService.UserId, null,
                //                                        Enumeration.TransactionCategory.Update, message);
                
                ////Get generic cause count
                //genericCausesCount = (from genericRCT in _entityContext.RootCauseTree.AsEnumerable()
                //                      join rootCauses in _entityContext.RootCauses.AsEnumerable()
                //                      on genericRCT.RootCauseID equals rootCauses.RootCauseID
                //                      where genericRCT.CausalFactorID == causalFactorID && genericRCT.IsSelected == true
                //                       && rootCauses.RootCauseCategoryID == 3
                //                      select genericRCT).ToList().Count();
            
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            //return generic cause count;
            return GetGenericCauseCount(causalFactorID);


        }

        public void SelectAllParentNodes(int causalFactorID, int rootCauseID)
        {

            tap.dat.RootCauses rct =
                          _entityContext.RootCauses.Where(a => a.RootCauseID == rootCauseID).FirstOrDefault();


            tap.dat.RootCauses parentRCT =
                            _entityContext.RootCauses.Where(a => a.RootCauseID == rct.ParentID).FirstOrDefault();

            SaveRCTNodeStatus(causalFactorID, parentRCT.RootCauseID, true);

            if (parentRCT.RootCauseCategoryID != 2) { 
                SelectAllParentNodes(causalFactorID, parentRCT.RootCauseID);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="causalFactorID"></param>
        /// <param name="status"></param>
        /// <param name="rootCauseId"></param>
        public void SaveRCTNodeStatus(int causalFactorID,int rootCauseId,  bool? status)
        {

               tap.dat.RootCauseTree rctTree = _entityContext.RootCauseTree.FirstOrDefault
                                                 (a => a.CausalFactorID == causalFactorID && a.RootCauseID == rootCauseId);

                
                if (rctTree == null)
                {
                    rctTree = new tap.dat.RootCauseTree();
                    rctTree.CausalFactorID = causalFactorID;
                    rctTree.RootCauseID = rootCauseId;
                    rctTree.IsSelected = status;
                    _entityContext.AddToRootCauseTree(rctTree);
                }
                else
                {
                    rctTree.IsSelected = status;
                }

                _entityContext.SaveChanges(true);

        }

        public void EraseOrUnSelectAllChildNodes(int rootCauseID, int causalFactorID, bool? status)
        {

            IList<tap.dat.RootCauses> childNodes =
                            _entityContext.RootCauses.Where(a => a.ParentID == rootCauseID).ToList();


            foreach (var nodes in childNodes)
            {

                SaveRCTNodeStatus(causalFactorID, nodes.RootCauseID, status);               

                EraseOrUnSelectAllChildNodes(nodes.RootCauseID, causalFactorID, status);
            }


        }

        /// <summary>
        ///  Save rct question status into database
        /// </summary>
        /// <param name="rootCauseTreeID">root cause tree node id</param>
        /// <param name="response">user response</param>
        public void  SaveRCTQuestionResponse(string response, int rootCauseTreeID)
        {

            if (response != string.Empty) { 

                string[] questionResponse = response.Split(',');
                int questionID = 0;
                bool? checkedValue = false;
                string userSelection = string.Empty;

                //Get  the selected rct ids and related checked status and save to database
                for (int i = 0; i < questionResponse.Length; i++)
                {

                    //Get rct id and the checked state
                    questionID = Convert.ToInt32(questionResponse[i].Split(':')[0]);
                    userSelection = questionResponse[i].Split(':')[1];

                    if (userSelection != "null")
                    {
                        checkedValue = Convert.ToBoolean(userSelection);
                    }
                    else
                    {
                        checkedValue = null;
                    }
                                        
                    tap.dat.RCTDictionaryQuestionResponse rctDictionaryQuestionResponse = _entityContext.RCTDictionaryQuestionResponse.FirstOrDefault
                                                    (a => a.RootCauseTreeID == rootCauseTreeID && a.RCTDictionaryQuestionID == questionID);


                    //Delete rctquestion record
                    if (rctDictionaryQuestionResponse != null)
                    {
                        if (checkedValue != null)
                        {
                            rctDictionaryQuestionResponse.IsSelected = checkedValue;
                        }
                        else
                        {
                            _entityContext.DeleteObject(rctDictionaryQuestionResponse);
                        }
                    }
                    else
                    {

                        //save the question status
                        if (checkedValue != null)
                        {
                            rctDictionaryQuestionResponse = new tap.dat.RCTDictionaryQuestionResponse();
                            rctDictionaryQuestionResponse.RCTDictionaryQuestionID = questionID;
                            rctDictionaryQuestionResponse.IsSelected = checkedValue;
                            rctDictionaryQuestionResponse.RootCauseTreeID = rootCauseTreeID;
                            _entityContext.AddToRCTDictionaryQuestionResponse(rctDictionaryQuestionResponse);
                        }
                    }

                    _entityContext.SaveChanges(true);

                }
            }
        }


        /// <summary>
        /// Delete Tab related Data
        /// </summary>
        /// <param name="tabIds">Root cause tree node ids related to the tabs to be hidden</param>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        public void DeleteTabData(string tabIds,int causalFactorID)
        {

            try
            {
                var ids = tabIds.Split(',');

                for (int i = 0; i <= ids.Length - 1; i++)
                {
                    
                    var rctId = ids[i];

                    if (rctId != "")
                    {

                        int id = Convert.ToInt32(rctId);

                        //Get root cause tree by causal factor and rct id
                        tap.dat.RootCauseTree rootCauseTree = _entityContext.RootCauseTree.FirstOrDefault
                                                                (a => a.CausalFactorID == causalFactorID && a.RootCauseID == id);

                        if (rootCauseTree != null)
                        {
                            _entityContext.DeleteObject(rootCauseTree);
                            _entityContext.SaveChanges(true); 
                            
                             // Delete all rct records and its child record added for a causal factor
                            DeleteRecursivelyAllChild(id, causalFactorID);
                         
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
           
        }


        /// <summary>
        /// Delete all rct records and its child record added for a causal factor
        /// </summary>
        /// <param name="rootCauseId">root cause tree node id</param>
        /// <param name="causalFactorID">causal factor id for the root cause tree node</param>
        public void DeleteRecursivelyAllChild(int rootCauseId, int causalFactorID)
        {

            //Get the root causes
            IList<tap.dat.RootCauses> rootCauses = _entityContext.RootCauses.Where(a => a.ParentID == rootCauseId).ToList();

            foreach (var rct in rootCauses)
            {
                tap.dat.RootCauseTree rctTree = _entityContext.RootCauseTree.FirstOrDefault
                                                 (a => a.CausalFactorID == causalFactorID && a.RootCauseID == rct.RootCauseID);

                if (rctTree != null)
                {
                    _entityContext.DeleteObject(rctTree);
                    _entityContext.SaveChanges(true);                   
                }

                DeleteRecursivelyAllChild(rct.RootCauseID, causalFactorID);
            }

        }

        
        /// <summary>
        ///  Delete root cause tree node
        /// </summary>       
        /// <param name="rootCauseTree">rootCauseTree record to be deleted</param>
        public void DeleteRootCauseTreeNode(tap.dat.RootCauseTree rootCauseTree)
        {
            
            try
            {
                _entityContext.DeleteObject(rootCauseTree);
                _entityContext.SaveChanges(true);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        /// <summary>
        /// Update status of RCT
        /// </summary>
        /// <param name="causalFactorId">causal factor id for the root cause tree node</param>
        /// <param name="status">status of the root cause tree node</param>
        /// <returns></returns>
        public void UpdateRCTStatus(int causalFactorId, string status)
        {

            bool isComplete = (status == "checked") ? true : false;

            //Get root cause tree by causal factor id and rct id
            tap.dat.CausalFactors causalFactor = _entityContext.CausalFactors.FirstOrDefault(a => a.CausalFactorID == causalFactorId);
            causalFactor.IsComplete = isComplete;
            _entityContext.SaveChanges(true);

        }
        public bool SaveRCTMode(string userId, bool isRCTClassicMode)
        {
                      
            try
            {

                int userID = Convert.ToInt32(userId);

                tap.dat.UserSettings userSettings = new tap.dat.UserSettings();
                userSettings = _entityContext.UserSettings.Where(x => x.UserId == userID).FirstOrDefault();

                if (userSettings != null)
                {
                   
                    userSettings.IsRCTClassicMode = isRCTClassicMode;

                }
                else
                {
                    userSettings = new tap.dat.UserSettings();
                    userSettings.UserId = userID;
                    userSettings.IsRCTClassicMode = isRCTClassicMode;
                    _entityContext.AddToUserSettings(userSettings);
                }

                _entityContext.SaveChanges();

                return isRCTClassicMode;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isRCTClassicMode;

        }

        public bool CheckIfRCTClassicMode(string userId)
        {

            bool isRCTClassicMode = false;
            
            try
            {

                int userID = Convert.ToInt32(userId);
                var userSettings = _entityContext.UserSettings.Where(x => x.UserId == userID).FirstOrDefault();
                isRCTClassicMode = (userSettings == null) ? false : userSettings.IsRCTClassicMode;
            
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isRCTClassicMode;
        }

        public string GetUserLanguage(string userId)
        {

            string languageCode = ForeignLanguage.English_Language;

            try
            {

                int userID = Convert.ToInt32(userId); 
                var users = (from user in _entityContext.Users
                             join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                             where user.UserID == userID
                            select new
                            {
                                languageId = user.LanguageId,
                                LangguageCode = language.LangaugeCode
                            }).FirstOrDefault();


                languageCode = ((users == null) ? ForeignLanguage.English_Language : users.LangguageCode);
            

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return languageCode;
        }


        //Check if any RCT completed for snapchart or not
        public bool AnyRCTComplete(string eventIdStr)
        {

            bool isAnyRCTCompleted = false;
            
            try
            {
                tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
              
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventIdStr));
                if (eventId.Equals(0))
                    return false;

                if (eventId != 0)
                {
                    var snapchartInfo = (from snapchart in _entityContext.SnapCharts where snapchart.EventID == eventId select snapchart).AsEnumerable();

                    if (snapchartInfo.ToList().Count() != 0)
                    {
                        var seasonId = snapchartInfo.LastOrDefault().SeasonID;
                        var isComplete = snapchartInfo.FirstOrDefault().IsComplete;

                        //Check for causal factors exists for this snapchart id.
                        var isSC = _entityContext.CausalFactors.FirstOrDefault(a => a.SnapChartID == snapchartInfo.FirstOrDefault().SnapChartID);
                        var season = snapchartInfo.LastOrDefault().SeasonID;

                        var autumnSeasonID = (from season1 in _entityContext.Seasons where season1.SeasonName == AUTUMN_SEASON select season1).FirstOrDefault().SeasonID;


                        if (isSC != null && season == autumnSeasonID)
                        {

                            isAnyRCTCompleted = (from x in _entityContext.CausalFactors
                                                  where x.SnapChartID == snapchartInfo.FirstOrDefault().SnapChartID && (x.IsComplete == true)
                                                  select x).AsEnumerable().Count() > 0 ? true : false;

                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return isAnyRCTCompleted;
        }


        //Check ALL RCT completed for snapchart or not
        public bool AllRCTComplete(string eventIdStr)
        {
            try
            {
                tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventIdStr));
                if (eventId.Equals(0))
                    return false;

                int snapChartId = _entityContext.SnapCharts.Where(x => x.EventID == eventId).FirstOrDefault().SnapChartID;

                List<tap.dat.CausalFactors> causal = _entityContext.CausalFactors.Where(x => x.SnapChartID == snapChartId && (x.IsComplete == null || x.IsComplete == false)).ToList();

                if (causal.Count > 0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return false;
        }
    }

}
