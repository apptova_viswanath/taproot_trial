﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.usr.settings
{
    public class UserSettings
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        #region SnapCharT autosave duration Save
        
        /// <summary>
        /// Save/Update the Snapchart auto save duration selected by the user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="autosaveDuration"></param>
        public void SnapChartAutoSaveDurationSet(int userId, int autosaveDuration)
        {
            try
            {                               
                tap.dat.UserSettings snapchartAutosaveSettings = null;

                snapchartAutosaveSettings = _db.UserSettings.FirstOrDefault(a=>a.UserId == userId);
                if (snapchartAutosaveSettings != null)//Update the autosave duration based on user id.
                {
                    snapchartAutosaveSettings.SnapCharTAutoSave = autosaveDuration;
                }
                else//Insert new record for user based auto save duration.
                {
                    snapchartAutosaveSettings = new dat.UserSettings();
                    snapchartAutosaveSettings.UserId = userId;
                    snapchartAutosaveSettings.SnapCharTAutoSave = autosaveDuration;
                    _db.AddToUserSettings(snapchartAutosaveSettings);
                }
                _db.SaveChanges();

                //tap.dat.UserSettings snapchartAutosaveSettings = null;
                //var snapchartAutosaveCount = (from user in _db.UserSettings select user).Count();

                //if (snapchartAutosaveCount > 0)
                //{                    
                //    snapchartAutosaveSettings = _db.UserSettings.FirstOrDefault(a => a.UserId == userId);
                //    snapchartAutosaveSettings.SnapCharTAutoSave = autosaveDuration;
                //}
                //else
                //{
                //    snapchartAutosaveSettings = new dat.UserSettings();
                    
                //    snapchartAutosaveSettings.UserId = userId;
                //    snapchartAutosaveSettings.SnapCharTAutoSave = autosaveDuration;
                //    _db.AddToUserSettings(snapchartAutosaveSettings);
                //}                
               
                //_db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }

        #endregion

        #region Get the latest SnapCharT autosave duration

        /// <summary>
        /// Get the latest SnapCharT autosave duration
        /// </summary>
        /// <returns></returns>
        public int? SnapChartAutoSaveDurationGet(int userId)
        {
            int? SnapChartAutoSaveDuration = -1;
            //int? SnapChartAutoSaveDuration = 0;
            tap.dat.UserSettings usersetting = null;
            usersetting = _db.UserSettings.FirstOrDefault(a=>a.UserId == userId);            

            if (usersetting != null )
                SnapChartAutoSaveDuration = usersetting.SnapCharTAutoSave;            

            return SnapChartAutoSaveDuration;

           //int? SnapChartAutoSaveDuration = 0;   
        
           //IList<tap.dat.UserSettings> usersettings = _db.UserSettings.OrderByDescending(a => a.UserId == userId).ToList();

           //if (usersettings.Count > 0)
           //{
           //    tap.dat.UserSettings usersetting = usersettings.First();
           //    SnapChartAutoSaveDuration = usersetting.SnapCharTAutoSave;
           //}
           
           // return SnapChartAutoSaveDuration;
        }

        #endregion


    }
}
