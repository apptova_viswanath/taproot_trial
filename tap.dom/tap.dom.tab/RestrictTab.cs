﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.tab
{
    public class RestrictTab
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();


        public string RestrictUsersList(int page, int rows, string filter, int ignoreCase, string sortIndex, string sortOrder, int companyID, int tabId)
        {
            if (sortIndex == null)
                sortIndex = "FirstName";

            if (sortOrder == null)
                sortOrder = "desc";
            try
            {

                var details = (from u in _db.Users
                               join
                                   tu in _db.TabUsers on u.UserID equals tu.UserId
                                   into temptu
                               from TU in temptu.Where(a => a.TabId == tabId).DefaultIfEmpty()
                               where u.CompanyID == companyID && u.Active == true
                               select new
                               {
                                   UserId = u.UserID,
                                   FirstName = u.FirstName,
                                   LastName = u.LastName,
                                   hasAccess = (TU.UserId != null) ? true : false
                               });


                if (filter != null && filter.Length > 0)
                {
                    switch (sortIndex)
                    {
                        case "FirstName":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.FirstName).Where(a => a.FirstName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.FirstName).Where(a => a.FirstName.ToLower().Contains(filter.ToLower())));
                            break;
                        case "LastName":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.LastName).Where(a => a.LastName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.LastName).Where(a => a.LastName.ToLower().Contains(filter.ToLower())));
                            break;
                        case "hasAccess":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasAccess == true ? "1" : "0").Where(a => a.FirstName.ToLower().Contains(filter.ToLower())) : details.OrderBy(e => e.hasAccess == true ? "1" : "0").Where(a => a.FirstName.ToLower().Contains(filter.ToLower())));
                            break;
                    }
                }
                else
                {
                    switch (sortIndex)
                    {
                        case "FirstName":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.FirstName) : details.OrderBy(e => e.FirstName));
                            break;
                        case "LastName":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.LastName) : details.OrderBy(e => e.LastName));
                            break;
                        case "hasAccess":
                            details = ((sortOrder.Equals("desc")) ? details.OrderByDescending(e => e.hasAccess == true ? "1" : "0") : details.OrderBy(e => e.hasAccess == true ? "1" : "0"));
                            break;
                    }
                }

                int skipRowsCount = 0;
                if (page != 1)
                    skipRowsCount = (page - 1) * rows;

                #region To Add filter opeartion

                int totalRecords = 0;
                var currentPazeSize = rows.ToString() ?? "15";
                int pageSize = int.Parse(currentPazeSize);

                var data = details.ToList();

                totalRecords = (data != null && data.Count() > 0) ? data.Count() : 0;

                int totalPages = (int)Math.Ceiling((float)totalRecords / (float)pageSize);
                var currentPage1 = page.ToString() ?? "1";

                int currentPageNumber = int.Parse(currentPage1);

                if (currentPageNumber > 1 && data != null && data.Count() > 0)
                    data = data.Skip((currentPageNumber - 1) * pageSize).Take(pageSize).ToList();

                #endregion
                var jsonData = new tap.dom.hlp.jqGridJson();

                jsonData = new tap.dom.hlp.jqGridJson()
                {
                    total = totalPages,
                    page = currentPageNumber,
                    records = totalRecords,
                    rows = (
                        from user in data
                        let UserId = user.UserId.ToString()
                        let FirstName = user.FirstName
                        let LastName = user.LastName
                        let hasAccess = user.hasAccess
                        select new tap.dom.hlp.jqGridRowJson
                        {
                            id = user.UserId.ToString(),
                            cell = new string[] {                            
                            user.UserId.ToString(),
                            user.FirstName,
                            user.LastName,
                            user.hasAccess.ToString()
                            }
                        }).ToArray()
                };

                return _efSerializer.EFSerialize(jsonData);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return string.Empty;
        }
        public string UpdateRestrictedUser(int userId, int tabId, bool isRestricted)
        {
            string result = string.Empty;
            try
            {
                if (userId == 0 || tabId == 0)
                    return null;

                if (isRestricted)
                {
                    tap.dat.TabUsers user = new dat.TabUsers();
                    user = _db.TabUsers.FirstOrDefault(x => x.UserId == userId && x.TabId == tabId);
                    if (user == null)
                    {
                        tap.dat.TabUsers newuser = new dat.TabUsers();
                        newuser.UserId = userId;
                        newuser.TabId = tabId;
                        _db.AddToTabUsers(newuser);
                        _db.SaveChanges();
                    }

                    result = "Restriction Access Updated";
                }
                else
                    result = RemoveRestrictedUser(userId, tabId);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
        public string RemoveRestrictedUser(int userId, int tabId)
        {
            string result = string.Empty;
            try
            {
                if (userId == 0 || tabId == 0)
                    return null;


                tap.dat.TabUsers user = new dat.TabUsers();
                user = _db.TabUsers.FirstOrDefault(x => x.UserId == userId && x.TabId == tabId);

                _db.DeleteObject(user);
                _db.SaveChanges();

                result = "Restriction Access Updated";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return result;
        }
    }
}
