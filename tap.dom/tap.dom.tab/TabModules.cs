﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.adm
{
    public class TabModules
    {
        tap.dat.EFEntity _entityContext = new dat.EFEntity();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public tap.dat.TabModules GetTabModulesByTabIdAndModuleId(int tabId, int moduleId)
        {
            return _entityContext.TabModules.FirstOrDefault(a => a.TabID == tabId && a.ModuleID == moduleId);
        }
        
        /// <summary>
        /// Save tab module relation ship
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="moduleId"></param>
        public void SaveTabModules(int tabId, int moduleId)
        {
            tap.dat.TabModules tabModules = new tap.dat.TabModules();
            tabModules = GetTabModulesByTabIdAndModuleId(tabId,moduleId);

            var item = _entityContext.TabModules.OrderByDescending(a => a.SortOrder).Where(a=> a.ModuleID == moduleId).FirstOrDefault();

            int sortOrder = 0;
            try
            {
                sortOrder = Convert.ToInt32(item.SortOrder);
            }
            catch (Exception ex) {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            sortOrder = sortOrder + 1;

            if (tabModules == null)
            {
                tabModules = new tap.dat.TabModules();
                tabModules.TabID = tabId;
                tabModules.ModuleID = moduleId;
                tabModules.SortOrder = sortOrder;
                _entityContext.AddToTabModules(tabModules);
                _entityContext.SaveChanges();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tabId"></param>
        public void   DeleteTabModulesByTabId(int tabId)
        {
              //Get the tab by id
              IList<tap.dat.TabModules> tabModules = _entityContext.TabModules.Where(a => a.TabID == tabId).ToList();
              foreach (tap.dat.TabModules tabModuleData in tabModules)
              {
                  _entityContext.DeleteObject(tabModuleData);
                  _entityContext.SaveChanges();
              }
        }

        /// <summary>
        /// Delete tab module relationship
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="moduleId"></param>
        public void DeleteTabModules(int tabId, int moduleId)
        {
            tap.dat.TabModules tabModules = new tap.dat.TabModules();
            tabModules = GetTabModulesByTabIdAndModuleId(tabId, moduleId);

            if (tabModules != null)
            {
                _entityContext.DeleteObject(tabModules);
                _entityContext.SaveChanges();
            }
        }        
    }
}
