﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using tap.dat;
using tap.dom.adm;
using tap.dom.gen;
using tap.dom.usr;
//using System.Reflection;

namespace tap.dom.tab
{
    public class CustomFields
    {
        #region For Global Variable
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        List<int> _dateFieldList = new List<int>();
        Dictionary<int, int> _systemList = new Dictionary<int, int>();
        public const string DATA_TYPE_STRING = "string";
        #endregion
        
        #region To Get Field  and Field values based  on Eventid  And  Tabid
        /// <summary>
        /// description-To create customfields based on Tabid
        /// </summary>
        /// <param name="eventid"></param>
        /// <param name="tabid"></param>
        /// <param name="includefieldvalue"></param>
        /// <returns></returns>
        public string BuildHTMLPage(string eventID, int tabId, bool includeFieldvalue, string virtualDirectory,string companyID)
        {

            StringBuilder htmlPageContent = new StringBuilder();

            // below two variables are added to fix the speed issue
            int listDisplayType = 0;

            try
            {
                 int eventId=0;

                if(eventID!="0")
                     eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                var fields = _db.Fields.Where(a => a.TabID == tabId && a.IsActive == true);
                var fldValues = _db.FieldValues.Where(a => a.EventID == eventId);

                var customFieldWithValue = from a in fields
                                           join b in fldValues
                                            on a.FieldID equals b.FieldID into temp
                                           from t in temp.DefaultIfEmpty()
                                           orderby a.SortOrder
                                           let listname = _db.SystemLists.FirstOrDefault(p => p.ListID == a.ListID).ListName
                                           select new
                                           {
                                               FieldID = a.FieldID,
                                               TabID = a.TabID,
                                               DisplayName = a.DisplayName,
                                               SortOrder = a.SortOrder,
                                               Datatype = a.Datatype,
                                               ControlType = a.ControlType,
                                               MaxLength = a.MaxLength,
                                               IsRequired = a.IsRequired,
                                               ListID = a.ListID,
                                               ListName = listname,
                                               fieldValue = t.FieldValue,
                                               Description = a.Description
                                           };



                if (customFieldWithValue != null)
                {

                    htmlPageContent.Append("<div id='ulCustomField' >");

                    foreach (var fld in customFieldWithValue)
                    {

                        listDisplayType = GetDisplayType(fld.ListID);

                        if (fld.ControlType == tap.dom.hlp.Enumeration.ControlType.Selectmultiplefromlist.ToString())
                        {
                            listDisplayType = (int)tap.dom.hlp.Enumeration.Display.TreeviewDisplay;
                        }

                        //to filter the  Control  with DateTime and Date                       
                        if ((fld.Datatype == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || fld.Datatype == tap.dom.hlp.Enumeration.DataType.Date.ToString()) && fld.ControlType.Replace(" ","") == tap.dom.hlp.Enumeration.ControlType.TextBox.ToString())
                            _dateFieldList.Add(fld.FieldID);

                        //to filter the  Control with treeview
                        if ((fld.ControlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectonefromlist.ToString() ||
                            fld.ControlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectmultiplefromlist.ToString())
                            && listDisplayType == (int)tap.dom.hlp.Enumeration.Display.TreeviewDisplay
                            )
                            _systemList.Add(Convert.ToInt32(fld.FieldID), Convert.ToInt32(fld.ListID));

                        htmlPageContent.Append(" <div class='ClearAll '>" + " <div class='MarginTop10 '>" + BuildDisplayname(fld.DisplayName, Convert.ToInt32(fld.FieldID), Convert.ToBoolean(fld.IsRequired), fld.Datatype) + " </div>");

                        htmlPageContent.Append(" <div class='MarginTop10 '>");
                        htmlPageContent.Append(BuildHTMLControl(fld.ControlType, fld.DisplayName, Convert.ToInt32(fld.SortOrder), Convert.ToInt32(fld.MaxLength),
                                                        fld.fieldValue, Convert.ToInt32(fld.FieldID), Convert.ToBoolean(fld.IsRequired), Convert.ToInt32(fld.ListID),
                                                         listDisplayType, //added 
                                                        fld.Datatype, fld.ListName, includeFieldvalue, virtualDirectory, Convert.ToInt32(companyID)));

                        htmlPageContent.Append(CreateInfoButton(fld.Description, Convert.ToInt32(fld.FieldID), virtualDirectory) + " </div>" + " </div>");
                    }

                    htmlPageContent.Append(" </div>" + BuildOnReadyFunction(companyID, eventID));
               }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return htmlPageContent.ToString();
        }

        /// <summary>
        /// Get the ListValuId for supplied ListId, then check
        /// if only first level of child items are present for that 'ListValuId', then display the list as dropdown
        /// and if there any children item found for any of the 'first level of child item' then
        /// display the list as treeview (i.e if second level of child items are present).
        /// </summary>
        /// <param name="listID"></param>
        /// <returns></returns>
        public int GetDisplayType(int? listID)
        {
            //Used to get the number of second level child items present
            int count = 0;

            //Used to get the value to decide wheather to display the list as dropdown or treeview
            //1 is for dropdown and 2 is for treeview.
            int listDisplayType = (int)tap.dom.hlp.Enumeration.Display.DopdownDisplay;

            var resultSet = from listvalues in
                                (from listvalues in _db.ListValues
                                 where
                                 (from listvalues0 in _db.ListValues
                                  where

                                  (from listvalues01 in _db.ListValues
                                   where
                                   listvalues01.ListID == listID
                                   select new
                                   {
                                       listvalues01.ListValueID
                                   }).Contains(new { ListValueID = (Int32)listvalues0.Parent })
                                  select new
                                  {
                                      listvalues0.ListValueID
                                  }).Contains(new { ListValueID = (Int32)listvalues.Parent })
                                 select new
                                 {
                                     Dummy = "x"
                                 })
                            group listvalues by new { listvalues.Dummy } into g
                            select new { RowCount = (int)g.Count() };

            foreach (var fld in resultSet)
            {
                count = fld.RowCount;
            }

            //If second level of child items are there, for the listid supplied
            if (count > 0)
            {
                listDisplayType =(int)tap.dom.hlp.Enumeration.Display.TreeviewDisplay;
            }

            return listDisplayType;
        }

        #endregion

        #region To Add DateTime picker For Datetime textbox
        /// <summary>
        /// BuildOnReadyFunction()
        /// </summary>
        /// <returns></returns>
        public string BuildOnReadyFunction(string companyID, string eventID)
        {
            StringBuilder readOnlyContent = new StringBuilder("<script>\n $(document).ready(function () { \n");
            //for Adding datetime picker
            if (_dateFieldList != null && _dateFieldList.Count() > 0)
            {
                tap.dom.adm.SiteSettings siteSettings = new adm.SiteSettings();

                int compId = Convert.ToInt32(companyID);
                string currentDateFormat = _db.SiteSettings.FirstOrDefault(a => a.CompanyID == compId).DateFormat;
                currentDateFormat = DateFormatConvert(currentDateFormat);

                foreach (var fieldid in _dateFieldList)
                {

                    readOnlyContent.Append("$('#txt" + fieldid + "').datepicker({dateFormat: localStorage.dateFormat});\n");

                    //Added change event on respective textboxes for Auto save functionality.
                    if (eventID != "0" && eventID != "-1")
                    {                        
                        readOnlyContent.Append("$('#txt" + fieldid + "').change(function (e) { SaveFields(e); });\n");
                    }
                }

            }

            readOnlyContent.Append(" $('#ui-datepicker-div').addClass('notranslate');}); </script>");
            return readOnlyContent.ToString();
        }

        #endregion 

        #region To create Html Controls
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controltype"></param>
        /// <param name="displayname"></param>
        /// <param name="sortorder"></param>
        /// <param name="maxlength"></param>
        /// <param name="displayvalue"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string BuildHTMLControl(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, int listId, int dispType, string dataType, string listName, bool includeFieldValue, string virtualDirectory, int companyId)
        {
            if (controlType.Replace(" ", "") == tap.dom.hlp.Enumeration.ControlType.TextBox.ToString() && (dataType == tap.dom.hlp.Enumeration.DataType.Int.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.String.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString()))
                return CreateTextbox(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, dataType, includeFieldValue, companyId);

            else if (controlType.Replace(" ", "") == tap.dom.hlp.Enumeration.ControlType.DateField.ToString() && (dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString()))
                return CreateTextbox(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, dataType, includeFieldValue, companyId);

            else if (controlType == CommonOperation.RADIOBUTTON)
                return CreateRadioButton(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, includeFieldValue);
                        
            else if (controlType.Replace(" ", "") == tap.dom.hlp.Enumeration.ControlType.ParagraphText.ToString())
                return CreateParagraphtext(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, includeFieldValue);
           
            else if (controlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectmultiplefromlist.ToString() && dispType == (int)tap.dom.hlp.Enumeration.Display.DopdownDisplay)
                return CreateDivTag(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, listId, dispType, listName, includeFieldValue, virtualDirectory);
            
            else if ((controlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectonefromlist.ToString() || controlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectmultiplefromlist.ToString()) && dispType == (int)tap.dom.hlp.Enumeration.Display.TreeviewDisplay)
                return CreateDivTag(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, listId, dispType, listName, includeFieldValue, virtualDirectory);
           
            else if ((controlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectonefromlist.ToString() && dispType == (int)tap.dom.hlp.Enumeration.Display.DopdownDisplay))
                return CreateDropDownlist(controlType, displayName, sortOrder, maxLength, displayValue, fieldId, isRequired, listId, dispType, includeFieldValue);
            
            return "";
        }
        #endregion

        #region To  Create TextBox Control
        /// <summary>
        /// CreateTextbox
        /// </summary>
        /// <param name="displayname"></param>
        /// <param name="sortorder"></param>
        /// <param name="maxlength"></param>
        /// <param name="displayvalue"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string CreateTextbox(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, string dataType, bool includeFieldValue, int companyId)
        {
            StringBuilder textBoxContent = new StringBuilder();
            try
            {
                string css = string.Empty;
                bool hasMaxLength = (maxLength != 0);

                DateFormat dt = new DateFormat();
                string dateFormat = _db.SiteSettings.Where(a => a.CompanyID == companyId).Select(a => a.DateFormat).FirstOrDefault();


                css = ((dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString()) ? "<input class='DateInputTextBox trackCustomChanges trackDynamicFields' type='text'" : "<input class='ContentInputTextBox WidthP40 trackCustomChanges trackDynamicFields' type='text'");
                textBoxContent.Append(css);
                var id = "txt" + fieldId;
                textBoxContent.Append("  id='" + id + "'");
                textBoxContent.Append(hasMaxLength ? "  maxlength='" + maxLength + "'" : string.Empty);
                if ((dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString()))
                {
                    if (displayValue != null)
                        //displayValue = Convert.ToDateTime(displayValue).ToString(SessionService.DateFormat, CultureInfo.InvariantCulture);
                        displayValue = Convert.ToDateTime(displayValue).ToString(dateFormat, CultureInfo.InvariantCulture);
                    // displayValue = dt.convertAsAdminDate(displayValue).ToString();
                    //displayValue = Convert.ToDateTime(displayValue).ToString(SessionService.DateFormat);

                    textBoxContent.Append(includeFieldValue ? "  value='" + displayValue + "'" : string.Empty);
                }
                else
                {
                    textBoxContent.Append(includeFieldValue ? "  value='" + displayValue + "'" : string.Empty);
                }

                textBoxContent.Append("  tabindex='0'");
                textBoxContent.Append(isRequired ? "  data-required='true'" : string.Empty);

                if (dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString())
                    _dateFieldList.Add(fieldId);

                if (dataType == tap.dom.hlp.Enumeration.DataType.DateTime.ToString() || dataType == tap.dom.hlp.Enumeration.DataType.Date.ToString())
                    textBoxContent.Append("readonly='readonly'");
                if (dataType == tap.dom.hlp.Enumeration.DataType.Int.ToString())
                {
                    textBoxContent.Append("onkeyup= NumericValidation(event,id)");
                }


                //Working on Custom fields Autosave and it is in progress                

                textBoxContent.Append(OperationModeSelect(displayValue));
                textBoxContent.Append(DataPropertyAppend(controlType, fieldId, dataType));

                textBoxContent.Append("   />");


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return textBoxContent.ToString();
        }

        #endregion       
       
        #region To create CheckBox  Control

        /// <summary>
        /// CreateCheckbox
        /// </summary>
        /// <param name="displayname"></param>
        /// <param name="sortorder"></param>
        /// <param name="displayvalue"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string CreateCheckbox(string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, bool includeFieldValue)
        {
            StringBuilder checkBoxContent = new StringBuilder();
            try
            {
                checkBoxContent.Append("<input type='checkbox' style='margin-left:20px;' class='trackCustomChanges trackDynamicFields' "); ;
                checkBoxContent.Append("  id='chk" + fieldId + "'");
                checkBoxContent.Append(includeFieldValue ? ((displayValue == "1") ? "  checked='checked'" : string.Empty) : string.Empty);
                //checkBoxContent.Append("  tabindex='" + sortOrder + "'");
                checkBoxContent.Append("  tabindex='0'");
                checkBoxContent.Append(isRequired ? "  data-required='true'" : string.Empty);

                checkBoxContent.Append(OperationModeSelect(displayValue));      

                checkBoxContent.Append("   />");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return checkBoxContent.ToString();
        }

        #endregion

        #region To Create ParagraphText

        /// <summary>
        /// Create TextArea
        /// </summary>
        /// <param name="displayname"></param>
        /// <param name="sortorder"></param>
        /// <param name="maxlength"></param>
        /// <param name="displayvalue"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string CreateParagraphtext(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, bool includeFieldValue)
        {
            StringBuilder paragraphtextContent = new StringBuilder();
            try
            {
                paragraphtextContent.Append("<textarea class='ContentInputTextBox WidthP40 TextAreaHeight trackCustomChanges trackDynamicFields' ");
                paragraphtextContent.Append("  id='txtarea" + fieldId + "'");

                //this check block is used to set the max length only if user has set some value in it
                if (maxLength != 0)
                {
                    paragraphtextContent.Append("  maxlength='" + maxLength + "'");
                    paragraphtextContent.Append("  onblur=\"javascript:TextAreaMaxlength('" + fieldId + "','" + maxLength + "');return false;\"");
                }

                //paragraphtextContent.Append("  tabindex='" + sortOrder + "'");
                paragraphtextContent.Append("  tabindex='0'");
                paragraphtextContent.Append(isRequired ? "  data-required='true'" : string.Empty);

                paragraphtextContent.Append(OperationModeSelect(displayValue));
                paragraphtextContent.Append(DataPropertyAppend(controlType, fieldId, DATA_TYPE_STRING)); 

                paragraphtextContent.Append("   >");
                paragraphtextContent.Append(includeFieldValue ? displayValue : string.Empty);
                paragraphtextContent.Append("</textarea>");

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return paragraphtextContent.ToString();

        }

        #endregion

        #region To Create Label Control
        /// <summary>
        /// To create display name using Label control
        /// </summary>
        /// <param name="displayname"></param>
        /// <returns></returns>
        public string BuildDisplayname(string displayName, int fieldId, bool isRequired,string dataType)
        {
            StringBuilder displayNameContent = new StringBuilder();
            try
            {
                string labelValue = (dataType == "Label" ? "<label  class='contentLabel' id= 'lbl" + fieldId + "'  >" + displayName : "<label  class='ContentUserLabel wordWrap' id= 'lbl" + fieldId + "'  >" + displayName);
                
                displayNameContent.Append(labelValue);
                displayNameContent.Append(isRequired ? "<span style='color:red' id='spn" + fieldId + "'>*</span>" : string.Empty);
                displayNameContent.Append("</label>");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return displayNameContent.ToString();
        }
        #endregion

        #region To create Dropdownlist
        /// <summary>
        /// Creating Dropdownlist
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="sortOrder"></param>
        /// <param name="maxLength"></param>
        /// <param name="displayValue"></param>
        /// <param name="fieldId"></param>
        /// <param name="isRequired"></param>
        /// <param name="listId"></param>
        /// <param name="dispType"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string CreateDropDownlist(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, int listId, int dispType, bool includeFieldValue)
        {
            StringBuilder ddlContent = new StringBuilder();
            try
            {
                controlType = "DropdownList";
                var listValue = _db.ListValues.Where(a => a.ListID == listId).ToList().Skip(1);
                string appendProperties = string.Format("{0} {1}", OperationModeSelect(displayValue), DataPropertyAppend(controlType, fieldId, DATA_TYPE_STRING));
                ddlContent.Append("<select '" + appendProperties + "'  class='ContentInputTextBox WidthP40 trackCustomChanges trackDynamicFields' style='width: 40.7%' id='ddl" + fieldId + "'>");
                
                if (listValue != null)
                {
                    int listDispVal = ((displayValue != null && displayValue != string.Empty) ? Convert.ToInt32(displayValue) : 0);
                    ddlContent.Append("<option value='0'>" + "<< Select >>" + "</option>");

                    foreach (var a in listValue)
                    {
                        ddlContent.Append((includeFieldValue && (a.ListValueID == listDispVal)) ? "<option  selected='selected' value='" + a.ListValueID + "'>" : "<option value='" + a.ListValueID + "'>");
                        ddlContent.Append(a.ListValueName + "</option>");
                    }
                }
                ddlContent.Append("</select>");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return ddlContent.ToString();
        }
        #endregion

        #region To Create TreeView Control
        /// <summary>
        /// Creating Treeview Control using DivTag
        /// </summary>
        /// <param name="sControlType"></param>
        /// <param name="displayName"></param>
        /// <param name="sortOrder"></param>
        /// <param name="maxLength"></param>
        /// <param name="displayValue"></param>
        /// <param name="fieldId"></param>
        /// <param name="isRequired"></param>
        /// <param name="listId"></param>
        /// <param name="dispType"></param>
        /// <param name="listName"></param>
        /// <param name="includeFieldValue"></param>
        /// <param name="sVirtualPath"></param>
        /// <returns></returns>
        public string CreateDivTag(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, 
                                    int fieldId, bool isRequired, int listId, int dispType, string listName, bool includeFieldValue, string virtualDirectory)
        {
            StringBuilder divTagContent = new StringBuilder();
            string sListValues = string.Empty;

            try
            {
                //check the null or empty 
                if (!string.IsNullOrEmpty(displayValue))
                {
                    tap.dom.adm.ListValues listValues = new tap.dom.adm.ListValues();
                    var fullPath = listValues.SelectLocationPath(displayValue, CommonOperation.ZERO, CommonOperation.ZERO);

                    StringBuilder sbLstVal = new StringBuilder();
                    foreach (var vList in fullPath)
                    {
                        sbLstVal.Append(vList + "</br>");
                    }

                    sListValues = Convert.ToString(sbLstVal).Remove(sbLstVal.ToString().LastIndexOf("</br>"));
                }               

                string additionalProperties = string.Empty;
                additionalProperties = displayValue == null ? string.Format(@" {0} {1} data-entity-hiddenFieldId='hdn{2}' ", OperationModeSelect(displayValue), DataPropertyAppend(controlType, fieldId, DATA_TYPE_STRING), fieldId) : string.Format(@" {0} {1} data-entity-displayId={2} data-entity-hiddenFieldId='hdn{3}' ", OperationModeSelect(displayValue), DataPropertyAppend(controlType, fieldId, DATA_TYPE_STRING), displayValue, fieldId);
                                
               //check the control type .it is sigle selection list
                bool isSelectOneFromList = (controlType.Replace(" ", "").ToString() == tap.dom.hlp.Enumeration.ControlType.Selectonefromlist.ToString());
                string listDisplayType = " <img style='padding-top:5px;' class='HandCursor PaddingLeft20' id='lnk" + fieldId + "' alt='OpenLin' src='" + virtualDirectory + "/Images/OpenLink.png' onclick=\"javascript:LoadTreeView('" + fieldId + "','" + listId + "');return false;\"/>";
                if (!isSelectOneFromList)
                    listDisplayType = "<img style='padding-top:5px; ' class='HandCursor PaddingLeft20' id='lnk" + fieldId + "' alt='OpenLin' src='" + virtualDirectory + "/Images/OpenLink.png' onclick=\"OpenMultiSelectionTreeView('" + fieldId + "','" + listId + "');return false;\"/>";
                divTagContent.Append(listDisplayType + "<label '" + additionalProperties + "' class='EventImageLabel trackCustomChanges trackDynamicFields' id='lblCust" + fieldId + "'>" + sListValues + "</label><input id='hdn" + fieldId + "' type='hidden' name='hdn" + fieldId + "' value='" + displayValue + "'   />");
             }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return divTagContent.ToString();
        }
        #endregion
       

        #region Create Radio Button

        /// <summary>
        /// CreateRadioButton
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="sortOrder"></param>
        /// <param name="maxLength"></param>
        /// <param name="displayValue"></param>
        /// <param name="fieldId"></param>
        /// <param name="isRequired"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>
        public string CreateRadioButton(string controlType, string displayName, int sortOrder, int maxLength, string displayValue, int fieldId, bool isRequired, bool includeFieldValue)
        {
            StringBuilder radioButtonContent = new StringBuilder();
            try
            {
                bool isDisplayVaue = (displayValue != null);

                bool isDisplayVaueWithONE = (includeFieldValue && displayValue == CommonOperation.YES);
                bool isDisplayValueWithZERO = (includeFieldValue && displayValue == CommonOperation.NO);                

                if (!isDisplayVaue)                                
                    radioButtonContent.Append(CreateRadioButton(controlType, CommonOperation.YES, sortOrder, displayValue, fieldId, isRequired, false) + CreateRadioButton(controlType, CommonOperation.NO, sortOrder, displayValue, fieldId, isRequired, false));
                else
                {
                    radioButtonContent.Append(isDisplayVaueWithONE ?
                        CreateRadioButton(controlType, CommonOperation.YES, sortOrder, displayValue, fieldId, isRequired, true)
                        +
                        CreateRadioButton(controlType, CommonOperation.NO, sortOrder, displayValue, fieldId, isRequired, false)
                        : isDisplayValueWithZERO ?
                        CreateRadioButton(controlType, CommonOperation.YES, sortOrder, displayValue, fieldId, isRequired, false)
                        +
                        CreateRadioButton(controlType, CommonOperation.NO, sortOrder, displayValue, fieldId, isRequired, true) : string.Empty);
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return radioButtonContent.ToString();
        }

        /// <summary>
        /// CreateRadioButton
        /// </summary>
        /// <param name="sYesNo"></param>
        /// <param name="sortOrder"></param>
        /// <param name="displayValue"></param>
        /// <param name="fieldId"></param>
        /// <param name="isRequired"></param>
        /// <param name="includeFieldValue"></param>
        /// <returns></returns>

        public string CreateRadioButton(string controlType, string option, int sortOrder, string displayValue, int fieldId, bool isRequired, bool includeFieldValue)
        {
            StringBuilder radioButtonContent = new StringBuilder();
            try
            {
                radioButtonContent.Append("<input type='radio' style='margin-left:20px;border:0px solid red' ");
                radioButtonContent.Append((option == CommonOperation.YES) ? "  id='rbY" + fieldId + "'" : "  id='rbN" + fieldId + "'");
                radioButtonContent.Append(includeFieldValue ? ((displayValue == CommonOperation.YES) ? "  checked='checked'" : (displayValue == CommonOperation.NO) ? "  checked='checked'" : string.Empty) : string.Empty);
                //radioButtonContent.Append("  tabindex='" + sortOrder + "'");
                radioButtonContent.Append("  tabindex='0'");
                radioButtonContent.Append(isRequired ? "  data-required='true'" : string.Empty);

                var radioButtonValue = option == CommonOperation.YES ? CommonOperation.YES : CommonOperation.NO;
                radioButtonContent.Append(" name='group" + fieldId + "' value='" + radioButtonValue + "' class='trackCustomChanges trackRadioButtonChanges trackDynamicFields' ");                
                
                radioButtonContent.Append(OperationModeSelect(displayValue)); //Append the mode, Insert/Update.               
                radioButtonContent.Append(DataPropertyAppend(controlType, fieldId, DATA_TYPE_STRING)); //Append all the properties required for Auto save.

                radioButtonContent.Append(" /> ");
                radioButtonContent.Append("<span class='ContentCustomRadioButton'>" + option + "</span>");

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return radioButtonContent.ToString();
        }

        #endregion

        #region Create info button
        /// <summary>
        ///  CreateInfoButton()
        /// Description - Create the info button for the field decription
        /// </summary>
        /// <param name="sDescription"></param>
        /// <param name="iFieldId"></param>
        /// <param name="sVirtualPath"></param>
        /// <returns>string</returns>
        public string CreateInfoButton(string description, int fieldId, string virtualdirectory)
        {
            StringBuilder infoButtonContent = new StringBuilder();
            try
            {   //check not null
                infoButtonContent.Append((description != null) ? "<span id='anc" + fieldId + "' class='help' style='padding-left:10px;vertical-align:bottom;padding-top:5px; '><img id='img" + fieldId + "' border='0' src='" + virtualdirectory + "/Images/Info.png' /><span id='spn_" + fieldId + "'><p style='word-wrap: break-word;'>" + description + "</p></span></span>" : "<span id='anc" + fieldId + "' style='display:none '></span>");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return infoButtonContent.ToString();
        }
        #endregion

        #region Append the data properties
       
        /// <summary>
        /// Append the data properties to the html mark up for 
        /// Custom fields Auto save.
        /// </summary>
        /// <param name="controlType"></param>
        /// <param name="fieldId"></param>
        /// <returns></returns>

        public string DataPropertyAppend(string controlType, int fieldId, string dataType)
        {
            return string.Format(@" data-type='Custom'
                data-entity-type='tap.dat.FieldValues,tap.dat'
                data-entity-set='FieldValues'
                data-entity-property-pk='FieldValueID'
                data-entity-property='FieldValue'
                data-entity-datatype=""{2}""
                data-entity-queryfield='FieldID'               
                data-entity-sec-queryfield='EventID'                
                data-entity-controltype=""{0}""
                data-entity-queryfield-value= ""{1}"";", controlType, fieldId, dataType);
        }

        #endregion

        #region Get Insertion or Update

        /// <summary>
        /// Get Insertion or Update mode for
        /// Custom fields Auto save
        /// </summary>
        /// <param name="operationMode"></param>
        /// <returns>string</returns>
        public string OperationModeSelect(string operationMode)
        {                      
            return operationMode == null ? "  data-mode='Insert'" : "  data-mode='Update'";
        }

        #endregion

        #region Convert Date format

        /// <summary>
        /// Convert the Database dateformat to the plugin date format.
        /// </summary>
        /// <param name="currentDateFormat"></param>
        /// <returns> converted date format </returns>
        private string DateFormatConvert(string currentDateFormat)
        {
            string dateFormat = string.Empty;

            switch (currentDateFormat)
            {

                case "dd/MM/yyyy":
                    dateFormat = "dd/mm/yy";
                    break;

                case "MM/dd/yyyy":
                    dateFormat = "mm/dd/yy";
                    break;

                case "MMM dd yyyy":
                    dateFormat = "M dd yy";
                    break;

                case "dd-MM-yyyy":
                    dateFormat = "dd-mm-yy";
                    break;

                case "militaryformat":
                    dateFormat = "dd-M-yy";
                    break;

            }

            return dateFormat;
        }

        #endregion


    }
}
