﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;


namespace tap.dom.tab
{
    public class AdminTabs
    {
        #region For Global Variables

        tap.dat.EFEntity _db = new tap.dat.EFEntity();

        const string MAIN_TAB_ID = "lnkmain";

        const string MAIN_TAB_URL = "/Admin/Configuration/Custom-Tabs/";
        const string DETAIL_SUB_TAB = "Details";
        const string ATTACHMENT_SUB_TAB = "Attachments";

        const string NEW_TAB = "New Tab";
        const string CSS_ACTIVE_TAB = "sub-tab";
        const string CSS_INACTIVE_TAB = "inactive-tab";

        const string SUB_TAB_ID = "lnk";
        const string SUB_TAB_URL = "/Admin/Configuration/";
        const string DIVISIONSUB_TAB_URL = "/Admin/Division/";

        const string ADMIN = "Admin";

        #endregion

        #region To Build Admin site HtMl Tabs
        /// <summary>
        /// Description-Create the mainTabs in HTML format
        /// </summary>
        /// <param name="virtualDir"></param>
        /// <returns></returns>
        public string BuildHTMlMainTabs(string baseURL, int divisionId)
        {
            StringBuilder mainTabContent = new StringBuilder();
            try
            {
                var mainTabs = _db.Modules.Where(a => (a.Active == true && a.ModuleName != ADMIN));
                foreach (var maintab in mainTabs)
                {
                    mainTabContent.Append("<li class='ui-state-default ui-corner-top'>" + CreateAnchorTag(baseURL, maintab.ModuleID, maintab.ModuleName, tap.dom.hlp.Enumeration.TabType.MainTab.ToString(), 0, false, divisionId) + "</li>");
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return mainTabContent.ToString();
        }
        #endregion

        #region For Creating AnchorTag To display Tabs
        /// <summary>
        ///Decsription-Creating Anchor tag. 
        /// </summary>
        /// <param name="tabType"></param>
        /// <param name="moduleId"></param>
        /// <param name="id"></param>
        /// <param name="tabDisplayName"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public string CreateAnchorTag(string baseURL, int id, string tabDisplayName, string tabType, int moduleId, bool isActive, int divisionId)
        {
            StringBuilder anchorTagContent = new StringBuilder();
            try
            {
                string tabId = MAIN_TAB_ID + id;
                string displayName = tabDisplayName;

                string formattedDisplayName = (tabDisplayName.Contains(" ") ? tabDisplayName.Replace(" ", "-").ToString() : tabDisplayName);

                string moduleName = GetModuleName(id, moduleId);

                string url = baseURL + MAIN_TAB_URL + moduleName;
                if (divisionId > 0)
                    url = baseURL + "/Admin/Division/" + divisionId + "/Custom-Tabs/" + moduleName;
                anchorTagContent.Append(String.Format("<a  id='{0}' onclick=\"javascript:RedirectPageUrl('" + url + "');return false;\" >{1}</a>", tabId, displayName));

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return anchorTagContent.ToString();

        }
        #endregion

        #region For Building SubTabs
        /// <summary>
        /// Description-To create HTMl subtabs based on the moduleid
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public string BuildHTMLSubTabs(string baseURL, int moduleId, int companyId, int divisionId)
        {
            StringBuilder subTabContent = new StringBuilder();
            try
            {

                var universalTabs = (from a in _db.Tabs
                                     join company in _db.Companies on a.CompanyID equals company.CompanyID
                                     where a.CompanyID == companyId && a.IsUniversal == true && a.IsSystem == false && a.IsDeleted != true
                                     select new
                                     {
                                         TabID = a.TabID,
                                         TabName = a.TabName,
                                         ModuleID = 1,
                                         Active = a.Active,
                                         IsSystem = false,
                                         SortOrder = a.SortOrder,
                                         CompanyParentId = company.ParentId
                                     });

                var moduleSpecificTab = (from a in _db.Tabs
                                         join b in _db.TabModules
                                             on a.TabID equals b.TabID
                                         join company in _db.Companies on a.CompanyID equals company.CompanyID
                                         where (b.ModuleID == moduleId && (a.IsUniversal == false || a.IsUniversal ==null) && a.CompanyID == companyId && a.IsDeleted != true)
                                         orderby (a.Active)
                                         select new
                                         {
                                             TabID = a.TabID,
                                             TabName = a.TabName,
                                             ModuleID = b.ModuleID,
                                             Active = a.Active,
                                             IsSystem = false,
                                             SortOrder = b.SortOrder,
                                             CompanyParentId = company.ParentId
                                         });


                var systemTabs = (from a in _db.Tabs
                                  join b in _db.TabModules
                                    on a.TabID equals b.TabID
                                  join company in _db.Companies on a.CompanyID equals company.CompanyID
                                  where (a.CompanyID == companyId && a.Active == true && a.IsUniversal == true && a.IsSystem == true && b.ModuleID == moduleId && a.IsDeleted != true)
                                  orderby (a.Active)
                                  select new
                                  {
                                      TabID = a.TabID,
                                      TabName = a.TabName,
                                      ModuleID = b.ModuleID,
                                      Active = a.Active,
                                      IsSystem = true,
                                      SortOrder = a.SortOrder,
                                      CompanyParentId = company.ParentId
                                  }).OrderBy(a => a.SortOrder).ToList();


                var subTabs = moduleSpecificTab.Union(universalTabs).ToList();

                systemTabs = systemTabs.OrderByDescending(a => a.Active).ToList();

                if (systemTabs != null && systemTabs.Count() > 0)
                {

                    foreach (var systemtab in systemTabs)
                    {
                        subTabContent.Append("<div id='lnk_" + systemtab.TabID + "' class='div-admin-tabs'>");
                        
                        //if (systemtab.IsSystem && systemtab.SortOrder == 0 && systemtab.CompanyParentId == null) //Global admin should edit Details tab
                        if (systemtab.IsSystem && systemtab.SortOrder == 0)//global admin and normal user also can edit Details tab
                        {
                            subTabContent.Append(BuildAnchorTag(baseURL, systemtab.TabID, systemtab.TabName, tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, Convert.ToBoolean(systemtab.Active), divisionId));
                        }
                        else if (!systemtab.IsSystem)
                        {
                            subTabContent.Append(BuildAnchorTag(baseURL, systemtab.TabID, systemtab.TabName, tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, Convert.ToBoolean(systemtab.Active), divisionId));
                        }
                        else
                        {
                            subTabContent.Append(BuildUniversalTab(systemtab.TabID, systemtab.TabName,systemtab.CompanyParentId,systemtab.SortOrder));
                        }

                        subTabContent.Append("</div>");
                    }

                }

                subTabs = subTabs.OrderBy(b => b.SortOrder).ToList();

                if (subTabs != null && subTabs.Count() > 0)
                {
                    subTabContent.Append("<div id='sortorder' moduleid='" + moduleId + "'>");
                    foreach (var subtab in subTabs)
                    {
                        subTabContent.Append("<div id='lnk_" + subtab.TabID + "' class='div-admin-tabs'>");

                        string tabname = subtab.TabName;

                        if (!subtab.IsSystem)
                        {
                            subTabContent.Append(BuildAnchorTag(baseURL, subtab.TabID, tabname, tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, Convert.ToBoolean(subtab.Active), divisionId));
                        }
                        else
                        {
                            subTabContent.Append(BuildUniversalTab(subtab.TabID, tabname, subtab.CompanyParentId, subtab.SortOrder));
                        }

                        subTabContent.Append("</div>");
                    }
                    subTabContent.Append("</div>");
                }
                
                // for Building New Tab
                // subTabContent.Append("<div id='lnk_0' class='div-admin-tabs'>" + BuildAnchorTag(baseURL, 0, NEW_TAB, tap.dom.hlp.Enumeration.TabType.SubTab.ToString(), moduleId, true, divisionId) + "</div>");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return subTabContent.ToString();
        }
        #endregion


        public string GetURL(string baseURL, int id, string tabDisplayName, string tabType, int moduleId, bool isActive, int divisionId)
        {

            string css = ((isActive == true) ? CSS_ACTIVE_TAB : CSS_INACTIVE_TAB);

            string tabId = SUB_TAB_ID + id;
            //string displayName = tabDisplayName;
            //if (displayName.Equals("New Tab"))
            //    displayName = "<label class='addSymbol'>&nbsp;</label>" + displayName;

            string formattedDisplayName = (tabDisplayName.Contains(" ") ? tabDisplayName.Replace("<-", "").Replace("->", "").Replace(" ", "-").ToString() : tabDisplayName.Replace("<-", "").Replace("->", ""));
            string moduleName = GetModuleName(id, moduleId);

            string url = baseURL + SUB_TAB_URL + moduleName + "/" + ((RemoveSpecialCharacters(formattedDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(formattedDisplayName)) + "-" + id;
            if (divisionId > 0)
                url = baseURL + DIVISIONSUB_TAB_URL + divisionId + "/" + moduleName + "/" + ((RemoveSpecialCharacters(formattedDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(formattedDisplayName)) + "-" + id;

            return url;
        }
        #region To create Anchor Tag
        /// <summary>
        /// Method To Create BuildAnchorTag
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="displayName"></param>
        /// <param name="tabType"></param>
        public string BuildAnchorTag(string baseURL, int id, string tabDisplayName, string tabType, int moduleId, bool isActive, int divisionId)
        {
            StringBuilder anchorTagContent = new StringBuilder();

            try
            {
                string css = ((isActive == true) ? CSS_ACTIVE_TAB : CSS_INACTIVE_TAB);

                string tabId = SUB_TAB_ID + id;
                string displayName = tabDisplayName;
                if (displayName.Equals("New Tab"))
                    displayName = "<label class='addSymbol'>&nbsp;</label>" + displayName;

                string formattedDisplayName = (tabDisplayName.Contains(" ") ? tabDisplayName.Replace("<-", "").Replace("->", "").Replace(" ", "-").ToString() : tabDisplayName.Replace("<-", "").Replace("->", ""));
                string moduleName = GetModuleName(id, moduleId);

                string url = baseURL + SUB_TAB_URL + moduleName + "/" + ((RemoveSpecialCharacters(formattedDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(formattedDisplayName)) + "-" + id;
                if (divisionId > 0)
                    url = baseURL + DIVISIONSUB_TAB_URL + divisionId + "/" + moduleName + "/" + ((RemoveSpecialCharacters(formattedDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(formattedDisplayName)) + "-" + id;

                anchorTagContent.Append(String.Format("<a class='{0}'  id='{1}' href='{2}'>{3}</a>", css, tabId, url, displayName));
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return anchorTagContent.ToString();
        }
        #endregion

        #region to create Universal anchor tag
        /// <summary>
        /// Description-Creating the universal tab
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tabDisplayName"></param>
        /// <param name="tabType"></param>
        /// <param name="virtualDirectory"></param>
        /// <param name="moduleId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        public string BuildUniversalTab(int id, string tabDisplayName,int? CompanyParentId,int? SortOrder)
        {
            StringBuilder universalTabContent = new StringBuilder();

            try
            {
                string tabId = SUB_TAB_ID + id;
                universalTabContent.Append(string.Format("<a  id='{0}'  class={1}  onclick=\"javascript:DisplayNofification('" + tabDisplayName + "','" + CompanyParentId + "','" + SortOrder + "');return false;\">{2}</a>", tabId, CSS_ACTIVE_TAB, tabDisplayName));
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return universalTabContent.ToString();
        }

        #endregion

        public string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }

        #region To get Module Name
        /// <summary>
        /// GetModuleName
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ModuleId"></param>
        /// <returns></returns>
        public string GetModuleName(int id, int moduleId)
        {
            string moduleName = string.Empty;
            moduleName = ((moduleId == (int)tap.dom.hlp.Enumeration.Modules.Incident || id == (int)tap.dom.hlp.Enumeration.Modules.Incident) ? tap.dom.hlp.Enumeration.Modules.Incident.ToString() : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Investigation || id == (int)tap.dom.hlp.Enumeration.Modules.Investigation) ? tap.dom.hlp.Enumeration.Modules.Investigation.ToString() : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Audit || id == (int)tap.dom.hlp.Enumeration.Modules.Audit) ? tap.dom.hlp.Enumeration.Modules.Audit.ToString() : tap.dom.hlp.Enumeration.Modules.ActionPlan.ToString());
            return moduleName;
        }

        #endregion


    }
}
