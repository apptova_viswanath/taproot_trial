﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using tap.dat;

namespace tap.dom.tab
{
    public class EventTabs
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();

        public bool _hasInvestigation = false;
        public bool _hasCAP = false;
        public int _companyId = 0;
        public const string TAPROOT_TAB_NAME = "TapRooT®";
        public const string TAPROOT_TAB_DISPLAY_NAME = "TapRooT";
        public const string CAP_TAB_NAME = "Corrective Actions";
        public const string CAP_TAB_DISPLAY_NAME = "CorrectiveActions";

        #region to Create HTML Main Tabs for Public site
        /// <summary>
        /// Description-Create HtMl MainTabs 
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="eventId"></param>
        /// <param name="moduleId"></param>
        /// <param name="newEventType"></param>
        /// <param name="virtualDir"></param>
        /// <returns></returns>
        public string BuildHTMlMainTabs(int tabId, string eventID, int moduleId, string baseURL, int companyId)
        {
            StringBuilder mainTabContent = new StringBuilder();
            const string IncidentTabName = "Incident";
            const string InvestigationTableName = "Investigation";
            const string AuditTabname = "Audit";
            const string ActionPlanTabName = "Action Plan";
            try
            {
                int eventId = Convert.ToInt32((!string.IsNullOrEmpty(eventID) && eventID != "0") ? _cryptography.Decrypt(eventID) : "0");

                if (eventId != 0)
                {
                    var incident = _db.Incidents.FirstOrDefault(a => a.EventID == eventId);
                    var investigation = _db.Investigations.FirstOrDefault(a => a.EventID == eventId);
                    var cap = _db.CorrectiveActionPlans.FirstOrDefault(a => a.EventID == eventId);
                    var audit = _db.Audits.FirstOrDefault(a => a.EventID == eventId);//nw

                    bool hasIncident = (incident != null);
                    bool hasInvestigation = (investigation != null);
                    bool isCreateInvestigation = (hasIncident && !hasInvestigation);
                    bool hasCAP = (cap != null);
                    bool hasAudit = (audit != null);
                    bool isCreateCAP = ((hasIncident && hasInvestigation && !hasCAP) || (hasAudit && !hasCAP) || (hasIncident && !hasCAP));

                    _hasCAP = hasCAP;
                    _hasInvestigation = hasInvestigation;

                    string lnkIncident = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.Incident, eventId, IncidentTabName, tap.dom.hlp.Enumeration.TabType.MainTab.ToString(), 0) + "</li>";
                    string lnkInvestigation = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.Investigation, eventId, InvestigationTableName, tap.dom.hlp.Enumeration.TabType.MainTab.ToString(), 0) + "</li>";
                    string lnkAudit = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.Audit, eventId, AuditTabname, string.Empty, 0) + "</li>";
                    string lnkCAP = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.ActionPlan, eventId, ActionPlanTabName, tap.dom.hlp.Enumeration.TabType.MainTab.ToString(), 0) + "</li>";

                    if (isCreateInvestigation)
                        lnkInvestigation = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.Investigation, eventId, "Add Investigation", string.Empty, 0) + "</li>";

                    if (isCreateCAP)
                        lnkCAP = GetLi() + CreateAnchorTag(companyId, baseURL, (int)tap.dom.hlp.Enumeration.Modules.ActionPlan, eventId, "Add Action Plan", string.Empty, 0) + "</li>";

                    SetActiveClass(ref lnkIncident, ref lnkInvestigation, ref lnkAudit, ref lnkCAP, moduleId);

                    if (hasIncident)
                    {
                        mainTabContent.Append(hasIncident ? lnkIncident : string.Empty);

                        mainTabContent.Append((hasIncident && hasInvestigation) ? lnkInvestigation : (hasIncident && isCreateInvestigation) ? lnkInvestigation : string.Empty);

                        mainTabContent.Append(hasCAP ? lnkCAP : isCreateCAP ? lnkCAP : string.Empty);
                    }
                    else if (hasAudit)
                    {
                        mainTabContent.Append(hasAudit ? lnkAudit : string.Empty);
                        mainTabContent.Append((hasAudit && hasCAP) ? lnkCAP : (hasAudit && isCreateCAP) ? lnkCAP : string.Empty);
                    }

                }
                else
                {
                    string formattedModuleName = string.Empty;
                    formattedModuleName = ((moduleId == (int)tap.dom.hlp.Enumeration.Modules.Incident) ? IncidentTabName : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Investigation) ? InvestigationTableName : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Audit) ? AuditTabname : (moduleId == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan) ? ActionPlanTabName : string.Empty);
                    mainTabContent.Append(GetActiveLi() + CreateAnchorTag(companyId, baseURL, moduleId, 0, formattedModuleName, string.Empty, 0) + "</li>");
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return mainTabContent.ToString();
        }

        #endregion

        public void SetActiveClass(ref string lnkIncident, ref string lnkInvestigation, ref string lnkAudit, ref string lnkCAP, int moduleId)
        {

            switch (moduleId)
            {
                case ((int)tap.dom.hlp.Enumeration.Modules.Incident):
                    lnkIncident = lnkIncident.Replace("<li class='ui-state-default ui-corner-top'>", "<li class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active'>");
                    break;

                case ((int)tap.dom.hlp.Enumeration.Modules.Investigation):
                    lnkInvestigation = lnkInvestigation.Replace("<li class='ui-state-default ui-corner-top'>", "<li class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active'>");
                    break;

                case ((int)tap.dom.hlp.Enumeration.Modules.Audit):
                    lnkAudit = lnkAudit.Replace("<li class='ui-state-default ui-corner-top'>", "<li class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active'>");
                    break;

                case ((int)tap.dom.hlp.Enumeration.Modules.ActionPlan):
                    lnkCAP = lnkCAP.Replace("<li class='ui-state-default ui-corner-top'>", "<li class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active'>");
                    break;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public string GetLi()
        {
            return "<li class='ui-state-default ui-corner-top'>";
        }

        public string GetActiveLi()
        {
            return "<li class='ui-state-default ui-corner-top ui-tabs-selected ui-state-active'>";
        }
        #region For Creating Anchor Tag
        /// <summary>
        /// Decsription-Creating Anchor Tag to display Tabs
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <param name="EventId"></param>
        /// <param name="displayName"></param>
        /// <param name="virtualDir"></param>
        /// <param name="tabType"></param>
        /// <param name="TabId"></param>
        /// <returns></returns>
        public string CreateAnchorTag(int companyId, string baseURL, int moduleId, int eventId, string tabDisplayName, string tabType = null, int? tabId = 0)
        {
            StringBuilder anchorTagContent = new StringBuilder();
            try
            {
                string eventID = (!string.IsNullOrEmpty(eventId.ToString()) && eventId.ToString() != "0") ? _cryptography.Encrypt(eventId.ToString()) : "0";

                string url = string.Empty;
                string moduleName = ((hlp.Enumeration.Modules)moduleId).ToString();
                string action = string.Empty;
                action = "Edit";

                string id = string.Empty;

                int detailTabId = 1;

                if (!string.IsNullOrEmpty(companyId.ToString()))
                    detailTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;

                if (tabType != null && tabType != string.Empty)
                {
                    id = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "ancmain" + moduleId : "anc" + tabId);
                    string tabName = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "Details-" + detailTabId : tabDisplayName + "-" + tabId);

                    if (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString())
                    {
                        bool navigateToTapRoot = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.Investigation && _hasInvestigation);
                        bool navigateToCAP = (moduleId == (int)tap.dom.hlp.Enumeration.Modules.ActionPlan && _hasCAP);

                        string navigateTabName = navigateToTapRoot ? TAPROOT_TAB_NAME : CAP_TAB_NAME;
                        tabId = _db.Tabs.Where(x => x.CompanyID == companyId && x.TabName == navigateTabName).FirstOrDefault().TabID;

                        string displayName = navigateToTapRoot ? TAPROOT_TAB_DISPLAY_NAME : CAP_TAB_DISPLAY_NAME;


                        url = (navigateToTapRoot || navigateToCAP) ?
                                String.Format(baseURL + "/Event/{0}/{1}/{2}", moduleName, displayName + "-" + tabId, eventID)
                                : String.Format(baseURL + "/Event/{0}/{1}/{2}/{3}", moduleName, tabName, eventID, action);

                    }

                    else if (tabType == tap.dom.hlp.Enumeration.TabType.SubTab.ToString())
                    {
                        url = ((tabName == "Details-" + detailTabId)
                            ? String.Format(baseURL + "../Event/{0}/{1}/{2}/{3}", moduleName, tabName, eventID, action)
                            : (tabDisplayName != "7 Steps") ? String.Format("/Event/{0}/{1}/{2}", moduleName, tabName, eventId)
                            : String.Format("/Event/{0}/7-Steps-{1}/{2}", moduleName, tabId, eventId));
                    }
                }
                else
                {
                    id = "ancmain" + moduleId;
                    url = String.Format(baseURL + "/Event/{0}/{1}/{2}/{3}", moduleName, "Details-" + detailTabId, eventID, action);
                }

                anchorTagContent.Append(String.Format("<a id='{0}'  onclick=\"javascript:RedirectUrl('" + url + "','" + moduleName + "');\" >{1}</a>", id, tabDisplayName));

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return anchorTagContent.ToString();
        }
        #endregion

        #region For Building SubTabs
        /// <summary>
        /// Description-Creating HTML subTabs based on Moduleid
        /// </summary>
        /// <param name="tabId"></param>
        /// <param name="eventId"></param>
        /// <param name="moduleId"></param>
        /// <param name="virtualDir"></param>
        /// <returns></returns>
        public string BuildHTMlSubTabs(int tabId, string eventID, int moduleId, string baseURL, int companyId, int userId)
        {
            StringBuilder subTabContent = new StringBuilder();
            try
            {
                int eventId = Convert.ToInt32((!string.IsNullOrEmpty(eventID) && eventID != "0") ? _cryptography.Decrypt(eventID) : "0");

                var universalTabs = (from a in _db.Tabs
                                     where a.CompanyID == companyId && a.IsUniversal == true && a.IsSystem == false && a.IsDeleted != true && a.Active == true
                                     select new
                                     {
                                         TabID = a.TabID,
                                         TabName = a.TabName,
                                         ModuleID = 1,
                                         Active = a.Active,
                                         SortOrder = a.SortOrder
                                     });

                List<int> userAccessTabs = (from tab in _db.TabUsers.AsEnumerable()
                                          where tab.UserId == userId
                                          select Convert.ToInt32(tab.TabId)).ToList();

                List<int> restrictedTabIds = (from tab in _db.Tabs.AsEnumerable()
                                              where tab.CompanyID == companyId && tab.isRestricted == true
                                              select Convert.ToInt32(tab.TabID)).ToList();

                var moduleSpecificTab = (from a in _db.Tabs
                                         join b in _db.TabModules
                                             on a.TabID equals b.TabID
                                         where (b.ModuleID == moduleId && (a.IsUniversal == false || a.IsUniversal == null) && a.IsDeleted != true && a.CompanyID == companyId && a.Active == true &&
                                                (!restrictedTabIds.Contains(a.TabID) || (restrictedTabIds.Contains(a.TabID) && userAccessTabs.Contains(a.TabID))))

                                         orderby (a.Active)
                                         select new
                                         {
                                             TabID = a.TabID,
                                             TabName = a.TabName,
                                             ModuleID = b.ModuleID,
                                             Active = a.Active,
                                             SortOrder = b.SortOrder
                                         });

          
                var parentCildModuleSpcTabs = moduleSpecificTab;

                var systemTabs = (from a in _db.Tabs
                                  join b in _db.TabModules
                 on a.TabID equals b.TabID
                                  where (a.CompanyID == companyId && a.Active == true && a.IsUniversal == true && a.IsSystem == true && b.ModuleID == moduleId && a.IsDeleted != true)
                                  orderby (a.Active)
                                  select new
                                  {
                                      TabID = a.TabID,
                                      TabName = a.TabName,
                                      ModuleID = b.ModuleID,
                                      Active = a.Active,
                                      SortOrder = a.SortOrder
                                  }).OrderBy(a => a.SortOrder).ToList();


                var subTabs = universalTabs.Union(parentCildModuleSpcTabs).OrderBy(b => b.SortOrder).ToList();


                subTabs = systemTabs.Union(subTabs).ToList();

                subTabs = subTabs.OrderByDescending(a => a.Active).ToList();
                if (subTabs != null && subTabs.Count() > 0)
                {
                    subTabContent.Append("<div class='span8'><ul>");
                    foreach (var subtab in subTabs)
                    {
                        subTabContent.Append("<li id='anc_" + subtab.TabID + "'>");

                        subTabContent.Append(CreateAnchorTag(companyId, baseURL, moduleId, subtab.TabID, eventId, subtab.TabName, tap.dom.hlp.Enumeration.TabType.SubTab.ToString()));

                        subTabContent.Append("</li>");

                    }
                    subTabContent.Append("</ul></div>");

                    int borderBottomLength = "<div class='borderBottom'></div>".Length;


                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return subTabContent.ToString();
        }
        #endregion


        public List<int> GetCompanyParentIds(int companyId)
        {
            var parentIds = new List<int>();
            tap.dat.Companies company = new Companies();
            string Id = null;
            do
            {
                Id = _db.Companies.Where(x => x.CompanyID == companyId).FirstOrDefault().ParentId.ToString();

                if (Id != null && Id.Length > 0 && !Id.Equals("0"))
                {
                    parentIds.Add(Convert.ToInt32(Id));
                    companyId = Convert.ToInt32(Id);
                }
            } while (Id != null && Id.Length > 0 && !Id.Equals("0"));

            return parentIds.OrderBy(v => v).ToList();
        }


        #region To create Anchor Tag
        /// <summary>
        /// Description-Create Anchor tag For SubTabs
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <param name="TabId"></param>
        /// <param name="EventId"></param>
        /// <param name="displayName"></param>
        /// <param name="tabType"></param>
        /// <param name="virtualDir"></param>
        /// <returns></returns>
        public string CreateAnchorTag(int companyId, string baseURL, int moduleId, int tabId, int eventId, string tabDisplayName, string tabType)
        {
            StringBuilder anchorTagContent = new StringBuilder();
            try
            {
                string eventID = (!string.IsNullOrEmpty(eventId.ToString()) && eventId.ToString() != "0") ? _cryptography.Encrypt(eventId.ToString()) : "0";

                int detailTabId = 1;

                if (!string.IsNullOrEmpty(companyId.ToString()))
                    detailTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;

                string url = string.Empty;
                string css = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "LiUserTabSelect" : "sub-tab");

                //In SU application, "Sharing" tab should show as "Team Members"
                if (css == "sub-tab" && tabDisplayName == "Sharing")
                    css = "sub-tab SUTeamMembers";
                
                string id = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "ancmain" + moduleId : "anc" + tabId);

                //string tabName = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "Details-1" : ((RemoveSpecialCharacters(tabDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(tabDisplayName)) + "-" + tabId);
                string tabName = (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString() ? "Details-" + detailTabId : ((RemoveSpecialCharacters(tabDisplayName).Length == 0) ? " " : RemoveSpecialCharacters(tabDisplayName)) + "-" + tabId);

                string moduleName = ((hlp.Enumeration.Modules)moduleId).ToString();
                string action = tabDisplayName.Contains("Create") ? "New" : "Edit";

                if (tabType == tap.dom.hlp.Enumeration.TabType.MainTab.ToString())
                    url = String.Format(baseURL + "/Event/{0}/{1}/{2}/{3}", moduleName, tabName, eventID, action);
                else if (tabType == tap.dom.hlp.Enumeration.TabType.SubTab.ToString())
                {

                    //Need  to check later(temp solution)
                    switch (tabDisplayName)
                    {
                        case "Details":
                            url = String.Format(baseURL + "/Event/{0}/Details-{1}/{2}/{3}", moduleName, tabId, eventID, action);
                            break;
                        case "Sharing":
                            url = String.Format(baseURL + "/Event/{0}/Sharing-{1}/{2}/{3}", moduleName, tabId, eventID, action);
                            break;
                        case "7 Steps":
                            url = String.Format(baseURL + "/Event/{0}/7-Steps-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        case "TapRooT®":
                            url = String.Format(baseURL + "/Event/{0}/TapRooT-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        case "Fix":
                            url = String.Format(baseURL + "/Event/{0}/Fix-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        case "Report":
                            url = String.Format(baseURL + "/Event/{0}/Report-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        case "Reference":
                            url = String.Format(baseURL + "/Event/{0}/Reference-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        case "Corrective Actions":
                            url = String.Format(baseURL + "/Event/{0}/CorrectiveActions-{1}/{2}", moduleName, tabId, eventID);
                            break;

                        default:
                            url = String.Format(baseURL + "/Event/{0}/{1}/{2}", moduleName, tabName, eventID);
                            break;

                    }

                }
                anchorTagContent.Append(String.Format("<a class='{0}'  id='{1}'  href='{2}'>{3}</a>", css, id, url, tabDisplayName));


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return anchorTagContent.ToString();
        }
        #endregion

        #region To create Details SubTabs
        /// <summary>
        /// Description-Create Details Tab
        /// </summary>
        /// <param name="ModuleId"></param>
        /// <param name="EventId"></param>
        /// <param name="virtualDir"></param>
        /// <returns></returns>
        public string BuildDetailsSubTab(int moduleId, string eventID, string baseURL, int companyId)
        {
            StringBuilder detailSubTabContent = new StringBuilder();
            try
            {
                int eventId = Convert.ToInt32((!string.IsNullOrEmpty(eventID) && eventID != "0") ? _cryptography.Decrypt(eventID) : "0");
                int detailTabId = 1;
                if (!string.IsNullOrEmpty(companyId.ToString()))
                    detailTabId = _db.Tabs.Where(a => a.CompanyID == companyId && a.Active == true).FirstOrDefault().TabID;

                string css = "selected-tab";
                string id = "anc1";
                string action = "New";
                string url = String.Format(baseURL + "/Event/{0}/{1}/{2}/{3}", ((hlp.Enumeration.Modules)moduleId).ToString(), "Details-" + detailTabId, eventID, action);
                detailSubTabContent.Append("<ul id='new-event-detail'><li>");
                detailSubTabContent.Append(String.Format("<a class='{0}'  id='{1}'  href='{2}'>{3}</a>", css, id, url, "Details"));
                detailSubTabContent.Append("<div  class='arrow-down'></div></li></ul>");
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return detailSubTabContent.ToString();
        }
        #endregion

        public string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }
    }

}
