﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace tap.dom.jsc
{
    public class EFJavaScriptSerializer:JavaScriptSerializer
    {
        public EFJavaScriptSerializer()
        {
            RegisterConverters(new List<JavaScriptConverter> { new EFJavaScriptConverter() });
        }
    }
}
