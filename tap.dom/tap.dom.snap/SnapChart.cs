﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using tap.dom.gen;
using tap.dom.hlp;
using tap.dom.transaction;

namespace tap.dom.snap
{
    public class SnapChart
    {
        tap.dat.EFEntity _db = new dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.hlp.EFSerializer _efSerializer = new hlp.EFSerializer();
        JavaScriptSerializer js = new JavaScriptSerializer();

        private string _message;

        #region Saves the snapchart on changing the season .

        /// <summary>
        /// On change of the season , saves the snapchart graph .
        /// </summary>
        /// <param name="eventID"></param>
        /// <param name="seasonId"></param>
        /// <param name="snapChartData"></param>
        /// <returns></returns>
        public string SnapChartXmlSave(string eventID, int seasonId, string snapChartData)
        {
            string message = string.Empty;
            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();
            string seasonName = _db.Seasons.FirstOrDefault(a => a.SeasonID == seasonId).SeasonName;//Use Enumeration
            try
            {
                bool isNewSnapChart = true;
                string statusMessage = string.Empty;
                tap.dat.SnapCharts snapChart = null;
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

                snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId);
                if (snapChart != null)
                    isNewSnapChart = false;

                if (isNewSnapChart)
                {
                    snapChart = new tap.dat.SnapCharts();
                    message = "SnapCharT® Inserted";
                    _message = "SnapChart for season " + seasonName + " inserted";
                    transactionCategory = Enumeration.TransactionCategory.Create;
                }
                else
                {
                    message = "SnapCharT® Updated";
                    _message = "SnapChart for " + seasonName + " season updated";
                    transactionCategory = Enumeration.TransactionCategory.Update;
                }

                snapChart.EventID = eventId;
                snapChart.SeasonID = seasonId;
                snapChart.SnapChartData = snapChartData;

                if (isNewSnapChart)
                {
                    _db.AddToSnapCharts(snapChart);
                }

                _db.SaveChanges();

                //Update date modified for event
                tap.dom.usr.Event.UpdateDateModified(eventId);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }


            return message;


        }

        #endregion        

        #region SaveSnapChart
        /// <summary>
        /// To save the investigation season(spring,Summer,Autumn or winter)
        /// </summary>
        /// <param name="snapChartId"></param>
        /// <param name="eventId"></param>
        /// <param name="seasonId"></param>
        /// <param name="isComplete"></param>
        /// <param name="snapChartData"></param>
        /// <returns></returns>
        //public string SaveSnapChart(string mode, string eventID, int seasonId, bool isComplete, string snapChartData, bool isReverse, int[] causalfactorsId, string[] causalfactors)
        public string SaveSnapChart(string mode, string eventID, int seasonId, bool isComplete, string snapChartData, bool isRevision, int[] causalfactorsId, string[] causalfactors, int userId)
        {
            string message = string.Empty;
            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();
            string seasonName = _db.Seasons.FirstOrDefault(a => a.SeasonID == seasonId).SeasonName;
            try
            {
                int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
                bool isNewSnapChart = true;
                tap.dat.SnapCharts snapChart = null;
                if (seasonId == (int)tap.dom.hlp.Enumeration.Season.Winter)
                {
                    snapChart = (from a in _db.SnapCharts where a.EventID == eventId orderby a.SeasonID descending select a).FirstOrDefault();
                    isNewSnapChart = snapChart.SeasonID == seasonId ? false : true;
                }
                else
                {
                    snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId);
                    if (snapChart != null)
                        isNewSnapChart = false;
                    //snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);
                }

                if (isNewSnapChart)
                {
                    snapChart = new tap.dat.SnapCharts();
                    message = "Inserted";
                    _message = "SnapChart for season " + seasonName + " inserted";
                    transactionCategory = Enumeration.TransactionCategory.Create;
                }
                else
                {
                    //for deleting and recreating the snapchart
                    // DeleteSnapCharts(eventId, seasonId);
                    message = "Updated";
                    _message = "SnapChart for " + seasonName + " season updated";
                    transactionCategory = Enumeration.TransactionCategory.Update;
                }

                snapChart.EventID = eventId;
                snapChart.SeasonID = seasonId;



                //snapChart.IsComplete = isComplete;
                snapChart.SnapChartData = snapChartData;

                //snapChart.IsCurrent = isReverse;
                //snapChart.IsCurrent = (isReverse ? true : false);

                if (isNewSnapChart)
                {
                    _db.AddToSnapCharts(snapChart);
                }

                _db.SaveChanges();
                Transaction.SaveTransaction(eventId, userId, null, transactionCategory, _message);


                #region Save causalfactors

                //if (causalfactors.Length > 0 && mode == "Save")
                if (causalfactorsId.Length > 0 && mode == "Save")
                {
                    //Get the snapchartId
                    snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);
                    if (snapChart != null)
                    {
                        CausalFactorSave(eventId, snapChart.SnapChartID, causalfactorsId, causalfactors, userId);
                    }
                }

                #endregion

                #region Delete causalfactor

                if (mode == "Delete" && causalfactors.Length > 0)
                {
                    string status = CausalFactorRemove(eventId, seasonId, causalfactorsId, causalfactors);
                    if (status != string.Empty)
                        message = status;
                }

                #endregion

                #region Update the Season
                bool seasonStatus = false;
                if (seasonId == 3)
                {
                    seasonStatus = SnapChartSeasonComplete(eventID, seasonId, isComplete, userId);
                }
                else
                {
                    if (isComplete)
                    {
                        seasonStatus = SnapChartSeasonComplete(eventID, seasonId, isComplete, userId);
                    }
                }

                if (seasonStatus)
                    message = "SeasonUpdated";

                #endregion

                #region Revision of season

                if (isRevision)
                {
                    bool revisionStatus = SnapchartSeasonRevisionUpdate(seasonId, eventID);
                    if (revisionStatus)
                        message = "SeasonRevisioned";
                }


                #endregion

                //Update date modified for event
                tap.dom.usr.Event.UpdateDateModified(eventId);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return message;
        }
        #endregion

        #region Update the season changes

        //public bool SnapChartSeasonComplete(int eventId, int seasonId, bool isSeasonCompleted)
        public bool SnapChartSeasonComplete(string eventID, int seasonId, bool isSeasonCompleted, int userId)
        {
            bool isChanged = false;
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            //Update query
            tap.dat.SnapCharts snapChart = null;
            snapChart = SnapChartGet(eventId, seasonId);
            //snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);
            if (snapChart != null)
            {
                //snapChart.IsComplete = isSeasonCompleted;




                if (seasonId == 3)
                {
                    snapChart.IsComplete = isSeasonCompleted;
                }
                else
                {
                    snapChart.SeasonID = seasonId + 1;
                }

                _db.SaveChanges();

                isChanged = true;

                //Better can use Enumeration of seasons instead of hitting database for just season name.                
                tap.dat.Seasons season = _db.Seasons.FirstOrDefault(a => a.SeasonID == seasonId);
                Transaction.SaveTransaction(eventId, userId, null, Enumeration.TransactionCategory.Update, season.SeasonName + " Season " + "completed");

            }

            return isChanged;
        }

        #endregion

        #region Get the snapchartId based on event id and season id
        public int SnapchartIdSelect(string eventID, int seasonId)
        {
            int snapchartId = -1;
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            tap.dat.SnapCharts snapChart = null;

            //Get snapchartId either from Automn season or summer season
            snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && (a.SeasonID == seasonId || a.SeasonID == seasonId - 1));

            if (snapChart != null)
            {
                snapchartId = snapChart.SnapChartID;
                return snapchartId;
            }

            return snapchartId;
        }
        #endregion

        #region Causal factor Save

        //Save the causal factor.
        public void CausalFactorSave(int eventId, int snapchartId, int[] causalfactorsId, string[] causalfactors, int userId)
        {
            tap.dat.CausalFactors causalfactor = new dat.CausalFactors();
            causalfactor.SnapChartID = snapchartId;
            causalfactor.CausalFactorShapeId = causalfactorsId[causalfactorsId.Length - 1];
            causalfactor.Name = causalfactors[causalfactors.Length - 1];

            _db.AddToCausalFactors(causalfactor);
            _db.SaveChanges();

            Transaction.SaveTransaction(eventId, userId, null, Enumeration.TransactionCategory.Create, "Causal factor '" + causalfactors[causalfactors.Length - 1] + "' added");
        }

        #endregion.

        #region Causal factor Update

        public void CausalFactorUpdate(string eventID, int seasonId, int causalfactorId, string causalfactorName)
        {
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            tap.dat.SnapCharts snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);
            if (snapChart != null)
            {
                tap.dat.CausalFactors causalfactor = _db.CausalFactors.FirstOrDefault(a => a.SnapChartID == snapChart.SnapChartID && a.CausalFactorShapeId == causalfactorId);
                if (causalfactor != null)
                {
                    causalfactor.Name = causalfactorName;
                    _db.SaveChanges();
                }
            }

        }

        #endregion

        #region Causal factor Remove

        //Delete the causal factor.
        public string CausalFactorRemove(int eventId, int seasonId, int[] causalfactorsId, string[] causalfactors)
        {
            string message = string.Empty;
            //Get the snapchartId
            tap.dat.SnapCharts snapChart = SnapChartGet(eventId, seasonId);

            if (snapChart != null)
            {
                //Get the Causal factor data.
                tap.dat.CausalFactors causalfactor = CausalFactorGet(causalfactorsId, snapChart);

                if (causalfactor != null)
                {
                    //Check for root cause tree.                    
                    tap.dat.RootCauseTree rootcausetree = RootCauseTreeCheck(causalfactor);

                    //Delete Causal factor.
                    _db.DeleteObject(causalfactor);

                    if (rootcausetree == null)
                    {
                        //_db.DeleteObject(causalfactor);
                        message = "Causal factor deleted";
                    }

                    if (rootcausetree != null)
                    {
                        //Delete Causal factor.
                        //_db.DeleteObject(causalfactor);

                        var rootcausetreeList = (from rct in _db.RootCauseTree
                                                 where rct.CausalFactorID == causalfactor.CausalFactorID
                                                 select rct);

                        //Delete the RCT for the causal factor.
                        foreach (tap.dat.RootCauseTree rct in rootcausetreeList)
                        {
                            _db.DeleteObject(rct);
                        }

                        message = "Causal factor and RCT deleted";
                    }

                    _db.SaveChanges();
                }

            }
            return message;
        }

        #endregion

        #region Get the snapchart id

        //Get the snapchartId
        public tap.dat.SnapCharts SnapChartGet(int eventId, int seasonId)
        {
            tap.dat.SnapCharts snapChart = new dat.SnapCharts();
            snapChart = _db.SnapCharts.FirstOrDefault(a => a.EventID == eventId && a.SeasonID == seasonId);
            return snapChart;
        }

        #endregion

        #region Get the Causal factor data

        //Get the Causal factor data.
        public tap.dat.CausalFactors CausalFactorGet(int[] causalfactorsId, tap.dat.SnapCharts snapChart)
        {
            tap.dat.CausalFactors causalfactor = new dat.CausalFactors();
            int causalfactorShapeId = causalfactorsId[causalfactorsId.Length - 1];
            causalfactor = _db.CausalFactors.FirstOrDefault(e => e.CausalFactorShapeId == causalfactorShapeId && e.SnapChartID == snapChart.SnapChartID);
            return causalfactor;
        }

        #endregion

        #region Check the RootCauseTree for the Causal factor

        //Check for root cause tree.
        public tap.dat.RootCauseTree RootCauseTreeCheck(tap.dat.CausalFactors causalfactor)
        {
            //Check for root cause tree.
            tap.dat.RootCauseTree rootcausetree = new dat.RootCauseTree();
            rootcausetree = _db.RootCauseTree.FirstOrDefault(e => e.CausalFactorID == causalfactor.CausalFactorID);

            return rootcausetree;
        }

        #endregion

        #region Get default shapes

        //Get the default shapes by the user id.
        public object DefaultShapesByUserIdGet(int userId)
        {
            var shapes = from shape in _db.DefaultShapes
                         where shape.UserId == userId
                         select new
                         {
                             shapeType = shape.ShapeType,
                             color = shape.BackgroundColor,
                             fontFamily = shape.FontFamily,
                             fontSize = shape.FontSize,
                             fontColor = shape.FontColor,
                             bold = shape.Bold,
                             italic = shape.Italic,
                             underline = shape.Underline,
                             fontStyle = shape.FontStyle
                         };

            return _efSerializer.EFSerialize(shapes);
        }

        #endregion

        #region Default shapes data Insert/Update

        public string DefaultShapeSave(int userId, string shapeType, string key, string value)
        {
            string message = string.Empty;
            bool isDefaultShape = true;
            tap.dat.DefaultShapes defaultShape = null;

            defaultShape = _db.DefaultShapes.FirstOrDefault(a => a.UserId == userId && a.ShapeType == shapeType);

            if (defaultShape != null)
            {
                isDefaultShape = false;
            }

            if (isDefaultShape)
            {
                message = "inserted";
                defaultShape = new tap.dat.DefaultShapes();
            }
            else
            {
                message = "updated";
            }

            defaultShape.UserId = userId;
            defaultShape.ShapeType = shapeType;

            #region Default shape Switch

            switch (key)
            {
                case "BackgroundColor":
                    defaultShape.BackgroundColor = value;
                    break;
                case "FontColor":
                    defaultShape.FontColor = value;
                    break;
                case "FontSize":
                    defaultShape.FontSize = Convert.ToInt32(value);
                    break;
                case "FontFamily":
                    defaultShape.FontFamily = value;
                    break;
                case "Bold":
                    defaultShape.Bold = Convert.ToBoolean(value);
                    break;
                case "Italic":
                    defaultShape.Italic = Convert.ToBoolean(value);
                    break;
                case "Underline":
                    defaultShape.Underline = Convert.ToBoolean(value);
                    break;
                case "FontStyle":
                    defaultShape.FontStyle = Convert.ToInt32(value);
                    break;
                default:
                    break;
            }

            #endregion

            if (isDefaultShape)
            {
                _db.AddToDefaultShapes(defaultShape);
            }

            _db.SaveChanges();

            return message;
        }

        #endregion

        #region Save Shape Defaults styles
        //Save Shape Defaults style
        public string DefaultShapeSaveAll(int userId, string shapeType, string styles)
        {
            string message = string.Empty;
            bool isDefaultShape = true;
            tap.dat.DefaultShapes defaultShape = null;

            defaultShape = _db.DefaultShapes.FirstOrDefault(a => a.UserId == userId && a.ShapeType == shapeType);

            if (defaultShape != null)
            {
                isDefaultShape = false;
            }

            if (isDefaultShape)
            {
                message = "inserted";
                defaultShape = new tap.dat.DefaultShapes();
                defaultShape.UserId = userId;
                defaultShape.ShapeType = shapeType;
            }
            else
            {
                message = "updated";
            }            
            try
            {
                if (styles != null && styles.Length > 0)
                {
                    string[] splitStyle = styles.Split(',');

                    foreach (string item in splitStyle)
                    {
                        if (item.Contains(':'))
                        {
                            string[] splitKey = item.Split(':');
                            if (splitStyle != null)
                            {
                                if (splitKey[0].ToLower() == Enumeration.DefaultShapeStyle.FontSize.ToString().ToLower())
                                    defaultShape.FontSize = Convert.ToInt32(splitKey[1]);

                                if (splitKey[0].ToLower() == Enumeration.DefaultShapeStyle.FontFamily.ToString().ToLower())
                                    defaultShape.FontFamily = splitKey[1];

                                if (splitKey[0].ToLower() == Enumeration.DefaultShapeStyle.FontColor.ToString().ToLower())
                                    defaultShape.FontColor = splitKey[1];

                                if (splitKey[0].ToLower() == Enumeration.DefaultShapeStyle.BackgroundColor.ToString().ToLower())
                                    defaultShape.BackgroundColor = splitKey[1];

                                if (splitKey[0].ToLower() == Enumeration.DefaultShapeStyle.FontStyle.ToString().ToLower())
                                    defaultShape.FontStyle = Convert.ToInt32(splitKey[1]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            if (isDefaultShape)
            {
                _db.AddToDefaultShapes(defaultShape);
            }

            _db.SaveChanges();

            return message;
        }
        #endregion

        #region Check for the Root cause tree of the causal factor.

        //Check for the Root cause tree of the causal factor.
        public string CausalFactorWithRootCauseTreeCheck(string eventID, int seasonId, int[] causalfactorsId)
        {

            string message = string.Empty;
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));

            //Get the snapchartId
            tap.dat.SnapCharts snapChart = SnapChartGet(eventId, seasonId);

            if (snapChart != null)
            {
                //Get the Causal factor data.
                tap.dat.CausalFactors causalfactor = CausalFactorGet(causalfactorsId, snapChart);

                if (causalfactor != null)
                {
                    //Check for root cause tree.                    
                    tap.dat.RootCauseTree rootcausetree = RootCauseTreeCheck(causalfactor);
                    message = rootcausetree == null ? "NoRootCauseTree" : "RootCauseTreeExists";
                }
                else
                    message = "NoCausalFactoR";

            }
            else
                message = "NoSnapCharT";

            return message;
        }

        #endregion

        #region Get the revisioned season data

        public bool SnapchartSeasonRevisionUpdate(int seasonId, string eventID)
        {
            bool isRevisionStatus = false;
            int eventId = Convert.ToInt32(_cryptography.Decrypt(eventID));
            tap.dat.SnapCharts snapchart = new dat.SnapCharts();
            snapchart = SnapChartGet(eventId, seasonId);

            if (snapchart != null && seasonId > (int)tap.dom.hlp.Enumeration.Season.Spring)
            {
                snapchart.SeasonID = seasonId - 1;
                _db.SaveChanges();
                isRevisionStatus = true;
            }

            return isRevisionStatus;
        }

        #endregion

        #region Get all the snapcaps titles by event.
        public string SnapcapsTitlesByEventGet(int userId, string eventId)
        {
            int eventID = Convert.ToInt32(_cryptography.Decrypt(eventId));
            var snapcaptitles = _db.SnapCaps.Where(a => a.EventID == eventID).Select(u => u.SnapCapName).ToArray();
            return js.Serialize(snapcaptitles);
        }

        #endregion

        #region Get all the snapcaps Common Types
        public object SnapcapsCommonTypesGet(int userId, string eventId)
        {
            int eventID = Convert.ToInt32(_cryptography.Decrypt(eventId));
            var ctypes = from CT in _db.SnapCapCommonTypes
                         select new
                         {
                             SnapCapCommonTypeId = CT.SnapCapCommonTypeId,
                             SnapCapCommonTypeName = CT.SnapCapCommonTypeName,
                             isAlreadyUsed = (System.Int64)
                               (from SC in _db.SnapCaps
                                where
                                  (Int32)SC.SnapCapCommonTypeId == CT.SnapCapCommonTypeId &&
                                  SC.UserID == userId   &&
                                  SC.EventID == eventID
                                select new
                                {
                                    SC.SnapCapID
                                }).Count(p => p.SnapCapID != null) == 0 ? 0 : 1
                         };

            return _efSerializer.EFSerialize(ctypes);



        }

        #endregion


    }
}
