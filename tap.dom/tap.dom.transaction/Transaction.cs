﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
using System.Net;
using System.Management;
using System.Net.NetworkInformation;

namespace tap.dom.transaction
{
    public class Transaction
    {

        static tap.dat.EFEntity _db = new tap.dat.EFEntity();

        #region ""
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventID"></param>
        /// <param name="userID"></param>
        /// <param name="transactionMessage"></param>
        public static void SaveTransaction(int? eventID, int userId, int? companyID,
            Enumeration.TransactionCategory? transactionType, string transactionMessage)
        {
            try
            {
                int _userId = Convert.ToInt32(userId);
                int? _companyId = _db.Users.FirstOrDefault(a => a.UserID == _userId).CompanyID;

                tap.dat.UserTransactionsDaily userTransaction = new tap.dat.UserTransactionsDaily();
                userTransaction.EventID = eventID;
                userTransaction.UserID = _userId;
                userTransaction.CompanyID = Convert.ToInt32(_companyId);
                userTransaction.Details = transactionMessage;
                userTransaction.TransactionTypeID = Convert.ToInt32(transactionType);
                userTransaction.TransactionDate = DateTime.UtcNow; //GMT;          
                userTransaction.MachineName = string.Empty;
                userTransaction.IpAddress = null; //GetIPAddress();
                userTransaction.MACAddress = GetMacAddress();

                _db.AddToUserTransactionsDaily(userTransaction);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }            
        }

        #endregion

        #region " to get IP and MAC Address"
        /// <summary>
        /// GetIP() - get ip address -local
        /// </summary>
        /// <returns></returns>
        public static String GetIPAddress() 
        {
            string strHostName = string.Empty;
            string ipAddress = string.Empty;

            strHostName = System.Net.Dns.GetHostName();

            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);

            IPAddress[] addr = ipEntry.AddressList;

            ipAddress = addr[2].ToString();
            return ipAddress;
        }       
      
        /// <summary>
        /// Mac Address
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            string macAddresses = string.Empty;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }

            return macAddresses;
        }

        #endregion

    }
     
}
