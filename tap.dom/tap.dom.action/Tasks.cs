﻿/********************************************************************************
 *  File Name   : Tasks.cs
 *  Description : This file contains all the database related code for the Tasks table
 *  Created on  : N/A
 *******************************************************************************/

# region NameSpace

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using tap.dat;
using tap.dom.gen;
using tap.dom.hlp;
using tap.dom.usr;

#endregion


namespace tap.dom.action
{
    /// <summary>
    /// This class contains all the database related code for the Tasks table
    /// </summary>
    public class Tasks
    {
        enum TaskTypeId
        {
            Implementation = 1, Verification, Validation
        }

        //the private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();
        private string _message = string.Empty;

        #region "Save Task"
        /// <summary>
        /// Save the task for new task or edit task
        /// </summary>
        /// <param name="taskID"> TaskId for the task</param>
        /// <param name="correctiveActionID">For which ca the task is getting created/updated</param>
        /// <param name="taskTypeID">which type of task is getting created/updated</param>
        /// <param name="responsiblePersonID">who is responsible for the task getting created/updated</param>
        /// <param name="description">description of the task getting created/updated</param>
        /// <param name="notes">notes for task</param>
        /// <param name="isCompleted"> completed status of task</param>
        /// <param name="dueDate">dueDate set for the task getting created/updated</param>
        public int SaveTask(int newInsert, int correctiveActionID, int taskTypeID, string responsiblePersonID, string description, 
                            string notes, int isCompleted, DateTime dueDate,int companyId,int userId,bool isDirectTask, bool? isNewInitalDate = false)
        {

            int personId = 0;
            try
            {

                //Check if the save is for new task or edit task
                bool isNewInsert = (newInsert == 0 ? true : false);
                //notes = individualTask ? notes : "";

                //Get the details of task if is called for editing

                tap.dat.Tasks task = isNewInsert ? new tap.dat.Tasks() : GetTaskByID(correctiveActionID, taskTypeID,newInsert);
                DateTime? previousDueDate = task.DueDate;


                SetProperties(ref task, correctiveActionID, taskTypeID, description, notes, isCompleted, dueDate);

                //Save the changes to db
                SaveChanges(isNewInsert, task, userId);


              
                //Set all properties for the task to be saved
               

                //If the task is a new one or if the task is the updating one and the due date is updated for that, then save due date history
                //if (isNewInsert || (task.DueDate != previousDueDate))
               if (isNewInsert || (task.DueDate != previousDueDate))
               {
                   SaveDueDate(isNewInsert, dueDate, taskID: task.TaskID);
               }
                

                //Update date modified for event
                int eventId = _entityContext.CorrectiveActions.FirstOrDefault(a => a.CorrectiveActionID == correctiveActionID).EventID;

                tap.dom.usr.Event.UpdateDateModified(eventId);

                if (isDirectTask)
                {
                    int responsiblePersonId = 0;
                    tap.dat.TasksResponsiblePersons taskResponsiblePersons = new tap.dat.TasksResponsiblePersons();
                    if (!isNewInsert)
                    {                       
                        taskResponsiblePersons = _entityContext.TasksResponsiblePersons.FirstOrDefault(a => a.TaskId == task.TaskID);
                        responsiblePersonId = taskResponsiblePersons.TasksResponsiblePersonId;
                    }
                    personId = SaveTasksResponsiblePerson(newInsert, responsiblePersonId, companyId, userId, task.TaskID, responsiblePersonID, true);//remove hard code  
                }

                return task.TaskID;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return 0;
        }

        #endregion


        #region Default shapes data Insert/Update


        public string UpdateTasks(int taskID, int taskTypeID, string responsiblePersonID, string description,
                                  string notes, int isCompleted, string dueDate, int companyId, int userId, bool isDirectTask)
        {
            string message = string.Empty;

            tap.dat.Tasks tasks = null;

            tasks = _entityContext.Tasks.FirstOrDefault(a => a.TaskID == taskID);

            tasks.TaskTypeID = taskTypeID;

            bool isNewInsert = (tasks.CorrectiveActionID == Convert.ToInt32(tap.dom.hlp.Enumeration.Types.NewInsert)) ? true : false;


            tap.dat.TasksResponsiblePersons taskResponsiblePersons = isNewInsert ? new tap.dat.TasksResponsiblePersons() : _entityContext.TasksResponsiblePersons.FirstOrDefault(a => a.TaskId == taskID);


            var personid = SaveTasksResponsiblePerson(Convert.ToInt32(isNewInsert), taskResponsiblePersons.TasksResponsiblePersonId, companyId, userId, tasks.TaskID, responsiblePersonID, true);//will remove true hard code soon

            DateFormat dt = new DateFormat();
            //DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
            //dtfi.ShortDatePattern = "dd-MM-yyyy";
            //dtfi.DateSeparator = "-";
           // string duedt = dt.convertAsAdminDate(dueDate);
            //if (dueDate != null && dueDate.Length > 0)
            //{

            //    if (dueDate.Contains('/'))
            //    {
            //        string[] dateParams = dueDate.Split('/');
            //        if (dateParams[2].Length == 2)
            //            dateParams[2] = "20" + dateParams[2];
            //        DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[0]), Int32.Parse(dateParams[1]));
            //        duedt = date.ToString();
            //    }
            //    else
            //    {

            //        duedt = Convert.ToDateTime(dueDate, dtfi).ToString();
            //    }
            //}

            tasks.Description = description;
            tasks.DueDate = dt.fromDatefrmatToDate(dueDate, companyId);
            tasks.Notes = notes;
            tasks.IsCompleted = (isCompleted == 1);


            _entityContext.SaveChanges();

            return message;
        }

        #endregion

        #region "Save Responsible person task details"
        public int SaveTasksResponsiblePerson(int newInsert, int tasksResponsiblePersonId, int companyId, int userId, int taskId, string displayName ,bool isManualPersonAdd)
        {
            try
            {
                //Check if the save is for new task or edit task
                bool isNewInsert = (newInsert == 0) ? true : false;

                //Get the details of task if is called for editing
                tap.dat.TasksResponsiblePersons taskResponsiblePersons = isNewInsert ? new tap.dat.TasksResponsiblePersons() : GetTaskResponsiblePersonById(tasksResponsiblePersonId);

                //Save the changes to db
                int taskResponsiblePersonId = SaveResponsiblePersonChanges(isNewInsert, taskResponsiblePersons, companyId, userId, taskId, displayName, isManualPersonAdd);

                return taskResponsiblePersonId;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return 0;
        }
       

        public tap.dat.TasksResponsiblePersons GetTaskResponsiblePersonById(int tasksResponsiblePersonId)
        {

            tap.dat.TasksResponsiblePersons taskResponsiblePerson = new tap.dat.TasksResponsiblePersons();

            try
            {
                taskResponsiblePerson = _entityContext.TasksResponsiblePersons.FirstOrDefault(a => a.TasksResponsiblePersonId == tasksResponsiblePersonId);

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return taskResponsiblePerson;

        }

        public int SaveResponsiblePersonChanges(bool isNewInsert, tap.dat.TasksResponsiblePersons tasksResponsiblePerson, int companyId, int userId, int taskId,
            string displayName, bool isManualPersonAdd)
        {

            tap.dat.TasksResponsiblePersons taskResponsiblePersons = isNewInsert ? new tap.dat.TasksResponsiblePersons() : tasksResponsiblePerson;
            tap.dat.Users user = new tap.dat.Users();

            int userid = 0;
            if (!isManualPersonAdd)
            {
                var userResult = _entityContext.Users.FirstOrDefault(a => a.UserName == displayName || a.FirstName + " " + a.LastName == displayName);
                if (userResult != null)
                    userid = userResult.UserID;
            }
                
            taskResponsiblePersons.TasksResponsiblePersonId = tasksResponsiblePerson.TasksResponsiblePersonId;
            taskResponsiblePersons.CompanyId = (isManualPersonAdd) ? 0 : companyId;
            taskResponsiblePersons.UserId = (isManualPersonAdd) ? 0 : userid;
            taskResponsiblePersons.TaskId = taskId;
            taskResponsiblePersons.DisplayName = displayName;

            //SaveChanges
            if (isNewInsert)
                _entityContext.AddToTasksResponsiblePersons(taskResponsiblePersons);

            _entityContext.SaveChanges();

            return taskResponsiblePersons.TasksResponsiblePersonId;
        }
        #endregion

       
        #region "Save Due Date"
        /// <summary>
        /// Insert into TaskDueDates table, which is used to get the due date history for a task
        /// </summary>
        /// <param name="isNewInsert"> if insert is new </param>
        /// <param name="dueDate">due date of task </param>
        /// <param name="taskID">task id </param>
        public void SaveDueDate(bool isNewInsert, DateTime dueDate, int taskID)
        {
            try
            {               
                tap.dat.TaskDueDates taskDueDates = null;
                DateTime? nullDateTime = null;
                bool isNewRecordInsertion = true;
                int correctiveAtionId = _entityContext.Tasks.Where(a => a.TaskID == taskID).Select(a => a.CorrectiveActionID).FirstOrDefault();
                int taskTypeId = 0;

                if (isNewInsert)
                {
                    taskDueDates = new tap.dat.TaskDueDates();
                }
                else
                {
                    var taskduedate = _entityContext.TaskDueDates.Where(a => a.TaskID == taskID).FirstOrDefault();
                    if (taskduedate.DueDate == null && taskduedate.IsInitialDate == true)
                    {
                        taskDueDates = taskduedate;
                        isNewRecordInsertion = false;
                        isNewInsert = true;
                    }
                    else
                    {
                        taskDueDates = new tap.dat.TaskDueDates();
                    }
                }

                taskDueDates.IsInitialDate = isNewInsert;                
                taskDueDates.TaskID = taskID;

                taskDueDates.DueDate = dueDate == DateTime.Parse("1/1/0001 12:00:00 AM") ? nullDateTime : dueDate;
                //SaveChanges
                if(isNewRecordInsertion)
                    _entityContext.AddToTaskDueDates(taskDueDates);


                _entityContext.SaveChanges();

                // Update the CorrectiveAction table with the new/revised due date
                tap.dat.CorrectiveActions correctiveAction = _entityContext.CorrectiveActions.Where(a => a.CorrectiveActionID == correctiveAtionId).FirstOrDefault();
                taskTypeId = _entityContext.Tasks.Where(a => a.TaskID == taskID).Select(a => a.TaskTypeID).FirstOrDefault();

                switch (taskTypeId)
                {
                    case (int)TaskTypeId.Implementation:
                        correctiveAction.T_InitialDueDate = taskDueDates.DueDate;
                        break;

                    case (int)TaskTypeId.Validation:
                        correctiveAction.E_InitialDueDate = taskDueDates.DueDate;
                        break;

                    case (int)TaskTypeId.Verification:
                        correctiveAction.M_InitialDueDate = taskDueDates.DueDate;
                        break;
                }

                _entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        #endregion

        #region "Save Changes"
        /// <summary>
        /// Save changes to the database
        /// </summary>
        /// <param name="isNewInsert"></param>
        /// <param name="task"></param>
        public void SaveChanges(bool isNewInsert, tap.dat.Tasks task, int userId)
        {

            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();

            try
            {
                //If new task is getting created then add to the task table
                if (isNewInsert)
                {
                    _entityContext.AddToTasks(task);
                    _message = "TaskId created for corrective action " + task.CorrectiveActionID.ToString();
                    transactionCategory = Enumeration.TransactionCategory.Create;
                }
                else
                {
                    _message = "TaskId updated for corrective action '" + task.CorrectiveActionID.ToString() + "'";
                    transactionCategory = Enumeration.TransactionCategory.Update;
                }
              

                //Save the changes
                _entityContext.SaveChanges();

                _message = _message.Replace("TaskId", "TaskId '" + task.TaskID + "'");
                transaction.Transaction.SaveTransaction(null, userId, null, transactionCategory, _message);  


            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        /// <summary>
        /// Set column values/ properties for the task getting saved or created
        /// </summary>
        /// <param name="task"></param>
        /// <param name="correctiveActionID"></param>
        /// <param name="taskTypeID"></param>
        /// <param name="responsiblePersonID"></param>
        /// <param name="description"></param>
        public void SetProperties(ref tap.dat.Tasks task, int correctiveActionID, int taskTypeID, 
            string description, string notes, int isCompleted, DateTime dueDate)
        {

            try
            {
                DateTime? nullDateTime = null;
               
                task.CorrectiveActionID = correctiveActionID;
                task.TaskTypeID = taskTypeID;
            
                task.Description = description;
                task.DueDate = dueDate == DateTime.Parse("1/1/0001 12:00:00 AM") ? nullDateTime : dueDate;
                task.Notes = notes;
                task.IsCompleted = (isCompleted == 1);
                _entityContext.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
        #endregion

        #region "Get Task By Id"
        /// <summary>
        /// Get the task details by the task id
        /// </summary>
        /// <param name="taskID">TaskId by which the whole task can be fetched out from db</param>
        /// <returns>task</returns>
        public tap.dat.Tasks GetTaskByID(int correctiveActionId, int taskTypeID, int taskId)
        {

            tap.dat.Tasks task = new tap.dat.Tasks();

            try
            {
                //Removed the tasktypeid from the condition as it was restricting the tasktype updation .
                task = _entityContext.Tasks.FirstOrDefault(a => a.CorrectiveActionID == correctiveActionId  && a.TaskID == taskId);
                //task = _entityContext.Tasks.FirstOrDefault(a => a.CorrectiveActionID == correctiveActionId && a.TaskTypeID==taskTypeID && a.TaskID == taskId);
              
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return task;

        }
        public string GetResponsiblePerson(tap.dat.TasksResponsiblePersons taskrp)
        {
            string Fullname;
            if (taskrp.UserId != 0)
            {
                var taskRspName = (from users in _entityContext.Users
                                   where users.UserID == taskrp.UserId
                                   select new
                                   {

                                       fullName = users.FirstName + " " + users.LastName + " (" + users.Email + ")"
                                   }).ToList();

                List<string> Fullnamelst = taskRspName.Select(e => e.fullName).ToList();
                Fullname = Fullnamelst[0].ToString();
            }
            else Fullname = taskrp.DisplayName;

         
             return Fullname;

        }

        /// <summary>
        /// Get the task details by task id
        /// </summary>
        /// <param name="taskID">task id of task</param>
        /// <returns>task</returns>
        public object GetTaskDetailsByID(int taskID)
        {

            try
            {

                var taskDetail = (from taskDetails in _entityContext.Tasks.AsEnumerable()
                                  join taskResponsiblePerson in _entityContext.TasksResponsiblePersons.AsEnumerable() on taskDetails.TaskID equals taskResponsiblePerson.TaskId
                                 where taskDetails.TaskID == taskID
                                 select new
                                 {
                                     TaskID = taskDetails.TaskID,
                                     CorrectiveActionID = taskDetails.CorrectiveActionID,
                                     TaskTypeID = taskDetails.TaskTypeID,
                                     ResponsiblePersonID = GetResponsiblePerson(taskResponsiblePerson),
                                     Description = taskDetails.Description,
                                     DueDate = taskDetails.DueDate,
                                     Notes = taskDetails.Notes,
                                     IsCompleted = taskDetails.IsCompleted
                                 });

                return taskDetail;

            }
            catch (Exception ex)
            {

                tap.dom.hlp.ErrorLog.LogException(ex);
                return null;

            }

        }
        #endregion

        #region "GetTaskDueDateDetailsByID"
        /// <summary>
        ///  Get all the task due dates history by task id
        /// </summary>
        /// <param name="taskID">task id of task</param>
        /// <returns>task due dates</returns>
        public object GetTaskDueDateDetailsByID(int taskID)
        {

            try
            {

                var taskDueDateDetails = _entityContext.TaskDueDates.Select(a => a.TaskID == taskID).ToList();
                return taskDueDateDetails;

            }
            catch (Exception ex)
            {

                tap.dom.hlp.ErrorLog.LogException(ex);
                return null;

            }
        }
        #endregion
    }
}
