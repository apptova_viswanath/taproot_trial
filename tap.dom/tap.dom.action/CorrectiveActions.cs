﻿/********************************************************************************
 *  File Name   : CorrectiveActions.cs
 *  Description : This file contains all the database related code for the CorrcetiveAction table
 *  Created on  : N/A
 *******************************************************************************/

# region NameSpace

using System;
using System.Globalization;
using System.Linq;
using tap.dom.gen;
using tap.dom.hlp;

#endregion

namespace tap.dom.action
{
    //This class contains all the functions related to corrective action
    public class CorrectiveActions
    {
        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        tap.dom.usr.DateFormat dt = new usr.DateFormat();

        string _message = string.Empty;

        #region "Save Corrective Action Details"
        /// <summary>
        /// Save the corrective action
        /// </summary>
        /// <param name="correctiveActionID"> corrective action id </param>
        /// <param name="eventID"> event id </param>
        /// <param name="isSmarter"> is action is a smarter action plan </param>
        /// <param name="identifier"> identifier of action </param>
        /// <param name="description"> description of action plan </param>
        /// <param name="businessCase"> description of action plan </param>
        /// <param name="reviewNotes"> description of action plan </param>
        /// <param name="temporaryActions"> description of action plan </param>
        /// <param name="mResponsiblePersonID"> responsible person for measuring action plan </param>
        /// <param name="mInitialDueDate"> initial due date for measuring action plan </param>
        /// <param name="mVerificationPlan"> verification plan for measuring action plan </param>
        /// <param name="aResponsiblePersonID"> responsible person for accountable action plan </param>
        /// <param name="aResourceNeeded"> resource needed for accountable action plan </param>
        /// <param name="tInitialDueDate"> initial due date for timely action plan </param>
        /// <param name="eResponsiblePersonID"> responsible person for effectiveness of action plan </param>
        /// <param name="eInitialDueDate"> initial due date to measure effectiveness of action plan </param>
        /// <param name="eValidationPlan"> validation plan effectiveness of action plan </param>
        /// <param name="rootCausesSelected"> root causes selected for action plan </param>
        public void SaveCorrectiveAction(int correctiveActionID, string eventId, Boolean isSmarter,string identifier, string description, string businessCase,
                                         string reviewNotes, string temporaryActions, int? mResponsiblePersonID, string mInitialDueDate, string mVerificationPlan,
                                         int? aResponsiblePersonID, string aResourceNeeded, string tInitialDueDate, int? eResponsiblePersonID, string eInitialDueDate,
                                         string eValidationPlan, string rootCausesSelected, string genericCausesSelected,int tasksResponsiblePersonId, int companyId,
                                        int userId,string mResponsiblePersonName, string aResponsiblePersonName, string eResponsiblePersonName,
                                        bool isManualMPersonResponsible, bool isManualEPersonResponsible, bool isManualAPersonResponsible)
        {

            try
            {
                int eventID = Convert.ToInt32(_cryptography.Decrypt(eventId));
                //Check if the save is for new task or edit task
                bool isNewInsert = (correctiveActionID == Convert.ToInt32(tap.dom.hlp.Enumeration.Types.NewInsert)) ? true : false;


                //Get the details of task if is called for editing
                tap.dat.CorrectiveActions correctiveAction = isNewInsert ? new tap.dat.CorrectiveActions() : GetCorrectiveActionsByID(correctiveActionID);                

                bool isCreateImplementationTask = (tInitialDueDate != null && correctiveAction.T_InitialDueDate == null);
                bool isCreateVerificationTask = (mInitialDueDate != null && correctiveAction.M_InitialDueDate == null);
                bool isCreateValidationTask = (eInitialDueDate != null && correctiveAction.E_InitialDueDate == null);



                //Set all properties for the task to be saved
                SetProperties(ref correctiveAction, eventID,  isSmarter, identifier,  description,  businessCase, reviewNotes,  temporaryActions,  mResponsiblePersonID,
                            mInitialDueDate, mVerificationPlan, aResponsiblePersonID, aResourceNeeded, tInitialDueDate, eResponsiblePersonID,eInitialDueDate,
                              eValidationPlan, isNewInsert);                

                //Save the changes to db
                SaveChanges(isNewInsert, correctiveAction, userId);            
                
                //Create tasks
                if (isSmarter && (isCreateImplementationTask || isCreateVerificationTask || isCreateValidationTask))
                //if (isSmarter)
                {
                    CreateTasks(correctiveAction, isCreateImplementationTask, isCreateVerificationTask, isCreateValidationTask,
                            tasksResponsiblePersonId, companyId, userId, mResponsiblePersonName, aResponsiblePersonName, eResponsiblePersonName, isNewInsert,
                            isManualMPersonResponsible, isManualEPersonResponsible, isManualAPersonResponsible);
                }
                else
                {
                    UpdateCorrectiveActionPersonIds(
                            Convert.ToInt32(correctiveAction.CorrectiveActionID), Convert.ToInt32(aResponsiblePersonID),
                            Convert.ToInt32(mResponsiblePersonID), Convert.ToInt32(eResponsiblePersonID), userId);
                }

             
                if (rootCausesSelected != string.Empty)
                {
                    //Save all root causes selected for action plan
                    SaveRootCausesSelected(correctiveAction.CorrectiveActionID, rootCausesSelected);
                }

                if (genericCausesSelected != string.Empty)
                {
                    //Save all root causes selected for action plan
                    SaveGenericCausesSelected(correctiveAction.CorrectiveActionID, genericCausesSelected);
                }

                //Update date modified for event
                tap.dom.usr.Event.UpdateDateModified(eventID);
               
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        

        /// <summary>
        /// Save all root causes selected for action plan
        /// </summary>
        /// <param name="correctiveActionID"> Corrective action id </param>
        /// <param name="rootCauses"> rct node string </param>
        public void SaveGenericCausesSelected(int correctiveActionID, string genericCauses)
        {

            try
            {

                if (genericCauses != string.Empty)
                {
                    tap.dom.action.CorrectiveActionRCT rct = new tap.dom.action.CorrectiveActionRCT();

                    string[] genericCauseNodes = genericCauses.Split(',');
                    int genericID = 0;
                    Boolean checkedValue = false;

                    //Get  the selected rct ids and related checked status and save to database
                    for (int i = 0; i < genericCauseNodes.Length; i++)
                    {

                        //Get rct id and the checked state
                        genericID = Convert.ToInt32(genericCauseNodes[i].Split(':')[0]);
                        checkedValue = Convert.ToBoolean(genericCauseNodes[i].Split(':')[1]);

                        //Save into database
                        rct.SaveGenericIdForCorrectiveAction(correctiveActionID, genericID, checkedValue);

                    }
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        /// <summary>
        /// Save all root causes selected for action plan
        /// </summary>
        /// <param name="correctiveActionID"> Corrective action id </param>
        /// <param name="rootCauses"> rct node string </param>
        public void SaveRootCausesSelected(int correctiveActionID, string rootCauses)
        {

            try
            {

                if (rootCauses != string.Empty)
                {
                    tap.dom.action.CorrectiveActionRCT rct = new tap.dom.action.CorrectiveActionRCT();

                    string[] rootCauseNodes = rootCauses.Split(',');
                    int rctID = 0;
                    Boolean checkedValue = false;

                    //Get  the selected rct ids and related checked status and save to database
                    for (int i = 0; i < rootCauseNodes.Length; i++)
                    {

                        //Get rct id and the checked state
                        rctID = Convert.ToInt32(rootCauseNodes[i].Split(':')[0]);
                        checkedValue = Convert.ToBoolean(rootCauseNodes[i].Split(':')[1]);

                        //Save into database
                        rct.SaveRCTForCorrectiveAction(correctiveActionID, rctID, checkedValue);

                    }
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        #endregion 

        #region "Create Tasks"

        /// <summary>
        /// Create 3 types of tasks based on the data provided for corrective action
        /// </summary>
        /// <param name="correctiveAction">Corrective action object created</param>
        public void CreateTasks(tap.dat.CorrectiveActions correctiveAction, bool isCreateImplementationTask, bool isCreateVerificationTask, bool isCreateValidationTask,
                                 int tasksResponsiblePersonId, int companyId, int userId, string mResponsiblePersonName, string aResponsiblePersonName, string eResponsiblePersonName,
                    bool isNewInsert, bool isManualMPersonResponsible, bool isManualEPersonResponsible, bool isManualAPersonResponsible)
        {

            try
            {

                tap.dom.action.Tasks task = new tap.dom.action.Tasks();

                int newInsert = isNewInsert ? 0 : 1;
            
                int isCompleted = 0;
                string taskNotes = string.Empty;
                int aPersonId=0;
                int mPersonId=0;
                int ePersonId=0;
                int taskId = 0;
                int correctiveActionId = correctiveAction.CorrectiveActionID;

                //TOFIX : While updating the the validation date, the implementation and verify responsible id in CA table are updating as zero
                //Save task of type Implementation
                //if (isCreateImplementationTask)
                //{

                    //KASI- FIND THE FIRST IMPL TASK F THE CAP ID AND SET IT TO newInsert
                    if (!isNewInsert) { 
                    int tasktypeId = TaskType.GetTaskTypeIDByName(TaskType.IMPLEMENTATION_TYPE);
                    var CA_Task = _entityContext.Tasks.Where(a => a.CorrectiveActionID == correctiveActionId && a.TaskTypeID == tasktypeId).First();
                    if (CA_Task != null)
                    {
                        newInsert = CA_Task.TaskID;
                    }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////

                     taskId = task.SaveTask(newInsert, correctiveAction.CorrectiveActionID, TaskType.GetTaskTypeIDByName(TaskType.IMPLEMENTATION_TYPE),
                                    null, correctiveAction.A_ResourceNeeded, taskNotes,
                                    isCompleted, Convert.ToDateTime(correctiveAction.T_InitialDueDate), companyId, userId, false);

                    aPersonId = task.SaveTasksResponsiblePerson(newInsert, (isNewInsert ? tasksResponsiblePersonId : Convert.ToInt32(correctiveAction.A_ResponsiblePersonID)), companyId, userId, taskId, aResponsiblePersonName,
                                                 isManualAPersonResponsible);

                //}


                //Save task of type Verification
                //if (isCreateVerificationTask)
                //{
                    //KASI- FIND THE FIRST verification TASK F THE CAP ID AND SET IT TO newInsert
                    if (!isNewInsert)
                    {
                        int tasktypeId = TaskType.GetTaskTypeIDByName(TaskType.VERIFICATION_TYPE);
                        var CA_Task = _entityContext.Tasks.Where(a => a.CorrectiveActionID == correctiveActionId && a.TaskTypeID == tasktypeId).First();
                        if (CA_Task != null)
                        {
                            newInsert = CA_Task.TaskID;
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////

                    taskId = task.SaveTask(newInsert, correctiveAction.CorrectiveActionID, TaskType.GetTaskTypeIDByName(TaskType.VERIFICATION_TYPE),
                                 null, correctiveAction.M_VerificationPlan, taskNotes,
                                 isCompleted, Convert.ToDateTime(correctiveAction.M_InitialDueDate), companyId, userId, false);

                    mPersonId = task.SaveTasksResponsiblePerson(newInsert, (isNewInsert ? tasksResponsiblePersonId : Convert.ToInt32(correctiveAction.M_ResponsiblePersonID)), companyId, userId, taskId, mResponsiblePersonName,
                                                    isManualMPersonResponsible);
                //}

                //Save task of type Validation
                //if (isCreateValidationTask)
                //{
                    //KASI- FIND THE FIRST VALIDATION TASK F THE CAP ID AND SET IT TO newInsert
                    if (!isNewInsert)
                    {
                        int tasktypeId = TaskType.GetTaskTypeIDByName(TaskType.VALIDATION_TYPE);
                        var CA_Task = _entityContext.Tasks.Where(a => a.CorrectiveActionID == correctiveActionId && a.TaskTypeID == tasktypeId).First();
                        if (CA_Task != null)
                        {
                            newInsert = CA_Task.TaskID;
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////

                     taskId = task.SaveTask(newInsert, correctiveAction.CorrectiveActionID, TaskType.GetTaskTypeIDByName(TaskType.VALIDATION_TYPE),
                                 null, correctiveAction.E_ValidationPlan, taskNotes,
                                 isCompleted, Convert.ToDateTime(correctiveAction.E_InitialDueDate), companyId, userId, false);
                    ePersonId = task.SaveTasksResponsiblePerson(newInsert, (isNewInsert ? tasksResponsiblePersonId : Convert.ToInt32(correctiveAction.E_ResponsiblePersonID)), companyId, userId, taskId, eResponsiblePersonName, 
                                                isManualEPersonResponsible);
               // }

                //update Person ids based on taskId
                UpdateCorrectiveActionPersonIds(correctiveAction.CorrectiveActionID, aPersonId, mPersonId, ePersonId,userId);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        /// <summary>
        /// Update Person id's
        /// </summary>
        /// <param name="correctiveActionId"></param>
        /// <param name="aPersonId"></param>
        /// <param name="mPersonId"></param>
        /// <param name="ePersonId"></param>
        public void UpdateCorrectiveActionPersonIds(int correctiveActionId, int aPersonId, int mPersonId, int ePersonId,int userId)
        {
            try
            {
                //Get the details of task if is called for editing
                tap.dat.CorrectiveActions correctiveAction = GetCorrectiveActionsByID(correctiveActionId);
                
                correctiveAction.CorrectiveActionID = correctiveActionId;                
                correctiveAction.A_ResponsiblePersonID = aPersonId;
                correctiveAction.M_ResponsiblePersonID = mPersonId;
                correctiveAction.E_ResponsiblePersonID = ePersonId;

                _entityContext.SaveChanges();
            }
            catch(Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
        }


        #endregion

        #region "Set Properties for Action Plans"
        /// <summary>
        /// Set properties to the corrective action object getting created or edited
        /// </summary>
        /// <param name="correctiveAction"> action plan object</param>
        /// <param name="eventID"> event id </param>
        /// <param name="isSmarter"> is action is a smarter action plan </param>
        /// <param name="identifier"> identifier of action </param>
        /// <param name="description"> description of action plan </param>
        /// <param name="businessCase"> description of action plan </param>
        /// <param name="reviewNotes"> description of action plan </param>
        /// <param name="temporaryActions"> description of action plan </param>
        /// <param name="mResponsiblePersonID"> responsible person for measuring action plan </param>
        /// <param name="mInitialDueDate"> initial due date for measuring action plan </param>
        /// <param name="mVerificationPlan"> verification plan for measuring action plan </param>
        /// <param name="aResponsiblePersonID"> responsible person for accountable action plan </param>
        /// <param name="aResourceNeeded"> resource needed for accountable action plan </param>
        /// <param name="tInitialDueDate"> initial due date for timely action plan </param>
        /// <param name="eResponsiblePersonID"> responsible person for effectiveness of action plan </param>
        /// <param name="eInitialDueDate"> initial due date to measure effectiveness of action plan </param>
        /// <param name="eValidationPlan"> validation plan effectiveness of action plan </param>
        /// <param name="rootCausesSelected"> root causes selected for action plan </param>
        public void SetProperties(ref tap.dat.CorrectiveActions correctiveAction, int eventID, Boolean isSmarter, string identifier, string description,
                                         string businessCase, string reviewNotes, string temporaryActions, int? mResponsiblePersonID, string mInitialDueDate, 
                                         string mVerificationPlan,int? aResponsiblePersonID, string aResourceNeeded, string tInitialDueDate, int? eResponsiblePersonID,
                                         string eInitialDueDate, string eValidationPlan,bool isNewInsert)
        {

            try
            {
                DateTimeFormatInfo dtfi = new DateTimeFormatInfo();
                dtfi.ShortDatePattern = "dd-MM-yyyy";
                dtfi.DateSeparator = "-";
                correctiveAction.EventID = eventID;
                correctiveAction.IsSmarter = isSmarter;
                correctiveAction.Identifier = identifier;
                correctiveAction.Description = description;
                correctiveAction.BusinessCase = businessCase;
                correctiveAction.ReviewNotes = reviewNotes;
                correctiveAction.TemporaryActions = temporaryActions;

                if (isNewInsert)
                    correctiveAction.M_ResponsiblePersonID = (mResponsiblePersonID == 0 ? null : mResponsiblePersonID);
                else
                    correctiveAction.M_ResponsiblePersonID = correctiveAction.M_ResponsiblePersonID;

                int? companyId = _entityContext.Events.Where(a => a.EventID == eventID).Select(a => a.CompanyID).FirstOrDefault();

                //If mInitialDueDate is entered by user then convert to datetime
                if (mInitialDueDate != string.Empty) {
                    correctiveAction.M_InitialDueDate = dt.fromDatefrmatToDate(mInitialDueDate, companyId);
                    //if (mInitialDueDate.Contains('/'))
                    //{
                    //    string[] dateParams = mInitialDueDate.Split('/');
                    //    if (dateParams[2].Length == 2)
                    //        dateParams[2] = "20" + dateParams[2];

                    //    DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[1]), Int32.Parse(dateParams[0]));
                    //    correctiveAction.M_InitialDueDate = date;
                    
                    //}
                    //else
                    //{
                    //    correctiveAction.M_InitialDueDate = Convert.ToDateTime(mInitialDueDate, dtfi);
                    //}
                    
                }

                correctiveAction.M_VerificationPlan = mVerificationPlan;
                if (isNewInsert)
                    correctiveAction.A_ResponsiblePersonID = (aResponsiblePersonID == 0 ? null : aResponsiblePersonID);
                else
                    correctiveAction.A_ResponsiblePersonID = correctiveAction.A_ResponsiblePersonID;

                correctiveAction.A_ResourceNeeded = aResourceNeeded;
                
                //If tInitialDueDate is entered by user then convert to datetime
                if (tInitialDueDate != string.Empty) {
                    correctiveAction.T_InitialDueDate = dt.fromDatefrmatToDate(tInitialDueDate, companyId);
                    //if (tInitialDueDate.Contains('/'))
                    //{
                    //    string[] dateParams = tInitialDueDate.Split('/');
                    //    if (dateParams[2].Length == 2)
                    //        dateParams[2] = "20" + dateParams[2];
                    //    DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[1]), Int32.Parse(dateParams[0]));
                    //    correctiveAction.T_InitialDueDate = date;
                    //}
                    //else
                    //{
                    //    correctiveAction.T_InitialDueDate = Convert.ToDateTime(tInitialDueDate, dtfi);
                    //}
                    //correctiveAction.T_InitialDueDate = Convert.ToDateTime(tInitialDueDate,dtfi); 
                }

                if (isNewInsert)
                    correctiveAction.E_ResponsiblePersonID = (eResponsiblePersonID == 0 ? null : eResponsiblePersonID);
                else
                    correctiveAction.E_ResponsiblePersonID = correctiveAction.E_ResponsiblePersonID;

                //If eInitialDueDate is entered by user then convert to datetime
                if (eInitialDueDate != string.Empty) {
                    correctiveAction.E_InitialDueDate = dt.fromDatefrmatToDate(eInitialDueDate, companyId);
                   //// correctiveAction.E_InitialDueDate = Convert.ToDateTime(eInitialDueDate,dtfi); 
                   // if (eInitialDueDate.Contains('/'))
                   // {
                   //     string[] dateParams = eInitialDueDate.Split('/');
                   //     if (dateParams[2].Length == 2)
                   //         dateParams[2] = "20" + dateParams[2];
                   //     DateTime date = new DateTime(Int32.Parse(dateParams[2]), Int32.Parse(dateParams[1]), Int32.Parse(dateParams[0]));
                   //     correctiveAction.E_InitialDueDate = date;
                   // }
                   // else
                   // {
                   //     correctiveAction.E_InitialDueDate = Convert.ToDateTime(eInitialDueDate, dtfi);
                   // }
                }

                correctiveAction.E_ValidationPlan = eValidationPlan;

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        /// <summary>
        /// Save changes to the database
        /// </summary>
        /// <param name="isNewInsert"> A boolean value to determine whether to add the object to db or only update the changes done</param>
        /// <param name="correctiveAction"> Action plan object</param>
        public void SaveChanges(bool isNewInsert, tap.dat.CorrectiveActions correctiveAction, int userId)
        {
            
            Enumeration.TransactionCategory transactionCategory = new Enumeration.TransactionCategory();
            var capType = (Convert.ToBoolean(correctiveAction.IsSmarter)) ? "Smart " : string.Empty;

            try
            {
                //If new corrective action is getting created then add to the corrective action table
                if (isNewInsert)
                {
                    _entityContext.AddToCorrectiveActions(correctiveAction);
                    _message = capType + "Corrective Action '" + correctiveAction.Identifier + "' created.";
                    transactionCategory = Enumeration.TransactionCategory.Create;
                }
                else
                {
                    _message = capType + "Corrective Action Id'" + correctiveAction.CorrectiveActionID + "' updated.";
                    transactionCategory = Enumeration.TransactionCategory.Update;
                }
                
                //Save the changes
                _entityContext.SaveChanges();
                transaction.Transaction.SaveTransaction(correctiveAction.EventID, userId, null, transactionCategory, _message);  

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        #endregion

        /// <summary>
        /// Get the corrective action details by the corrective Action id
        /// </summary>
        /// <param name="correctiveActionID">correctiveActionID by which the whole correctiveAction can be fetched out from db</param>
        /// <returns></returns>
        public tap.dat.CorrectiveActions GetCorrectiveActionsByID(int correctiveActionID)
        {
            tap.dat.CorrectiveActions actionPlan = new tap.dat.CorrectiveActions();

            try
            {
                actionPlan = _entityContext.CorrectiveActions.FirstOrDefault(a => a.CorrectiveActionID == correctiveActionID);
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return actionPlan;

        }

        #region "Populate Corrective Action Data Details "

        /// <summary>
        /// Get the corrective action details by correctiveActionID
        /// </summary>
        /// <param name="correctiveActionID"></param>
        /// <returns></returns>
        public object GetCorrectiveActionDetailsByID(int correctiveActionID)
        {

            try
            {

                tap.dom.usr.Event events = new tap.dom.usr.Event();
             
                var companyId = (from actionPlan in _entityContext.CorrectiveActions
                                join evts in _entityContext.Events on actionPlan.EventID equals evts.EventID
                                where actionPlan.CorrectiveActionID == correctiveActionID
                                select evts).FirstOrDefault().CompanyID;

                var subTabId = events.GetSubTabId(Convert.ToInt32(companyId));

                var correctiveActionDetail = from correctiveAction in _entityContext.CorrectiveActions
                                             where correctiveAction.CorrectiveActionID == correctiveActionID
                                             select new
                                             {
                                                 CorrectiveAction = correctiveAction,
                                                 Identifier = correctiveAction.Identifier,
                                                 Description = correctiveAction.Description,
                                                 BusinessCase = correctiveAction.BusinessCase,
                                                 ReviewNotes = correctiveAction.ReviewNotes,
                                                 TemporaryActions = correctiveAction.TemporaryActions,
                                                 SubTabId = subTabId,
                                                 Tasks = (from tsk in _entityContext.Tasks.AsEnumerable()
                                                         join tskType in _entityContext.TaskType on tsk.TaskTypeID equals tskType.TaskTypeID
                                                        // join users in _entityContext.Users on tsk.ResponsiblePersonID equals users.UserID
                                                         join taskResponsiblePerson in _entityContext.TasksResponsiblePersons on tsk.TaskID equals taskResponsiblePerson.TaskId

                                                         where tsk.CorrectiveActionID == correctiveAction.CorrectiveActionID
                                                         //let TaskStatus = GetTaskStatus(tsk)
                                                         select new
                                                         {
                                                             TaskID = tsk.TaskID,
                                                             TaskType = tskType.TaskTypeName,
                                                             TaskStatus = tsk.IsCompleted,
                                                             //TaskDueDateStatus = (tsk.DueDate < DateTime.Now.Date ? "PastDue" : "InCompleted"),
                                                             TaskDueDate = tsk.DueDate,
                                                             ResponsiblePerson = taskResponsiblePerson.DisplayName,
                                                             DueDate1 = tsk.DueDate,
                                                             Description = tsk.Description,
                                                             DueDateHistory = from taskDueDate in _entityContext.TaskDueDates
                                                                              where taskDueDate.TaskID == tsk.TaskID
                                                                              select new
                                                                              {
                                                                                  DueDateType = (taskDueDate.IsInitialDate) ? "Initial Due Date" : "Revised Due Date",
                                                                                  DueDate = taskDueDate.DueDate
                                                                              }
                                                         }).AsEnumerable().OrderBy(a => a.DueDate1),
                                                 RootCauses = from correctiveActionRCT in _entityContext.CorrectiveActionRCT
                                                              where correctiveActionRCT.CorrectiveActionID == correctiveActionID
                                                              select new { RCTID = correctiveActionRCT.RootCauseTreeID },

                                                GenericCauses = from correctiveActionRCT in _entityContext.CorrectiveActionRCT
                                                where correctiveActionRCT.CorrectiveActionID == correctiveActionID
                                                select new { GenericCauseID = correctiveActionRCT.GenericCauseID }
                                             };


                return correctiveActionDetail;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
                return null;
            }
            
        }

        #endregion

        /// <summary>
        /// Set row number of result set retrieved
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int SetRowIndex(ref int index)
        {
            index = index + 1;
            return index;
        }


        #region "Get CAP Details to populate"
        /// <summary>
        /// Get corrective action details by event id
        /// </summary>
        /// <param name="eventID"></param>
        /// <returns></returns>
        public object GetCAPDetails(string eventId)
        {

            try
            {
                int eventID = Convert.ToInt32(_cryptography.Decrypt(eventId));
                //int index = 0;
                var correctiveActionDetail = (from correctiveAction in _entityContext.CorrectiveActions.AsEnumerable()
                                              where correctiveAction.EventID == eventID
                                              //let RowIndex = SetRowIndex(ref index)
                                              select new
                                              {                                                 
                                                  CorrectiveActionID = correctiveAction.CorrectiveActionID,
                                                  Identifier = correctiveAction.Identifier,
                                                  IsSmarter = correctiveAction.IsSmarter,
                                                  Tasks = (from tsk in _entityContext.Tasks.AsEnumerable()
                                                           join tskType in _entityContext.TaskType.AsEnumerable() on tsk.TaskTypeID equals tskType.TaskTypeID
                                                           where tsk.CorrectiveActionID == correctiveAction.CorrectiveActionID                                                          
                                                           let TaskStatus = GetTaskStatus(tsk)
                                                           select new
                                                           {
                                                               Type = tskType.TaskTypeName,
                                                               DueDate = tsk.DueDate,
                                                               TaskStatus = TaskStatus
                                                           }).AsEnumerable().OrderBy(a => a.DueDate).ToList()
                                              }).AsEnumerable();



                return correctiveActionDetail;

            }
            catch (Exception ex)
            {

                tap.dom.hlp.ErrorLog.LogException(ex);
                return null;

            }

        }
        #endregion

        /// <summary>
        /// Get task status
        /// </summary>
        /// <param name="tsk"></param>
        /// <returns></returns>
        public string GetTaskStatus(tap.dat.Tasks tsk)
        {
            string taskStatus = string.Empty;

            try
            {
                taskStatus = (Convert.ToBoolean(tsk.IsCompleted)) ? "Completed" : tsk.DueDate < DateTime.Now.Date ? "PastDue" : "InCompleted";
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);             
            }

            return taskStatus;

        }

        #region "Get Corrective Action Information"
        /// <summary>
        /// GetCorrectiveActionInfo-By passing taskId get correctiveActionID and isSmarter to open popup page(CAP)
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public string GetCorrectiveActionInfo(int taskId)
        {
            tap.dom.hlp.EFSerializer _efSerializer = new dom.hlp.EFSerializer();

            var getCAPSmarterDetails = (from tasks in _entityContext.Tasks
                                        join ca in _entityContext.CorrectiveActions
                                        on tasks.CorrectiveActionID equals ca.CorrectiveActionID
                                        where tasks.TaskID == taskId
                                        select new
                                        {
                                            CorrectiveActionId=ca.CorrectiveActionID,
                                            isSmarter=ca.IsSmarter
                                        });

            return _efSerializer.EFSerialize(getCAPSmarterDetails);
        }
        #endregion

    }
   
}
