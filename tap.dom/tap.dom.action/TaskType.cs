﻿/********************************************************************************
 *  File Name   : TaskType.cs
 *  Description : This file contain all the database related code for the TaskType table
 *  Created on  : N/A
 *******************************************************************************/

# region NameSpace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion


namespace tap.dom.action
{
    public class TaskType
    {        

        //the private variable for the entity data model
        static tap.dat.EFEntity _entityContext = new dat.EFEntity();

        //Constants that are being used by smart corrcetive action page in order to create tasks without the create task page
        public const string IMPLEMENTATION_TYPE = "Implementation";
        public const string VERIFICATION_TYPE = "Verification";
        public const string VALIDATION_TYPE = "Validation";


        /// <summary>
        /// Get task type id by task type name
        /// </summary>
        /// <param name="taskTypeName"> name of task type</param>
        /// <returns>task type id</returns>
        public static Int32 GetTaskTypeIDByName(string taskTypeName)
        {
            int taskTypeId = 0;

            try
            {
                taskTypeId = _entityContext.TaskType.ToList().Where(a => a.TaskTypeName == taskTypeName).FirstOrDefault().TaskTypeID;
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }            

            return taskTypeId;
        }


        /// <summary>
        /// Get all task types used in the application
        /// </summary>
        /// <returns></returns>
        public object GetAllTaskTypes()
        {

            IList<tap.dat.TaskType> taskTypes = new List<tap.dat.TaskType>();

            try
            {
                taskTypes = _entityContext.TaskType.ToList();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            
            return taskTypes;

        }

    }

}
