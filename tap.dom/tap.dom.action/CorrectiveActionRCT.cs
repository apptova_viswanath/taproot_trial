﻿/********************************************************************************
 *  File Name   : CorrectiveActionRCT.cs
 *  Description : This file contains all the database related code for the Root Cause Tree
 *  Created on  : N/A
 *******************************************************************************/

# region NameSpace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion


namespace tap.dom.action
{

    //This class contains all the database related code for the Root Cause Tree
    public class CorrectiveActionRCT
    {

        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();


        /// <summary>
        /// Save root cause tree for corrective action
        /// </summary>
        /// <param name="capID">action plan id</param>
        /// <param name="rootCuaseTreeID">root cause selected for action plan</param>
        /// <param name="checkedValue">checked status of rct node</param>
        public void SaveRCTForCorrectiveAction(int capID, int rootCuaseTreeID, bool checkedValue)
        {

            try
            {

                if (checkedValue)
                {

                    tap.dat.CorrectiveActionRCT correctiveActionRCT = new tap.dat.CorrectiveActionRCT();

                    //Set values
                    correctiveActionRCT.RootCauseTreeID = rootCuaseTreeID;
                    correctiveActionRCT.GenericCauseID = 0;
                    correctiveActionRCT.CorrectiveActionID = capID;

                    //Save changes
                    _entityContext.AddToCorrectiveActionRCT(correctiveActionRCT);
                    _entityContext.SaveChanges();

                }
                else
                {
                    //Delete root cause tree created for a corrective action
                    DeleteCorrectiveActionRCT(capID, rootCuaseTreeID);
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }
   
        
        
              /// <summary>
        /// Save root cause tree for corrective action
        /// </summary>
        /// <param name="capID">action plan id</param>
        /// <param name="rootCuaseTreeID">root cause selected for action plan</param>
        /// <param name="checkedValue">checked status of rct node</param>
        public void SaveGenericIdForCorrectiveAction(int capID, int genericCuaseTreeID, bool checkedValue)
        {

            try
            {

                if (checkedValue)
                {

                    tap.dat.CorrectiveActionRCT correctiveActionRCT = new tap.dat.CorrectiveActionRCT();

                    //Set values
                    correctiveActionRCT.RootCauseTreeID = 0;
                    correctiveActionRCT.GenericCauseID = genericCuaseTreeID;
                    correctiveActionRCT.CorrectiveActionID = capID;

                    //Save changes
                    _entityContext.AddToCorrectiveActionRCT(correctiveActionRCT);
                    _entityContext.SaveChanges();

                }
                else
                {
                    //Delete root cause tree created for a corrective action
                    DeleteCorrectiveActionRCTByGenericCause(capID, genericCuaseTreeID);
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

        
   
        /// <summary>
        /// Delete root cause tree created for a corrective action
        /// </summary>
        /// <param name="capID"> action plan id </param>
        /// <param name="rootCuaseTreeID"> root cause selected for action plan </param>
        public void DeleteCorrectiveActionRCTByGenericCause(int capID, int genericCauseID)
        {

            try
            {
                
                tap.dat.CorrectiveActionRCT correctiveActionRCT = _entityContext.CorrectiveActionRCT
                                                                  .Where(a => a.GenericCauseID == genericCauseID && a.CorrectiveActionID == capID).FirstOrDefault();
                
                //Delete and save changes
                _entityContext.DeleteObject(correctiveActionRCT);
                _entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }


        /// <summary>
        /// Delete root cause tree created for a corrective action
        /// </summary>
        /// <param name="capID"> action plan id </param>
        /// <param name="rootCuaseTreeID"> root cause selected for action plan </param>
        public void DeleteCorrectiveActionRCT(int capID, int rootCauseTreeID)
        {

            try
            {
                
                tap.dat.CorrectiveActionRCT correctiveActionRCT = _entityContext.CorrectiveActionRCT
                                                                  .Where(a => a.RootCauseTreeID == rootCauseTreeID && a.CorrectiveActionID == capID).FirstOrDefault();
                
                //Delete and save changes
                _entityContext.DeleteObject(correctiveActionRCT);
                _entityContext.SaveChanges();

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

        }

    }

}
