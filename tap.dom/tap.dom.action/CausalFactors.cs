﻿//----------------------------------------------------------------------------------------------------------
// File Name 	: CausalFactors.cs
// Date Created : N/A
// Description  : This file contains all the functions related to causal factor
//----------------------------------------------------------------------------------------------------------

# region NameSpace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.gen;

#endregion


namespace tap.dom.action
{

   
    //This class contains all the functions related to causal factor
    public class CausalFactors
    {
        tap.dom.gen.Cryptography _cryptography = new dom.gen.Cryptography();
        const string AUTUMN_SEASON = "Autumn";
        const int RCID_TOLERABLE_FAILURE = 152;
        const int RCID_NATURAL_SABOTAGE = 187;
        const int RCID_OTHER = 188;
        const int RCID_DESIGN_REVIEW = 159;
        const int RCID_MANAGEMENT_SYSTEM = 182;
        
        const int RCID_EQUIPMENT_DIFFICULTY = 151;
        const int RCID_PROCEDURES = 21;
        const int RCID_TRAINING = 48;
        const int RCID_HPD_QUALITY_CONTROL = 61;
        const int RCID_COMMUNICATIONS = 70;
        const int RCID_HPD_MANAGEMENT_SYSTEM = 84;
        const int RCID_HUMAN_ENGINEERING = 107;
        const int RCID_WORK_DIRECTION = 133;


        //The private variable for the entity data model
        tap.dat.EFEntity _entityContext = new dat.EFEntity();

        public object GetCausalFactorNameAndTabStatus(int causalFactorId, int userId)
        {
            
            //string spanishLanguage = "es";
            string englishLanguage = ForeignLanguage.English_Language.ToString();
            //string frenchlanguage = "fr";

                var users = (from user in _entityContext.Users
                             join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                             where user.UserID == userId
                             select new
                             {
                                 languageId = user.LanguageId,
                                 LangguageName = language.LangaugeCode
                             }).FirstOrDefault();

                int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
                int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));

                string languageName = ((users == null) ? ForeignLanguage.English_Language : users.LangguageName);

                var isTitleAvaliable = (from title in _entityContext.RootCausesLanguages 
                                        where  title.LanguageId == languageId select title).FirstOrDefault();

                if (isTitleAvaliable == null)
                {
                    languageId = englishLangugeId;
                    languageName = ForeignLanguage.English_Language;
                }


            var multiLanguageRCTTitles =  from rootCauses in _entityContext.RootCauses join rctTitles in _entityContext.RootCausesLanguages
                                          on rootCauses.RootCauseID equals rctTitles.RootCauseId where rctTitles.LanguageId == languageId select rctTitles;

                                   
            var causalFactorNameAndTabStatus = //(from cf in _entityContext.CausalFactors where cf.CausalFactorID == CausalFactorId select cf.Name).AsEnumerable();
                                (from causalFactor in _entityContext.CausalFactors.AsEnumerable()
                                 where causalFactor.CausalFactorID == causalFactorId
                                 select new
                                 {
                                     
                                     EquipmentText = 
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_EQUIPMENT_DIFFICULTY select multiLanguageRCTTitle.Title,
                                     
                                     ProcedureText = 
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_PROCEDURES select multiLanguageRCTTitle.Title, 
                                     
                                     TrainingText =  
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_TRAINING select multiLanguageRCTTitle.Title,
                                     
                                     QualityControlText =  
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_HPD_QUALITY_CONTROL select multiLanguageRCTTitle.Title,
                                     
                                     CommunicationText =  
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_COMMUNICATIONS select multiLanguageRCTTitle.Title,
                                     
                                     ManagementText =  
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_MANAGEMENT_SYSTEM select multiLanguageRCTTitle.Title,
                                     
                                     HumanEngineeringText =  
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_HUMAN_ENGINEERING select multiLanguageRCTTitle.Title,

                                     WorkDirectionText = 
                                     from multiLanguageRCTTitle in multiLanguageRCTTitles where multiLanguageRCTTitle.RootCauseId == RCID_WORK_DIRECTION select multiLanguageRCTTitle.Title,

                                     RCTLanguageName = languageName,

                                     GenericCausesCount = (from genericRCT in _entityContext.RootCauseTree.AsEnumerable()
                                                           join rootCauses in _entityContext.RootCauses.AsEnumerable()
                                                           on genericRCT.RootCauseID equals rootCauses.RootCauseID
                                                           where genericRCT.CausalFactorID == causalFactorId  && genericRCT.IsSelected == true
                                                            && rootCauses.RootCauseCategoryID == 3 
                                                           select genericRCT).ToList().Count(),                  

                                     CausalFactorName = causalFactor.Name,

                                     TabStatus = (from rootCauseTree in _entityContext.RootCauseTree.AsEnumerable()
                                                 join rootCauses in _entityContext.RootCauses.AsEnumerable() on rootCauseTree.RootCauseID
                                                 equals rootCauses.RootCauseID
                                                 where rootCauseTree.CausalFactorID == causalFactorId 
                                                 select new
                                                 {
                                                     Title = rootCauses.Title,
                                                     RootCauseID = rootCauses.RootCauseID,
                                                     RootCauseCategoryID = rootCauses.RootCauseCategoryID,
                                                     ParentId = rootCauses.ParentID,
                                                     ParentTitle = from rct in _entityContext.RootCauses.AsEnumerable() where rct.RootCauseID == rootCauses.ParentID select rct.Title ,
                                                     IsSelected = rootCauseTree.IsSelected
                                                 }),

                                     IsCompleted = causalFactor.IsComplete
                                     
                                 });

            return causalFactorNameAndTabStatus;
        }



        /// <summary>
        /// Get all causal factor details by event id
        /// </summary>
        /// <param name="eventID"> event id</param>
        /// <returns>causal factors</returns>
        public object GetCausalFactorDetails(string eventId, int userId)
        {

            try
            {
                int eventID = Convert.ToInt32(_cryptography.Decrypt(eventId));
                tap.dom.rct.RCTGenericCause rctGenericCause = new tap.dom.rct.RCTGenericCause();

                var autumnSeasonID = (from season in _entityContext.Seasons where season.SeasonName == AUTUMN_SEASON select season).FirstOrDefault().SeasonID;

                //(HARD CODED FOR NOW ONLY, as no causal factor screen is present in the application)
                var snapChartDetails = (from snapCharts in _entityContext.SnapCharts
                                        where snapCharts.EventID == eventID &&
                                            snapCharts.SeasonID == autumnSeasonID
                                        select snapCharts).FirstOrDefault();

                if (snapChartDetails != null)
                {
                    int snapChartID = snapChartDetails.SnapChartID;

                    adm.ListValues listValues = new adm.ListValues();

                    int correctiveType = Convert.ToInt32(tap.dom.hlp.Enumeration.DictionaryTypes.CorrectiveAction);
                    int rctType = Convert.ToInt32(tap.dom.hlp.Enumeration.DictionaryTypes.RootCauseTree);

                    var rcType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == "Root Cause" select rootCauseType).FirstOrDefault();
                    var nearRCType = (from rootCauseType in _entityContext.RootCauseCategories where rootCauseType.Category == "Near" select rootCauseType).FirstOrDefault();

                    int rootCauseCategoryID = Convert.ToInt32(rcType.RootCauseCategoryID);
                    int nearRootCauseCategoryID = Convert.ToInt32(nearRCType.RootCauseCategoryID);

                    var corrcetiveActions = (from ca in _entityContext.CorrectiveActions where ca.EventID == eventID select ca.CorrectiveActionID).AsEnumerable();

                  
                    string englishLanguage = "en";
                    var users = (from user in _entityContext.Users
                             join language in _entityContext.Languages on user.LanguageId equals language.LanguageID
                             where user.UserID == userId
                             select new
                             {
                                 languageId = user.LanguageId,
                                 LangguageName = language.LangaugeCode
                             }).FirstOrDefault();

                    int englishLangugeId = _entityContext.Languages.FirstOrDefault(a => a.LangaugeCode == englishLanguage).LanguageID;
                    int languageId = ((users == null) ? englishLangugeId : Convert.ToInt32(users.languageId));
                    string languageName = ((users == null) ? englishLanguage : users.LangguageName);
                    var languageQuesionsCount=  (from rowcount
                          in _entityContext.RCTDictionaryQuestionLanguages
                                           where rowcount.LanguageId == languageId
                                           select rowcount).Count();
              


                if(languageQuesionsCount <= 0)
                {
                    languageId = englishLangugeId;
                    languageName = englishLanguage;
                }





                    //Get causal factor details
                    var causalFactorDetail = 
                        (from causalFactor in _entityContext.CausalFactors
                        where causalFactor.SnapChartID == snapChartID
                        select new
                        {
                            CausalFactorID = causalFactor.CausalFactorID,
                            Name = causalFactor.Name,
                            IsComplete = causalFactor.IsComplete,
                            RootCauses = 
                                (from rootCauseTree in _entityContext.RootCauseTree
                                 join rootCauses in _entityContext.RootCauses on rootCauseTree.RootCauseID equals rootCauses.RootCauseID                            
                                 join rctLanguages in _entityContext.RootCausesLanguages on 
                                 rootCauses.RootCauseID equals rctLanguages.RootCauseId                               
                                 join rootCauseCategory in _entityContext.RootCauseCategories on rootCauses.RootCauseCategoryID equals rootCauseCategory.RootCauseCategoryID
                                 where rctLanguages.LanguageId == languageId &&
                                 rootCauseTree.CausalFactorID == causalFactor.CausalFactorID && rootCauseTree.IsSelected == true &&
                                 rootCauseTree.RootCauses.RootCauseID != RCID_DESIGN_REVIEW &&
                                 rootCauseTree.RootCauses.RootCauseID != RCID_MANAGEMENT_SYSTEM

                                 &&

                                 (
                                    (rootCauses.RootCauseCategoryID == rootCauseCategoryID)
 
                                 ||

                                  (
                                      (rootCauses.RootCauseID == RCID_NATURAL_SABOTAGE)//This below section for Hardcoded rcts
                                              || (rootCauses.RootCauseID == RCID_OTHER)
                                              || (rootCauses.RootCauseID == RCID_TOLERABLE_FAILURE)
                                   )
                                  
                                  || // This below section was added for - Near Root Causes Are Root Causes when Children are Eliminated


                                 (
                                    (rootCauses.RootCauseCategoryID == nearRootCauseCategoryID)
                                      && ((from rootCauses1 in _entityContext.RootCauses
                                           where rootCauses1.ParentID == rootCauses.RootCauseID && rootCauses1.RootCauseCategoryID == rootCauseCategoryID 
                                           select rootCauses1).Count() ==                                  
                                           (from rootCauseTree1 in _entityContext.RootCauseTree
                                            where rootCauseTree1.CausalFactorID == causalFactor.CausalFactorID && rootCauseTree1.IsSelected == false &&
                                            rootCauseTree1.RootCauses.ParentID == rootCauses.RootCauseID
                                            && rootCauseTree1.RootCauses.RootCauseCategoryID == rootCauseCategoryID                                        
                                            select rootCauseTree1).Count())                                           
                                    )
                                 )
                                       
                                 
                                 
                                 orderby rootCauses.RootCauseID
                                   // (
                                   //     rootCauseTree.CausalFactorID == causalFactor.CausalFactorID && rootCauseTree.IsSelected == true && 
                                   //     (  
                                   //         (rootCauses.RootCauseCategoryID == rootCauseCategoryID) || 
                                   //         (rootCauses.RootCauseCategoryID == nearRootCauseCategoryID && 
                                  
                                   //             (
                                   //                 (from rootCauses1 in _entityContext.RootCauses 
                                   //                 where rootCauses1.ParentID == rootCauses.RootCauseID && rootCauses1.RootCauseCategoryID == rootCauseCategoryID 
                                   //                 select rootCauses1).Count() == 5
                                  
                                   //                 //(from rootCauseTree1 in _entityContext.RootCauseTree
                                   //                 //where rootCauseTree1.CausalFactorID == causalFactor.CausalFactorID && rootCauseTree1.IsSelected == false && rootCauseTree1.RootCauses.ParentID == rootCauses.RootCauseID
                                   //                 //select rootCauseTree1).Count()
                                   //             )
                                   //         )
                                   //     )
                                   //)


                                
                                            //let rc = listValues.FormattedFullpath(rootCauses.FullPath)
                                            select new
                                            {
                                                RootCauseTreeID = rootCauseTree.RootCauseTreeID,
                                                RootCauseID = rootCauses.RootCauseID,
                                                RootCauseType = rootCauseCategory.Category,
                                                RootCause = rctLanguages.Title, //rootCauses.Title,
                                                AddressedByCA = (from carct in _entityContext.CorrectiveActionRCT
                                                                    where
                                                                        corrcetiveActions.Contains(carct.CorrectiveActionID)
                                                                        && carct.RootCauseTreeID == rootCauseTree.RootCauseTreeID
                                                                    select carct).Count(),
                                                //RCTDictionary = (from dictionary in _entityContext.DictionaryDefinition
                                                //                    where dictionary.RootCauseID == rootCauses.RootCauseID &&
                                                //                        dictionary.DictionaryTypeID == rctType
                                                //                    select dictionary.HTMLDescription).FirstOrDefault(),
                                                //CAPDictionary = (from dictionary in _entityContext.DictionaryDefinition
                                                //                    where dictionary.RootCauseID == rootCauses.RootCauseID &&
                                                //                        dictionary.DictionaryTypeID == correctiveType
                                                //                    select dictionary.HTMLDescription).FirstOrDefault()
                                                                   
                                            }),

                            GenericCauses = (from genericCauses in _entityContext.GenericCause
                                            where genericCauses.CausalFactorID == causalFactor.CausalFactorID
                                            select new
                                            {
                                                Restate = genericCauses.Restate,
                                                GenericCauseID = genericCauses.GenericCauseID,
                                                GenericCauseAddressedByCA = (from carct in _entityContext.CorrectiveActionRCT
                                                                where
                                                                    corrcetiveActions.Contains(carct.CorrectiveActionID)
                                                                    && carct.GenericCauseID == genericCauses.GenericCauseID
                                                                select carct).Count(),
                                            }),
                                            });

                    return causalFactorDetail;
                }

            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }

            return null;
        }

    }
}
