﻿//---------------------------------------------------------------------------------------------
// File Name 	: AttachmentsInfo.cs

// Date Created  : N/A

// Description   : A class used for definig the properties of jqgrid and jqgridrow
//---------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tap.dom.hlp
{
    //jqgrid properties
    public class jqGridJson
    {
        public int total { get; set; }
        public int page { get; set; }
        public int records { get; set; }
        public jqGridRowJson[] rows { get; set; }
    }

    //jqgridrow properties
    public class jqGridRowJson
    {
        public string id { get; set; }
        public string[] cell { get; set; }
    }
}
