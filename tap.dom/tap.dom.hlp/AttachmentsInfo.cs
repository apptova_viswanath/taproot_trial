﻿//---------------------------------------------------------------------------------------------
// File Name 	: AttachmentsInfo.cs

// Date Created  : N/A

// Description   : A class used to hold the properties used for Attachemnets
//---------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.hlp
{
    public class AttachmentsInfo
    {
        public int? hirNode { get; set; }
        public int? hirNodeParent { get; set; }
        public int? hirLevel { get; set; }
        public string title { get; set; }
        public string fullPath { get; set; }
        public int? attachId { get; set; }
        public int? attachFolderId { get; set; }
        public string documentName { get; set; }
        public string extension { get; set; }
        public int? eventId { get; set; }
    }
}
