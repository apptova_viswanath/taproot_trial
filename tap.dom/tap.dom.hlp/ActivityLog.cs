﻿/*Acitivity Log Details mathods*/
#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
#endregion

namespace tap.dom.hlp
{
    public class ActivityLog
    {
        tap.dat.EFEntity _db = new tap.dat.EFEntity();
        Enumeration.Types nullvalue = Enumeration.Types.NullValue;

        #region Insert Activity Logging in "UserTransactionsDaily"
        public bool SaveActivityLog(string companyID, int? eventId, string userID, int categoryId,string logDetails)
        {
            tap.dat.UserTransactionsDaily transaction = null;
          
            //Decrypt the value and pass
            int companyId = Convert.ToInt32(companyID);
            int userId = Convert.ToInt32(userID);
            try
            {
                transaction = new tap.dat.UserTransactionsDaily();

                transaction.CompanyID = companyId;
                if (eventId == Convert.ToInt32(nullvalue))
                    transaction.EventID = null;
                else
                    transaction.EventID = eventId;

                transaction.UserID = userId;
                transaction.TransactionTypeID = categoryId;
                transaction.Details = logDetails;
                transaction.TransactionDate = DateTime.UtcNow; //GMT; 
                transaction.IpAddress = tap.dom.transaction.Transaction.GetIPAddress();
                transaction.MACAddress = tap.dom.transaction.Transaction.GetMacAddress();

                _db.AddToUserTransactionsDaily(transaction);
                _db.SaveChanges();
            }
            catch (Exception ex)
            {
                tap.dom.hlp.ErrorLog.LogException(ex);
            }
            return true;
        }
        #endregion

        #region "Search Activity logging"
        public object GetActivityLogDetails(int companyId, int userId,string toDate,string fromDate)
        {
            var logDetails = from e in _db.UserTransactionsDaily
                             where e.TransactionTypeID == companyId && e.UserID == userId
                              select new
                              {
                                  TransactionId = e.TransactionID,
                                  CompanyId = e.CompanyID,
                                  EventId = e.EventID,
                                  UserId = e.UserID,
                                  CategoryId = e.TransactionTypeID,                                  
                                  Details=e.Details,
                                  DateTimeStamp = e.TransactionDate
                              };

            List<string> logDateList = new List<string>();           
            logDetails = logDetails.Where(a => logDateList.Contains(a.Details) && (a.DateTimeStamp >=Convert.ToDateTime(fromDate) && a.DateTimeStamp < Convert.ToDateTime(toDate))); 

            return logDetails.ToList();
           
        }
        #endregion

    }
}
