﻿//--------------------------------------------------------------------------------------------------------
// File Name 	: EncryptDecrypt.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Encryption and Decryption (Generic Method)

//--------------------------------------------------------------------------------------------------------

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tap.dom.hlp;
using System.Security.Cryptography;
using System.IO;
#endregion

namespace tap.dom.hlp
{
    public class EncryptDecrypt
    {
        //private static Byte[] KEY_64 = { 42, 16, 93, 156, 78, 4, 218, 32 };
        //private static Byte[] IV_64 = { 55, 103, 246, 79, 36, 99, 167, 3 };

        //#region "Encrypt the Data"
        ///// <summary>
        ///// Encrypt
        ///// </summary>
        ///// <param name="stringToEncrypt"></param>
        ///// <returns></returns>
        //public static string Encrypt(string stringToEncrypt)
        //{
        //    if (stringToEncrypt != string.Empty)
        //    {
        //        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        //        MemoryStream ms = new MemoryStream();
        //        CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateEncryptor(KEY_64, IV_64), CryptoStreamMode.Write);
        //        StreamWriter sw = new StreamWriter(cs);

        //        sw.Write(stringToEncrypt);
        //        sw.Flush();
        //        cs.FlushFinalBlock();
        //        ms.Flush();
        //        return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
        //    }

        //    return string.Empty;
        //}
        //#endregion

        //#region "Descrypt the value from Encrypted format"
        ///// <summary>
        ///// Decrypt
        ///// </summary>
        ///// <param name="decryptValue"></param>
        ///// <returns></returns>
        //public static string Decrypt(string decryptValue)
        //{
        //    if (decryptValue != string.Empty && decryptValue != null)
        //    {
        //        DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
        //        Byte[] buffer = Convert.FromBase64String(decryptValue);
        //        MemoryStream ms = new MemoryStream(buffer);
        //        CryptoStream cs = new CryptoStream(ms, cryptoProvider.CreateDecryptor(KEY_64, IV_64), CryptoStreamMode.Read);
        //        StreamReader sr = new StreamReader(cs);
        //        return sr.ReadToEnd();
        //    }


        //    return string.Empty;
        //}
        //#endregion
    }
}
