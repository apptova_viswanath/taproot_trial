﻿
//--------------------------------------------------------------------------------------------------------
// File Name 	: EFSerializer.cs

// Date Created  : N/A

// Description   : A class used to do the operation related to Serialization (Generic Method)

//--------------------------------------------------------------------------------------------------------

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace tap.dom.hlp
{
   public  class EFSerializer
    {
        //Creates an instance of EFJavaScriptSerializer and serializes the provided object.

        #region  "Javascript Serializer Generic Method"

       /// <summary>
        /// EFSerialize- Javascript Serializer and returns JSON object
       /// </summary>
       /// <param name="rawData"></param>
       /// <returns></returns>
        public string EFSerialize(Object rawData)
       {
           try
           {
               tap.dom.jsc.EFJavaScriptSerializer _efSerializer = new tap.dom.jsc.EFJavaScriptSerializer();
               return _efSerializer.Serialize(rawData); 
 
           }
           catch (Exception ex)
           {
               tap.dom.hlp.ErrorLog.LogException(ex);
           }

           return null;
       }

        #endregion
    }
}
