﻿/*SQL Execute Queries methods*/

#region "Namespaces"
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Objects;
using System.Data.Metadata.Edm;
using System.Data.Objects.ELinq;
using System.Data.Entity;
using System.Reflection;
using tap.dom.hlp;
using System.Data.Common;
#endregion

namespace tap.dom.hlp
{
    public class SqlQueryInfo
    {
        #region "Execute Query - Sql Connection"

        string connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;

        /// <summary>
        /// ExecuteQuery-sql connection used
        /// </summary>
        /// <param name="cmdText"></param>
        /// <returns></returns>
        public DataTable ExecuteQuery(string cmdText)
        {

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = con.CreateCommand();

                cmd.CommandText = cmdText;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                con.Close();
                dr = null;

                return dt;
            }
        }
        #endregion
    }
}
