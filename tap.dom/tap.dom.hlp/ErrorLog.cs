﻿/*Dispaly Error Log Details in Notepad*/

#region "Namespace"
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using System.Data;
#endregion

namespace tap.dom.hlp
{
    public class ErrorLog
    {
        #region "Method to Trace the Error Log"
        public static void LogException(Exception exceptionToLog, string optionalClassName = "ClassName", string optionalFunctionName = "FunctionName")
        {
            //string tempHolder = string.Empty;
            StringBuilder tempHolder = new StringBuilder();
            string logfilePath = string.Empty;
            try
            {
                //tap.dom.adm.CommonOperation com = new tap.dom.adm.CommonOperation();
                if (System.Web.HttpContext.Current != null)
                {
                logfilePath = System.Web.HttpContext.Current.Server.MapPath("~/Log/");//ConfigurationManager.AppSettings["VirtualErrorLog"].ToString();
                }
                else{
                    logfilePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "\\Log";
                }

                if (Directory.Exists(logfilePath) == false)
                {
                    Directory.CreateDirectory(logfilePath);
                }
                using (FileStream logFileStream = new FileStream(Path.Combine(logfilePath, "Exc_" + DateTime.Now.ToString("MMdd") + ".log"), FileMode.Append, FileAccess.Write, FileShare.Write))
                {
                    using (StreamWriter logStreamWriter = new StreamWriter(logFileStream))
                    {
                        tempHolder.Append(tempHolder.ToString().PadRight(80, Convert.ToChar("*"))); 

                        tempHolder.Append("Occurred At: " + DateTime.Now.ToLongTimeString() + Environment.NewLine);

                        tempHolder.Append("Class Name: " + optionalClassName + Environment.NewLine);
                        tempHolder.Append("Function Name: " + optionalFunctionName + Environment.NewLine);

                        tempHolder.Append("Description: " + exceptionToLog.Message.ToString() + Environment.NewLine);

                        if (exceptionToLog.InnerException != null)
                        {
                            tempHolder.Append("InnerException: " + exceptionToLog.InnerException.Message + Environment.NewLine);
                        }
                        if (exceptionToLog.Source != null)
                        {
                            tempHolder.Append("Source: " + exceptionToLog.Source.ToString() + Environment.NewLine);                            
                        }
                        if (exceptionToLog.TargetSite != null)
                        {
                            tempHolder.Append("Target: " + exceptionToLog.TargetSite.Name.ToString() + Environment.NewLine);                           
                        }
                        if (exceptionToLog.StackTrace != null)
                        {
                            tempHolder.Append("Stack Trace: " + exceptionToLog.StackTrace.ToString() + Environment.NewLine);
                        }
                        if (exceptionToLog.HelpLink != null)
                        {
                            tempHolder.Append("HelpLink: " + exceptionToLog.HelpLink + Environment.NewLine);
                        }

                        logStreamWriter.WriteLine(tempHolder);
                    }
                }              

            }
            catch
            {
                //throw ex;
            }
        }
        #endregion
    }
}
