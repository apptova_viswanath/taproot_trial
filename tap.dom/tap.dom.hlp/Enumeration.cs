﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tap.dom.hlp
{
    /// <summary>
    /// To remove hard coded values and replace with enum values
    /// Created By: Deepa RJ
    /// Created Date: 30-05-2012
    /// </summary>
    public class Enumeration
    {
        #region "For New Types"
        public enum Types
        {
            New = -1,
            NullValue = 0,
            ParentNode = 1,
            NewInsert = 0
        }
        #endregion

        #region "For Dictionary Types"
        public enum DictionaryTypes
        {
            RootCauseTree = 1,
            CorrectiveAction = 2
        }
        #endregion

        #region For Module
        public enum Modules
        {
            Incident = 1,
            Investigation = 2,
            Audit = 3,
            ActionPlan=4
                
            //CAP = 4,
            //CorrectiveActionPlan = 4
           
        }
        #endregion

        #region  for New Module name
        public enum ModuleNew
        {
            NewIncident = 1,
            NewInvestigation = 2,
            NewAudit = 3,
            NewCap = 4
        }

        #endregion

        #region  for Password Expiration
        public enum PasswordExpiration
        {
            days,
            months,
            year,
            never
            
        }

        #endregion

        #region  for Existing Module name
        public enum ExistingModule
        {
            IncidentData = 1,
            InvestigationData = 2,
            NewAuditData = 3,
            CapData = 4
        }
        #endregion

        #region For Status
        public enum EventStatus
        {
            Active = 1,
            Archived = 2,
            Completed = 3
        }
        #endregion

        #region "System Fields"
        public enum SystemFields
        {
            Events_Location = 1,
            Events_Classification = 2
        }
        #endregion

        #region Hirlevel
        public enum HirLevel
        {
            Attachment = 1,
            TopNav = 2
        }
        #endregion

        #region TabType
        public enum TabType
        {
            SubTab,
            MainTab
        }
        #endregion

        #region "For Display Types"
        public enum Display
        {
            DopdownDisplay = 1,
            TreeviewDisplay = 2
        }
        #endregion

        #region For defining Enumeration For Control Type
        /// <summary>
        ///Description-Enumeration for Control Type
        /// </summary>
        public enum ControlType
        {
            TextBox,
            ParagraphText,
            CheckBox,
            Selectonefromlist,
            Selectmultiplefromlist,
            DateField
        }
        #endregion

        #region For Enumeration For DataType
        /// <summary>
        /// Description-Enumeration for DataType
        /// </summary>
        public enum DataType
        {
            Int,
            String,
            Boolean,
            DateTime,
            Date
        }
        #endregion

        #region SnapChart Shape Defaults style
        public enum DefaultShapeStyle
        {
            FontSize ,
            FontColor ,
            FontBold ,
            FontItalic ,
            FontUnderline,
            BackgroundColor,
            FontFamily,
            FontStyle
        }
        #endregion


        #region Season
        public enum Season
        {
            Spring = 1,
            Summer = 2,
            Autumn = 3,
            Winter = 4
        }
        #endregion

        #region "Field Names for Reports"
        public enum ReportFields
        { 
            Label=1,
            DataField=4,
            CustomField=5,
            StandAlone=6,
            Integrate=7
        }
        #endregion

        #region ""

        public enum RootCauseTreeType
        {
          	Near = 1,
	        Basic = 2,
	        RootCause = 3,
	        QuestionNode = 4,
	        TreeNode = 5
        }
        #endregion

        #region "Custom Elements"
        public enum CustomElementTypes
        {
            Line,
            Label,
            CompanyLogo,
            CompanyName,
            Image
        }
        #endregion

        #region "Transaction Category"
        public enum TransactionCategory
        {
            Create = 1,
            Update = 2,
            Delete = 3
        }
        #endregion

    }
}
